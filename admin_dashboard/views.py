
# coding= utf-8 
# -*- coding: utf-8 -*-

import hashlib
import json
import operator
import uuid

import psycopg2
import flask
from flask import Flask, Blueprint, request, session, render_template, flash, url_for
from flask_login import current_user, logout_user
from psycopg2.extras import RealDictCursor
from sqlalchemy import and_
from werkzeug.exceptions import abort
from werkzeug.utils import redirect
import config
from appholder import csrf, table_service, block_blob_service, db

from constants import empty_check
from dbmodels import Donations,Admin, Voters, VoterStatus,KriyaVolunteers,KriyaMembers,KriyaMemberNomineeDetails,KriyaMemberpaymentDetails,KriyaMembersDashboard
from main import sendmailmandrill, sendbulkemails, verifyOtp, find_placeholders
from utilities.classes import Assembly_constituencies_details, encrypt_decrypt, Membership_details, Voter_details
from utilities.decorators import crossdomain, admin_login_required, ip_validation_required, required_roles, \
    constituency_login_required
from utilities.general import checkUserAuth, upload_image
from utilities.sms import sendBULKSMS, sendBULKSMS_samemessage
from utilities.others import random_membership, give_cursor_json
from celery_tasks.sms_email import sendSMS, send_bulk_email_operations, send_bulk_sms_excel, send_bulk_sms_operations, \
    send_bulk_sms_manual, send_donation_remainder_sms,sendExportResultsToEmail
from utilities.mailers import genOtp
from utilities.utility import generate_receipt
from utilities.others import missedcalls_membership_sms


blueprint_admin_dash = Blueprint('admin_dash', __name__)

UPLOAD_FOLDER = './tmp'
ALLOWED_EXTENSIONS = set(['xls', 'xlsx', 'csv'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@blueprint_admin_dash.route('/generateStateyReport', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def generateStateyReport():
    state_id = request.values.get("state")
    if state_id is None or state_id == '':
        return json.dumps({"errors": ["state_id missing"], "data": {"status": "0"}})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data = {"status_wise": {}, "gender_wise": {}, "profession_wise": {}, "qualification_wise": {},
                "assembly_wise": {}, "age_wise": {}}
        cur.execute(
            "select f.status,count(*) as old_new,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.status,a.state_id,f.post_allotment having a.state_id=%s and f.post_allotment=%s;",
            (state_id, 'T'))
        status_stats = cur.fetchall()
        data['status_wise'] = status_stats
        cur.execute(
            "select f.gender,count(*) as gender_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.gender,a.state_id,f.post_allotment having a.state_id=%s and post_allotment=%s ;",
            (state_id, 'T'))
        gender_stats = cur.fetchall()
        data['gender_wise'] = gender_stats
        cur.execute(
            "select f.profession,count(*) as profession_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.profession,a.state_id,f.post_allotment having a.state_id=%s and post_allotment=%s;",
            (state_id, 'T'))
        profession_stats = cur.fetchall()
        data['profession_wise'] = profession_stats
        cur.execute(
            "select f.qualification,count(*) as qualification_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.qualification,a.state_id,f.post_allotment having a.state_id=%s and post_allotment=%s ;",
            (state_id, 'T'))
        qualification_wise = cur.fetchall()
        data['qualification_wise'] = qualification_wise
        cur.execute(
            "select a.parliament_constituency_name as assembly,count(*) as assembly_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by a.parliament_constituency_name,a.state_id,f.post_allotment having a.state_id=%s and post_allotment=%s ;",
            (state_id, 'T'))
        assembly_wise = cur.fetchall()
        data['assembly_wise'] = assembly_wise
        cur.execute(
            "select case when f.age  between '0' and '25' then '0-25' when f.age between '26' and '35' then '26-35' when f.age between '36' and '45' then '36-45' when age between '46' and '55' then '46-55' when age between '56' and '65' then '56-65' when age >'65' then '65+' else 'not mentioned' END as age_range,count(f.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by age,a.state_id,age_range,f.post_allotment having a.state_id=%s and f.post_allotment=%s order by age_range  ;",
            (state_id, 'T'))
        age_wise = cur.fetchall()

        data['age_wise'] = age_wise
        con.commit()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1", "children": data}})


    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@blueprint_admin_dash.route('/changePasswordAdmin', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@csrf.exempt
def changePasswordAdmin():
    if request.method == 'POST':

        oldpass = request.values.get('oldpass')
        newpass = request.values.get('newpass')

        if oldpass is None or oldpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_oldpass_MISSING'], 'data': {'status': '0'}})
        if newpass is None or newpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_newpass_MISSING'], 'data': {'status': '0'}})

        try:
            admin_id = current_user.id
            print admin_id
            session_id = session['session']

            unique = 'tomatoPotatoadmin'

            old_hashed_password = hashlib.sha256(oldpass + unique).hexdigest()

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute('select user_mod_hash from admin_team where admin_id=%s', (admin_id,))
            password = cur.fetchone()[0]

            if str(password) == str(unicode(old_hashed_password, errors='ignore')):

                new_HashedPassword = hashlib.sha256(newpass + unique).hexdigest()
                cur.execute('update admin_team  set user_mod_hash=%s,update_dttm=now() where admin_id=%s',
                            (new_HashedPassword, admin_id,))

                con.commit()

                js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'Password Changed successfully'}})

                return js

            else:
                js = json.dumps({'errors': ['old password does not match'], 'data': {'status': '0'}})

                return js

        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)

            js = json.dumps({'errors': ['Exception Occured'], 'data': {'status': '0'}})
            return js
    else:
        return render_template("admin/changepassword.html")


@blueprint_admin_dash.route('/forgotAdminPassword', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def forgotAdminPassword():
    if request.method == "POST":

        email = request.form.get("email")

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()

            registered_user = Admin.query.filter_by(email=email.lower()).first()

            if registered_user is not None:
                uuid_str = str(uuid.uuid4())
                cur.execute("update admin_team set uuid=%s where lower(user_name)=%s", (uuid_str, str(email).lower()))
                con.commit()
                con.close()

                if (config.ENVIRONMENT_VARIABLE != 'local'):
                    resetlink = "<html>Hi, <br/><br/> Please click on the link below to reset your password:<a href=\'https://nivedika.janasenaparty.org/resetAdminpassword?check=" + uuid_str + "\'>Click here to Reset Password</a></html>"
                else:
                    resetlink = "<html>Hi, <br/><br/> Please click on the link below to reset your password:<a href=\'http://127.0.0.1:5000/resetAdminpassword?check=" + uuid_str + "\'>Click here to Reset Password</a></html>"

                subject = "JanaSena Party Password Reset Link"
                msgToSendEmail = resetlink
                name = "JanaSena Party"
                sendmailmandrill(name, email, resetlink, subject)

                flash("Password Reset Link was sent successfully", 'success')
                return render_template("admin/forgotpassword.html")
            else:
                flash("Email Not Found", 'error')
                return render_template("admin/forgotpassword.html")

        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)
            return json.dumps({'status': 'exception occured'})
    else:
        return render_template("admin/forgotpassword.html")


@blueprint_admin_dash.route('/resetAdminpassword', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def resetAdminpassword():
    if request.method == 'GET':
        admin_uuid = request.values.get('check')

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("select user_name from admin_team where uuid=%s", (admin_uuid,))
            existing = cur.fetchone()
            if existing is not None:
                return render_template("admin/resetpassword.html")
            else:
                return "It Seems Given Link Expired.please request New one"

        except (psycopg2.Warning, psycopg2.Error) as e:
            return json.dumps({'error': 'exception occured'})

    elif request.method == 'POST':

        admin_uuid = request.values.get('check')

        newpassword = request.form.get("newpass")

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("select user_name from admin_team where uuid=%s", (admin_uuid,))
            existing = cur.fetchone()
            if existing is not None:
                unique = 'tomatoPotatoadmin'
                hashed_password = hashlib.sha256(newpassword + unique).hexdigest()
                cur.execute("update admin_team set user_mod_hash=%s,update_dttm=now() where uuid=%s",
                            (hashed_password, admin_uuid))
                con.commit()
                flash("Changed password Successfully", "success")
                return redirect(url_for("admin_dash.admin"))
            else:
                flash("incorrect Details provided", "error")
                return redirect(url_for("admin_dash.resetAdminpassword"))
        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)
            return json.dumps({'status': 'exception occured'})
    else:
        js = json.dumps({'errors': ['UNAUTHORIZED_ACCESS', 'Please refer API Documentation'], 'data': {}})


@blueprint_admin_dash.route("/leaderdashboard", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["AD"])
def leaderDashboard():
    state = request.values.get('state')
    if state is None or state == "AP":
        return render_template('indexap.html')
    elif state == 'TS':
        return render_template("indexts.html")
    else:
        return render_template('indexap.html')


@blueprint_admin_dash.route('/adminDashboard', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def adminDashboard():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("select service_name,service_url,target,role from admin_access where role=%s order by seq_id",( current_user.urole ,))
        rows=cur.fetchall()
        session['menus']=rows
        return render_template("admin_layout.html")
    except psycopg2.Error as e:
        print str(e)
        return render_template("admin_layout.html")

    # if current_user.urole == 'AD':



    #     #return render_template("admin/memberships/rankings.html",menus=rows)

    # elif current_user.urole == 'RC':
    #     return render_template("admin/review_committee.html")
    # elif current_user.urole == 'FC':
    #     return render_template("admin/final_committee.html")
    # elif current_user.urole == 'SM':
    #    return redirect(url_for('.searchMembers2'))
    # elif current_user.urole == 'CD':
    #     return redirect(url_for('.assembly_comittee'))
    # elif current_user.urole == 'OP':
    #     return render_template("admin/operations/main_portal.html")
    # elif current_user.urole == 'MB' or current_user.urole=='SC' or current_user.urole=='DT':
    #     return render_template("admin/operations/main_portal.html")
    # elif current_user.urole == 'MD':
    #     return render_template("admin/media.html")
    # elif current_user.urole == 'AC':

    #    return render_template("donations/manual_donations.html")
    #elif current_user.urole=='MR':

    #     return redirect(url_for('.membershipRankings'))
    # elif current_user.urole == 'VM':

    #     return redirect(url_for('.female_membership_data'))
    # elif current_user.urole == 'PP':

    #     return redirect(url_for('janaswaram.janaswaram_data'))
    # elif current_user.urole=="CC":
    #     return redirect(url_for('.observers_portal'))

    # else:
    #     abort(401)


@blueprint_admin_dash.route('/createCategory', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
def createCategory():
    if request.method == 'POST':
        role_name = request.values.get('role_name')

        role = request.values.get('role')
        if role_name is None or role_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_role_name_MISSING'], 'data': {'status': '0'}})
        if role is None or role == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_role_MISSING'], 'data': {'status': '0'}})

        else:
            try:
                con = None
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("select * from admin_category where role=%s", (role,))
                existing = cur.fetchone()
                if existing is None:
                    cur.execute(
                        'insert into admin_category(role_name,role,createdttm,update_dttm) values(%s,%s,now(),now())',
                        (str(role_name), role))
                    con.commit()
                    return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted "}})
                else:
                    return json.dumps(
                        {'errors': ['Role already created . please change role type'], 'data': {'status': '0'}})


            except psycopg2.Error as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    else:

        return render_template("admin/createcategory.html")


@blueprint_admin_dash.route('/getJspmembers', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
def getJspmembers():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select name,phone,voter_id,email,address,age,gender,village_town,relationship_name,membership_id,polling_station_name,district_name,constituency_name,referral_id from janasena_membership  ")
        rows = cur.fetchall()
        con.close()
        for item in rows:
            item['phone'] = encrypt_decrypt(item['phone'])
            if item['voter_id'] is not None and item['voter_id'] != '':
                item['voter_id'] = encrypt_decrypt(item['voter_id'])

            if item['email'] is not None and item['email'] != '':
                item['email'] = encrypt_decrypt(item['email'])
        return json.dumps({"errors": [], 'data': {"status": '1', "children": rows}})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": ['Exception occured'], 'data': {"status": '0'}})


@blueprint_admin_dash.route('/getAdminTeam', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
def getAdminTeam():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from admin_team at join admin_category ac on ac.role=at.role order by at.admin_id ")
        admin_team = cur.fetchall()
        return render_template("admin/admin_team.html", admin_team=admin_team)
    except psycopg2.Error as e:
        print str(e)
        return str(e)


@blueprint_admin_dash.route('/createAdmin', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
def createAdmin():
    if request.method == 'POST':
        uname = request.values.get('uname')
        password = request.values.get('password')
        first_name = request.values.get('first_name')
        last_name = request.values.get('last_name')
        role = request.values.get('role')
        if uname is None or uname == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_uname_MISSING'], 'data': {'status': '0'}})
        if password is None or password == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_password_MISSING'], 'data': {'status': '0'}})
        if first_name is None or first_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_first_name_MISSING'], 'data': {'status': '0'}})
        if last_name is None or last_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_last_name_MISSING'], 'data': {'status': '0'}})
        if role is None or role == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_role_MISSING'], 'data': {'status': '0'}})
        else:
            try:
                con = None
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()

                unique = 'tomatoPotatoadmin'

                key = hashlib.sha256(password + unique).hexdigest()
                cur.execute("select * from admin_team where user_name=%s", (str(uname).lower(),))
                existing = cur.fetchone()
                if existing is None:
                    cur.execute(
                        'insert into admin_team(user_name,user_mod_hash,createdttm,update_dttm,first_name,last_name,role) values(%s,%s,now(),now(),%s,%s,%s)',
                        (str(uname).lower(), key, str(first_name), str(last_name), role))
                    con.commit()

                    return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
                else:
                    return json.dumps(
                        {'errors': ["someone already had access with this email"], 'data': {'status': '0'}})
            except psycopg2.Error as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    else:
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from admin_category ")
            roles = cur.fetchall()
            return render_template("admin/createadmin.html", roles=roles)
        except psycopg2.Error as e:
            print str(e)
            return str(e)


@blueprint_admin_dash.route('/logout')
@admin_login_required
def logout():
    logout_user()

    session.clear()
    print session
    return redirect(url_for('admin_dash.admin'))


@blueprint_admin_dash.route('/admin', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def admin():
    if request.method == 'POST':
        uname = request.form.get('username')
        password = request.form.get('password')
        utype = 'A'

        if checkUserAuth(str(uname).lower(), password, utype):

            session['next'] = url_for('admin_dash.adminDashboard')
            session['current'] = url_for('admin_dash.admin')
            session['logged_in'] = True

            return redirect(url_for('.adminDashboard'))
        else:

            flash('Invalid Details', 'error')
            return redirect(url_for('.admin'))
    else:

        if session.get('logged_in') is not None:
            if session.get('logged_in') == True and session.get('t') == 'A':
                return redirect(url_for('.adminDashboard'))

        return render_template("admin/login.html")


@blueprint_admin_dash.route('/getElectionTrends/<district>', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getElectionTrends(district):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select ec.state_id,ec.ac_no,ac.constituency_name,ec.party_name,ec.candidate_name,ec.community,ec.votes,ec.year from election_trends ec join assembly_constituencies ac on (ec.state_id=ac.state_id and ec.ac_no=ac.constituency_id) where lower(ac.district_name)=%s order by ec.year desc,ec.ac_no",
            (district.lower(),))
        rows = cur.fetchall()
        data = {}
        for row in rows:
            constituency = str(row['constituency_name']).rstrip()
            year = str(row['year'])
            if constituency in data.keys():

                if year in data[constituency].keys():
                    parties = []
                    for entry in data[constituency][year]:
                        parties.append(entry['party'])
                    if row['party_name'] not in parties:
                        value = {}
                        value['party'] = row['party_name']
                        value['votes'] = row['votes']
                        value['candidate_name'] = row['candidate_name']
                        value['community'] = row['community']
                        data[constituency][year].append(value)
                else:
                    d = []

                    value = {}
                    value['party'] = row['party_name']
                    value['votes'] = row['votes']
                    value['candidate_name'] = row['candidate_name']
                    value['community'] = row['community']
                    d.append(value)
                    data[constituency][year] = d

            else:
                data[constituency] = {}

                d = {}
                d[year] = []

                value = {}
                value['party'] = row['party_name']
                value['votes'] = row['votes']
                value['candidate_name'] = row['candidate_name']
                value['community'] = row['community']
                d[year].append(value)
                data[constituency] = d

        for key in data.keys():
            child = data[key]

            if '2004' not in child.keys():
                child['2004'] = []
                value = {}
                value['party'] = 'NA'
                value['votes'] = 0
                value['candidate_name'] = 'NA'
                value['community'] = 'NA'
                child['2004'].append(value)

                child['2004'].append(value)
            if '2009' not in child.keys():
                child['2009'] = []

                value = {}
                value['party'] = 'NA'
                value['votes'] = 0
                value['candidate_name'] = 'NA'
                value['community'] = 'NA'
                child['2009'].append(value)
                child['2009'].append(value)

            if '2014' not in child.keys():
                child['2014'] = []
                value = {}
                value['party'] = 'NA'
                value['votes'] = 0
                value['candidate_name'] = 'NA'
                value['community'] = 'NA'
                child['2014'].append(value)
                child['2014'].append(value)
        latest_data = {}
        for key in data.keys():
            child = data[key]
            children = []
            for childkey in child.keys():
                d = {}
                d['key'] = childkey
                newlist = sorted(child[childkey], key=operator.itemgetter('votes'), reverse=True)
                d['value'] = newlist
                children.append(d)

            latest_data[key] = children
        return json.dumps({"errors": [], "data": {"status": "1", "children": latest_data}})
    except Exception as e:
        print str(e)
        return 'false'


@blueprint_admin_dash.route('/getMandals/<assembly>', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getMandals(assembly):
    print assembly
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select gm.mdl_name,gm.seq_id from geocoordinates_mdl gm join assembly_constituencies ac on (ac.state_id=gm.state_id and ac.constituency_id=gm.constituency_id) where ac.constituency_name=%s ",
            (assembly,))
        rows = cur.fetchall()
        return json.dumps({"errors": [], "data": {"status": '1', 'children': rows}})


    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception occured'], "data": {"status": '0'}})


@blueprint_admin_dash.route('/admin/getPowerchamps', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def getPowerchamps():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data = {}

        cur.execute("select count(*) as count from janasena_membership ")
        total_memberships = cur.fetchone()

        cur.execute("select count(*) as count from janasena_membership where user_device_id!=''")
        total_app_installs = cur.fetchone()

        cur.execute("select count(*) as count from janasena_membership where referral_id!=''")
        total_memberships_through_app = cur.fetchone()

        top_ten = []

        cur.execute(
            """
                select gg.membership_id,gg.name,a.district_name,a.constituency_name,refs as count
                from
                (select m.referral_id as membership_id,count(*) as refs
                from janasena_membership m join  janasena_membership jm on jm.membership_id = m.referral_id
                group by m.referral_id
                order by count(*) desc limit 50) jj
                join janasena_membership gg on jj.membership_id=gg.membership_id
                join  assembly_constituencies a on (a.state_id=gg.state_id and a.constituency_id=gg.constituency_id) 
                order by refs desc;""",
        )
        top_ten_members_data = cur.fetchall()

        children = {}
        total = {}

        total['total_app_installs'] = total_app_installs['count']
        total['total_memberships_through_app'] = total_memberships_through_app['count']
        total['top_ten_referrals'] = top_ten_members_data
        total['total_memberships'] = total_memberships['count']
        # cur.execute("select count(*) as count from janasena_membership where is_volunteer='T' and date(create_dttm)=current_date")
        # today_total_volunteers=cur.fetchone()
        ### current_day
        cur.execute(
            "select count(*) as count from janasena_membership where user_device_id!='' and date(update_dttm)=current_date")
        today_app_installs = cur.fetchone()
        cur.execute(
            "select count(*) as count from janasena_membership where referral_id!='' and date(create_dttm)=current_date")
        today_total_memberships_through_app = cur.fetchone()
        cur.execute("select count(*) as count from janasena_membership where   date(create_dttm)=current_date")
        today_total_memberships = cur.fetchone()

        cur.execute(
            """
             select gg.membership_id,gg.name,a.district_name,a.constituency_name,refs as count
                from
                (select m.referral_id as membership_id,count(*) as refs
                from janasena_membership m join  janasena_membership jm on jm.membership_id = m.referral_id
                where date(m.create_dttm) = current_date
                group by m.referral_id
                order by count(*) desc limit 50) jj
                join janasena_membership gg on jj.membership_id=gg.membership_id
                join  assembly_constituencies a on (a.state_id=gg.state_id and a.constituency_id=gg.constituency_id) 
                
                order by refs desc;
            """)
        today_top_ten_referrals = cur.fetchall()

        today = {}

        today['total_app_installs'] = today_app_installs['count']
        today['total_memberships_through_app'] = today_total_memberships_through_app['count']
        today['top_ten_referrals'] = today_top_ten_referrals
        today['today_total_memberships'] = today_total_memberships['count']
        # today['today_total_missedcalls']=today_total_missedcalls['count']
        # date wise
        cur.execute(
            " select cast(date(create_dttm) as text ) as date,count(*) from janasena_membership group by date(create_dttm)  having  date(create_dttm)> current_date - interval '15' day  order by date(create_dttm) ")
        date_wise_memberships = cur.fetchall()
        cur.execute(
            " select cast(date(create_dttm) as text ) as date,count(*) from janasena_membership where referral_id!='' group by date(create_dttm)  having  date(create_dttm)>current_date - interval '15' day  order by date(create_dttm) ")
        date_wise_referrals = cur.fetchall()
        cur.execute(
            "select cast(date(update_dttm) as text ) as date,count(*) from janasena_membership where user_gcm_id!='' group by date(update_dttm)  having  date(update_dttm)>current_date - interval '15' day  order by date(update_dttm) ")
        date_wise_downloads = cur.fetchall()
        datewise_data = []
        for day in date_wise_memberships:
            d = {}
            d['date'] = day['date']
            d['memberships'] = day['count']
            datewise_data.append(d)
        for i in range(len(datewise_data)):
            datewise_data[i]['referrals'] = date_wise_referrals[i]['count']
            datewise_data[i]['downloads'] = date_wise_downloads[i]['count']

        children['total'] = total
        children['today'] = today
        children['datewise_data'] = datewise_data
        return json.dumps({"errors": [], "data": {"status": "1", "children": children}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": [], "data": {"status": "0", "children": []}})


@blueprint_admin_dash.route('/admin/getPowerchampsByDistrict', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["VL", "AD"])
def getPowerchampsByDistrict():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data = {}

        cur.execute("select count(*) as count from janasena_membership ")
        total_memberships = cur.fetchone()

        cur.execute("select count(*) as count from janasena_membership where user_device_id!=''")
        total_app_installs = cur.fetchone()

        cur.execute("select count(*) as count from janasena_membership where referral_id!=''")
        total_memberships_through_app = cur.fetchone()

        top_ten = []

        cur.execute(
            "select referral_id,count(*) from janasena_membership m group by m.referral_id,m.district_id  having referral_id!='' and district_id=2 order by count(*) desc limit 50")
        top_ten_referrals = cur.fetchall()

        for referral in top_ten_referrals:
            top_ten.append(referral["referral_id"])
        cur.execute(
            "select m.membership_id,m.name,m.phone,m.email,a.district_name,a.constituency_name from janasena_membership m join assembly_constituencies a on (a.state_id=m.state_id and a.constituency_id=m.constituency_id) where m.membership_id in %s",
            (tuple(top_ten),))
        top_ten_members_data = cur.fetchall()

        for member in top_ten_members_data:
            for referral in top_ten_referrals:
                if member['membership_id'] == referral['referral_id']:
                    member['phone'] = encrypt_decrypt(member['phone'], 'D')
                    member['email'] = encrypt_decrypt(member['email'], 'D')

                    referral.update(member)
        children = {}
        total = {}

        total['total_app_installs'] = total_app_installs['count']
        total['total_memberships_through_app'] = total_memberships_through_app['count']
        total['top_ten_referrals'] = top_ten_referrals
        total['total_memberships'] = total_memberships['count']
        # cur.execute("select count(*) as count from janasena_membership where is_volunteer='T' and date(create_dttm)=current_date")
        # today_total_volunteers=cur.fetchone()
        ### current_day
        cur.execute(
            "select count(*) as count from janasena_membership where user_device_id!='' and date(update_dttm)=current_date")
        today_app_installs = cur.fetchone()
        cur.execute(
            "select count(*) as count from janasena_membership where referral_id!='' and date(create_dttm)=current_date")
        today_total_memberships_through_app = cur.fetchone()
        cur.execute("select count(*) as count from janasena_membership where   date(create_dttm)=current_date")
        today_total_memberships = cur.fetchone()

        cur.execute(
            "select referral_id,count(*) from janasena_membership m group by m.referral_id,date(m.create_dttm)  having referral_id!='' and date(create_dttm)=current_date order by count(*) desc limit 50")
        today_top_ten_referrals = cur.fetchall()

        today_top_ten = []
        for referral in today_top_ten_referrals:
            today_top_ten.append(referral["referral_id"])
        if len(today_top_ten):
            cur.execute(
                "select m.membership_id,m.name,m.phone,m.email,a.district_name,a.constituency_name from janasena_membership m join assembly_constituencies a on (a.state_id=m.state_id and a.constituency_id=m.constituency_id) where m.membership_id in %s",
                (tuple(today_top_ten),))
            today_top_ten_members_data = cur.fetchall()

            for member in today_top_ten_members_data:
                for referral in today_top_ten_referrals:
                    if member['membership_id'] == referral['referral_id']:
                        member['phone'] = encrypt_decrypt(member['phone'], 'D')
                        member['email'] = encrypt_decrypt(member['email'], 'D')

                        referral.update(member)

        today = {}

        today['total_app_installs'] = today_app_installs['count']
        today['total_memberships_through_app'] = today_total_memberships_through_app['count']
        today['top_ten_referrals'] = today_top_ten_referrals
        today['today_total_memberships'] = today_total_memberships['count']
        # today['today_total_missedcalls']=today_total_missedcalls['count']

        children['total'] = total
        children['today'] = today
        return json.dumps({"errors": [], "data": {"status": "1", "children": children}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@blueprint_admin_dash.route("/admin/members", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD"])
def srikakulam():
    return render_template("admin/memberships/srikakulam.html")


@blueprint_admin_dash.route("/membership_portal", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def membership_portal():
    return render_template("admin/memberships/memberships_portal.html")


@blueprint_admin_dash.route("/admin/membership_data", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP"])
def membership_data():
    return render_template("admin/memberships/portal_backup.html")


@blueprint_admin_dash.route("/admin/female_membership_data", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
# @ip_validation_required
@admin_login_required
@required_roles(["AD", "VM"])
def female_membership_data():
    return render_template("admin/memberships/female_members.html")


@blueprint_admin_dash.route("/admin/offline_membership_data", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD"])
def offline_membership_data():
    return render_template("admin/memberships/offline_memberships.html")


@blueprint_admin_dash.route("/admin/offlinemembersdata", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def admin_offlinemembersdata():
    try:
        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None

        if data_type == 'assembly':
            state = request.values.get('state')
        if data_type is None:
            return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
        if value is None:
            return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
        membership_instance = Membership_details()

        data = membership_instance.get_offline_members_profile_details(data_type=data_type, value=value,
                                                                       state=state
                                                                       )

        return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

    except (Exception, KeyError) as e:

        return json.dumps({"errors": [str(e)], "data": {"status": 0}})


@blueprint_admin_dash.route("/admin/setTarget", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def admin_settarget():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        membership_id = request.values.get("membership_id")
        target = request.values.get("target")
        print membership_id, target
        if membership_id is None or membership_id == '':
            return json.dumps({"errors": ['parameter membership_id missing'], "data": {"status": 0}})
        if target is None or target == '':
            return json.dumps({"errors": ['parameter target missing'], "data": {"status": 0}})
        cur.execute("update membership_targets set target=%s where membership_id=%s", (target, membership_id))
        con.commit()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1"}})


    except (Exception, KeyError) as e:

        return json.dumps({"errors": [str(e)], "data": {"status": 0}})


@blueprint_admin_dash.route("/admin/female_membersdata", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
# @ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", 'VM'])
def female_membersdata():
    try:
        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None

        if data_type is None:
            return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
        if value is None:
            return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
        if data_type == 'assembly':
            state = request.values.get('state')
        membership_instance = Membership_details()

        data = membership_instance.get_members_profile_details(data_type=data_type, value=value,

                                                               state=state, gender='female'
                                                               )

        for member in data:
            if member['voter_id'] is not None and member['voter_id'] != '':
                member['voter_id'] = encrypt_decrypt(member['voter_id'], 'D')
            # member['phone'] = encrypt_decrypt(member['phone'], 'D')
            if member['email'] is not None and member['email'] != '':
                member['email'] = encrypt_decrypt(member['email'], 'D')

        return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

    except (Exception, KeyError) as e:

        return json.dumps({"errors": [str(e)], "data": {"status": 0}})


@blueprint_admin_dash.route("/admin/membersdata", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def admin_membersdata():
    try:
        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None
        achievement = request.values.get('achievement')
        membership_through = request.values.get('membership_through')
        page_no = request.values.get('page_no')
        if page_no is None:
            page_no = 1
        if data_type == 'assembly':
            state = request.values.get('state')
        # if data_type is None:
        #     return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
        # if value is None:
        #     return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
        membership_instance = Membership_details()

        data = membership_instance.get_members_profile_details(data_type=data_type, value=value,
                                                               achievement=achievement,
                                                               membership_through=membership_through, state=state,
                                                               page_no=page_no, )

        for member in data:
            if member['voter_id'] is not None and member['voter_id'] != '':
                member['voter_id'] = encrypt_decrypt(member['voter_id'], 'D')
            # member['phone'] = encrypt_decrypt(member['phone'], 'D')
            if member['email'] is not None and member['email'] != '':
                member['email'] = encrypt_decrypt(member['email'], 'D')

        return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

    except (Exception, KeyError) as e:

        return json.dumps({"errors": [str(e)], "data": {"status": 0}})


@blueprint_admin_dash.route("/advancedmembershipreports", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
@csrf.exempt
def advancedmembershipreports():
    try:
        region_level = request.values.get('level')
        value = request.values.get('value')
        print region_level, value
        if region_level is None or region_level == '':
            return json.dumps({"errors": ["parameter region_level Missing"], "data": {"status": "0"}})
        if value is None or value == '':
            return json.dumps({"errors": ["parameter value Missing"], "data": {"status": "0"}})

        if region_level == 'state':
            status, data = get_state_level_report(value)


        elif region_level == 'district':
            status,data = get_district_level_report(value)
        elif region_level == 'parliament':
            status, data = get_parliament_level_report(value)
        elif region_level == 'assembly':
            state = request.values.get('state')
            if state is None or state == '':
                return json.dumps({"errors": ["parameter state Missing"], "data": {"status": "0"}})
            status, data = get_assembly_level_report(value, state)
            status, data = get_district_level_report(value)
        else:
            return json.dumps({"errors": ["invalid filter requested"], "data": {"status": "0"}})
        if status:

            return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
        else:
            return json.dumps({"errors": [data], "data": {"status": "0"}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["exception occured"], "data": {"status": "0"}})


def get_state_level_report(value):
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select case when m.age  between '0' and '17' then '0-17' when m.age between '18' and '35' then '18-35' when m.age between '36' and '50' then '36-50' when m.age between '51' and '65' then '51-65'  when m.age >'65' then '65+' else 'not mentioned' END as age_range,count(m.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m group by age_range,m.state having state=%s",
            (value,))
        agewise_stats = cur.fetchall()
        cur.execute(
            "select trim(lower(m.gender)) as gender,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m group by trim(lower(m.gender)),state having state=%s ",
            (value,))
        genderwise_stats = cur.fetchall()
        cur.execute(
            "select case when m.membership_through='ME' then 'VoterId Not Found' when m.membership_through='S' then 'NO VoterId' else 'VoterID' end as MemberType,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m group by MemberType,state having state=%s",
            (value,))
        membershiptype_wise_stats = cur.fetchall()
        cur.execute(
            "select a.district_name,count(*) as count ,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id= case when m.state='AP' then 1 else 2 end group by a.district_name,m.state having m.state=%s",
            (value,))
        districtwise_stats = cur.fetchall()
        cur.execute(
            "select a.parliament_constituency_name,count(*) as count ,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id= case when m.state='AP' then 1 else 2 end  group by a.parliament_constituency_name,m.state having m.state=%s",
            (value,))
        parliamentwise_stats = cur.fetchall()
        data = {'age_wise': agewise_stats, 'gender_wise': genderwise_stats, 'membershiptype': membershiptype_wise_stats,
                'district_wise': districtwise_stats, 'parliament_wise': parliamentwise_stats}
        return (True, data)
    except (psycopg2.Error, Exception) as e:
        print str(e)
        return (False, str(e))


def get_district_level_report(value):
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select case when m.age  between '0' and '17' then '0-17' when m.age between '18' and '35' then '18-35' when m.age between '36' and '50' then '36-50' when m.age between '51' and '65' then '51-65'  when m.age >'65' then '65+' else 'not mentioned' END as age_range,count(m.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end  group by age_range,a.district_name having a.district_name=%s",
            (value,))
        agewise_stats = cur.fetchall()
        cur.execute(
            "select trim(lower(m.gender)) as gender,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end  group by trim(lower(m.gender)),a.district_name having a.district_name=%s ",
            (value,))
        genderwise_stats = cur.fetchall()
        cur.execute(
            "select case when m.membership_through='ME' then 'VoterId Not Found' when m.membership_through='S' then 'NO VoterId' else 'VoterID' end as MemberType,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end group by MemberType,a.district_name having a.district_name=%s",
            (value,))
        membershiptype_wise_stats = cur.fetchall()
        cur.execute(
            "select a.constituency_name,count(*) as count ,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id= case when m.state='AP' then 1 else 2 end group by a.constituency_name,a.district_name having a.district_name=%s",
            (value,))
        assembly_stats = cur.fetchall()

        data = {'age_wise': agewise_stats, 'gender_wise': genderwise_stats, 'membershiptype': membershiptype_wise_stats,
                'assembly_wise': assembly_stats}
        return (True, data)
    except (psycopg2.Error, Exception) as e:
        print str(e)
        return (False, str(e))


def get_parliament_level_report(value):
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select case when m.age  between '0' and '25' then '0-25' when m.age between '26' and '35' then '26-35' when m.age between '36' and '45' then '36-45' when m.age between '46' and '55' then '46-55' when m.age between '56' and '65' then '56-65' when m.age >'65' then '65+' else 'not mentioned' END as age_range,count(m.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end  group by age_range,a.parliament_constituency_name having a.parliament_constituency_name=%s",
            (value,))
        agewise_stats = cur.fetchall()
        cur.execute(
            "select trim(lower(m.gender)) as gender,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end  group by trim(lower(m.gender)),a.parliament_constituency_name having a.parliament_constituency_name=%s ",
            (value,))
        genderwise_stats = cur.fetchall()
        cur.execute(
            "select case when m.membership_through='ME' then 'VoterId Not Found' when m.membership_through='S' then 'NO VoterId' else 'VoterID' end as MemberType,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id =case when m.state='AP' then 1 else 2 end group by MemberType,a.parliament_constituency_name having a.parliament_constituency_name=%s",
            (value,))
        membershiptype_wise_stats = cur.fetchall()
        cur.execute(
            "select a.constituency_name,count(*) as count ,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m join assembly_constituencies a on a.constituency_id=m.constituency_id and a.state_id= case when m.state='AP' then 1 else 2 end group by a.constituency_name,a.parliament_constituency_name having a.parliament_constituency_name=%s",
            (value,))
        assembly_stats = cur.fetchall()

        data = {'age_wise': agewise_stats, 'gender_wise': genderwise_stats, 'membershiptype': membershiptype_wise_stats,
                'assembly_wise': assembly_stats}
        return (True, data)
    except (psycopg2.Error, Exception) as e:
        print str(e)
        return (False, str(e))


def get_assembly_level_report(value, state):
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select case when m.age  between '0' and '25' then '0-25' when m.age between '26' and '35' then '26-35' when m.age between '36' and '45' then '36-45' when m.age between '46' and '55' then '46-55' when m.age between '56' and '65' then '56-65' when m.age >'65' then '65+' else 'not mentioned' END as age_range,count(m.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m  group by age_range,constituency_id,state having constituency_id=%s and state=%s",
            (value, state))
        agewise_stats = cur.fetchall()
        cur.execute(
            "select trim(lower(m.gender)) as gender,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m group by trim(lower(m.gender)),constituency_id,state having constituency_id=%s and state=%s",
            (value, state))
        genderwise_stats = cur.fetchall()
        cur.execute(
            "select case when m.membership_through='ME' then 'VoterId Not Found' when m.membership_through='S' then 'NO VoterId' else 'VoterID' end as MemberType,count(*) as count,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m  group by MemberType,constituency_id,state having constituency_id=%s and state=%s",
            (value, state))
        membershiptype_wise_stats = cur.fetchall()
        cur.execute(
            "select m.polling_station_id as booth ,count(*) as count ,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from janasena_membership m group by m.polling_station_id,constituency_id,state having constituency_id=%s and state=%s",
            (value, state))
        booth_stats = cur.fetchall()

        data = {'age_wise': agewise_stats, 'gender_wise': genderwise_stats, 'membershiptype': membershiptype_wise_stats,
                'booth_wise': booth_stats}
        return (True, data)
    except (psycopg2.Error, Exception) as e:
        print str(e)
        return (False, str(e))


@blueprint_admin_dash.route("/membershipreports", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @ip_validation_required
@admin_login_required
@required_roles(["AD", "OP","MB"])
@csrf.exempt
def membershipReports():
    if request.method == 'GET':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            cur.execute("""SELECT sum(count) as total, 
            sum(count) filter (where flag  in ('S','W','ME','U','SM')) as website,
            sum(count) filter (where flag in ('M','WB')) as app,
            sum(count) filter (where flag in ('OFV','OFNF','OFW')) as offline,
            sum(count) filter (where flag ='CC') as call_center,
            sum(count) filter (where flag in ('MC','MU')) as contacts_db 
            from membership_through_flags""")
            membership_data = cur.fetchone()

            cur.execute("""SELECT count as missedcalls from missedcall_counts where flag ='M' """)
            missedcalls = cur.fetchone()['missedcalls']
            cur.execute("select flag,count,description from missedcall_status_counts")
            rows = cur.fetchall()

            d = {}
            for row in rows:
                d[row['flag']]=row['count']
            print d
            return render_template("admin/membership_reports.html",missedcalls_count=missedcalls,missedcalls_data=d,membership_data=membership_data)

        except (psycopg2.Error, Exception) as e:
            print str(e)


    else:
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                """update janasena_missedcalls set supporter_id='JSP'||lpad(cast(jsp_supporter_seq_id as text),8,'0') where supporter_id is null""")
            con.commit()
            start_date = request.values.get('start_date')
            end_date = request.values.get('end_date')

            if start_date is None or start_date == '':
                return json.dumps({"errors": ["parameter start_date Missing"], "data": {"status": "0"}})
            if end_date is None or end_date == '':
                return json.dumps({"errors": ["parameter end_date Missing"], "data": {"status": "0"}})

            query = """select distinct on(time_range)to_char(customdate.date,'yyyy-mm-dd') as time_range,
            coalesce(missed_calls.value,0) as missed_calls,
            coalesce(missed_calls_to_be_converted.value,0) as missed_calls_to_be_converted,
            
            coalesce(memberships.value,0) as memberships,
            coalesce(web_memberships.value,0) as web_memberships,
            coalesce(app_memberships.value,0) as app_memberships,
            coalesce(call_center_memberships.value,0) as call_center_memberships,
            coalesce(offline_memberships.value,0) as offline_memberships,
            coalesce(contacts_mapping_memberships.value,0) as contacts_mapping_memberships  
from generate_series(%s::date,%s::date,'1 day'::interval) as customdate(date) 
left outer join (select count(*)as value,date(create_dttm) as date from janasena_missedcalls 
where date(create_dttm) between %s and %s and member_through in ('M','JT') GROUP BY date ) as missed_calls 
on missed_calls.date=customdate.date
left outer join (select count(*)as value,date(jm.create_dttm) as date from janasena_missedcalls jm left join janasena_membership m on jm.supporter_id=m.membership_id 
where date(jm.create_dttm) between %s and %s and jm.member_through='M' and m.membership_id is null  GROUP BY date ) as missed_calls_to_be_converted 
on missed_calls_to_be_converted.date=customdate.date
LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s  GROUP BY date ) as memberships on memberships.date=customdate.date

LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s and membership_through in ('W','ME','S','U') GROUP BY date ) as web_memberships on web_memberships.date=customdate.date

LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s and membership_through in ('M') GROUP BY date ) as app_memberships on app_memberships.date=customdate.date
LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s and membership_through in ('CC') GROUP BY date ) as call_center_memberships on call_center_memberships.date=customdate.date
LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s and membership_through in ('OFV','OFNF','OFW') GROUP BY date ) as offline_memberships on offline_memberships.date=customdate.date
LEFT OUTER JOIN(SELECT COUNT(*) AS value,date(create_dttm) as date from janasena_membership where date(create_dttm) 
between %s and %s and membership_through in ('MC') GROUP BY date ) as contacts_mapping_memberships on contacts_mapping_memberships.date=customdate.date
"""

            cur.execute(query, (
            start_date, end_date, start_date, end_date, start_date, end_date, start_date, end_date, start_date,
            end_date, start_date, end_date, start_date, end_date, start_date, end_date, start_date, end_date))

            rows = cur.fetchall()

            return json.dumps({"errors": [], "data": {"status": "1", "children": rows}})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            return json.dumps({"errors": ["exception occured"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/volunteersReport', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def volunteersReport():
    return render_template("admin/memberships/volunteers_portal.html")


@blueprint_admin_dash.route('/getTopVolunteers', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "MB"])
def getTopVolunteers():
    try:
        region_type = request.values.get("type")
        region_value = request.values.get("value")
        volume = request.values.get("vol")
        if region_type is None or region_type == '':
            return json.dumps({"errors": ["Parameter type missing"], "data": {"status": "0"}})

        if region_value is None or region_value == '':
            return json.dumps({"errors": ["Parameter value missing"], "data": {"status": "0"}})

        if volume is None or volume == '':
            return json.dumps({"errors": ["Parameter volume missing"], "data": {"status": "0"}})

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        base_query = """select distinct(m.membership_id),ac.district_name,ac.constituency_name,psd.mandal as mdl_name,m.name,m.age,m.gender,m.polling_station_id,m.email,m.phone,mt.achievement,m.membership_id   
from janasena_membership m join assembly_constituencies ac
on m.state_id=ac.state_id and m.constituency_id=ac.constituency_id join membership_targets mt on m.membership_id=mt.membership_id left join polling_station_details psd on m.state_id=psd.state_id and m.constituency_id=psd.constituency_id and m.polling_station_id=psd.part_no 
where {{where_clause}} and mt.achievement > 0
order by mt.achievement desc limit {{limit}}; """
        where_clause = ""
        if region_type == 'state':

            if region_value == "Andhra Pradesh":
                region_value = 1
            else:
                region_value = 2
            where_clause = "ac.state_id='" + str(region_value) + "'"
        elif region_type == 'district':
            where_clause = "ac.district_name='" + str(region_value) + "'"
        elif region_type == 'parliament':
            where_clause = "ac.parliament_constituency_name='" + str(region_value) + "'"
        elif region_type == 'assembly':
            state_id = request.values.get('state_id')
            print
            if state_id == "Andhra Pradesh":
                state_id = 1
            else:
                state_id = 2
            where_clause = "ac.state_id='" + str(state_id) + "' and ac.constituency_id='" + str(region_value) + "' "
        else:
            return json.dumps({"errors": ["Invalid parameters sent"], "data": {"status": "0"}})
        base_query = base_query.replace("{{where_clause}}", where_clause)
        base_query = base_query.replace("{{limit}}", volume)
        print base_query
        cur.execute(base_query)
        rows = cur.fetchall()

        for row in rows:
            row['phone'] = encrypt_decrypt(str(row['phone']), 'D')
            if row['email'] is not None and row['email'] != '':
                row['email'] = encrypt_decrypt(str(row['email']), 'D')
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ["Exception Occured"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/resourcepersonsFilters', methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@csrf.exempt
@required_roles(["OP", "AD", "MB",'PP'])
def resourcepersonsFilters():
    try:
        data = request.json
        fkeys = data.keys()



        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        if current_user.urole=='PP':
            cur.execute( "select distinct on (mobile) name,gender,age,mobile,email,category,district,assembly,post from operationsdata where category='visitors' " )
            rows=cur.fetchall()
            return json.dumps({"errors": [], "data": {"status": "1", 'children': rows}})


        all_data = []

        if len(fkeys):
            query = "select distinct on (mobile) name,gender,age,mobile,email,category,district,assembly,post from operationsdata where "
            if 'categories' in fkeys:
                categories = []
                for item in data['categories']:
                    categories.append(str(item))
                if len(categories) == 1:
                    query = query + " category ='" + str(categories[0]) + "'"
                else:
                    query = query + " category in " + str(tuple(categories))

            if 'level' in fkeys:
                if 'categories' in fkeys:
                    query = query + " and "

                if data['level'] == 'assembly':

                    query = query + " state='" + data['state'] + "' and  lower(assembly) ='" + str(
                        data["value"]).lower() + "'"
                else:
                    query = query + " " + "lower(" + str(data['level']) + ") ='" + str(data['value']).lower() + "'"
            print query
            if 'genders' in fkeys:
                if len(data['genders']) == 2:
                    pass
                else:

                    if 'categories' in fkeys or 'level' in fkeys:
                        query = query + " and "

                    query = query + " lower(gender)='" + str(data['genders'][0]).lower() + "'"
            if 'ages' in fkeys:
                if 'categories' in fkeys or 'level' in fkeys or 'genders' in fkeys:
                    query = query + " and ("
                else:
                    query = query + "  ("
                age_query = ''
                for group in data['ages']:

                    if group == '>65':
                        if age_query != '':
                            age_query = age_query + " or age > '65' "
                        else:
                            age_query = age_query + " age > '65' "

                    else:
                        ranges = group.split("-")
                        if age_query != '':

                            age_query = age_query + " or age between '" + ranges[0] + "' and '" + ranges[1] + "'"
                        else:
                            age_query = age_query + " age between '" + ranges[0] + "' and '" + ranges[1] + "'"
                query = query + " " + age_query + ")"
            if 'posts' in fkeys:
                if 'categories' in fkeys or 'level' in fkeys or 'genders' in fkeys or 'posts' in fkeys:
                    query = query + " and "
                if len(data['posts']) == 1:
                    query = query + " post ='" + str(data['posts'][0]) + "'"


                else:
                    posts = []
                    for item in data['posts']:
                        posts.append(str(item))
                    query = query + " post in " + str(tuple(posts))

            # query=query+ " limit 1000"
            print query
            cur.execute(query)
            rows = cur.fetchall()
            all_data = rows

        return json.dumps({"errors": [], "data": {"status": "1", 'children': all_data}})

    except (Exception, psycopg2.Error) as e:
        print str(e)
        return json.dumps({"errors": ["Exception Occured"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/OperationsPortal', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
@required_roles(["OP", "AD", "MB","PP"])
def OperationsPortal():
    return render_template("admin/operations/main_portal.html")


@blueprint_admin_dash.route('/operations/resourcepersons', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
def resourcepersons():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select district,assembly,name,email,mobile,applying_for,age,gender from resourcepersons where attendance_status='T' order by district limit 1000")
        rows = cur.fetchall()

        return render_template("admin/operations/resourcepersons.html", data=rows)

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@blueprint_admin_dash.route('/operations/facilitators', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@ip_validation_required
@admin_login_required
def facilitators():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select district,assembly,name,email,mobile,age,gender from facilitators  order by district limit 1000")
        rows = cur.fetchall()

        return render_template("admin/operations/facilitators.html", data=rows)

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@blueprint_admin_dash.route('/BulkSMS', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD"])
def BulkSMS():
    if request.method == 'GET':
        return render_template('admin/operations/bulksms.html')


@blueprint_admin_dash.route('/BulkEmails', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD"])
def BulkEmails():
    if request.method == 'GET':
        return render_template('admin/operations/bulkemails.html')


@blueprint_admin_dash.route('/processBulkEmailTest', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD", "MD"])
def processBulkEmailTest():
    if request.method == 'POST':

        body = request.values.get('body')
        subject = request.values.get('subject')

        if body is None or body == '':
            return json.dumps({"errors": ["parameter body missing"], "data": {"status": "0"}})
        if subject is None or subject == '':
            return json.dumps({"errors": ["parameter subject missing"], "data": {"status": "0"}})
        file_path = "https://docs.google.com/spreadsheets/d/1L4654KJDOcaNSQWyCMxJWrBjtrOqfx_dXU6E4PeggA8/edit?usp=sharing"

        placeholders = find_placeholders(body)
        subject_placeholders = find_placeholders(subject)
        placeholders.extend(subject_placeholders)

        import gspread
        from oauth2client.service_account import ServiceAccountCredentials
        scope = ['https://spreadsheets.google.com/feeds']
        creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
        client = gspread.authorize(creds)

        sheet = client.open("internal_team").sheet1

        list_of_hashes = sheet.get_all_records()
        keys = [v for v in list_of_hashes[0].keys()]

        if 'email' not in keys:
            return json.dumps({"errors": ["email Missing in Excel File"], "data": {"status": "0"}})

        if set(placeholders).issubset(set(keys)):

            emails_object = {}

            for row in list_of_hashes:

                to = str(row['email'])
                if to is not None and to != '':
                    emails_object[to] = {}

                    for placeholder in placeholders:
                        emails_object[to][placeholder] = row[placeholder]
            temp_message = body
            name = "JanaSena Party"
            emails = emails_object.keys()

            for placeholder in placeholders:
                temp_message = temp_message.replace("{{" + placeholder + "}}", r"%recipient." + str(placeholder) + "%")
                subject = subject.replace("{{" + placeholder + "}}", r"%recipient." + str(placeholder) + "%")
            email_message = open("./notifications/email_template.html").read()
            email_message = email_message.replace("{{email_body}}", temp_message)

            if len(emails):
                sendbulkemails(name, emails, email_message, subject, emails_object)

            return json.dumps({"errors": [], "data": {"status": "1"}})
        else:

            return json.dumps({"errors": ["All Place holders should in Excel Sheet Columns "], "data": {"status": "0"}})


@blueprint_admin_dash.route('/processOperationalEmails', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD"])
def processOperationalEmails():
    otp = request.values.get("otp")

    if not verifyOtp(config.admin_number, otp, "BE"):
        return json.dumps({"errors": ["invalid OTP"], "data": {"status": "0"}})

    body = request.values.get('body')
    subject = request.values.get('subject')

    if body is None or body == '':
        return json.dumps({"errors": ["parameter body missing"], "data": {"status": "0"}})
    contacts = request.values.get('contacts')
    print type(contacts)
    placeholders = find_placeholders(body)
    subject_placeholders = find_placeholders(subject)
    placeholders.extend(subject_placeholders)
    jcontacts = json.loads(contacts)
    if isinstance(jcontacts, list):
        if len(jcontacts):
            keys = jcontacts[0].keys()
            if 'email' not in keys:
                return json.dumps({"errors": ["email Missing in Excel File"], "data": {"status": "0"}})
            if set(placeholders).issubset(set(keys)):
                if config.ENVIRONMENT_VARIABLE == 'local':
                    send_bulk_email_operations(jcontacts, body, subject, placeholders, keys)
                else:
                    send_bulk_email_operations.delay(jcontacts, body, subject, placeholders, keys)
                return json.dumps({"errors": [], "data": {"status": "1"}})

            else:

                return json.dumps({"errors": ["unknown Place holders found "], "data": {"status": "0"}})

        return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})

    return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/processMembershipsEmails', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["AD"])
def processMembershipsEmails():
    otp = request.values.get("otp")

    if not verifyOtp(config.admin_number, otp, "BE"):
        return json.dumps({"errors": ["invalid OTP"], "data": {"status": "0"}})

    body = request.values.get('body')
    subject = request.values.get('subject')

    if body is None or body == '':
        return json.dumps({"errors": ["parameter body missing"], "data": {"status": "0"}})
    contacts = request.values.get('contacts')

    placeholders = find_placeholders(body)
    subject_placeholders = find_placeholders(subject)
    placeholders.extend(subject_placeholders)
    jcontacts = json.loads(contacts)
    if isinstance(jcontacts, list):
        if len(jcontacts):
            keys = jcontacts[0].keys()
            if 'email' not in keys:
                return json.dumps({"errors": ["email Missing in Excel File"], "data": {"status": "0"}})
            if set(placeholders).issubset(set(keys)):
                if config.ENVIRONMENT_VARIABLE == 'local':
                    send_bulk_email_operations(jcontacts, body, subject, placeholders, keys)
                else:
                    send_bulk_email_operations.delay(jcontacts, body, subject, placeholders, keys)
                return json.dumps({"errors": [], "data": {"status": "1"}})

            else:

                return json.dumps({"errors": ["unknown Place holders found "], "data": {"status": "0"}})

        return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})

    return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/processBulkSMSTest', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD", "MD", "MB"])
def processBulkSMSTest():
    if request.method == 'POST':

        message = request.values.get('message')
        message = message.encode('utf-8')
        category = request.values.get("category")
        temp_message = message
        if message is None or message == '':
            return json.dumps({"errors": ["parameter message missing"], "data": {"status": "0"}})

        file_path = "https://docs.google.com/spreadsheets/d/1L4654KJDOcaNSQWyCMxJWrBjtrOqfx_dXU6E4PeggA8/edit?usp=sharing"

        placeholders = find_placeholders(message)

        import gspread
        from oauth2client.service_account import ServiceAccountCredentials
        scope = ['https://spreadsheets.google.com/feeds',
                 'https://www.googleapis.com/auth/drive']
        import os
        cwd = os.getcwd()
        print cwd
        creds = ServiceAccountCredentials.from_json_keyfile_name(cwd + '/client_secret.json', scope)
        client = gspread.authorize(creds)

        sheet = client.open("internal_team").sheet1

        list_of_hashes = sheet.get_all_records()

        keys = [v for v in list_of_hashes[0].keys()]

        if 'mobile_number' not in keys:
            return json.dumps({"errors": ["mobile_number Missing in Excel File"], "data": {"status": "0"}})
        if category == 'Media':
            category = 'M'
        else:
            category = 'I'
        if len(placeholders):
            if set(placeholders).issubset(set(keys)):
                mno_message = ''

                for row in list_of_hashes:
                    print row
                    print "personalised message"
                    if row['type'] == category:
                        temp_message = message
                        numberToSend = str(int(row['mobile_number']))

                        if len(str(numberToSend)) == 10:
                            numberToSend = "91" + str(numberToSend)
                        elif len(str(numberToSend)) == 12:
                            pass
                        else:
                            numberToSend = ''

                        for placeholder in placeholders:
                            temp_message = temp_message.replace("{{" + placeholder + "}}", str(row[placeholder]))

                        if numberToSend != '':
                            mno_message += numberToSend + '^' + temp_message + '~'

                if mno_message != '':
                    sendBULKSMS(mno_message)

                return json.dumps({"errors": [], "data": {"status": "1"}})
            else:

                return json.dumps(
                    {"errors": ["All Place holders should in Excel Sheet Columns "], "data": {"status": "0"}})
        else:
            contacts = []
            for row in list_of_hashes:
                if row['type'] == category:
                    print row
                    print "normal message"

                    numberToSend = str(int(row['mobile_number']))

                    if len(str(numberToSend)) == 10:
                        numberToSend = "91" + str(numberToSend)
                    contacts.append(numberToSend)
            if len(contacts):
                sendBULKSMS_samemessage(message, contacts, language='unicode')
            return json.dumps({"errors": [], "data": {"status": "1"}})


@blueprint_admin_dash.route('/SMSOTPVerification', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD",  "MB"])
def SMSOTPVerification():
    otp_type = request.values.get("otp_type")
    try:
        if genOtp(config.admin_number, otp_type):

            return json.dumps({"errors": [], "data": {"status": "1"}})
        else:
            return json.dumps({"errors": ["something Wrong"], "data": {"status": "0"}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["something Wrong"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/admin/memberscount', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
@required_roles(["AD", "OP",  "MB"])
def memberscount():
    try:
        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None
        gender = request.values.get('gender')
        is_volunteer=None
        membership_through=None
        if data_type == 'assembly':
            state = request.values.get('state')
        # if data_type is None:
        #     return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
        # if value is None:
        #     return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
        membership_instance = Membership_details()

        count = membership_instance.get_members_count(data_type=data_type, value=value,
                                                      is_volunteer=is_volunteer,
                                                      membership_through=membership_through, state=state,gender=gender)
        return json.dumps({"errors": [], "data": {"status": "1", "count": count}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["something wrong"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/processMembershipsSMS', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["AD",  "MB"])
def processMembershipsSMS():
    otp = request.values.get("otp")

    if not verifyOtp(config.admin_number, otp, "BS"):
        return json.dumps({"errors": ["invalid OTP"], "data": {"status": "0"}})

    message = request.values.get('message')

    if message is None or message == '':
        return json.dumps({"errors": ["parameter message missing"], "data": {"status": "0"}})
    query = json.loads(request.values.get('contacts'))

    placeholders = find_placeholders(message)
    campaign_keys = ['name', 'membership_id']
    if 'type' in query.keys():
        data_type = query['type']
    else:
        data_type = None
    if 'value' in query.keys():
        value = query['value']
    else:
        value = None
    if 'is_volunteer' in query.keys():
        is_volunteer = query['is_volunteer']
    else:
        is_volunteer = None
    if 'membership_through' in query.keys():
        membership_through = query['membership_through']
    else:
        membership_through = None
    if 'gender' in query.keys():
        gender = query['gender']
    else:
        gender = None
    if 'state' in query.keys():
        state = query['state']
    else:
        state = None

    if set(placeholders).issubset(set(campaign_keys)):
        membership_instance = Membership_details()

        sms_contacts = membership_instance.get_members_details_campaigns(data_type=data_type, value=value,
                                                                         is_volunteer=is_volunteer,
                                                                         membership_through=membership_through,
                                                                         state=state, keys=placeholders, c_type='sms',gender=gender)
    else:
        return json.dumps({"errors": ["unknown placeholders added"], "data": {"status": "0"}})

    if isinstance(sms_contacts, list):
        if len(sms_contacts):
            print len(sms_contacts)
            keys = sms_contacts[0].keys()
            if 'mobile' not in keys and 'phone' not in keys:
                return json.dumps({"errors": ["mobile_number Missing in Excel File"], "data": {"status": "0"}})

            if config.ENVIRONMENT_VARIABLE == 'local':
                print'start sending sms'
                send_bulk_sms_operations(sms_contacts, message, placeholders, keys)
            else:
                
                send_bulk_sms_operations.delay(sms_contacts, message, placeholders, keys)
            return json.dumps({"errors": [], "data": {"status": "1"}})

        return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})

    return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})


def get_membership_contatcs(query, placeholders, c_type,page_no=None):
    try:

        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None
        is_volunteer = request.values.get('is_volunteer')
        membership_through = request.values.get('membership_through')

        if data_type == 'assembly':
            state = request.values.get('state')

        membership_instance = Membership_details()

        data = membership_instance.get_members_profile_details(data_type=data_type, value=value,
                                                               is_volunteer=is_volunteer,
                                                               membership_through=membership_through, state=state,
                                                               page_no=page_no, )

        for member in data:
            member['phone'] = encrypt_decrypt(member['phone'], 'D')
            if member['email'] is not None:
                member['email'] = encrypt_decrypt(member['email'], 'D')

    except Exception as e:
        print str(e)


@blueprint_admin_dash.route('/processOperationalSMS', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD", "MB"])
def processOperationalSMS():
    otp = request.values.get("otp")

    if not verifyOtp(config.admin_number, otp, "BS"):
        return json.dumps({"errors": ["invalid OTP"], "data": {"status": "0"}})

    message = request.values.get('message')

    if message is None or message == '':
        return json.dumps({"errors": ["parameter message missing"], "data": {"status": "0"}})
    contacts = request.values.get('contacts')

    placeholders = find_placeholders(message)
    contacts = json.loads(contacts)
    if isinstance(contacts, list):
        if len(contacts):
            keys = contacts[0].keys()

            if 'mobile' not in keys and 'phone' not in keys:
                return json.dumps({"errors": ["mobile_number Missing in Excel File"], "data": {"status": "0"}})
            if set(placeholders).issubset(set(keys)):
                if config.ENVIRONMENT_VARIABLE == 'local':
                    send_bulk_sms_operations(contacts, message, placeholders, keys)
                else:
                    send_bulk_sms_operations.delay(contacts, message, placeholders, keys)
                return json.dumps({"errors": [], "data": {"status": "1"}})

            else:

                return json.dumps({"errors": ["unknown Place holders found "], "data": {"status": "0"}})

        return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})

    return json.dumps({"errors": ["No Users found to send SMS"], "data": {"status": "0"}})


# @blueprint_admin_dash.route('/sendSMStoallusers', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# def sendSMStoallusers():
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select phone_number as mobile from contacts")
#         contacts = cur.fetchall()
#         message = "Janasena warmly welcomes you all for the 'Formation Day Maha Sabha' on 14th March, 2018 in Kaza, Guntur. Follow goo.gl/8iZeEw for more updates"
#         send_bulk_sms_operations.delay(contacts, message, [], [])
#
#         return "success"
#
#     except Exception as e:
#         print str(e)
#         return str(e)


@blueprint_admin_dash.route('/processBulkSMS', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["OP", "AD", "GM"])
def processBulkSMS():
    try:
        if request.method == 'POST':

            sourcetype = request.form.get('source')
            otp = request.form.get("otp")

            if not verifyOtp(config.admin_number, otp, "BS"):
                return json.dumps({"errors": ["invalid OTP"], "data": {"status": "0"}})

            if sourcetype is None or sourcetype == '':
                return json.dumps({"errors": ["parameter sourcetype missing"], "data": {"status": "0"}})
            message = request.form.get('message')

            if message is None or message == '':
                return json.dumps({"errors": ["parameter message missing"], "data": {"status": "0"}})
            if sourcetype == 'excel':
                excel_file = request.files['file']

                if excel_file and allowed_file(excel_file.filename):

                    placeholders = find_placeholders(message)
                    import xlrd
                    workbook = xlrd.open_workbook(file_contents=excel_file.read())
                    sheet_name = workbook.sheet_names()[0]
                    work_sheet = workbook.sheet_by_name(sheet_name)
                    keys = [v.value for v in work_sheet.row(0)]

                    if 'mobile_number' not in keys:
                        return json.dumps({"errors": ["mobile_number Missing in Excel File"], "data": {"status": "0"}})
                    contacts = []
                    for row_number in range(work_sheet.nrows):
                        d = {}

                        if row_number == 0:
                            continue

                        for col_number, cell in enumerate(work_sheet.row(row_number)):

                            if keys[col_number] == 'mobile_number':
                                numberToSend = ''

                                if cell.value != '':
                                    numberToSend = str(cell.value).split(".")[0]

                                # if len(str(numberToSend)) == 10:
                                #     numberToSend = "91" + str(numberToSend)
                                # elif len(str(numberToSend)) == 12:
                                #     pass
                                # else:
                                #     numberToSend = ''
                                numberToSend=numberToSend[2:]
                                print numberToSend
                                d['mobile_number'] = numberToSend
                            else:
                                d[keys[col_number]] = str(cell.value)
                        numberToSend = d['mobile_number']
                        contacts.append(d)
                        msgToSend = message
                        msgToSend=msgToSend.replace("{{name}}",d['name'])
                        print msgToSend, numberToSend
                    # sendSMS("919676881991", msgToSend)
                    print len(contacts)
                    # if set(placeholders).issubset(set(keys)):

                    #     # if config.ENVIRONMENT_VARIABLE == 'local':
                    #     #     print keys
                    #     #     send_bulk_sms_excel(contacts, message, placeholders, keys)
                    #     # else:
                    #     #     send_bulk_sms_excel.delay(contacts, message, placeholders, keys)

                    # else:

                    #     return json.dumps(
                    #         {"errors": ["All Place holders should in Excel Sheet Columns "], "data": {"status": "0"}})
                else:

                    return json.dumps({"errors": ["Invalid File Uploaded. please check "], "data": {"status": "0"}})

            else:
                database = request.form['contacts']
                if database is None or database == '':
                    return json.dumps({"errors": ["parameter contacts missing"], "data": {"status": "0"}})
                contacts = str(database).split(',')
                if len(contacts):
                    if config.ENVIRONMENT_VARIABLE == 'local':
                        pass
                        #send_bulk_sms_manual(contacts, message)
                    else:
                        send_bulk_sms_manual.delay(contacts, message)

        return json.dumps({"errors": [], "data": {"status": "1"}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["Exception Occured"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/getManualDonations', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@required_roles(["AC", "AD"])
def getManualDonations():
    if request.method == 'POST':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select seq_id,donar_name,address,amount,payment_type,reference_number,reference_date,reference_bank,receipt_url,to_char(create_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY HH12:MI AM') as date from manual_donations order by seq_id")
            rows = cur.fetchall()
            con.close()
            return json.dumps({"errors": [], "data": {"status": "1", "children": rows}})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            return str(e)
    else:
        return render_template("donations/manual_donations.html")

@blueprint_admin_dash.route('/send_donation_receipt', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AC", "AD"])
def send_donation_receipt():
    return render_template("donations/donation_receipts.html")

@blueprint_admin_dash.route('/manualDonations', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AC", "AD"])
def manualDonations():
    if request.method == 'GET':

        return render_template('donations/manual_donation_form.html')
    else:
        name = request.values.get('name')
        address = request.values.get('address')
        payment_type = request.values.get('payment_type')
        amount = request.values.get('amount')
        amount_in_words = request.values.get('amount_in_words')
        ref_number = ''
        ref_date = ''
        ref_bank = ''
        ref_date = request.values.get('date')
        if payment_type != 'Cash':
            ref_number = request.values.get('number')

            ref_bank = request.values.get('bank')

            if ref_number is None or ref_number == '':
                return json.dumps({"errors": ["Parameter ref_number Shoul not be Empty"], "data": {"status": "0"}})

            if ref_bank is None or ref_bank == '':
                return json.dumps({"errors": ["Parameter ref_bank Shoul not be Empty"], "data": {"status": "0"}})

        if name is None or name == '':
            return json.dumps({"errors": ["Parameter Name Shoul not be Empty"], "data": {"status": "0"}})
        if payment_type is None or payment_type == '':
            return json.dumps({"errors": ["Parameter payment_type Shoul not be Empty"], "data": {"status": "0"}})
        if amount is None or amount == '':
            return json.dumps({"errors": ["Parameter amount Shoul not be Empty"], "data": {"status": "0"}})
        if amount_in_words is None or amount_in_words == '':
            return json.dumps({"errors": ["Parameter amount_in_words Shoul not be Empty"], "data": {"status": "0"}})
        if ref_date is None or ref_date == '':
            return json.dumps({"errors": ["Parameter ref_date Shoul not be Empty"], "data": {"status": "0"}})

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "insert into manual_donations(donar_name,address,amount,payment_type,reference_number,reference_date,reference_bank)values(%s,%s,%s,%s,%s,%s,%s) returning seq_id",
                (name, address, amount, payment_type, ref_number, ref_date, ref_bank))
            order_id = cur.fetchone()['seq_id']
            con.commit()

            import tempfile
            import os

            handle, filepath = tempfile.mkstemp(suffix='.pdf')
            fileobj = open(filepath, 'wb')

            response = generate_receipt(fileobj, ordno=str(order_id), name=name, address=address, amount=amount,
                                        payment_type=payment_type, ref_id=ref_number, ref_date=ref_date,
                                        ref_bank=ref_bank, amount_in_words=amount_in_words)
            url = ''
            if response:
                fileobj.close()
                read_file = open(filepath, 'r')

                filename = str(name) + "_" + str(order_id) + ".pdf"
                url = upload_file(read_file, 'donationreceipts', filename)
                cur.execute("update manual_donations set receipt_url=%s where seq_id=%s", (url, order_id))
                con.commit()
                con.close()
            data = {"url": url, 'transaction_id': str(order_id)}

            return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            con.close()
            return json.dumps({"errors": [str(e)], "data": {"status": "0"}})


@blueprint_admin_dash.route('/update_manual_donations', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AC", "AD"])
def update_manual_donations():
    transaction_id = request.values.get('transaction_id')
    name = request.values.get('name')
    address = request.values.get('address')
    payment_type = request.values.get('payment_type')
    amount = request.values.get('amount')
    amount_in_words = request.values.get('amount_in_words')
    ref_number = ''
    ref_date = ''
    ref_bank = ''
    ref_date = request.values.get('date')
    if payment_type != 'Cash':
        ref_number = request.values.get('number')

        ref_bank = request.values.get('bank')

        if ref_number is None or ref_number == '':
            return json.dumps({"errors": ["Parameter ref_number Shoul not be Empty"], "data": {"status": "0"}})

        if ref_bank is None or ref_bank == '':
            return json.dumps({"errors": ["Parameter ref_bank Shoul not be Empty"], "data": {"status": "0"}})

    if name is None or name == '':
        return json.dumps({"errors": ["Parameter Name Shoul not be Empty"], "data": {"status": "0"}})
    if payment_type is None or payment_type == '':
        return json.dumps({"errors": ["Parameter payment_type Shoul not be Empty"], "data": {"status": "0"}})
    if amount is None or amount == '':
        return json.dumps({"errors": ["Parameter amount Shoul not be Empty"], "data": {"status": "0"}})
    if amount_in_words is None or amount_in_words == '':
        return json.dumps({"errors": ["Parameter amount_in_words Shoul not be Empty"], "data": {"status": "0"}})
    if ref_date is None or ref_date == '':
        return json.dumps({"errors": ["Parameter ref_date Shoul not be Empty"], "data": {"status": "0"}})
    if transaction_id is None or transaction_id == '':
        return json.dumps({"errors": ["Parameter transaction_id Shoul not be Empty"], "data": {"status": "0"}})

    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "update manual_donations set donar_name=%s,address=%s,amount=%s,payment_type=%s,reference_number=%s,reference_date=%s,reference_bank=%s where  seq_id=%s",
            (name, address, amount, payment_type, ref_number, ref_date, ref_bank, transaction_id))

        con.commit()

        import tempfile
        import os

        handle, filepath = tempfile.mkstemp(suffix='.pdf')
        fileobj = open(filepath, 'wb')

        response = generate_receipt(fileobj, ordno=str(transaction_id), name=name, address=address, amount=amount,
                                    payment_type=payment_type, ref_id=ref_number, ref_date=ref_date,
                                    ref_bank=ref_bank, amount_in_words=amount_in_words)
        url = ''
        if response:
            fileobj.close()
            read_file = open(filepath, 'r')

            filename = str(name) + "_" + str(transaction_id) + ".pdf"
            url = upload_file(read_file, 'donationreceipts', filename)
            cur.execute("update manual_donations set receipt_url=%s where seq_id=%s", (url, transaction_id))
            con.commit()
            con.close()
        data = {"url": url, 'transaction_id': str(transaction_id)}

        return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
    except (psycopg2.Error, Exception) as e:
        print str(e)
        con.close()
        return json.dumps({"errors": [str(e)], "data": {"status": "0"}})


def upload_file(file, container, filename):
    try:

        try:
            block_blob_service.create_blob_from_stream(container, filename, file)
            attachment = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            print attachment
            return attachment
        except Exception as e:
            print 'Exception=' + str(e)
            return ''


    except Exception as e:
        print str(e)
        return ""


@blueprint_admin_dash.route('/onlineDonations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def onlineDonations():
    try:

        return render_template("admin/donations/donations.html")


    except (psycopg2.Error, Exception) as e:
        print str(e)


@blueprint_admin_dash.route('/admin/getDonations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def getDonations():
    try:
        from_date = request.values.get('from_date')
        to_date = request.values.get('to_date')

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select cast(sum(cast(transaction_payment as numeric))as text) as total_amount from janasena_donations where payment_status='S';")
        total_row = cur.fetchone()
        total_amount = 0
        today_amount = 0
        if total_row is not None:
            if total_row['total_amount'] is not None:
                total_amount = total_row['total_amount']
        cur.execute(
            "select cast(sum(cast(transaction_payment as numeric))as text) as total_amount from janasena_donations where payment_status='S' and date(update_dttm  AT TIME ZONE 'Asia/Calcutta' )=current_date;")
        today_row = cur.fetchone()
        if today_row is not None:
            if today_row['total_amount'] is not None:
                today_amount = today_row['total_amount']

        if from_date is not None and to_date is not None:

            cur.execute(
                "select concat(d.donar_name,' ',d.last_name) as name,d.payment_id,d.relation_name,d.phone,d.email,d.transaction_payment,d.state,ac.district_name,ac.constituency_name,cast(to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,d.pancard,d.address from janasena_donations d left join assembly_constituencies ac on cast(ac.district_id as text)=d.district_id and cast(ac.constituency_id as text)=d.constituency_id where payment_status='S' and date(create_dttm  AT TIME ZONE 'Asia/Calcutta') between %s and %s order by d.seq_id desc;",
                (from_date, to_date))
            rows = cur.fetchall()
            con.close()
        else:
            cur.execute(
                "select concat(d.donar_name,' ',d.last_name) as name,d.payment_id,d.relation_name,d.phone,d.email,d.transaction_payment,d.state,ac.district_name,ac.constituency_name,cast(to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,d.pancard,d.address from janasena_donations d left join assembly_constituencies ac on cast(ac.district_id as text)=d.district_id and cast(ac.constituency_id as text)=d.constituency_id where payment_status='S' and date(create_dttm  AT TIME ZONE 'Asia/Calcutta' )>=current_date-1 order by d.seq_id desc;")
            rows = cur.fetchall()
            con.close()
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "total_amount": total_amount,
                                                  "today_amount": today_amount}})



    except (psycopg2.Error, Exception) as e:
        print str(e)

@blueprint_admin_dash.route('/online_recurring_donations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def online_recurring_donations():
    try:

        return render_template("admin/donations/recurring_donations.html")


    except (psycopg2.Error, Exception) as e:
        print str(e)

@blueprint_admin_dash.route('/admin/get_recurring_donations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def get_recurring_donations():
    try:
        from_date = request.values.get('from_date')
        to_date = request.values.get('to_date')

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select cast(sum(cast(charged_Amount as numeric))as text) as total_amount from recurring_payments_charge_details where payment_status='captured';")
        total_row = cur.fetchone()
        total_amount = 0
        today_amount = 0
        if total_row is not None:
            if total_row['total_amount'] is not None:
                total_amount = total_row['total_amount']
        cur.execute(
            """ select cast(sum(cast(charged_Amount as numeric))as text) as total_amount from recurring_payments_charge_details
where payment_status='captured'
and date(update_dttm  AT TIME ZONE 'Asia/Calcutta' )=current_date;""")
        today_row = cur.fetchone()
        if today_row is not None:
            if today_row['total_amount'] is not None:
                today_amount = today_row['total_amount']

        if from_date is not None and to_date is not None:

            cur.execute(
                """select d.subscription_id,concat(d.donar_name,' ',d.last_name) as name,rc.payment_id,d.relation_name,d.phone,
                    d.email,rc.charged_amount,d.state,ac.district_name,ac.constituency_name,
                    cast(to_char(rc.update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,
                    d.pancard,d.address from janasena_recurring_payments d 
                    join recurring_payments_charge_details rc on d.subscription_id=rc.subscription_id 
                    left join 
                    assembly_constituencies ac on ac.district_id =d.district_id and 
                    ac.constituency_id =d.constituency_id where rc.payment_status='captured' 
                    and date(rc.create_dttm  AT TIME ZONE 'Asia/Calcutta') between %s and %s order by d.seq_id desc; """,
                (from_date, to_date))
            rows = cur.fetchall()
            con.close()
        else:
            cur.execute(
                """ select d.subscription_id,concat(d.donar_name,' ',d.last_name) as name,rc.payment_id,d.relation_name,d.phone,
            d.email,rc.charged_amount,d.state,ac.district_name,ac.constituency_name,
            cast(to_char(rc.update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,
            d.pancard,d.address from janasena_recurring_payments d 
            join recurring_payments_charge_details rc on d.subscription_id=rc.subscription_id 
            left join 
            assembly_constituencies ac on ac.district_id =d.district_id and 
            ac.constituency_id =d.constituency_id where rc.payment_status='captured' 
            and date(rc.create_dttm  AT TIME ZONE 'Asia/Calcutta') >=current_date-1  order by d.seq_id desc;""")
            rows = cur.fetchall()
            con.close()
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "total_amount": total_amount,
                                                  "today_amount": today_amount}})



    except (psycopg2.Error, Exception) as e:
        print str(e)



@blueprint_admin_dash.route('/nriDonations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def nriDonations():
    try:

        return render_template("admin/donations/nri_donations.html")


    except (psycopg2.Error, Exception) as e:
        print str(e)


@blueprint_admin_dash.route('/admin/getNRIDonations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def getNRIDonations():
    try:
        from_date = request.values.get('from_date')
        to_date = request.values.get('to_date')

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select cast(sum(cast(transaction_payment as numeric))as text) as total_amount from janasena_nri_donations where payment_status='S';")
        total_row = cur.fetchone()
        total_amount = 0
        today_amount = 0
        if total_row is not None:
            if total_row['total_amount'] is not None:
                total_amount = total_row['total_amount']
        cur.execute(
            "select cast(sum(cast(transaction_payment as numeric))as text) as total_amount from janasena_nri_donations where payment_status='S' and date(update_dttm  AT TIME ZONE 'Asia/Calcutta' )=current_date;")
        today_row = cur.fetchone()
        if today_row is not None:
            if today_row['total_amount'] is not None:
                today_amount = today_row['total_amount']

        if from_date is not None and to_date is not None:

            cur.execute(
                "select d.donar_name,d.last_name,d.transaction_id,d.transaction_through,d.payment_id,d.relation_name,d.phone,d.email,d.transaction_payment,d.state,cast(to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,d.passport,d.address,d.passport_url from janasena_nri_donations d  where  date(create_dttm  AT TIME ZONE 'Asia/Calcutta') between %s and %s and (payment_status='S' or payment_status is null) order by d.seq_id desc;",
                (from_date, to_date))
            rows = cur.fetchall()
            con.close()
        else:
            cur.execute(
                "select d.donar_name,d.last_name,d.transaction_id,d.transaction_through,d.payment_id,d.relation_name,d.phone,d.email,d.transaction_payment,d.state,cast(to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD-MM-YYYY') as text) as date,d.passport,d.address,d.passport_url from janasena_nri_donations d  where   date(create_dttm  AT TIME ZONE 'Asia/Calcutta' )>=current_date-1 and (payment_status='S' or payment_status is null) order by d.seq_id desc;")
            rows = cur.fetchall()
            con.close()
        for row in rows:
            row['phone'] = encrypt_decrypt(row['phone'], 'D')
            row['email'] = encrypt_decrypt(row['email'])
            row['passport'] = encrypt_decrypt(row['passport'])
            row['passport_url'] = encrypt_decrypt(row['passport_url'], 'D')

            last_name = encrypt_decrypt(row['last_name'], 'D')
            row['name'] = row['donar_name'] + " " + last_name
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "total_amount": total_amount,
                                                  "today_amount": today_amount}})



    except (psycopg2.Error, Exception) as e:
        print str(e)


@blueprint_admin_dash.route('/jsp_leaders', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@ip_validation_required
@admin_login_required
def JSPLeaders():
    if request.method == "GET":
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select category,count(*) from party_leaders group by category")
            rows = cur.fetchall()
            con.close()
            return render_template("admin/leaders.html", data=rows)

        except (psycopg2.Error, Exception) as e:
            print str(e)
            return render_template("admin/leaders.html")



    else:
        region_type = request.values.get("region_type")
        value = request.values.get("value")
        if region_type is None or region_type == '':
            return json.dumps({"errors": ["parameter region type is Missing"], "data": {"status": "0"}})
        if value is None or value == '':
            return json.dumps({"errors": ["parameter value is Missing"], "data": {"status": "0"}})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            data = []
            if region_type == 'district':
                cur.execute("select category,count(*) from party_leaders where district_id=%s group by category ",
                            (value,))
                data = cur.fetchall()
            elif region_type == 'parliament':
                cur.execute(
                    "select category,count(*) from party_leaders where parliament_constituency_id=%s group by category ",
                    (value,))
                data = cur.fetchall()
            elif region_type == 'assembly':
                cur.execute("select category,count(*) from party_leaders where constituency_id=%s group by category ",
                            (value,))
                data = cur.fetchall()
            elif region_type == 'mandal':
                cur.execute("select category,count(*) from party_leaders where mandal_id=%s group by category ",
                            (value,))
                data = cur.fetchall()
            else:
                data = []
            return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            return json.dumps({"errors": ["something wrong please try again later"], "data": {"status": "0"}})


@blueprint_admin_dash.route('/membershipRankings', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
# @ip_validation_required
@admin_login_required
def membershipRankings():
    if request.method == "GET":
        return render_template("admin/memberships/rankings.html")
    else:
        region_type = request.values.get("type")
        value = request.values.get("value")
        if region_type is None or region_type == '':
            return json.dumps({"errors": ["parameter region type is Missing"], "data": {"status": "0"}})
        if value is None or value == '':
            return json.dumps({"errors": ["parameter value is Missing"], "data": {"status": "0"}})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            data = []
            if region_type == 'state':
                if value == 'all':
                    cur.execute(
                        "select st.name,st.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / st.total_votes),2) as text)  AS  percentage from janasena_membership m join state st on st.id=m.state_id where m.state_id in (1,2,3) group by st.name,st.total_votes order by percentage desc")
                    data = cur.fetchall()

                else:
                    cur.execute(
                        "select  d.district_name,d.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / d.total_votes),2) as text)  AS  percentage from janasena_membership m join districts d on d.seq_id=m.district_id where m.state_id=%s group by d.district_name,d.total_votes order by percentage desc",
                        (value,))
                    data = cur.fetchall()

            elif region_type == 'district':

                if value == 'all':
                    cur.execute(
                        "select  d.district_name,d.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / d.total_votes),2) as text)  AS  percentage from janasena_membership m join districts d on d.seq_id=m.district_id where m.state_id in (1,2) group by d.district_name,d.total_votes order by percentage desc")
                    data = cur.fetchall()
                else:
                    cur.execute(
                        "select  ac.district_name,ac.constituency_name,ac.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / ac.total_votes),2) as text) AS  percentage from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where m.district_id=%s  group by ac.district_name,ac.constituency_name,ac.total_votes order by percentage desc",
                        (value,))
                    data = cur.fetchall()
            elif region_type == 'assembly':
                if value == 'all':
                    cur.execute(
                        "select ac.district_name,ac.constituency_name,ac.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / ac.total_votes),2) as text)  AS  percentage from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where m.state_id=1 group by ac.district_name,ac.constituency_name,ac.total_votes order by percentage desc")
                    data = cur.fetchall()
                else:
                    state_id = request.values.get('state_id')
                    cur.execute(
                        "select ac.district_name,ac.constituency_name,ac.total_votes as votes,count(*) as memberships,cast(round(((count(*) * 100)::numeric / ac.total_votes),2)  as text)AS  percentage from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where m.state_id=%s and m.constituency_id=%s group by ac.district_name,ac.constituency_name,ac.total_votes order by percentage desc",
                        (state_id, value,))
                    data = cur.fetchall()
            elif region_type == 'parliament':
                if value == 'all':
                    cur.execute(
                        "select  pc.parliament_constituency_name,pc.total_votes as votes,count(*)as memberships,cast(round(((count(*) * 100)::numeric / pc.total_votes),2) as text) AS  percentage from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id join parliament_constituencies pc on pc.parliament_constituency_name=ac.parliament_constituency_name where m.state_id in (1,2) group by pc.parliament_constituency_name,pc.total_votes order by percentage desc")
                    data = cur.fetchall()
                else:
                    cur.execute(
                        "select  pc.parliament_constituency_name,ac.constituency_name,ac.total_votes as votes,count(*)as memberships,cast(round(((count(*) * 100)::numeric / ac.total_votes),2) as text)  AS  percentage from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id join parliament_constituencies pc on pc.parliament_constituency_name=ac.parliament_constituency_name where pc.parliament_constituency_name=%s group by pc.parliament_constituency_name,ac.total_votes,ac.constituency_name order by percentage desc",
                        (value,))

                    data = cur.fetchall()

            elif region_type == 'mandal':

                cur.execute(
                    "select ac.district_name,ac.parliament_constituency_name,ac.constituency_name,mdl.mandal_name,count(*) as memberships from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id  join mandals mdl on m.mandal_id=mdl.id and m.state_id=mdl.state_id and m.constituency_id=mdl.constituency_id where m.state_id=1 group by ac.district_name,ac.parliament_constituency_name,ac.constituency_name,mdl.mandal_name ")
                data = cur.fetchall()

            else:
                pass
            return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            return json.dumps({"errors": ["something wrong please try again later"], "data": {"status": "0"}})


@blueprint_admin_dash.route("/getPollingBoothVoters", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getPollingBoothVoters():
    state_id = request.values.get('state_id')
    constituency_id = request.values.get('constituency_id')
    polling_booth_id = request.values.get('booth_id')

    if state_id is None or state_id == '':
        return json.dumps({"errors": ["state_id missing .please try again"], "data": {"status": "0"}})
    if constituency_id is None or constituency_id == '':
        return json.dumps({"errors": ["constituency_id missing .please try again"], "data": {"status": "0"}})
    if polling_booth_id is None or polling_booth_id == '':
        return json.dumps({"errors": ["polling_booth_id missing .please try again"], "data": {"status": "0"}})

    try:
        part_no = polling_booth_id
        ac_no = constituency_id
        st = state_id
        subquery = db.session.query(VoterStatus.voter_id)
        voter_list = db.session.query(Voters.voter_name.label('name'), Voters.relation_name, Voters.relation_type,
                                      Voters.house_no, Voters.section_name).filter(
            and_(Voters.state == st, Voters.ac_no == ac_no, Voters.part_no == part_no
                 )).order_by(Voters.house_no).all()
        data = [u._asdict() for u in voter_list]
        return json.dumps({"errors": [], 'data': {'status': '1', "children": data}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_admin_dash.route("/searchVoterID", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def searchVoterID():
    if request.method == 'GET':
        return render_template("admin/search_voter.html")

    else:

        try:

            voter_id = request.values.get("voter_id")

            if voter_id is None or voter_id.isspace():
                return json.dumps({"errors": ["invalid Voter_id .please try again"], "data": {"status": "0"}})

            children = {}

            voterdetails_instance = Voter_details()
            voter_id = str(voter_id).lower()
            record = voterdetails_instance.get_voter_details(voter_id)
            if record is not None:
                try:
                    con = psycopg2.connect(config.DB_CONNECTION)
                    cur = con.cursor()
                    cur.execute(
                        "select constituency_name,district_name from assembly_constituencies where constituency_id=%s and state_id=%s",
                        (record['ac_no'], record['state']))
                    row = cur.fetchone()
                    con.close()
                    if row is not None:
                        record['constituency_name'] = row[0]
                        record['district_name'] = row[1]

                    else:
                        record['constituency_name'] = ''
                        record['district_name'] = ''

                except Exception as e:
                    print str(e)

                return json.dumps({"errors": [], 'data': {'status': '1', 'children': record}})
            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0', 'children': children}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_admin_dash.route("/searchMember", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@ip_validation_required
@required_roles(["AD","OP","MB","SM"])
@csrf.exempt
def searchMember():
    if request.method == 'GET':
        return render_template("admin/search_members2.html")

    else:

        try:

            m_type = request.values.get("type")
            value = request.values.get("value")
            if m_type is None or m_type.isspace():
                return json.dumps({"errors": ["parameter type .please try again"], "data": {"status": "0"}})
            if m_type is None or m_type.isspace():
                return json.dumps({"errors": ["parameter type .please try again"], "data": {"status": "0"}})
            children = {}
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            rows = []
            if m_type == 'm':
                membership_id = str(value).upper()
                cur.execute(
                    "select m.jsp_membership_seq_id,m.membership_id,m.name,m.phone,m.address,m.relationship_name,mt.achievement,m.polling_station_name,m.image_url,m.email,m.voter_id,m.age,m.gender,ac.district_name,ac.constituency_name,ac.parliament_constituency_name,m.polling_station_id,m.image_url from janasena_membership m  left join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id join membership_targets mt on m.membership_id=mt.membership_id where m.membership_id=%s",
                    (membership_id,))
                rows = cur.fetchall()
                for row in rows:
                    row['jsp_membership_seq_id']=create_hashid(row['jsp_membership_seq_id'])
                    row['phone'] = encrypt_decrypt(row['phone'], 'D')
                    if row['voter_id'] is not None and row['voter_id'] != '':
                        row['voter_id'] = encrypt_decrypt(row['voter_id'], 'D')
                    if row['email'] is not None:
                        row['email'] = encrypt_decrypt(row['email'], 'D')
                    image_url = row['image_url']
                    row['image_url'] = 'https://janasenaparty.org/static/img/default-avatar.png'
                    if image_url is not None and image_url != '':
                        old_img_container = str(image_url).split(".")[0]
                        if old_img_container.lower() != "https://janasenalive":
                            row['image_url'] = image_url
                    row['community'] = ''
                    row['occupation'] = ''
                    row['qualification'] = ''
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': rows}})
            else:
                phone = encrypt_decrypt(str(value), 'E')
                cur.execute(
                    "select m.jsp_membership_seq_id,m.membership_id,m.voter_id,mt.achievement,m.address,m.relationship_name,m.polling_station_name,m.image_url,m.polling_station_id,m.name,m.phone,m.email,m.age,m.gender,ac.district_name,ac.constituency_name,ac.parliament_constituency_name from janasena_membership m  left join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id join membership_targets mt on m.membership_id=mt.membership_id where m.phone=%s order by m.create_dttm limit 5",
                    (phone,))
                rows = cur.fetchall()
                for row in rows:
                    row['jsp_membership_seq_id']=create_hashid(row['jsp_membership_seq_id'])
                    row['phone'] = encrypt_decrypt(row['phone'], 'D')
                    if row['voter_id'] is not None and row['voter_id'] != '':
                        row['voter_id'] = encrypt_decrypt(row['voter_id'], 'D')
                    if row['email'] is not None:
                        row['email'] = encrypt_decrypt(row['email'], 'D')
                    image_url = row['image_url']
                    row['image_url'] = 'https://janasenaparty.org/static/img/default-avatar.png'
                    if image_url is not None and image_url != '':
                        old_img_container = str(image_url).split(".")[0]
                        if old_img_container.lower() != "https://janasenalive":
                            row['image_url'] = image_url
                    row['community'] = ''
                    row['occupation'] = ''
                    row['qualification'] = ''
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': rows}})
            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0', 'children': children}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})
from utilities.general import decode_hashid,create_hashid

@blueprint_admin_dash.route("/searchMembers2", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
#@ip_validation_required
@required_roles(["AD","OP","MB","SM"])
@csrf.exempt
def searchMembers2():
    if request.method == 'GET':
        return render_template("admin/search_member.html")

    else:

        try:

            m_type = request.values.get("type")
            value = request.values.get("value")
            if m_type is None or m_type.isspace():
                return json.dumps({"errors": ["parameter type .please try again"], "data": {"status": "0"}})
            if m_type is None or m_type.isspace():
                return json.dumps({"errors": ["parameter type .please try again"], "data": {"status": "0"}})
            children = {}
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            rows = []
            if m_type == 'm':
                membership_id = str(value).upper()
                cur.execute(
                    "select m.jsp_membership_seq_id,m.membership_id,m.name,m.phone,m.address,m.polling_station_name,m.image_url,m.email,m.voter_id,m.age,m.gender,ac.district_name,ac.constituency_name,ac.parliament_constituency_name,m.polling_station_id,m.image_url from janasena_membership m  left join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where m.membership_id=%s",
                    (membership_id,))
                rows = cur.fetchall()
                for row in rows:

                    row['jsp_membership_seq_id']=create_hashid(row['jsp_membership_seq_id'])
                    row['phone'] = encrypt_decrypt(row['phone'], 'D')
                    if row['voter_id'] is not None and row['voter_id'] != '':
                        row['voter_id'] = encrypt_decrypt(row['voter_id'], 'D')
                    if row['email'] is not None:
                        row['email'] = encrypt_decrypt(row['email'], 'D')
                    image_url = row['image_url']
                    row['image_url'] = 'https://janasenaparty.org/static/img/default-avatar.png'
                    if image_url is not None and image_url != '':
                        old_img_container = str(image_url).split(".")[0]
                        if old_img_container.lower() != "https://janasenalive":
                            row['image_url'] = image_url
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': rows}})
            else:
                phone = encrypt_decrypt(str(value), 'E')
                cur.execute(
                    "select m.jsp_membership_seq_id,m.membership_id,m.voter_id,m.address,m.polling_station_name,m.image_url,m.polling_station_id,m.name,m.phone,m.email,m.age,m.gender,ac.district_name,ac.constituency_name,ac.parliament_constituency_name from janasena_membership m  left join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where m.phone=%s order by m.create_dttm limit 5",
                    (phone,))
                rows = cur.fetchall()
                for row in rows:
                    row['jsp_membership_seq_id']=create_hashid(row['jsp_membership_seq_id'])
                    row['phone'] = encrypt_decrypt(row['phone'], 'D')
                    if row['voter_id'] is not None and row['voter_id'] != '':
                        row['voter_id'] = encrypt_decrypt(row['voter_id'], 'D')
                    if row['email'] is not None:
                        row['email'] = encrypt_decrypt(row['email'], 'D')
                    image_url = row['image_url']
                    row['image_url'] = 'https://janasenaparty.org/static/img/default-avatar.png'
                    if image_url is not None and image_url != '':
                        old_img_container = str(image_url).split(".")[0]
                        if old_img_container.lower() != "https://janasenalive":
                            row['image_url'] = image_url
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': rows}})
            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0', 'children': children}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_admin_dash.route("/deleteMembership", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@ip_validation_required
@required_roles(["AD", "OP", "MB"])
@csrf.exempt
def deleteMembership():
    if request.method == 'POST':

        membership_id = request.json["membership_id"]
        reason = request.json["reason"]

        print membership_id, reason
        admin_id = current_user.id
        if membership_id is None or membership_id == '':
            return json.dumps({"errors": ["memebrship_id missing"]})
        if reason is None or reason == '':
            return json.dumps({"errors": ["reason missing"]})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from janasena_membership where membership_id=%s", (membership_id,))
            row = cur.fetchone()
            con.close()
            if row is not None:
                if delete_membership(membership_id, admin_id, reason):
                    return json.dumps({"errors": [], "status": "1"})
                else:
                    return json.dumps({"errors": ["something wrong"], "status": "0"})

            else:
                return json.dumps({"errors": ["No Member found with this Membership ID"], "status": "0"})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


def delete_membership(membership_id, admin_id, reason):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from janasena_membership where membership_id=%s", (membership_id,))
        row = cur.fetchone()
        if row is not None:
            phone = encrypt_decrypt(row['phone'], 'D')
            voter_id = ''
            name = row['name']
            referral_id = row['referral_id']
            if row['voter_id'] is not None:
                voter_id = encrypt_decrypt(row['phone'], 'D')
                cur.execute("delete from voter_registration_status where voter_id=%s", (voter_id,))
                cur.execute("update voters set status=null where voter_id=%s", (voter_id,))
                con.commit()
            cur.execute("delete from janasena_missedcalls where supporter_id=%s", (membership_id,))
            con.commit()
            cur.execute("delete from janasena_membership where membership_id=%s", (membership_id,))
            con.commit()
            cur.execute("delete from membership_targets where membership_id=%s", (membership_id,))
            con.commit()

            if referral_id is not None:
                cur.execute("update membership_targets set achievement=achievement-1 where membership_id=%s",
                            (referral_id,))
                con.commit()
            cur.execute(
                "insert into deleted_memberships(membership_id,referral_id,name,phone,voter_id,deleted_by,reason)values(%s,%s,%s,%s,%s,%s,%s)",
                (membership_id, referral_id, name, phone, voter_id, admin_id, reason))
            con.commit()
            con.close()
            msgToSend = "Your membership has been removed, Thanks for supporting us till now - JANASENA"
            sendSMS(phone, msgToSend)
            return True
        return False

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return False


@blueprint_admin_dash.route("/update_youth_members", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def update_youth_members():
    if config.ENVIRONMENT_VARIABLE == 'local':
        random_membership()
    else:
        random_membership.delay()
    return "success"


from utilities.others import missedcalls_membership


@blueprint_admin_dash.route("/missedcallsMembership", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def missedcallsMembership():
    if config.ENVIRONMENT_VARIABLE == 'local':
        missedcalls_membership()
    else:
        missedcalls_membership.delay()
    return "success"





@blueprint_admin_dash.route("/missedcallsMembershipsms", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def missedcallsMembershipsms():
    if config.ENVIRONMENT_VARIABLE == 'local':
        missedcalls_membership_sms()
    else:
        missedcalls_membership_sms.delay()
    return "success"


@blueprint_admin_dash.route("/send_donation_remainder", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def send_donation_remainder():
    if config.ENVIRONMENT_VARIABLE == 'local':
        send_donation_remainder_sms()
    else:
        send_donation_remainder_sms.delay()
    return "success"


@blueprint_admin_dash.route("/downloads_page_uploader", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def downloads_page_uploader():
    def temp(con, cur):
        if request.method == 'POST':
            if 'input17' in request.files:
                thumbnail = request.files['input17']
            else:
                return json.dumps({'status': 0, 'error': 'PARAMETER input17 MISSING'})

            if 'input05' in request.files:
                download_file = request.files['input05']
            else:
                return json.dumps({'status': 0, 'error': 'PARAMETER input05 MISSING'})
            name = request.values.get('title')
            category = request.values.get('category')
            if name in empty_check:
                return json.dumps({'status': 0, 'error': 'PARAMETER title MISSING'})
            if category in empty_check:
                return json.dumps({'status': 0, 'error': 'PARAMETER category MISSING'})
            container = 'download_thumbnail'

            thumbnail_url = upload_image(thumbnail, container)
            container = 'download_files'
            download_file_url = upload_image(download_file, container)
            cur.execute("""
                INSERT INTO downloads_section
                (name,thumbnail_url,file_url,category,update_dttm)
                values(%s,%s,%s,%s,now())
            
            """, (name, thumbnail_url, download_file_url, category))
            con.commit()
            return json.dumps({'status': 1, 'msg': 'Entry created successfully'})

    return give_cursor_json(temp)


@blueprint_admin_dash.route("/downloads_page", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def downloads_page():
    def temp(con, cur):
        if request.method == 'POST':
            dict_cur = con.cursor(cursor_factory=RealDictCursor)
            dict_cur.execute("""
                           select * from downloads_section
                          

                       """)
            data = dict_cur.fetchall()
            return json.dumps({'status': 1, 'data': data})

    return give_cursor_json(temp)


@blueprint_admin_dash.route("/shatagni_magazine_subscriptions", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP"])
@csrf.exempt
def shatagni_magazine_subscriptions():
    if request.method == 'GET':
        return render_template("admin/magazine_subscriptions.html")
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(query="""select name,phone,email,concat_ws(', ',address,city_town,state,pincode) as address,
    transaction_id,subscription_plan,quantity,cast(subscription_start_date as text) as start_date,cast(subscription_end_date as text) as end_date
    from shatagni_magazine_subscription_details where payment_status ='S' order by seq_id   """)
        rows = cur.fetchall()
        return json.dumps({"errors": [], "status": "1", "data": rows})
    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/get_whatsapp_numbers", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
@csrf.exempt
def get_whatsapp_numbers():
    if request.method == 'GET':
        data = []
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select constituency,phone from whatsapp_groups_number order by seq_id")
            rows = cur.fetchall()
            con.close()
            data = rows
        except Exception as e:
            print str(e)

        return render_template("admin/whatsapp_numbers.html", data=data)

    else:
        constituency = request.values.get("constituency")
        phone = request.values.get("phone")
        if constituency is None or constituency == '':
            return json.dumps({"errors": ["parameter constituency missing"], "status": "0"})
        if phone is None or phone == '':
            return json.dumps({"errors": ["parameter phone missing"], "status": "0"})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select constituency,phone from whatsapp_groups_number where lower(constituency)=%s",
                        (str(constituency).lower(),))
            row = cur.fetchone()
            if row is None:
                cur.execute("insert into whatsapp_groups_number(constituency,phone)values(%s,%s)",
                            (str(constituency).lower(), phone))

                con.commit()
                return json.dumps({"errors": [], "status": "1"})
            else:
                return json.dumps({"errors": ["constituency already there"], "status": "0"})
        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/grampanchayats", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP"])
@csrf.exempt
def grampanchayats():
    if request.method == 'GET':
        return render_template("admin/gram_panchayats.html")


@blueprint_admin_dash.route("/grampanchayat_wards", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP"])
@csrf.exempt
def grampanchayat_wards():
    if request.method == 'GET':
        try:
            panchayat_id = request.values.get("id")
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from grampanchayat_wards where parent=%s order by ward", (panchayat_id,))
            rows = cur.fetchall()

            return json.dumps({"errors": [], "status": "1", "data": rows})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/active_volunteers", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP"])
@csrf.exempt
def active_volunteers():
    if request.method == 'GET':
        try:
            panchayat_id = request.values.get("id")
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select distinct on (av.phone)ac.district_name,ac.constituency_name,m.polling_station_id,m.name,av.phone,m.gender,m.age,m.membership_id from active_volunteers av join janasena_missedcalls jm on jm.phone=av.phone join janasena_membership m on m.membership_id=jm.supporter_id join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id ")
            rows = cur.fetchall()

            return json.dumps({"errors": [], "status": "1", "data": rows})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/update_volunteers_status", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
@csrf.exempt
def update_volunteers_status():
    if request.method == 'POST':
        try:
            member_id = request.values.get("id")
            member_type = request.values.get("type")
            flag = request.values.get("flag")
            value = request.values.get("value")
            if member_id is None or member_id == '':
                return json.dumps({"errors": ["id missing"], "status": "0"})
            if member_type is None or member_type == '':
                return json.dumps({"errors": ["type missing"], "status": "0"})
            if flag is None or flag == '':
                return json.dumps({"errors": ["flag missing"], "status": "0"})
            if value is None or value == '':
                return json.dumps({"errors": ["value missing"], "status": "0"})

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            if member_type == "observer":
                if flag == "p":
                    cur.execute("update party_observers set photos_status=%s where id=%s", (value, member_id))
                    con.commit()
                elif flag == "m":
                    cur.execute("update party_observers set material_status=%s where id=%s", (value, member_id))
                    con.commit()
                elif flag == 'r':
                    cur.execute("update party_observers set remarks=%s where id=%s", (value, member_id))
                    con.commit()
                else:
                    date = request.values.get('date')
                    cur.execute(
                        "select * from house_holds_covered where member_type=%s and member_id=%s and date(create_dttm)=%s",
                        ('observer', member_id, date))
                    existing = cur.fetchone()
                    if existing is None:
                        cur.execute(
                            "insert into house_holds_covered(member_id,member_type,create_dttm,houses_covered)values(%s,%s,%s,%s)",
                            (member_id, 'observer', date, value))
                        con.commit()
                    else:
                        cur.execute(
                            "update house_holds_covered set houses_covered=%s where member_type=%s and member_id=%s and date(create_dttm)=%s ",
                            (value, 'observer', member_id, date))
                        con.commit()

            else:
                if flag == "p":
                    cur.execute("update active_members_ap set photos_status=%s where seq_id=%s", (value, member_id))
                    con.commit()
                elif flag == 'h':

                    date = request.values.get('date')
                    cur.execute(
                        "select * from house_holds_covered where member_type=%s and member_id=%s and date(create_dttm)=%s",
                        ('member', member_id, date))
                    exisitng = cur.fetchone()
                    if exisitng is None:
                        cur.execute(
                            "insert into house_holds_covered(member_id,member_type,create_dttm,houses_covered)values(%s,%s,%s,%s)",
                            (member_id, 'member', date, value))
                        con.commit()
                    else:
                        cur.execute(
                            "update house_holds_covered set houses_covered=%s where member_type=%s and member_id=%s and date(create_dttm)=%s ",
                            (value, 'member', member_id, date))
                        con.commit()
                else:
                    cur.execute("update active_members_ap set material_status=%s where seq_id=%s", (value, member_id))
                    con.commit()
            print "success"
            return json.dumps({"errors": [], "status": "1"})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/observers_portal", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP", "CC"])
@csrf.exempt
def observers_portal():
    pcs = []
    print current_user
    if current_user.urole == 'CC':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            statrt = 0
            end = 0
            if current_user.id == 40:
                statrt = 1
                end = 5
            elif current_user.id == 41:
                statrt = 6
                end = 10
            elif current_user.id == 42:
                statrt = 11
                end = 15
            elif current_user.id == 43:
                statrt = 16
                end = 20
            else:
                statrt = 21
                end = 25
            cur.execute(
                "select pc_id as parliament_constituency_id,parliament_constituency_name from parliament_constituencies where  state_id =1 and pc_id between %s and %s ",
                (statrt, end))
            pcs = cur.fetchall()
            con.close()
        except Exception as e:
            print str(e)

    else:
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select pc_id as parliament_constituency_id,parliament_constituency_name from parliament_constituencies where  state_id =1  ")
            pcs = cur.fetchall()
            con.close()

        except Exception as e:
            print str(e)

    return render_template("admin/observers.html", parliament_constituencies=pcs)


@blueprint_admin_dash.route("/active_volunteers_by_type", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
@csrf.exempt
def active_volunteers_by_type():
    if request.method == 'GET':
        try:
            req_type = request.values.get("type")
            req_value = request.values.get("value")
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            data = []
            pickup_points = []
            if req_type == 'assembly':
                cur.execute(
                    "select id,obs_name,phone,photos_status,material_status from party_observers where observer_type=%s and constituency_id=%s",
                    ('CONSTITUENCY_OBS', req_value))
                rows = cur.fetchall()
                for row in rows:
                    observer_id = row['id']

                    cur.execute(
                        "select seq_id,name,phone,photos_status,material_status from active_members_ap  where observer_id=%s ",
                        (observer_id,))
                    mrows = cur.fetchall()
                    row['active_members'] = mrows
                cur.execute(
                    "select distinct on (pp.pickup_point_name,pp.pickup_person,pp.phone)pp.pickup_point_name,pp.pickup_person,pp.phone from pickup_points pp join pickup_points_mandal_wise pm on pm.pickup_point_name=pp.pickup_point_name where pm.constituency_id=%s ",
                    (req_value,))
                pickup_points = cur.fetchall()

                data = rows
            else:

                cur.execute(
                    "select id,obs_name,phone,photos_status,material_status from party_observers where observer_type=%s and mandal_id=%s",
                    ('MANDAL_OBS', req_value))
                rows = cur.fetchall()
                for row in rows:
                    observer_id = row['id']

                    cur.execute(
                        "select name,phone,photos_status,material_status,seq_id from active_members_ap  where observer_id=%s ",
                        (observer_id,))
                    mrows = cur.fetchall()
                    row['active_members'] = mrows
                cur.execute(
                    "select distinct on (pp.pickup_point_name,pp.pickup_person,pp.phone)pp.pickup_point_name,pp.pickup_person,pp.phone from pickup_points pp join pickup_points_mandal_wise pm on pm.pickup_point_name=pp.pickup_point_name where pm.mandal_id=%s ",
                    (req_value,))
                pickup_points = cur.fetchall()
                if len(pickup_points) == 0:
                    cur.execute(
                        "select distinct on (pp.pickup_point_name,pp.pickup_person,pp.phone)pp.pickup_point_name,pp.pickup_person,pp.phone from pickup_points pp join pickup_points_mandal_wise pm on pm.pickup_point_name=pp.pickup_point_name join mandals m on m.constituency_id=pm.constituency_id where m.id=%s",
                        (req_value,))
                    pickup_points = cur.fetchall()
                data = rows
            return json.dumps({"errors": [], "status": "1", "data": rows, "pickup_points": pickup_points})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/active_volunteers_report", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP"])
@csrf.exempt
def active_volunteers_report():
    if request.method == 'GET':
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select count(*) from active_volunteers_view ")
        count = cur.fetchone()['count']
        con.close()
        return render_template("admin/active_members.html", count=count)

    try:
        req_type = request.values.get("type")

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        if req_type == 'district':
            cur.execute(
                "select ac.district_name,count(*) from active_volunteers_view av join janasena_missedcalls jm on jm.phone=av.phone join janasena_membership m on m.membership_id=jm.supporter_id join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where ac.state_id=%s group by ac.district_id,ac.district_name order by count(*) desc ",
                (1,))
            rows = cur.fetchall()
        else:
            cur.execute(
                "select ac.district_name,ac.constituency_name,count(*) from active_volunteers_view av join janasena_missedcalls jm on jm.phone=av.phone join janasena_membership m on m.membership_id=jm.supporter_id join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where ac.state_id=%s group by ac.district_id,ac.district_name,ac.constituency_id,ac.constituency_name order by count(*) desc ",
                (1,))
            rows = cur.fetchall()
        return json.dumps({"errors": [], "status": "1", "data": rows})


    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["something wrong"], "status": "0"})


from utilities.general import decode_hashid


@blueprint_admin_dash.route("/jsp_tarangam", methods=['GET', 'POST', 'PUT'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def jsp_tarangam():
    if request.method == 'GET':
        try:
            # id=decode_hashid(id)
            # con = psycopg2.connect(config.DB_CONNECTION)
            # cur = con.cursor(cursor_factory=RealDictCursor)
            # cur.execute("select id,obs_name,phone from party_observers where id=%s ",(id,))
            # row=cur.fetchone()
            # con.close()
            # if row is not  None:

            return render_template("active_members.html")

        except Exception as e:
            print str(e)
            return render_template("active_members.html")


    elif request.method == 'PUT':
        try:
            # id=decode_hashid(id)
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            mobile = request.values.get('mobile')
            mobile = "91" + str(mobile)
            cur.execute("select id,obs_name,phone from party_observers where  phone=%s", (mobile,))
            row = cur.fetchone()
            con.close()
            if row is not None:
                genOtp(mobile)

                return json.dumps({"errors": [], "status": "1"})
            return json.dumps({"errors": ["mobile number does not match"], "status": "0"})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})

    else:

        otp = request.values.get('otp')
        mobile = request.values.get('mobile')
        mobile = "91" + str(mobile)
        if verifyOtp(mobile, otp):
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select am.name,am.phone from active_members_ap am join party_observers obs on obs.id=am.observer_id where  obs.phone=%s   ",
                (mobile,))
            rows = cur.fetchall()
            return json.dumps({"errors": [], "status": "1", "data": rows})

        else:
            return json.dumps({"errors": ["Wrong OTP"], "status": "0"})


@blueprint_admin_dash.route("/assembly_comittee", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "OP", 'CD'])
# @ip_validation_required
@csrf.exempt
def assembly_comittee():
    return render_template("admin/assembly_comittee_details.html")

@blueprint_admin_dash.route("/parliament_committee", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","OP"])
#@ip_validation_required
@csrf.exempt
def parliament_committee():

    return render_template("admin/parliament_comittee_details.html")

@blueprint_admin_dash.route("/vm_details", methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
#@ip_validation_required
@csrf.exempt
def vm_details():
    if request.method == 'GET':
        return render_template("admin/vm_details.html")
    elif request.method == 'POST':
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor() as cur:
            cur.execute('select wing_name,name,phone,alternate_phone,designation,email_id,'
                        'jsp_voter_id,assembly,parliament_constituency,remarks from vm_details')
            rows = cur.fetchall()
            return json.dumps({'status':1,'msg':'data fetched successfully','data':rows})

@blueprint_admin_dash.route("/parliament_committee_details", methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
#@ip_validation_required
@admin_login_required
@required_roles(["AD","OP"])
@csrf.exempt
def parliament_committee_details():

    if request.method=='POST':
        try:
            parliament=request.json['parliament']

            data=[]
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            main_query="""select seq_id,name,designation,phone,age,gender,
                assembly,parliament,
                caste,membership_id,
                voter_id,image_url from parliament_committee """
            where_clause=""
            if parliament!='' :
                where_clause="where lower(parliament) ='"+str(parliament).lower()+"'"


            query=main_query+" "+where_clause
            cur.execute(query)
            rows=cur.fetchall()


            return json.dumps({"errors":[],"status":"1","data":rows})


        except Exception as e:
            print str(e)
            return json.dumps({"errors":["something wrong"],"status":"0"})



@blueprint_admin_dash.route("/assembly_comittee_details", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @ip_validation_required
@admin_login_required
@required_roles(["AD", "OP", "CD"])
@csrf.exempt
def assembly_comittee_details():
    if request.method == 'POST':
        try:
            parliament = request.json['parliament']
            skills = request.json['skills']
            assembly = request.json['assembly']
            role = request.json['role']
            data = []
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            main_query = """select cm.seq_id,cm.name,cm.role,cm.phone,cm.age,cm.gender,
                ac.constituency_name,ac.parliament_constituency_name,mdl.mandal_name,
                cm.photo_url,cm.village,cm.skills,cm.caste,cm.experience,cm.membership_id,cm.source,
                cm.voter_id,cm.village,cm.completed_prints,cm.pdf_url,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY ') as date from assembly_comittee cm 
                join assembly_constituencies ac on ac.constituency_id=cm.assembly
                join mandals mdl on mdl.id=cm.mandal"""

            if parliament != '' and assembly != '' and role != '':
                where_clause = "where ac.state_id=1 and ac.constituency_id=" + str(
                    assembly) + " and cm.role='" + role + "'"

            elif parliament != '' and assembly != '' and role == '':
                where_clause = "where ac.state_id=1 and ac.constituency_id= " + str(assembly)

            elif parliament == '' and role != '':
                where_clause = "where ac.state_id=1 and cm.role ='" + role + "'"
            elif parliament != '' and role != '' and assembly == '':
                where_clause = "where ac.state_id=1 and cm.role='" + role + "' and ac.parliament_constituency_id= " + str(
                    parliament)
            elif parliament != '' and role == '' and assembly == '':
                where_clause = "where ac.state_id=1  and ac.parliament_constituency_id= " + str(parliament)
            else:
                where_clause = "where ac.state_id=1"
            query = main_query + " " + where_clause
            cur.execute(query)
            rows = cur.fetchall()
            data = []
            if len(skills):
                for row in rows:
                    r_skills = row['skills'].split(',')
                    if set(skills) < set(r_skills):
                        data.append(row)
            else:
                data = rows
            for item in data:
                skills = item['skills'].split(",")

                skills_str = ''
                for index in range(len(skills)):
                    skills_str = skills_str + str(index + 1) + ". " + skills[index] + "<br>"
                item['skills'] = skills_str

            return json.dumps({"errors": [], "status": "1", "data": data})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route("/update_member_role", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
@ip_validation_required
@csrf.exempt
def update_member_role():
    if request.method == 'POST':
        try:
            seq_id = request.json['seq_id']
            membership_id = request.json['membership_id']
            role = request.json['role']
            print seq_id, membership_id, role
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            membership_id = str(membership_id).lower()
            cur.execute("""update assembly_comittee set role=%s where seq_id=%s and lower(membership_id)=%s """,
                        (role, seq_id, membership_id))
            con.commit()

            return json.dumps({"errors": [], "status": "1"})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something wrong"], "status": "0"})


@blueprint_admin_dash.route('/verify_assembly_member', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verify_assembly_member():
    try:
        phone = request.values.get("phone")
        if phone is None:
            return json.dumps({"status": "0", "errors": "phone number missing"})
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                    select * 
                    from ac_head
                    where phone=%s
                """, (phone,))
            phone_proxy = cur.fetchone()
            if phone_proxy:
                utype = phone_proxy['utype']
                genOtp("91" + phone, type=utype)
                return json.dumps({"status": "1"})

            return json.dumps({"status": "0", "errors": "this phone number is not authorized to login"})
    except (psycopg2.Error, Exception) as e:
        print(e)
        return json.dumps({'status': 0, 'msg': 'Something Wrong'})


@blueprint_admin_dash.route("/constituency_member_login", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def constituency_member_login():
    if request.method == 'GET':
        if session.get('cl_logged_in') is not None:
            if session.get('cl_logged_in'):
                return redirect(url_for('.constituency_members'))
        return render_template('admin/constituency_member_login.html')
    elif request.method == 'POST':

        phone_number = request.json.get('phone')
        otp = request.json.get('otp')
        print(phone_number, otp)
        if phone_number is None or otp is None:
            return json.dumps({"errors": ["parameters missing"], "status": "0"})
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                select * from ac_head
                where phone = %s
            """, (phone_number,))
            row = cur.fetchone()
            if row:
                utype = row['utype']
            else:
                return json.dumps({"errors": ["Invalid Details"], "status": "0"})
            if verifyOtp('91' + phone_number, otp, type=utype):
                if checkUserAuth(phone_number, otp, utype):
                    session['next'] = url_for('.constituency_members')
                    session['current'] = url_for('.constituency_member_login')
                    session['cl_logged_in'] = True

                    return json.dumps({"errors": [], "status": "1", "redirecturi": "/constituency_members"})
                else:
                    return json.dumps({"errors": ["Invalid Details"], "status": "0"})
            else:

                return json.dumps({"errors": ["Wrong OTP. Please check"], "status": "0"})


@blueprint_admin_dash.route("/jsp_aspirants_details", methods=['POST', 'GET'])
@admin_login_required
@required_roles(["AD", "OP",'WR'])
def jsp_aspirants_details():
    if request.method == 'GET':
        return render_template('admin/jsp_aspirant_details.html')
    else:
        assembly = request.values.get('assembly')
        parliament = request.values.get('parliament')
        process_status = request.values.get('further_process')
        if parliament is None:
            parliament = ''
        if assembly is None:
            assembly = ''
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            base_query = """select seq_id,name,aspirant_type,caste,aspirant_location,mobile,membership_id,photo_url,further_process,aspirant_locations,gender
                    from aspirants """
            where_condition = ""
            if process_status != '' and process_status is not None:
                where_condition = "where further_process='" + process_status + "'"
            parliament_status = False
            asm = []
            if parliament != '' and assembly == '':
                cur.execute(
                    "select constituency_name from assembly_constituencies where parliament_constituency_name =%s",
                    (parliament,))
                assemblies = cur.fetchall()

                for item in assemblies:
                    asm.append(item['constituency_name'] + ' assembly')
                asm.append(parliament + ' parliament')
                asms = ', '.join("'{0}'".format(w) for w in asm)
                if where_condition == '':

                    where_condition = "where  (aspirant_locations -> 'locations')::jsonb ?| array[" + asms + "]"

                else:
                    where_condition = where_condition + " and (aspirant_locations -> 'locations')::jsonb ?| array[" + asms + "]"

            elif parliament != '' and assembly != '':
                assembly = assembly + " assembly"
                if where_condition == '':

                    where_condition = "where  (aspirant_locations -> 'locations')::jsonb @> '[\"" + assembly + "\"]'::jsonb"
                else:
                    where_condition = where_condition + " and (aspirant_locations -> 'locations')::jsonb @> '[\"" + assembly + "\"]'::jsonb"
            else:
                pass
            query = base_query + " " + where_condition

            if parliament_status:
                cur.execute(query, (tuple(asm),))
            else:
                cur.execute(query)

            data = cur.fetchall()
            for member in data:
                if member['aspirant_locations'] is not None:
                    try:
                        member['locations'] = ",".join(member['aspirant_locations']['locations'])
                    except Exception as e:
                        member['locations'] = ''
                else:
                    member['locations'] = ''
            return json.dumps({"status": 1, "data": data})


@blueprint_admin_dash.route("/jsp_volunteers_details", methods=['POST', 'GET'])
@admin_login_required
@required_roles(["AD", "OP"])
def jsp_volunteers_details():
    if request.method == 'GET':
        return render_template('admin/volunteers_details.html')
    else:
        assembly=request.values.get('assembly')
        district=request.values.get('district')
        print assembly,district
        if district is None:
            district = ''
        if assembly is None:
            assembly = ''
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            base_query = """select rsp.seq_id,ac.district_name,ac.constituency_name,mdl.mandal_name,rsp.name,rsp.age,rsp.mobile,rsp.volunteer_status,rsp.work_type,rsp.volunteer_work_location,rsp.profession,rsp.email,rsp.whatsapp_number,rsp.start_date,rsp.end_date,rsp.part_time_hours,rsp.skills ->'skills' as skills from rsp_volunteers rsp join assembly_constituencies ac on ac.state_id=rsp.state and rsp.constituency_id=ac.constituency_id join mandals mdl on rsp.mandal=mdl.id  """
            order_by="order by ac.state_id,ac.constituency_id"
            where_condition=""
            if district!="" and assembly!="":
                where_condition="where ac.district_name='"+district+"' and ac.constituency_id="+assembly
            elif district!='' and assembly=='':
                where_condition="where ac.district_name='"+district+"'"
            else:
                where_condition=""
            query = base_query + " " + where_condition+" "+order_by
            cur.execute(query)
            data = cur.fetchall()
            for member in data:
                if member['part_time_hours']==0:
                    member['part_time_hours']='' 
                if member['start_date'] is not None:
                    member['start_date']=str(member['start_date']).split(" ")[0] 
                if member['end_date'] is not None:
                    member['end_date']=str(member['end_date']).split(" ")[0]     
                if member['skills'] is not None:
                    try:
                        member['skills'] = ",".join(member['skills'])
                    except Exception as e:
                        member['skills'] = ''
                else:
                    member['skills'] = ''
            return json.dumps({"status": 1, "data": data}) 

@blueprint_admin_dash.route("/nri_volunteersdata", methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","OP"])
def nri_volunteersdata():
    assembly=request.values.get('assembly')
    district=request.values.get('district')
    print assembly,district
    if district is None:
        district=''
    if assembly is None:
        assembly=''
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        base_query=""" SELECT ac.district_name,ac.constituency_name,nv.name,nv.age,nv.email,nv.country,nv.phone,nv.city,nv.profession,nv.skills->'skills' as skills ,nv.work_as_volunteer,nv.ful_time_or_part_time,nv.work_in_abroad FROM NRI_VOLUNTEERS nv join assembly_constituencies ac on ac.state_id=nv.state and nv.constituency=ac.constituency_id """
        where_condition=""

        asm=[]
        if district !='' and assembly !='':



            where_condition="where  ac.district_name='"+str(district)+ "' and constituency="+assembly


        elif district !='' and assembly =='':
            assembly=assembly+" assembly"


            where_condition="where  ac.district_name='"+str(district)+ "'"

        else:
            where_condition=" where ac.state_id=1"
        order_condition="order by ac.state_id,ac.constituency_id"
        query=base_query+" "+where_condition +" "+order_condition


        cur.execute(query)

        data = cur.fetchall()
        for member in data:

            try:
                member['skills']=",".join(member['skills'])
            except Exception as e:
                member['skills']=''

        return json.dumps({"status":1,"data":data})


@blueprint_admin_dash.route("/nri_volunteers_data", methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","OP"])
def nri_volunteers_data():

    return render_template('admin/nri_volunteers.html')

@blueprint_admin_dash.route("/it_volunteers", methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","OP"])
def it_volunteers():

    return render_template('admin/it_volunteers.html')

@blueprint_admin_dash.route("/it_volunteersdata", methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","OP"])
def it_volunteersdata():
    assembly=request.values.get('assembly')
    district=request.values.get('district')
    print assembly,district
    if district is None:
        district=''
    if assembly is None:
        assembly=''
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        base_query=""" select name,email,phone,membership_id,state,district,constituency,company,current_location from it_volunteers  """
        where_condition=""

        asm=[]
        if district !='' and assembly !='':



            where_condition="where  ac.district='"+str(district)+ "' and constituency='"+assembly+"'"


        elif district !='' and assembly =='':



            where_condition="where  district='"+str(district)+ "'"

        else:
            where_condition=" "
        order_condition="order by state,district,constituency"
        query=base_query+" "+where_condition +" "+order_condition


        cur.execute(query)

        data = cur.fetchall()


        return json.dumps({"status":1,"data":data})


@blueprint_admin_dash.route("/jsp_aspirants_details_by_id", methods=['POST', 'GET'])
@admin_login_required
@required_roles(["AD", "OP",'WR'])
def jsp_aspirants_details_by_id():
    if request.method == 'GET':
        seq_id = request.values.get('seq_id')
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:

            cur.execute("""
                    select * 
                    from aspirants where seq_id=%s """, (seq_id,))
            data = cur.fetchone()
            if data:
                data['create_date'] = str(data['create_date']).split(" ")[0]

                if data['aspirant_locations'] is not None:
                    try:
                        data['locations'] = ",".join(data['aspirant_locations']['locations'])
                    except Exception as e:
                        data['locations'] = ''
                else:
                    data['locations'] = ''

                return json.dumps({"status": 1, "data": data})
            else:
                return json.dumps({"status": 0})
from a3 import aspirant_pdf
@blueprint_admin_dash.route("/aspirant_prints", methods=['POST', 'GET'])
# @admin_login_required
# @required_roles(["AD","OP"])
def aspirant_prints():
    if request.method == 'GET':
        seq_id=request.values.get('seq_id')
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:


            cur.execute("""select seq_id,name,mobile,email,relation_name,birth_place,dob,higher_education,school_and_place,membership_id,photo_url
,occupation,(aspirant_locations -> 'locations') as locations,gender,permenant_address,current_address,further_process
from aspirants where further_process in ('further process','may be further process') and membership_id='JSP45416372' order by membership_id,further_process 
                   """)
            rows = cur.fetchall()
            for row in rows:
                row['locations']=','.join(row['locations'])
                aspirant_pdf(row)
            return 'completed'


@blueprint_admin_dash.route("/constituency_members", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@constituency_login_required
def constituency_members():
    print(current_user.utype)
    if current_user.utype == 'CL':
        state_id = current_user.state_id
        constituency_id = current_user.constituency_id
        print(state_id, constituency_id)
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor() as cur:
            cur.execute("""
                select jm.name,jm.phone,t.mandal_name
                from 
                (select pb.state_id,pb.constituency_id,pb.polling_station_no,m.mandal_name
                from polling_booths_2018_jan pb join
                mandals m on m.state_id = pb.state_id and m.constituency_id =pb.constituency_id and m.id = pb.mandal_id)t
                right join janasena_membership jm on
                t.state_id = jm.state_id and t.constituency_id = jm.constituency_id
                and t.polling_station_no = jm.polling_station_id and t.state_id = jm.state_id
                where jm.constituency_id = %s and jm.state_id=%s and jm.name not like %s
            """, (constituency_id, state_id, '%test%'))
            rows = cur.fetchall()
            final_data = []
            for row in rows:
                drow = list(row)
                drow[1] = encrypt_decrypt(row[1], 'D')
                final_data.append(drow)

        return render_template("admin/constituency_members.html", final_data=final_data)
    elif current_user.utype == 'CLA':
        return render_template("admin/constituency_members_all.html")
    elif current_user.utype == 'CLD':
        return render_template("admin/constituency_members_district.html")
    else:
        return redirect('admin_dash.constituency_member_login')


@blueprint_admin_dash.route("/constituency_members_all", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@constituency_login_required
def constituency_members_all():
    print(request.json)
    constituency_id = request.json.get('constituency_id')
    state_id = 1
    print(constituency_id)
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                    select jm.name,jm.phone,t.mandal_name as mandal
                    from 
                    (select pb.state_id,pb.constituency_id,pb.polling_station_no,m.mandal_name
                    from polling_booths_2018_jan pb join
                    mandals m on m.state_id = pb.state_id and m.constituency_id =pb.constituency_id and m.id = pb.mandal_id)t
                    right join janasena_membership jm on
                    t.state_id = jm.state_id and t.constituency_id = jm.constituency_id
                    and t.polling_station_no = jm.polling_station_id and t.state_id = jm.state_id
                    where jm.constituency_id = %s and jm.state_id=%s and jm.name not like %s
                """, (constituency_id, state_id, '%test%'))
        rows = cur.fetchall()
        final_data = []
        for row in rows:
            drow = list(row)
            drow[1] = encrypt_decrypt(row[1], 'D')
            final_data.append(drow)
        print(final_data)
        return json.dumps({'status': 1, 'data': final_data})


@blueprint_admin_dash.route("/constituency_members_district", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@constituency_login_required
def constituency_members_district():
    print(request.json)
    constituency_id = request.json.get('constituency_id')
    mandal_id =  request.json.get('mandal_id',None)
    state_id = 1
    # print(constituency_id)
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        if mandal_id not in empty_check:
            cur.execute("""
                        select jm.name,jm.phone,t.mandal_name as mandal,jm.gender
                        from 
                        (select pb.state_id,pb.constituency_id,pb.polling_station_no,m.mandal_name
                        from polling_booths_2018_jan pb join
                        mandals m on m.state_id = pb.state_id and m.constituency_id =pb.constituency_id and m.id = pb.mandal_id
                        where m.id =%s)t
                        join janasena_membership jm on
                        t.state_id = jm.state_id and t.constituency_id = jm.constituency_id
                        and t.polling_station_no = jm.polling_station_id and t.state_id = jm.state_id
                        where jm.constituency_id = %s and jm.state_id=%s and jm.name not like %s
                        order by t.mandal_name asc,jm.gender desc
                    """, (mandal_id,constituency_id, state_id, '%test%'))
        else:
            cur.execute("""
                            select jm.name,jm.phone,t.mandal_name as mandal,jm.gender
                            from 
                            (select pb.state_id,pb.constituency_id,pb.polling_station_no,m.mandal_name
                            from polling_booths_2018_jan pb join
                            mandals m on m.state_id = pb.state_id and m.constituency_id =pb.constituency_id 
                            and m.id = pb.mandal_id
                            )t
                            right join janasena_membership jm on
                            t.state_id = jm.state_id and t.constituency_id = jm.constituency_id
                            and t.polling_station_no = jm.polling_station_id and t.state_id = jm.state_id
                            where jm.constituency_id = %s and jm.state_id=%s and jm.name not like %s
                            order by t.mandal_name asc,jm.gender desc
                        """, ( constituency_id, state_id, '%test%'))
        rows = cur.fetchall()
        final_data = []
        for row in rows:
            drow = list(row)
            drow[1] = encrypt_decrypt(row[1], 'D')
            final_data.append(drow)
        # print(final_data)
        return json.dumps({'status': 1, 'data': final_data})


@blueprint_admin_dash.route('/logoutcl')
@constituency_login_required
def logoutcl():
    logout_user()

    session.clear()

    return redirect(url_for('admin_dash.constituency_member_login'))

@blueprint_admin_dash.route('/test_data_mnmn',methods=['GET','POST'])
@admin_login_required
@required_roles(["MN"])
def test_data_mnmn():
    if request.method == 'GET':
        return render_template('/admin/fake_numbers.html')
    india_count = request.values.get('india')
    
    if india_count is None :
        return json.dumps({'errors':['fields missing'],'status':0})

    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("insert into fake_counts(india_count)values(%s)",(india_count,))
        con.commit()

        con.close()
        return json.dumps({"errors":[],"status":"1"})

    except psycopg2.Error as e:
        return makeResponse(json.dumps({"status": "error", "response": {"message": "Internal Server Error"}}), 500,
                            'application/json')

@blueprint_admin_dash.route("/addkriyavolunteer",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@admin_login_required
@required_roles(["AD","KM"])
@csrf.exempt
def addkriyavolunteer():
    
    try:
        request_data = request.get_json()
        print(request_data)
        mobile=request_data.get('mobile',None)
        name  = request_data.get('name',None)
        
        state  = request_data.get('state',None)
        district_id  = request_data.get('district_id',None)
        constituency_id  = request_data.get('constituency_id',None)
        
        
        status,errmsg = KriyaVolunteers.validateUser( name, mobile,state,district_id,constituency_id)
        if status is False:            
            return SendResponse(code=400,error=errmsg,result=None)
        existing = KriyaVolunteers.query.filter_by(mobile=mobile).first()
        if existing is None:
            newvolunteer =KriyaVolunteers(name=name,mobile=mobile,state=state,
                district_id=district_id,constituency_id=constituency_id,status="Pending")
            
            db.session.add(newvolunteer)
            db.session.commit()
            return SendResponse(code=200,error=None,result={"message":"added successfully"})
        return SendResponse(code=200,error="volunteerr already existed with given mobile number",result=None)
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


def SendResponse(code=200, error=None, result=None):
    if error is None:
        status = 'success'
        response = result
    else:
        status = 'error'
        response = {'message':error}
    message = {
        "status": status,
        "response": response,
        "status_code": code
    }
    resp = flask.jsonify(message)
    resp.status_code = code
    return resp

@blueprint_admin_dash.route("/activate_deactivate_kriya_volunteer",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@admin_login_required
@required_roles(["AD","KM"])
@csrf.exempt
def activate_deactivate_volunteer():
    if request.method=='POST':
        try:
            request_data = request.get_json()
            id=request_data.get('id',None)
            status=request_data.get('status',None)
            
            if id is None or id.isspace():
                return SendResponse(code=200,error="id missing",result=None)               
                
            if status is None or status.isspace() :
                
                return SendResponse(code=200,error="status missing",result=None)
            if status not in ["Active","Suspended"]:                
                return SendResponse(code=200,error="Invalid status",result=None)

            registered_user = KriyaVolunteers.query.filter_by(id =id).first()
            if registered_user is not None:
                
                registered_user.status = status
                db.session.commit()
                return SendResponse(code=200,error=None,result={"message":"updated successfully"})                
                
            else:
                return SendResponse(code=200,error="volunteers details not found with given id",result=None)
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@blueprint_admin_dash.route("/send_kriya_volunteer_app",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@admin_login_required
@required_roles(["AD","KM","AC"])
@csrf.exempt
def send_kriya_volunteer_app():
    if request.method=='POST':
        try:
            request_data = request.get_json()
            id=request_data.get('id',None)
            
            
            if id is None or id.isspace():
                return SendResponse(code=200,error="id missing",result=None)              
                
            

            registered_user = KriyaVolunteers.query.filter_by(id =id).first()
            if registered_user is not None:
                app_link = "https://tinyurl.com/mwrmxdds"
                name = registered_user.name
                mobile = registered_user.mobile
                message = """హాయ్ ..జనసేన పార్టీ క్రియాశీలక సభ్యత్వానికి సంబంధించిన ఆండ్రాయిడ్ యాప్ ఈ లింక్ లో ఉంది. డౌన్ లోడ్ చేసుకోగలరు """+str(app_link) + " Helpline Numbers:8106770099,6304900789,6302753644,6302753655"
                numberToSend = "91"+str(mobile)
                sendSMS(numberToSend,message)
                return SendResponse(code=200,error=None,result={"message":"message sent successfully"})                
                
            else:
                return SendResponse(code=200,error="volunteers details not found with given id",result=None)
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@blueprint_admin_dash.route("/kriya_volunteers",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_volunteers():
    return render_template("admin/kriya_membership_dashboard.html")
    
@blueprint_admin_dash.route("/kriya_members",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC"])

def kriya_members():
    
    return render_template("admin/kriya_members_dashboard.html")

@blueprint_admin_dash.route("/kriya_dashboard",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","PK"])

def kriya_members1():
    
    return render_template("admin/kriya_members_dashboard_query_builder.html")


@blueprint_admin_dash.route("/get_kriya_volunteers",methods=['GET','OPTIONS','POST'])
@crossdomain(origin="*",headers="*")
@admin_login_required
@required_roles(["AD","KM","AC","KV"])
@csrf.exempt
def get_kriya_volunteers():
    if request.method == 'GET':
        try:
            
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select kv.id,ktv.completed,ktv.pending,ttv.yesterday as filter_by_date,kv.name,kv.mobile,kv.approved_by,kv.status,kv.state,ac.district_name,ac.constituency_name from kriya_volunteers kv left join kriya_top_volunteers ktv on ktv.added_by =kv.id left join today_top_volunteers ttv on ttv.added_by =kv.id join assembly_constituencies ac on ac.state_id = kv.state and ac.constituency_id = kv.constituency_id order by ac.constituency_id asc")
            volunteers = cur.fetchall()                  
  
            return SendResponse(code=200,error=None,result=volunteers)
                
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
    else:
        try:
            
            # assembly_id = request.values.get('assembly_id')
            # if  assembly_id is None:
            #     return SendResponse(code=400,error="params missing",result=None)
            
            # con = psycopg2.connect(config.DB_CONNECTION)
            # cur = con.cursor(cursor_factory=RealDictCursor)
            # cur.execute("select kv.id,ktv.completed,ktv.pending,ttv.yesterday,kv.name,kv.mobile,kv.approved_by,kv.status,ac.district_name,ac.constituency_name from kriya_volunteers kv left join kriya_top_volunteers ktv on ktv.added_by =kv.id left join today_top_volunteers ttv on ttv.added_by =kv.id join assembly_constituencies ac on ac.state_id = kv.state and ac.constituency_id = kv.constituency_id where ac.state_id=1 and ac.constituency_id =%s ")
            # volunteers = cur.fetchall() 
            post_data = request.get_json()
            from_date = post_data.get('from_date',None)
            to_date = post_data.get('to_date',None)
            if  from_date is None or to_date is  None:
                return SendResponse(code=400,error="params missing",result=None)
            
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select ktv.completed,ktv.pending,kv.id,kv.name,kv.mobile,kv.approved_by,kv.status,kv.state,ac.district_name,ac.constituency_name,km.count as filter_by_date from kriya_volunteers kv left join (select added_by,count(*) as count from kriya_members k join kriya_member_payment_details kmp on kmp.kriya_member_id=k.id where k.payment_status='Success' and date(kmp.created_on) between %s and %s group by k.added_by) as km on km.added_by =kv.id  join assembly_constituencies ac on ac.state_id = kv.state and ac.constituency_id = kv.constituency_id left join kriya_top_volunteers ktv on ktv.added_by =kv.id ",(from_date,to_date))
            volunteers = cur.fetchall()                  
  
            return SendResponse(code=200,error=None,result=volunteers)
                
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

# # get all kriya members for table
# @blueprint_admin_dash.route("/get_kriya_members",methods=['GET','OPTIONS','POST'])
# @crossdomain(origin="*",headers="*")
# @csrf.exempt
# @admin_login_required
# @required_roles(["AD","KM","AC"]) 
# def get_kriya_members():
#         if request.method == 'GET':
#             try:
#                 con = psycopg2.connect(config.DB_CONNECTION)
#                 cur = con.cursor(cursor_factory=RealDictCursor)
                

#                 cur.execute("""select kv.name as added_by,cast(to_char(km.dob ,'DD-MM-YYYY')as text) as dob,km.jsp_id,km.id,km.payment_verified,km.name,km.mobile,km.remarks,km.gender,km.email,km.state,km.district_id,km.constituency_id,km.address,ac.district_name,ac.constituency_name,km.aadhar_number,kmn.nominee_name,
#                     kmn.aadhar_id as nominee_aadhar_number,km.working_with_janasena_from_year_and_month,km.do_you_know_janasena_principles,km.which_principles_are_you_aware_of,km.participated_jsp_activities,km.how_much_time_can_you_spend_for_party_activities,
#                     kmp.payment_mode,kmp.payment_id,cast(to_char(kmp.created_on ,'DD-MM-YYYY') as text
#                 )as payment_date,kmp.payment_url,kmp.payment_status,km.aadhar_card,km.photo from kriya_members km left join kriya_member_payment_details kmp on kmp.kriya_member_id=km.id
#                 left join kriya_member_nominee_details kmn on kmn.kriya_member_id=km.id join kriya_volunteers kv on kv.id=km.added_by join assembly_constituencies ac on ac.state_id=km.state  and ac.constituency_id=km.constituency_id order by kmp.created_on desc limit 7000""")
                
#                 results = []
#                 results = cur.fetchall()              
                
#                 return SendResponse(code=200,error=None,result=results)               
                
#             except Exception as e:
#                 print(str(e))
#                 return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
#         else:
#             try:
#                 con = psycopg2.connect(config.DB_CONNECTION)
#                 cur = con.cursor(cursor_factory=RealDictCursor)
#                 post_data = request.get_json()
#                 print(post_data)
#                 if post_data is not None:
#                     to_date = post_data.get('to_date',None)
#                     from_date = post_data.get('from_date',None)
#                     district_id = post_data.get('district_id',None)
#                     assembly_id = post_data.get("assembly_id",None)
#                 results = []
#                 where_clause = "where "
#                 date_filter = False
#                 district_filter = False
#                 assembly_filter = False
#                 if to_date is not None and from_date is not None:
#                     where_clause = where_clause+ "date(kmp.created_on) between '{}' and '{}' ".format(from_date,to_date)
#                     date_filter= True
#                 if district_id is not None and assembly_id is None :
#                     if date_filter:
#                         where_clause = where_clause + " and ac.state_id=1 ac.district_id = {}".format(district_id)
#                     else:
#                         where_clause = where_clause+" ac.state_id=1 ac.district_id = {}".format(district_id)

#                 if district_id is not None and assembly_id is not None :
#                     if date_filter:
#                         where_clause = where_clause + " and ac.state_id=1 ac.district_id = {} and ac.constituncy_id = {}".format(district_id,assembly_id)
#                     else:
#                         where_clause = where_clause+" ac.state_id=1 ac.district_id = {} and ac.constituency_id= {}".format(district_id,assembly_id)


#                 query = """select kv.name as added_by,cast(to_char(km.dob ,'DD-MM-YYYY')as text) as dob,km.jsp_id,km.id,km.payment_verified,km.name,km.remarks,km.mobile,km.gender,km.email,km.state,km.district_id,km.constituency_id,km.address,ac.district_name,ac.constituency_name,km.aadhar_number,kmn.nominee_name,
#                     kmn.aadhar_id as nominee_aadhar_number,km.working_with_janasena_from_year_and_month,km.do_you_know_janasena_principles,km.which_principles_are_you_aware_of,km.participated_jsp_activities,km.how_much_time_can_you_spend_for_party_activities,kmp.payment_mode,kmp.payment_id,cast(to_char(kmp.created_on ,'DD-MM-YYYY') as text
#                 )as payment_date,kmp.payment_url,kmp.payment_status,km.aadhar_card,km.photo from kriya_members km left join kriya_member_payment_details kmp on kmp.kriya_member_id=km.id
#                 left join kriya_member_nominee_details kmn on kmn.kriya_member_id=km.id join kriya_volunteers kv on kv.id=km.added_by join assembly_constituencies ac on ac.state_id=km.state  and ac.constituency_id=km.constituency_id {} order by kmp.created_on""".format(where_clause)
#                 print(query)
#                 cur.execute(query)         
            
#                 results = cur.fetchall()              
                
#                 return SendResponse(code=200,error=None,result=results)               
                
#             except Exception as e:
#                 print(str(e))
#                 return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@blueprint_admin_dash.route("/get_kriya_members",methods=['GET','OPTIONS','POST'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_kriya_members():
        if request.method == 'GET':
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                

                cur.execute("""select kv.name as added_by,cast(to_char(km.dob ,'DD-MM-YYYY')as text) as dob,km.jsp_id,km.id,km.payment_verified,km.name,km.mobile,km.remarks,km.gender,km.email,km.state,km.district_id,km.constituency_id,km.address,ac.district_name,ac.constituency_name,km.aadhar_number,kmn.nominee_name,
                    kmn.aadhar_id as nominee_aadhar_number,km.working_with_janasena_from_year_and_month,km.do_you_know_janasena_principles,km.which_principles_are_you_aware_of,km.participated_jsp_activities,km.how_much_time_can_you_spend_for_party_activities,
                    kmp.payment_mode,kmp.payment_id,cast(to_char(kmp.created_on ,'DD-MM-YYYY') as text
                )as payment_date,kmp.payment_url,kmp.payment_status,km.aadhar_card,km.photo from kriya_members km left join kriya_member_payment_details kmp on kmp.kriya_member_id=km.id
                left join kriya_member_nominee_details kmn on kmn.kriya_member_id=km.id join kriya_volunteers kv on kv.id=km.added_by join assembly_constituencies ac on ac.state_id=km.state  and ac.constituency_id=km.constituency_id order by km.created_on desc limit 1500""")
                
                results = []
                results = cur.fetchall()              
                
                return SendResponse(code=200,error=None,result=results)               
                
            except Exception as e:
                print(str(e))
                return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
        else:
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                post_data = request.get_json()
                print(post_data)
                if post_data is None or post_data == {}:
                    return SendResponse(code=400,error="Filter query missing",result=None)
                if post_data is not None:
                    to_date = post_data.get('to_date',None)
                    from_date = post_data.get('from_date',None)
                    state = post_data.get("state",None)
                    district_id = post_data.get('district_id',None)
                    assembly_id = post_data.get("assembly_id",None)
                results = []
                where_clause = "where "
                date_filter = False
                district_filter = False
                assembly_filter = False
                if to_date is not None and from_date is not None:
                    where_clause = where_clause+ "date(kmp.created_on) between '{}' and '{}' ".format(from_date,to_date)
                    date_filter= True
                if state is not None:
                    if date_filter:
                        where_clause = where_clause + " and ac.state_id= {}".format(state)
                    else:
                        where_clause = where_clause+" ac.state_id  = {}".format(state)

                if district_id is not None and assembly_id is None :
                    
                        where_clause = where_clause + "  and ac.district_id = {}".format(district_id)
                    

                if  assembly_id is not None :
                    
                        where_clause = where_clause + "  and ac.constituency_id = {}".format(assembly_id)
                    


                query = """select kv.name as added_by,cast(to_char(km.dob ,'DD-MM-YYYY')as text) as dob,km.jsp_id,km.id,km.payment_verified,km.name,km.remarks,km.mobile,km.gender,km.email,km.state,km.district_id,km.constituency_id,km.address,ac.district_name,ac.constituency_name,km.aadhar_number,kmn.nominee_name,
                    kmn.aadhar_id as nominee_aadhar_number,km.working_with_janasena_from_year_and_month,km.do_you_know_janasena_principles,km.which_principles_are_you_aware_of,km.participated_jsp_activities,km.how_much_time_can_you_spend_for_party_activities,kmp.payment_mode,kmp.payment_id,cast(to_char(kmp.created_on ,'DD-MM-YYYY') as text
                )as payment_date,kmp.payment_url,kmp.payment_status,km.aadhar_card,km.photo from kriya_members km left join kriya_member_payment_details kmp on kmp.kriya_member_id=km.id
                left join kriya_member_nominee_details kmn on kmn.kriya_member_id=km.id join kriya_volunteers kv on kv.id=km.added_by join assembly_constituencies ac on ac.state_id=km.state  and ac.constituency_id=km.constituency_id {} order by kmp.created_on""".format(where_clause)
                
                
                cur.execute(query)         
            
                results = cur.fetchall()              
                
                return SendResponse(code=200,error=None,result=results)               
                
            except Exception as e:
                print(str(e))
                return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

 # get kriya member details by id for view /edit
@blueprint_admin_dash.route("/get_kriya_member_details",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@admin_login_required
@required_roles(["AD","KM","AC"])
def get_kriya_member_details():
    
        try:
            id = request.values.get("id",None)
            if id is None:
                return SendResponse(code=400,error="id missing",result=None)  
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("""select kv.name as added_by,km.id,km.name,cast(to_char(km.dob ,'YYYY-MM-DD')as text) as dob ,km.mobile,km.aadhar_card,km.remarks,km.photo,km.gender,km.email,km.state,km.district_id,km.constituency_id,km.address,ac.district_name,ac.constituency_name,km.aadhar_number,kmn.nominee_name,
                kmn.aadhar_id as nominee_aadhar_number,km.working_with_janasena_from_year_and_month,km.do_you_know_janasena_principles,km.which_principles_are_you_aware_of,km.participated_jsp_activities,km.how_much_time_can_you_spend_for_party_activities,kmp.payment_mode,cast(to_char(kmp.created_on AT TIME ZONE 'Asia/Calcutta','DD-mon-YYYY HH:MM AM') as text
)as payment_date,kmp.payment_id,kmp.payment_url,kmp.payment_status from kriya_members km left join kriya_member_payment_details kmp on kmp.kriya_member_id=km.id
            left join kriya_member_nominee_details kmn on kmn.kriya_member_id=km.id join kriya_volunteers kv on kv.id=km.added_by join assembly_constituencies ac on ac.state_id=km.state  and ac.constituency_id=km.constituency_id where km.id=%s""",(id,))
            
            
            results = cur.fetchone()              
            
            return SendResponse(code=200,error=None,result=results)               
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@blueprint_admin_dash.route("/delete_kriya_volunteer_details",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"])
def delete_kriya_volunteer_details():
    
        try:
            id = request.values.get("id",None)
            if id is None:
                return SendResponse(code=400,error="id missing",result=None)  
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("delete from kriya_volunteer_session where kriya_volunteer_id = %s",(id,))            
            cur.execute("delete from kriya_volunteers where id =%s",(id,))
            
            con.commit()                     
            
            return SendResponse(code=200,error=None,result="deleted successfully")               
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

# delete kriya members by id
@blueprint_admin_dash.route("/delete_kriya_member_details",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM"])
def delete_kriya_member_details():
    
        try:
            id = request.values.get("id",None)
            if id is None:
                return SendResponse(code=400,error="id missing",result=None)  
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("delete from kriya_member_payment_details where kriya_member_id=%s",(id,))            
            cur.execute("delete from kriya_member_nominee_details where kriya_member_id=%s",(id,))
            cur.execute("delete from kriya_members where id=%s",(id,)) 
            con.commit()                     
            
            return SendResponse(code=200,error=None,result="deleted successfully")               
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

# update  nominee details by id
@blueprint_admin_dash.route("/update_member_nominee_details",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")

@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"])
def update_member_nominee_details():
    try:
        postdata = request.get_json()  
        if postdata is None :
            return SendResponse(code = 400,error = "post data  missing",result=None)
        id =postdata.get("id",None)
        
        nominee_name = postdata.get("nominee_name",None)
        aadhar_card = postdata.get("nominee_aadhar_number")
        
        if id is None or str(id).isspace():
            return SendResponse(code = 400,error = "id missing",result=None)
        
        if nominee_name is None or nominee_name.isspace():
            return SendResponse(code = 400,error = "nominee name missing",result=None)
        if aadhar_card is None or aadhar_card.isspace():
            return SendResponse(code = 400,error = "nominee aadhar number missing",result=None)
        
        nominee_details = KriyaMemberNomineeDetails.query.filter_by(kriya_member_id=id).first()
        if nominee_details is not None:
            nominee_details.nominee_name = nominee_name
            nominee_details.aadhar_id= aadhar_card
            db.session.commit()            
            
            return SendResponse(code = 200,error = None,result={"message":"nominee details updated"})
        else:   
            return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

#update member details
@blueprint_admin_dash.route("/update_member_details",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"])
def update_member_details():
    try:
        member_details = request.get_json()  
        if member_details is None :
            return SendResponse(code = 400,error = "post data  missing",result=None)
        id =member_details.get("id",None)
        
        
        mobile  =member_details.get('mobile',None)
        name =member_details.get('fullname',None)
        email =member_details.get('email',None)
        state =member_details.get('state',None)
        district_id =member_details.get('district_id',None)
        constituency_id =member_details.get('constituency_id',None)
        # payment_id =member_details.get('payment_id',None)      
        remarks=member_details.get('remarks',None)
        address =member_details.get('address',None)
        gender =member_details.get('gender',None)
        dob =member_details.get('dob',None)
        #education =member_details.get('education',None)
        #occupation =member_details.get('occupation',None)
        aadhar_number =member_details.get('aadhar_number',None)
        
        if mobile is None or mobile.isspace():
            return SendResponse(code = 400,error = "Mobile missing",result=None)
        if dob is None or dob.isspace():
            return SendResponse(code = 400,error = "DOB missing",result=None)
        if name is None or name.isspace():
            return SendResponse(code = 400,error = "name  missing",result=None)
        if email is None or email.isspace():
            return SendResponse(code = 400,error = "email  missing",result=None)
        if state is None or state.isspace():
            return SendResponse(code = 400,error = "state missing",result=None)

        if district_id is None or district_id.isspace():
            return SendResponse(code = 400,error = "district_id missing",result=None)
        if constituency_id is None or constituency_id.isspace():
            return SendResponse(code = 400,error = "constituency_id missing",result=None)
        if address is None or address.isspace():
            return SendResponse(code = 400,error = "address missing",result=None)

        if gender is None or gender.isspace():
            return SendResponse(code = 400,error = "gender missing",result=None)
        if aadhar_number is None or aadhar_number.isspace():
            return SendResponse(code = 400,error = "constituency_id missing",result=None)
        # if payment_id is None or payment_id.isspace():
        #     return SendResponse(code = 400,error = "payment_id missing",result=None)
        
            
        member_details = KriyaMembers.query.filter_by(id=id).first()
        if member_details is not None:
            member_details.name=name
            member_details.mobile= mobile
            member_details.email = email
            member_details.state =state
            member_details.district_id =district_id
            member_details.constituency_id =constituency_id
            member_details.address =address
            member_details.gender =gender
            member_details.aadhar_number =aadhar_number
            member_details.dob = dob
            member_details.remarks=remarks

            
            db.session.commit()
            return SendResponse(code = 200,error = None,result={"message":"member details updated"})

        else:   
            return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@blueprint_admin_dash.route("/kriya_reports_assembly_wise",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report():
    return render_template("admin/kriya_members_report.html")
    
@blueprint_admin_dash.route("/get_assembly_wise_counts",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_counts():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)       

        cur.execute(""" select km.state,km.constituency_id,ac.district_name,ac.constituency_name,count(*),sum(case when date(kmp.created_on)=current_date then 1 else 0 end) as today from kriya_members km join assembly_constituencies ac on km.state=ac.state_id and ac.constituency_id = km.constituency_id join kriya_member_payment_details kmp on km.id=kmp.kriya_member_id where km.payment_status='Success' and state='1' and ac.constituency_id in (4,18,21,22,23,24,35,42,47,50,58,56,57,79,80,81,94,95,108,112,117,121,167,168,126,127,137,144,160,161,157) group by ac.district_name,ac.constituency_name,km.state, km.constituency_id  order by count desc  """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

# Telangana Reports

@blueprint_admin_dash.route("/kriya_reports_assembly_wise_telangana",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_telangana():
    return render_template("admin/kriya_members_report_telangana.html")
    
@blueprint_admin_dash.route("/get_assembly_wise_counts_telangana",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_counts_telangana():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)       

        cur.execute(""" select km.state,km.constituency_id,ac.district_name,ac.constituency_name,count(*),sum(case when date(kmp.created_on)=current_date then 1 else 0 end) as today from kriya_members km join assembly_constituencies ac on km.state=ac.state_id and ac.constituency_id = km.constituency_id join kriya_member_payment_details kmp on km.id=kmp.kriya_member_id where km.payment_status='Success' and state='2' group by ac.district_name,ac.constituency_name,km.state, km.constituency_id  order by count desc  """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@blueprint_admin_dash.route("/telangana_kriya_reports_assembly_wise_top_five",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_five_telangana():
    return render_template("admin/kriya_members_assembly_wise_top_five_telangana.html")


@blueprint_admin_dash.route("/get_assembly_wise_top_five_telangana",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_top_five_telangana():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute(""" SELECT district_name, constituency_name, name,mobile,completed  FROM  (SELECT ac.district_name, ac.constituency_name, kv.name,kv.mobile,ktv.completed, ROW_NUMBER() OVER (PARTITION BY ac.constituency_name ORDER BY ktv.completed DESC) as assembly_rank  FROM kriya_volunteers kv join kriya_top_volunteers ktv on ktv.added_by=kv.id join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id=kv.constituency_id where state='2') ranked WHERE assembly_rank <= 5  order by district_name asc,constituency_name asc,completed desc """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@blueprint_admin_dash.route("/kriya_reports_state_wise_top_five_telangana",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_state_five_telangana():
    return render_template("admin/kriya_members_state_wise_top_five_telangana.html")



@blueprint_admin_dash.route("/get_state_wise_top_five_telangana",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_state_wise_top_five_telangana():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute("""select kv.name,ac.district_name,ac.constituency_name,kv.mobile,ktp.completed,ktp.pending from kriya_volunteers kv join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id =kv.constituency_id  join kriya_top_volunteers ktp on ktp.added_by=kv.id where state='2' order by ktp.completed desc limit 5 """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
        















@blueprint_admin_dash.route("/kriya_reports_assembly_wise_top_five",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_five():
    return render_template("admin/kriya_members_assembly_wise_top_five.html")


@blueprint_admin_dash.route("/get_assembly_wise_top_five",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_top_five():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute(""" SELECT district_name, constituency_name, name,mobile,completed  FROM  (SELECT ac.district_name, ac.constituency_name, kv.name,kv.mobile,ktv.completed, ROW_NUMBER() OVER (PARTITION BY ac.constituency_name ORDER BY ktv.completed DESC) as assembly_rank  FROM kriya_volunteers kv join kriya_top_volunteers ktv on ktv.added_by=kv.id join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id=kv.constituency_id where  ac.constituency_id in (4,18,21,22,23,24,35,42,47,50,58,56,57,79,80,81,94,95,108,112,117,121,167,168,126,127,137,144,160,161,157)) ranked WHERE assembly_rank <= 5  order by district_name asc,constituency_name asc,completed desc """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@blueprint_admin_dash.route("/kriya_reports_state_wise_top_five",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_state_five():
    return render_template("admin/kriya_members_state_wise_top_five.html")



@blueprint_admin_dash.route("/get_state_wise_top_five",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
# @admin_login_required
# @required_roles(["AD","KM","AC"]) 
def get_state_wise_top_five():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute("""select kv.name,ac.district_name,ac.constituency_name,kv.mobile,ktp.completed,ktp.pending from kriya_volunteers kv join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id =kv.constituency_id  join kriya_top_volunteers ktp on ktp.added_by=kv.id where ac.constituency_id in (4,18,21,22,23,24,35,42,47,50,58,56,57,79,80,81,94,95,108,112,117,121,167,168,126,127,137,144,160,161,157) order by ktp.completed desc limit 5 """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
        






## 3rd Phase Reports API

@blueprint_admin_dash.route("/kriya_reports_assembly_wise_phase_3",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_phase_3():
    return render_template("admin/kriya_members_report_phase_3.html")
    
@blueprint_admin_dash.route("/get_assembly_wise_counts_phase_3",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_counts_phase_3():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)       

        cur.execute(""" select km.state,km.constituency_id,ac.district_name,ac.constituency_name,count(*),sum(case when date(kmp.created_on)=current_date then 1 else 0 end) as today from kriya_members km join assembly_constituencies ac on km.state=ac.state_id and ac.constituency_id = km.constituency_id join kriya_member_payment_details kmp on km.id=kmp.kriya_member_id where km.payment_status='Success' and state='1' and ac.constituency_id in (3,6,7,14,17,20,25,26,30,31,32,34,37,38,39,41,43,44,48,49,51,52,55,59,61,62,65,68,72,74,75,76,78,82,88,89,90,91,93,94,85,97,102,103,104,114,115,120,122,125,130,133,138,139,142,148,152,158,159,163,164,171,175,169)  group by ac.district_name,ac.constituency_name,km.state, km.constituency_id order by count desc """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)




@blueprint_admin_dash.route("/kriya_reports_assembly_wise_top_five_phase_3",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_five_phase_3():
    return render_template("admin/kriya_members_assembly_wise_top_five_phase_3.html")


@blueprint_admin_dash.route("/get_assembly_wise_top_five_phase_3",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"]) 
def get_assembly_wise_top_five_phase_3():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute(""" SELECT district_name, constituency_name, name,mobile,completed  FROM  (SELECT ac.district_name, ac.constituency_name, kv.name,kv.mobile,ktv.completed, ROW_NUMBER() OVER (PARTITION BY ac.constituency_name ORDER BY ktv.completed DESC) as assembly_rank  FROM kriya_volunteers kv join kriya_top_volunteers ktv on ktv.added_by=kv.id join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id=kv.constituency_id where state='1' and ac.constituency_id in (3,6,7,14,17,20,25,26,30,31,32,34,37,38,39,41,43,44,48,49,51,52,55,59,61,62,65,68,72,74,75,76,78,82,88,89,90,91,93,94,85,97,102,103,104,114,115,120,122,125,130,133,138,139,142,148,152,158,159,163,164,171,175,169)) ranked WHERE assembly_rank <= 5  order by district_name asc,constituency_name asc,completed desc """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@blueprint_admin_dash.route("/kriya_reports_state_wise_top_five_phase_3",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","KM","AC","KV"])

def kriya_members_report_top_state_five_phase_3():
    return render_template("admin/kriya_members_state_wise_top_five_phase_3.html")



@blueprint_admin_dash.route("/get_state_wise_top_five_phase_3",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
# @admin_login_required
# @required_roles(["AD","KM","AC"]) 
def get_state_wise_top_five_phase_3():
        
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        

        cur.execute("""select kv.name,ac.district_name,ac.constituency_name,kv.mobile,ktp.completed,ktp.pending from kriya_volunteers kv join assembly_constituencies ac on ac.state_id=kv.state and ac.constituency_id =kv.constituency_id  join kriya_top_volunteers ktp on ktp.added_by=kv.id where ac.constituency_id in (3,6,7,14,17,20,25,26,30,31,32,34,37,38,39,41,43,44,48,49,51,52,55,59,61,62,65,68,72,74,75,76,78,82,88,89,90,91,93,94,85,97,102,103,104,114,115,120,122,125,130,133,138,139,142,148,152,158,159,163,164,171,175,169) order by ktp.completed desc limit 5 """)
        
        results = []
        results = cur.fetchall()              
        
        return SendResponse(code=200,error=None,result=results)               
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)





















@blueprint_admin_dash.route("/map_kriya_member_upi_transactions",methods=['GET','OPTIONS'])
@admin_login_required
@required_roles(["AD","AC"])

def kriya_members_bank_status_check():
    return render_template("admin/kriya_member_bank_status_check.html")



import csv
import tempfile
import os
import pandas as pd
@blueprint_admin_dash.route('/map_kriya_member_payment_status', methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
#@admin_login_required
#@required_roles(["AD", "AC"])
def map_kriya_member_payment_status():

    try:
        input_file = request.files['file']        
        data = pd.read_csv(input_file)     
        upi_ids = [item for sublist in data.values.tolist() for item in sublist]
         
        total = len(upi_ids)
        count,updated_data =update_upi_payments_verification_status(upi_ids)
        if len(updated_data)>=1:
            
            print(len(updated_data))
            handle, filepath = tempfile.mkstemp(suffix='.csv')
            
            with open(filepath, 'w') as csvfile:
                
                fieldnames = ['upi', 'id','name','mobile']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                
                writer.writeheader()
                
                writer.writerows(updated_data)
            
            import time
            millis =time.strftime("%Y%m%d-%H%M%S")
            
            read_file = open(filepath, 'r')
            
            filename = str(millis) +".csv"
            url = upload_file(read_file, 'kriya-member-upi-transcations-mapping', filename)
            
            return SendResponse(code=200,error=None,result={'url':url,"total":total,"mapped":count})
        return SendResponse(code=200,error="no upi id matched",result=None)
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error=str(e),result=None)



            

def update_upi_payments_verification_status(upi_ids):
    try:
        print (upi_ids)
        updated_data = []
        count = 0
        for id in upi_ids:
           
            d={}
            if len(str(id))<12:
                id = str(id).zfill(12)
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("""select kmp.id,kmp.kriya_member_id,km.name,km.mobile from kriya_member_payment_details kmp join kriya_members km on km.id=kmp.kriya_member_id where kmp.payment_id=%s""",(str(id),))
            row=cur.fetchone()
            d['upi']=id
            if row is not None:
                count +=1
                cur.execute("update kriya_members set payment_verified='Yes' where id=%s",(row["kriya_member_id"],))
                con.commit()
                d['id'] =row["kriya_member_id"]
                
                d['name'] =row["name"]
                
                    
                d["mobile"] = row["mobile"]
                
            else:
                d['id'] ="NA"
                d['name'] = "NA"
                d["mobile"] ="NA"

            updated_data.append(d)
        return count,updated_data
    except Exception as e:
        print(str(e))
        return count, []








@blueprint_admin_dash.route("/update_kriya_member_payment_details",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
@admin_login_required
@required_roles(["AD","KM","AC"])
def update_kriya_member_payment_details():
    try:
        kriya_member_payment_details = request.get_json()  
        if kriya_member_payment_details is None :
            return SendResponse(code = 400,error = "post data  missing",result=None)
        id =kriya_member_payment_details.get("id",None)
        payment_id =kriya_member_payment_details.get('payment_id',None)      
        if payment_id is None or payment_id.isspace():
            return SendResponse(code = 400,error = "payment_id missing",result=None)
           
        kriya_member_payment_details = KriyaMemberpaymentDetails.query.filter_by(kriya_member_id=id).first()
        if kriya_member_payment_details is not None:
            kriya_member_payment_details.payment_id=payment_id
            
            db.session.commit()
            return SendResponse(code = 200,error = None,result={"message":"member details updated"})

        else:   
            return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@blueprint_admin_dash.route("/generate_membership_id_by_constituency",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
#@admin_login_required
#@required_roles(["AD","KM","AC"])
def generate_membership_id_by_constituency():
    try:
        constituency_id = request.values.get("constituency_id")  
        state =   request.values.get("state") 
        if constituency_id is None or constituency_id.isspace():
            return SendResponse(code = 400,error = "constituncy_id missing",result=None)
        if state is None or state.isspace():
            return SendResponse(code = 400,error = "state missing",result=None)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        current_max_id = 0
        cur.execute("select max(jsp_id) as max_id from kriya_members where state=%s and constituency_id=%s",(state,constituency_id,))
        max_row = cur.fetchone()
        print(max_row)
        if max_row['max_id'] is not None:
                current_max_id = str(max_row['max_id']).split("-")[1]
        else:           
            
                current_max_id= 0

        cur.execute("select id,mobile,jsp_id from kriya_members where state=%s and  constituency_id=%s and jsp_id is null and payment_verified='Yes' order by created_on asc",(state,constituency_id,))   
        rows = cur.fetchall()
        three_digits_constituency_id = str(constituency_id).zfill(3)
        if len(rows):
            for row in rows:
                member_id = row['id']
                current_max_id = int(current_max_id) +1
                if str(state) == '2':
                    jsp_id = "T"+str(three_digits_constituency_id)+"-"+str(current_max_id).zfill(5)
                else:
                    jsp_id = str(three_digits_constituency_id)+"-"+str(current_max_id).zfill(5)
                print("{} - {}".format(member_id,jsp_id))
                cur.execute("update kriya_members set jsp_id=%s where id=%s",(jsp_id,member_id))
                con.commit()

            return SendResponse(code = 200,error =None,result="generated successfully")
        else:
            return SendResponse(code = 200,error ="No Members found without membership id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)




@blueprint_admin_dash.route("/searchActiveMember", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @ip_validation_required
# @required_roles(["AD","KS"])
@csrf.exempt
def searchActiveMember():
    if request.method == 'GET':
        return render_template("admin/kriya_members_search_by_jspid.html")

    else:

        try:

            m_type = request.values.get("type")
            value = request.values.get("value")
            if m_type is None or m_type.isspace():
                return json.dumps({"errors": ["parameter type missing.please try again"], "data": {"status": "0"}})
            if value is None or value.isspace():
                return json.dumps({"errors": ["parameter value missing .please try again"], "data": {"status": "0"}})
            children = {}
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            rows = []
            if m_type == 'jsp_id':
                jsp_id = str(value)

                
                query = "select jm.id,jm.jsp_id,jm.mobile,jm.name,jm.aadhar_number,ac.district_name,ac.constituency_name,jm.photo,jm.working_with_janasena_from_year_and_month,jm.which_principles_are_you_aware_of,cast(to_char(jm.dob ,'YYYY-MM-DD')as text) as dob,jm.gender,jm.address,jm.email from kriya_members jm join assembly_constituencies ac on ac.state_id=jm.state and ac.constituency_id=jm.constituency_id  where jm.jsp_id=%s"
            elif m_type=="mobile":
                mobile= str(value)
                query = "select jm.id,jm.jsp_id,jm.mobile,jm.name,jm.aadhar_number,ac.district_name,ac.constituency_name,jm.photo,jm.working_with_janasena_from_year_and_month,jm.which_principles_are_you_aware_of,cast(to_char(jm.dob ,'YYYY-MM-DD')as text) as dob,jm.gender,jm.address,jm.email from kriya_members jm join assembly_constituencies ac on ac.state_id=jm.state and ac.constituency_id=jm.constituency_id  where jm.mobile=%s"
                
            elif m_type=="aadhar":
                aadhar_number = str(value)
                query = "select jm.id,jm.jsp_id,jm.mobile,jm.name,jm.aadhar_number,ac.district_name,ac.constituency_name,jm.photo,jm.working_with_janasena_from_year_and_month,jm.which_principles_are_you_aware_of,cast(to_char(jm.dob ,'YYYY-MM-DD')as text) as dob,jm.gender,jm.address,jm.email from kriya_members jm join assembly_constituencies ac on ac.state_id=jm.state and ac.constituency_id=jm.constituency_id  where jm.aadhar_number=%s"
            else:
                query= None
            if query is not None:
                cur.execute(query,(value,))
                row = cur.fetchone()
                if row is not None:

                    return json.dumps({"errors": [], 'data': {'status': '1', 'children': row}})

            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_admin_dash.route("/kriya_members_reports", methods=['POST','GET'])
# @crossdomain(origin="*", headers="Content-Type")
# @ip_validation_required
#@admin_login_required
#@required_roles(["AD"])
@csrf.exempt
def kriya_members_reports():
    if request.method == 'GET':
        return render_template("admin/kriya_members_district_wise_report.html")
    if request.method=='POST':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("""select count(*) as total,count(case when lower(gender)='male' then 1 end) as male_cnt,
count(case when lower(km.gender)='female' then 1 end) as female_cnt,
count(case when EXTRACT(YEAR FROM age(cast(dob as date))) between 0 and 25 then 1 end) as age_upto_25,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 26 and 30 then 1 end) as age_26_to_30,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 31 and 40 then 1 end)as age_31_40 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) between 41 and 45 then 1 end)as age_41_45 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) >45 then 1 end) as age_above_45 from kriya_members km; """)
            state_wise_report =cur.fetchone()
            cur.execute("""select ac.district_id,ac.district_name,count(*) as total,count(case when lower(gender)='male' then 1 end) as male_cnt,
count(case when lower(km.gender)='female' then 1 end) as female_cnt,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 0 and 25 then 1 end) as age_upto_25,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 26 and 30 then 1 end) as age_26_to_30,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 31 and 40 then 1 end)as age_31_40 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) between 41 and 45 then 1 end)as age_41_45 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) >45 then 1 end) as age_above_45 from kriya_members km
join assembly_constituencies ac on ac.state_id=km.state and
 ac.constituency_id=km.constituency_id
 group by ac.district_id,ac.district_name """)
            district_wise_report = cur.fetchall()
            cur.execute("""select ac.constituency_id,ac.district_id,ac.district_name,ac.constituency_name,count(*) as total,count(case when lower(gender)='male' then 1 end) as male_cnt,
count(case when lower(km.gender)='female' then 1 end) as female_cnt,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 0 and 25 then 1 end) as age_upto_25,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 26 and 30 then 1 end) as age_26_to_30,
count(case when  EXTRACT(YEAR FROM age(cast(dob as date))) between 31 and 40 then 1 end)as age_31_40 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) between 41 and 45 then 1 end)as age_41_45 ,
count (case when EXTRACT(YEAR FROM age(cast(dob as date))) >45 then 1 end) as age_above_45 from kriya_members km
join assembly_constituencies ac on ac.state_id=km.state and
 ac.constituency_id=km.constituency_id
 group by ac.constituency_id,ac.district_id,ac.district_name,ac.constituency_name
 order by ac.constituency_id """)
            assembly_wise_reports = cur.fetchall()

            data = {}
            data['state']= state_wise_report
            data['district'] =  district_wise_report
            data["assembly"] = assembly_wise_reports      
        
        
       
            return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
        except Exception as e:
            print(str(e))
            return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})

        

    



#update member idcard,certificate pdf file url details
@blueprint_admin_dash.route("/update_member_card_details",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
def update_member_card_details():
    try:
        jsp_id = request.json.get("jsp_id")
        url = request.json.get("shorturl")
        if jsp_id is None or url is None:
            return json.dumps({"errors": ['values missing'], 'data': {'status': 0}})
        print(jsp_id,'----',url)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("update kriya_members set membership_cards_file_url = %s where jsp_id=%s",(url,jsp_id))
        con.commit()
        return json.dumps({"errors": [], 'data': {'status': 1}})

    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})

        


@blueprint_admin_dash.route('/admin/getDonationFilters', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def getDonationFilters():
    try:
        filters = [ 
        {
            "id": 'state',
            "label": 'State',
            "type": 'integer',
            "input": 'select',
            "values": {
              1: 'Andhra Pradesh',
              2: 'Telangana',
             
            },
            "filter_name":"state",
            "has_dependent_filters":True,
            "dependent_filters_props":{
              "district_level_filter":True,
              "assembly_level_filter":True,
              "district_column_name":"district_id",
              "assembly_column_name":"constituency_id"
            },
    
    "operators": ['equal']
  },
        {
            "id": 'name',
            "label": 'Donor Name',
            "type": 'string'
        
        },
        {
            "id": 'phone',
            "label": 'Phone Number',
            "type": 'string'
        
        },
        {
            "id": 'pancard',
            "label": 'PanCard',
            "type": 'string'
        
        },
        {
            "id": 'email',
            "label": 'Email',
            "type": 'string'
        
        },
        {
            "id": 'transaction_id',
            "label": 'Transaction ID',
            "type": 'string'
        
        },
        {
            "id": 'payment_status',
            "label": 'Donation Status',
            "type": 'string',
            "input": 'radio',
            "values": {
              'S': 'Success',
              'I': 'Pending',
              'F':'Failed'
            },
            "operators": ['equal','not_equal']
          },
        {
            "id": 'update_dttm',
            "label": 'Donation Date',
            "type": 'date',
            "validation": {
              "format": 'YYYY-MM-DD'
            },
            "plugin": 'datepicker',
            "plugin_config": {
              "format": 'yyyy-mm-dd',
              "todayBtn": 'linked',
              "todayHighlight": True,
              "autoclose": True
            }
          },

       ]
        rules_basic = {
         "condition": 'AND',
         "rules": [{
    "id": 'state',
    "label": 'State',
    "type": 'integer',
    "input": 'select',
    "values": {
      1: 'Andhra Pradesh',
      2: 'Telangana',
     
    },
    "filter_name":"state",
    "has_dependent_filters":True,
    "dependent_filters_props":{
      "district_level_filter":True,
      "assembly_level_filter":True,
      "district_column_name":"district_id",
      "assembly_column_name":"constituency_id"
    },
    
    "operators": ['equal']
  }]
        }

        return json.dumps({"errors": [], 'data': {'status': 1,"filters":filters,"rules_basic":rules_basic}})
    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


from sqlalchemy_filters import apply_filters
from sqlalchemy import desc

@blueprint_admin_dash.route('/admin/getDonationsbyquery', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC"])
def getDonationsbyquery():
    try:
        rules = request.json.get('rules',[])
        page_number = request.json.get('page_number',0)
        page_size = request.json.get('page_size',10)
        offset = page_size * page_number
        query = db.session.query(Donations)
        filtered_query = apply_filters(query, rules)
        count = filtered_query.count()
        filtered_query = filtered_query.order_by(desc(Donations.seq_id))
        filtered_query = filtered_query.limit(page_size).offset(offset) 
        print(filtered_query)  
        rows =filtered_query.all() 
        data = [] 
        print(rules)
        print(rows)
        # print(Donations.UI_Columns)
        UI_Columns = Donations.UI_Columns
        for row in rows:   
            row = row.__dict__        
            row_dict = {k:v for k,v in row.items() if k in UI_Columns}
            
            row_dict['update_dttm'] = "" #str(row['update_dttm']).split(" ")[0]

            data.append(row_dict)
        
        return json.dumps({"errors": [], "data": {"status": "1", "children": data, "total_amount": 99999,
                                                  "today_amount": 0}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})




@blueprint_admin_dash.route('/admin/kriyaMembersFilters', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC","PK"])
def kriyaMembersFilters():
    try:
        filters = [ 
        {
            "id": 'state',
            "label": 'State',
            "type": 'integer',
            "input": 'select',
            "values": {
              1: 'Andhra Pradesh',
              2: 'Telangana',
             
            },
            "filter_name":"state",
            "has_dependent_filters":True,
            "dependent_filters_props":{
              "district_level_filter":True,
              "assembly_level_filter":True,
              "district_column_name":"district_id",
              "assembly_column_name":"constituency_id"
            },
    
            "operators": ['equal']
        },
        {
            "id": 'name',
            "label": 'Member Name',
            "type": 'string',
            "operators":["equal"]
        
        },
        {
            "id": 'mobile',
            "label": 'Phone Number',
            "type": 'string',
            "operators":["equal"]
        
        },
        {
            "id": 'aadhar_number',
            "label": 'Aadhar Number',
            "type": 'string',
            "operators":["equal"]
        
        },
        {
            "id": 'added_by',
            "label": 'Volunteer',
            "type": 'string',
            "operators":["equal"]
        
        },
        {
            "id": 'payment_verified',
            "label": 'Payment Verification',
            "type": 'string',
            "input":"radio",
            "operators":["equal","not_equal"],
            "values":[{"Yes":"Completed"},{"No":"Not Completed"}]
        
        },
        {
            "id": 'payment_status',
            "label": 'Payment Status',
            "type": 'string',
            "input":"radio",
            "operators":["equal","not_equal"],
            "values":[{"Success":"Success"},{"Pending":"Pending"}]
        
        },
        {
            "id": 'gender',
            "label": 'Gender',
            "type": 'string',
            "input":"radio",
            "operators":["equal","not_equal"],
            "values":[{"Male":"Male"},{"Female":"Female"}]
        
        },
        {
            "id": 'age',
            "label": 'Age',
            "type": 'integer',
            "input":"text",
            "operators":["equal","between","less","greater","less_or_equal","greater_or_equal"]
           
        
        },
        
         {
            "id": 'jsp_id',
            "label": 'JSP ID',
            "type": 'string',
            "operators":["equal"]
        
        },
        
        {
            "id": 'payment_date',
            "label": 'Creation Date',
            "type": 'date',
            "validation": {
              "format": 'YYYY-MM-DD'
            },
            "plugin": 'datepicker',
            "plugin_config": {
              "format": 'yyyy-mm-dd',
              "todayBtn": 'linked',
              "todayHighlight": True,
              "autoclose": True
            },
            "operators":["between","equal","less_or_equal","greater","less","greater_or_equal"]
          },

       ]
        rules_basic = {
         "condition": 'AND',
         "rules": [{
            "id": 'mobile',
            "label": 'Mobile Number',
            "type": 'string',
            "input": 'text',          
           
            "operators": ['equal']
        }]
        }

        return json.dumps({"errors": [], 'data': {'status': 1,"filters":filters,"rules_basic":rules_basic}})
    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


@blueprint_admin_dash.route('/admin/getTablemetadata/<report_id>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])
@csrf.exempt
def get_table_metadata(report_id):
    try:
        if report_id is not None:
            metadata = getTableMetadata(report_id)
            return json.dumps({"errors": [], 'data': {'status': 1,"metadata":metadata}})
        return json.dumps({"errors": ['report_id missing'], 'data': {'status': 0}})
    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})



import datetime
@blueprint_admin_dash.route('/admin/getDatabyquery/<report_id>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC","PK"])
@csrf.exempt
def getDatabyquery(report_id):
    try:
        print("request comes in")
        data = json.loads(request.get_data())
        print(data)
        if data is not None:
            rules = data.get('rules',[])            
            
            length =data.get('length',10)                   
            page = data.get('page',1)   
            if page is None :
                page =1
        else:
            
            page = 1
            length = 10

        offset = (page-1) * length     
        db_model = getDBModel(report_id)       
        query = db.session.query(db_model)
        filtered_query = apply_filters(query, rules)
        recordsFiltered = filtered_query.count()
       
        recordsTotal = recordsFiltered
        print(recordsTotal)
        filtered_query = filtered_query.order_by(desc(db_model.id))
        pagination = filtered_query.limit(length).offset(offset)
        
        rows = pagination.all() 
        print(len(rows))
        data = []       
        
        UI_Columns = db_model.UI_Columns
        for row in rows:   
            row = row.__dict__   
            d={}
            for k,v in row.items():
                
                if k in UI_Columns :
                    try:
                        if isinstance(v, (float, int, str, list, dict, tuple)):
                            d[k] = v
                        elif isinstance(v,(datetime.datetime,datetime.date)):
                            d[k] = v.strftime("%d-%m-%Y")
                        else:                        
                            if v is not None:
                                d[k] = v
                            else:
                                d[k] = " "
                    except Exception as ex:
                        print(ex,k,v)
                
            data.append(d)
        end = offset+length
        if (offset+length) <= recordsTotal:
            end= offset+length
        else:
            end = recordsTotal
        res = {'recordsTotal' :recordsTotal,'data': data,"start":"{:,}".format(offset+1),"end":"{:,}".format(end),"recordsTotalStr":"{:,}".format(recordsTotal)}
        
        return json.dumps({"errors": [], "data": {"status": "1", "children": res}})

    except  Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


    

@blueprint_admin_dash.route('/admin/exportReport', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", "AC","PK"])
@csrf.exempt
def exportReport():
    try:
        filters = request.json.get('rules',[])
        report_id = request.json.get('report_id',None)
        name= current_user.fname
        email = current_user.email
        if report_id is None:
            return json.dumps({"errors": ['report_id missing'], 'data': {'status': 0}})
        
        if config.ENVIRONMENT_VARIABLE != 'local':
            
            sendExportResultsToEmail.delay(name,email,filters,report_id)
        else:    
            
            sendExportResultsToEmail(name,email,filters,report_id)
        return json.dumps({"errors": [], "data": {"status": "1","message":"ecport data will be sent to your EmailID shortly"}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})

def getDBModel(report_id):
    return KriyaMembersDashboard


def getTableMetadata(report_id):
    db_model = getDBModel(report_id)
    return db_model.table_metadata
