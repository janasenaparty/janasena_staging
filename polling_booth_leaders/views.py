from flask import Blueprint, request, json, render_template

from appholder import csrf
from constants import empty_check
from utilities.decorators import crossdomain
from utilities.others import give_cursor_json

pbl = Blueprint('pbl', __name__)

@pbl.route('/pbl/dashboard', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def dashboard():
    return render_template('pbl/index.html')

@pbl.route('/pbl/get_pb_list', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_pb_list():
    print request.values,request.method
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            district = request.values.get('district')
            assembly = request.values.get('assembly')
            # state_id = request.values.get('state_id',1)
            if district in empty_check:
                return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})
            if assembly in empty_check:
                return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})
            # if state_id in empty_check:
            #     return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})
            cur.execute("""
            select distinct on (polling_station_no) polling_station_no,polling_station_name,polling_station_address
            from polling_station_details
            where district_id = (select seq_id from districts where district_name=%s) and constituency_id=%s and section_no=1 
            order by polling_station_no
            """,(district,assembly))

            rows = cur.fetchall()


            return json.dumps({'errors': [], 'status': 1, 'data': rows})

    return give_cursor_json(temp)



@pbl.route('/pbl/fetch_leaders', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def fetch_leaders():
    def temp(con, cur):
        if request.method == 'POST':
            district_id = request.values.get('district_id')
            assembly_id = request.values.get('assembly_id')
            polling_booth_id = request.values.get('polling_booth_id')
            print request.values
            cur.execute("""
                select * 
                from polling_booth_leader
                where district_id=(select seq_id from districts where district_name=%s) and assembly_id=%s and polling_booth_no=%s
            """,(district_id,assembly_id,polling_booth_id))
            rows = cur.fetchall()
            return json.dumps({'errors':[],'status':1,'data':rows})

    return give_cursor_json(temp)

@pbl.route('/pbl/check_voter_id', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def check_voter_id():
    def temp(con, cur):
        if request.method == 'POST':
            voter_id = request.values.get('voter_id')
            polling_station_id = request.values.get('polling_station_id')
            ac_no = request.values.get('ac_no')
            if voter_id in empty_check:
                return json.dumps({'errors':['voter_id missing'],'status':0})
            if polling_station_id in empty_check:
                return json.dumps({'errors':['polling_station_id missing'],'status':0})
            if ac_no in empty_check:
                return json.dumps({'errors':['ac_no missing'],'status':0})

            cur.execute("""
                            select voter_id 
                            from polling_booth_leader
                            where voter_id=%s

                        """, (voter_id,))
            row = cur.fetchone()
            if row:
                return json.dumps({'errors': ['ALREADY REGISTERED'], 'status': 0})
            cur.execute("""
                           select voter_name
                           from voters
                           where voter_id=%s
                       """, (voter_id,  ))

            row = cur.fetchone()
            if not row:
                cur.execute("""
                                                           select voter_name
                                                           from voters
                                                           where voter_id=%s and part_no=%s and ac_no=%s
                                                       """, (voter_id, polling_station_id,ac_no))

                r = cur.fetchone()
                if not r:
                    return json.dumps({'errors': [], 'status': 3})
                return json.dumps({'errors':[],'status':2})
            else:
                cur.execute("""
                                           select voter_name
                                           from voters
                                           where voter_id=%s and part_no=%s
                                       """, (voter_id, polling_station_id))

                r = cur.fetchone()
                if not r:
                    return json.dumps({'errors': [], 'status': 3})
                data = dict()
                data['name'] = row[0]
                return json.dumps({'errors':[],'status':1,'user_data':data})


    return give_cursor_json(temp)




@pbl.route('/pbl/add_leader', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def add_leader():
    def temp(con, cur):
        if request.method == 'POST':
            district = request.values.get('district')
            assembly = request.values.get('assembly')
            polling_booth_id = request.values.get('pb_id')
            user_name = request.values.get('user_name')
            mobile = request.values.get('mobile')
            jsp_id = request.values.get('jsp_id')
            village = request.values.get('village')
            voter_id = request.values.get('voter_id')
            if district in empty_check:
                return json.dumps({'error':['district'],'status':0})
            if assembly in empty_check:
                return json.dumps({'error':['assembly'],'status':0})
            if polling_booth_id in empty_check:
                return json.dumps({'error':['polling_booth_id'],'status':0})
            if user_name in empty_check:
                return json.dumps({'error':['user_name'],'status':0})
            if mobile in empty_check:
                return json.dumps({'error':['mobile'],'status':0})
            if village in empty_check:
                return json.dumps({'error':['village'],'status':0})
            if voter_id in empty_check:
                return json.dumps({'error':['voter_id'],'status':0})
            cur.execute("""
                select seq_id from districts where district_name=%s
            """,(district,)
                        )
            district_id = cur.fetchone()[0]

            cur.execute("""
                select voter_id 
                from polling_booth_leader
                where voter_id=%s
            
            """,(voter_id,))
            row = cur.fetchone()
            if row:
                return json.dumps({'errors':['ALREADY REGISTERED'],'status':0})
            else:
                cur.execute("""
                    insert into polling_booth_leader(lname,village,voter_id,jspid,polling_booth_no,district_id,assembly_id,mobile)
                    values(%s,%s,%s,%s,%s,%s,%s,%s)
                """,(user_name,village,voter_id,jsp_id,polling_booth_id,district_id,assembly,mobile))
                con.commit()
            return json.dumps({'errors': [], 'status': 1, 'data': ''})

    return give_cursor_json(temp)