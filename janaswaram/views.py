import psycopg2

from azure.storage.table import TablePayloadFormat, TableBatch
from flask import Blueprint, request, json, render_template
from flask_login import current_user
from psycopg2.extras import RealDictCursor

import config
from appholder import table_service, csrf
from utilities.decorators import crossdomain, admin_login_required

blueprint_janaswaram    = Blueprint('janaswaram', __name__)



@blueprint_janaswaram.route('/getReviewCommitteeData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getReviewCommitteeData():
    next_marker = request.values.get('next_marker')
    if next_marker is None:

        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
    all_data = []
    x_ms_continuation = {}
    admin = current_user.fname
    query = "assigned_to eq '" + admin + "' and review_status eq 'F'"
    print query
    try:
        entities = table_service.query_entities('janaswaram', num_results=1000, filter=query, marker=marker,
                                                accept=TablePayloadFormat.JSON_MINIMAL_METADATA, property_resolver=None,
                                                timeout=None)
        # entities=table_service.query_entities('janaswaram',filter=query)
        entit = list(entities)
        
        for row in entit:
            row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
            if 'Attachment' in row.keys():
                attachment_url=row['Attachment']
                if attachment_url!='NA':
                
                    if "janasenalive" in str(attachment_url).lower():

                        row['Attachment']='NA'
            else:
               row['Attachment']='NA'
        all_data = entit

        if hasattr(entities, 'next_marker'):
            x_ms_continuation = getattr(entities, 'next_marker')
    except Exception as e:
        print str(e)

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})



@blueprint_janaswaram.route('/getFinalCommitteeData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getFinalCommitteeData():
    next_marker = request.values.get('next_marker')
    if next_marker is None:

        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
    all_data = []
    x_ms_continuation = {}
    admin = current_user.fname
    query = " review_status eq 'T'"
    
    try:
        entities = table_service.query_entities('janaswaram', num_results=1000, filter=query, marker=marker,
                                                accept=TablePayloadFormat.JSON_MINIMAL_METADATA, property_resolver=None,
                                                timeout=None)
        # entities=table_service.query_entities('janaswaram',filter=query)
        entit = list(entities)
        for row in entit:
            row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
            if 'Attachment' in row.keys():
                attachment_url=row['Attachment']
                if attachment_url!='NA':
                
                    if "janasenalive" in str(attachment_url).lower():

                        row['Attachment']='NA'
            else:
               row['Attachment']='NA' 
        all_data = entit

        if hasattr(entities, 'next_marker'):
            x_ms_continuation = getattr(entities, 'next_marker')
    except Exception as e:
        print str(e)

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})



@blueprint_janaswaram.route('/janaswaramDashboard', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def janaswaramDashboard():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select at.admin_id,at.first_name,at.role from admin_team at join admin_category ac on ac.role=at.role order by at.admin_id ")
        admin_team = cur.fetchall()
        print admin_team
        return render_template("admin/admin.html", admin_team=admin_team)
    except psycopg2.Error as e:
        print str(e)
        return str(e)


@blueprint_janaswaram.route('/janaswaram_data', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def janaswaram_data():
    return render_template('admin/janaswaram.html')


@blueprint_janaswaram.route('/getJanaswaramData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getJanaswaramData():
    next_marker = request.values.get('next_marker')
    if next_marker is None:

        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
    all_data = []

    entities = table_service.query_entities('janaswaram', num_results=50, filter=None, marker=marker,
                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA, property_resolver=None,
                                            timeout=None)

    entit = list(entities)
    for row in entit:
        
        row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
        if 'Attachment' in row.keys():
            attachment_url=row['Attachment']
            if attachment_url!='NA':
                
                if "janasenalive" in str(attachment_url).lower():

                    row['Attachment']='NA'
        else:
           row['Attachment']='NA' 
    all_data = entit
    x_ms_continuation = {}
    if hasattr(entities, 'next_marker'):
        x_ms_continuation = getattr(entities, 'next_marker')

    # if x_ms_continuation!={}:
    # 	marker['nextpartitionkey'] = x_ms_continuation['nextpartitionkey']
    # 	marker['nextrowkey'] = x_ms_continuation['nextrowkey']
    # else:
    # 	break;

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})



@blueprint_janaswaram.route('/shortlistPolicies', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
def shortlistPolicies():
    if request.method == 'POST':
        comment = request.values.get('comment')

        rowkey = request.values.get('rowkey')

        # if comment is None or comment == "":
        #     return json.dumps({'errors': ['BAD_REQUEST','PARAMETER_comment_MISSING'],'data' : {'status':'0'}})
        if rowkey is None or rowkey == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_rowkey_MISSING'], 'data': {'status': '0'}})

        else:
            try:

                batch = TableBatch()

                task = {"review_notes": comment, "review_status": "T", "RowKey": str(rowkey), 'PartitionKey': 'pk'}
                if_match = ""
                batch.merge_entity(task, if_match)
                table_service.commit_batch('janaswaram', batch)
                return json.dumps({'errors': [], 'data': {'status': '1'}})
            except Exception as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})


@blueprint_janaswaram.route('/getCommitteeTeam', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getCommitteeTeam():
    role = request.values.get('role')
    if role is None or role == "":
        return json.dumps({"errors": ['PARAMETER_role_MISSING'], 'data': {"status": '0'}})
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select admin_id,first_name from admin_team where role=%s order by at.admin_id ", (role,))
        team = cur.fetchall()
        return json.dumps({"errors": [], 'data': {"status": '1', "children": team}})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": ['Exception occured'], 'data': {"status": '0'}})



@blueprint_janaswaram.route('/getFilteredData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getFilteredData():
    next_marker = request.values.get('next_marker')
    query = request.values.get('query')
    
    if next_marker is None:
        print 'none'
        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
        
    all_data = []
    if query is not None:
        filter = str(query)
    else:
        filter = None
    print str(query)
    entities = table_service.query_entities('janaswaram', num_results=1000, filter=filter, marker=marker,
                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA, property_resolver=None,
                                            timeout=None)
    
    entit = list(entities)
    for row in entit:
        row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
        if 'Attachment' in row.keys():
            attachment_url=row['Attachment']
            if attachment_url!='NA':
                
                if "janasenalive" in str(attachment_url).lower():

                    row['Attachment']='NA'

        else:
           row['Attachment']='NA' 
        if 'email' not in  row.keys():
            row['email']=''
        if 'Suggestion' not in row.keys():
            row['Suggestion']=''
    all_data = entit
    x_ms_continuation = {}
    if hasattr(entities, 'next_marker'):
        x_ms_continuation = getattr(entities, 'next_marker')

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})


