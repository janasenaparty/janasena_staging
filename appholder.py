# coding= utf-8 
# -*- coding: utf-8 -*-
import os
import sys
from logging import INFO
from celery import signals
from azure.storage.blob import BlockBlobService
from azure.storage.table import TableService
from azure.storage.models import CorsRule
from celery import Celery
from celery.schedules import crontab
from flask import Flask
from flasgger import Swagger
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from pymongo import MongoClient
from raven.contrib.flask import Sentry
from raven import Client
from raven.contrib.celery import register_signal, register_logger_signal
from datetime import timedelta
import config

reload(sys)
sys.setdefaultencoding('utf-8')

# Create app
app = Flask(__name__, static_url_path='/static')

app.config['PROPAGATE_EXCEPTIONS'] = True

# Azure connections
table_service = TableService(account_name='janasenabackup',
                             account_key='B4Smr3G5FYzIcta5cTisx728LLGvYLJOtxqGVANOfx+udWYQh+NZ5dLE37sUtcYuyEu+ZPihGM4QXm7NgUo4IA==')
block_blob_service = BlockBlobService(account_name='janasenabackup',
                                      account_key='B4Smr3G5FYzIcta5cTisx728LLGvYLJOtxqGVANOfx+udWYQh+NZ5dLE37sUtcYuyEu+ZPihGM4QXm7NgUo4IA==')
cors_rule = CorsRule(
    allowed_origins=['*'],
    allowed_methods=['POST', 'GET', 'OPTIONS'],
    allowed_headers=['*'],
    exposed_headers=['*'],
    max_age_in_seconds=20000)
block_blob_service.set_blob_service_properties(cors=[cors_rule])

# Secret key for csrf
app.secret_key = '20e4492ae7a626111af96bb012255a7d573f8ee8'

csrf = CSRFProtect(app)

# Celery worker configuration and creation
 # 'recurring_donations_remainder': {
 #        'task': 'recurring_donations_remainder',
 #        'schedule': crontab(hour=8, minute=15),
 #        'args': []
 #    },
app.config['CELERYBEAT_SCHEDULE'] = {
    'donations_check_task': {
        'task': 'donations_check_task',
        'schedule': crontab(hour=7, minute=15),
        'args': []
    },
   

    'supporter_id_update': {
        'task': 'supporter_id_update',
        'schedule': timedelta(hours=01),
        'args': []
    },
    'missedcalls_mapping_task': {
        'task': 'missedcalls_mapping_task',
        'schedule': crontab(hour=23,minute=55),
        'args': []
    }


}
app.config['CELERY_TIMEZONE'] = 'Asia/Kolkata'
app.config['CELERY_ACCEPT_CONTENT'] = ['msgpack','yaml','json']
# app.config['CELERY_WORKER_MAX_TASKS_PER_CHILD'] = 500

app.config['CELERY_TASK_SERIALIZER'] = 'json'
app.config['CELERY_RESULT_SERIALIZER'] = 'json'
app.config['CELERY_BROKER_URL'] = config.REDIS_LIVE_URL
app.config['CELERY_RESULT_BACKEND'] = 'redis'
# app.config['REDIS_SOCKET_TIMEOUT'] = 200

# conn = None

# @postfork
# def reconnect_to_db():
#     conn = MongoClient(config.MONGO_URL, config.MONGO_PORT)

def make_celery(app):
    if config.ENVIRONMENT_VARIABLE == "local":
        celerya = Celery(app.name,broker=config.REDIS_LIVE_URL, CELERY_RESULT_BACKEND=config.REDIS_LIVE_URL)
    else:
        celerya = Celery(app.name, broker=config.REDIS_LIVE_URL, CELERY_RESULT_BACKEND=config.REDIS_LIVE_URL)
    celerya.conf.update(app.config)
    TaskBase = celerya.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celerya.Task = ContextTask
    return celerya

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
app.config['DB_CONNECTION']=config.DB_CONNECTION
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
if config.ENVIRONMENT_VARIABLE == 'local':
    celery_app = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
else:
    # celery_app = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
    celery_app = make_celery(app)
all_celery_tasks=["utilities.others.send_membership_sms1","utilities.others.missedcalls_membership_sms",
                  "utilities.others.missedcalls_mapping_task","utilities.others.send_membership_card_email",
                  "celery_tasks.sms_email.send_bulk_sms_operations","celery_tasks.sms_email.update_country_task",
                  "celery_tasks.sms_email.send_bulk_email_operations","celery_tasks.sms_email.send_bulk_sms_excel","celery_tasks.sms_email.senddonationsreceipt",
                  "utilities.others.random_membership","celery_tasks.sms_email.send_membership_sms",
                  "celery_tasks.sms_email.donations_check_task","celery_tasks.sms_email.send_donation_remainder_sms",
                  "celery_tasks.sms_email.supporter_id_update","celery_tasks.sms_email.upload_passport_file",
                  "celery_tasks.sms_email.upload_visa_file","celery_tasks.sms_email.sendNriDonationsEmail",
                  "celery_tasks.sms_email.jspMissedcallTask2","utilities.others.send_memebrship_update_msg",
                    "celery_tasks.others.shitty_form_task","celery_tasks.others.generate_current",
                  "celery_tasks.others.get_map","celery_tasks.others.save_requests","celery_tasks.others.map_booth_with_voter_id",
                  "utilities.othersinsert_new_mem","celery_tasks.sms_email.send_recurring_payment_activation_email",
                  "celery_tasks.sms_email.upload_supporters_data","celery_tasks.sms_email.upload_mrpresident_file",
                  "celery_tasks.sms_email.send_recurring_donations_ack","celery_tasks.sms_email.send_subscription_cancel_email",
                  "celery_tasks.sms_email.recurring_donations_remainder","celery_tasks.sms_email.sendExportResultsToEmail"

                  ]
if config.ENVIRONMENT_VARIABLE == 'local':
    celery_app.autodiscover_tasks(all_celery_tasks)
else:
    celery_app.autodiscover_tasks(all_celery_tasks,force=True)



# @signals.setup_logging.connect
# def setup_celery_logging(**kwargs):
#     pass
celery_app.conf.broker_transport_options = {'visibility_timeout': 3600}
celery_app.log.setup()
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

app.config['MINIFY_PAGE'] = True

# HTMLMIN(app)

if config.ENVIRONMENT_VARIABLE != 'local':
    sentry = Sentry(app, dsn='https://421c700dd5234ac09acaf6a6c7adfea3:a8b4929fde91417f9193acd03928fd9f@sentry.io/1206750')
    client = Client('https://421c700dd5234ac09acaf6a6c7adfea3:a8b4929fde91417f9193acd03928fd9f@sentry.io/1206750')
    register_logger_signal(client)
    register_logger_signal(client, loglevel=INFO)
    register_signal(client)
    register_signal(client, ignore_expected=True)


template = {
"openapi":"3.0.0",
  "swagger": "2.0",
  "info": {
    "title": "Janasena Internal Systems API Documentation",
    "description": """This is a documentation for all the apis used in the project. This includes the urls that are used for serving any kind of html pages. \n# Introduction\n The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format:\n  ```
    {
      "message": "page not found"
    }\n ```\n # Authentication\n  Authentication for registries is handled client side. The client has to send authentication details to various endpoints that need to communicate with registries, such as `POST /images/(name)/push`. These are sent as `X-Registry-Auth` header as a Base64 encoded (JSON) string with the following structure:\n   ```
    {
      "username": "string",
      "password": "string",
      "email": "string",
      "serveraddress": "string"
    }``` \n
    The `serveraddress` is a domain/IP without a protocol. Throughout this structure, double quotes are required.\n\n   If you have already got an identity token from the [`/auth` endpoint](#operation/SystemAuth), you can just pass this instead of credentials:

    ```
    {
      "identitytoken": "9cbaf023786cd7..."
    }
    ```
    """,
    "contact": {
      "name": "tech@janasenaparty.org",
      "email": "tech@janasenaparty.org",
      "url": "https://nivedika.janasenaparty.org",
    },
      "x-logo": {
          "url": "https://janasenaparty.org/static/logo.png",
          "backgroundColor": "#0033A0"
      },

    "version": "0.0.1"
  },
  "host": "janasenaparty.org",  # overrides localhost:500
  "basePath": "/",  # base bash for blueprint registration
  "schemes": [
    "https"
  ],
  "operationId": "getmyData",
  "servers": [
    {
      "url": "https://janasenaparty.org",
      "description": "Live Server"
    },
    {
      "url": "https://nivedika.janasenaparty.org",
      "description": "Sandbox Server"
    }
  ],
  
  "components":{
      "securitySchemes":{
        'basicAuth': {
            'description': 'Basic auth',
            'name': 'Authorization',
            'type': 'apikey',
            'in': 'header'

        }
      },
      "schemas":{
        "GMD1": {
      "example": {
        "data": {
          "constituency": "Kadapa",
          "constituency_id": 1,
          "mandal": 1,
          "polling_booth": 1
        },
        "msg": "Successful",
        "status": 1
      },
      "properties": {
        "data": {
          "properties": {
            "constituency": {
              "example": "Vishakapatnam East",
              "type": "string"
            },
            "constituency_id": {
              "example": 1,
              "type": "integer"
            },
            "mandal": {
              "example": 1,
              "type": "integer"
            },
            "polling_booth": {
              "example": 1,
              "type": "integer"
            }
          },
          "type": "object"
        },
        "msg": {
          "example": "Successfull",
          "type": "string"
        },
        "status": {
          "example": 1,
          "type": "integer"
        }
      },
      "type": "object"
    },
        "GMD2": {
          "examples": {
            "three": {
              "msg": "యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు .",
              "status": 3
            },
            "two": {
              "msg": "ఓటర్ ఐడి నమోదు చెయ్యండి",
              "status": 2
            },
            "zero": {
              "msg": "Please fill your voter details in janasainyam app.",
              "status": 0
            }
          },
          "properties": {
            "msg": {
              "example": "Successfull",
              "type": "string"
            },
            "status": {
              "description": "0 indicates errors in the transaction 2 voter id is not registered 3 indicates that the user has already registered",
              "example": 1,
              "type": "integer"
            }
          },
          "type": "object"
        },
        "GVD2": {
          "examples": {
            "one": {
              "msg": "Proceed to entering details",
              "status": 1
            },
            "three": {
              "msg": "యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు .",
              "status": 3
            },
            "two": {
              "msg": "యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు .",
              "status": 2
            },
            "zero1": {
              "msg": "Please fill your voter details in janasainyam app.",
              "status": 0
            },
            "zero2": {
              "msg": "No Such User",
              "status": 0
            },
            "zero3": {
              "msg": "No Voter id found in database",
              "status": 0
            }
          },
          "properties": {
            "msg": {
              "example": "Successfull",
              "type": "string"
            },
            "status": {
              "description": "0 indicates errors in the transaction 2 voter id is not registered 3 indicates that the user has already registered",
              "example": 1,
              "type": "integer"
            }
          },
          "type": "object"
        },
        "Result": {
      "example": {
        "data": [
          {
            "membership_id": "JSP00000001",
            "name": "abhinav"
          }
        ],
        "msg": "received",
        "status": 1
      },
      "properties": {
        "data": {
          "items": {
            "properties": {
              "membership_id": {
                "example": "JSP00000001",
                "type": "string"
              },
              "name": {
                "example": "abhinav",
                "type": "string"
              }
            },
            "type": "object"
          },
          "type": "array"
        },
        "msg": {
          "example": "Successfull",
          "type": "string"
        },
        "status": {
          "example": 1,
          "type": "integer"
        }
      },
      "type": "object"
    }
      }

  }
}
swagger = Swagger(app, template=template)





    