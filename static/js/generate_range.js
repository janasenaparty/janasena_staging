function generate_range (start, quantity) {
    
    var arr = [];
    delta=50;
    for (var i = 0; i <= quantity; i++ ) {
        arr.push(pad(i * delta + start),8);
    };
    return arr;
}
function pad(num, len) { return ("00000000" + num).substr(-len) };