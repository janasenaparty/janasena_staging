	
	// var typepieChart = dc.pieChart("#dc-typeform-pieChart");
	var regpieChart = dc.pieChart("#dc-reg-pieChart");
	
	var ageBarChart = dc.barChart("#dc-age-barChart");

	var pcBarChart=dc.barChart("#dc-pc-barChart");
	var acBarChart=dc.barChart("#dc-ac-barChart");
	var genderpieChart = dc.pieChart("#dc-gender-pieChart");
	var agerangepieChart=dc.pieChart("#dc-agerange-pieChart");
	// var election_trends_chart=dc.compositeChart("#election_trends_chart");
	var analysis_data;
	function ReadJsonForCrossFilter(distinctName) {
	    var sum = 0;
	    var ss;
	    var colors1 = ["#F8B700", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];

	    d3.csv("static/json/PopData.csv", function (data) {
	        var selectedDistinct = [];
	        data.forEach(function (d) {

	            if (d.DistName.trim() == distinctName.trim()) {
	                selectedDistinct.push(d);
	                sum = sum + parseInt(d.Population);

	            }
	        });
	        sum = d3.format(",")(sum);
    		
	        var facts = crossfilter(selectedDistinct);

	        

	        var typeDimension = facts.dimension(function (a) {
	            return a.Gender;
	        });

	        var typeDimensionGroup = typeDimension.group().reduceSum(function (d) {
	            return d.Population;
	        });


	   //      typepieChart.width(220)
	   //          .height(200)
	   //          .radius(90)
	   //          .innerRadius(40)
	   //          .ordinalColors(colors1)
				// .legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	   //          .renderLabel(true)
	   //          .transitionDuration(1500)
	   //          .label(function (d) {
	   //          return d3.round(d.value / typeDimensionGroup.all().reduce(function (a, v) {
	   //              return a + v.value;
	   //          }, 0) * 100, 1) + "%";
	   //      })
	   //          .dimension(typeDimension)
	   //          .group(typeDimensionGroup)
	   //          .title(function (d) {
	   //          return d.key + "  :  " + d3.format(",")(d.value);
	   //      });


	        var regDimension = facts.dimension(function (a) {
	            return a.Rural_Urban;
	        });

	        var regDimensionGroup = regDimension.group().reduceSum(function (d) {
	            return (d.Population);
	        });


	        regpieChart.width(220)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
				.legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	            .ordinalColors(colors4)	         
	        .label(function (d) {
	            return d3.round(d.value / typeDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .transitionDuration(1500)
	            .dimension(regDimension)
	            .group(regDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });

	        var DateDimension = facts.dimension(function (a) {
	            return a.SubDistName;
	        });

	        var DateDimensionGroup = DateDimension.group().reduceSum(function (a) {
	            return a.Population;
	        });


	        
	        $("#lblDistinct").html(distinctName);
	        $("#Populationlabel").html(sum);
	        dc.renderAll();

	    })
	}

	function ReadAge(distinctName) {
	    var sum = 0;
	    var colors1 = ["#F8B700", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];
	    var colors5 = ["#67CD00", "#", "#", "#", "#"];
	    d3.csv("static/json/StateAgeData.csv", function (data) {
	        var selectedDistinct = [];
	        data.forEach(function (d) {

	            if (d.DistName.trim() == distinctName.trim()) {
	                selectedDistinct.push(d);


	            }
	        });

	        var facts = crossfilter(selectedDistinct);



	        var ageDimension = facts.dimension(function (a) {
	            return a.AgeGroup;
	        });

	        var ageDimensionGroup = ageDimension.group().reduceSum(function (a) {
	            return a.Population;
	        });


	        ageBarChart.width(400)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 50
	        })
	            .dimension(ageDimension)
	            .group(ageDimensionGroup)
	            .colors(colors5)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(ageDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)");
	        })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });



	        dc.renderAll();

	    })


	}

	//  jsp Charts 
	function draw_jsp_charts(district)
	{
		$.ajax({
           type: "GET",
           url: "/getMPConstituencies/"+district,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
              var data=response.data.children;
              draw_voter_charts(data.ac_data);
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });

	}

function draw_voter_charts(data)

{

	    var colors1 = ["#F8B700", "#E064CD", "#56B2EA", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];
	    
			var voters = crossfilter(data);
	     	        var pcDimension = voters.dimension(function (a) {
	            return a.parliament_constituency_name;
	        });

	        var pcDimensionGroup = pcDimension.group().reduceSum(function (a) {
	            return a.count;
	        });


	        pcBarChart.width(400)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 70
	        })
	            .dimension(pcDimension)
	            .group(pcDimensionGroup)
	            .colors(colors4)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(pcDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)");
	        })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });


             var acDimension = voters.dimension(function (a) {
	            return a.constituency_name;
	        });

	        var acDimensionGroup = acDimension.group().reduceSum(function (a) {
	            return a.count;
	        });


	        acBarChart.width(400)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 70
	        })
	            .dimension(acDimension)
	            .group(acDimensionGroup)
	            .colors(colors2)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(acDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)")
	            
	         
	          })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });


var genderDimension = voters.dimension(function (a) {
	            return a.gender;
	        });

	        var genderDimensionGroup = genderDimension.group().reduceSum(function (d) {
	            return d.count;
	        });


	        genderpieChart.width(220)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
	            .ordinalColors(colors1)
				.legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	            .renderLabel(true)
	            .transitionDuration(1500)
	            .label(function (d) {
	            return d3.round(d.value / genderDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .dimension(genderDimension)
	            .group(genderDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });

var ageDimension = voters.dimension(function (a) {
	            return a.age_range;
	        });

	        var ageDimensionGroup = ageDimension.group().reduceSum(function (d) {
	            return d.count;
	        });


	        agerangepieChart.width(240)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
	            .ordinalColors(colors1)
				.legend(dc.legend().x(200).y(2).itemHeight(8).gap(5))
	            .renderLabel(true)
	            .transitionDuration(1500)
	            .label(function (d) {
	            return d3.round(d.value / ageDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .dimension(ageDimension)
	            .group(ageDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });



	        dc.renderAll();

	   

 // acBarChart.selectAll('rect').on("filtered", function(d) {
 //    console.log("click!", d);
 //  })
 
acBarChart.on('renderlet.barclicker', function(chart, filter)
{


    chart.selectAll('rect.bar').on('click.custom', function(d) {

    	
    	var data=[];
    	
    	draw_election_charts(d['data']['key'].trim());
    	mandal_wise_charts(d['data']['key'].trim())
    	
    
       
    });
});

pcBarChart.on('renderlet.barclicker', function(chart, filter)
{
    chart.selectAll('rect.bar').on('click.custom', function(d) {
    	console.log(d['data']['key']);
    	var data=[];
    	for (var i=0;i<analysis_data.length;i++)
    	{
    		var assembly=analysis_data[i]['parliament_constituency_name'].trim();
    		// if (assembly==d['data']['key'].trim())
    		// {
    		// 	data.push(analysis_data[i]);
    			
    			draw_analysis_charts(data);

    		// }
    	}
    	
    
       
    });
});
}


	//  end of jsp charts

// data tables
function draw_analysis_tables(district){

$.ajax({
           type: "GET",
           url: "/getAnalysisDetails/"+district,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
              analysis_data=response.data.children;
              draw_analysis_charts(analysis_data);
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });

}
function draw_analysis_charts(data)
{
	
	var tbody=get_population('Avanigadda');
	
	$('.community_table').html(tbody);
}
 


     
$('body').on('click', '.issues_class', function() {
   
         // var assembly= $(this).closest('tr').children('td.name').html();
         var assembly='Avanigadda'
var html_issues_body=get_suggestion(assembly);
$("#issues_body").html(html_issues_body);
         $('#issues_model').modal('show');
    });

$('body').on('click', '.trends_class', function() {
   
         var assembly= $(this).closest('tr').children('td.name').html();
          
var population_data=get_population(assembly);
$(".popuation_table").html(population_data);
         $('#trends_model').modal('show');
    });

$('body').on('click', '.suggestion_class', function() {
   
         var assembly= $(this).closest('tr').children('td.name').html();
         
// var html_issues_body=get_issue(assembly);
$("#suggestions_body").html('html_issues_body');
         $('#suggestion_model').modal('show');
    });

function get_population(assembly)
{
  for(var i=0;i<analysis_data.length;i++)
  {
    if ((analysis_data[i].constituency_name).trim()==assembly.trim())
    {var html_str='';
    	var data=analysis_data[i].votes_details;
    	
    	data.sort(function(a, b){
  return a.count == b.count ? 0 : +(a.count < b.count) || -1;
  
})
    	var array_data=[ ["Community", "Population", { role: "style" } ]];
  var colors=["green","silver","gold","skyblue","orange","silver","gold","#e5e4e2","#b87333","silver","gold","#e5e4e2"];
    	for (var i=0;i<10;i++)
    	{
    		var percentage=data[i]['count'];
    		var community=data[i]['caste'];
    		var color=colors[i];
    		array_data.push([community,percentage,color])
    		
    	}

	console.log(array_data);
    	
      google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable(array_data);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Top 10 COmmunities",
        width: 300,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(data, options);
  }
    }
  }
  return '';
}
function get_suggestion(assembly)
{
  for(var i=0;i<analysis_data.length;i++)
  {
    if ((analysis_data[i].constituency_name).trim()==assembly.trim())
    {
    	$("#issues_model_label").html("<h2 align='center'>"+assembly+"</h2>");
    	var data=analysis_data[i].constituency_analysis;
    	var html_str='';
    	$.each(data, function (key, value) {
    		var header_string="<h3 align='center'>"+key.toUpperCase()+"</h3>";
    		html_str=html_str+header_string;
    		var para="<p>"+value+"</p>";
    		html_str=html_str+para;
    		html_str=html_str+"<br>"
    	})
      return html_str
    }
  }
  return '';
}

function get_age_range_data(assembly)
{var age_data=[]
  for(var i=0;i<agerange_data.length;i++)
  {
    if (agerange_data[i].constituency==assembly.toLowerCase())
    {
      var d={};
      d['age_range']= agerange_data[i]['age_range'];
      d['count']=agerange_data[i]['count'];
      age_data.push(d);
    }
  }
  return age_data;
  
}
var elections_data;
function get_election_trends(district)
{
	$.ajax({
           type: "GET",
           url: "/getElectionTrends/"+district,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
              elections_data=response.data.children;
              var constituency='';
              for (var key in elections_data){
		        constituency= key;
		        
		        break;
		        
		    }
		    draw_election_charts(constituency);
              
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });



}
function winnig_party_table(data)
{
	$("#winning_party_candidates").html();
	var html_str="";
	for (var i=0; i<data.length;i++)
	{
		html_str=html_str+"<tr><td>"+data[i].key+"</td><td>"+data[i].value[0].party+":"+data[i].value[0].candidate_name+"</td><td>Kapu</td></tr>"
	}
	$("#winning_party_candidates").html(html_str);
}
function draw_election_charts(constituency)
{
	console.log(constituency);
	var data=elections_data[constituency];
	console.log(data)
	winnig_party_table(data);
	$(".election_trends").html(constituency+" Election Trend");
//'use strict';
let compositeChart=dc.compositeChart("#election_trends_chart");

const BAR_PADDING = .2; // percentage the padding will take from the bar
const RANGE_BAND_PADDING = .5; // padding between 'groups'
const OUTER_RANGE_BAND_PADDING = 0.5; // padding from each side of the chart

let sizing = chart => {
    chart
        .width(window.innerWidth)
        .height(window.innerHeight)
        .redraw();
};

let resizing = chart => window.onresize = () => sizing(chart);

let barPadding;
let scaleSubChartBarWidth = chart => {
	console.log("hello")
    let subs = chart.selectAll(".sub");

    if (typeof barPadding === 'undefined') { // first draw only
        // to percentage
        barPadding = BAR_PADDING / subs.size() * 100;
        // each bar gets half the padding
        barPadding = barPadding / 2;
    }

    let startAt, endAt,
        subScale = d3.scale.linear().domain([0, subs.size()]).range([0, 100]);

    subs.each(function (d, i) {

        startAt = subScale(i + 1) - subScale(1);
        endAt = subScale(i + 1);

        startAt += barPadding;
        endAt -= barPadding;

       

        d3.select(this)
            .selectAll('rect')
            .attr("clip-path", `polygon(${startAt}% 0, ${endAt}% 0, ${endAt}% 100%, ${startAt}% 100%)`);

    });
};
var colorScale = d3.scale.ordinal()
   .domain(['TDP','INC','YSRC','PRAP'])
   .range(["yellow","skyblue","Blue","RED"]);


let ndx = crossfilter(data),
    dimension = ndx.dimension(d => d.key),
    group = {all: () => data}; // for simplicity sake (take a look at crossfilter group().reduce())

let barChart1 = dc.barChart(compositeChart)
    .barPadding(0)
    .valueAccessor(d => d.value[0].votes)
    .title(d =>d.value[0].party+":"+d.value[0].votes);
   
   

let barChart2 = dc.barChart(compositeChart)
    .barPadding(0)
    .valueAccessor(d => d.value[1].votes)
    .title(d =>d.value[1].party+": "+d.value[1].votes);
  

// let barChart3 = dc.barChart(election_trends_chart)
//     .barPadding(0)
//     .valueAccessor(d => d.value[2].value)
//    .title(d =>d.value[2].key)


compositeChart
    .shareTitle(false)
    .dimension(dimension)
    .group(group)
    .height(250)
    .width(300)
    .margins({
	            top: 10,
	            right: 10,
	            bottom: 20,
	            left: 70
	        })
    ._rangeBandPadding(RANGE_BAND_PADDING)
    ._outerRangeBandPadding(OUTER_RANGE_BAND_PADDING)
    .x(d3.scale.ordinal())
    .y(d3.scale.linear().domain([0, 100000]))
    .xUnits(dc.units.ordinal)
    .compose([barChart1, barChart2])

    .on('renderlet',function (chart) {
    	scaleSubChartBarWidth(chart) 
                        chart.selectAll('rect.bar').each(function(d){                          
                        	var title=d3.select(this)[0][0].textContent;
                        	title=title.split(":",1);
                   
                        d3.select(this).attr("fill",                             									
                        (function(d){
                       if(title=='TDP')
                       {
                       return "Yellow";
                       }
                       else if( title=='INC')
                       {
                       return "skyblue";
                       }
                        else if( title=='YSRC')
                       {
                       return "Blue";
                       }  
                         else if( title=='PRAP')
                       {
                       return "red";
                       }  
                       else
                       {
                       return "grey";
                       }
                                                  
                                              

                                   }))
                        }); 

            })
    
   
    
    .render();

// sizing(compositeChart);
// resizing(compositeChart);


}
function mandal_wise_charts(assembly)
{
	$.ajax({
           type: "GET",
           url: "/getMandals/"+assembly,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
             json_data=response.data.children;
             
		    draw_mandal_wise_charts(json_data);
              
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });
}

function draw_mandal_wise_charts(json_data)
{
var diameter = 300, //max size of the bubbles
    color    = d3.scale.category20b(); //color category

var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);
    $("#bubble_chart").empty();
var tooltip = d3.select("#bubble_chart")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("color", "white")
    .style("padding", "8px")
    .style("background-color", "rgba(0, 0, 0, 0.75)")
    .style("border-radius", "6px")
    .style("font", "12px sans-serif")
    
var svg = d3.select("#bubble_chart")
    .append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");
  
   var nodes = bubble.nodes(processData(json_data))
   // filter out the outer bubble
   .filter(function(d) { return !d.children; });

    var bubbles = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes)
        .enter();


    //create the bubbles
    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d) { return get_color(d.champs); })
        .on("mouseover", function(d) {
              tooltip.text(d.mandal_name + ": " + d.members);
              tooltip.style("visibility", "visible");
      })
         .on("mousemove", function() {
          return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
      })
      .on("mouseout", function(){return tooltip.style("visibility", "hidden");})
      .on("click", function(d){draw_polling_stations(d)});

    //format the text for each bubble
    bubbles.append("text")
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y + 5; })
        .attr("text-anchor", "middle")
        .text(function(d){ return d["mandal_name"]; })
        .style({
            "fill":"white", 
            "font-family":"Helvetica Neue, Helvetica, Arial, san-serif",
            "font-size": "12px"
        });
 

}
function get_color(value){
	if (value<=50 && value >=20)

		return 'skyblue';
	if (value>50 )

		return 'Green';
	else{
		return 'red';
	}
}
function processData(data) {
	 var obj = data;

   var newDataSet = [];

   for(var prop in obj) {
      newDataSet.push({mandal_name: obj[prop]['mdl_name'],value:200,champs:52,
         className: obj[prop]['mdl_name'].toLowerCase(), members: 52});
   }
   
   return {children: newDataSet};
}

function draw_polling_stations(data)
{
	console.log(data);

}