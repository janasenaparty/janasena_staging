$(document).ready(function(){
    var attendance = $('#attendance');
    var phone_rel = $('#phone_rel');
    var loader = $('.loader');
    var confirm = $('#confirm');


    confirm.addClass('hidden');
    phone_rel.addClass('hidden');
    loader.addClass('hidden');

    var mobile = $.urlParam('mobile');
    var event_id = $.urlParam('event_id');
    console.log(mobile,event_id);

//    page1
    var y = $('#y');
    var n = $('#n');

//    page2
    var iphone = $('#iphone');
    var android = $('#android');
    var nsp = $('#nsp');


    var at = 'N';
    var pr = 'N';

    var show_next = function(){
        attendance.addClass('hidden');
        phone_rel.removeClass('hidden');
    }

    var do_ajax_call = function(){
        var url = '';
        var data = {}
        data.at = at;
        data.pr = pr;
        data.mobile = mobile;
        data.event_id = event_id;
        data.csrf_token = csrf_token;
        loader.removeClass('hidden');
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var resp = JSON.parse(response);
            if(resp.status == 0){

                error(resp);
            }
            else if(resp.status == 2){
                error(resp);
            }
            else{

            }
            loader.addClass('hidden');
            phone_rel.addClass('hidden');
            confirm.removeClass('hidden');
          }
        });
    }

    y.click(function(){
        at = 'Y';
        show_next();
    });
    n.click(function(){
        at = 'N';
        show_next();
    });

    iphone.click(function(){
        pr = 'I'
        do_ajax_call();
    });
    android.click(function(){
        pr = 'A'
        do_ajax_call();
    });
    nsp.click(function(){
        pr = 'N'
        do_ajax_call();
    });

});