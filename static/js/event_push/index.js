$(document).ready(function(){
    var loader = $('.loader');
//  create event
    var event_desc_box = $('#edes');
    var event_date_box = $('#edate');
    var event_active_box = $('#ea');
    var event_add_submit = $("#add_event_submit");
    var modal_box = $('#myModal');
//    Send sms
    var sms = $('#sms_event');
    var sms_file = $('#sms_file');
    var smsmodal = $('#smsmodal');
    var send_sms_submit = $('#send_sms_submit');

    var error = function(resp){
        swal({
          title: "Error!",
          text: resp.errors[0],
          icon: "error",
        });
    }

    event_add_submit.click(function(){

        var url = '';
        var data = {}

        data.desc = event_desc_box.val();
        data.date = event_date_box.val();
        data.active = event_active_box.is(':checked');
        data.sms = sms.val();
        data.csrf_token = csrf_token;
        loader.removeClass('hidden');
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var resp = JSON.parse(response);
            if(resp.status == 0){

                error(resp);
            }
            else if(resp.status == 2){
                error(resp);
            }
            else{
                event_desc_box.val('');
                event_date_box.val('');
                modal_box.modal('toggle');
                event_active_box.prop('checked', false);
                loader.addClass('hidden');
            }

          }
        });

    });

    send_sms_submit.click(function(){
        console.log(sms_file[0].files[0]);
        var data = new FormData();
        data.set('sms_file',sms_file[0].files[0]);
        data.set('csrf_token',csrf_token);

        $.ajax({
            url: '/event/send_sms',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function(data){
                console.log(data);
            }
        });
    });

});