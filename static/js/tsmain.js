DemographicsListingView = Backbone.View.extend({
    initialize: function(){
        this.render();
    },
    render: function(){
	var variables = {
	    demographics_data: this.options.demographics_data.sort(function(a,b){
		return parseInt(a.gender_ratio_number_of_females_per_1000_males_2011) >= parseInt(b.gender_ratio_number_of_females_per_1000_males_2011);
	    })
	};
	var template = _.template( $("#demographics_listing_template").html(), variables );
	this.$el.html( template );
    },
});
function change(val1, val2) {
    var change = parseInt(val1) - parseInt(val2);
    if( change == 0 ) {
	return "No Change";
    } else if( change > 0) {
	return "Positive " + change;
    } else {
	return "Negative " + change;
    }
}
$(document).ready(function(){
	  var width = 540, height = 530, demographics_data;
    var lt_1k_brightness_scale, gt_1k_brightness_scale;

    var svg1, regions1, projection1, path1, centered;
    var svg2, regions2, projection2, path2, centered_2;
	
	svg1 = d3.select("#map1").append("svg")
	.attr("width", width)
	.attr("height", height-250);
	
	 regions1 = svg1.append("g")
	.attr("id", "regions");
    regions1.append("rect")
	.attr("class", "background")
	.attr("width", width)
	.attr("height", height)
	.on("click", click1);
	
	projection1 = d3.geo.mercator()
	.center([50.0, 1])
	.scale(6.5*width)
	.translate([-1*((6*width)/2), height*2.27]);
    path1 = d3.geo.path()
	.projection(projection1);    
	
	dataset = [   {   district: 'Adilabad',
							count: 2741239
						}, {
							district: 'Karimnagar',
							count: 3776269
						}, {
							district: 'Khammam',
							count: 2797370
						},{
							district: 'Mahabubnagar',
							count: 4053028
						}, {
							district: 'Medak',
							count: 3033288
						}, {
							district: 'Nalgonda',
							count: 3488809
						}, {
							district: 'Nizamabad',
							count: 2551335
						},{
							district: 'Warangal',
							count: 2551335
						},{
							district: 'Hyderabad',
							count: 3943323
						},{
							district: 'Rangareddy',
							count: 5296741
						}];		
	
			
								
		d3.json("static/json/ts-dist_topo.json", function(error, ap_topo_json) {

	    
	    var subunits = topojson.feature(ap_topo_json, ap_topo_json.objects["ap-dist"]);	
		
	    regions1.selectAll("path")
		.data(subunits.features)
		.enter().append("path")
		.attr("class", "region")
		.style("fill", function(d,i){		    
		    var region_id = d.id;
		    var region_name = _.map(
			d.properties.name.split(/\s+/),
			function(e){ return _.str.capitalize(e.toLowerCase()); }
		    ).join(" ");
		 
		    if ( dataset[i].district ) {
			
			var gender_ratio_as_int = parseInt(dataset[i].count);
			if( gender_ratio_as_int < 3000000 ){
			    return d3.rgb(20,216,18);
			} else {
			    return d3.rgb(240,120,50);
			}
		    } else {
			return "#000";
		    }
		})
		.attr("id", function(d){ return d.id; } )
		.attr("d", path1)
		.on("click", click1);
	   ReadJsonForCrossFilter("Hyderabad");   
	   ReadAge("Hyderabad");
	   draw_jsp_charts("Hyderabad");
	    draw_analysis_tables("Hyderabad");
	});
	
	function mouseover1(d){
		var overlay_html = "<strong>" + region_name + "</strong>";		
	}
	
	function click1(d,i) {
		var x, y, k;
	
	if (d && centered !== d) {
	    var centroid = path1.centroid(d);
	    x = centroid[0];
	    y = centroid[1];
	    k = 2;
	    centered = d;
	} else {
	    x = width / 2;
	    y = height / 2;
	    k = 1;
	    centered = null;
	}

	if( $("div.html-overlay").length > 0 ) {
	    d3.select("div.html-overlay").remove();
	} else {
	    svg1.selectAll("#overlay").remove();
	}

	var gender_ratio_as_int = parseInt(dataset[i].count);
	
	regions1.selectAll("path.active").style("fill", function(d,i){
	    var region_id = d.id;
	    var region_name = _.map(
		d.properties.name.split(/\s+/),
		function(e){ return _.str.capitalize(e.toLowerCase()); }
	    ).join(" ");
		
		  if ( dataset[i].district ) { 
			if( gender_ratio_as_int < 3000000 ){
			    return d3.rgb(240,120,50);
			} else {
			    
				return d3.rgb(20,216,18);
			}
		    } else {
			return "#000";
		    }
	    
	});

	regions1.selectAll("path")
	    .classed("active", centered && function(d,i) {
		if ( d === centered ) {
		    this.style.fill = "#0033CC";
		}
		return d === centered;
	    });

 regions1.transition()
	    /* .duration(1000)
	    .attr(
		"transform",
		"translate(" + width / 2 + "," + height / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")"
	    ) */
	    .style("stroke-width", 1.0 / k + "px")
	    .each("end", function(){
		
		if(centered != null) {

		    var region_id = d.id;
		    var region_name = _.map(
			d.properties.name.split(/\s+/),
			function(e){ return _.str.capitalize(e.toLowerCase()); }
		    ).join(" ");
		    var overlay_html = "<strong>" + region_name + "</strong>";
		    var overlay_text = region_name;
		    if ( dataset[region_name] ) {
			count=dataset[region_name] ["count"];
			/* overlay_html += "<p>Claimed amount : " + male_count ;
			overlay_text += "\nGender Ratio: " + gender_ratio + "\n"; */
			
		    } else {
			overlay_html += "<p>No Data found!</p>";
			overlay_text += "No Data found!";
		    }

		    
		}		
	    });
		
		
		
		ReadJsonForCrossFilter(d.properties.name);
		ReadAge(d.properties.name);
		draw_jsp_charts(d.properties.name);
		draw_analysis_tables(d.properties.name);
    }
	});
	