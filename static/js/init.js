csrf_token = ''
var init_csrf = function(te){
    csrf_token = te
}
var error_4xx = function(data, textStatus, xhr){
$('.loader').css('display','none');
console.log(data, textStatus, xhr);
    if(xhr.status == 401){
        console.log('401 found');
    }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('{{csrf_token()}}')
    },
    error: error_4xx
});

error = function(resp){
        swal({
          title: "Error!",
          text: resp.errors[0],
          icon: "error",
        });
    }