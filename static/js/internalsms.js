function send_internal_sms(data)
{
console.log(data);

$.ajax({
           type: "POST",
           url: "/processBulkSMSTest",
           dataType: "json",
           data:data,
           
           success: function (response) {
           $(".loader").css("display","None");
               if (response.data.status=='1') {
               	swal('success',"sent successfully",'success')
               }
               else
               {
               	$(".loader").css("display","None");
               	swal('Error',response.errors,'error')
 
             return false;
               }
           },
            error:function(response)
           {
            console.log(response.errors)
            $(".loader").css("display","None");
            swal('Error',response.errors,'error')
             return false;
           }
           
       }); 

}
function send_internal_email(data)
{
console.log(data);

$.ajax({
           type: "POST",
           url: "/processBulkEmailTest",
           dataType: "json",
           data:data,
           
           success: function (response) {
           $(".loader").css("display","None");
               if (response.data.status=='1') {
                swal('success',"sent successfully",'success')
               }
               else
               {
                $(".loader").css("display","None");
                swal('Error',response.errors,'error')
 
             return false;
               }
           },
            error:function(response)
           {
            console.log(response.errors)
            $(".loader").css("display","None");
            swal('Error',response.errors,'error')
             return false;
           }
           
       }); 

}

function otp_verification(type)
{

  $.ajax({
           type: "GET",
           url: "/SMSOTPVerification?otp_type="+type,
           dataType: "json",
           
           
           success: function (response) {
           $(".loader").css("display","None");
               if (response.data.status=='1') {
                swal('success',"OTP sent successfully",'success');

                $("#otp_div,#email_otp_div").css("display","block")
               }
               else
               {
                $(".loader").css("display","None");
                swal('Error',response.errors,'error')
 
             return false;
               }
           },
            error:function(response)
           {
            console.log(response.errors)
            $(".loader").css("display","None");
            swal('Error',response.errors,'error')
             return false;
           }
           
       }); 
}