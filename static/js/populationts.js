	var typepieChart = dc.pieChart("#dc-typeform-pieChart");
	var regpieChart = dc.pieChart("#dc-reg-pieChart");
	
	var ageBarChart = dc.barChart("#dc-age-barChart");

	var pcBarChart=dc.barChart("#dc-pc-barChart");
	var acBarChart=dc.barChart("#dc-ac-barChart");
	var genderpieChart = dc.pieChart("#dc-gender-pieChart");
	var agerangepieChart=dc.pieChart("#dc-agerange-pieChart");

	function ReadJsonForCrossFilter(distinctName) {
	    var sum = 0;
	    var colors1 = ["#F8B700", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];

	    d3.csv("static/json/PopData.csv", function (data) {
	        var selectedDistinct = [];
	        data.forEach(function (d) {

	            if (d.DistName.trim() == distinctName.trim()) {
	                selectedDistinct.push(d);
	                sum = sum + parseInt(d.Population);

	            }
	        });
	        sum = d3.format(",")(sum);
	        var facts = crossfilter(selectedDistinct);

	        

	        var typeDimension = facts.dimension(function (a) {
	            return a.Gender;
	        });

	        var typeDimensionGroup = typeDimension.group().reduceSum(function (d) {
	            return d.Population;
	        });


	        typepieChart.width(220)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
	            .ordinalColors(colors1)
	            .legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	            .renderLabel(true)
	            .transitionDuration(1500)
	            .label(function (d) {
	            return d3.round(d.value / typeDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .dimension(typeDimension)
	            .group(typeDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });


	        var regDimension = facts.dimension(function (a) {
	            return a.Rural_Urban;
	        });

	        var regDimensionGroup = regDimension.group().reduceSum(function (d) {
	            return (d.Population);
	        });


	        regpieChart.width(220)
	            .height(200)
	            .radius(90)
	            .innerRadius(40).legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	            .ordinalColors(colors4)
	            .label(function (d) {
	            return d3.round(d.value / typeDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .transitionDuration(1500)
	            .dimension(regDimension)
	            .group(regDimensionGroup)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });

	       

	        
	        $("#lblDistinct").html(distinctName);
	        $("#Populationlabel").html(sum);
	        dc.renderAll();

	    })


	}



	function ReadAge(distinctName) {
	    var sum = 0;
	    var colors1 = ["#F8B700", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];
	    var colors5 = ["#67CD00", "#", "#", "#", "#"];
	    d3.csv("static/json/StateAgeData.csv", function (data) {
	        var selectedDistinct = [];
	        data.forEach(function (d) {

	            if (d.DistName.trim() == distinctName.trim()) {
	                selectedDistinct.push(d);


	            }
	        });

	        var facts = crossfilter(selectedDistinct);



	        var ageDimension = facts.dimension(function (a) {
	            return a.AgeGroup;
	        });

	        var ageDimensionGroup = ageDimension.group().reduceSum(function (a) {
	            return a.Population;
	        });


	        ageBarChart.width(420)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 50
	        })
	            .dimension(ageDimension)
	            .group(ageDimensionGroup)
	            .colors(colors5)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(ageDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)");
	        })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });



	        dc.renderAll();

	    })


	}
	//  jsp Charts 
	function draw_jsp_charts(district)
	{
		$.ajax({
           type: "GET",
           url: "/getMPConstituencies/"+district,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
              var data=response.data.children;
              draw_voter_charts(data.ac_data);
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });

	}

function draw_voter_charts(data){
var sum = 0;
	    var ss;
	    var colors1 = ["#F8B700", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"];
	    var colors2 = ["#56B2EA", "#7B71C5", "#F8B700", "#78CC00", "#E064CD"];
	    var colors3 = ["#FF1744", "#0F9D58", "#4286F5", "#78CC00", "#7B71C5"];
	    var colors4 = ["#7B71C5", "#78CC00", "#F8B700", "#78CC00", "#56B2EA"];
	    
			var voters = crossfilter(data);
	     	        var pcDimension = voters.dimension(function (a) {
	            return a.parliament_constituency_name;
	        });

	        var pcDimensionGroup = pcDimension.group().reduceSum(function (a) {
	            return a.count;
	        });


	        pcBarChart.width(400)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 70
	        })
	            .dimension(pcDimension)
	            .group(pcDimensionGroup)
	            .colors(colors4)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(pcDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)");
	        })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });


             var acDimension = voters.dimension(function (a) {
	            return a.constituency_name;
	        });

	        var acDimensionGroup = acDimension.group().reduceSum(function (a) {
	            return a.count;
	        });


	        acBarChart.width(400)
	            .height(250)
	            .margins({
	            top: 10,
	            right: 10,
	            bottom: 100,
	            left: 70
	        })
	            .dimension(acDimension)
	            .group(acDimensionGroup)
	            .colors(colors2)
	            .transitionDuration(1500)

	            .x(d3.scale.ordinal().domain(acDimension))
	            .xUnits(dc.units.ordinal)
	            .yAxisLabel("")
	            .brushOn(false)
	            .renderlet(function (chart) {
	            chart.selectAll("g.x text").attr('dx', '-10').attr(
	                'dy', '-7').attr('transform', "rotate(-90)");
	        })
	            .elasticY(true)
	            .title(function (d) {
	            return d.key + " : " + d3.format(",")(d.value);
	        });


var genderDimension = voters.dimension(function (a) {
	            return a.gender;
	        });

	        var genderDimensionGroup = genderDimension.group().reduceSum(function (d) {
	            return d.count;
	        });


	        genderpieChart.width(220)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
	            .ordinalColors(colors1)
				.legend(dc.legend().x(2).y(2).itemHeight(10).gap(12))
	            .renderLabel(true)
	            .transitionDuration(1500)
	            .label(function (d) {
	            return d3.round(d.value / genderDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .dimension(genderDimension)
	            .group(genderDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });

var ageDimension = voters.dimension(function (a) {
	            return a.age_range;
	        });

	        var ageDimensionGroup = ageDimension.group().reduceSum(function (d) {
	            return d.count;
	        });


	        agerangepieChart.width(240)
	            .height(200)
	            .radius(90)
	            .innerRadius(40)
	            .ordinalColors(colors1)
				.legend(dc.legend().x(200).y(2).itemHeight(8).gap(5))
	            .renderLabel(true)
	            .transitionDuration(1500)
	            .label(function (d) {
	            return d3.round(d.value / ageDimensionGroup.all().reduce(function (a, v) {
	                return a + v.value;
	            }, 0) * 100, 1) + "%";
	        })
	            .dimension(ageDimension)
	            .group(ageDimensionGroup)
	            .title(function (d) {
	            return d.key + "  :  " + d3.format(",")(d.value);
	        });



	        dc.renderAll();

	   





}

function draw_analysis_tables(district){

$.ajax({
           type: "GET",
           url: "/getAnalysisDetails/"+district,
                       
           dataType:"json",
           contentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	
              analysis_data=response.data.children;
              draw_analysis_charts(analysis_data);
             
                
               }
              
               else {
                   
                    // $(".loader").css("display","None");
                    // alert(response.errors);
                    swal('Error',response.errors,'error')

                    return false;
                   
               }
           },
           error:function(response)
           {
            
            $(".loader").css("display","None");
            swal('Error','Please try again','error')
             return false;
           }
           
       });

}
function draw_analysis_charts(data)
{
	console.log('jjjjjj');
	console.log(data)
	var tbody = '';
	for (var i = 0; i <data.length; i++) {
		
		var s_no=i+1;
		tbody +=  "<tr class='assembly'><td>"+s_no+"</td><td class='name'>"+data[i]['constituency_name']+"</td><td>"+data[i]['parliament_constituency_name']+"</td><td>"+data[i]['district_name']+"</td><td><input type='button' class='btn btn-success trends_class'  value='OPEN' /> </td><td><input type='button' class='btn btn-success issues_class'  value='OPEN' /> </td><td><input type='button' class='btn btn-success suggestion_class'  value='OPEN' /> </td></tr>";
	}
	console.log(tbody)
	$('.analysis_result').html(tbody);
}
 


     
$('body').on('click', '.issues_class', function() {
   
         var assembly= $(this).closest('tr').children('td.name').html();
         
var html_issues_body=get_suggestion(assembly);
$("#issues_body").html(html_issues_body);
         $('#issues_model').modal('show');
    });

$('body').on('click', '.trends_class', function() {
   
         var assembly= $(this).closest('tr').children('td.name').html();
          
var population_data=get_population(assembly);
$(".popuation_table").html(population_data);
         $('#trends_model').modal('show');
    });

$('body').on('click', '.suggestion_class', function() {
   
         var assembly= $(this).closest('tr').children('td.name').html();
         
// var html_issues_body=get_issue(assembly);
$("#suggestions_body").html('html_issues_body');
         $('#suggestion_model').modal('show');
    });

function get_population(assembly)
{
  for(var i=0;i<analysis_data.length;i++)
  {
    if ((analysis_data[i].constituency_name).trim()==assembly.trim())
    {var html_str='';
    	var data=analysis_data[i].votes_details;
    	var total=0;
    	for (var i=0;i<data.length;i++)
    	{
    			total=total+data[i]['count']
    	}
    	data.sort(function(a, b){
  return a.count == b.count ? 0 : +(a.count < b.count) || -1;
});
    	for (var i=0;i<data.length;i++)
    	{
    		var percentage=((data[i]['count']/total) * 100).toFixed(2);
    		var S_no=i+1;
    		html_str=html_str+"<tr><td>"+S_no+"</td><td>"+data[i]['caste']+"</td><td>"+data[i]['count']+"</td><td>"+percentage+"</td></tr>";
    	}

		$("#trends_model_label").html(assembly+" "+"Constituency Total :"+total);
    	
    	
      return html_str;
    }
  }
  return '';
}
function get_suggestion(assembly)
{
  for(var i=0;i<analysis_data.length;i++)
  {
    if ((analysis_data[i].constituency_name).trim()==assembly.trim())
    {
    	$("#issues_model_label").html("<h2 align='center'>"+assembly+"</h2>");
    	var data=analysis_data[i].constituency_analysis;
    	var html_str='';
    	$.each(data, function (key, value) {
    		var header_string="<h3 align='center'>"+key+"</h3>";
    		html_str=html_str+header_string;
    		var para="<p>"+value+"</p>";
    		html_str=html_str+para;
    		html_str=html_str+"<br>"
    	})
      return html_str
    }
  }
  return '';
}

function get_age_range_data(assembly)
{var age_data=[]
  for(var i=0;i<agerange_data.length;i++)
  {
    if (agerange_data[i].constituency==assembly.toLowerCase())
    {
      var d={};
      d['age_range']= agerange_data[i]['age_range'];
      d['count']=agerange_data[i]['count'];
      age_data.push(d);
    }
  }
  return age_data;
  
}
	//  end of jsp charts