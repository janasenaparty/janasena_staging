var current_path = '';
var members_box = $('#member_list');
var select_box = $('#sel1');
var search_row_box = $('#search_row');
var role_list_g1_box = $('#role_list');
var edit_group_modal = $('#edit_group_modal');
var edit_group_path = $('#edit_group_path');
var edit_group_name = $('#edit_group_name');
var roles_modal = $('#roles_modal');
var edit_submit = $('#edit_submit');
var edit_member_modal = $('#edit_member_modal');
var edit_role_value = $('#edit_role_value');
var all_roles = {};
var role_submit =$('#role_submit');
var roles_modal =$('#roles_modal');
var gtid;

//$("ul").find("[data-slide='" + current + "']");
var current_role_list = []

 var get_group_types = function(group_type_id){

    var url = '/group_type_list'
    var data = {}
//    console.log(group_type_id)
    if(group_type_id){
        data.group_type_id = group_type_id;
    }
    data.csrf_token = csrf_token;
//    console.log(data);
    $('.loader').css('display','block');
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(response){
//            console.log(response);
            var group_types =  JSON.parse(response).data.children;

            var gl = '';
            for(var g =0;g<group_types.length;g++){
                gl += '<option value="'+ group_types[g][0] +'">'+ group_types[g][1] +'</option>'
            }
            select_box.html(gl);
            $('.loader').css('display','none');
          },

        });




}

var get_tree = function(){
        var url = '/get_tree'
        var data = {}
        data.csrf_token = csrf_token;
        $('.loader').css('display','block');
                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){
                    console.log(response);

                    tree_array = JSON.parse(response).data.children;
                    if(tree_array.length == 0){
                        $('#cr_button').show();
                    }
                    else{
                        $('#cr_button').hide();
                        regen_tree(tree_array, '#tree');
                    }
                    $('.loader').css('display','none');
                  },

                });

        }

var regen_tree = function(tree_array, selector){
    var tree = $(selector);
    tree.html('');
    if(tree_array.length == 0){

    }
    else{
//        console.log(tree_array.length);
        for(var i = 0;i< tree_array.length;i++){
            current_element = tree_array[i];
//            console.log(current_element);
            var path = current_element[0]
            var path_split = current_element[0].split('.')
            var parent_path = path_split.join('.');
            var name = current_element[1]
            var indent_count = current_element[2] - 1;
            var group_type_id = current_element[3];

            var indent_text = "";
//            console.log(indent_count);
            for(var j=0;j<=indent_count;j++){

                indent_text += '<span class="indent"></span>';
            }
//            console.log(indent_text);


            var lt = tree.find('li').find("[data-path='" + parent_path + "']")
            if(lt.length == 0){
//                var temp =
                if(path_split.length == 1){
                    var len = tree.find('ul').find("[data-path='root']").length;
                    if(len == 0){
                        tree.append('<ul class="list-group tree_ul" data-path="root"></ul>');
                        var temp = tree.find("[data-path='root']")
//                    .addClass('list-group').data('path','root')
                    temp.append('<li   class="list-group-item tree_li"  data-path="'+path+'"data-group_type_id="'+group_type_id +'">' + indent_text + '<span class="extra glyphicon glyphicon-asterisk"></span><span class="tree-node">'+name+'</span></li>')
                    temp.append('<ul class="list-group tree_ul hide_list" data-path="'+path+'"data-group_type_id="'+group_type_id +'"></ul>')

                    }


//                    temp.add('li').html(name);
                }
                else{

                var last_ele = path_split[path_split.length-1]
                path_split.pop();
                var parent_path = path_split.join('.');
                var temp = $("ul[data-path='" + parent_path +"']");
                var temp_li = $("li[data-path='" + parent_path +"']");
                temp_li.find('.extra').addClass('glyphicon-plus');
                if(temp.length == 0){
                    var ul_ele = '<ul class="list-group tree_ul" data-path="'+parent_path+'"data-group_type_id="'+group_type_id +'"></ul>';

                    temp.parent().append(ul_ele);
                    var e = $("ul[data-path='" + parent_path +"']");
                     e.append('<li   class="list-group-item tree_li"  data-path="'+path+'"data-group_type_id="'+group_type_id +'">' + indent_text + '<span class="extra glyphicon"></span><span class="tree-node">'+name+'</span></li>')
                     e.append('<ul class="list-group tree_ul hide_list" data-path="'+path+'"data-group_type_id="'+group_type_id +'"></ul>');
                }
                else{
                    temp.append('<li   class="list-group-item tree_li"  data-path="'+path+'"data-group_type_id="'+group_type_id +'">' + indent_text + '<span class="extra glyphicon"></span><span class="tree-node">'+name+'</span></li>')
                    temp.append('<ul class="list-group tree_ul hide_list" data-path="'+path+'"data-group_type_id="'+group_type_id +'"></ul>');
                }

//                ul_ele = $.find("[data-path='"+parent_path+"']").find('ul');
//                console.log(ul_ele);
//                ul_ele[0].append('<li class="list-group-item" data-path="'+path+'">');

                }
            }
//            console.log(lt);
        }
    }
}

var add_group = function(path, group_type_id){
//    console.log(group_type_id) + "path";
    get_group_types(group_type_id);
    var group_pop = $('#group_pop_modal');
    group_pop.find('#parent_path').val(path);
    group_pop.modal('show');
}

var add_members_old = function(path,group_type_id){


    var url = '/default_members';
    var data = {};
    current_path = path;
    data.current_path = current_path;
    data.csrf_token = csrf_token;

    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//            console.log(response);
            $('.loader').css('display','none');
            var members_array = JSON.parse(response).data.children;
//            console.log(members_array);
            if(members_array.length == 0){
                members_box.html('');
                members_box.html('No Users to be added');
            }
            else{
                members_box.html('');
                var table_create = '';
                var table_rows = '';
                for(var i in members_array){
                    var temp = '';
                    temp += `
                                <tr>
                                        <td><input type="checkbox" name='add_mem' value='mem_id'></td>
                                        <td>fname</td>
                                        <td>lname</td>
                                        <td>email</td>
                                      </tr>
                                `
                    temp = temp.replace('fname',members_array[i][0]);
                    temp = temp.replace('lname',members_array[i][1]);
                    temp = temp.replace('email',members_array[i][2]);
                    temp = temp.replace('mem_id',members_array[i][3]);
//                    console.log(temp);
                    table_rows += temp;
                }

                table_create +=  '<table class="table table-bordered">';
//                Table header
                table_create +=  `<thead>
                                      <tr>
                                        <th></th>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Age</th>
                                      </tr>
                                    </thead>`;
                table_create +=  `<tbody>
                                    table_rows
                                    </tbody>`;
                table_create = table_create.replace('table_rows',table_rows);

                table_create +=   '</table>';
                table_create += '<button class="btn" id="member_add">Add Members</button>'
                members_box.html(table_create);


            }

          },

    });

}

var add_members = function(path, group_type_id){
//    console.log(search_row_box);
    current_path = path;
    search_row_box.removeClass('hide_list');
    members_box.html('');
    console.log('add members right click');

    var url='/group/get_avalilable_roles';
    var data = {};
    data.group_type_id = group_type_id;
    data.csrf_token = csrf_token;

    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var group_types =  JSON.parse(response).data.children;

            var gl = '';
            if (group_types.length == 0){
                console.log('no users');
                role_list_g1_box.html('No Users to display.');
            }
            else{
                for(var g =0;g<group_types.length;g++){
                    gl += '<option value="'+ group_types[g]['id'] +'">'+ group_types[g]['name'] +'</option>'
                }
                role_list_g1_box.html(gl);
                current_role_list = gl;


            }

            $('.loader').css('display','none');
          }
     });
}

var get_all_members = function(path,group_type_id){
search_row_box.addClass('hide_list');
    var url = '/get_group_members';
    var data = {};
    current_path = path;
    data.current_path = current_path;
    data.csrf_token = csrf_token;
    data.count = 1;

    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//            console.log(response);
            $('.loader').css('display','none');
            var members_array = JSON.parse(response).data.children;
//            console.log(members_array);
            if(members_array.length == 0){
                members_box.html('');
                members_box.html('No Users to be added');
            }
            else{
                members_box.html('');
                var table_create = '';
                var table_rows = '';
                for(var i in members_array){
                    var temp = '';
                    temp += `
                                <tr>

                                        <td>fname</td>
                                        <td>lname</td>
                                        <td>email</td>
                                      </tr>
                                `
                    temp = temp.replace('fname',members_array[i][0]);
                    temp = temp.replace('lname',members_array[i][1]);
                    temp = temp.replace('email',members_array[i][2]);
                    temp = temp.replace('mem_id',members_array[i][3]);
//                    console.log(temp);
                    table_rows += temp;
                }

                table_create +=  '<table class="table table-bordered">';
//                Table header
                table_create +=  `<thead>
                                      <tr>

                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Age</th>
                                      </tr>
                                    </thead>`;
                table_create +=  `<tbody>
                                    table_rows
                                    </tbody>`;
                table_create = table_create.replace('table_rows',table_rows);

                table_create +=   '</table>';
                table_create += '<button class="btn" id="member_add">Add Members</button>'
                members_box.html(table_create);


            }

          },

    });
}

// g2.html functions
var add_group_type = function(path,group_type_id){
//    console.log(path);
     var group_pop = $('#group_pop_modal');
    group_pop.find('#parent_path').val(path);
    group_pop.modal('show');
}


var add_group_type_role = function(path,group_type_id){

roles_modal.modal('show');
gtid = group_type_id;

}
var get_roles = function(path,group_type_id){
//    console.log(group_type_id,"inside get_roles");
    gtid = group_type_id;
    var roles = $('#roles');
    var url = '/group/get_roles';
    var data = {}
    var roles_box = $('#roles');
    data.csrf_token = csrf_token;
    data.group_type_id = group_type_id;
    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//            console.log(response);
            $('.loader').css('display','none');
            var members_array = JSON.parse(response).data.children;
//            console.log(members_array);
            if(members_array.length == 0){
                roles_box.html('');
                roles_box.html('No Roles have been added');
            }
            else{
                roles_box.html('');
                var table_create = '';
                var table_rows = '';
                for(var i in members_array){
                    var temp = '';
                    temp += `
                                <tr>

                                        <td>role_name</td>
                                        <td>max_count</td>

                                      </tr>
                                `
                    temp = temp.replace('role_name',members_array[i]['name']);
                    temp = temp.replace('max_count',members_array[i]['max_count']);
  //                    console.log(temp);
                    table_rows += temp;
                }

                table_create +=  '<table class="table table-bordered">';
//                Table header
                table_create +=  `<thead>
                                      <tr>

                                        <th>Role Name</th>
                                        <th>Max Count</th>

                                      </tr>
                                    </thead>`;
                table_create +=  `<tbody>
                                    table_rows
                                    </tbody>`;
                table_create = table_create.replace('table_rows',table_rows);

                table_create +=   '</table>';
//                table_create += '<button class="btn" id="member_add">Add Members</button>'
                roles_box.html(table_create);


            }

          },

    });
}

var get_all_roles = function(){
    var url = '/group/get_all_roles';
    var data = {};
    $('.loader').css('display','block');
    data.csrf_token = csrf_token;
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//            console.log(response);
            $('.loader').css('display','none');
            all_roles = JSON.parse(response).data.children;

          }

     });
}

if(role_submit.length ==1){
    role_submit.click(function(){
    var role_name = $('#role_name');
    var max_count = $('#max_count');
    var data = {};
    var url = '/groups/add_role'

    data.group_type_id = gtid;
    data.role_name = role_name.val();
    data.max_count = max_count.val();
    data.csrf_token = csrf_token;

//    console.log(data);
    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
          console.log(response);

           $('.loader').css('display','none');
            swal('Successfully added');
            role_name.val('');
            max_count.val('1');
            roles_modal.modal('toggle');
            get_roles(0,gtid);


          }
    });

    });
}

// Edit group start
var edit_group = function(path, group_type_id){
    edit_group_path.val(path);
    console.log(path,group_type_id);

    var current_text = $("li[data-path='" + path +"']").find('.tree-node').html()
    edit_group_name.val(current_text);
    edit_group_modal.modal('show');
}
edit_submit.click(function(){
    console.log('I am clicked')
     var data = {};
    var url = '/group/edit'

    data.current_path = edit_group_path.val();
    data.group_name = edit_group_name.val();

    data.csrf_token = csrf_token;

//    console.log(data);
    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
          console.log(response);

           $('.loader').css('display','none');
            swal('Successfully added');


            edit_group_modal.modal('hide');
//            location.reload();

          },
          error: function(){
            edit_group_modal.modal('hide');
          }
    });

});

//Edit group end
var delete_group = function(path,group_type_id){

    swal({
    title:"Delete Group",
        text: "You will not be able to recover this imaginary file!",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "No, cancel plx!",
  closeOnConfirm: true,
  closeOnCancel: true},

  function(isConfirm) {
  if (isConfirm) {
     var data = {};
    var url = '/group/delete'

    data.current_path = path;


    data.csrf_token = csrf_token;

//    console.log(data);
    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){

          console.log(response);
          resp_json = JSON.parse(response);
          if(resp_json.status == 0){
            swal("Cancelled", resp_json.errors[0], "error");
          }
          else{
                get_tree();
                console.log('I am here');
             swal("Success", "Group Deleted successfully", "success");
          }
           $('.loader').css('display','none');


          }

    });
  } else {

  }
}
  )
}
// Common functions
var function_links = {
    "add-grp": add_group,
    "add-members": add_members,
    "get-all-members": get_all_members,
    "add-grp-type": add_group_type,
    "add-grp-type-role": add_group_type_role,
    "get_roles":get_roles,
    'edit_group': edit_group,
    'delete_group':delete_group
};

var tree_dom = $('#tree');
var tree2_dom = $("#tree2");
if(tree_dom.length==1){
    tree_dom.contextMenu({
        selector: 'li',
        callback: function(key, options) {
            var path = $(this).data('path')
            var group_type_id = $(this).data('group_type_id');
            console.log(group_type_id);

            function_links[key](path,group_type_id);
//            var m = "clicked: " + key + " on " + $(this).text();
//            window.console && console.log(m) || alert(m);
        },
        items: {
            "add-grp": {name:"Add Group", icon:"add"},
            "add-members": {name:"Add Members", icon:"add"},
            "get-all-members":{name:'Get All members', icon:"add"},
            "edit_group": {name:'Edit Name',icon:'edit'},
            "delete_group": {name:'Delete Group',icon:'edit'}

//            "add-mem": {name:"Add Members", icon:"add"},
//            "edit": {name: "Edit", icon: "edit"},
//            "cut": {name: "Cut", icon: "cut"},
//            "copy": {name: "Copy", icon: "copy"},
//            "paste": {name: "Paste", icon: "paste"},
//            "delete": {name: "Delete", icon: "delete"},
//            "sep1": "---------",
//            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
}
else if(tree2_dom.length==1){
    tree2_dom.contextMenu({

    selector: 'li',
    callback: function(key, options) {
        var path = $(this).data('path');
        var group_type_id = $(this).data('group_type_id');

        function_links[key](path,group_type_id);
//            var m = "clicked: " + key + " on " + $(this).text();
//            window.console && console.log(m) || alert(m);
    },
    items: {
        "add-grp-type": {name:"Add Group Type", icon:"add"},
        "add-grp-type-role":  {name:"Add Role", icon:"add"},
        "get_roles":  {name:"Get Roles", icon:"add"},
     }

});
}




$('body').on('click', '.extra',function(){
//    console.log('I am clicked');
    var parent_path = $(this).parent().data('path');

    var sibiling = $(this).parent().parent().find("ul[data-path='" + parent_path +"']");
//    console.log(sibiling);
    sibiling.toggleClass("hide_list");
    $(this).toggleClass('glyphicon-plus');
    $(this).toggleClass('glyphicon-minus');


});

$('body').on('click', '#member_add',function(){
    var url = '/add_members';
    var yourArray = [];
    $("input:checkbox[name=add_mem]:checked").each(function(){
    yourArray.push($(this).val());
    });
//    console.log(yourArray);
    var data = {};
    data.current_path = current_path;
    data.csrf_token = csrf_token;
    data.member_ids = JSON.stringify(yourArray);
//    console.log(data);
    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//          console.log(response);

           $('.loader').css('display','none');
            swal('Successfully added');
            add_members(current_path);
            add_member_box.addClass('hide-list')
          }
    });


});

$('#tree').on('click', '.tree_li',function(event){
    var url = '/get_group_members';
     var data = {};
    $('#search_row').addClass('hide_list');
    current_path = $(this).data('path');
    gtid = $(this).data('group_type_id');
    data.current_path = $(this).data('path');
    data.csrf_token = csrf_token;

    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//            console.log(response);
            $('.loader').css('display','none');
            var members_array = JSON.parse(response).data.children;
//            console.log(members_array);
            if(members_array.length == 0){
                members_box.html('');
                members_box.html('No Users have been added to this group');
            }
            else{
                members_box.html('');
                var table_create = '';
                var table_rows = '';
                for(var i in members_array){
                    var temp = '';
                    temp += `
                                <tr class='color_class'>
                                        <td>email</td>
                                        <td class='member_name'>fname</td>
                                        <td class='table-role' data-role_id ='role-id'>lname</td>

                                      </tr>
                               `
                    console.log(members_array[i][3]);
                    if(members_array[i][3]){
                        temp = temp.replace('color_class','admin_color')
                    }
                    else{
                        temp = temp.replace('color_class','')
                    }

                    temp = temp.replace('fname',members_array[i][0]);
                    temp = temp.replace('lname',all_roles[members_array[i][1].toString()][0]);
                    temp = temp.replace('email','<input type="checkbox" name="selected_members" value="'+members_array[i][2] +'">');
                    temp = temp.replace('role-id',members_array[i][1].toString())
//                    console.log(temp);
                    table_rows += temp;
                }

                table_create +=  '<table class="table table-bordered mtable">';
//                Table header
                table_create +=  `<thead>
                                      <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Role</th>

                                      </tr>
                                    </thead>`;
                table_create +=  `<tbody >
                                    table_rows
                                    </tbody>`;
                table_create = table_create.replace('table_rows',table_rows);

                table_create +=   '</table>';
                var additional_button = '<button class="btn mbutton edit_member">Edit</button><button class="btn mbutton delete_member">Delete</button>'
                additional_button += '<button class="btn mbutton assign_admin">Set Admin</button>'
                additional_button += '<span>Admin marked by color green</span>'
                var final = additional_button + table_create;

                members_box.html(final);

            }

          }
    });


});


var selected_state = '';
var states_l = {'AP':1, 'TS':2};
var gl_filter = $('#advanced_filter_btn');

if(gl_filter.length ==1){
    gl_filter.click(function(){

        var mobile_number = $('#mobile_number').val();
        console.log(mobile_number);
        var url = '/search_unassigned_members';
        var data = {};
        data.mobile_no = mobile_number;
        data.csrf_token = csrf_token;
        data.state = selected_state;
        data.const_id = $('#value_select').val();

        $('.loader').css('display','block');
        $.ajax({
              type: "POST",
              url: url,
              data: data,

              success: function(response){
              console.log(response);

               $('.loader').css('display','none');
                members_array = JSON.parse(response).data.children;
                if(member_list.length == 0){
                    members_box.html("Sorry No users found");
                }

                else{
                    var table_rows= '';
                    for(var i in members_array){
                        var temp = '';
                        temp += `
                                    <tr>
                                            <td >mem_id</td>
                                            <td >uname</td>
                                            <td >constituency</td>
                                            <td >district</td>

                                    </tr>
                                    `
                        console.log(members_array[i]);
                        temp = temp.replace('uname',members_array[i][1]);
                        temp = temp.replace('mem_id',"<input type='hidden' value='"+members_array[i][0]+"' name='member_ids'>");

                        temp = temp.replace('constituency',`
              <select class="form-control roles"></select>`);
                        temp = temp.replace('district',`<button type="button" class="btn btn-success form-control add_member" >Add member</button>`);


      //                    console.log(temp);
                        table_rows += temp;
                    }
                    table_create = '';
                     table_create +=  '<table class="table table-bordered col-md-12">';
//                Table header
                    table_create +=  `<thead>
                                      <tr>
                                         <th width=5%></th>
                                        <th width=40%>Name</th>
                                        <th width=35%>Role</th>
                                        <th width=20%>Add</th>

                                      </tr>
                                    </thead>`;
                table_create +=  `<tbody>
                                    table_rows
                                    </tbody>`;
                table_create = table_create.replace('table_rows',table_rows);

                table_create +=   '</table>';
                members_box.html(table_create);
                var roles = $('.roles');
                roles.html(current_role_list);
                }
              }
        });
    });

}

// Input checkboxes for adding members
$('body').on('click', 'input[name=member_ids]',function(){
//    console.log(this);
    var count = $('input[name=member_ids]:checked').size();
//    console.log(count);
    if(count==0){
        $('#add_member_box').addClass('hide_list');
    }
    else{
        $('#add_member_box').removeClass('hide_list');
    }
});
$('body').on('click', '.add_member',function(){
    var selected_members = [];
    var tr_parent =  $(this).parent().parent()
    var member_id =tr_parent.find('input[type="hidden"]').val()
    selected_members.push(member_id)
    var role = tr_parent.find('select').val()
 //    console.log(selected_members);
    var url = '/add_members';
    var data = {};
    data.current_path = current_path;
    data.member_ids = JSON.stringify(selected_members);
    data.role = role;
    data.csrf_token = csrf_token;
//    data.state = selected_state;
//    data.const_id = $('#value_select').val();

    $('.loader').css('display','block');
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
//          console.log(response);

          resp_json = JSON.parse(response);
          if(resp_json.status == 0){
            swal('Error',resp_json.errors[0]);
          }
          else{
            members_box.html('')
            $('#mobile_number').val('')
            swal('Sucess','Member added Successfully.');

          }
          $('.loader').css('display','none');

          }
      });

});

$('body').on('click', '.edit_member',function(){
    var yourArray = [];
    $("input:checkbox[name=selected_members]:checked").each(function(){
     var temp = {}
     temp['id']= $(this).val();
     temp['role_id'] = $(this).parent().parent().find('.table-role').data('role_id');
     temp['member_name'] = $(this).parent().parent().find('.member_name').html();
    yourArray.push(temp);
    });
    console.log(yourArray);
    if(yourArray.length == 0){
        swal('Please select atleast one member');
    }
    else if(yourArray.length > 1){
        swal('You can only edit one user at a time.')
    }
    else{
        var url = '';
        var m = yourArray[0];
        $('#edit_member_name').html(m['member_name']);
        $('#edit_role_id').val(m['id']);
        var url='/group/get_avalilable_roles';
        var data = {};
        data.group_type_id = gtid;
        data.csrf_token = csrf_token;

        $('.loader').css('display','block');
        $.ajax({
              type: "POST",
              url: url,
              data: data,

              success: function(response){
                console.log(response);
                var group_types =  JSON.parse(response).data.children;

                var gl = '';
                if (group_types.length == 0){
                    console.log('no users');
                    role_list_g1_box.html('No Users to display.');
                }
                else{
                    for(var g =0;g<group_types.length;g++){
                        if(group_types[g]['id'] == m['role_id']){
                            gl += '<option value="'+ group_types[g]['id'] +'" selected="selected" >'+ group_types[g]['name'] +'</option>'
                        }
                        else{
                            gl += '<option value="'+ group_types[g]['id'] +'">'+ group_types[g]['name'] +'</option>'
                        }

                    }
                    edit_role_value.html(gl);
                    current_role_list = gl;
                    edit_member_modal.modal('show');


                }

                $('.loader').css('display','none');
              }
         });








    }


});
$('body').on('click', '.delete_member',function(){
    var edit_id = $(this).data('member');
    console.log(edit_id, current_path);
    var edit_id = [];
    $("input:checkbox[name=selected_members]:checked").each(function(){
     var temp = {}
     temp['id']= $(this).val();
     temp['role_id'] = $(this).parent().parent().find('.table-role').data('role_id');
    edit_id.push(temp);
    });
    console.log(edit_id);

//    data.state = selected_state;
//    data.const_id = $('#value_select').val();
     swal({
            title: "Delete!",
            text: "Do you really want to remove the user?!",
            type: "warning"
        }, function() {
            $('.loader').css('display','block');
            var url = '/delete_member';
            var data = {};
        //    data.current_path = current_path;
        //    data.member_ids = JSON.stringify(selected_members);
        //    data.role = role;
            data.delete_id = JSON.stringify(edit_id);
            data.csrf_token = csrf_token;
            $.ajax({
              type: "POST",
              url: url,
              data: data,

              success: function(response){
    //          console.log(response);

              resp_json = JSON.parse(response);
              if(resp_json.status == 0){
                swal('Error',resp_json.errors[0]);
              }
              else{
    //            members_box.html('')
    //            $('#mobile_number').val('')
                var url = '/get_group_members';
                data.current_path = current_path;
                data.csrf_token = csrf_token;

                $('.loader').css('display','block');
                $.ajax({
                      type: "POST",
                      url: url,
                      data: data,

                      success: function(response){
                        console.log(response);
                        $('.loader').css('display','none');
                        var members_array = JSON.parse(response).data.children;
            //            console.log(members_array);
                        if(members_array.length == 0){
                            members_box.html('');
                            members_box.html('No Users have been added to this group');
                        }
                        else{
                            members_box.html('');
                            var table_create = '';
                            var table_rows = '';
                            for(var i in members_array){
                                var temp = '';
                                temp += `
                                            <tr color_class >

                                                    <td>fname</td>
                                                    <td>lname</td>
                                                    <td>email</td>
                                                  </tr>
                                             `
                                console.log(members_array[i][3]);
                                temp = temp.replace('color_class','')
                                temp = temp.replace('fname',members_array[i][0]);
                                temp = temp.replace('lname',all_roles[members_array[i][1].toString()][0]);
                                temp = temp.replace('email','<button class="edit_member" data-member="'+members_array[i][2] +'">Edit</button>' +'<button class="delete_member" data-member="'+members_array[i][2] +'">Delete</button>');
            //                    console.log(temp);
                                table_rows += temp;
                            }

                            table_create +=  '<table class="table table-bordered">';
            //                Table header
                            table_create +=  `<thead>
                                                  <tr>
                                                    <th>Name</th>
                                                    <th>Role</th>
                                                    <th>Edit</th>
                                                  </tr>
                                                </thead>`;
                            table_create +=  `<tbody>
                                                table_rows
                                                </tbody>`;
                            table_create = table_create.replace('table_rows',table_rows);

                            table_create +=   '</table>';

                            members_box.html(table_create);

                        }

                      }
                });
                swal('Sucess','Member Deleted Successfully.');

                }
              $('.loader').css('display','none');

              }
            });
        });



});

$('body').on('click', '.assign_admin',function(){

    var url = '/group/assign_admin';
    var data = {};
    var yourArray = [];
//    Need member id, path for which he is admin, default csrf token
    $("input:checkbox[name=selected_members]:checked").each(function(){
     var temp = {}
     temp['id']= $(this).val();
     temp['role_id'] = $(this).parent().parent().find('.table-role').data('role_id');
     temp['member_name'] = $(this).parent().parent().find('.member_name').html();
    yourArray.push(temp);
    });
    console.log(yourArray);
    if(yourArray.length == 0){
        swal('Please select atleast one member');
    }
    else if(yourArray.length > 1){
        swal('You can only assign one user as admin.')
    }
    else{

        data.current_path = current_path;
        data.csrf_token = csrf_token;
        data.member_id = yourArray[0]['id'];
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            $('.loader').css('display','none');
            swal('Sucess','Member Updated Successfully.');
//            edit_member_modal.modal('toggle');
            $('#tree .tree_li').click();
          }


    });

    }

});

$('#edit_role_submit').click(function(){
    var member_name = $('#edit_member_name').val();
    var role_id = $('#edit_role_value').val();
    var id = $('#edit_role_id').val();
    $('.loader').css('display','block');
    var url = '/group/member_edit';
    var data = {}
    data.csrf_token = csrf_token;
    data.id = id;
//    data.member_name = member_name;
    data.current_path = current_path;

    data.role_id = role_id;
    console.log(data);
    $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            $('.loader').css('display','none');
            swal('Sucess','Member Updated Successfully.');
            edit_member_modal.modal('toggle');
            $('#tree .tree_li').click();
          }


    });





});

// Rambabu functions for filter
var states=[];
var districts=[];
var assemblies=[];
var parliament_constituencies=[];
var calling_function = function(){
//console.log('constituency data start');
 $.getJSON('/static/json/constituencies.json',function(data){
            json_data=data;
            $.each(data,function(i,val){

 if(states.indexOf(data[i].state) === -1){
        states.push(data[i].state);
    }
    if(districts.indexOf(data[i].district_name) === -1){
        districts.push(data[i].district_name);
    }
  });
//console.log(states,districts);
$("#report_level").addClass('hide_list');
var report_level = 'assembly';
if(report_level=='assembly')
	{

		$('#district_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select district</option>')
        .val('');
		$.each(districts, function( index, value ) {
  			 $('#district_select').append($("<option></option>").attr("value",value).text(value));
		});
		$("#district_div").css("display","block");
		$('#value_div').css("display","none");
	}


        }).error(function(){
//            console.log('error');
        });

$.getJSON('/static/json/parliament_constituencies.json',function(data){
    parliament_json_data=data;
    $.each(data,function(i,val){

if(states.indexOf(data[i].state) === -1){
parliament_constituencies.push(data[i].parliament_constituency_name);
}

});


}).error(function(){
//    console.log('error');
});

//console.log('constituency data end');


}

      //


$('#district_select').change(function()
{
	var district=$(this).val();
	var assemblies=[];
	$('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select Assembly</option>')
        .val('');
	 $.each(json_data,function(i,val){
	 	if(val.district_name===district)
	 	{
	 		selected_state=val.state;
	 		 $('#value_select').append($("<option></option>").attr("value",val.constituency_id).text(val.constituency_name));
	 	}

	 	 });
	 $('#value_div').css("display","block");



})

//

//Assign local admin
var assign_local_admin = function(){

    data.csrf_token = csrf_token;
    data.current_path = current_path;

    data.member_id = member_id;
    console.log(data);

}