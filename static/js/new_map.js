$(document).ready(function(){
	  var width = 1024, height = 900, demographics_data;
    var lt_1k_brightness_scale, gt_1k_brightness_scale;

    var svg1, regions1, projection1, path1, centered;
    var svg2, regions2, projection2, path2, centered_2;

	svg1 = d3.select("#map1").append("svg")
	.attr("width", width)
	.attr("height", height-30);

	 regions1 = svg1.append("g")
	.attr("id", "regions");
    regions1.append("rect")
	.attr("class", "background")
	.attr("width", width)
	.attr("height", height-30)
	.on("click", click1);

	projection1 = d3.geo.mercator()
	.center([49.0, 1])
	.scale(6.5*width)
	.translate([-1*((6*width)/2), height*2.27]);
    path1 = d3.geo.path()
	.projection(projection1);

    d3.json("static/json/ap-dist_topo.json", function(error, ap_topo_json) {

	    var subunits = topojson.feature(ap_topo_json, ap_topo_json.objects["ap-dist"]);

	    regions1.selectAll("path")
		.data(subunits.features)
		.enter().append("path")
		.attr("class", "region")
		.style("fill", function(d,i){
		    var region_id = d.id;
		    var region_name = _.map(
			d.properties.name.split(/\s+/),
			function(e){

			return _.str.capitalize(e.toLowerCase()); }
		    ).join(" ");
		})
		.attr("id", function(d){ return d.id; } )
		.attr("d", path1)
		.on("click", click1);;
    });
	var click1 = function(){
	}


});