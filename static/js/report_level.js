var mandals = [];
var states = [];
var parliament_constituencies = [];
var districts = [];
var polling_booths=[];
var selected_state;
$.getJSON('/static/json/mandals.json',function(data){
            mandals=data;
});
$.getJSON('/static/json/constituencies.json',function(data){
    json_data=data;
//    console.log(data);
    $.each(data,function(i,val){

     if(states.indexOf(data[i].state) === -1){
            states.push(data[i].state);
        }
        if(districts.indexOf(data[i].district_name) === -1){
            districts.push(data[i].district_name);
        }
      });
//      console.log(states);
//      console.log(districts);
      $.each(districts, function( index, value ) {
  			 $('#district_select').append($("<option></option>").attr("value",value).text(value));

		});
		$('#district_select').trigger('change');


}).error(function(){
    console.log('error');
});

$.getJSON('/static/json/parliament_constituencies.json',function(data){
    parliament_json_data=data;
    $.each(data,function(i,val){

     if(states.indexOf(data[i].state) === -1){
            parliament_constituencies.push(data[i].parliament_constituency_name);
        }

    });


}).error(function(){
    console.log('error');
});

$("#report_level").change(function()
{
	if($(this).val()=='state')
	{
		$('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select state</option>')
        .val('');
		$.each(states, function( index, value ) {
  			 $('#value_select').append($("<option></option>").attr("value",value).text(value));
		});
		$("#value_div").css("display","block");
		$("#district_div").css("display","none");
	}
	else if($(this).val()=='district')
	{
		$('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select district</option>')
        .val('');
		$.each(districts, function( index, value ) {
  			 $('#value_select').append($("<option></option>").attr("value",value).text(value));
		});
		$("#value_div").css("display","block");
		$("#district_div").css("display","none");
	}

	else if($(this).val()=='assembly')
	{

		$('#district_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select district</option>')
        .val('');
		$.each(districts, function( index, value ) {
  			 $('#district_select').append($("<option></option>").attr("value",value).text(value));
		});
		$("#district_div").css("display","block");
		$('#value_div').css("display","none");
	}
    else if ($(this).val() == 'parliament') {

        $('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select parliament</option>')
        .val('');
        $.each(parliament_constituencies, function (index, value) {
        $('#value_select').append($("<option></option>").attr("value", value).text(value));
        });
        $("#value_div").css("display", "block");
        $("#district_div").css("display", "none");
        }
    else if($(this).val()=='mandal')
        {

        $('#district_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select district</option>')
        .val('');
    $.each(districts, function( index, value ) {
         $('#district_select').append($("<option></option>").attr("value",value).text(value));
    });
    $("#district_div").css("display","block");
    $('#value_div').css("display","none");
  }
})

$('#district_select').change(function()
{
    var district=$(this).val();
    if ($("#report_level").val()=='assembly'){


	    var assemblies=[];
	    $('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select Assembly</option>')
        .val('');
	    $.each(json_data,function(i,val){
            if(val.district_name===district)
            {
                selected_state=val.state;
                 $('#value_select').append($("<option></option>").attr("value",val.constituency_id).text(val.constituency_name));
            }

        });
	    $('#value_div').css("display","block");
    }
    else if ($("#report_level").val()=="mandal"){
        $('#value_select')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected>Select Mandal</option>')
        .val('');
        $.each(mandals,function(key,val){

            if(key==district)
            {
                $.each(val,function(i,value){
                // selected_state=val.state;
                    $('#value_select').append($("<option></option>").attr("value",value.seq_id).text(value.mdl_name));
                });
            }

        });
        $('#value_div').css("display","block");
    }


});


