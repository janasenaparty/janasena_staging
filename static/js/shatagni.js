

var profile;
var profile_status;

function get_profile()
{
	$.ajax({
      type: "POST",
      url: "/shatagni/profile",
      dataType:"json",
      
       
      success: function (response) {
      	
       if(response.status=='1')
       {
       	profile=response.profile;
        console.log(profile)
       	profile_status=profile.status;
        return profile;
       
       }
       else
       {
        return false;
       }

     }
       
    })

}


  $('#edit_add').click(function (e) {
  	$("#e_house_no").val(profile.house_no);
$("#e_street_village").val(profile.street_village);
$("#e_city_town").val(profile.city_town);
$("#e_pincode").val(profile.pincode);
$("#e_state").val(profile.state);
$("#e_landmark").val(profile.landmark);
   
    $(".active").hide();
     $('#edit_address').css('display', 'block');

  })

  $('#address_form').submit(function (e) {
    e.preventDefault();
  
var house_no=$("#e_house_no").val();
var street_village=$("#e_street_village").val();
var city_town=$("#e_city_town").val();
var pincode=$("#e_pincode").val();
var state=$("#e_state").val();
var landmark=$("#e_landmark").val();
   var data={house_no:house_no,street_village:street_village,city_town:city_town,pincode:pincode,state:state,landmark:landmark}
    $.ajax({
      type: "POST",
      url: "/shatagni/change_address",
      data: JSON.stringify(data),
      dataType: "json",
      contentType: "application/json",
      success: function (response) {
        if(response.status=='1')
        	{
        		alert("success");
        		get_profile();

        	}
        	else
        	{
        		alert(response.errors);
        		return false;
        	}
      },
      error: function (response) {
      	alert(response.errors);
        		return false;

      }
    })
  })
  
 $('#profile_form').submit(function(e){
  e.preventDefault();
  var name=$('#p_name').val();
  var house_no=$('#p_house_no').val();
  var street_village=$('#p_street_village').val();
  var landmark=$('#p_landmark').val();
  var state=$('#p_state').val();
  var phno=$('#p_phno').val();
  var city_town =$('#p_city_town').val();
  var pincode=$('#p_pincode').val();
  var email=$('#p_email').val();
  var data={name:name,
			house_no:house_no,
			street_village:street_village,
			landmark:landmark,
			state:state,
			phno:phno,
			city_town:city_town,
			pincode:pincode,
			email:email}
  $.ajax({
    type: "POST",
    url: "/shatagni/update_profile",
    data: JSON.stringify(data),
    dataType: "json",
    contentType: "application/json",
    success: function (response) {
      if(response.status=='1')
      {
		alert("success")
		get_profile();
      }
      else
      {
      	alert(response.erros)
      	return false;
      }
    },
    error: function (response) {
		alert(response.erros)
      	return false;
    }
  })
})
