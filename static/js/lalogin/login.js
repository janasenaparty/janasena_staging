$(function(){

    var phone = $('#username');
    var fg_phone = $('#phone');
    var password = $('#password');
    var check = $('#check');
    var login = $('#login');
    var passfm = $('#passformgp');
    var reset_pass = $('#reset-pass')
    var otp = $('#otp')
    var otp_grp = $('#otp-grp')
    var reset_bt = $('#reset-bt');
    var submit_otp = $('#submit-otp');
    var otp_sub = $('#otp-sub');
    var my_modal = $('#myModal');
    var submit_pass = $('#pass-sub');
    var p1 = $('#p1');
    var p2 = $('#p2');
    passfm.addClass('hidden');
    login.addClass('hidden');



    var error = function(resp){
        swal({
          title: "Error!",
          text: resp.errors[0],
          icon: "error",
        });
    }

    check.click(function(){
        var url ='/group/check_local_admin';
        var data = {};
        data.phone = phone.val();
        $('.loader').css('display','block');
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var resp = JSON.parse(response);
            if(resp.status == 0){

                error(resp);
            }
            else if(resp.status == 2){
                error(resp);
            }
            else{
                passfm.removeClass('hidden');
                login.removeClass('hidden');
                password.removeClass('hidden');
                check.addClass('hidden');
            }
            $('.loader').css('display','none');

          }
        });
    });

    reset_pass.click(function(){
        var url ='/group/forgotlaPassword';
        var data = {};
        data.phone = fg_phone.val();
        $('.loader').css('display','block');
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var resp = JSON.parse(response);
            if(resp.status == 0){

                error(resp);
            }
            else if(resp.status == 2){
                error(resp);
            }
            else{
                otp_grp.removeClass('hidden');
                reset_bt.addClass('hidden');
                otp_sub.removeClass('hidden');
            }
            $('.loader').css('display','none');

          }
        });

    });

    otp_sub.click(function(){
        var url ='/group/verify_otp';
        var data = {};
        data.phone = fg_phone.val();
        data.otp = otp.val();
        $('.loader').css('display','block');
        $.ajax({
          type: "POST",
          url: url,
          data: data,

          success: function(response){
            console.log(response);
            var resp = JSON.parse(response);
            if(resp.status == 0){

                error(resp);
            }
            else if(resp.status == 2){
                error(resp);
            }
            else{
                my_modal.modal('toggle');
            }
            $('.loader').css('display','none');

          }
        });

    });

    submit_pass.click(function(){
        var url ='/group/set_password';

        if(p1.val() != p2.val()){
            swal({
          title: "Error!",
          text: "Passwords do not match",
          icon: "error",
        });
        fg_phone.val('');
        otp.val('');
        p1.val('');
        p2.val('');
        }
        else{
            var data = {};
            data.p1 = p1.val();
            data.p2 = p2.val();
            data.phone = fg_phone.val();
            $('.loader').css('display','block');
            $.ajax({
              type: "POST",
              url: url,
              data: data,

              success: function(response){
                console.log(response);
                var resp = JSON.parse(response);
                if(resp.status == 0){

                    error(resp);
                }
                else if(resp.status == 2){
                    error(resp);
                }
                else{
                swal({
                          title: "Success!",
                          text: "Password Updated successfully",
                          icon: "successs"

                        }).then(function() {
                                window.location.href = '/group/lalogin';
                            });
                    my_modal.modal('toggle');


                }
                $('.loader').css('display','none');

              }
            });

        }




    });

});