//DemographicsListingView = Backbone.View.extend({
//    initialize: function(){
//        this.render();
//    },
//    render: function(){
//	var variables = {
//	    demographics_data: this.options.demographics_data.sort(function(a,b){
//		return parseInt(a.gender_ratio_number_of_females_per_1000_males_2011) >= parseInt(b.gender_ratio_number_of_females_per_1000_males_2011);
//	    })
//	};
//	var template = _.template( $("#demographics_listing_template").html(), variables );
//	this.$el.html( template );
//    },
//});

function change(val1, val2) {
    var change = parseInt(val1) - parseInt(val2);
    if( change == 0 ) {
	return "No Change";
    } else if( change > 0) {
	return "Positive " + change;
    } else {
	return "Negative " + change;
    }
}


$(document).ready(function(){
    init_csrf('{{csrf_token()}}');

	  var width = 992, height = 500, demographics_data;
    var lt_1k_brightness_scale, gt_1k_brightness_scale;

    var svg1, regions1, projection1, path1, centered;
    var svg2, regions2, projection2, path2, centered_2;

	svg1 = d3.select("#map1").append("svg")
	.attr("width", width)
	.attr("height", height-30);

	 regions1 = svg1.append("g")
	.attr("id", "regions");
    regions1.append("rect")
	.attr("class", "background")
	.attr("width", 0)
	.attr("height", 0)
	.on("click", click1);

	projection1 = d3.geo.mercator()
//	.center([50.0, 1])
	.scale(500)
	.translate([50, height-50]);
    path1 = d3.geo.path()
	.projection(projection1);

    d3.json("static/json/ap-dist_topo.json", function(error, ap_topo_json) {

    var subunits = topojson.feature(ap_topo_json, ap_topo_json.objects["ap-dist"]);

    regions1.selectAll("path")
    .data(subunits.features)
    .enter().append("path")
    .attr("class", "region")
//    .style("fill", function(d,i){
//        var region_id = d.id;
//        var region_name = _.map(
//        d.properties.name.split(/\s+/),
//        function(e){ return _.str.capitalize(e.toLowerCase()); }
//        ).join(" ");
//
//
//        return d3.rgb(20,216,18);
//
//    })
    .attr("id", function(d){ return d.id; } )
    .attr("d", path1)
    .on("click", click1)
    .on("hover", hover);

	});




    function click1(d,i) {

        svg1.selectAll('path').attr('class','region');
      d3.select(this).attr('class','selected');

        console.log(d.properties.id)
        var url = '/get_constituencies_by_district';
        var data = {};
        data.district_id = d.properties.id;
        data.csrf_token = csrf_token;


        $('.loader').css('display','block');
        $.ajax({
              type: "POST",
              url: url,
              data: data,

              success: function(response){
              console.log(response);

              resp_json = JSON.parse(response);
              if(resp_json.status == 0){
                swal('Error',resp_json.errors[0]);
              }
              else{
                var consti = $('#constituencies');
    //            console.log(resp_json);
                var data = resp_json.data.children;
                populate_constituencies(data);

              }
              $('.loader').css('display','none');

              }
          });



    }

    var populate_constituencies = function(data){

        var max_len = data.length/2;
        var div1='';
        var div2='';
        $.each(data,function(index,value){
            if(index <= max_len){
                div1 = div1 +
                        '<button data-toggle="modal" href="#" onclick="details('+
                        value[0] +
                        ')" class="list-group-item list-group-item-action">' +
                        value[1] + '</button>'
            }
            else{
            div2 = div2 +
                        '<button data-toggle="modal" href="#" onclick="details('+
                        value[0] +
                        ')" class="list-group-item list-group-item-action">' +
                        value[1] + '</button>'
            }


        });


        $('#first_div').html(div1);
         $('#second_div').html(div2);

    }


    function hover(d,i){

    }

    details = function(clicked_value){
    console.log(clicked_value);

    var mymodal = $('#myModal');
    console.log(mymodal);
//    mymodal.show();
    $('#myModal').modal('show');
    }


});
