
function isNumeric (evt) {
 
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    
    var regex = /[0-9\b]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }  

function validateMobileNo()
{

  var regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;
  var mobile_num=$("#mobile").val();
 
  console.log(mobile_num);
    if(regex.test(mobile_num))
    {
      
              return true;
    }
    else
    {
      
      return false;
    }





  }

function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile_image')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                
                $('#profile_image').css("display","block")
                reader.readAsDataURL(input.files[0]);
            }
        }

 function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }


  function validatevoter_id()
{
  var regex = /^[A-Za-z]{2,3}[0-9]*$/g;
  var voter_id=$("#voter_id").val();
  var length=voter_id.length;
  if(length>=10 && length<=14){
    if(regex.test(voter_id))
    {
      
      return true;
      
    }
    else
    {
     
      return false;
      
    }
      
 

}


  } 

function validateform(){

          var name=document.getElementById("name").value;
          
         if( name.trim().length==0)
         {
          swal("Error","Name Field Should not be empty.","error")
          return false;
         }
           var email=document.getElementById("email").value;
         if( email.trim().length==0)
         {
          swal("Error","email Field Should not be empty.","error")
          return false;
         }
         
         var city_town=document.getElementById("city_town").value;
         if( city_town.trim().length==0)
         {
          swal("Error","Place of Living Field should not be empty.","error")
          return false;
         }
         var dob=document.getElementById("dob").value;
         if( dob.trim().length==0)
         {
          swal("Error","Date Of Birth Field should not be empty.","error")
          return false;
         }
          var phone=document.getElementById("mobile").value;
         if( !validateMobileNo())
         {
          swal("Error","Please Enter a Valid Phone Number.","error")
          return false;
         }
          
          if($("[name='skills[]']:checked").length == 0)
          {
            swal("Error","Please Select atleast one Skill.","error")
          return false;
          }
          if($("[name='socialmediachannels[]']:checked").length == 0)
          {
            swal("Error","Please Select atleast one Social Media.","error")
          return false;
          }
         var voter_id=$("#voter_id").val();
             
              if (voter_id!='')
              {
                if(!validatevoter_id()){
                  swal("Error","please enter valid Voter ID","error");
                  return false;
                }
              }

              var aadhar_id=$("#aadhar_id").val();
             
              if (aadhar_id!='')
              {
                if(aadhar_id.length!=12)
                {
                  swal("Error","please enter valid Aadhar ID","error");
                  return false;
                }
                
              } 
              
                
             
          var captcha_response = grecaptcha.getResponse();
          // if(captcha_response.length == 0)
          // {
              
          //     swal('Please verify google captcha',"Error","error");
          //     return false;
          // }

          return true;
          
      };



 function ShowHideDiv(chksocialmedia,div_id,element) {
  
 
    if(chksocialmedia.checked)
      {
        $("#"+div_id).css("display","block");
         $('#'+element).prop('required',true);
      }
      else
      {
        $("#"+div_id).css("display","none");
         $('#'+element).prop('required',false);
      }
  

 
           
        }
  

