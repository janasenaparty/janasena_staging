function setCookie(c_name, value, expdays) {
    var expdate = new Date();
    expdate.setDate(expdate.getDate() + expdays);
    var c_value = escape(value) + ((expdays == null) ? "" : "; expires=" + expdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}
function getCookie(c_name, defaultvalue) {
    var i, x, y, cookies = document.cookie.split(";");
    for (i = 0; i < cookies.length; i++) {
        x = cookies[i].substr(0, cookies[i].indexOf("="));
        y = cookies[i].substr(cookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return defaultvalue;
}
// This functions adds option to drop down list
function addOption(elemid, text, value, selected) {
    var elem = document.getElementById(elemid);
    var newopt = document.createElement('option');
    newopt.text = text;
    newopt.value = value;
    newopt.selected = !(!selected);
    elem.add(newopt);
}
//
lang_divs ={
    "telugu": [{
        "top1": "జనవాక్యమే జనసేనకు శిరోధార్యం! అదే జనస్వరం !!!!!",
        "top2": "జనస్వరం, ప్రజల కోసం ప్రజలే విధాన రూపశిల్పులుగా , మార్గనిర్దేశకులుగా అవతరించిన జనవేదిక.",
        "top3": "విలువైన సూచనలు అమూల్యమైన సలహాలు లోతైన అభిప్రాయలు  ఉన్న వ్యక్తులకి నూతన నాయకత్వం పారదర్శక విధానాలు రూపకల్పనలో పాల్గొనే అవకాశం!!! రండి పాలుపంచుకోండి",
        "preferedlang" : "sdfds : ",
        "name" : "పేరు",
        "profession" : "వృత్తి",
        "email" : "ఈ-మెయిల్ ",
        "phoneno" : "ఫోన్ నెంబర్",
        "policyissue" : "విధి విధానాలు",
        "suggestionlabel" : "సలహా / అభిప్రాయం / ఆలోచన ",
        "attachements" : "Attachments",
        "attachements_hint" : "Please attach supporting documents or videos",
        "selectone" :"ఎంచుకోండి",
        "academician" :"విద్యావేత్తలు",
        "student" :"విద్యార్థి",
        "selfemployed" :"స్వయం ఉపాధిపరుడు",
        "housewife" :"గృహిణి",
        "journalist" :"విలేఖరి",
        "Doctor":"వైద్యుడు ",
        "Lawyer":"న్యాయవాది ",
        "SoftwareEngineer":"సాఫ్ట్ వేర్ ఇంజనీర్",
        "other" :"ఇతరులు",
        "landacquisition" :"భూసమీకరణ",
        "minningissues" :"గనులు - వాటి సమస్యలు",
        "agriculture" :"వ్యవసాయం",
        "StartupsandInnovation":"అంకుర సంస్థలు & వినూత్న ఆవిష్కరణలు",
        "ScienceandTechnology":"సైన్స్ & టెక్నాలజీ",
"FitnessandMartialArts":"దేహ దారుఢ్యం & మల్ల విద్యలు",
"Castebasedoccupationsandrelatedissues":"కుల వృత్తులు -పరిరక్షణ",
"Naxalism":"నక్సలిజం",

"HealthCare":"ఆరోగ్యం",
"SeniorCitizensandpensionersissues":"సీనియర్ సిటిజన్లు & పెన్షనర్ల సమస్యలు",
"SelfEmploymentandVocationaltraining":"వృత్తి విద్య శిక్షణ & స్వయం ఉపాధి",
"CivilRightsandLiberties":"పౌర హక్కులు",
"SportsandInfrastructure":"క్రీడలు & వసతుల",
"AgricultureCreditandRuralBanking":"వ్యవసాయ రుణాలు & గ్రామీణ బ్యాంకులు",
"UrbanPlanningandRuraldevelopmen":"పట్టణాభివృద్ధి & గ్రామీణాభివృద్ధి",
"PoultryDairyandAnimalhusbandry":"పాల ఉత్పత్తులు , కోళ్ల పెంపకం, పశుపోషణ",
"Socialwelfareandrelatedissues":"సాంఘిక సంక్షేమం & సమస్యలు",
"Tribalwelfareandissues":"గిరిజన సంక్షేమం & సమస్యలు",
"LiquorandExcisepolicy":"మద్యం & ఆబ్కారీ విధానం",
"CultureandArts":"సంస్కృతి & కళలు",
"PublicTransportationandSafety":"ప్రజా రవాణా & సురక్షిత ప్రయాణం",
"Corruption":"అవినీతి",
"DemographicDividendandEmploymentopportunities":"యువత & ఉపాధి కల్పన",
"LawandOrder":"శాంతి భద్రతలు",
"Tourism":"పర్యాటకం",
"Industrialization":"పరిశ్రమలు",
"Irrigation":"నీటిపారుదల & ప్రాజెక్టులు",
"Education":"విద్య",
"WomanSafetyandEmpowerment":"మహిళా భద్రత & సాధికారిత",
"Pollution":"కాలుష్యం",
"Horticulture":"పుష్పాలు & పండ్ల తోటల పెంపకం",
"Judiciary":"న్యాయవ్యవస్థ"
    }],
    "hindi": [{
        "top1": "जन की बात में आपका स्वागत हैं ! जन की बात नागरिक समाज की आवाज़ हैं - यानी आपकी आवाज़ हैं !",
        "top2": "'जनस्वर' ऐसा मंच हैं जो सार्वजनिक नीति निर्माण की दिशा में आपको ले जाता हैं ! यहाँ पर आपके व्यावहारिक विचार, राय और सुजाव कई मुद्दों पर आगे बड़ कर दे सकते हैं !",
        "top3": "नेतृत्व आपका,  कार्यक्रम हम सबका !!",
        "preferedlang" : "preferred language : ",
        "name" : "नाम",
        "profession" : "पेश",
        "email" : "ई-मेल",
        "phoneno" : "फ़ोन नंबर",
        "policyissue" : "नीति मुद्दा",
        "suggestionlabel" : "सुजाव / राई / विचार",
        "attachements" : "Attachments ",
        "attachements_hint" : "Please attach supporting documents or videos",
        "selectone" :"किसी एक को चुनिये",
        "academician" :"अकैडेमिशन",
        "student" :"छात्र",
        "selfemployed" :"स्वनियोजित",
        "housewife" :"घरेलु महिला",
        "journalist" :"पत्रकार",
        "Doctor": "चिकित्सक",
        "Lawyer":"वकील",
        "SoftwareEngineer":"सॉफ्टवेयर इंजीनियर",
        "other" :"वगैरा",
        "landacquisition" :"भूमि अर्जन",
        "minningissues" :"खननं मुद्दे",
        "agriculture" :"कृषि",
        "StartupsandInnovation":"स्टार्टअप और इनोवेशन",
        "ScienceandTechnology":"विज्ञान और तकनीक",
"FitnessandMartialArts":"फिटनेस और मार्शल आर्ट्स",
"Castebasedoccupationsandrelatedissues":"जात पात मुद्दे ",
"Naxalism":"नक्सलवाद",

"HealthCare":"स्वास्थ्य देखभाल",
"SeniorCitizensandpensionersissues":"वरिष्ठ नागरिक और पेन्शनभोगी मुद्दे",
"SelfEmploymentandVocationaltraining":"स्वयंरोजगार और व्यवसाहिक परिक्षण",
"CivilRightsandLiberties":"नागरिक अधिकार और स्वतंत्रता",
"SportsandInfrastructure":"खेल के मुद्दे ",
"AgricultureCreditandRuralBanking":"कृषि श्रण और ग्रामीण बैंकिंग",
"UrbanPlanningandRuraldevelopmen":"शहरी नियोजन और ग्रामीण विकास",
"PoultryDairyandAnimalhusbandry":"मुर्गी पालन , डेरी और पशुपालन",
"Socialwelfareandrelatedissues":"सामजिक कल्याण",
"Tribalwelfareandissues":"जन जातीय कल्याण",

"LiquorandExcisepolicy":"शराब और एक्साइज नीति",
"CultureandArts":"संस्कृति और कला",
"PublicTransportationandSafety":"सार्वजनिक परिवहन और सुरक्षा",
"Corruption":"ब्रष्टाचार",
"DemographicDividendandEmploymentopportunities":"जनसांख्यिकीय लाभांश और रोजगार के अवसर",
"LawandOrder":"कानून व्यवस्थ",
"Tourism":"पर्यटन",
"Pollution":"प्रदुषण",
"Industrialization":"औद्योगिकीकरण",
"Irrigation":"सिंचाई",
"Education":"शिक्षा",
       "Horticulture":"बागबानी",
        "Judiciary":"न्यायपालिका",

     "WomanSafetyandEmpowerment":"महिला सुरक्षा और सशक्तिकरण"
    }],
    "english": [{
        "top1": "Janasena Party stands for strengthening and empowering civil society.",
        "top2": "Introducing Janaswaram, a platform for public participation in policy making.",
        "top3": "People with insightful ideas, opinions and suggestions will get a chance to participate in emerging leadership programs & policy workshops.",
        "preferedlang" : "preferred language : ",
        "name" : "Name",
        "profession" : "Profession",
        "email" : "Email",
        "phoneno" : "Phone Number",
        "policyissue" : "Policy Issues",
        "suggestionlabel" : "Suggestion/Opinion/Idea ",
        "attachements" : "Attachments ",
        "attachements_hint" : "Please attach supporting documents or videos",
        "selectone" :"Select one",
        "academician" :"Academician",
        "student" :"Student",
        "Doctor" :"Doctor",
        "SoftwareEngineer":"SoftwareEngineer",
        "selfemployed" :"Selfemployed",
        "housewife" :"Housewife",
        "journalist" :"Journalist",
        "other" :"Other",
        "landacquisition" :"Land Acquisition",
        "minningissues" :"Mining & Issues",
        "agriculture" :"Agriculture",
        "StartupsandInnovation":"Startups and Innovation",
        "ScienceandTechnology":"Science and Technology",
"FitnessandMartialArts":"Fitness and Martial Arts",
"Castebasedoccupationsandrelatedissues":"Caste based occupations and related issues",
"Naxalism":"Naxalism",

"HealthCare":"Health Care",
"SeniorCitizensandpensionersissues":"Senior Citizens & pensioners issues",
"SelfEmploymentandVocationaltraining":"Self Employment & Vocational training",
"CivilRightsandLiberties":"Civil Rights & Liberties",
"SportsandInfrastructure":"Sports & Infrastructure",
"AgricultureCreditandRuralBanking":"Agriculture Credit & Rural Banking",
"UrbanPlanningandRuraldevelopmen":"Urban Planning & Rural development",
"PoultryDairyandAnimalhusbandry":"Poultry, Dairy & Animal husbandry",
"Socialwelfareandrelatedissues":"Social welfare & related issues",
"Tribalwelfareandissues":"Tribal welfare & issues",
"Pollution":"Pollution",
"LiquorandExcisepolicy":"Liquor & Excise policy ",
"CultureandArts":"Culture & Arts",
"PublicTransportationandSafety":"Public Transportation & Safety ",
"Corruption":"Corruption",
"DemographicDividendandEmploymentopportunities":"Demographic Dividend & Employment opportunities",
"LawandOrder":"Law & Order",
"Tourism":"Tourism",
"Industrialization":"Industrialization",
"Irrigation":"Irrigation",
"Education":"Education",
       "Horticulture":"Horticulture",
"Socialwelfareandrelatedissues":"Socialwelfare and related issues",
     "WomanSafetyandEmpowerment":"Woman Safety and Empowerment"
    }]
}

function replacelabels(data){
    console.log(data);
    $.each(data[0],function(key,value){
            console.log(key+":"+value);
            $("."+key).text(value);
        });
}

var tips = [], currenttip = 0, turnoff = false, piresourcebase='';
// Callback function which gets called when user presses F9 key --.
function scriptChangeCallback(lang, kb, context) {

    
    //alert("check : "+lang)--;
   // alert(lang_divs);
    replacelabels(lang_divs[lang]);
    //console.log(lang_divs[lang]);
    
    // Change the dropdown to new selected language.
    //document.getElementById('cmdhelp').className = (lang == 'english' ? 'disabled' : '');

    var icon = pramukhIME.getIcon(4);
    // PramukhIME toolbar settings
    //document.getElementById('cmdhelp').style.background = "transparent url('"+piresourcebase+"img/" + icon.iconFile + "') " + (lang == 'english' ? 100 : -1 * icon.x) + "px " + (lang == 'english' ? 100 : -1 * icon.y) + "px no-repeat";

    /*var i, dd = document.getElementById('drpLanguage');
    for (i = 0; i < dd.options.length; i++) {
        if (dd.options[i].value == kb + ':' + lang) {
            dd.options[i].selected = true;
        }
    }*/
    // Change the image
   //-- document.getElementById('pramukhimecharmap').src = piresourcebase + 'img/' + pramukhIME.getHelpImage();
    /*var filename = pramukhIME.getHelp();
    if (filename != '') {
        document.getElementById('pramukhimehelpdetailed').src = piresourcebase + 'help/' + filename;
    }
    setCookie('pramukhime_language', kb + ':' + lang, 10);*/




}
// Changing the language by selecting from dropdown list
function changeLanguage(newLanguage) {
    //alert(newLanguage);
    if (!newLanguage || newLanguage == "")
        newLanguage = 'english';
    // set new script
    var lang = newLanguage.split(':');
    pramukhIME.setLanguage(lang[1], lang[0]);
}
function showHelp(elem) {
    if (elem.className && elem.className == 'disabled') {
        return false;
    }
    showDialog('Pramukh Type Pad Help');
    document.getElementById('pramukhimehelp').style.display = 'block';
    selectHelpType();
    return false;
}
function closeDialog() {
    document.getElementById('blocker').style.display = 'none';
    document.getElementById('dialog').style.display = 'none';
    document.getElementById('typingarea').focus();
    return false;
}
function showDialog(title) {
    document.getElementById('blocker').style.display = 'block';
    document.getElementById('dialog').style.display = 'block';
    document.getElementById('dialogheader').innerHTML = title;
    document.getElementById('pramukhimehelp').style.display = 'none';
}
function selectHelpType() {
    var rdolist = document.getElementsByName('helptype');
    if (rdolist[1].checked) {
        document.getElementById('pramukhimehelpquick').style.display = 'none';
        document.getElementById('pramukhimehelpdetailed').style.display = 'block';
    } else {
        document.getElementById('pramukhimehelpquick').style.display = 'block';
        document.getElementById('pramukhimehelpdetailed').style.display = 'none';
    }
}
function showNextTip() {
    currenttip++;
    if (currenttip > tips.length) {
        currenttip = 1; // reset tip
    }
    var tipelem = document.getElementById('pi_tips'), li;
    // get first child node
    var elem, len = ul.childNodes.length, i;
    for (i = 0; i < len; i++) {
        elem = ul.childNodes[i];
        if (elem.tagName && elem.tagName.toLowerCase() == 'li') {
            li = elem;
            break;
        }
    }
    if (!turnoff && li) {
        li.innerHTML = ''; // clear
        li.appendChild(document.createTextNode('Tip ' + currenttip + ': ' + tips[currenttip - 1] + ' '));
        elem = document.createElement('a');
        elem.href = 'javascript:;';
        elem.innerHTML = 'Turn Off';
        elem.className = 'tip';
        elem.onclick = turnOffTip;
        li.appendChild(elem);
        setTimeout('showNextTip()', 10000);
    } else if (li) {
        tipelem.removeChild(li);
    }

}
function turnOffTip() {
    turnoff = true;
    showNextTip();
}
