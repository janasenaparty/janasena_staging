// IE 11 Fix
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
      'use strict';
      if (typeof start !== 'number') {
        start = 0;
      }
  
      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }
  // End IE Fix
  $ (document).ready(function() {
  // Globals
    var amountSofarFrom121 = 0;
    var firstPage121Id = 0;
    
    var mobileErrorMsg  = $('#mobile-error').text();
  
    
    
 
    $('[data-toggle="tooltip"]').tooltip();
  
   
  
    loadCookies();
  
    /*
    $('input, textarea').keyup(function() {
      validateElement(this.id);
    });
  
    $('select').change(function() {
      validateElement(this.id);
    });
    */
    function addUrlParam(param, url) {
      var parts = url.split('?');
      var newUrl = '';
      if(typeof parts[1] !== 'undefined'){
        parts[1] = parts[1].replace(/[&]{0,1}language=[a-z]{2}/,'');
        if(param !== '')
          newUrl = parts[0] + '?'+ param;
        else
          newUrl = parts[0];
        if(parts[1] !== '')
          newUrl += '&' + parts[1];
  
        newUrl = newUrl.replace('&&','&');
        newUrl = newUrl.replace('/&','/?');
        return newUrl;
      }
      if(param !== '')
        return parts[0] + '?'+ param;
      else
        return parts[0] ;
    }
 
  
    // Validate all visibile components of a page
    function pageValidateVisible() {
      var validationStatus = true;
      $('input:visible, select:visible, textarea:visible').each(function() {
          if(this.name != '') { // Validate named elements only
            if(!validateElement(this.id)) {
              validationStatus = false;
              $([document.documentElement, document.body]).animate({scrollTop: $('#'+this.id).offset().top -100}, 500);
              return false;
            }
          }
      });
      return validationStatus;
    }
    // Check if an array contains an element
    function arrayContains(array, element) {
      for (var i = 0; i < array.length; i++) {
        if (array[i] === element) {
          return true;
        }
      }
      return false;
    }
    // On focusout from form element, validate it immediately
    $('#main-form input, select, textarea').focusout(function() {
      if(!this.id.includes('btn') && this.name != '')
        validateElement(this.id);
    });
    $('#main-form select').change(function() {
      if(arrayContains(['state', 'forstate', 'fordistrict', 'forloksabha', 'forvidhansabha'], this.id))
        validateElement(this.id);
    });
  
    // Return true if injection detected else false
    function injectionValidator(value) {
      var keywords = ["--", "/*", "*/", "@@",
                    " char ", " nchar ", " varchar ", " nvarchar ",
                    " alter ", " begin ", " cast ", " create ", " cursor ",
                    " declare ", " delete ", " drop ", " end ", " exec ",
                    " execute ", " fetch ", " insert ", " kill ",
                    " select ", " sys ", " sysobjects ", " syscolumns ",
                    " table ", " update "];
      value = value.toLowerCase();
      for (var i = 0; i < keywords.length; i++) {
        if(value.includes(keywords[i])) {
          return true;
        }
      }
      return false;
    }
    function validateElement(element) {
      validator = element + 'Validator()';
      element = '#'+ element;
      elementType = $(element).prop('nodeName');
      var errorElement = element + '-error';
      switch(elementType) {
        case 'INPUT':
        case 'TEXTAREA':
          if(!eval(validator) || injectionValidator($(element).val())) {
            if(errorElement == '#mobile-error') {
              var errorMsg = mobileErrorMsg.replace('__COUNTRY__', $('#country option:selected').text());
              $(errorElement).text(errorMsg);
            }
            $(errorElement).show();
            return false;
          }
        case 'SELECT':
          if($(element).val() == 0) {
            $(errorElement).show();
            return false;
          }
      }
      $(errorElement).hide();
      return true;
    }
  
    // All validation functions below
    function amountValidator() {
      var amount = $('#amount').val();
      if(/^[0-9]{1,7}$/.test(amount) == false)
        return false;
      amount = parseInt(amount);
      if($('#recurring').hasClass('btn-primary'))
        var min = 100;
      else
        var min = 9;
      if( !amount || amount < min || amount > 1000000)
        return false;
      return true;
    }
  
 
    
  
    function mobileValidator() {
      var mobile = $('#mobile').val();
          var countryCode = $("#country option:selected").val();
          var country = $("#country option:selected" ).text();
          var parsed = new libphonenumber.parseNumber(mobile, countryCode);
          if(jQuery.isEmptyObject(parsed))
        return false;
      return true;
    }
  
   
  
  
  
   
  
  
    function startdateValidator() {
      return true;
    }
  
    function vpaValidator() {
      var vpa = $('#vpa').val();
      var re = /^[A-Za-z0-9\.\-\_]{1,32}@[A-Za-z0-9\.\-_]{1,32}$/;
      return re.test(vpa);
    }
  
   
   
  
  
  
  
     
    // Get a date ndays into the future as yyyy, mm, dd
  
  
    // Save form input fields and checkboxes into cookies.
  
  
  
  
  
    // Capitalize first letter of string
    function ucfirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    // Given key value pair JSON, a key or part of it and a select dropdown  id
    // Fill it with the options from data. Used to fill Districts, Vidhansabha and Loksabha dropdowns
    function setSelects(data, selected, select) {
      $(select).empty();
      var unit = ucfirst(select.replace('#for', ''));
      var options = "<option selected value=\"0\">Select a " + unit + " to donate to</option>\n";
      for(var key in data) {
        if(key.includes(selected))
          options += '<option value="' + key + '">' + data[key] + '</option>';
      }
      $(select).html(options);
    }
  
    //
    function appLog(msg, data) {
      $.post( "appLog.php", {msg:msg, data: data}, function() {
        $('#donateform').submit();
  
      }).fail(function(data) {
        //alertMsg("Could not post to 121 server! Data: " + JSON.stringify(data));
      });
    }
    // Prepares data to be posted to 121. Assumes data is validated. No validation done  here.
    // Throws an ERROR ALERT if any of the expected fields are missing
    // Checks if any of the required fields are missing, if so adds defaults
    // Maps between our names and their names when they are different
    // Following additional logic should be applied at validation time AND here
    // Additional logic: Mandatory PAN if Amount so far > 20K, Passport if country != IN, vpa for UPI *NOT DONE*
    function prepareFor121Post(data) {
      // WARNING! We will end up passing empty parameters if we donot get a required parameter in data,
      // without flagging an error or loging it.
      var preparedData = {
        "name": "", "country": "", "state": "","email": "", "mobile": "", "address": "", "address1": "", "gateway": "3",
        "pan": "","forlevel": "National","forstate": "0","fordistrict": "0","forls": "0","forvs": "0","forward": "0",
        "frequency": "0","remark": "", "passportno": "","utm_source": "","utm_medium": "","utm_term": "",
        "utm_content": "","utm_campaign": "","fullurl": "","referer": "","vpa": "","userid": "", "pincode": "",
        "startdate": "", "amount": "", "obscurestatus":"", "declaration":"" };
  
      // map our keys to their keys when different
      var keyMap = {'payment-mode': 'gateway', 'forloksabha': 'forls', 'forvidhansabha': 'forvs', 'passport': 'passportno', 'contactme': 'obscurestatus' };
      // Mandatory keys of 121 for post
      var mandatoryNonEmpty = ["name", "country", "email", "mobile", "address", "gateway", "forlevel", "amount"];
      for(var key in data) {
        var mappedKey = (typeof keyMap[key] !== 'undefined') ? keyMap[key] : key;
        if(typeof data[key] != 'undefined')
          preparedData[mappedKey] = data[key];
      }
      mandatoryNonEmpty.forEach(function(key) {
        if(typeof preparedData[key] === 'undefined') {
          appLog('Critical Error in prepareFor121Post: undefined field ' + key, data);
          return false;
        }
               if(preparedData[key].length == 0) {
          appLog('Critical Error in prepareFor121Post: empty field ' + key, data);
          return false;
              }
          });
      if(preparedData['forstate'] !== "0") {
        preparedData['forlevel'] = 'State';
      }
      if(preparedData['fordistrict'] !== "0") {
        preparedData['forlevel'] = 'District';
        preparedData['fordistrict'] = preparedData['fordistrict'].split('-')[1];
      }
      if(preparedData['forls'] !== "0") {
        preparedData['forlevel'] = 'Loksabha';
        preparedData['forls'] = preparedData['forls'].split('-')[1];
      }
      if(preparedData['forvs'] !== "0") {
        preparedData['forlevel'] = 'Vidhansabha';
        preparedData['forvs'] = preparedData['forvs'].split('-')[1];
      }
      preparedData['fullurl'] = fullURL;
      var mobile = preparedData['mobile'];
      var country = preparedData['country'];
      var phoneNo = '+' + libphonenumber.getPhoneCode(country) +  libphonenumber.parse(mobile, country).phone;
      mobile = phoneNo.replace('+91', '');
      mobile = mobile.replace('+', '00');
      preparedData['mobile'] = mobile;
      if(typeof urlParamsJson !== 'undefined') {
        $.each(urlParamsJson, function(key, value) {
          preparedData[key] = value;
        });
      }
      preparedData['noofmonths'] = $('#payfor-nmonths').val();
      preparedData['UniqueID'] = firstPage121Id;
      if(preparedData['startdate'].length != 0) {
        var startdate = preparedData['startdate'];
        var tmp = startdate.split('/');
        preparedData['startdate'] = tmp[2] + '-' + tmp[1] + '-' + tmp[0];
      }
      return preparedData;
    }
  
 
    // Page 1 proceed clicked, validate page 1 data - Needs tighter validation
    $('#btn-proceed').click(function() {
      if(!pageValidateVisible())
        return false;
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
      mobile = mobile.replace('+91', '');
      mobile = mobile.replace('+', '00');
          var countryCode = $('#country').val();
      var amount = $('#amount').val();
      var recurType = $('#recur-type').val();
      var obscurestatus = $('#contactme').is(':checked') ? 'Yes' : 'No';
      var frequency = (recurType == 'donateonce') ? 0: 1;
      saveCookies();
      $('#page-1').hide();
      $('#page-2').show();
      $('#btn-back').show();
      $([document.documentElement, document.body]).animate({scrollTop: $('#'+this.id).offset().top -100}, 500);
      var jqxhr = $.post( "api121Request.php", {uniqueid: firstPage121Id, name: name, email: email, mobile: mobile, country: countryCode, amount: amount, frequency: frequency, obscurestatus: obscurestatus}, function(data) {
          //alertMsgEx(data, 'alert-info');
          data = JSON.parse(data);
          amountSofarFrom121 = data.DonationAmount;
          firstPage121Id = data.UniqueID;
          //alertMsgEx("Total Amount: " + amountSofarFrom121 + ", UniqueID: " + firstPage121Id, 'alert-info');
        }).fail(function(data) {
          alertMsg("Network not available. Check your internet connection and try again...");
      });
      addPanIfNeeded();
          return true;
      });
    // collect input, select, textarea, checkbox from page 1 and all visible input select, checkbox, textare from page 2
    // Assemble a json. Add additional parameters needed
    // Put it into the new form, single field. Submit the form.
    $('#btn-submit').click(function() {
      var postData = {};
      if(!pageValidateVisible())
        return false;
      $('#btn-submit').attr("disabled", true);
      $('#btn-submit').text('Loading...');
      postData['amount'] = $('#amount').val(); // Since this is outside page 1 and 2 and always displayed
      postData['frequency'] = $('#recurring').hasClass('btn-primary') ? 1 : 0;
      switch($('#recur-type').val()) {
        case 'eNach' :
          postData['gateway'] = 6;
          break;
        case 'credit-card':
          postData['gateway'] = 3;
          break;
        default:
          postData['gateway'] = $('payment-mode').val();
      }
      $('#page-1 input, #page-1 select, #page-1 textarea').each(function() {
        if(this.hasAttribute('name'))  // Skip buttons with no name on them
          postData[this.id] = this.value;
      });
      $('#page-2 input, #page-2 select, #page-2 textarea').each(function() {
        if($(this).is(":visible")) {
          if(this.hasAttribute('name')) {  // Skip buttons with no name on them
            postData[this.id] = this.value;
          }
        }
      });
  
      postData = prepareFor121Post(postData);
      postDataJson = JSON.stringify(postData);
      saveCookies();
      $([document.documentElement, document.body]).animate({scrollTop: $('#'+this.id).offset().top -100}, 500);
      $('#donatedata').val(postDataJson);
      appLog("Post", postData); // This actually does the submit after writing log to our file. Need to change later.
    });
    // Go back to page 1 on back button click
   
  
    
 
  
    function changeCountry() {
          var country = $('#country').val();
          if(country != 'IN') {
              $('#as-in-pp').show();
        $('#stateDiv').hide();
        $('#passport-ctl').show();
      } else {
              $('#as-in-pp').hide();
        $('#stateDiv').show();
        $('#passport-ctl').hide();
      }
      var phoneCountryCode = '+' + libphonenumber.getPhoneCode(country);
      $('#mobile').val(phoneCountryCode + ' ');
    }

  
 
  
      $('#country').change(function() {
      changeCountry();
      });
  
    
   
    
   

  
    $('#eNach').click(function() {
      $('#recur-type').val('eNach');
      changeRecurType();
    });
  
  
   
  
    // If btn-recur is enach, startdate should be today + 15 days
  
    $(".btn-recur").click(function () {
      $( ".btn-recur" ).each(function( index ) {
        $(this).removeClass('btn-primary');
      });
      $(this).addClass('btn-primary');
      $('recur-type').val($(this).name);
      addPanIfNeeded();
      if(this.name != 'donateonce')
        changeStartDate(this.name);
    });
  
  
  });
  