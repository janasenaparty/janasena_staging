$('#report_level').trigger('change');

var district_id = 0;
var assembly_id = 0;
var polling_id = 0;
var $name = $('#name');
var $voter_prefill = $('#voter_prefill');
var $user_fill_data = $('#user_fill_data');
var $village = $('#village');
var $jsp_id = $('#jspid');
var $mobile = $('#mobile');
var $voter_div = $('#voter_div');

$('#advanced_filter_btn').click(function(){
    district_id = $('#district_select').val();
    assembly_id = $('#value_select').val();
    if (district_id =='' || district_id ==undefined || district_id == null){
        swal('Error','Please select a district','error');
    }
    else if (assembly_id =='' || assembly_id ==undefined || assembly_id == null){
        swal('Error','Please select an assembly','error');
    }
    else{
        var data = {
        'district':district_id,
        'assembly':assembly_id
        }
        $.ajax({

          url: '/pbl/get_pb_list',
          data: data,
          method:'POST',
          success: function(response){
    //        console.log(response);
            var data = JSON.parse(response);
            var mlist = data['data'];
    //        console.log(mlist);
    //        $('#polling-booth-table').html('');
            var header_string = "<thead><tr><th>Name</th><th>Location</th><th>Add</th></tr></thead>"
            var tr_string = "<tr><td>name</td><td>location</td><td><button class='btn btn-success' onclick='showmodal(polling_id)'>Add Leader</Button></td></tr>"
            var tot_tr = '<tbody>'
            for(var i=0;i<mlist.length;i++){
                tot_tr+=tr_string;
    //            console.log(typeof(mlist[i][1]),mlist[i][1]);
                var name = typeof(mlist[i][1]) == 'string'?mlist[i][1]:'';
                var location = typeof(mlist[i][2]) == 'string'?mlist[i][2]:'';
    //            console.log(name);
                tot_tr = tot_tr.replace("name",name);
    //            console.log(tot_tr);
                tot_tr = tot_tr.replace("location",location);
                tot_tr = tot_tr.replace("polling_id",mlist[i][0]);
    //            console.log(tot_tr);
            }
            tot_tr = tot_tr +'</tbody>'
    //        console.log(tot_tr);
            tot_tr = header_string+tot_tr;
    //        console.log(tot_tr);
            $('#polling-booth-table').html(tot_tr);
          }
        });

    }




});

var showmodal = function(p_id){
    polling_id = JSON.parse(JSON.stringify(p_id));;
    console.log(polling_id);
    console.log('I am here');
    var data  = {};
    data['polling_booth_id'] = polling_id;
    data['district_id'] = district_id;
    data['assembly_id'] = assembly_id;
    $.ajax({

      url: '/pbl/fetch_leaders',
      data: data,
      method:'POST',
      success: function(response){
      console.log(response);
        var resp = JSON.parse(response);
        var errors = resp['errors']
        if(errors.length==0){
            var leader_list = resp['data'];
            console.log(leader_list,leader_list.length);
            if(leader_list.length == 0){
                $('#voter_div').addClass('show').removeClass('hide');
                $('#polling-leader-list').html();
                $('#polling-leader-list').addClass('hide').removeClass('show');
            }
            else{
             $('#user_fill_data').addClass('hide').removeClass('show');
                $('#polling-leader-list').addClass('show').removeClass('hide');
                var tr_string = ''
                for(var i=0;i<leader_list.length;i++){
                    tr_string += "<tr><td>id</td><td>name</td><td>village</td><td>jspid</td><td>mobile</td></tr>"
                                    .replace('id',leader_list[i][0]).replace('name',leader_list[i][1])
                                    .replace('village',leader_list[i][2]).replace('jspid',leader_list[i][3])
                                    .replace('mobile',leader_list[i][4])
                }
                $('#polling-leader-list').html(tr_string);

            }

        }



      }
      });
//    fetch the leaders assigned
//      If no leaders show a button to create a leader
    $('#myModal').modal('show');
    console.log('after toggle');
}

var add_leader = function(){
    console.log(polling_id);
    var voter_id = $('#voter_id').val();
    var name = $name.val();
    var mobile = $mobile.val();
    var jsp_id = $jsp_id.val();
    var village = $village.val();
    var data = {};
    data['voter_id'] = voter_id;
    data['district'] = district_id;
    data['assembly'] = assembly_id;
    data['pb_id'] = polling_id;
    data['user_name'] = name;
    data['mobile'] = mobile;
    data['jsp_id'] = jsp_id;
    data['village'] = village;



     $.ajax({

      url: '/pbl/add_leader',
      data: data,
      method:'POST',
      success: function(response){
        console.log(response);
        response = JSON.parse(response);
        console.log(response.status)
        if(response.status==0){
            swal('Error',response['errors'][0],'error');
        }
        else{
             swal('Success','Added leader successfully','success');
             $('#voter_id').val('');
             $name.val('');
            $mobile.val('');
            $jsp_id.val('');
            $village.val('');
            $voter_prefill.val('');
            $('#myModal').modal('hide');
            $('#add-leader-div').addClass('hide').removeClass('show');
        }

      }
      });
}
$('#myModal').on('hidden.bs.modal', function () {
    $('#voter_id').val('');
    $name.val('');
    $mobile.val('');
    $jsp_id.val('');
    $village.val('');
    $voter_prefill.val('');
})

$('#add-leader-button').click(add_leader);

var check_voter_id = function(){
//xtm0769042

    var voter_id = $('#voter_id').val();
    var data = {}
    data['voter_id'] = voter_id;
    data['polling_station_id'] = polling_id;
    data['ac_no'] = assembly_id;
     $.ajax({

      url: '/pbl/check_voter_id',
      data: data,
      method:'POST',
      success: function(response){
            console.log(response);
            response = JSON.parse(response);
            if(response.status == 1){
                $voter_div.addClass('hide').removeClass('show');
                $user_fill_data.addClass('show').removeClass('hide');
                $voter_prefill.val(voter_id);
                $voter_prefill.prop('readonly', true);
                $name.val(response.user_data.name);
                $name.prop('readonly', true);

            }
            else if(response.status == 2){
                $voter_div.addClass('hide').removeClass('show');
                $user_fill_data.addClass('show').removeClass('hide');
                $voter_prefill.val(voter_id);
                $voter_prefill.prop('readonly', true);
            }
            else if(response.status == 3){
                swal('Error','Voter id doesnot belong to the polling station.','error');
            }
            else{
                swal('Error',response['errors'][0],'error');
            }

      }

     });

}