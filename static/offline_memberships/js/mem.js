//  offline membership functions
 function isNumeric (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9\b]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  } 
  function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }           
$(document).ready(function(){

    $('#full_data').addClass('hide');
    var mandals = [];
    var states = [];
    var parliament_constituencies = [];
    var districts = [];
    var polling_booths=[];
    var ac;
    $.getJSON('static/offline_memberships/json/membership/mandals.json',function(data){
        mandals = data;
    });

    $.getJSON('static/offline_memberships/json/membership/districts.json',function(data){
        districts = data;
    });
    $.getJSON('static/offline_memberships/json/membership/constituencies.json',function(data) {
        ac =data;
    });

    $('#state_select').selectpicker({
        style: 'btn-default',
        size: false
    });
    var state_id;
    var district_id;
    var ass_id;

    // $('#state').selectpicker('refresh');
    $('#state').change(function(e){

            state_id = $('#state').val();
            console.log(state_id)
            var d =[];
             $('#assembly').html("<option value=''>select Assembly</option>");
                $('#district').html("<option value=''>select district</option>");
                $('#mandal').html("<option value=''>select mandal</option>");

            if(state_id>'2')
            {
                d=[]
              $('#district').removeAttr("required") ; 
              $('#assembly').removeAttr("required") ;
               $('#assembly').html("<option value=''>select Assembly</option>");
                $('#district').html("<option value=''>select district</option>");
                
            }
            else
            {
               d=[]
                $('#district').attr("required","required") ; 
              $('#assembly').attr("required","required") ;
              

            }
            if(state_id<='3')
            {
                 d = districts[state_id];
            }
            

           $('#district').html("<option value=''>select district</option>");
            $.each(d,function(i,val){

                $('#district').append($("<option></option>").attr("value",val.id).text(val.district_name));


            });
        });
           
            // $('#district').selectpicker('refresh');
            $('#district').change(function(dis_e){
                district_id = $("#district").val();
               
$('#assembly').html("<option value=''>select Assembly</option>");
               
                $('#mandal').html("<option value=''>select mandal</option>");
                    
                    var local_ac = ac[district_id];

                    $('#assembly').html("<option value=''>select Assembly</option>");
                    $.each(local_ac,function(i1,val1) {
                        $('#assembly').append($("<option data-stateid='" + val1.state_id + "'></option>").attr("value", val1.constituency_id).text(val1.constituency_name));
                    });

                });
                    // $('#assembly').selectpicker({
                    //     style: 'btn-default',
                    //     size: false
                    // });
                    // $('#assembly').selectpicker('refresh');
                $('#assembly').change(function(ass_e){
                    var ass_id = $('#assembly').val();
                    
                    
                    var mandalist=_.where(mandals,{state_id:parseInt(state_id), district_id:parseInt(district_id),constituency_id:parseInt(ass_id)})
                    // console.log(mandalist);
                    $('#mandal').html("<option value=''>select Mandal</option>");
                    $.each(mandalist,function(i,val){

                        $('#mandal').append($("<option></option>").attr("value",val.id).text(val.mandal_name));


                    });
                   

                });
       




// 


$('input[type=radio][name=optradio]').change(function() {
        if (this.value == 'yes') {
              $('#precheck').addClass('show').removeClass('hide');
            $('#full_data').addClass('hide').removeClass('show');
           
        }
        else  {
             $('#precheck').addClass('hide').removeClass('show');
            $('#full_data').addClass('show').removeClass('hide');
        }
    });



    $('.input2').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })


    $('.validate-form .input2').each(function(){
        $(this).focus(function(){
            hideValidate(this);
        });
    });

    function showValidate(input) {
        var thisAlert = $(input).closest('div.validate-input');
       
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    //First form validation

    var phone = $('.validate-input input[name="phone"]');
    // console.log(phone);
    var voter_id = $('.validate-input input[name="voter_id"]');
    var full_name = $('input[name="full_name"]');
    // console.log( full_name);
    var gender = $('input[name=switch_2]');
    var relation_name = $('input[name="relation_name"]');
    var assembly = $('#assembly');
    var mno = $('.validate-input input[name="mno"]');
    var vtid = $('.validate-input input[name="vtid"]');
    var email = $('.validate-input input[name="email"]');
    // var fbid = $('.validate-input input[name="fbid"]');
    var age = $('.validate-input input[name="age"]');
    var education = $('.validate-input input[name="education"]');
    var occupation = $('.validate-input input[name="occupation"]');
    var hno = $('.validate-input input[name="hno"]');
    var street = $('.validate-input input[name="street"]');
    var pincode = $('.validate-input input[name="pincode"]');
    var receipt_no = $('.validate-input input[name="receipt_no"]');
    var village = $('.validate-input input[name="village"]');
    var refid = $('.validate-input input[name="refid"]');
    var pincode = $('.validate-input input[name="pincode"]');

    var district = $('#district');
    var state = $('#state');
    var mandal = $('#mandal')
    var booth_id = 0;




    var validate_success_form1 = function(response,status,xhr){

        if(response.data.status == '0' ){

             $('#precheck').addClass('show').removeClass('hide');
            $('#full_data').addClass('hide').removeClass('show');
            swal('Error',response.errors[0],'error');
             $('.loader').css("display","none");
            return false;
        }

        else if(response.data.status == '1'){

            $('.loader').css("display","none");
            $('#precheck').addClass('hide').removeClass('show');
            $('#full_data').addClass('show').removeClass('hide');
            var data = response.data.children;
            
            if(data === undefined){
                mno.val(phone.val());
                mno.focus();
                vtid.val(voter_id.val());
                vtid.focus();

            }else{
                
                state.val(data['state']).change();
                district.val(data['district_id']).change();
                assembly.val(data['ac_no']).change();



                booth_id = data['part_no'];
                $('.selectpicker').selectpicker('refresh')


                mno.val(phone.val());
                mno.focus();
                mno.attr('disabled','disabled');
               
                vtid.val(voter_id.val());
                vtid.focus();
                vtid.attr('disabled','disabled');
                age.val(data['age']);
                age.focus();
                hno.val(data['house_no']);
                hno.focus();
                full_name.val(data['voter_name']);
                full_name.focus();
                relation_name.val(data['relation_name']);
                relation_name.focus();

                //    Male female switch
                gen = data['sex'];
                gen = gen.trim().toLowerCase();

                
                gender.attr("checked",false);
                $(switches[gen]).attr("checked", true).change();
                // gender.focus()


            }
            $('#precheck').addClass('hide').removeClass('show');
            $('#full_data').addClass('show').removeClass('hide');



        }
        else
        {
            mno.val(phone.val());
              mno.addClass('has-val');
                vtid.val(voter_id.val());
                $(vtid).addClass('has-val');
                swal('Error',response.errors[0],'error');
              $('.loader').css("display","none");
             $('#precheck').addClass('hide').removeClass('show');
            $('#full_data').addClass('show').removeClass('hide');
        }

    }
    var switches = {
        'male':'#switch_left',
        'female':'#switch_right'
    }
   
    $('#check_details').on('click',function(){
        var check = true;

        if($(phone).val().trim().match(/^[6-9]{1}[0-9]{9}$/g) == null){
            showValidate(phone);
            check=false;
        }
        if($(voter_id).val().trim().match(/^[A-Za-z]{2,3}[0-9]*$/g) == null){
            showValidate(voter_id);
            check=false;
        }

       

        if(check){
            $('.loader').css("display","block")
            var data = {};
            data['mobile_num'] = '91'+$(phone).val().trim();
            data['voter_id'] = $(voter_id).val().trim();
           

            $.ajax({
                type: "GET",
                url: "/verifyOfflineDetails",
                data:data,
                dataType:"json",

                success: validate_success_form1

            });

        }





    });


    $('#submit_details').on('click',function(){

        var check = true;


        // Full name validation for not empty

        if($(full_name).val() === ''){
            showValidate(full_name);
            check=false;
        }
        if($(age).val()==''){
            showValidate(age);
            check=false
        }

        // relation_name validation for not empty
        if($(relation_name).val() === ''){
            showValidate(relation_name);
            check=false;
        }
        if($(receipt_no).val() === ''){
            showValidate(receipt_no);
            check=false;
        }
        if($(refid).val().trim() === ''){
            showValidate(refid);
            check=false;
        }
        
        if($(mno).val().trim().match(/^[6-9]{1}[0-9]{9}$/g) == null){
            showValidate(mno);
            check=false;
        }
        if($(vtid).val().trim()!='')
        {
           if($(vtid).val().trim().match(/^[A-Za-z]{2,3}[0-9]*$/g) == null){
            showValidate(voter_id);
            check=false;
        }  
        }
       








        if(check){
             if(state.val().trim() == ''){
            alert("State is required");
            return false;
            
        }
        var district_val;
        var assembly_val;
        if (state.val()<='2')
        {
           if(district.val().trim() == ''){
             alert("District is required");
            return false;
        }
        if(assembly.val().trim() == ''){
           alert("Assembly is required");
            return false;
        } 
        district_val=district.val();
        assembly_val=assembly.val();
        }
        else
        {
            district_val=0;
        assembly_val=0;

        }

       
            $('.loader').css("display","block");
            var data = {};
            data['phone'] = "91"+$(mno).val().trim();
            data['voter_id'] = $(vtid).val().trim();
            data['email'] = $(email).val().trim();
            data['name'] = $(full_name).val().trim();
            data['relation_name'] = $(relation_name).val().trim();
            data['age'] = $(age).val().trim();
            data['fb_id'] = '';
            data['education'] = $(education).val().trim();
            data['referral_id'] = $(refid).val().trim();
            data['occupation'] = $(occupation).val().trim();
            data['house_no'] = $(hno).val().trim();
            data['address'] = $(street).val().trim();
            data['pincode'] = $(pincode).val().trim();
            data['receipt_no'] = $(receipt_no).val().trim();
            data['village'] = $(village).val().trim();
            data['gender'] = $('input[name=switch_2]:checked').val().trim();
            data['district'] = district_val;
            data['state_id'] = $(state).val();
            data['constituency']= assembly_val;
            data['mandal'] = $(mandal).val()
            data['booth_id'] = booth_id;
            
            
            $.ajax({
                type: "POST",
                url: "/offlineMemberships",
                data:JSON.stringify(data),
                dataType:"json",
                contentType:"application/json",
                success: function (response) {
                    
                    if(response.data.status=='1')
                    {
                        $('.loader').css("display","none");
                            swal({
                            title: "Details Added Successfully",
                            icon:"success",
                            text: "Membership ID is: "+response.data.membership_id,
                            type: "success",
                            showCancelButton: false,
                            
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                            },
                           function()
                           {
                            location.reload();
                           });
                            
                           
                        
                    }
                    else if(response.data.status>='2')
                    {
                        var issue;
                         if (response.data.status=='2')
                        {
                           issue="Already Registered";  
                        }
                            
                        else
                        {
                            issue="Limit with Phone Number Already Reached"; 
                        }
                         var uri="/offlineMembershipsissues?referral_id="+data['referral_id']+"&receipt_no="+data['receipt_no']+"&name="+data['name']+"&district="+data['district']+"&constituency="+data['constituency']+"&issue="+issue+"&place="+data['village']
                  
                        $('.loader').css("display","none");
                           swal({
                                title:response.errors[0],
                                text:"Raise Issue",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                              },
                              function(){
                               
                                window.location.replace(uri);
                              }); 
                    }
                    else
                    {
                        swal("Error",response.errors[0],"error")
                         $('.loader').css("display","none"); 
                    }

                },
                error:function(response)
                {
                swal("Error","Something Wrong","error")
                $('.loader').css("display","none");
                }

            });
        }

    });
});