$(document).ready(function() {
var membership_id;
   
    var mandals = [];
    var states = [];
    var parliament_constituencies = [];
    var districts = [];
    var polling_booths = [];
    var ac;

    $.getJSON('static/offline_memberships/json/membership/mandals.json',function(data){
        mandals = data;
    });

    $.getJSON('static/offline_memberships/json/membership/districts.json',function(data){
        districts = data;
    });
    $.getJSON('static/offline_memberships/json/membership/constituencies.json',function(data) {
        ac =data;
    });

    $('#state_select').selectpicker({
        style: 'btn-default',
        size: false
    });
    $('#state_select').selectpicker('refresh');
    $('#state_select').change(function (e) {

        var state_id = e.target.value;
        var d = districts[state_id];
        $('#district_select').html('');
        $.each(d, function (i, val) {

            $('#district_select').append($("<option></option>").attr("value", val.id).text(val.district_name));


        });
        $('#district_select').selectpicker({
            style: 'btn-default',
            size: false
        });
        $('#district_select').selectpicker('refresh');
        $('#district_select').change(function (dis_e) {

            var district_id = dis_e.target.value;
            console.log(dis_e.target.value);

            console.log(district_id);
            var local_ac = ac[district_id];

            $('#assembly_select').html('');
            $.each(local_ac, function (i1, val1) {
                $('#assembly_select').append($("<option data-stateid='" + val1.state_id + "'></option>").attr("value", val1.constituency_id).text(val1.constituency_name));
            });
            $('#assembly_select').selectpicker({
                style: 'btn-default',
                size: false
            });
            $('#assembly_select').selectpicker('refresh');
            $('#assembly_select').change(function (ass_e) {
                var ass_id = ass_e.target.value;
                console.log(typeof(state_id), typeof(district_id), typeof (ass_id))
                var mandalist = _.where(mandals, {
                    state_id: parseInt(state_id),
                    district_id: parseInt(district_id),
                    constituency_id: parseInt(ass_id)
                })
                // console.log(mandalist);
                $('#mandal_select').html('');
                $.each(mandalist, function (i, val) {

                    $('#mandal_select').append($("<option></option>").attr("value", val.id).text(val.mandal_name));


                });
                $('#mandal_select').selectpicker({
                    style: 'btn-default',
                    size: false
                });
                $('#mandal_select').selectpicker('refresh');

            });
        });
    });


    $('.input2').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })


    $('.validate-form .input2').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function showValidate(input) {
        var thisAlert = $(input).parent();
        // console.log($(thisAlert));
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    
    $('#check_details').on('click', function () {
        var check = true;
        var fd = $('input[name="membership_id"]').val();
        console.log(fd)
        if ($('#membership_id').val()) {
            showValidate();
            check = false;

        }


        if (check) {
            var switches = {
                'male': '#switch_left',
                'female': '#switch_right'
            }


            //    Male female switch
            // gen = 'Female        ';
            // gen = gen.trim().toLowerCase();


            // console.log(switches[gen]);
            
            // gender.focus()
            $.ajax({
                type: "GET",
                url:  "/edit_offline_Membership?membership_id=" + fd,
                data: {"membership_id": fd},
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    console.log(response)
                    if(response.data.status==1){
                       
                         $('#full_data').css('display','block');
                    membership_id=response['data']['children']["membership_id"]
                    $('#name').val(response['data']['children']["name"]).focus();
                    $('#name').focus();
                    $('#age').val(response['data']['children']["age"]).focus();
                    $('#email').val(response['data']['children']["email"]).focus();

                    $('#membership_id').val(response['data']['children']["membership_id"]).focus();
                    $('#phone').val(response['data']['children']['phone']).focus();
                    $('#relationship_name').val(response['data']['children']["relationship_name"]).focus();
                    $('#occupation').val(response['data']['children']["occupation"]).focus();
                    $('#voter_id').val(response['data']['children']["voter_id"]).focus();
                    $('#fb_id').val(response['data']['children']["fb_id"]).focus();
                    $('#address').val(response['data']['children']["address"]).focus();
                    $('#receipt_no').val(response['data']['children']["receipt_no"]).focus();

                    $('#state_select').val(response['data']['children']['state_id']).change();
                    $('#district_select').val(response['data']['children']['district_id']).change();
                    $('#assembly_select').val(response['data']['children']['constituency_id']).change();
                    $('#mandal_select').val(response['data']['children']['mandal']).change();
                    gen = response['data']['children']['gender'];
                    gen = gen.trim().toLowerCase();
                   $('#gender').attr("checked", false);
            $(switches[gen]).attr("checked", true).change();
                    $(".loader").css('display','none');
                        
                        
                        }
                        else
                        {
                             $('#full_data').css('display','none');
                swal("Error", response.errors[0],"error")
                        $(".loader").css("display","none")
                        }
                   

                },


                error: function (response) {
                    swal("Error", response.errors[0],"error")
                        $(".loader").css("display","none")
                }

            });
        }
 });

        $('#submit_details').on('click', function () {
            var check = true;

            var gender =$('input[name=switch_2]');
            var data = {};
            if (check) {
                data['membership_id']=membership_id;
                data['phone'] = $('#phone').val();
                data['voter_id'] = $('#voter_id').val();
                data['email'] = $('#email').val();
                data['name'] = $('#name').val();
                data['relation_name'] = $('#relationship_name').val();
                data['age'] = $('#age').val();
                data['fb_id'] = $('#fb_id').val();
                data['education'] = $('#education').val();
                data['occupation'] = $('#occupation').val();
                data['house_no'] = $('#address').val();
                data['pincode'] = $('#pincode').val();
                data['village'] = $('#village').val();
                data['gender'] =$('input[name=switch_2]:checked').val();
                data['district'] = $('#district_select').val();
                data['state_id'] = $('#state_select').val();
                data['constituency'] = $('#assembly_select').val();
                data['mandal'] = $('#mandal_select').val()
                data['receipt_no'] = $('#receipt_no').val()

                $.ajax({
                    type: "POST",
                    url:   "/edit_offline_Membership",
                    data: JSON.stringify(data),
                    dataType: "json",
                    contentType: "application/json",
                    success: function (response) {
                       
                        
                        if(response.data.status==1){
                            $(".loader").css("display","none");
                            swal({
                                title:"Updated successfully",
                                icon:"success",
                                type:"success"
                            }).then(function(){
                                location.reload();
                            })
                        }
                        else
                            {
                        swal("Error", response.errors[0],"error")
                            $(".loader").css("display","none")
                            }
                        },
                        error: function (response) {
                            swal("Error", response.errors[0],"error")
                            $(".loader").css("display","none")
                        }



                   
                });
            }

            })
   
})

