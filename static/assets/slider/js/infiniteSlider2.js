/**
 * infiniteSlider v2.0.2
 */

;(function($) {
  $.fn.infiniteSlider2 = function(options) {
    var settings = $.extend({}, $.fn.infiniteSlider2.defaults, options);
    return this.each(function() {
      var slider = $(this),
        slideQuantity = slider.children().length,
        sliderWidth = settings.width,
        sliderHeight = settings.height,
        sliderWrapperWidth = slideQuantity * 100,
        arrows = settings.arrows,
        toggles = settings.toggles,
        labels = settings.labels,

        slideBackgroundColor = settings.slideBackgroundColor,
        slideBackgroundImage = settings.slideBackgroundImage,

        arrowWidth = settings.arrowWidth,
        arrowHeight = settings.arrowHeight,
        arrowMargin = settings.arrowMargin,
        arrowBackgroundColor = settings.arrowBackgroundColor,
        arrowBackgroundImageRight = settings.arrowBackgroundImageRight,
        arrowBackgroundImageLeft = settings.arrowBackgroundImageLeft,
        arrowOpacity = settings.arrowOpacity,
        arrowFill = settings.arrowFill,
        arrowAnimate = settings.arrowAnimate,

        toggleShape = settings.toggleShape,
        toggleWidth = settings.toggleWidth,
        toggleHeight = settings.toggleHeight,
        toggleGutter = settings.toggleGutter,
        toggleOpacity = settings.toggleOpacity,
        toggleColor = settings.toggleColor,
        toggleActiveColor = settings.toggleActiveColor,
        toggleBorder = settings.toggleBorder,
        toggleActiveBorder = settings.toggleActiveBorder,
        toggleMargin = settings.toggleMargin - 10,
        togglePadding = '0 ' + toggleGutter / 2 + 'px 0',
        togglesWidth = slideQuantity * (toggleWidth + toggleGutter) + 30,
        togglesWrapperWidth = togglesWidth / 2,
        toggleAnimate = settings.toggleAnimate,

        slideLabelWidth = settings.slideLabelWidth,
        slideLabelHeight = settings.slideLabelHeight,
        slideLabelBorderWidth = settings.slideLabelBorderWidth,
        slideLabelBorderStyle = settings.slideLabelBorderStyle,
        slideLabelBorderColor = settings.slideLabelBorderColor,
        slideActiveLabelBorderColor = settings.slideActiveLabelBorderColor,
        slideLabelBackgroundColor = settings.slideLabelBackgroundColor,
        slideLabelImage = settings.slideLabelImage,

        autoplay = settings.autoplay,
        slideInterval = settings.slideInterval,
        slideDuration = settings.slideDuration,
        sliderTimer,
        cursor = settings.cursor;

      slider.on('mousemove', function() {
        $('.inf-arrow-left-wrapper').css({
          'opacity': arrowOpacity,
          'left': arrowMargin
        });
        $('.inf-arrow-right-wrapper').css({
          'opacity': arrowOpacity,
          'right': arrowMargin
        });
        $('.inf-toggles-wrapper').css({
          'opacity': toggleOpacity,
          'bottom': toggleMargin
        });
        slider.unbind('mousemove');
      });
      slider.children()
        .wrapAll('<div class="inf-wrapper" data-current="0"></div>');
      $('.inf-wrapper')
        .children()
        .addClass('inf-item');
      $(window).resize(function() {
        if (autoplay) clearInterval(sliderTimer);
        $('.inf-wrapper:animated').stop(true, true);
        $('.inf-wrapper').css('width', slider.outerWidth() * slideQuantity);
        $('.inf-item').css('width', ($('.inf-wrapper').outerWidth() / slideQuantity));
        $('.inf-wrapper').css('left', -(parseInt($('.inf-wrapper').data('current')) * slider.outerWidth()));
        slider.css('height', $('.inf-item').outerHeight());
      });
      var infWrapper = $('.inf-wrapper'),
        infItem = $('.inf-item');
      if (slideBackgroundImage || slideBackgroundColor) {
        for (var i = 0; i < slideQuantity; i++) {
          infItem.children()
            .eq(i)
            .addClass('slide-content-' + i)
            .css({
              'backgroundColor': slideBackgroundColor[i],
              'backgroundImage': slideBackgroundImage[i],
            });
        }
      }
      slider.css({
        'width': sliderWidth + '%',
        'margin': '0 auto',
        'position': 'relative',
        'overflow': 'hidden'
      });
      infWrapper.css({
        'width': sliderWrapperWidth + '%',
        'position': 'absolute',
        'top': 0,
        'left': 0
      });
      infItem.css({
        'display': 'block',
        'float': 'left',
        'width': infWrapper.outerWidth() / slideQuantity,
        'height': sliderHeight,
        'margin': 0,
      });
      infItem.children('img')
        .css({
          'display': 'block',
          'width': '100%',
          'height': '100%',
          'margin': 0
        });
      slider.css('height', infItem.outerHeight());

      if (arrows) {
        slider.append('<div class="inf-arrow-wrapper inf-arrow-left-wrapper"><div class="inf-arrow inf-arrow-left"></div></div><div class="inf-arrow-wrapper inf-arrow-right-wrapper"><div class="inf-arrow inf-arrow-right"></div></div>');
        $('.inf-arrow-wrapper').css({
          'width': arrowWidth,
          'height': arrowHeight / 2,
          'position': 'absolute',
          'bottom': '50%',
        });
        if (arrowAnimate) {
          $('.inf-arrow-left-wrapper').css({
            'left': 5,
            'opacity': 0
          });
          $('.inf-arrow-right-wrapper').css({
            'right': 5,
            'opacity': 0
          });
        } else {
          $('.inf-arrow-left-wrapper').css({
            'left': arrowMargin,
            'opacity': arrowOpacity
          });
          $('.inf-arrow-right-wrapper').css({
            'right': arrowMargin,
            'opacity': arrowOpacity
          });
        }
        $('.inf-arrow').css({
          'width': arrowWidth,
          'height': arrowHeight,
          'margin': 0,
          'backgroundColor': arrowBackgroundColor,
          'cursor': cursor
        });
        if (arrowBackgroundImageRight || arrowBackgroundImageLeft) {
          $('.inf-arrow-right').css({
            'margin': 0,
            'backgroundImage': arrowBackgroundImageRight,
            'backgroundPosition': 'right center',
            'backgroundRepeat': 'no-repeat'
          });
          $('.inf-arrow-left').css({
            'margin': 0,
            'backgroundImage': arrowBackgroundImageLeft,
            'backgroundPosition': 'left center',
            'backgroundRepeat': 'no-repeat'
          });
        } else {
          $('.inf-arrow-right').append('<svg version="1.2" baseProfile="tiny" id="svg-arrow-right" class="svg-arrow svg-arrow-right" x="0px" y="0px" viewBox="0 0 32 76" xml:space="preserve"><path d="M32,38.1L4.1,75.2C3.7,75.7,3.2,76,2.6,76c-1,0-1.9-0.9-1.9-1.9c0-0.4,0.1-0.7,0.4-1.1l26.2-34.9L1.1,3.2c-0.7-0.8-0.7-2,0.1-2.7c0.8-0.7,2-0.7,2.7,0.1L32,38.1z"/></svg>');
          $('.inf-arrow-left').append('<svg version="1.2" baseProfile="tiny" id="svg-arrow-left" class="svg-arrow svg-arrow-left" x="0px" y="0px" viewBox="0 0 32 76" xml:space="preserve"><path d="M0,37.9L27.9,0.8C28.3,0.3,28.8,0,29.4,0c1,0,1.9,0.9,1.9,1.9c0,0.4-0.1,0.7-0.4,1.1L4.7,37.9l26.2,34.9c0.7,0.8,0.7,2-0.1,2.7c-0.8,0.7-2,0.7-2.7-0.1L0,37.9z"/></svg>');
          $('.svg-arrow').css({
            'width': arrowWidth,
            'height': arrowHeight,
            'fill': arrowFill,
          });
        }
      }

      function bindLeftToggles() {
        var leftToggles = $('.inf-toggle-on').prevAll();
        leftToggles.each(function(indx, element) {
          var prevToggle = $(element);
          prevToggle.off('click');
          prevToggle.on('click', function() {
            for (var i = 0; i <= indx; i++) prevSlide();
          });
        });
      }

      function bindRightToggles() {
        var rightToggles = $('.inf-toggle-on').nextAll();
        rightToggles.each(function(indx, element) {
          var nextToggle = $(element);
          nextToggle.off('click');
          nextToggle.on('click', function() {
            for (var i = 0; i <= indx; i++) nextSlide();
          });
        });
      }

      function showLabels() {
        for (var j = 1; j <= slideQuantity; j++) {
          $('.inf-toggle:nth-child(' + j + ')')
            .off('mouseenter mouseleave')
            .hover(function() {
              if (!$(this).is('.inf-toggle-on')) {
                $('.slide-label-wrapper-' + $(this).index())
                  .stop()
                  .animate({
                    'opacity': 1,
                    'bottom': toggleHeight + 24,
                  }, 150);
              }
            }, function() {
              $('.slide-label-wrapper-' + $(this).index())
                .stop()
                .animate({
                  'opacity': 0,
                  'bottom': toggleHeight + 17,
                }, 150);
            });
        }
      }

      function bildTogglesBorder() {
        var currentSlideNumber = $('.inf-toggle-on').index() + 1;
        for (var k = 2; k <= slideQuantity + 1; k++) {
          if (k == currentSlideNumber + 1) {
            $('.inf-toggles-wrapper > .slide-label-wrapper:nth-child(' + k + ') > .slide-label')
              .css('borderColor', slideActiveLabelBorderColor)
              .children('.lable-triangle')
              .css('borderTopColor', slideActiveLabelBorderColor);
          } else {
            $('.inf-toggles-wrapper > .slide-label-wrapper:nth-child(' + k + ') > .slide-label')
              .css('borderColor', slideLabelBorderColor)
              .children('.lable-triangle')
              .css('borderTopColor', slideLabelBorderColor);
          }
        }
        showLabels();
      }

      function prevSlide() {
        var currentSlide = parseInt($('.inf-wrapper').data('current'));
        currentSlide--;
        if (currentSlide < 0) {
          $('.inf-wrapper')
            .css('left', -(currentSlide + 2) * slider.outerWidth())
            .prepend($('.inf-wrapper').children().last().clone())
            .children()
            .last()
            .remove();
          currentSlide++;
        }
        $('.inf-wrapper')
          .stop()
          .animate({
            'left': -currentSlide * slider.outerWidth()
          }, slideDuration, 'swing', function() {
            $('.slide-label-wrapper-' + $('.inf-toggle-on').index())
              .stop()
              .animate({
                'opacity': 0,
                'bottom': toggleHeight + 17
              }, 150);
          })
          .data('current', currentSlide);
        $('.inf-toggles')
          .append($('.inf-toggles').children().first().clone())
          .children()
          .first()
          .remove();
        if (toggles) {
          bindLeftToggles();
          bindRightToggles();
          if (labels) bildTogglesBorder();
        }
      }

      function nextSlide() {
        var currentSlide = parseInt($('.inf-wrapper').data('current'));
        currentSlide++;
        if (currentSlide >= $('.inf-wrapper').children().length) {
          $('.inf-wrapper')
            .css('left', -(currentSlide - 2) * slider.outerWidth())
            .append($('.inf-wrapper').children().first().clone())
            .children()
            .first()
            .remove();
          currentSlide--;
        }
        $('.inf-wrapper')
          .stop()
          .animate({
            'left': -currentSlide * slider.outerWidth()
          }, slideDuration, 'swing', function() {
            $('.slide-label-wrapper-' + $('.inf-toggle-on').index())
              .stop()
              .animate({
                'opacity': 0,
                'bottom': toggleHeight + 17,
              }, 150);
          })
          .data('current', currentSlide);
        $('.inf-toggles')
          .prepend($('.inf-toggles').children().last().clone())
          .children()
          .last()
          .remove();
        if (toggles) {
          bindLeftToggles();
          bindRightToggles();
          if (labels) bildTogglesBorder();
        }
      }

      if (toggles) {
        slider.append('<div class=inf-toggles-wrapper><div class="inf-toggles"></div></div>');
        for (var i = 0; i < slideQuantity; i++) {
          $('.inf-toggles')
            .append('<div class=inf-numb-' + i + '><div class="toggle-shape"></div></div>')
            .children('[class^="inf-numb"]')
            .addClass('inf-toggle');
          $('.inf-wrapper > div')
            .eq(i)
            .addClass('inf-slide-' + i);
        }
        $('.inf-numb-0').addClass('inf-toggle-on');
        $('.inf-toggles-wrapper').css({
          'position': 'absolute',
          'right': '50%',
          'width': togglesWrapperWidth
        });
        if (toggleAnimate) {
          $('.inf-toggles-wrapper').css({
            'opacity': 0,
            'bottom': 5
          });
        } else {
          $('.inf-toggles-wrapper').css({
            'opacity': 1,
            'bottom': toggleMargin
          });
        }
        var togglesPaddingTopBottom = 10,
          togglesPaddingLeftRight = 15;
        $('.inf-toggles').css({
          'position': 'relative',
          'boxSizing': 'border-box',
          'width': togglesWidth,
          'height': toggleHeight + togglesPaddingTopBottom * 2,
          'margin': 0,
          'padding': togglesPaddingTopBottom + 'px ' + togglesPaddingLeftRight + 'px',
          'overflow': 'visible'
        });
        $('.inf-toggle').css({
          'position': 'relative',
          'boxSizing': 'content-box',
          'width': toggleWidth,
          'height': toggleHeight,
          'margin': 0,
          'padding': togglePadding,
          'float': 'left',
          'cursor': cursor
        });
        $('.toggle-shape').css({
          'boxSizing': 'border-box',
          'height': '100%',
          'margin': 0,
          'backgroundColor': toggleColor,
          'border': toggleBorder
        });
        $('.inf-toggle-on .toggle-shape').css('border', toggleActiveBorder);
        if (toggleShape === 'circle') {
          $('.toggle-shape').css('borderRadius', '50%');
        } else if (toggleShape === 'square') {
          $('.toggle-shape').css('borderRadius', 'inherit');
        }
        $('.inf-toggle-on > .toggle-shape').css('backgroundColor', toggleActiveColor);
        if (labels) {
          for (var i = 0; i < slideQuantity; i++) {
            $('.inf-toggles-wrapper')
              .append('<div class="slide-label-wrapper slide-label-wrapper-' + i + '"><div class="slide-label slide-label-' + i + '"><div class="lable-triangle"></div></div></div>');
            $('.slide-label-' + i).css({
              'backgroundImage': slideLabelImage[i],
              'backgroundColor': slideLabelBackgroundColor[i],
              'backgroundPosition': 'center center',
              'backgroundRepeat': 'no-repeat'
            });
          }
          var slideLabelWrapperLeft = togglesPaddingLeftRight + (toggleWidth + toggleGutter - slideLabelWidth) / 2;
          for (var i = 0; i < slideQuantity; i++) {
            $('.slide-label-wrapper-' + i).css({
              'position': 'absolute',
              'bottom': toggleHeight + 17,
              'left': slideLabelWrapperLeft + (i * (toggleGutter + toggleWidth)),
              'opacity': 0
            });
          }
          $('.slide-label').css({
            'position': 'relative',
            'width': slideLabelWidth,
            'height': slideLabelHeight,
            'boxSizing': 'border-box',
            'borderWidth': slideLabelBorderWidth,
            'borderStyle': slideLabelBorderStyle,
            'borderColor': slideLabelBorderColor,
            'borderRadius': '50%'
          });
          if (slideLabelBorderWidth) {
            var lableTriangleWidth = 6,
              lableTriangleHeight = 10;
            var toggleBorderFloat = parseFloat(slideLabelBorderWidth) || 0;
            $('.lable-triangle').css({
              'display': 'block',
              'boxSizing': 'border-box',
              'position': 'absolute',
              'bottom': -10 - toggleBorderFloat,
              'left': ((slideLabelWidth - 6) / 2) - toggleBorderFloat,
              'content': " ",
              'margin': 'auto',
              'width': 0,
              'height': 0,
              'borderTopWidth': '10px',
              'borderTopStyle': 'solid',
              'borderTopColor': slideLabelBorderColor,
              'borderLeft': '3px solid transparent',
              'borderRight': '3px solid transparent',
            });
          }
          var currentSlideNumber = $('.inf-toggle-on').index() + 1;
          $('.inf-toggles-wrapper > .slide-label-wrapper:nth-child('+ (currentSlideNumber + 1) +') > .slide-label')
            .css('borderColor', slideActiveLabelBorderColor)
            .children('.lable-triangle')
            .css('borderTopColor', slideActiveLabelBorderColor);
          bildTogglesBorder();
        }
      }

      slider.hover(function() {
        if (autoplay) clearInterval(sliderTimer);
        if (arrowAnimate) {
          $('.inf-arrow-left-wrapper')
            .stop()
            .animate({
              'left': arrowMargin,
              'opacity': arrowOpacity
            }, 200);
          $('.inf-arrow-right-wrapper')
            .stop()
            .animate({
              'right': arrowMargin,
              'opacity': arrowOpacity
            }, 200);
        }
        if (toggleAnimate) {
          $('.inf-toggles-wrapper')
            .stop()
            .animate({
              'opacity': toggleOpacity,
              'bottom': toggleMargin
            }, 200);
        }
      }, function() {
        if (autoplay) sliderTimer = setInterval(nextSlide, slideInterval);
        $(this).unbind('mousemove');
        if (arrowAnimate) {
          $('.inf-arrow-left-wrapper')
            .stop()
            .animate({
              'left': 5,
              'opacity': 0
            }, 200);
          $('.inf-arrow-right-wrapper')
            .stop()
            .animate({
              'right': 5,
              'opacity': 0
            }, 200);
        }
        if (toggleAnimate) {
          $('.inf-toggles-wrapper')
            .stop()
            .animate({
              'opacity': 0,
              'bottom': 5
            }, 200);
        }
      });
      $('.inf-arrow-right').on('click', function() {
        nextSlide();
      });
      $('.inf-arrow-left').on('click', function() {
        prevSlide();
      });
      if (toggles) {
        bindRightToggles();
        bindLeftToggles();
      }
    });
  }

  $.fn.infiniteSlider2.defaults = {
    // general defaults
    width: 100,
    height: 'auto',
    arrows: true,
    toggles: true,
    labels: true,

    // slide background defaults
    slideBackgroundColor: [
      '#5eabe7',
      '#cca27e',
      '#7e4c39',
      '#83595e',
      '#2e2432'
    ],
    slideBackgroundImage: [],

    // arrow defaults
    arrowWidth: 32,
    arrowHeight: 76,
    arrowMargin: 20,
    arrowBackgroundColor: '',
    arrowBackgroundImageRight: '',
    arrowBackgroundImageLeft: '',
    arrowFill: 'white',
    arrowOpacity: 0.4,
    arrowAnimate: true,

    // toggle defaults
    toggleShape: 'circle',
    toggleWidth: 16,
    toggleHeight: 16,
    toggleGutter: 8,
    toggleOpacity: 1,
    toggleColor: '',
    toggleActiveColor: '',
    toggleBorder: '3px solid rgba(255, 255, 255, 0.4)',
    toggleActiveBorder: '3px solid white',
    toggleMargin: 30,
    toggleAnimate: true,

    // label defaults
    slideLabelWidth: 74,
    slideLabelHeight: 74,
    slideLabelBorderWidth: 3,
    slideLabelBorderStyle: 'solid',
    slideLabelBorderColor: 'rgba(255, 255, 255, 0.4)',
    slideActiveLabelBorderColor: 'white',
    slideLabelBackgroundColor: [
      '#5eabe7',
      '#cca27e',
      '#7e4c39',
      '#83595e',
      '#2e2432'
    ],
    slideLabelImage: [],

    // advanced defaults
    autoplay: true,
    slideInterval: 6000,
    slideDuration: 600,
    cursor: 'pointer'
  }
})(jQuery);
