function setCookie(c_name, value, expdays) {
    var expdate = new Date();
    expdate.setDate(expdate.getDate() + expdays);
    var c_value = escape(value) + ((expdays == null) ? "" : "; expires=" + expdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}
function getCookie(c_name, defaultvalue) {
    var i, x, y, cookies = document.cookie.split(";");
    for (i = 0; i < cookies.length; i++) {
        x = cookies[i].substr(0, cookies[i].indexOf("="));
        y = cookies[i].substr(cookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return defaultvalue;
}
// This functions adds option to drop down list
function addOption(elemid, text, value, selected) {
    var elem = document.getElementById(elemid);
    var newopt = document.createElement('option');
    newopt.text = text;
    newopt.value = value;
    newopt.selected = !(!selected);
    elem.add(newopt);
}
//
lang_divs = {
    "telugu": [{

        "sup": "మద్దతుదారుడి పేరు",
        "preferedlang": "sdfds",
        "registration": "నమోదు చేసుకున్న ప్రదేశం",
        "whomtomeet": "ఎవరిని కలుసుకోవాలనుకుంటున్నారు ",
        "online": "ఆన్‌లైన్",
        "prashashannagar": "ప్రశాసన్ నగర్ పార్టీ ఆఫీస్",
        "mangalagirioffice": "మంగళగిరి పార్టీ ఆఫీస్",
        "benzcircle": "విజయవాడ - బెంజ్ సర్కిల్ పార్టీ  ఆఫీస్",
        "otherofficies": "ఇతర పార్టీ ఆఫీసులు",
        "designation": "హోదా",
        "genderm": "పురుషుడు",
        "genderf": "స్త్రీ",
        "president": "గౌ⎸⎸పార్టీ అధ్యక్షులు",
        "chairman": "గౌ⎸⎸ చైర్మన్ - పార్టీ అఫైర్స్",
        "otherpartyofficials": "ఇతర పార్టీ అధికారులు",
        "partyofficialsename": "పార్టీ అధికారుల పేరు",
        "otherofficename": "ఇతర పార్టీ కార్యాలయం",
        "state": "రాష్ట్రం",
        "district": "జిల్లా",
        "constituency": "అసెంబ్లీ నియోజకవర్గం:",
        "mandal": "మండలం:",
        "otherdistrict": "జిల్లా",
        "otherassembly": "అసెంబ్లీ నియోజకవర్గం",
        "othermandal": "మండలం",
        "applicantname": "పేరు (ప్రధాన దరఖాస్తుదారుడి):",
        "gender": "లింగం",
        "age": "వయస్సు",
        "phonenumber": "ఫోను నంబరు:",
        "whatsapp": "వాట్సాప్ నంబర్:",
        "applicantaddress": "ప్రధాన దరఖాస్తుదారుడి చిరునామా:",
        "alreadyparty": "పార్టీ సభ్యత్వం కలిగి ఉన్నారా?",
        "membershipid": "ఉన్నట్లయితే, సభ్యత్వం నంబరు:",
        "membershiplink": "లేనట్లయితే, పార్టీ సభ్యత్వం పొందగలరు:",
        "category": "అభ్యర్థన యొక్క ముఖ్య ఉద్దేశ్యం:",
        "politicalissues": "రాజకీయ సమస్యలు",
        "publicissues": "ప్రజా సమస్యలు",
        "personalissues": "వ్యక్తిగత సమస్యలు",
        "meetandgreet": "కలుసుకోవడం & అభినందించడం (ఫోటో)",
        "otherscategory": "ఇతర సమస్యలు",
        "otherpurpose": "ఇతర సమస్యలు వివరాలు",
        "issues": "అభ్యర్థనను / సమస్యలను క్లుప్తంగా వివరించండి:",
        "documents": "అభ్యర్థనకు / సమస్యలకు సంబంధించిన పత్రాలను పొందుపరచండి:",
        "visiting": "ఎలా కలవాలని అనుకుంటున్నారు?",
        "singleoralone": "ఒక్కరు మాత్రమే",
        "bygroup": "ఒక టీం గా (1 + 4)",
        "partymember": "పార్టీ సభ్యత్వం కలిగి ఉన్నారా?",
        "supportname": "పేరు ",
        "supportjspid": "జనసేన పార్టీ సభ్యత్వం నంబరు",
        "supportphone": "ఫోను",
        "supportage": "వయస్సు",
        "supportgender": "లింగం",
        "partymemberyou": "పార్టీ సభ్యత్వం కలిగి ఉన్నారా?",
        "referredofficials": "పార్టీ అధికార ప్రతినిధి లేక జనసైనికులు సూచించారా?",
        "officialsname": " అధికార ప్రతినిధి లేక జనసైనికుడి పేరు",
        "officialsnumber": "అధికార ప్రతినిధి లేక జనసైనికుడిపేరు ఫోన్ నెంబర్:",
    }],
    "english": [{
        "sup": "Nme of supporter",
        "preferedlang": "preferred language : ",
        "registration": "Place of visitors Registration",
        "whomtomeet": "Whom to Meet",
        "online": "online",
        "prashashannagar": "Prashashan nagar Party Office",
        "mangalagirioffice": "Mangalagiri Party Office",
        "benzcircle": "VJY-Benz circle Party office",
        "otherofficies": "others offices",
        "president": "Mr.President",
        "chairman": "Mr.Chairman- Party Affairs",
        "otherpartyofficials": "Other Party Officials",
        "designation": "Desigination",
        "partyofficialsename": "Name of Party officials",
        "otherofficename": "Other office name",
        "state": "State",
        "district": "District",
        "constituency": "Assembly",
        "mandal": "Mandal",
        "otherdistrict": "District",
        "otherassembly": "Assembly",
        "othermandal": "Mandal",
        "applicantname": "Main Applicant Name",
        "gender": "Gender",
        "age": "Age",
        "phonenumber": "Phone Number",
        "whatsapp": "Whats App Number",
        "applicantaddress": "Main Applicant Address",
        "alreadyparty": "Already a Party Member",
        "membershipid": "Membership ID",
        "membershiplink": "Membership Link",
        "category": "Purpose of Meeting (Request/Issues Category)",
        "politicalissues": "Political Issues",
        "publicissues": "Public Issues",
        "personalissues": "Personal Issues",
        "meetandgreet": "Meet & Greet(Photo)",
        "otherscategory": "others",
        "otherpurpose": "Other Purpose",
        "issues": "Brief note on Issues",
        "documents": "Option to upload supportive documents",
        "visiting": "Type of Visiting",
        "singleoralone": "Single or Alone",
        "bygroup": "By Group(1+4 persons)",
        "partymember": "Are you a Party Member?",
        "supportname": "Name ",
        "supportjspid": "JSP Membership ID",
        "supportphone": "Phone Number",
        "supportage": "Age",
        "supportgender": "Gender",
        "partymemberyou": "Are you a Party Member?",
        "referredofficials": "Referred by Party Official / Janasainik",
        "officialsname": "Name of party official / Janasainik",
        "officialsnumber": "Phone Number",

    }]
}

function replacelabels(data) {
    console.log(data);
    $.each(data[0], function (key, value) {
        console.log(key + ":" + value);
        $("." + key).text(value);
    });
}

var tips = [], currenttip = 0, turnoff = false, piresourcebase = '';
// Callback function which gets called when user presses F9 key --.
function scriptChangeCallback(lang, kb, context) {


    //alert("check : "+lang)--;
    // alert(lang_divs);
    replacelabels(lang_divs[lang]);
    //console.log(lang_divs[lang]);

    // Change the dropdown to new selected language.
    //document.getElementById('cmdhelp').className = (lang == 'english' ? 'disabled' : '');

    var icon = pramukhIME.getIcon(4);
    // PramukhIME toolbar settings
    //document.getElementById('cmdhelp').style.background = "transparent url('"+piresourcebase+"img/" + icon.iconFile + "') " + (lang == 'english' ? 100 : -1 * icon.x) + "px " + (lang == 'english' ? 100 : -1 * icon.y) + "px no-repeat";

    /*var i, dd = document.getElementById('drpLanguage');
    for (i = 0; i < dd.options.length; i++) {
        if (dd.options[i].value == kb + ':' + lang) {
            dd.options[i].selected = true;
        }
    }*/
    // Change the image
    //-- document.getElementById('pramukhimecharmap').src = piresourcebase + 'img/' + pramukhIME.getHelpImage();
    /*var filename = pramukhIME.getHelp();
    if (filename != '') {
        document.getElementById('pramukhimehelpdetailed').src = piresourcebase + 'help/' + filename;
    }
    setCookie('pramukhime_language', kb + ':' + lang, 10);*/




}
// Changing the language by selecting from dropdown list
function changeLanguage(newLanguage) {
    //alert(newLanguage);
    if (!newLanguage || newLanguage == "")
        newLanguage = 'english';
    // set new script
    var lang = newLanguage.split(':');
    pramukhIME.setLanguage(lang[1], lang[0]);
    if (lang[1] == 'telugu') {
        $('label').css('font-size', '19px')
    }
    else {
        $('label').css('font-size', '15px')
    }

}
function showHelp(elem) {
    if (elem.className && elem.className == 'disabled') {
        return false;
    }
    showDialog('Pramukh Type Pad Help');
    document.getElementById('pramukhimehelp').style.display = 'block';
    selectHelpType();
    return false;
}
function closeDialog() {
    document.getElementById('blocker').style.display = 'none';
    document.getElementById('dialog').style.display = 'none';
    document.getElementById('typingarea').focus();
    return false;
}
function showDialog(title) {
    document.getElementById('blocker').style.display = 'block';
    document.getElementById('dialog').style.display = 'block';
    document.getElementById('dialogheader').innerHTML = title;
    document.getElementById('pramukhimehelp').style.display = 'none';
}
function selectHelpType() {
    var rdolist = document.getElementsByName('helptype');
    if (rdolist[1].checked) {
        document.getElementById('pramukhimehelpquick').style.display = 'none';
        document.getElementById('pramukhimehelpdetailed').style.display = 'block';
    } else {
        document.getElementById('pramukhimehelpquick').style.display = 'block';
        document.getElementById('pramukhimehelpdetailed').style.display = 'none';
    }
}
function showNextTip() {
    currenttip++;
    if (currenttip > tips.length) {
        currenttip = 1; // reset tip
    }
    var tipelem = document.getElementById('pi_tips'), li;
    // get first child node
    var elem, len = ul.childNodes.length, i;
    for (i = 0; i < len; i++) {
        elem = ul.childNodes[i];
        if (elem.tagName && elem.tagName.toLowerCase() == 'li') {
            li = elem;
            break;
        }
    }
    if (!turnoff && li) {
        li.innerHTML = ''; // clear
        li.appendChild(document.createTextNode('Tip ' + currenttip + ': ' + tips[currenttip - 1] + ' '));
        elem = document.createElement('a');
        elem.href = 'javascript:;';
        elem.innerHTML = 'Turn Off';
        elem.className = 'tip';
        elem.onclick = turnOffTip;
        li.appendChild(elem);
        setTimeout('showNextTip()', 10000);
    } else if (li) {
        tipelem.removeChild(li);
    }

}
function turnOffTip() {
    turnoff = true;
    showNextTip();
}
