/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

// Names of the two caches used in this version of the service worker.
// Change to v2, etc. when you update any of the local resources, which will
// in turn trigger the install event again.
const PRECACHE = 'precache-v2';
const RUNTIME = 'runtime';

// A list of local resources we always want to be cached.
const PRECACHE_URLS = [
];

// The install handler takes care of precaching the resources we always need.
self.addEventListener('install', event => {
	console.log('in sw.js install');
	event.waitUntil(
		caches.open(PRECACHE)
			.then(cache => cache.addAll(PRECACHE_URLS))
			.then(self.skipWaiting())
	);

});

// // The activate handler takes care of cleaning up old caches.
self.addEventListener('activate', event => {
	console.log('in sw.js activate');

	const currentCaches = [PRECACHE, RUNTIME];
	event.waitUntil(
		caches.keys().then(cacheNames => {
			return cacheNames.filter(cacheName => !currentCaches.includes(cacheName));
		}).then(cachesToDelete => {
			return Promise.all(cachesToDelete.map(cacheToDelete => {
				return caches.delete(cacheToDelete);
			}));
		}).then(() => self.clients.claim())
	);
});





self.addEventListener('push', function (event) {
	if (!(self.Notification && self.Notification.permission === 'granted')) {
		return;
	}

	console.log('Received push');
	var data = event.data.json();
	let notificationTitle = data.title;
	let notificationOptions;

	if (event.data) {
		const dataText = event.data.json();
		notificationTitle = dataText.title;
		// notificationOptions.body = `Push data: '${dataText}'`;
		notificationOptions = {
			body: dataText.message,
			icon: 'static/jspwebsitemain/images/push_icon_logo.jpg',
			badge: 'static/jspwebsitemain/images/badge-72x72.png',
			tag: 'push' + new Date(),
			requireInteraction: true,
			image: dataText.image,
			data: {
				url: dataText.clickTarget,
				buttons: dataText.buttons
			},
			actions: dataText.buttons
		}
	}

	return event.waitUntil(self.registration.showNotification(notificationTitle, notificationOptions));
})

self.addEventListener('notificationclick', function (event) {
	event.notification.close();

	let clickResponsePromise = Promise.resolve();
	if (event.action || (event.notification.data && event.notification.data.url)) {
		if (event.action === "1") {
			clients.openWindow(event.notification.data.buttons[0].actionURL);
			event.notification.close();
		} else if (event.action === "2") {
			clients.openWindow(event.notification.data.buttons[1].actionURL);
			event.notification.close();
		}
		else {
			clients.openWindow(event.notification.data.url);
		}
	}
	/*if (event.notification.data && event.notification.data.url) {
		clickResponsePromise = clients.openWindow(event.notification.data.url);
	}*/

	event.waitUntil(
		Promise.all([
			clickResponsePromise
			// self.analytics.trackEvent('notification-click'),
		])
	);
});

self.addEventListener('notificationclose', function (event) {
	event.notification.close();
	console.log("notification closed");
	/*event.waitUntil(
		Promise.all([
			self.analytics.trackEvent('notification-close'),
		])
	);*/
});

self.addEventListener('pushsubscriptionchange', function (event) {
	console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
	let applicationServerPublicKey = localStorage.getItem('jsapplicationServerPublicKey');
	const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
	event.waitUntil(
		self.registration.pushManager.subscribe({
			userVisibleOnly: true,
			applicationServerKey: applicationServerKey
		})
			.then(function (newSubscription) {
				// TODO: Send to application server
				let saveData = $.ajax({
					type: 'POST',
					url: "https://june.digitalsena.org/api/subscribe/update-token",
					data: {
						oldToken: localStorage.getItem('jsDeviceId'),
						newToken: newSubscription.endpoint,
						p256dh: newSubscription.getKey('p256dh'),
						auth: newSubscription.getKey('auth'),
						appKey:'5a982fda1de796604ecf4a9c'
					},
					success: function (resultData) { 
						localStorage.setItem('jsDeviceId', newSubscription.endpoint)
						console.log("token updated") }
				});
				saveData.error(function () { console.log("Something went wrong"); });

				console.log('[Service Worker] New subscription: ', newSubscription);
			})
	);
});