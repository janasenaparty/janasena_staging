
$(document).ready(function(){
    var myTable;
   myTable = $('#cc').DataTable( {
                  data:[],


              columns: [
                    { title: report_level=='state'?'state':report_level=='district'?'district':'assembly' },
                    { title: "count" },
                   ],
              dom: 'Bfrtip',
              buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
              "lengthMenu": [ [10, 25, 50, "All"]]
              } );
    var noitemdiv = $('#noitemdiv');
    $('.nav.nav-tabs li').click(function(){
        if($(this).data('loc') =='counts'){
            noitemdiv.addClass('show').removeClass('hide');
        }
        else{
            noitemdiv.addClass('hide').removeClass('show');
        }

    });


    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawBackgroundColor);
    function drawBackgroundColor() {

                }



        $('#advanced_filter_btn').click(function(e){

            var nname = $('.tab-pane.active').data('name');

            var report_level = $('#report_level').val();
            var district_select =$('#district_select').val();
            var value_select = $('#value_select').val();
            var number_of_days = $('#no_days').val();
            var number_of_items = $('#no_items').val();





            if(nname == 'counts'){

        //        group_type
        //        no_items
        //        no_days
        //        default_id
                var data = {};
                data['group_type'] = report_level;
                data['default_id'] = value_select;
                data['no_items'] = number_of_items;
                data['no_days'] = number_of_items;

                $.ajax({
                    url:'/report/group_n_days',
                    data:data,
                    method:'POST',
                    success:function(response){
        //                console.log(response);
                        response = JSON.parse(response);
                        var dataSet = response['data']
        //                console.log(dataSet);


                         myTable.clear().draw()
                         var new_title = report_level=='state'?'state':report_level=='district'?'district':'assembly'
                         console.log(new_title);
                         var v = myTable.column(0).header();
                         $(v).text(new_title)
                         //                         console.log(myTable);
//                        console.log(myTable.column(0).data());
//                        myTable.column().name(report_level=='state'?'state':report_level=='district'?'district':'assembly').draw()
                         myTable.rows.add( dataSet ).draw()
//                         console.log(myTable.column(0).data())



                    }


                });

            }
            else {

                var data = {};
                console.log(report_level,value_select,number_of_items,number_of_days)
                data['group_type'] = report_level;
                data['default_id'] = value_select;
                data['no_items'] = number_of_items;
                data['no_days'] = number_of_days;
                if($('#report_level').val() == 'assembly'){
                    data['state_id'] = district_select;
                }

                 $.ajax({
                    url:'/report/trend_graph1',
                    data:data,
                    method:'POST',
                    success:function(response){
                        console.log(response)
                        response = JSON.parse(response)
                        var resp_data = response['data'];
                         var d = new google.visualization.DataTable();

                      d.addColumn('string', 'Date');
                      d.addColumn('number', 'Number of Members');

                        d.addRows(resp_data);
                        var view = new google.visualization.DataView(d);
//                          view.setColumns([0, 1,
//                                           { calc: "stringify",
//                                             sourceColumn: 1,
//                                             type: "string",
//                                             role: "annotation" },
//                                           2]);

                            var options = {
                            title: "Number of memberships per date",
                            height:500,
                            hAxis: {
                               title: "Date",
                               slantedText: true,  /* Enable slantedText for horizontal axis */
                               slantedTextAngle: 90 ,
                               format:'dd-MM-yyyy',
                               showTextEvery:1,
                               viewWindowMode:'maximized',
                                /* Define slant Angle */
                           },
                           
                            legend: { position: "none" },
                            'chartArea': { 'width': '82%', height: '60%'}
                          };
//                      var options = {
//                        hAxis: {
//                          title: 'Date'
//                        },
//                        vAxis: {
//                          title: 'Number of Members'
//                        },
//                        backgroundColor: '#f1f8e9'
//                      };

                      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                      chart.draw(view, options);
                    }
                    });


            }





        });


});
