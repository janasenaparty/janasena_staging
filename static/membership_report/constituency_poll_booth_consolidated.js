$(document).ready( function () {
//    console.log("document is ready")
     $(".loader").css("display","block");
     var table = $('#table_id').DataTable({
       
           data:[],
                    columns:[
      
      {data: "constituency_id"},
     {data: "district_name"},
      {data: "constituency_name"},
      {data: "total"},
      {data: "filled"},
      {data: "unfilled"}],
         dom: 'Bfrtip',
         buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],

        "lengthMenu": [ [10, 25, 50, "All"]]});

    
        $.ajax({
        "url":"/mem_report/constituency_pollbooth_member_count_consolidated",
        "type": "POST",
        
        "success":function(data){

            data = JSON.parse(data)
            var total=0;
            var existing=0;
            var tobeidentified=0;
             $.each(data['data'], function( index, value ) {
                            total=total+value.total;
                            existing=existing+value.filled;
                            tobeidentified=tobeidentified+value.unfilled;
                           });



$("#identified_booths").html(tobeidentified);
 $("#exisitng_booths").html(existing);
                           $("#total_booths").html(total);
            table.clear().draw();
           
            table.rows.add(data['data']).draw();
          $(".loader").css("display","none")

        }
    })

   


} );