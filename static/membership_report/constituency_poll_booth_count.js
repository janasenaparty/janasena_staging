$(document).ready( function () {
//    console.log("document is ready")
     $("#report_level").find('option')[1].selected = true;
    $("#report_level").change();
     var table = $('#table_id').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
     }
     );

    $('#advanced_filter_btn').click(function(){
        var district_name = $('#district_select').val()
        var constituency_id = $('#value_select').val()
        $(".loader").css("display","block")
        var data = {
        "district_name":district_name,

        "constituency_id":constituency_id
        }
        $.ajax({
        "url":"/mem_report/constituency_pollbooth_member_count",
        "type": "POST",
        "data":data,
        "success":function(data){

            data = JSON.parse(data)
            table.clear().draw();
            $("#memberships").html("Total Memberships: "+data['memberships']);
            $("#to_be_identified").html("To Be Identified Booths: "+data['to_be_identified']);
//            console.log(data);

            table.rows.add(data['data']).draw();
          $(".loader").css("display","none")

        }
    })

    });


} );