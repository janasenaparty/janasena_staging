var hod = angular.module('hod', ['ngRoute','textAngular']);

hod.config(function ($routeProvider, $locationProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
    $routeProvider.
               
                when("/profile", {templateUrl: "static/userdatamanagement/app/partials/profile.html", controller: "profileCtrl"}).
                // when("/activities", {templateUrl: "static/userdatamanagement/app/partials/activities.html", controller: "activitiesCtrl"}).
                when("/idcard", {templateUrl: "static/userdatamanagement/app/partials/idcard.html", controller: "idcardCtrl"}).
                when("/setpin", {templateUrl: "static/userdatamanagement/app/partials/setpin.html", controller: "setpinCtrl"}).
                otherwise({redirectTo : "/profile"})
});
 hod.directive('allowOnlyNumbers', function () {  
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });  
hod.directive("ngFileSelect",function(){

  return {
    link: function($scope,el,name){
      var image_for=name.name;
      el.bind("change", function(e){
      
        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile($scope.file,image_for);
      })
      
    }
    
  }
  
  
})


hod.directive('myMaxlength', ['$compile', '$log', function($compile, $log) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$set("ngTrim", "false");
                var maxlength = parseInt(attrs.myMaxlength, 10);
                ctrl.$parsers.push(function (value) {
                    $log.info("In parser function value = [" + value + "].");
                    if (value.length > maxlength)
                    {
                        $log.info("The value [" + value + "] is too long!");
                        value = value.substr(0, maxlength);
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                        $log.info("The value is now truncated as [" + value + "].");
                    }
                    return value;
                });
            }
        };
    }]);

hod.run(function($rootScope, $location){
    $rootScope.localUrl = "/";
    $rootScope.globalUrl = "/";
    $rootScope.assignUrl = $rootScope.globalUrl;
   
    $rootScope.menudata = [{
            icon_name: "fa fa-user",
            name: "Profile",
            link: "Profile"
        },{
            icon_name: "fa fa-user",
            name: "MembershipCard",
            link: "idcard"
        },{
            icon_name: "fa fa-tasks",
            name: "setpin",
            link: "setpin"
        }];
       
        $rootScope.profile_data;
    $rootScope.current_page = "Member"
    $rootScope.ui = {
   menu: false
}
$rootScope.go = function ( path ) {
 $location.path( path );
    $rootScope.triggermenu()
};
$rootScope.triggermenu = function(){
    if($rootScope.ui.menu){
        $rootScope.ui.menu = false;
    }
    else{
        $rootScope.ui.menu = true;
        console.log($rootScope.ui.menu)
    }  
}






});




