hod.controller('idcardCtrl', function ($scope, $http, $filter, $rootScope,$location) {
    
$scope.hidedata = {
       loadingscrren: true
   }
    // -----------------------------------------
var membership_id;
var crop_image_url='';
var orig_src;
var image_target; 
var crop_canvas;
 var $container,
 
  event_state = {},
  constrain = false,
  min_width = 100, // Change as required
  min_height = 100,
  max_width = 800, // Change as required
  max_height = 400,
  init_height=300,
  init_width=300,
  resize_canvas = document.createElement('canvas');
  imageData=null;

var resizeableImage = function(image_target1) {
  // Some variable and settings
  
  orig_src = new Image()
  image_target = $(image_target1).get(0),
 

  init = function(){
  
  //load a file with html5 file api
  $('.js-loadfile').change(function(evt) {
    var ext = $('.js-loadfile').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    
    swal('Error','invalid File! please upload image file','error');
    return false;
}
    var files = evt.target.files; // FileList object
    var reader = new FileReader();

    reader.onload = function(e) {
      imageData=reader.result;
      loadData();
    }
    reader.readAsDataURL(files[0]);
  });
  
  //add the reset evewnthandler
  $('.js-reset').click(function() {
    if(imageData)
      loadData();
  });
  

    // When resizing, we will always use this copy of the original as the base
    orig_src.src=image_target.src;

    // Wrap the image with the container and add resize handles
    $(image_target).height(init_height)
  .wrap('<div class="resize-container" style="position:relative" ></div>')
    .before('<span class="resize-handle resize-handle-nw"></span>')
    .before('<span class="resize-handle resize-handle-ne"></span>')
    .after('<span class="resize-handle resize-handle-se"></span>')
    .after('<span class="resize-handle resize-handle-sw"></span>');

    // Assign the container to a variable
    $container =  $('.resize-container');

  $container.prepend('<div class="resize-container-ontop"></div>');
  
    // Add events
    $container.on('mousedown touchstart', '.resize-handle', startResize);
    $container.on('mousedown touchstart', '.resize-container-ontop', startMoving);
    $('.js-crop').on('click', crop);
  };
  
  loadData = function() {
      
  //set the image target
  image_target.src=imageData;
  orig_src.src=image_target.src;
  
  //set the image tot he init height
  $(image_target).css({
    width:'auto',
    height:init_height
  });
  
  
  //resize the canvas
  $(orig_src).bind('load',function() {
    resizeImageCanvas($(image_target).width(),$(image_target).height());
  });
  };
  
  startResize = function(e){
    e.preventDefault();
    e.stopPropagation();
    saveEventState(e);
    $(document).on('mousemove touchmove', resizing);
    $(document).on('mouseup touchend', endResize);
  };

  endResize = function(e){
  resizeImageCanvas($(image_target).width(), $(image_target).height())
    e.preventDefault();
    $(document).off('mouseup touchend', endResize);
    $(document).off('mousemove touchmove', resizing);
  };

  saveEventState = function(e){
    // Save the initial event details and container state
    event_state.container_width = $container.width();
    event_state.container_height = $container.height();
    
    event_state.container_left = $container.offset().left; 
    event_state.container_top = $container.offset().top;
    event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft(); 
    event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
  
  // This is a fix for mobile safari
  // For some reason it does not allow a direct copy of the touches property
  if(typeof e.originalEvent.touches !== 'undefined'){
    event_state.touches = [];
    $.each(e.originalEvent.touches, function(i, ob){
      event_state.touches[i] = {};
      event_state.touches[i].clientX = 0+ob.clientX;
      event_state.touches[i].clientY = 0+ob.clientY;
    });
  }
    event_state.evnt = e;
  };

  resizing = function(e){
    var mouse={},width,height,left,top,offset=$container.offset();
    mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft(); 
    mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
    
    // Position image differently depending on the corner dragged and constraints
    if( $(event_state.evnt.target).hasClass('resize-handle-se') ){
      width = mouse.x - event_state.container_left;
      height = mouse.y  - event_state.container_top;
      left = event_state.container_left;
      top = event_state.container_top;
    } else if($(event_state.evnt.target).hasClass('resize-handle-sw') ){
      width = event_state.container_width - (mouse.x - event_state.container_left);
      height = mouse.y  - event_state.container_top;
      left = mouse.x;
      top = event_state.container_top;
    } else if($(event_state.evnt.target).hasClass('resize-handle-nw') ){
      width = event_state.container_width - (mouse.x - event_state.container_left);
      height = event_state.container_height - (mouse.y - event_state.container_top);
      left = mouse.x;
      top = mouse.y;
      if(constrain || e.shiftKey){
        top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
      }
    } else if($(event_state.evnt.target).hasClass('resize-handle-ne') ){
      width = mouse.x - event_state.container_left;
      height = event_state.container_height - (mouse.y - event_state.container_top);
      left = event_state.container_left;
      top = mouse.y;
      if(constrain || e.shiftKey){
        top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
      }
    }
  
    // Optionally maintain aspect ratio
    if(constrain || e.shiftKey){
      height = width / orig_src.width * orig_src.height;
    }

    if(width > min_width && height > min_height && width < max_width && height < max_height){
      // To improve performance you might limit how often resizeImage() is called
      resizeImage(width, height);  
      // Without this Firefox will not re-calculate the the image dimensions until drag end
      $container.offset({'left': left, 'top': top});
    }
  }

  resizeImage = function(width, height){
  $(image_target).width(width).height(height);
  };
  
  resizeImageCanvas = function(width, height){
    resize_canvas.width = width;
    resize_canvas.height = height;
    resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);   
    $(image_target).attr('src', resize_canvas.toDataURL("image/png"));  
  //$(image_target).width(width).height(height);
  };

  startMoving = function(e){
    e.preventDefault();
    e.stopPropagation();
    saveEventState(e);
    $(document).on('mousemove touchmove', moving);
    $(document).on('mouseup touchend', endMoving);
  };

  endMoving = function(e){
    e.preventDefault();
    $(document).off('mouseup touchend', endMoving);
    $(document).off('mousemove touchmove', moving);
  };

  moving = function(e){
    var  mouse={}, touches;
    e.preventDefault();
    e.stopPropagation();
    
    touches = e.originalEvent.touches;
    
    mouse.x = (e.clientX || e.pageX || touches[0].clientX) + $(window).scrollLeft(); 
    mouse.y = (e.clientY || e.pageY || touches[0].clientY) + $(window).scrollTop();
    $container.offset({
      'left': mouse.x - ( event_state.mouse_x - event_state.container_left ),
      'top': mouse.y - ( event_state.mouse_y - event_state.container_top ) 
    });
    // Watch for pinch zoom gesture while moving
    if(event_state.touches && event_state.touches.length > 1 && touches.length > 1){
      var width = event_state.container_width, height = event_state.container_height;
      var a = event_state.touches[0].clientX - event_state.touches[1].clientX;
      a = a * a; 
      var b = event_state.touches[0].clientY - event_state.touches[1].clientY;
      b = b * b; 
      var dist1 = Math.sqrt( a + b );
      
      a = e.originalEvent.touches[0].clientX - touches[1].clientX;
      a = a * a; 
      b = e.originalEvent.touches[0].clientY - touches[1].clientY;
      b = b * b; 
      var dist2 = Math.sqrt( a + b );

      var ratio = dist2 /dist1;

      width = width * ratio;
      height = height * ratio;
      // To improve performance you might limit how often resizeImage() is called
      resizeImage(width, height);
    }
  };

  crop = function(){
    $('.resize-container').css("position","absolute");
    //Find the part of the image that is inside the crop box
    var left = $('.overlay').offset().left- $container.offset().left,
        top =  $('.overlay').offset().top - $container.offset().top,
        width = $('.overlay').width(),
        height = $('.overlay').height();
    
    crop_canvas = document.createElement('canvas');
  
    crop_canvas.width = width;
    crop_canvas.height = height;
  
    crop_canvas.getContext('2d').drawImage(image_target, left, top, width, height, 0, 0, width, height);
  var dataURL=crop_canvas.toDataURL("image/png");
  image_target.src=dataURL;
  orig_src.src=image_target.src;
  
  
  $(image_target).bind("load",function() {
    $(this).css({
      width:width,
      height:height
    }).unbind('load').parent().css({
      top:$('.overlay').offset().top- $('.crop-wrapper').offset().top,
      left:$('.overlay').offset().left- $('.crop-wrapper').offset().left
    })
  });
   $("#save").prop("disabled", false);
    //window.open(crop_canvas.toDataURL("image/png"));
  }

  init();
};

// Kick everything off with the target image
resizeableImage($('.resize-image'));
$("#upload").change(function()
  {
    console.log($(this).val())
    $("#camera_div").css("display","none");
    $(".crop-wrapper").css("display","block");
  $(".resize-container").css("position","relative");
    $(".js-crop").prop("disabled",false);
  
  });

function toblob(stuff) {
    var g, type, bi, ab, ua, b, i;
    g = stuff.split(',');
    if (g[0].split('png')[1])
        type = 'png';
    else if (g[0].split('jpeg')[1])
        type = 'jpeg';
    else
        return false;
    bi = atob(g[1]);
    ab = new ArrayBuffer(bi.length);
    ua = new Uint8Array(ab);
    for (i = 0; i < bi.length; i++) {
        ua[i] = bi.charCodeAt(i);
    }
    b = new Blob([ua], {
        type: "image/" + type
    });
    console.log(b);
    return b;
}
$("#camera").click(function()
{
  cameraon();
})
$("#take_snapshot").click(function()
{
  take_snapshot();
})
$("#skip_profile").click(function()
{

$("#image_div").css("display","none");

  $("#id_card_div").css("display","block");

});

 function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {
      type: mimeString
    });
  }

$('#save').on('click', function()
  {

    
    
    var image_data=crop_canvas.toDataURL("image/png")
   member_img_url=image_data;
   $(".loader").css("display","block");
 
     var blob = dataURItoBlob(image_data);
    var formData = new FormData();
    formData.append('image', blob);
    formData.append('membership_id',membership_id);
      $.ajax({
           type: "POST",
           url: "/changeMemberProfileImage",
          
           data:formData,
           contentType: false, //need this
      processData: false, //need this
      cache: false,
           success: function (response) {
            console.log(response);
            response=JSON.parse(response)
               if (response.data.status=='1') {
                $(".loader").css("display","None");
              // swal("Success","thank you for showing interest","success")
               
               $("#image_div").css("display","none");
                $("#id_card_div").css("display","block");
                
               } else {
                   
                    $(".loader").css("display","None");
                    // swal("Error",response.errors,"error");
                     
                    return false;
                   
               }
           },
           error:function(response)
           {
            console.log(response)
            $(".loader").css("display","None");
             swal('Error','Please try again','error')
             return false;
           }
           
       });


write_image(member_img_url);

$(".loader").css("display","none");
$("#image_div").css("display","none");
$("#id_card_div").css("display","block");


  });

function write_image(member_img_url)
{
   var c = document.getElementById("canvas_download");
   
    var ctx = c.getContext("2d");

    var img = new Image()
    var display_canvas=document.getElementById("canvas_display");
    var ctx_display = display_canvas.getContext("2d");
    var member_img=new Image();
    member_img.src=member_img_url;
member_img.setAttribute('crossOrigin', 'anonymous');
member_img.onload = function(){



         
   ctx_display.save();
    ctx_display.beginPath();
    ctx_display.arc(148,148, 60, 0, Math.PI * 2, true);
    ctx_display.closePath();
    ctx_display.clip();

    ctx_display.drawImage(member_img, 88,88 ,118, 118);

    ctx_display.beginPath();
    ctx_display.arc(148,148, 60, 0, Math.PI * 2, true);
     ctx_display.lineWidth = 5;
// line color
ctx_display.strokeStyle = '#d81615';
ctx_display.stroke();

    ctx_display.clip();
    ctx_display.closePath();
    ctx_display.restore();

    // downloadable canvas
    ctx.save();
    ctx.beginPath();
    ctx.arc(173,180, 60, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.clip();

   ctx.drawImage(member_img, 113,120,118,118);

    ctx.beginPath();
    ctx.arc(173,180, 60, 0, Math.PI * 2, true);
    ctx.lineWidth = 5;
// line color
ctx.strokeStyle = '#d81615';
ctx.stroke();

    ctx.clip();
    ctx.closePath();
    ctx.restore();

   
        
        

 }
 


}
function dataURItoBlob1(dataURI) {
  var byteString = atob(dataURI.split(',')[1]);
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) { ia[i] = byteString.charCodeAt(i); }
  return new Blob([ab], { type: 'image/jpeg' });
}


    // -----------------------------------------
function take_snapshot() {
    
      
      Webcam.snap( function(data_uri) {
       
document.getElementById('member_image').src="";
        document.getElementById('member_image').src=data_uri;
        
  
    
  image_target.src=data_uri;
  orig_src.src=image_target.src;
      $("#camera_div").css("display","none");
      $(".crop-wrapper").css("display","block");
       $(".js-crop").prop("disabled",false);
      } );
    }

    function cameraon()
    {
      $("#upload").val('');
      $(".crop-wrapper").css("display","none");
      $("#camera_div").css("display","block");
      Webcam.set({
      width: 300,
      height: 300,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#camera_image' );
    }

 var qrcode_img;
 function wrapText(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for(var n = 0; n < words.length; n++) {
          var testLine = line + words[n] + ' ';
          var metrics = context.measureText(testLine);
          var testWidth = metrics.width;
          if (testWidth > maxWidth && n > 0) {
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
          }
          else {
            line = testLine;
          }
        }
        context.fillText(line, x, y);
        return y;
      }

 var id_card_info;

 $.ajax({
           type: "GET",
           url: "/memberProfile",
           dataType: "json",
           
           ContentType:"application/json",
           success: function (response) {
            console.log(response);
               if (response.data.status=='1') {
               	id_card_info=response.data.children;

               	var name=id_card_info.name;
  
   membership_id=id_card_info.membership_id;
   var status=id_card_info.membership_through;
   var image_url=id_card_info.image_url;
   write_canvas(image_url,name,membership_id,'Telugu');

   var qr = new QRious({
    element: document.getElementById('qr'),
    value: "https://janasenaparty.org/getQRCodeDetails?id="+membership_id,
    background: 'white', // background color
    foreground: 'black', // foreground color
    level: 'L', // Error correction level of the QR code (L, M, Q, H)
    mime: 'image/png', // MIME type used to render the image for the QR code
    size: 120 // Size of the QR code in pixels.
})
qrcode_img=qr.toDataURL('image/png');

    }
               else{
               	alert("error")

               }
           
         }
 });

$scope.hidedata.loadingscrren = false;
   $("body,html").animate({
                scrollTop:  $(document).height()
           });

$("#upload_image").click(function()
{
  $("#image_div").css("display","block");
  $("#id_card_div").css("display","none");
   // $("body,html").animate({
   //              scrollTop:  100
   //         });
})

function write_canvas(member_img_url,name,membership_id,language)
{
  membership_card_front="/static/img/membership/front.jpg";
  if(language=='English')
  {
    
    membership_card_back="/static/img/membership/english.jpg";
  }
  else if(language=='Hindi')
  {

    
    membership_card_back="/static/img/membership/hindi.jpg";
  
  }
  else
  {
    
    membership_card_back="/static/img/membership/telugu.jpg";
  }
 
  
    var c = document.getElementById("canvas_download");
   
    var ctx = c.getContext("2d");

    var img = new Image()
    var display_canvas=document.getElementById("canvas_display");
    var ctx_display = display_canvas.getContext("2d");

     img.setAttribute('crossOrigin', 'anonymous');
 



 
    var member_img=new Image();
  img.onload = function(){

      ctx.drawImage(img, 0, 0,350,542); 
      ctx_display.drawImage(img, 0, 0,300,464);
member_img.src=member_img_url;
  }
 
img.setAttribute('crossOrigin', 'anonymous');
  img.src = membership_card_front;


   
// ;
member_img.setAttribute('crossOrigin', 'anonymous');
 member_img.onload = function(){



         
    ctx_display.save();
    ctx_display.beginPath();
    ctx_display.arc(148,148, 60, 0, Math.PI * 2, true);
    ctx_display.closePath();
    ctx_display.clip();

    ctx_display.drawImage(member_img, 88,88 ,118, 118);

    ctx_display.beginPath();
    ctx_display.arc(148,148, 60, 0, Math.PI * 2, true);
     ctx_display.lineWidth = 5;
// line color
ctx_display.strokeStyle = '#d81615';
ctx_display.stroke();

    ctx_display.clip();
    ctx_display.closePath();
    ctx_display.restore();

    // downloadable canvas
    ctx.save();
    ctx.beginPath();
    ctx.arc(173,180, 60, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.clip();

   ctx.drawImage(member_img, 113,120,118,118);

    ctx.beginPath();
    ctx.arc(173,180, 60, 0, Math.PI * 2, true);
    ctx.lineWidth = 5;
// line color
ctx.strokeStyle = '#d81615';
ctx.stroke();

    ctx.clip();
    ctx.closePath();
    ctx.restore();

    // end of downloadble canvas
          ctx.font = " bold 20px bebasneue   ";
          ctx.fillStyle='black';
         
          ctx_display.font = "bold 20px bebasneue   ";
          ctx_display.fillStyle='black';
        
        ctx_display.moveTo(150, 0);
        ctx_display.lineTo(150, 400);
        ctx_display.textAlign="center";
        ctx.moveTo(175, 0);
        ctx.lineTo(175, 500);
        ctx.textAlign="center";
         
          y=275;
          y=wrapText(ctx, name, 170, y, 280, 20);

          y=y+28;
          ctx.fillStyle='#d81615';
          ctx.fillText(String(membership_id),170,y)
          //  

        
          y=235;
          y=wrapText(ctx_display, name, 150, y, 250, 20);

          y=y+28;
           ctx_display.fillStyle='#d81615';
          ctx_display.fillText(String(membership_id),150,y)

 }
 
  
var qr_img=new Image();
   var back_img = new Image()
      back_img.src = membership_card_back;
    
     
     back_img.setAttribute('crossOrigin', 'anonymous');
     back_img.onload = function(){
     ctx.drawImage(back_img, 400,0,350,542);
      qr_img.src=qrcode_img;


}

    
     qr_img.setAttribute('crossOrigin', 'anonymous');
 qr_img.onload = function(){
     ctx.drawImage(qr_img, 531,63,95,95);
     



}

}


// canvas writing  

function downloadCanvas(link, canvasId, filename) {
 
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

document.getElementById('download').addEventListener('click', function() {
    var currentdate = new Date();
var datetime_string =  currentdate.getDate() + "_" + (currentdate.getMonth()+1) + "_" + currentdate.getFullYear() + " _" + currentdate.getHours() + "_" + currentdate.getMinutes() + "_" + currentdate.getSeconds();
  var filename="JANASENAPARTY_"+datetime_string+".png";
    downloadCanvas(this, 'canvas_download', filename);
}, false);












   

});