hod.controller('profileCtrl', function ($scope, $http, $filter, $rootScope,$location) {
    
   
    $scope.volunteer_div=false;
    $scope.voter_details_div=false;
    $scope.old_profile_details=true;
    $scope.new_voter_details=false;
    
     $scope.hidedata = {
       loadingscrren: true
   }
   $scope.validate_voter_id=function()
{
  
  var regex = /^[A-Za-z]{2,3}[0-9]*$/g;
  var voter_id=$scope.voterid;
  var length=voter_id.length;
  if(length>=10 && length<=14){
    if(regex.test(voter_id))
    {
      return true;
      
    }
    else
    {
      return false;
      
    }

}
else
{
   return false; 
}


}
 


    $http({
     method  : 'GET',
     url     : '/memberProfile',
     headers : {'Content-Type': undefined} 
    }).success(function(response) {
        if(response.data.status=='1')
        {
            $scope.hidedata.loadingscrren = false;
        $scope.profile_data = response.data.children;
        $rootScope.profile_data=response.data.children;
        if($scope.profile_data.membership_through=='S')
        {
            $scope.voter_div=true;
        }
        else
        {
             $scope.voter_div=false;
        }
        
        
        }
        
        else{
            alert(response.data.errors);
            $scope.hidedata.loadingscrren = false;
        }
    })   
$scope.showvotersdiv=function(){
    $scope.old_profile_details=false;
     $scope.voter_details_div=true;

    
};
$scope.cancel=function()
{
    $scope.old_profile_details=true;
     $scope.voter_details_div=false;
}
 $scope.check_voter_id=function(){

    var voter_id= $scope.voterid;
    if($scope.validate_voter_id())
    {
        $scope.hidedata.loadingscrren = true;
        $http({
     method  : 'GET',
     url     : '/checkVoterId?voter_id='+voter_id,
     headers : {'Content-Type': undefined} 
    }).success(function(response) {
        if(response.data.status=='1')
        {
            console.log(response.data.children);
            $scope.hidedata.loadingscrren = false;
            $scope.voter_details_div=false;
            $scope.new_voter_details=true;
        $scope.voter_data = response.data.children;
         $scope.hidedata.loadingscrren = false;
        }
        
        else{
            
            $scope.hidedata.loadingscrren = false;
            swal('Error',response.errors,"error");
        }
    }) 
        
    }
    else
    {
         swal("Error","Enter Valid VoterID","error");
         return false;
    }
    
console.log(voter_id);
    
};

$scope.confirm_voter_id=function()
{$scope.hidedata.loadingscrren = true;
     var voter_id= $scope.voterid;
   $http({
     method  : 'POST',
     url     : '/updateVoterId?voter_id='+voter_id,
     headers : {'Content-Type': undefined} 
    }).success(function(response) {
        if(response.data.status=='1')
        {
           
            $scope.hidedata.loadingscrren = false;
           
          window.location.reload();
        }
        
        else{
            
            $scope.hidedata.loadingscrren = false;
            swal('Error',response.errors,"error");
        }
    }) 
  

    
}
$scope.cancelconfirmation=function()
{
window.location.reload();
}
    
});