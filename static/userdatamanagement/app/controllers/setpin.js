hod.controller('setpinCtrl', function ($scope,$route, $http, $filter, $rootScope,$location) {
  $("#savepin").click(function()
  {
    var pin=$("#pin").val();
    var confirmpin=$("#confirmpin").val();

    if(pin!=confirmpin)
    {
      swal("Error","Password and Confirm Password are not matched","error");
      return false;

    }

    else
    {
      var data={pin:pin};
      console.log(data);
       $.ajax({
           type: "GET",
           url: "/creatememberpin?pin="+pin,
            data:data,
             dataType: "json",
           contentType:"application/json",
      
           success: function (response) {
            console.log(response);
            
               if (response.data.status=='1') {
                $(".loader").css("display","None");
              swal("Success","your pin saved Successfully","success")
               
               window.location.reload();
               } else {
                   
                    $(".loader").css("display","None");
                    swal("Error",response.errors,"error");
                     
                    return false;
                   
               }
           },
           error:function(response)
           {
            console.log(response)
            $(".loader").css("display","None");
             swal('Error','Please try again','error')
             return false;
           }
           
       });

    }
  }) 
    
});