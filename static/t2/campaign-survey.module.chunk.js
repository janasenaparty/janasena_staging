webpackJsonp(["campaign-survey.module"],{

/***/ "./node_modules/rxjs/_esm5/add/operator/catch.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/operator/catch.js");
/** PURE_IMPORTS_START .._.._Observable,.._.._operator_catch PURE_IMPORTS_END */


__WEBPACK_IMPORTED_MODULE_0__Observable__["a" /* Observable */].prototype.catch = __WEBPACK_IMPORTED_MODULE_1__operator_catch__["a" /* _catch */];
__WEBPACK_IMPORTED_MODULE_0__Observable__["a" /* Observable */].prototype._catch = __WEBPACK_IMPORTED_MODULE_1__operator_catch__["a" /* _catch */];
//# sourceMappingURL=catch.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/add/operator/map.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/operator/map.js");
/** PURE_IMPORTS_START .._.._Observable,.._.._operator_map PURE_IMPORTS_END */


__WEBPACK_IMPORTED_MODULE_0__Observable__["a" /* Observable */].prototype.map = __WEBPACK_IMPORTED_MODULE_1__operator_map__["a" /* map */];
//# sourceMappingURL=map.js.map


/***/ }),

/***/ "./src/app/modules/campaign-survey/campaign-survey.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignSurveyModule", function() { return CampaignSurveyModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_campaign_survey_component__ = __webpack_require__("./src/app/modules/campaign-survey/view/campaign-survey.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__campaign_survey_routing__ = __webpack_require__("./src/app/modules/campaign-survey/campaign-survey.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_campaign_survey_service__ = __webpack_require__("./src/app/modules/campaign-survey/service/campaign-survey.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CampaignSurveyModule = /** @class */ (function () {
    function CampaignSurveyModule() {
    }
    CampaignSurveyModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__view_campaign_survey_component__["a" /* CampaignSurveyComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */], __WEBPACK_IMPORTED_MODULE_3__campaign_survey_routing__["a" /* CampaignSurveyRoutingModule */]],
            providers: [__WEBPACK_IMPORTED_MODULE_4__service_campaign_survey_service__["a" /* CampaignSurveyService */]]
        })
    ], CampaignSurveyModule);
    return CampaignSurveyModule;
}());



/***/ }),

/***/ "./src/app/modules/campaign-survey/campaign-survey.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampaignSurveyRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_campaign_survey_component__ = __webpack_require__("./src/app/modules/campaign-survey/view/campaign-survey.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__view_campaign_survey_component__["a" /* CampaignSurveyComponent */],
        children: []
    }
];
var CampaignSurveyRoutingModule = /** @class */ (function () {
    function CampaignSurveyRoutingModule() {
    }
    CampaignSurveyRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
        })
    ], CampaignSurveyRoutingModule);
    return CampaignSurveyRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/campaign-survey/service/campaign-survey.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampaignSurveyService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CampaignSurveyService = /** @class */ (function () {
    function CampaignSurveyService(http) {
        this.http = http;
    }
    CampaignSurveyService.prototype.getData = function () {
        var url = "http://localhost:8081/getCampaign";
        return this.http.get(url);
    };
    CampaignSurveyService.prototype.setData = function (data) {
        var url = "http://localhost:8081/setCampaignResponse";
        return this.http.post(url, data);
    };
    CampaignSurveyService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], CampaignSurveyService);
    return CampaignSurveyService;
}());



/***/ }),

/***/ "./src/app/modules/campaign-survey/view/campaign-survey.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"base-container\">\r\n    <div class=\"v-wrapper\" *ngIf=\"showDemo\" (click)=\"toggleDemo()\">\r\n        <i class=\"fa fa-home fa-2x\" title=\"Home\"></i>\r\n    </div>\r\n    <div class=\"v-wrapper\" *ngIf=\"!showDemo\" (click)=\"getCampaignData()\">\r\n        <img src=\"../../../../assets/images/survey.png\" class=\"survey-icon\" title=\"Take Survey\"/>\r\n    </div>\r\n    <div *ngIf=\"showDemo\" class=\"c-wrapper\">\r\n        <div class=\"q-wrapper\">\r\n            <div class=\"prev-section\" (click)=\"decIdx()\">\r\n                <i class=\"fa fa-chevron-left\"></i>\r\n            </div>\r\n            <div class=\"content-section\">\r\n                <div class=\"question\">{{currentQuestion}}</div>\r\n                <div class=\"options-contanier\">\r\n                    <button class=\"option\" [class.selected-option]=\"currentSelectedOption == option\" *ngFor=\"let option of currentOptions; let index = index;\" (click)=\"onAnswerSelected(index)\">{{option}}</button>\r\n                </div>\r\n                <div class=\"options-contanier\">\r\n                    <button class=\"option submit\" [disabled]=\"disableSubmit\" (click)=\"onSubmitAnswers()\">Submit</button>\r\n                </div>\r\n            </div>\r\n            <div class=\"next-section\" (click)=\"incIdx()\">\r\n                    <i class=\"fa fa-chevron-right\"></i>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/modules/campaign-survey/view/campaign-survey.component.scss":
/***/ (function(module, exports) {

module.exports = ".base-container {\n  height: 75%;\n  width: 100%;\n  background: white;\n  background: url('janasena-logo.44254b1ad8d4d3056815.png');\n  background-repeat: no-repeat;\n  background-position: center; }\n\n.v-wrapper {\n  margin: 10px 20px;\n  font-size: 20px;\n  position: fixed;\n  top: 7em;\n  cursor: pointer; }\n\n.c-wrapper {\n  width: 700px;\n  height: 350px;\n  margin: 0 auto;\n  border-radius: 8px;\n  position: relative;\n  top: 7.5em;\n  background-color: #acd0e5f5; }\n\n.survey-icon {\n  height: 34px; }\n\n.q-wrapper {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.prev-section, .content-section, .next-section {\n  display: inline-block;\n  vertical-align: middle;\n  text-align: center; }\n\n.question {\n  font-family: cursive;\n  font-weight: 500;\n  margin-bottom: 35px; }\n\n.option {\n  display: inline-block;\n  vertical-align: middle;\n  background: #ffffff8f;\n  font-size: 16px;\n  margin: 15px 10px;\n  width: 98px;\n  height: 35px;\n  border-radius: 4px;\n  font-family: cursive;\n  outline: none;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  border: 1px solid #000; }\n\n.prev-section, .next-section {\n  width: 10%;\n  height: 100%;\n  position: relative;\n  color: lightslategrey;\n  font-size: 16px;\n  margin-top: 150px;\n  cursor: pointer; }\n\n.content-section {\n  width: 78%;\n  height: 100%;\n  position: relative;\n  color: #000;\n  font-size: 17px; }\n\n.submit {\n  margin-top: 30px; }\n\n.submit[disabled] {\n  cursor: not-allowed;\n  opacity: 0.6; }\n\n.selected-option {\n  border: 2px solid #3643c7; }\n"

/***/ }),

/***/ "./src/app/modules/campaign-survey/view/campaign-survey.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampaignSurveyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_campaign_survey_service__ = __webpack_require__("./src/app/modules/campaign-survey/service/campaign-survey.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CampaignSurveyComponent = /** @class */ (function () {
    function CampaignSurveyComponent(campaignSurveyService, http) {
        this.campaignSurveyService = campaignSurveyService;
        this.http = http;
        this.showDemo = false;
        this.idx = 0;
        this.currentQuestion = "";
        this.currentOptions = [];
        this.currentSelectedOption = "";
        this.disableSubmit = true;
        this.questionnarie = [];
    }
    CampaignSurveyComponent.prototype.ngOnInit = function () { };
    CampaignSurveyComponent.prototype.ngOnChanges = function () { };
    //1. Handler for click of view demo
    //2. Service call to get the campaign-survey information
    //3. TODO -> Custom exception handling
    CampaignSurveyComponent.prototype.getCampaignData = function () {
        //this.campaignSurveyService.getData().subscribe((response: any) => {
        //response = JSON.parse(response);
        var response = [
            {
                "question": "Did you verify your voter ID status?",
                "options": ["Yes", "No"],
                "selectedOption": ""
            },
            {
                "question": "Is individual responsibility more important than social responsibility?",
                "options": ["Yes", "No", "Not sure"],
                "selectedOption": ""
            },
            {
                "question": "Political accountability should be mandated!",
                "options": ["Agreed", "Disagree", "Neutral", "Not sure"],
                "selectedOption": ""
            },
            {
                "question": "How much are you satisfied with the efforts of government?",
                "options": ["Very Good", "Good", "Fair", "Bad"],
                "selectedOption": ""
            }
        ];
        this.questionnarie = response;
        this.setQusAndOptions();
        this.toggleDemo();
        //}, error => {			
        //consoloe.log("Error while retrieving data!");
        //});
    };
    CampaignSurveyComponent.prototype.toggleDemo = function () {
        this.showDemo = !this.showDemo;
    };
    CampaignSurveyComponent.prototype.decIdx = function () {
        if (this.idx) {
            --this.idx;
            this.setQusAndOptions();
        }
    };
    CampaignSurveyComponent.prototype.incIdx = function () {
        if (this.idx != this.questionnarie.length - 1) {
            ++this.idx;
            this.setQusAndOptions();
        }
        else {
            this.setQusAndOptions();
        }
    };
    CampaignSurveyComponent.prototype.setQusAndOptions = function () {
        this.currentQuestion = this.questionnarie[this.idx].question;
        this.currentOptions = this.questionnarie[this.idx].options;
        this.currentSelectedOption = this.questionnarie[this.idx].selectedOption;
    };
    CampaignSurveyComponent.prototype.onAnswerSelected = function (idx) {
        this.questionnarie[this.idx]['selectedOption'] = this.questionnarie[this.idx]['options'][idx];
        this.incIdx();
        this.checkAnswers();
    };
    CampaignSurveyComponent.prototype.checkAnswers = function () {
        var that = this;
        var counter = 0;
        this.questionnarie.map(function (o) {
            if (o['selectedOption']) {
                counter++;
            }
        });
        if (counter == this.questionnarie.length) {
            this.disableSubmit = false;
        }
        else {
            this.disableSubmit = true;
        }
    };
    CampaignSurveyComponent.prototype.onSubmitAnswers = function () {
        //this.campaignSurveyService.setData(this.questionnarie).subscribe((response: any) => {
        this.questionnarie = [];
        this.currentOptions = [];
        this.currentSelectedOption = "";
        this.currentQuestion = "";
        this.toggleDemo();
        //}, error => {			
        //consoloe.log("Error while retrieving data!");
        //});
    };
    CampaignSurveyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'campaign-survey',
            template: __webpack_require__("./src/app/modules/campaign-survey/view/campaign-survey.component.html"),
            styles: [__webpack_require__("./src/app/modules/campaign-survey/view/campaign-survey.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewEncapsulation */].None
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_campaign_survey_service__["a" /* CampaignSurveyService */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], CampaignSurveyComponent);
    return CampaignSurveyComponent;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]
            ],
            exports: [],
            declarations: [],
            providers: [],
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ })

});
//# sourceMappingURL=campaign-survey.module.chunk.js.map