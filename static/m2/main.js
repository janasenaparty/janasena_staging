(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/agechart/agechart.component.css":
/*!*************************************************!*\
  !*** ./src/app/agechart/agechart.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FnZWNoYXJ0L2FnZWNoYXJ0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/agechart/agechart.component.html":
/*!**************************************************!*\
  !*** ./src/app/agechart/agechart.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\">\n  <div class=\"col-md-2\"><b>18-30:</b> {{barChartData[0].data}}</div>\n  <div class=\"col-md-2\"><b>31-45:</b> {{barChartData[1].data}}</div>\n  <div class=\"col-md-2\"><b>46-60:</b> {{barChartData[2].data}}</div>\n  <div class=\"col-md-2\"><b>> 60:</b> {{barChartData[3].data}}</div>\n  <div class=\"col-md-4\"><b>Age not available:</b> {{barChartData[4].data}}</div>\n</div>\n<div  style=\"display: block; height: 400px;width: 100%\">\n  <canvas baseChart\n          [datasets]=\"barChartData\"\n          [labels]=\"barChartLabels\"\n          [options]=\"barChartOptions\"\n          [legend]=\"barChartLegend\"\n          [colors]=\"barColors\"\n          [chartType]=\"barChartType\"\n          (chartHover)=\"chartHovered($event)\"\n          (chartClick)=\"chartClicked($event)\"></canvas>\n</div>\n"

/***/ }),

/***/ "./src/app/agechart/agechart.component.ts":
/*!************************************************!*\
  !*** ./src/app/agechart/agechart.component.ts ***!
  \************************************************/
/*! exports provided: AgechartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgechartComponent", function() { return AgechartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/shared.service */ "./src/app/services/shared.service.ts");



var AgechartComponent = /** @class */ (function () {
    function AgechartComponent(shared) {
        this.shared = shared;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var d = data.datasets;
                        var sum = 0;
                        for (var _i = 0, d_1 = d; _i < d_1.length; _i++) {
                            var p = d_1[_i];
                            sum = sum + p.data[0];
                        }
                        if (sum === 0) {
                            sum = 1;
                        }
                        var percentage = d[tooltipItem.datasetIndex].data[0] / sum * 100;
                        var percentage_string = percentage.toFixed(2);
                        return percentage_string.toString() + '%';
                    }
                }
            },
        };
        this.barChartLabels = ['Age'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barColors = [
            { backgroundColor: '#4cff11' },
            { backgroundColor: '#00ceb2' },
            { backgroundColor: '#ff0023' },
            { backgroundColor: '#0006ff' },
            { backgroundColor: '#a100f7' }
        ];
        this.barChartData = [
            { data: [0], label: '18-30' },
            { data: [0], label: '30-45' },
            { data: [0], label: '45-60' },
            { data: [0], label: '>60' },
            { data: [0], label: 'Age_NA' }
        ];
    }
    // events
    AgechartComponent.prototype.chartClicked = function (e) {
        // console.log(e);
    };
    AgechartComponent.prototype.chartHovered = function (e) {
        // console.log(e);
    };
    AgechartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getData().subscribe(function (data) {
            // console.log(data);
            if (data != null) {
                if (data.data != null) {
                    _this.barChartData = [
                        { data: [data.data['18-30']], label: '18-30' },
                        { data: [data.data['31-45']], label: '30-45' },
                        { data: [data.data['46-60']], label: '45-60' },
                        { data: [data.data['>60']], label: '>60' },
                        { data: [data.data['Age_NA']], label: 'Age_NA' }
                    ];
                }
                else {
                    _this.barChartData = [
                        { data: [0], label: '18-30' },
                        { data: [0], label: '30-45' },
                        { data: [0], label: '45-60' },
                        { data: [0], label: '>60' },
                        { data: [0], label: 'Age_NA' }
                    ];
                }
            }
            else {
                _this.barChartData = [
                    { data: [0], label: '18-30' },
                    { data: [0], label: '30-45' },
                    { data: [0], label: '45-60' },
                    { data: [0], label: '>60' },
                    { data: [0], label: 'Age_NA' }
                ];
                console.log('I am here');
                _this.shared.set_loading(false);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], AgechartComponent.prototype, "data", void 0);
    AgechartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-agechart',
            template: __webpack_require__(/*! ./agechart.component.html */ "./src/app/agechart/agechart.component.html"),
            styles: [__webpack_require__(/*! ./agechart.component.css */ "./src/app/agechart/agechart.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], AgechartComponent);
    return AgechartComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Tooltip container */\r\n.tooltip {\r\n  position: relative;\r\n  display: block;\r\n  /* If you want dots under the hoverable text */\r\n}\r\n/* Tooltip text */\r\n.tooltip .tooltiptext {\r\n  visibility: hidden;\r\n  width: 120px;\r\n  background-color: black;\r\n  color: #fff;\r\n  text-align: center;\r\n  padding: 5px 0;\r\n  border-radius: 6px;\r\n\r\n  /* Position the tooltip text - see examples below! */\r\n  position: absolute;\r\n  z-index: 1;\r\n}\r\n/* Show the tooltip text when you mouse over the tooltip container */\r\n.tooltip:hover .tooltiptext {\r\n  visibility: visible;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdUJBQXVCO0FBQ3ZCO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZiwrQ0FBK0M7Q0FDaEQ7QUFFRCxrQkFBa0I7QUFDbEI7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHdCQUF3QjtFQUN4QixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7O0VBRW5CLHFEQUFxRDtFQUNyRCxtQkFBbUI7RUFDbkIsV0FBVztDQUNaO0FBRUQscUVBQXFFO0FBQ3JFO0VBQ0Usb0JBQW9CO0NBQ3JCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBUb29sdGlwIGNvbnRhaW5lciAqL1xyXG4udG9vbHRpcCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIC8qIElmIHlvdSB3YW50IGRvdHMgdW5kZXIgdGhlIGhvdmVyYWJsZSB0ZXh0ICovXHJcbn1cclxuXHJcbi8qIFRvb2x0aXAgdGV4dCAqL1xyXG4udG9vbHRpcCAudG9vbHRpcHRleHQge1xyXG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmc6IDVweCAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuXHJcbiAgLyogUG9zaXRpb24gdGhlIHRvb2x0aXAgdGV4dCAtIHNlZSBleGFtcGxlcyBiZWxvdyEgKi9cclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuLyogU2hvdyB0aGUgdG9vbHRpcCB0ZXh0IHdoZW4geW91IG1vdXNlIG92ZXIgdGhlIHRvb2x0aXAgY29udGFpbmVyICovXHJcbi50b29sdGlwOmhvdmVyIC50b29sdGlwdGV4dCB7XHJcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n\r\n<div class=\"container\">\r\n  <app-dropdowns #dd ></app-dropdowns>\r\n  <div class=\"row\">\r\n    <h3 class=\"col-md-2 col-md-offset-1\">Total:{{total}}</h3>\r\n    <!--<div class=\"col-md-1 col-md-offset-1\" style=\"padding:10px;\">-->\r\n      <!--<button class=\"btn\" (click)=\"exportAsXLSX()\">-->\r\n        <!--<i class=\"fa fa-file-excel-o\"></i>-->\r\n        <!--</button>-->\r\n      <!--<div class=\"tooltip\">Hover over me-->\r\n        <!--<span class=\"tooltiptext\">Tooltip text</span>-->\r\n      <!--</div>-->\r\n    <!--</div>-->\r\n\r\n    <!--<button class=\"col-md-1 col-md-offset-1\" ><i class=\"fa fa-file-excel-o\" style=\"font-size:48px;color:blue\"></i></button>-->\r\n  </div>\r\n  <div class=\"row\" style=\"padding-top:40px;\">\r\n\r\n    <app-mfchart [data]=male_female class=\"col-md-4 col-md-offset-1\"></app-mfchart>\r\n    <app-agechart [data]=age class=\"col-md-5 col-md-offset-1\"></app-agechart>\r\n  </div>\r\n</div>\r\n<ng-template #customLoadingTemplate>\r\n  <div class=\"custom-class\">\r\n    <h3>\r\n      Loading...\r\n    </h3>\r\n\r\n  </div>\r\n</ng-template>\r\n\r\n<ngx-loading [show]=\"loading\" [config]=\"{ backdropBorderRadius: '3px' }\" [template]=\"customLoadingTemplate\"></ngx-loading>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _services_excel_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/excel.service */ "./src/app/services/excel.service.ts");
/* harmony import */ var _dropdowns_dropdowns_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dropdowns/dropdowns.component */ "./src/app/dropdowns/dropdowns.component.ts");





var AppComponent = /** @class */ (function () {
    function AppComponent(shared, cd, excelService) {
        this.shared = shared;
        this.cd = cd;
        this.excelService = excelService;
        this.total = 0;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.get_loading().subscribe(function (data) {
            _this.loading = data;
        });
        this.shared.getData().subscribe(function (data) {
            if (data) {
                if (data.data) {
                    _this.total = data.data.total;
                    _this.data = [data.data];
                }
                else {
                    _this.total = 0;
                }
            }
            else {
                _this.total = 0;
            }
        });
        this.shared.getPCid().subscribe(function (data) {
            _this.dd.resetAC();
            _this.dd.resetmandal();
            _this.dd.resetbooth();
        });
        this.shared.getACid().subscribe(function (data) {
            _this.dd.resetmandal();
            _this.dd.resetbooth();
        });
        this.shared.getmid().subscribe(function (data) {
            _this.dd.resetbooth();
        });
    };
    AppComponent.prototype.ngOnChanges = function (changes) {
        // console.log(changes);
    };
    AppComponent.prototype.exportAsXLSX = function () {
        if (!this.data) {
            alert('Please select any dropdown to download the specific data.');
        }
        else {
            console.log(this.data);
            this.excelService.exportAsExcelFile(this.data, 'sample');
        }
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        this.loading = false;
        this.cd.detectChanges();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dd'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _dropdowns_dropdowns_component__WEBPACK_IMPORTED_MODULE_4__["DropdownsComponent"])
    ], AppComponent.prototype, "dd", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('tbl'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AppComponent.prototype, "table", void 0);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _services_excel_service__WEBPACK_IMPORTED_MODULE_3__["ExcelService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mfchart_mfchart_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mfchart/mfchart.component */ "./src/app/mfchart/mfchart.component.ts");
/* harmony import */ var _agechart_agechart_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./agechart/agechart.component */ "./src/app/agechart/agechart.component.ts");
/* harmony import */ var _dropdowns_dropdowns_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dropdowns/dropdowns.component */ "./src/app/dropdowns/dropdowns.component.ts");
/* harmony import */ var _dropdowns_pcd_pcd_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./dropdowns/pcd/pcd.component */ "./src/app/dropdowns/pcd/pcd.component.ts");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _dropdowns_mandald_mandald_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dropdowns/mandald/mandald.component */ "./src/app/dropdowns/mandald/mandald.component.ts");
/* harmony import */ var _dropdowns_boothd_boothd_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dropdowns/boothd/boothd.component */ "./src/app/dropdowns/boothd/boothd.component.ts");
/* harmony import */ var _dropdowns_acd_acd_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./dropdowns/acd/acd.component */ "./src/app/dropdowns/acd/acd.component.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_loading__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-loading */ "./node_modules/ngx-loading/fesm5/ngx-loading.js");

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _mfchart_mfchart_component__WEBPACK_IMPORTED_MODULE_6__["MfchartComponent"],
                _agechart_agechart_component__WEBPACK_IMPORTED_MODULE_7__["AgechartComponent"],
                _dropdowns_dropdowns_component__WEBPACK_IMPORTED_MODULE_8__["DropdownsComponent"],
                _dropdowns_acd_acd_component__WEBPACK_IMPORTED_MODULE_13__["AcdComponent"],
                _dropdowns_pcd_pcd_component__WEBPACK_IMPORTED_MODULE_9__["PcdComponent"],
                _dropdowns_mandald_mandald_component__WEBPACK_IMPORTED_MODULE_11__["MandaldComponent"],
                _dropdowns_boothd_boothd_component__WEBPACK_IMPORTED_MODULE_12__["BoothdComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_5__["ChartsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClientModule"],
                ng2_select__WEBPACK_IMPORTED_MODULE_10__["SelectModule"],
                ngx_loading__WEBPACK_IMPORTED_MODULE_16__["NgxLoadingModule"].forRoot({})
            ],
            providers: [_services_shared_service__WEBPACK_IMPORTED_MODULE_14__["SharedService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dropdowns/acd/acd.component.css":
/*!*************************************************!*\
  !*** ./src/app/dropdowns/acd/acd.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Ryb3Bkb3ducy9hY2QvYWNkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dropdowns/acd/acd.component.html":
/*!**************************************************!*\
  !*** ./src/app/dropdowns/acd/acd.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span>Select Assembly Constituency</span>\n<ng-select #sel\n           [allowClear]=\"true\"\n           [items]=\"items\"\n           [disabled]=\"disabled\"\n           (data)=\"refreshValue($event)\"\n           (selected)=\"selected($event)\"\n           (removed)=\"removed($event)\"\n           (typed)=\"typed($event)\"\n           placeholder=\"Assembly Constituency\">\n</ng-select>\n"

/***/ }),

/***/ "./src/app/dropdowns/acd/acd.component.ts":
/*!************************************************!*\
  !*** ./src/app/dropdowns/acd/acd.component.ts ***!
  \************************************************/
/*! exports provided: AcdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcdComponent", function() { return AcdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_3__);




var AcdComponent = /** @class */ (function () {
    function AcdComponent(shared) {
        this.shared = shared;
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    AcdComponent.prototype.reset = function () {
        if (this.sel) {
            var activeItem = this.sel.activeOption;
            if (activeItem) {
                this.sel.remove(activeItem);
            }
        }
    };
    Object.defineProperty(AcdComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    AcdComponent.prototype.selected = function (value) {
        var _this = this;
        // console.log(this.value);
        this.shared.sendACid(value.id);
        this.shared.getACData(this.pcid, value.id).subscribe(function (data) {
            _this.shared.sendData(data);
        });
    };
    AcdComponent.prototype.removed = function (value) {
        // console.log('Removed value is: ', value);
    };
    AcdComponent.prototype.typed = function (value) {
        // console.log('New search input: ', value);
    };
    AcdComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    AcdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getPCid().subscribe(function (data) {
            _this.pcid = data;
            _this.value = {};
            _this.shared.getAssemblyConstituencies().subscribe(function (l) {
                _this.items = l.filter(function (p) {
                    return p['pcid'] === _this.pcid;
                });
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_select__WEBPACK_IMPORTED_MODULE_3__["SelectComponent"])
    ], AcdComponent.prototype, "sel", void 0);
    AcdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-acd',
            template: __webpack_require__(/*! ./acd.component.html */ "./src/app/dropdowns/acd/acd.component.html"),
            styles: [__webpack_require__(/*! ./acd.component.css */ "./src/app/dropdowns/acd/acd.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], AcdComponent);
    return AcdComponent;
}());



/***/ }),

/***/ "./src/app/dropdowns/boothd/boothd.component.css":
/*!*******************************************************!*\
  !*** ./src/app/dropdowns/boothd/boothd.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Ryb3Bkb3ducy9ib290aGQvYm9vdGhkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dropdowns/boothd/boothd.component.html":
/*!********************************************************!*\
  !*** ./src/app/dropdowns/boothd/boothd.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span>Select Polling Booth</span>\n<ng-select #sel\n          [allowClear]=\"true\"\n           [items]=\"items\"\n           [disabled]=\"disabled\"\n           (data)=\"refreshValue($event)\"\n           (selected)=\"selected($event)\"\n           (removed)=\"removed($event)\"\n           (typed)=\"typed($event)\"\n           placeholder=\"Select Polling Booth\">\n</ng-select>\n"

/***/ }),

/***/ "./src/app/dropdowns/boothd/boothd.component.ts":
/*!******************************************************!*\
  !*** ./src/app/dropdowns/boothd/boothd.component.ts ***!
  \******************************************************/
/*! exports provided: BoothdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoothdComponent", function() { return BoothdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_3__);




var BoothdComponent = /** @class */ (function () {
    function BoothdComponent(shared) {
        this.shared = shared;
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    BoothdComponent.prototype.reset = function () {
        if (this.sel) {
            var activeItem = this.sel.activeOption;
            if (activeItem) {
                this.sel.remove(activeItem);
            }
        }
    };
    Object.defineProperty(BoothdComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    BoothdComponent.prototype.selected = function (value) {
        var _this = this;
        this.shared.getBoothData(this.mandal_id, value.id).subscribe(function (data) {
            // console.log(data);
            _this.shared.sendData(data);
            _this.shared.set_loading(false);
        });
    };
    BoothdComponent.prototype.removed = function (value) {
        // console.log('Removed value is: ', value);
    };
    BoothdComponent.prototype.typed = function (value) {
        // console.log('New search input: ', value);
    };
    BoothdComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    BoothdComponent.prototype.populateBooths = function (mandal_id) {
        var _this = this;
        if (this.mandal_id) {
            this.shared.getBooths(this.mandal_id).subscribe(function (l) {
                // console.log(l);
                var d = l['data'];
                _this.items = d.filter(function (p) {
                    return p['mandal_id'] === _this.mandal_id;
                });
                _this.shared.set_loading(false);
            });
        }
    };
    BoothdComponent.prototype.ngOnInit = function () {
        var that = this;
        this.shared.getmid().subscribe(function (d) {
            console.log(d);
            that.value = {};
            if (d) {
                that.mandal_id = d.id;
                that.populateBooths(d.id);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_select__WEBPACK_IMPORTED_MODULE_3__["SelectComponent"])
    ], BoothdComponent.prototype, "sel", void 0);
    BoothdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-boothd',
            template: __webpack_require__(/*! ./boothd.component.html */ "./src/app/dropdowns/boothd/boothd.component.html"),
            styles: [__webpack_require__(/*! ./boothd.component.css */ "./src/app/dropdowns/boothd/boothd.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], BoothdComponent);
    return BoothdComponent;
}());



/***/ }),

/***/ "./src/app/dropdowns/dropdowns.component.css":
/*!***************************************************!*\
  !*** ./src/app/dropdowns/dropdowns.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Ryb3Bkb3ducy9kcm9wZG93bnMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dropdowns/dropdowns.component.html":
/*!****************************************************!*\
  !*** ./src/app/dropdowns/dropdowns.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\" style=\"padding-top: 40px;\">\n  <div class=\"col-md-3\">\n    <app-pcd #pc></app-pcd>\n  </div>\n  <div class=\"col-md-3\">\n    <app-acd #ac></app-acd>\n  </div>\n  <div class=\"col-md-3\">\n    <app-mandald #mandal></app-mandald>\n  </div>\n  <div class=\"col-md-3\">\n    <app-boothd #booth></app-boothd>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/dropdowns/dropdowns.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dropdowns/dropdowns.component.ts ***!
  \**************************************************/
/*! exports provided: DropdownsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownsComponent", function() { return DropdownsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _pcd_pcd_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pcd/pcd.component */ "./src/app/dropdowns/pcd/pcd.component.ts");
/* harmony import */ var _acd_acd_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./acd/acd.component */ "./src/app/dropdowns/acd/acd.component.ts");
/* harmony import */ var _mandald_mandald_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mandald/mandald.component */ "./src/app/dropdowns/mandald/mandald.component.ts");
/* harmony import */ var _boothd_boothd_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./boothd/boothd.component */ "./src/app/dropdowns/boothd/boothd.component.ts");






var DropdownsComponent = /** @class */ (function () {
    function DropdownsComponent() {
    }
    DropdownsComponent.prototype.ngOnInit = function () {
    };
    DropdownsComponent.prototype.resetPC = function () {
        this.pc.reset();
    };
    DropdownsComponent.prototype.resetAC = function () {
        this.ac.reset();
    };
    DropdownsComponent.prototype.resetmandal = function () {
        this.mandal.reset();
    };
    DropdownsComponent.prototype.resetbooth = function () {
        this.booth.reset();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('pc'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _pcd_pcd_component__WEBPACK_IMPORTED_MODULE_2__["PcdComponent"])
    ], DropdownsComponent.prototype, "pc", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ac'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _acd_acd_component__WEBPACK_IMPORTED_MODULE_3__["AcdComponent"])
    ], DropdownsComponent.prototype, "ac", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mandal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _mandald_mandald_component__WEBPACK_IMPORTED_MODULE_4__["MandaldComponent"])
    ], DropdownsComponent.prototype, "mandal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('booth'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _boothd_boothd_component__WEBPACK_IMPORTED_MODULE_5__["BoothdComponent"])
    ], DropdownsComponent.prototype, "booth", void 0);
    DropdownsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dropdowns',
            template: __webpack_require__(/*! ./dropdowns.component.html */ "./src/app/dropdowns/dropdowns.component.html"),
            styles: [__webpack_require__(/*! ./dropdowns.component.css */ "./src/app/dropdowns/dropdowns.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DropdownsComponent);
    return DropdownsComponent;
}());



/***/ }),

/***/ "./src/app/dropdowns/mandald/mandald.component.css":
/*!*********************************************************!*\
  !*** ./src/app/dropdowns/mandald/mandald.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Ryb3Bkb3ducy9tYW5kYWxkL21hbmRhbGQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dropdowns/mandald/mandald.component.html":
/*!**********************************************************!*\
  !*** ./src/app/dropdowns/mandald/mandald.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span>Select Mandal</span>\n<ng-select #sel\n          [allowClear]=\"true\"\n           [items]=\"items\"\n           [disabled]=\"disabled\"\n           (data)=\"refreshValue($event)\"\n           (selected)=\"selected($event)\"\n           (removed)=\"removed($event)\"\n           (typed)=\"typed($event)\"\n           placeholder=\"Mandal\">\n</ng-select>\n"

/***/ }),

/***/ "./src/app/dropdowns/mandald/mandald.component.ts":
/*!********************************************************!*\
  !*** ./src/app/dropdowns/mandald/mandald.component.ts ***!
  \********************************************************/
/*! exports provided: MandaldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MandaldComponent", function() { return MandaldComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_3__);




var MandaldComponent = /** @class */ (function () {
    function MandaldComponent(shared) {
        this.shared = shared;
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
        this.value = {};
    }
    MandaldComponent.prototype.reset = function () {
        if (this.sel) {
            var activeItem = this.sel.activeOption;
            if (activeItem) {
                this.sel.remove(activeItem);
            }
        }
    };
    Object.defineProperty(MandaldComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    MandaldComponent.prototype.selected = function (value) {
        var _this = this;
        this.mandal_id = value.id;
        this.shared.sendmid(this.acid, value.id, value);
        this.shared.getMandalData(this.acid, value.id).subscribe(function (data) {
            _this.shared.set_loading(false);
            _this.shared.sendData(data);
        });
    };
    MandaldComponent.prototype.removed = function (value) {
        // console.log('Removed value is: ', value);
    };
    MandaldComponent.prototype.typed = function (value) {
        // console.log('New search input: ', value);
    };
    MandaldComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    MandaldComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getACid().subscribe(function (data) {
            _this.value = {};
            _this.acid = data;
            _this.shared.getMandals().subscribe(function (l) {
                _this.items = l.filter(function (p) {
                    return p['constituency_id'] === _this.acid;
                });
                _this.shared.set_loading(false);
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_select__WEBPACK_IMPORTED_MODULE_3__["SelectComponent"])
    ], MandaldComponent.prototype, "sel", void 0);
    MandaldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mandald',
            template: __webpack_require__(/*! ./mandald.component.html */ "./src/app/dropdowns/mandald/mandald.component.html"),
            styles: [__webpack_require__(/*! ./mandald.component.css */ "./src/app/dropdowns/mandald/mandald.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], MandaldComponent);
    return MandaldComponent;
}());



/***/ }),

/***/ "./src/app/dropdowns/pcd/pcd.component.css":
/*!*************************************************!*\
  !*** ./src/app/dropdowns/pcd/pcd.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Ryb3Bkb3ducy9wY2QvcGNkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dropdowns/pcd/pcd.component.html":
/*!**************************************************!*\
  !*** ./src/app/dropdowns/pcd/pcd.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span>Select Parliament Constituency</span>\n<ng-select #pc\n           [allowClear]=\"true\"\n           [items]=\"items\"\n           [disabled]=\"disabled\"\n           (data)=\"refreshValue($event)\"\n           (selected)=\"selected($event)\"\n           (removed)=\"removed($event)\"\n           (typed)=\"typed($event)\"\n           placeholder=\"Parliament Constituency\">\n</ng-select>\n"

/***/ }),

/***/ "./src/app/dropdowns/pcd/pcd.component.ts":
/*!************************************************!*\
  !*** ./src/app/dropdowns/pcd/pcd.component.ts ***!
  \************************************************/
/*! exports provided: PcdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PcdComponent", function() { return PcdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_3__);

///<reference path="../../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>



var PcdComponent = /** @class */ (function () {
    function PcdComponent(shared) {
        this.shared = shared;
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
    }
    PcdComponent.prototype.reset = function () {
        if (this.sel) {
            var activeItem = this.sel.activeOption;
            if (activeItem) {
                this.sel.remove(activeItem);
            }
        }
    };
    Object.defineProperty(PcdComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    PcdComponent.prototype.selected = function (value) {
        var _this = this;
        console.log(value);
        this.sendPCid(value.id);
        this.shared.getPCData(value.id).subscribe(function (data) {
            _this.shared.set_loading(false);
            _this.shared.sendData(data);
        });
    };
    PcdComponent.prototype.removed = function (value) {
    };
    PcdComponent.prototype.typed = function (value) {
    };
    PcdComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    PcdComponent.prototype.sendPCid = function (id) {
        this.shared.sendPCid(id);
    };
    PcdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getParliamentConstituencies().subscribe(function (data) {
            // console.log(data);
            _this.items = data;
            _this.shared.set_loading(false);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_select__WEBPACK_IMPORTED_MODULE_3__["SelectComponent"])
    ], PcdComponent.prototype, "sel", void 0);
    PcdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pcd',
            template: __webpack_require__(/*! ./pcd.component.html */ "./src/app/dropdowns/pcd/pcd.component.html"),
            styles: [__webpack_require__(/*! ./pcd.component.css */ "./src/app/dropdowns/pcd/pcd.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], PcdComponent);
    return PcdComponent;
}());



/***/ }),

/***/ "./src/app/mfchart/mfchart.component.css":
/*!***********************************************!*\
  !*** ./src/app/mfchart/mfchart.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21mY2hhcnQvbWZjaGFydC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/mfchart/mfchart.component.html":
/*!************************************************!*\
  !*** ./src/app/mfchart/mfchart.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"><b>Male:</b> {{barChartData[0].data}}</div>\r\n    <div class=\"col-md-4\"><b>Female:</b> {{barChartData[1].data}}</div>\r\n    <div class=\"col-md-4\"><b>NA:</b> {{barChartData[2].data}}</div>\r\n  </div>\r\n    <div  style=\"display: block; height:400px;\">\r\n      <canvas baseChart\r\n              [datasets]=\"barChartData\"\r\n              [labels]=\"barChartLabels\"\r\n              [options]=\"barChartOptions\"\r\n              [legend]=\"barChartLegend\"\r\n              [colors]=\"barColors\"\r\n              [chartType]=\"barChartType\"\r\n              (chartHover)=\"chartHovered($event)\"\r\n              (chartClick)=\"chartClicked($event)\"></canvas>\r\n\r\n    </div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/mfchart/mfchart.component.ts":
/*!**********************************************!*\
  !*** ./src/app/mfchart/mfchart.component.ts ***!
  \**********************************************/
/*! exports provided: MfchartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MfchartComponent", function() { return MfchartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/shared.service */ "./src/app/services/shared.service.ts");

///<reference path="../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>


var MfchartComponent = /** @class */ (function () {
    function MfchartComponent(shared) {
        this.shared = shared;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var d = data.datasets;
                        var sum = 0;
                        for (var _i = 0, d_1 = d; _i < d_1.length; _i++) {
                            var p = d_1[_i];
                            sum = sum + p.data[0];
                        }
                        if (sum === 0) {
                            sum = 1;
                        }
                        var percentage = d[tooltipItem.datasetIndex].data[0] / sum * 100;
                        var percentage_string = percentage.toFixed(2);
                        return percentage_string.toString() + '%';
                    }
                }
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        };
        this.barChartLabels = ['Gender'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barColors = [
            { backgroundColor: '#4cff11' },
            { backgroundColor: '#00ceb2' },
            { backgroundColor: '#ff0023' }
        ];
        this.barChartData = [
            { data: [0], label: 'Male' },
            { data: [0], label: 'Female' },
            { data: [0], label: 'NA' }
        ];
    }
    // events
    MfchartComponent.prototype.chartClicked = function (e) {
        // console.log(e);
    };
    MfchartComponent.prototype.chartHovered = function (e) {
        // console.log(e);
    };
    MfchartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getData().subscribe(function (data) {
            if (data != null) {
                _this.shared.set_loading(false);
                if (data.data != null) {
                    _this.barChartData = [
                        { data: [data.data.male], label: 'Male' },
                        { data: [data.data.female], label: 'Female' },
                        { data: [data.data.na], label: 'NA' }
                    ];
                }
                else {
                    _this.barChartData = [
                        { data: [0], label: 'Male' },
                        { data: [0], label: 'Female' },
                        { data: [0], label: 'NA' }
                    ];
                }
            }
            else {
                _this.barChartData = [
                    { data: [0], label: 'Male' },
                    { data: [0], label: 'Female' },
                    { data: [0], label: 'NA' }
                ];
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MfchartComponent.prototype, "data", void 0);
    MfchartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mfchart',
            template: __webpack_require__(/*! ./mfchart.component.html */ "./src/app/mfchart/mfchart.component.html"),
            styles: [__webpack_require__(/*! ./mfchart.component.css */ "./src/app/mfchart/mfchart.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], MfchartComponent);
    return MfchartComponent;
}());



/***/ }),

/***/ "./src/app/services/excel.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/excel.service.ts ***!
  \*******************************************/
/*! exports provided: ExcelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExcelService", function() { return ExcelService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_3__);




var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = /** @class */ (function () {
    function ExcelService() {
    }
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_3__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_3__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], { type: EXCEL_TYPE });
        file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());



/***/ }),

/***/ "./src/app/services/shared.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/shared.service.ts ***!
  \********************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var SharedService = /** @class */ (function () {
    function SharedService(http) {
        this.http = http;
        this.pcid = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.acid = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.mid = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.bid = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.dat = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.load = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
    }
    SharedService.prototype.getParliamentConstituencies = function () {
        this.set_loading(true);
        return this.http.get('/static/m2/assets/pc.json');
    };
    SharedService.prototype.getAssemblyConstituencies = function () {
        return this.http.get('/static/m2/assets/pcac.json');
    };
    SharedService.prototype.getMandals = function () {
        return this.http.get('/static/m2/assets/constituency_pc_mand.json');
    };
    SharedService.prototype.getBooths = function (mandal_id) {
        if (mandal_id) {
            return this.http.get('/booth_mandal_admins/get_booths?mandal_id=' + mandal_id.toString());
        }
        else {
            return this.http.get('/booth_mandal_admins/get_booths');
        }
    };
    SharedService.prototype.sendPCid = function (id) {
        this.pcid.next(id);
    };
    SharedService.prototype.getPCid = function () {
        return this.pcid.asObservable();
    };
    SharedService.prototype.sendACid = function (id) {
        this.acid.next(id);
    };
    SharedService.prototype.getACid = function () {
        return this.acid.asObservable();
    };
    SharedService.prototype.sendmid = function (acid, id, dat) {
        var b = {
            id: id,
            dat: dat
        };
        this.mid.next(b);
    };
    SharedService.prototype.getmid = function () {
        return this.mid.asObservable();
    };
    //  Male female data etc.
    SharedService.prototype.getPCData = function (pc) {
        this.set_loading(true);
        return this.http.post('/booth_mandal_admins/get_membership_counts', {
            mtype: 'pc',
            pc: pc
        });
    };
    SharedService.prototype.sendPCValue = function (ac) {
    };
    SharedService.prototype.getACData = function (pc, ac) {
        this.set_loading(true);
        return this.http.post('/booth_mandal_admins/get_membership_counts', {
            mtype: 'ac',
            pc: pc,
            ac: ac
        });
    };
    SharedService.prototype.sendData = function (data) {
        return this.dat.next(data);
    };
    SharedService.prototype.getData = function () {
        return this.dat.asObservable();
    };
    SharedService.prototype.getMandalData = function (ac, mandal_id) {
        this.set_loading(true);
        return this.http.post('/booth_mandal_admins/get_membership_counts', {
            mtype: 'mandal',
            ac: ac,
            mandal: mandal_id
        });
    };
    SharedService.prototype.getBoothData = function (mandal, booth) {
        this.set_loading(true);
        return this.http.post('/booth_mandal_admins/get_membership_counts', {
            mtype: 'booth',
            mandal: mandal,
            booth: booth
        });
    };
    SharedService.prototype.set_loading = function (value) {
        this.load.next(value);
    };
    SharedService.prototype.get_loading = function () {
        return this.load.asObservable();
    };
    SharedService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\janasena\WebstormProjects\m2\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map