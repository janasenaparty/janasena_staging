import json

from flask import Blueprint, request, render_template

from appholder import csrf
from constants import empty_check
from utilities.decorators import crossdomain
from utilities.others import give_cursor_json

mug = Blueprint('mug', __name__)


@mug.route('/membership_groups', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def membership_groups():
    def temp(con, cur):
        if request.method == 'POST':
            cur.execute("""
                            select name_path, group_name  , nlevel(name_path),id
                            from local_member_hierarchy
                            order by nlevel(name_path),id
                        """)
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})
        pass

    return give_cursor_json(temp)


@mug.route('/temp_router', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def temp_router():
    return render_template('membership_groups/g1.html')