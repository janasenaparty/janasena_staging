import json
import random
import string

import requests
import subprocess

TOKEN = 'ee51a4c7bd15696f190bccc344a4567b4579617474c04e7ba7a97da0c0d82fa3'
headers = {
    "Content-Type": "application/json",

    "Authorization": "Bearer " + TOKEN
}

base_url = 'https://api.digitalocean.com'

def get_web_server_droplet_id():
    url = base_url + '/v2/droplets?tag_name=masterweb'
    r = requests.get(url, headers=headers)
    data = json.loads(r.text)
    if data:
        return data['droplets'][0]['id']
    else:
        return None

def create_snapshot(droplet_id=88237579):
    string_url = '/v2/droplets/%d/actions' % droplet_id
    url = base_url + string_url
    print url
    data = {'type': 'snapshot',
            'name': ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))}
    print data
    r = requests.post(url, headers=headers, data=json.dumps(data))

    data = json.loads(r.text)
    print data
    return 0

def get_last_snapshot(droplet_id=88237579):
    string_url = '/v2/droplets/%d/snapshots'%droplet_id
    url = base_url + string_url
    r = requests.get(url, headers=headers)
    print r.text
    return json.loads(r.text)


def delete_last_snapshot():
    snaplist = get_last_snapshot()
    if snaplist['snapshots']:
        #delete
        string_url = '/v2/snapshots/%d' % snaplist['snapshots'][0]['id']
        url = base_url + string_url
        r = requests.delete(url, headers=headers)
        pass
    else:
        pass

def get_clones():
    url = base_url + '/v2/droplets?tag_name=web_clone'
    r = requests.get(url, headers=headers)
    data = json.loads(r.text)
    return data['droplets']


if __name__ == "__main__":
    print "Deleting previous snapshot"
    delete_last_snapshot()
    print "Creating new snapshot"
    create_snapshot()
    clones= get_clones()
    for clone in clones:
        clone_ip= clone['networks']['v4'][1]['ip_address']
        process_string = 'sh /home/deploy/janasena-live/sync.sh '+ str(clone_ip)
        rc = subprocess.call(process_string, shell=True)
        pass

    pass