################## Membership Books tracking Portal #####################

import json
import psycopg2
from flask import Blueprint, request, session, render_template, flash, url_for
import config
from utilities.classes import Assembly_constituencies_details, encrypt_decrypt,Membership_details,Voter_details
from utilities.decorators import crossdomain, admin_login_required, required_roles
from appholder import csrf
from psycopg2.extras import RealDictCursor
offline_books = Blueprint('offline_books', __name__)

@offline_books.route('/get_offline_books', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def GetOfflineBooks():
    try:
        req_type=request.values.get("type")
        req_value=request.values.get("value")
        if req_type is not None and req_value is not None:

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            data=[]
            if req_type=='district' :
                if req_value=='all':

                    cur.execute("select ac.district_name,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number group by ac.district_name order by ac.district_name")
                    data=cur.fetchall()
                else:
                    cur.execute("select ac.constituency_name,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number where ac.district_id=%s group by ac.constituency_name order by ac.constituency_name",(req_value,))
                    data=cur.fetchall()
            if req_type=='assembly' :
                state=request.values.get('state_id')
                if req_value=='all':

                    cur.execute("select ac.district_name,ac.constituency_name,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number group by ac.district_name,ac.constituency_name order by ac.district_name")
                    data=cur.fetchall()
                else:
                    cur.execute("select ac.district_name,ac.constituency_name,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number where ac.state_id=%s and ac.constituency_id=%s group by ac.state_id,ac.district_name,ac.constituency_name order by ac.constituency_name",(state,req_value,))
                    data=cur.fetchall()
            if req_type=='state' :
                if req_value=='all':

                    cur.execute("select ac.state_id,case when ac.state_id =1 then 'Andhra Pradesh' else 'Telangana' end as state,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number group by ac.state_id,state order by ac.state_id")
                    data=cur.fetchall()
                else:
                    cur.execute("select ac.state_id,ac.district_name,count(*)as issued,sum(case  when m.book_current_status ='I' then 1 else 0 end) as to_be_received,sum(case  when m.book_current_status ='C' then 1 else 0 end) as data_entry_pending,sum(case  when m.book_current_status ='E' then 1 else 0 end) as data_entry_completed FROM membership_book_issue mi join assembly_constituencies ac on ac.district_id=mi.district_id and ac.constituency_id=mi.constituency_id join membership_books m on m.book_number=mi.book_number where ac.state_id=%s group by ac.state_id,ac.district_name order by ac.state_id",(req_value,))
                    data=cur.fetchall()
            
            con.close()
            return json.dumps({"errors":[],"status":"1","data":data})
        else:
            return json.dumps({"errors":["parameters missing"],"status":"0"})

    except (Exception,psycopg2.Error) as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})

@offline_books.route('/get_stock_books', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def GetStockBooks():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("select array(select mb.book_number from membership_books mb where book_current_status='S' order by mb.book_number) as arr ")
        data = cur.fetchone()
        data=data['arr']
        con.close()
        return json.dumps(data)
    except (Exception,psycopg2.Error) as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})
@offline_books.route('/get_collected_books', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def GetCollectedBooks():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("select array(select mb.book_number from membership_books mb where book_current_status='C' order by mb.book_number) as arr ")
        data = cur.fetchone()
        data=data['arr']
        con.close()
        return json.dumps(data)
    except (Exception,psycopg2.Error) as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})
@offline_books.route('/entry_offline_books', methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def EntryOfflineBooks():
    if request.method=='GET':
        return render_template("admin/books_portal/vendor.html")
    else:
        data=request.json
        parameters=["date","delivery_ph","delivery_name","recevier_name","book_type","book_lng","tags"]
        if set(parameters).issubset(set(data.keys())):
            for parameter in parameters:
                if data[parameter] is None or data[parameter]=='':
                    return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                for book in data["tags"]:
                    try:
                        cur.execute("""insert into membership_books(
                            book_number,
                            delivered_by,
                            delivered_phone,
                            delivered_date,
                            received_by,
                            book_type,
                            book_language,
                            book_current_status)
                        values(%s,%s,%s,%s,%s,%s,%s,%s)""",(book,data['delivery_name'],data['delivery_ph'],data['date'],data['recevier_name'],
                            data['book_type'],data['book_lng'],'S'))
                        con.commit()
                    except psycopg2.Error as e:
                        print str(e)
                        print "hereee"
                        continue
                con.close()
                return json.dumps({"errors":[],"status":"1"})
            except (Exception,psycopg2.Error) as e:
                print str(e) 
                return json.dumps({"errors":["something wrong.please contact tech team"],"status":"0"}) 

        else:
            return json.dumps({"errors":["Parameters Missing. please check "],"status":"0"})

@offline_books.route('/issue_offline_books', methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def issueOfflineBooks():
    if request.method=='GET':
        return render_template("admin/books_portal/issue.html")
    else:
        data=request.json

        parameters=["date","phno","name","jsp_id","district","consignment","constituency","book_details","issue_method","place"]
        if set(parameters).issubset(set(data.keys())):

            for parameter in parameters:
                if parameter not in ["place","consignment"]:
                    if data[parameter] is None or data[parameter]=='':
                        return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
            try:
                membership_instance=Membership_details()
                if not membership_instance.check_membership_existence(str(data["jsp_id"]).upper()):
                        return json.dumps({"errors":["Details not found with given JSP ID "],"status":"0"})

                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                for book in data["book_details"]:
                    try:
                        cur.execute("""insert into membership_book_issue(
                           book_number,
                            issued_to_name,
                            jsp_id,
                            phone,
                            district_id,
                            constituency_id,
                            place,
                            issued_dttm,
                            issue_type,consignment_number)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(book,data["name"],data["jsp_id"],data["phno"],data["district"],data["constituency"],data["place"],data["date"],data["issue_method"],data['consignment']))
                        con.commit()
                    except psycopg2.Error as e:
                        print str(e)
                        
                        continue
                cur.execute("update membership_books set book_current_status=%s where book_number in %s",('I',tuple(data["book_details"])))
                con.commit()
                con.close()
                return json.dumps({"errors":[],"status":"1"})
            except (Exception,psycopg2.Error) as e:
                print str(e) 
                return json.dumps({"errors":["something wrong.please contact tech team"],"status":"0"}) 

        else:
            return json.dumps({"errors":["Parameters Missing. please check "],"status":"0"})

@offline_books.route('/books_collection_portal', methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def BooksCollectionPortal():
    if request.method=='GET':
        return render_template("admin/books_portal/collection.html")
    else:
        try:
            data=request.json
           
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            parameters=["jsp_id","books"]
            
            if set(parameters).issubset(set(data.keys())):
                
                for parameter in parameters:
                    
                    if data[parameter] is None or data[parameter]=='':
                        return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
                
                cur.execute("update membership_book_issue set return_dttm=now() where jsp_id=%s and book_number in %s",(data['jsp_id'],tuple(data["books"])))
                cur.execute("update membership_books set book_current_status='C' where book_number in %s",(tuple(data['books']),))
                con.commit()
                con.close()
                return json.dumps({"errors":[],"status":"1"})
            return json.dumps({"errors":["Parameters Missing. please check "],"status":"0"})
            

        except (Exception,psycopg2.Error) as e:
                print str(e) 
                return json.dumps({"errors":["something wrong.please contact tech team"],"status":"0"})     

@offline_books.route('/books_enroll_portal', methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def BooksEnrollPortal():
    if request.method=='GET':
        return render_template("admin/books_portal/enroll.html")
    else:
        try:
            data=request.json
           
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            parameters=["jsp_id","books","name","date","phone"]
            
            if set(parameters).issubset(set(data.keys())):
                
                for parameter in parameters:
                    
                    if data[parameter] is None or data[parameter]=='':
                        return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
                membership_instance=Membership_details()
                if not membership_instance.check_membership_existence(str(data["jsp_id"]).upper()):
                        return json.dumps({"errors":["Details not found with given JSP ID "],"status":"0"})

                cur.execute("update membership_book_issue set entry_by_jsp_id=%s,entry_by_phone=%s,entry_assigned_to=%s,entry_assigned_dttm=%s  where  book_number in %s",(data['jsp_id'],data["phone"],data["name"],data["date"],tuple(data["books"])))
                cur.execute("update membership_books set book_current_status='E' where book_number in %s",(tuple(data['books']),))
                con.commit()
                con.close()
                return json.dumps({"errors":[],"status":"1"})
            return json.dumps({"errors":["Parameters Missing. please check "],"status":"0"})
            

        except (Exception,psycopg2.Error) as e:
                print str(e) 
                return json.dumps({"errors":["something wrong.please contact tech team"],"status":"0"})     

@offline_books.route('/get_issed_books', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def get_issed_books():
    search_type=request.values.get("search_type")
    search_value=request.values.get("search_value")
    if search_type is None or search_type=='':
        return json.dumps({"errors":["search_type missing "],"status":"0"})
    if search_value is None or search_value=='':
        return json.dumps({"errors":["search_value missing "],"status":"0"})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        base_query="SELECT bi.jsp_id,bi.issued_to_name,bi.phone,ac.district_name,ac.constituency_name,bi.place,cast(date(bi.issued_dttm) as text) as date,(array_agg(bi.book_number)) AS value FROM membership_book_issue bi join assembly_constituencies ac on ac.district_id=bi.district_id  and ac.constituency_id=bi.constituency_id  where {{search_type}}=%s and bi.return_dttm is null GROUP BY bi.jsp_id,bi.issued_to_name,bi.phone,ac.district_name,ac.constituency_name,bi.place,date "
        if search_type=='P':
            search="bi.phone"
        elif search_type=='M':
            search="bi.jsp_id"
        elif search_type=='B':
            search="bi.book_number"
        else:
           return json.dumps({"errors":[" invalid search_type "],"status":"0"}) 
        base_query =base_query.replace("{{search_type}}",search)
        cur.execute(base_query,(search_value,))
        row=cur.fetchone()
        if row is not None:
            return json.dumps({"errors":[],"status":"1","data":row})
        else:
            return json.dumps({"errors":["no data found"],"status":"2","data":{}})

    except (Exception,psycopg2.Error) as e:
        print str(e)
        return json.dumps({"errors":["something wrong.please contact tech team"],"status":"0"})     


@offline_books.route('/offline_books_portal', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["OB","MB","AD"])
@csrf.exempt
def OfflineBooksPortal():
    if request.method=='GET':
        summary={}

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            cur.execute("""SELECT count(*) as total, 
             COUNT(*) filter (where book_current_status !='S') as issued,   
             COUNT(*) filter (where book_current_status ='S') as stock,
            COUNT(*) filter (where book_current_status='I') as to_be_received,
            COUNT(*) filter (where book_current_status='C') as data_entry_pending,
            COUNT(*) filter (where book_current_status='E') as data_entry_completed from membership_books""")
            data=cur.fetchone()
           
            summary["total_books"]=data['total']
            summary["issued"]=data['issued']
            summary["books_stock"]=data['stock']
            summary["to_be_received"]=data['to_be_received']
            summary["data_entry_pending"]=data['data_entry_pending']
            summary["data_entry_completed"]=data['data_entry_completed']
            con.close()
        except (Exception,psycopg2.Error) as e:
            print str(e)
        return render_template("admin/books_portal/summary.html",summary=summary)


    
        