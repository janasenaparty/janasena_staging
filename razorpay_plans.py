from main import *
import time

import config
import app
## parameters ########
plan_name="Monthly 10Rs By Rambabu"
plan_description=" Optional"
amount=10
currency="INR"
interval=1
period="monthly"
start_at=int(time.time())
total_count=12
def checkout():
	plan_response=create_rp_plan(period,interval,plan_details={
	    "name": plan_name,
	    "description": plan_description,
	    "amount": int(amount*100),
	    "currency": "INR"
	  },notes={})
	rsp_plan=json.loads(plan_response.text)
	print rsp_plan
	try:
		if 'id' in rsp_plan.keys():
			plan_id= rsp_plan["id"]

			unixtime=int(time.time())
			
			subscription_response=create_rp_subscription(plan_id,total_count,start_at,notes={})
			subscription_response=json.loads(subscription_response.text)
			
			if 'status' in subscription_response.keys():
				if subscription_response["status"]=='created':
				## checkout button 
					subscription_id=subscription_response["id"]
					print subscription_id
					return render_template("subscription_heckout.html",
						razor_pay_key=config.razor_pay_key,subscription_id=subscription_id,plan_name=plan_name,plan_description=plan_description,customer_name="Rambabu",customer_email="rambabu.v68@gmail.com")
			else:
				print subscription_response['error']['code']
				print subscription_response['error']['description']
				

		else:
			print rsp_plan['error']['code']
			print rsp_plan['error']['description']
			print rsp_plan['error']['field']
	except Exception as e:
		print str(e)

checkout()