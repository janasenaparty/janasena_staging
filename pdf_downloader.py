import requests 
import os
import psycopg2
import config
from psycopg2.extras import RealDictCursor
con = psycopg2.connect(config.DB_CONNECTION)
cur = con.cursor(cursor_factory=RealDictCursor)
cur.execute("select * from affidavits")
rows=cur.fetchall()
print len(rows)
ac_path="e:/affidavits/ac/"
pc_path="e:/affidavits/pc/"
for row in rows:
	
	if str(row['party_name'])=='Janasena Party ':
		
		file_url = row['affidavit']
		if file_url!='':  
			r = requests.get(file_url, stream = True) 
			name= row['candidate_name']+'_'+str(row['constituency_id'])+'_'+str(row['seq_id'])+'.pdf' 
			path=''
			if row['type']=='ac':
				path=ac_path
			else:
				path=pc_path
			with open(path+name,"wb") as pdf: 
				for chunk in r.iter_content(chunk_size=1024): 
			  
					 # writing one chunk at a time to pdf file 
					 if chunk: 
						 pdf.write(chunk) 

				print name +' completed'