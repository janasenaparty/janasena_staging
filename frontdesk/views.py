import hashlib
from datetime import datetime

import psycopg2

from flask import Blueprint, request, json, render_template, session, url_for, flash
from flask_login import current_user, logout_user
from psycopg2.extras import RealDictCursor
from werkzeug.utils import redirect

import config
from appholder import csrf
from celery_tasks.sms_email import sendSMS
from utilities.classes import Assembly_constituencies_details
from utilities.decorators import crossdomain, frontdesk_login_required, admin_login_required, required_roles
from utilities.general import getAllOfficeLocations, upload_image, checkUserAuth

blueprint_frontdesk = Blueprint('frontdesk', __name__)

@blueprint_frontdesk.route('/changePasswordFrontdesk', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@frontdesk_login_required
def changePasswordFrontdesk():
    if request.method == 'POST':

        oldpass = request.values.get('oldpass')
        newpass = request.values.get('newpass')

        if oldpass is None or oldpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_oldpass_MISSING'], 'data': {'status': '0'}})
        if newpass is None or newpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_newpass_MISSING'], 'data': {'status': '0'}})

        try:
            admin_id = current_user.id
            print admin_id
            session_id = session['session']

            unique = 'tomatoPotatoadmin'

            old_hashed_password = hashlib.sha256(oldpass + unique).hexdigest()

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute('select user_mod_hash from front_desk_team where id=%s', (admin_id,))
            password = cur.fetchone()[0]

            if str(password) == str(unicode(old_hashed_password, errors='ignore')):

                new_HashedPassword = hashlib.sha256(newpass + unique).hexdigest()
                cur.execute('update front_desk_team  set user_mod_hash=%s,update_dttm=now() where id=%s',
                            (new_HashedPassword, admin_id,))

                con.commit()

                js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'Password Changed successfully'}})

                return js

            else:
                js = json.dumps({'errors': ['old password does not match'], 'data': {'status': '0'}})

                return js



        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)

            js = json.dumps({'errors': ['Exception Occured'], 'data': {'status': '0'}})
            return js
    else:
        return render_template("visitors/changepassword.html")


@blueprint_frontdesk.route('/geJSPtOfficeLocations', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def geJSPtOfficeLocations():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select l.seq_id,l.office_name,l.district,l.assembly,l.status,ac.first_name from jsp_office_locations l  join admin_team  ac on ac.admin_id=l.created_by  order by l.seq_id ")
        office_locations = cur.fetchall()
        return render_template("admin/office_locations.html", office_locations=office_locations)
    except psycopg2.Error as e:
        print str(e)
        return str(e)


@blueprint_frontdesk.route('/createOfficeLocation', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def createOfficeLocation():
    if request.method == 'POST':
        district = request.values.get('district')
        assembly = request.values.get('assembly')
        location_name = request.values.get('location_name')
        address = request.values.get('address')

        if district is None or district == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_district_MISSING'], 'data': {'status': '0'}})

        if assembly is None or assembly == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_assembly_MISSING'], 'data': {'status': '0'}})
        if location_name is None or location_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_location_name_MISSING'], 'data': {'status': '0'}})
        if address is None or address == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_address_MISSING'], 'data': {'status': '0'}})

        else:
            try:
                con = None
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                admin_id = current_user.id
                cur.execute(
                    'insert into jsp_office_locations(district,assembly,createdttm,update_dttm,office_name,address,status,state,created_by) values(%s,%s,now(),now(),%s,%s,%s,%s,%s)',
                    (str(district), assembly, str(location_name), str(address), 'A', '1', admin_id))
                con.commit()
                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
            except psycopg2.Error as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    else:
        try:
            assembly_instance = Assembly_constituencies_details()
            districts = assembly_instance.get_districts_list()
            data = []
            for district in districts:
                data.append(str(list(district)[0]).rstrip())
            return render_template("visitors/createnewoffice.html", data=data)
        except psycopg2.Error as e:
            print str(e)
            return str(e)



@blueprint_frontdesk.route('/createFrontDeskUser', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def createFrontDeskUser():
    if request.method == 'POST':

        uname = request.values.get('uname')
        password = request.values.get('password')
        first_name = request.values.get('first_name')
        last_name = request.values.get('last_name')

        location_id = request.values.get('location_id')
        mobile = request.values.get('mobile')

        if uname is None or uname == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_uname_MISSING'], 'data': {'status': '0'}})
        if password is None or password == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_password_MISSING'], 'data': {'status': '0'}})
        if first_name is None or first_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_first_name_MISSING'], 'data': {'status': '0'}})
        if last_name is None or last_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_last_name_MISSING'], 'data': {'status': '0'}})
        if mobile is None or mobile == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_mobile_MISSING'], 'data': {'status': '0'}})
        if location_id is None or location_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_location_id_MISSING'], 'data': {'status': '0'}})
        # if role is None or role == "":
        #     return json.dumps({'errors': ['BAD_REQUEST','PARAMETER_role_MISSING'],'data' : {'status':'0'}})
        else:
            try:
                con = None
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                admin_id = current_user.id
                unique = 'tomatoPotatoadmin'

                email = str(uname).lower()

                key = hashlib.sha256(password + unique).hexdigest()
                cur.execute("select * from front_desk_team where mobile=%s or lower(user_email)=%s", (mobile, email))
                row = cur.fetchone()
                if row is None:
                    admin_id = current_user.id
                    cur.execute(
                        'insert into front_desk_team(user_email,user_mod_hash,createdttm,update_dttm,first_name,last_name,role,mobile,status,location_id,created_by) values(%s,%s,now(),now(),%s,%s,%s,%s,%s,%s,%s)',
                        (str(email), key, str(first_name), str(last_name), 'F', mobile, 'A', location_id, admin_id))
                    con.commit()
                    con.close()
                    return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
                else:
                    return json.dumps({'errors': ['Email or Mobile already Existed'], 'data': {'status': '0'}})


            except psycopg2.Error as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    else:
        try:
            data = getAllOfficeLocations()
            return render_template("visitors/createfrontdesk.html", data=data)
        except psycopg2.Error as e:
            print str(e)
            return str(e)


@blueprint_frontdesk.route('/getOfficeLocations', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getOfficeLocations():
    try:

        rows = getAllOfficeLocations()
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows}})

    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": ["something went wrong"], "data": {"status": "0"}})


@blueprint_frontdesk.route('/visitorsDashboard', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD", 'OP'])
def visitorsDashboard():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select v.name,v.mobile,v.email,v.assembly,v.district,v.age,v.gender,v.category,v.file_url,o.office_name from visitors v join jsp_office_locations o on o.seq_id=v.visitor_location_id  order by v.createdttm desc limit 1000")
        rows = cur.fetchall()
        con.close()
        assembly_instance = Assembly_constituencies_details()
        districts = assembly_instance.get_districts_list()
        districts_data = []
        for district in districts:
            districts_data.append(str(list(district)[0]).rstrip())
        return render_template("visitors/visitorDashboard.html", data=rows, districts=districts_data)

    except psycopg2.Error as e:
        print str(e)
        con.close()
        return "Something Wrong"



@blueprint_frontdesk.route('/getTodaysVisitors', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@frontdesk_login_required
def getTodaysVisitors():
    try:
        admin_id = current_user.id
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select name,mobile,email,assembly,district,age,gender,category,file_url from visitors where entered_by_id=%s and date(createdttm)=TIMESTAMP 'today' order by createdttm",
            (admin_id,))
        rows = cur.fetchall()
        con.close()
        return render_template("visitors/todays_visitors.html", data=rows)

    except psycopg2.Error as e:
        print str(e)
        con.close()
        return "Something Wrong"


@blueprint_frontdesk.route('/getFrontDeskTeam', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getFrontDeskTeam():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select f.id,f.user_email,f.first_name,f.last_name,f.mobile,f.status,ac.first_name as created_by,l.office_name from front_desk_team f  join admin_team  ac on ac.admin_id=f.created_by join jsp_office_locations l on l.seq_id=f.location_id order by f.id ")
        frontdesk_team = cur.fetchall()
        return render_template("admin/frontdesk_team.html", frontdesk_team=frontdesk_team)
    except psycopg2.Error as e:
        print str(e)
        return str(e)


@blueprint_frontdesk.route('/getVisitorsData', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@csrf.exempt
def getVisitorsData():
    if request.method == 'GET':
        filter_type = request.values.get("filter_type")
        assembly = request.values.get("assembly")
        district = request.values.get("district")

        if filter_type == 'today':
            if assembly is not None and district is not None:
                query = "where date(v.createdttm)= TIMESTAMP 'today' and v.district='" + district + "' and v.assembly='" + assembly + "'"
            elif assembly is None and district is not None:
                query = "where date(v.createdttm)= TIMESTAMP 'today' and v.district='" + district + "'"
            else:
                query = "where date(v.createdttm)= TIMESTAMP 'today'"

        elif filter_type == 'yesterday':
            if assembly is not None and district is not None:
                query = "where date(v.createdttm)= TIMESTAMP 'yesterday' and v.district='" + district + "' and v.assembly='" + assembly + "'"
            elif assembly is None and district is not None:
                query = "where date(v.createdttm)= TIMESTAMP 'yesterday' and v.district='" + district + "'"
            else:

                query = "where date(v.createdttm)= TIMESTAMP 'yesterday'"
        elif filter_type == 'range':
            from_date = request.values.get('from_date')
            to_date = request.values.get('to_date')
            if from_date is None or from_date == '':
                return json.dumps({'errors': ['Parameter from_date Missing'], 'data': {'status': "0"}})
            if to_date is None or to_date == '':
                return json.dumps({'errors': ['Parameter to_date Missing'], 'data': {'status': "0"}})
            if assembly is not None and district is not None:
                query = "where date(v.createdttm) between '" + from_date + "' and '" + to_date + "' and v.district='" + district + "' and v.assembly='" + assembly + "'"
            elif assembly is None and district is not None:
                query = "where date(v.createdttm) between '" + from_date + "' and '" + to_date + "' and v.district='" + district + "'"
            else:
                query = "where date(v.createdttm) between '" + from_date + "' and '" + to_date + "'"
        else:
            if assembly is not None and district is not None:
                query = "where  v.district='" + district + "' and v.assembly='" + assembly + "'"
            elif assembly is None and district is not None:
                query = "where  v.district='" + district + "'"
            else:
                query = "where date(v.createdttm)= TIMESTAMP 'today'"
        try:
            print query
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select v.name,v.mobile,v.email,v.assembly,v.district,v.age,v.gender,v.category,v.file_url,o.office_name from visitors v join jsp_office_locations o on o.seq_id=v.visitor_location_id  " + query + "")
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'data': {'status': "1", "children": rows}})



        except (psycopg2.Error, Exception) as e:
            print str(e)
            return json.dumps({'errors': ['Something Went Wrong. Please try again later'], 'data': {'status': 0}})




@blueprint_frontdesk.route('/logoutFrontdesk')
@frontdesk_login_required
def logoutFrontdesk():
    logout_user()

    session.clear()

    return redirect(url_for('flogin'))

#################### Visitors Services ###########
@blueprint_frontdesk.route('/visitors', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@frontdesk_login_required
@csrf.exempt
def visitors():
    if request.method == 'GET':
        assembly_instance = Assembly_constituencies_details()
        districts = assembly_instance.get_districts_list()
        data = []
        for district in districts:
            data.append(str(list(district)[0]).rstrip())
        return render_template('visitors/visitors_form.html', data=data)
    else:
        name = request.form.get('name')
        email = request.form.get('email')
        mobile = request.form.get('mobile')
        gender = request.form.get('gender')
        age = request.form.get('age')
        category = request.form.get('category')
        assembly = request.form.get('assembly')
        district = request.form.get('district')
        remarks = request.form.get('remarks')
        voter_id = request.form.get('voter_id')

        membership_check = request.form.get('membership_check')

        errors = []
        if name is None or name == '':
            errors.append("parameter name missing")
        if mobile is None or mobile == '':
            errors.append("parameter mobile missing")
        if gender is None or gender == '':
            errors.append("parameter gender missing")
        if assembly is None or assembly == '':
            errors.append("parameter assembly missing")
        if district is None or district == '':
            errors.append("parameter district missing")
        if errors:
            flash(errors, 'error')
            return redirect(url_for('.visitors'))
        try:
            visitor_file = None

            if category in ["I", "R", "L"]:

                if request.files['file']:
                    visitor_file = request.files['file']
                    visitor_file = upload_image(visitor_file, 'visitorfiles')

            entered_by_id = current_user.id
            visitor_location_id = current_user.location_id
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "insert into visitors(name,email,mobile,gender,age,category,assembly,district,remarks,file_url,voter_id,entered_by_id,visitor_location_id)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning seq_id",
                (name, email, mobile, gender, age, category, assembly, district, remarks, visitor_file, voter_id,
                 entered_by_id, visitor_location_id))
            con.commit()
            form_id = cur.fetchone()
            cur.execute("select office_name from jsp_office_locations where seq_id=%s", (visitor_location_id,))
            location = cur.fetchone()
            phone_number = "91" + str(mobile)
            membership_status = ''
            cur.execute("select jsp_supporter_seq_id from janasena_missedcalls where phone=%s", (phone_number,))
            existing = cur.fetchone()
            if existing is None:

                if membership_check == 'true':

                    cur.execute(
                        "insert into janasena_missedcalls(phone,member_through)values(%s,%s) returning jsp_supporter_seq_id",
                        (phone_number, 'M'))
                    row = cur.fetchone()
                    con.commit()
                    membership_id = "JSP" + str(row['jsp_supporter_seq_id']).zfill(8)
                    membership_status = "new_member"
                else:
                    membership_status = "missed_Call"
            else:
                membership_status = "existing_member"

            con.close()
            office_location = "JanaSena Party Office"

            if location:
                office_location = location['office_name']
            if category != 'P':

                today = str(datetime.today().strftime('%d %B %Y (%A)'))
                if category == 'L':
                    msgToSend = "Dear " + name + ",  we have received your representation on " + today + " to " + office_location + ". We will review the same and get back to you soon."

                else:
                    # to disable sending membership messages

                    if membership_status == 'missed_Call':
                        msgToSend = "Thank you for visiting " + office_location + " on " + today + ".to become a janasena member please give a missed call to 9394022222 "
                    elif membership_status == 'new_member':
                        membership_link = "https://goo.gl/vwqBxR"
                        msgToSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                            membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here " + membership_link

                    else:

                        msgToSend = "Thank you for visiting " + office_location + " on " + today + "."
                numberToSend = "91" + str(mobile)
                if config.ENVIRONMENT_VARIABLE == 'local':
                    sendSMS(numberToSend, msgToSend)
                else:
                    sendSMS.delay(numberToSend, msgToSend)

            success_str = "Saved SuccessFully.Form ID Is:" + str(form_id['seq_id'])
            flash(success_str, 'success')
            return redirect(url_for('.visitors'))



        except (psycopg2.Error, Exception) as e:
            print str(e)
            con.close()
            flash(errors, 'error')
            return redirect(url_for('.visitors'))


@blueprint_frontdesk.route('/flogin', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def flogin():
    if request.method == 'POST':
        uname = request.form.get('username')
        password = request.form.get('password')
        utype = 'F'
        print uname, password, utype
        if checkUserAuth(uname, password, utype):
            if 'current' in session.keys():
                print session['current']
                return redirect(session['current'])
            session['next'] = url_for('visitors')
            session['current'] = url_for('flogin')
            session['flogged_in'] = True
            print "logged in"
            print current_user
            return redirect(url_for('.visitors'))
        else:
            flash('Invalid Details', 'error')
            return redirect(url_for('.flogin'))
    else:
        print "fuck off here"
        # print 'helloooo'
        if session.get('flogged_in') is not None:
            print session
            if session.get('flogged_in') == True:
                return redirect(url_for('.visitors'))

        return render_template("visitors/flogin.html")