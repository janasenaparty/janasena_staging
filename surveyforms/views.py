# -*- coding: utf-8 -*-
import json

import pandas as pd
import psycopg2

from flask import Blueprint, render_template, request
from psycopg2.extras import RealDictCursor

import config
from appholder import csrf
from constants import empty_check

#from celery_tasks.others import insert_new_mem

# from celery_tasks.others import insert_new_mem

from utilities.decorators import crossdomain
from utilities.others import generate_membershipid, insert_new_mem

survey_form = Blueprint('survey_form', __name__)
@survey_form.route("/influencers", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def election_agent():
    if request.method == 'GET':
        return render_template('surveyforms/policy.html')
    elif request.method == 'POST':
        print(request.form)
        parliament_id = request.form.get('parliament')
        assembly_id = request.form.get('assembly')
        mandal_id = request.form.get('mandal')
        name = request.form.get('name')
        occupation = request.form.get('occupation')
        party_inclination = request.form.get('party_inclination')
        influence = request.form.get('influence')
        community = request.form.get('community')
        ward_village = request.form.get('ward_village')
        phone = request.form.get('phone')
        remarks = request.form.get('remarks')
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                        insert into election_agent(parliament_id,assembly_id,mandal_id,name,occupation,party_inclination,
                        influence,community,ward_village,phone,remarks)
                        values(%s,%s,%s,%s,%s,%s,
                        %s,%s,%s,%s,%s)
                    """,(parliament_id,assembly_id,mandal_id,name,occupation,party_inclination,influence,community,
                         ward_village,phone,remarks))
            con.commit()




            return json.dumps({'status':1,'msg':'Success'})


@survey_form.route("/election_agent", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def influencers():
    if request.method == 'GET':
        return render_template('surveyforms/policy1.html')
    elif request.method == 'POST':
        print(request.form)
        name = request.form.get('name')
        phone = request.form.get('phone')
        district_id = request.form.get('district')
        assembly_id = request.form.get('assembly')
        mandal_id = request.form.get('mandal')
        ward_village = request.form.get('ward_village')
        address = request.form.get('address')
        polling_station = request.form.get('polling_station')
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                                insert into sf_influencers(name,phone,district_id,assembly_id,mandal_id,ward_village
                                ,address,polling_station)
                                values(%s,%s,%s,%s,%s,%s,%s,%s)
                            """, (name,phone,district_id,assembly_id,mandal_id,ward_village
                                ,address,polling_station))
            con.commit()

            return json.dumps({'status': 1, 'msg': 'Success'})

@survey_form.route("/call_survey_form", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def call_survey_form():
    if request.method == 'GET':
        return render_template('surveyforms/call_survey_form.html')
    elif request.method == 'POST':
        print(request.form)
        district_id = request.form.get('district_id')
        assembly_id = request.form.get('assembly_id')
        mandal_id = request.form.get('mandal_id')
        occupation = request.form.get('occupation')
        party_inclination = request.form.get('party_inclination')
        about_jsp_party = request.form.get('about_jsp_party')
        about_jsp_symbol = request.form.get('about_jsp_symbol')
        jsp_reaching_public = request.form.get('jsp_reaching_public')
        base_voting_village = request.form.get('base_voting_village')
        strong_party_village = request.form.get('strong_party_village')
        people_support_village = request.form.get('people_support_village')
        castes_mandal = request.form.get('castes_mandal')
        first_predominant_caste = request.form.get('first_predominant_caste')
        second_predominant_caste = request.form.get('second_predominant_caste')
        first_predominant_caste_support = request.form.get('first_predominant_caste_support')
        second_predominant_caste_support = request.form.get('second_predominant_caste_support')
        people_support_tdp_village = request.form.get('people_support_tdp_village')
        people_support_ysrcp_village = request.form.get('people_support_ysrcp_village')
        people_support_jsp_village = request.form.get('people_support_jsp_village')
        influencers_name = request.form.get('influencers_name')
        know_about_jsp_manifesto = request.form.get('know_about_jsp_manifesto')
        why_support_jsp = request.form.get('why_support_jsp')
        why_dont_support_jsp = request.form.get('why_dont_support_jsp')
        do_support_mla_candidate = request.form.get('do_support_mla_candidate')
        opinion_of_mla_candidate = request.form.get('opinion_of_mla_candidate')
        which_manifesto_people_like = request.form.get('which_manifesto_people_like')
        major_local_issues_village = request.form.get('major_local_issues_village')
        issues_pending_village = request.form.get('issues_pending_village')
        second_level_leadership_jsp_village = request.form.get('second_level_leadership_jsp_village')
        strength_jsp_cadre_village = request.form.get('strength_jsp_cadre_village')
        second_level_leadership_jsp_village_mandal = request.form.get('second_level_leadership_jsp_village_mandal')
        gaps_difference_leaders = request.form.get('gaps_difference_leaders')
        unsatisfied_jsp_leaders = request.form.get('unsatisfied_jsp_leaders')
        unsatisfied_person = request.form.get('unsatisfied_person')
        skills = request.form.get('skills')
        skills1 = request.form.get('skills1')
        skills2 = request.form.get('skills2')

        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                                insert into call_survey_form(district_id ,
	assembly_id ,
	mandal_id ,
	occupation ,
	party_inclination ,
	about_jsp_party ,
	about_jsp_symbol ,
	jsp_reaching_public ,
	base_voting_village ,
	strong_party_village ,
	people_support_village ,
	castes_mandal ,
	first_predominant_caste ,
	second_predominant_caste ,
	first_predominant_caste_support ,
	second_predominant_caste_support ,
	people_support_tdp_village ,
	people_support_ysrcp_village ,
	people_support_jsp_village ,
	influencers_name ,
	know_about_jsp_manifesto ,
	why_support_jsp ,
	why_dont_support_jsp ,
	do_support_mla_candidate ,
	opinion_of_mla_candidate ,
	which_manifesto_people_like ,
	major_local_issues_village ,
	issues_pending_village ,
	second_level_leadership_jsp_village ,
	strength_jsp_cadre_village ,
	second_level_leadership_jsp_village_mandal ,
	gaps_difference_leaders ,
	unsatisfied_jsp_leaders ,
	unsatisfied_person ,
	skills ,    
	skills1 ,
	skills2 
	)
                                values(%s,%s,%s,%s,%s,%s,
                                %s,%s,%s,%s,%s,%s,
                                %s,%s,%s,%s,%s,%s,
                                %s,%s,%s,%s,%s,%s,
                                %s,%s,%s,%s,%s,%s,
                                %s,%s,%s,%s,%s,%s,%s)
                            """, (district_id ,	assembly_id ,mandal_id ,
	occupation ,party_inclination ,	about_jsp_party ,
	about_jsp_symbol ,	jsp_reaching_public ,	base_voting_village ,
	strong_party_village ,	people_support_village ,	castes_mandal ,
	first_predominant_caste ,	second_predominant_caste ,	first_predominant_caste_support ,
	second_predominant_caste_support ,	people_support_tdp_village ,people_support_ysrcp_village ,
	people_support_jsp_village ,influencers_name ,know_about_jsp_manifesto ,
	why_support_jsp ,	why_dont_support_jsp ,	do_support_mla_candidate ,
	opinion_of_mla_candidate ,	which_manifesto_people_like ,	major_local_issues_village ,
	issues_pending_village ,	second_level_leadership_jsp_village ,	strength_jsp_cadre_village ,
	second_level_leadership_jsp_village_mandal ,	gaps_difference_leaders ,	unsatisfied_jsp_leaders ,
	unsatisfied_person ,	skills ,	skills1 ,
                                  skills2 ))
            con.commit()

            return json.dumps({'status': 1, 'msg': 'Success'})


@survey_form.route("/cadre_grievances", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def cadre_grievances():
    if request.method == 'GET':
        return render_template('surveyforms/policy3.html')
    elif request.method == 'POST':
        name = request.form.get('name')
        phone = request.form.get('phone')
        parliament_id = request.form.get('parliament_id')
        assembly_id = request.form.get('assembly_id')
        mandal_id = request.form.get('mandal_id')
        ward_village = request.form.get('ward_village')
        position_party = request.form.get('position_in_party')
        address = request.form.get('address')
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                insert into cadre_grievance(name,phone,parliament_id,assembly_id,mandal_id,ward_village,
                position_party,address)
                values(%s,%s,%s,%s,%s,%s,%s,%s)
            """,(name,phone,parliament_id,assembly_id,mandal_id,ward_village,position_party,address))
            con.commit()

            return json.dumps({'status': 1, 'msg': 'Success'})

        pass


@survey_form.route("/sst", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sst():
    if request.method == 'GET':
        return render_template('admin/dd-dash.html')

@survey_form.route("/sst2", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sst2():
    if request.method == 'GET':
        return render_template('admin/dd-dash2.html')

@survey_form.route("/e_v_caste_count", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def e_v_caste_count():
    constituency_id = request.json.get('constituency_id')
    polling_booth_id = request.json.get('polling_booth_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute(
                    """
                    select caste,count(*)
                    from v2_voters
                    where ac_no=%s and ps_no=%s
                    group by caste
                    """
            ,(constituency_id,polling_booth_id)
        )
        rows = cur.fetchall()
        print(rows)
        return json.dumps({'status': 1, 'msg': 'Data aquired', 'data': rows})

@survey_form.route("/g_c_c", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def g_c_c():
    constituency_id = request.json.get('constituency_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            select mandal_name_eng,count(*) as total,sum(case when gender='M' then 1 else 0 end) as male,
            sum(case when gender='F' then 1 else 0 end) as female,
            sum(case when caste_cat='OC' then 1 else 0 end) as oc,
            sum(case when caste_cat='BC' then 1 else 0 end) as bc,
            sum(case when caste_cat='SC' then 1 else 0 end) as sc,
            sum(case when caste_cat='ST' then 1 else 0 end) as st,
            sum(case when caste_cat not in ('OC','BC','SC','ST') then 1 else 0 end) as minorities,mandal_no
            from v2_voters
            where ac_no=%s
            group by mandal_no,mandal_name_eng
           
        """,(constituency_id,))
        rows = cur.fetchall()
        return json.dumps({'status':1,'msg':'Data aquired','data':rows})

@survey_form.route("/g_a_c", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def g_a_c():
    constituency_id = request.json.get('constituency_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                           select ps_location_eng,ps_no
                           from v2_voters
                           where ac_no=%s
                           group by ps_no,ps_location_eng

                       """, (constituency_id,))
        rows = cur.fetchall()
        return json.dumps({'status': 1, 'msg': 'Data aquired', 'data': rows})


@survey_form.route("/g_m_c", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def g_m_c():
    print(request.json)
    constituency_id = request.json.get('constituency_id')
    mandal_id = request.json.get('mandal_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                    select ps_location_eng,count(*) as total,sum(case when gender='M' then 1 else 0 end) as male,
                    sum(case when gender='F' then 1 else 0 end) as female,
                    sum(case when caste_cat='OC' then 1 else 0 end) as oc,
                    sum(case when caste_cat='BC' then 1 else 0 end) as bc,
                    sum(case when caste_cat='SC' then 1 else 0 end) as sc,
                    sum(case when caste_cat='ST' then 1 else 0 end) as st,
                    sum(case when caste_cat not in ('OC','BC','SC','ST') then 1 else 0 end) as minorities,ps_no
                    from v2_voters
                    where ac_no=%s and mandal_no=%s
                    group by ps_no,ps_location_eng

                """, (constituency_id,mandal_id,))
        rows = cur.fetchall()
        return json.dumps({'status': 1, 'msg': 'Data aquired', 'data': rows})


@survey_form.route("/g_v_d", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def g_v_d():
    print(request.json)
    constituency_id = request.json.get('constituency_id')
    polling_booth_id = request.json.get('polling_booth_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                    select voter_id,voter_name_eng,voter_rel_name_eng,gender,age,caste_cat,caste,m_no,m_no1,id
                    from v2_voters
                    where ac_no=%s and ps_no=%s
                    

                """, (constituency_id,polling_booth_id,))
        rows = cur.fetchall()
        return json.dumps({'status': 1, 'msg': 'Data aquired', 'data': rows})


@survey_form.route("/g_v", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def g_v():
    id_val = request.json.get('id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                        select voter_id,voter_name_eng,voter_rel_name_eng,gender,age,caste_cat,caste,m_no,occupation,party_inclination,
                        family_head,add_influencer,booth_agent
                        from v2_voters
                        where id=%s


                    """, (id_val, ))
        row = cur.fetchone()
        return json.dumps({'status': 1, 'msg': 'Data aquired', 'data': row})

    pass

@survey_form.route("/e_v", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def e_v():
    data = request.json
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            update v2_voters
            set voter_id=%s,voter_name_eng=%s,voter_rel_name_eng=%s,
            gender=%s,age=%s,caste_cat=%s,caste_group=%s,m_no=%s,
            occupation=%s,party_inclination=%s,add_influencer=%s,booth_agent=%s,family_head=%s
            where id=%s
        """,(data['voter_id'],data['voter_name'],data['voter_rel_name'],
             data['gender'],data['age'],data['caste_cat'],data['caste_group'],data['m_no'],
             data['occupation'],data['party_inclination'],data['influencer'],data['booth_agent'],data['family'],data['uid']
             ))
        con.commit()
        return json.dumps({'status':1,'msg':'Updated successfully'})


"""
create table banners_dyn(
    id serial primary key,
    url varchar(1024),
    classname varchar(200),
    thumbnail_url varchar(1024)
);
"""

@survey_form.route("/banners_dash", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def banners_dash():
    if request.method == 'GET':
        return render_template("admin/banners.html")
    elif request.method == 'POST':
        class_name = request.form.get('class_name')
        thumbnail_file = request.files.get('thumbnail_file')
        main_file = request.files.get('main_file')

@survey_form.route("/insert_new_mem", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def lmth():
    insert_new_mem.delay()
    return json.dumps({'status':1,'msg':'Creating'})






        