import json
import uuid
import psycopg2
from psycopg2.extras import RealDictCursor
from werkzeug.exceptions import abort

import config
from flask import Blueprint, render_template, send_from_directory, request, url_for
from werkzeug.utils import secure_filename, redirect

from appholder import table_service, block_blob_service, csrf
from utilities.decorators import crossdomain
from utilities.general import id_generator, decode_hashid, create_hashid
from utilities.mailers import send_thank_you_mail

blueprint_website = Blueprint('website', __name__)


@blueprint_website.route("/media", methods=['POST', 'GET'])
def media():
    return render_template('website/media.html')


@blueprint_website.route("/APSpecialStatusIssue", methods=['POST', 'GET'])
def APSpecialStatusIssue():
    return render_template('issues/apspecialstatus.html')


@blueprint_website.route("/UddanamKidneyIssue", methods=['POST', 'GET'])
def UddanamKidneyIssue():
    return render_template('issues/uddanam.html')


@blueprint_website.route("/BhimavaramAcquaFoodparkIssue", methods=['POST', 'GET'])
def BhimavaramAcquaFoodparkIssue():
    return render_template('issues/aquafoodpark.html')


@blueprint_website.route('/refunds_cancellations')
def refunds_cancellations():
    return render_template('website/refunds_cancellations.html')


@blueprint_website.route("/about", methods=['POST', 'GET'])
@blueprint_website.route("/about-janasena-party", methods=['POST', 'GET'])
def about():
    return render_template('website/about.html')


@blueprint_website.route('/resourcepersons', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
def resourcepersons():
    if request.method == "POST":

        try:
            name = request.values.get('name')
            applying_for = request.values.get('applying_for')
            age = request.values.get('age')
            gender = request.values.get('gender')
            email = request.values.get('email')
            phoneno = request.values.get('phone')
            Profession = request.values.get('Profession')
            qualification = request.values.get('qualification')
            address = request.values.get('address')
            district = request.values.get('district')
            assembly = request.values.get('assembly')
            ip_address = request.remote_addr
            data = {}
            data['Name'] = name
            data['applying_for'] = applying_for
            data['email'] = email.lower()
            data['profession'] = Profession
            data['age'] = age
            data['gender'] = gender
            data['mobile_num'] = phoneno
            data['qualification'] = qualification
            data['gender'] = gender
            data['address'] = address
            data['district'] = district
            data['assembly_constituency'] = assembly

            data['ipaddress'] = request.remote_addr

            data['PartitionKey'] = 'pk'
            data['RowKey'] = str(uuid.uuid4())
            try:
                tasks = table_service.query_entities('resourcepersons',
                                                     filter="mobile_num eq '" + str(phoneno) + "' or email eq '" + str(
                                                         email.lower()) + "'")
                for item in tasks:
                    return json.dumps({"errors": ['details already existed'], 'data': {'status': '0'}})
                else:

                    task = table_service.insert_entity('resourcepersons', data)
                    send_thank_you_mail(email)
            except Exception as e:
                print str(e)
                return json.dumps({"errors": ['Exception occured'], 'data': {'status': '0'}})

            return json.dumps({"errors": [], 'data': {'status': '1'}})
        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Something went Wrong'], 'data': {'status': '0'}})


@blueprint_website.route('/robots.txt')
@blueprint_website.route('/sitemap.xml')
@blueprint_website.route('/loaderio-17715b7e9105c6d568bafab563065ec4.txt')
@blueprint_website.route('/3ig5nqn9z3z6s3hkgbe87npwou0jc4.html')
def static_from_root():
    return send_from_directory(blueprint_website.static_folder, request.path[1:])


# @blueprint_website.route("/NRIConnect", methods=['POST', 'GET'])
# @blueprint_website.route("/nri-connect", methods=['POST', 'GET'])
# @blueprint_website.route("/nriconnect", methods=['POST', 'GET'])
# def NRIConnect():
#     if request.method == 'GET':
#         return render_template('website/NRIConnect.html')
#     elif request.method == 'POST':
#         try:
#             errors = []
#             phone = request.values.get('phone')
#             email = request.values.get("email")
#             name = request.values.get("name")
#             profession = request.values.get("profession")
#             city = request.values.get("city")
#             fb = request.values.get("fb")
#             twitter = request.values.get("twitter")
#             if phone is None or phone.isspace():
#                 errors.append('Phone Number should not be empty')
#             if email is None or email.isspace():
#                 errors.append('email should not be empty')
#             if name is None or name.isspace():
#                 errors.append('name should not be empty')
#             if city is None or city.isspace():
#                 errors.append('city should not be empty')
#             if profession is None or profession.isspace():
#                 errors.append('profession should not be empty')
#             if errors:
#                 return json.dumps({"errors": errors, 'data': {'status': '0'}})
#             data = {}
#             data['name'] = name
#             data['city'] = city
#             data['email'] = email.lower()
#             data['profession'] = profession
#             data['facebook_id'] = fb
#             data['twitter_handle'] = twitter
#             data['mobile_num'] = phone
#             data['ipaddress'] = request.remote_addr

#             data['PartitionKey'] = 'pk'
#             data['RowKey'] = str(uuid.uuid4())
#             try:
#                 tasks = table_service.query_entities('janasenavolunteer',
#                                                      filter="mobile_num eq '" + str(phone) + "' or email eq '" + str(
#                                                          email.lower()) + "'")
#                 for item in tasks:
#                     return json.dumps({"errors": ['details already existed'], 'data': {'status': '0'}})
#                 else:

#                     task = table_service.insert_entity('janasenavolunteer', data)
#                     send_thank_you_mail(email)
#             except Exception as e:
#                 print str(e)
#                 return json.dumps({"errors": ['Exception occured'], 'data': {'status': '0'}})

#             return json.dumps({"errors": [], 'data': {'status': '1'}})
#         except Exception as e:
#             print str(e)
#             return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})

@blueprint_website.route("/NRIConnect", methods=['POST', 'GET'])
@blueprint_website.route("/nri-connect", methods=['POST', 'GET'])
@blueprint_website.route("/nriconnect", methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")

def NRIConnect():
    if request.method == 'GET':
        return render_template('website/nri_form.html')
    elif request.method == 'POST':
        try:
            errors = []
            phone = request.values.get('phone')
            email = request.values.get("email")
            name = request.values.get("name")
            profession = request.values.get("profession")
            city = request.values.get("city")
            fb = request.values.get("fb")
            twitter = request.values.get("twitter")
            state=request.values.get("state")
            assembly = request.values.get("assembly_constituency")
            district = request.values.get("district")
            skills = request.values.get("skills")
            country = request.values.get("country")
            whats_app = request.values.get("whats_app")
            age = request.values.get("age")
            work_as_volunteer=request.values.get("work_as_volunteer")
            if phone is None or phone.isspace():
                errors.append('Phone Number should not be empty')
            if whats_app is None or whats_app.isspace():
                errors.append('whats_app Number should not be empty')
            if email is None or email.isspace():
                errors.append('email should not be empty')
            if name is None or name.isspace():
                errors.append('name should not be empty')
            if city is None or city.isspace():
                errors.append('city should not be empty')
            if profession is None or profession.isspace():
                errors.append('profession should not be empty')
            if assembly is None or assembly.isspace():
                errors.append('assembly constituency should not be empty')
            if district is None or district.isspace():
                errors.append('profession should not be empty')
            if work_as_volunteer is None or work_as_volunteer.isspace():
                errors.append('volunteer for janasena should not be empty')
            if age is None or age.isspace():
                errors.append('age should not be empty')

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            print errors
            cur.execute("select name from nri_volunteers where phone=%s or lower(email)=%s", (phone, email))
            existing = cur.fetchone()
            if existing is None:
                if work_as_volunteer=='yes':
                    ful_time_or_part_time=request.values.get("ful_time_or_part_time")
                    if work_as_volunteer is None or work_as_volunteer.isspace():
                        errors.append(' How do you want to volunteer for janasena option should not be empty')
                    if ful_time_or_part_time=='full':
                        work_in_india=request.values.get("work_in_india")
                        india_to_date=request.values.get("india_to_date")
                        india_from_date=request.values.get("india_from_date")
                        abroad_from_date=request.values.get("abroad_from_date")
                        abroad_to_date=request.values.get("abroad_to_date")
                        work_in_abroad=request.values.get("work_in_abroad")
                        part_time_hours=0
                        if work_in_india=='yes':
                            
                            if india_from_date is None or india_from_date=='':
                                errors.append(' From Date  option should not be empty')
                            if india_to_date is None or india_to_date=='':
                                errors.append(' To Date  option should not be empty')
                       
                        if work_in_abroad=='yes':
                            
                            if abroad_from_date is None or abroad_from_date=='':
                                errors.append(' From Date  option should not be empty')
                            if abroad_to_date is None or abroad_to_date=='':
                                errors.append(' To Date  option should not be empty')
                      
                    else:
                        india_to_date=''
                        india_from_date=''
                        work_in_abroad=''
                        work_in_india=''
                        abroad_from_date=request.values.get("abroad_from_date")
                        abroad_to_date=request.values.get("abroad_to_date")
                        if abroad_from_date is None or abroad_from_date=='':
                            errors.append(' From Date  option should not be empty')
                        if abroad_to_date is None or abroad_to_date=='':
                            errors.append(' To Date  option should not be empty')
                        part_time_hours=request.values.get("part_time_hours")
                        if part_time_hours is None or part_time_hours=='':
                            errors.append(' part time duration  option should not be empty') 
                    if errors:
                        print errors
                        return json.dumps({"errors": errors, 'status': '0'})
                    cur.execute(
                        """insert into nri_volunteers(name,phone,email,state,district,constituency,skills,
                            country,city,profession,twitter_id,facebook,work_as_volunteer,abroad_from_date,abroad_to_date,
                            part_time_hours,india_to_date,india_from_date,work_in_india,work_in_abroad,whatsapp,ful_time_or_part_time,age)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """,
                        (name, phone, str(email).lower(),state, district, assembly, skills, country, city, profession, twitter,
                         fb,work_as_volunteer,abroad_from_date,abroad_to_date,
                            part_time_hours,india_to_date,india_from_date,work_in_india,work_in_abroad,whats_app,ful_time_or_part_time,age))
                    con.commit()
                    con.close()

                    return json.dumps({"errors": [], "status": "1"})



                else:
                    
                    if errors:
                        print errors
                        return json.dumps({"errors": errors, 'status': '0'})

                
                    cur.execute(
                        "insert into nri_volunteers(name,phone,email,state,district,constituency,skills,country,city,profession,twitter_id,facebook,work_as_volunteer,whatsapp,age)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ",
                        (name, phone, str(email).lower(),state, district, assembly, skills, country, city, profession, twitter,
                         fb,work_as_volunteer,whats_app,age))
                    con.commit()
                    con.close()

                    return json.dumps({"errors": [], "status": "1"})

            return json.dumps({"errors": ["Mobile or Email already Existed. please register with different details"],
                               "status": "0"})

        except (psycopg2.Error, Exception) as e:
            print str(e)
            con.close()
            errors.append(str(e))

            return json.dumps({"errors": errors, "status": "0"})


@blueprint_website.route('/privacy')
def privacy():
    return render_template('website/privacy.html')


@blueprint_website.route('/success')
def success():
    return render_template('website/success.html')


@blueprint_website.route('/disclaimer')
def disclaimer():
    return render_template('website/disclaimer.html')


@blueprint_website.route("/contact", methods=['POST', 'GET'])
def contact():
    return render_template('website/contact.html')



@blueprint_website.route("/index", methods=['POST', 'GET'])
def index():
    return render_template('website/home.html')


@blueprint_website.route("/janaswaram", methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def janaswaram():
    if request.method == "GET":
        return render_template('website/janaswaram.html', errors=[])
    elif request.method == "POST":
        try:
            errors = []
            data = {}
            name = request.values.get("name")
            Profession = request.values.get("profession")
            email = request.values.get("email")
            phoneno = request.values.get("phone")
            policy = request.values.get("policy")
            suggestion = request.values.get("suggestion")
            ipaddress = request.remote_addr
           
            
            if request.files:
               
                container = 'janaswaram'

                file = request.files['userfile']
                filename = secure_filename(file.filename)
                fileextension = filename.rsplit('.', 1)[1]
                Randomfilename = id_generator()
                filename = Randomfilename + '.' + fileextension
                
                try:
                    block_blob_service.create_blob_from_stream(container, filename, file)
                except Exception as e:
                    print 'Exception=' + str(e)

                attachment = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename

            else:
               
                attachment = "NA"

            if name is None or name.isspace():
                errors.append('parameter Name should not be empty')
            if email is None or email.isspace():
                errors.append('parameter email should not be empty')
            if Profession is None or Profession.isspace():
                errors.append('parameter Profession should not be empty')
            if phoneno is None or phoneno.isspace():
                errors.append('parameter phone should not be empty')
            if policy is None or policy.isspace():
                errors.append('parameter Policy should not be empty')
            if suggestion is None or suggestion.isspace():
                errors.append('parameter Suggestion should not be empty')
            
            if len(errors):
                return  json.dumps({"errors":errors,"status":"0"})
            else:
               
                data['Name'] = name
                data['email'] = email
                data['Profession'] = Profession
                data['ipaddress'] = ipaddress
                data['Phone'] = phoneno
                data['Policy'] = policy
                data['Attachment'] = attachment
                data['Suggestion'] = suggestion
                data['PartitionKey'] = 'pk'
                data['RowKey'] = str(uuid.uuid4())
                try:
                    
                    task = table_service.insert_entity('janaswaram', data)
                    send_thank_you_mail(email)

                    
                    return json.dumps({"errors":[],"status":"1"})
                except Exception as e:
                    print str(e)
                    return json.dumps({"errors":["something wrong with your data .please try again"],"status":"0"})
        except Exception as e:
            print str(e)
            return json.dumps({"errors":["something wrong with your data .please try again"],"status":"0"})

# @blueprint_website.route("/m_active", methods=['POST', 'GET'])
# @csrf.exempt
# def m_active():
#     return render_template("website/jsp_mandal_team-n.html")
# @blueprint_website.route("/jsp_mandal_team", methods=['POST', 'GET'])
# @csrf.exempt
# def jsp_mandal_team():
#     if request.method == 'GET':
#         try:
#             hash = request.values.get('hash')
#             if hash is None:
#                 return redirect(url_for('.m_active'))
#             decoded = decode_hashid(hash)
#             con = psycopg2.connect(config.DB_CONNECTION)
#             with con.cursor(cursor_factory=RealDictCursor) as cur:
#                 cur.execute("""
#                 select * from
#                 active_members
#                 where seq_id=%s
#                 """,(decoded))
#                 amap = cur.fetchone()

#                 if amap is None:
#                     return render_template('website/jsp_mandal_team.html')

#                 data = dict()
#                 data['pc_id'] = amap['parliament_constituency_id']
#                 data['constituency_id'] = amap['constituency_id']
#                 data['mandal_id'] = amap['mandal_id']
#                 data['name'] = amap['name']
#                 data['age'] = amap['age']
#                 data['phone'] = amap['phone'][2:]
#                 data['hash'] = hash
                
#                 return render_template('website/jsp_mandal_team.html',data=data)
#         except (psycopg2.Error,Exception) as e:
#             print(e )
#             return redirect(url_for('.m_active'))


#     elif request.method == 'POST': 
#         try:
#             data = request.json
#             print(data)
#             if 'hash' not in data:
#                 return json.dumps({'status':0,"msg":"Please send data"})
#             unhash = decode_hashid(data['hash'])
#             if data is None:
#                 return json.dumps({'status':0,"msg":"Please send data"})
#             con = psycopg2.connect(config.DB_CONNECTION)
#             with con.cursor() as cur:

#                 print(unhash,"unhash",data['phone'])
#                 cur.execute("""
#                     select * 
#                     from active_members
#                     where phone=%s and seq_id=%s
#                 """,("91"+data['phone'],unhash[0]))
#                 not_user = cur.fetchone()
#                 print("notuser",not_user)
#                 if not_user is None:
#                     return json.dumps({'status':2,"msg":"You are not authorized. Redirecting"}) #redirect(url_for('.m_active'))
#                 cur.execute("""
#                     select * 
#                     from jsp_mandal_team
#                     where phone=%s
#                 """,(data['phone'],))
#                 phone_proxy = cur.fetchone()
#                 print(phone_proxy)
#                 if phone_proxy:
#                     return json.dumps({'status':0,'msg':'User with the phone number already exists.'})
#                 cur.execute("""
#                     insert into jsp_mandal_team(name,age,constituency_id,mandal_id,location,smart_phone,jsp_time,jsp_program,parliament_constituency_id,phone,whats_app)
#                     values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
#                 """,(data['name'],data['age'],data['assembly'],data['mandal'],data['location'],
#                      data['smart_phone'],data['jsp_time'],data['jsp_program'],data['parliament'],data['phone'],data['whats_app']))
#                 con.commit()

#                 return json.dumps({'status':1,"msg":"Inserted Successfully"})
#         except (psycopg2.Error,Exception) as e:
#             print(e)
#             return json.dumps({'status':0,'msg':'Something Wrong'})
