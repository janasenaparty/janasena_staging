import psycopg2
import urllib

from flask import Blueprint, request, json, render_template, session
from psycopg2.extras import RealDictCursor
from werkzeug.exceptions import abort

import config
from appholder import csrf, db,app
from dbmodels import Janasena_Missedcalls
from main import verifyOtp, delete_session, get_phonenumber, insert_temporary_details
from utilities.classes import MembershipSMS, Voter_details, Membership_details, Assembly_constituencies_details, \
    encrypt_decrypt, MembershipPendingDetails
from utilities.decorators import crossdomain, requires_auth, requires_authToken, admin_login_required
from utilities.general import updatevolunteerstatus, upload_image_web
from utilities.mailers import genOtp, genEmailOtp
from utilities.others import generate_membershipid,generate_membershipid_whatsappbot, makeResponse
from celery_tasks.sms_email import sendSMS

blueprint_membership = Blueprint('membership', __name__)


@blueprint_membership.route('/getMembershipWithVolunteerSMS', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@requires_auth
def getMembershipWithVolunteerSMS():
    volunteer_mobile = request.values.get('mobilenumber')
    message_body = request.values.get('message')

    if volunteer_mobile is None or volunteer_mobile == "":
        return json.dumps({'errors': ['volunteer_mobile_num missing'], 'data': {'status': '0'}})
    if message_body is None or message_body == "":
        return json.dumps({'errors': ['message_body missing'], 'data': {'status': '0'}})

    try:
        if len(volunteer_mobile) == 12:
            volunteer_mobile = volunteer_mobile[2:12]

        elif len(volunteer_mobile) == 10:
            pass
        else:
            return json.dumps({"errors": ['invalid Volunteer mobilenumer'], "data": {"status": "0"}})
        try:
            message_body = urllib.unquote(message_body).decode('utf8')
            message_body = ' '.join(message_body.split())
            message_parts = str(message_body).split(" ")
            new_mobile = message_parts[0]
            second_part = str(message_parts[1]).strip()
        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['invalid message'], "data": {"status": "0"}})

        if len(second_part) == 6:

            otp = second_part
            new_member_phone = new_mobile
            if otp is None or otp.isspace():
                return json.dumps({"errors": ["invalid otp given"], "data": {"status": "0"}})
            if new_member_phone is None or new_member_phone.isspace():
                return json.dumps({"errors": ["invalid new_member_phone Number"], "data": {"status": "0"}})
            if verifyOtp(new_member_phone, otp):
                membershipsms_instance = MembershipSMS()

                details = membershipsms_instance.get_details(volunteer_mobile, new_member_phone)
                if details is not None:
                    voter_id = details['voter_id']

                    children = {}

                    voterdetails_instance = Voter_details()
                    voter_id = str(voter_id).lower()
                    record = voterdetails_instance.get_voter_details(voter_id)

                    if record is not None:
                        voter_id = str(record['voter_id']).rstrip()
                        name = str(record['voter_name']).rstrip()
                        membership_instance = Membership_details()
                        referral_id = membership_instance.get_volunteer_membership_id(volunteer_mobile)

                        phone = new_member_phone
                        if record['state'] == 1:
                            state = 'AP'
                        else:
                            state = 'TS'

                        status, membership_id = generate_membershipid(name, phone, voter_id, record['relation_name'],
                                                                      record['relation_type'], record['house_no'], '',
                                                                      record['age'], record['sex'], record['part_no'],
                                                                      record['ac_no'], state, '',
                                                                      record['section_name'], referral_id, 'S')
                        if status:
                            return json.dumps({"errors": [],
                                               'data': {'status': '1', 'membership_id': membership_id, 'name': name,
                                                        'phone': phone}})

                        return json.dumps(
                            {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})

                        return json.dumps({"errors": [], 'data': {'status': '1'}})
                    return json.dumps({"errors": ['no record found with the given details'],
                                       'data': {'status': '0', 'children': children}})
            else:
                return json.dumps({"errors": ['invalid otp'], 'data': {'status': '0'}})


        elif len(second_part) >= 10:

            voter_id = second_part
            membership_instance = Membership_details()
            if membership_instance.check_volunteer_status(None, volunteer_mobile):
                voter_id = str(voter_id).lower()
                if membership_instance.check_voterid_existence(voter_id):

                    if membership_instance.check_phonenumber_count(new_mobile):

                        voterdetails_instance = Voter_details()
                        if voterdetails_instance.check_existence(voter_id):

                            membershipsms_instance = MembershipSMS()

                            membershipsms_instance.add_details(volunteer_mobile, new_mobile, voter_id)

                            if genOtp(new_mobile, "MB"):
                                return json.dumps(
                                    {'errors': [], 'data': {'status': '1', 'msg': 'otp sent  Successfully'}})

                            return json.dumps(
                                {'errors': ['sorry, You already reached OTP Limit. please try after some time.'],
                                 'data': {'status': '0'}})

                        return json.dumps(
                            {'errors': ['we could not found the details with this voter id'], 'data': {'status': '3'}})
                    return json.dumps(
                        {'errors': ['limit with this Phone Number alredy reached'], 'data': {'status': '3'}})

                return json.dumps({'errors': ['This Voter Id already existed'], 'data': {'status': '2'}})
            return json.dumps({'errors': ['You are not a volunteer'], 'data': {'status': '2'}})
        else:
            return json.dumps({'errors': ['Invalid Message'], 'data': {'status': '2'}})

    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Exception occured'], 'data': {'status': '0'}})


@blueprint_membership.route("/membershipWithNoVoterIDint", methods=['GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def membershipWithNoVoterIDinternational():
    data = []
    try:
        # delete_session()
        assembly_instance = Assembly_constituencies_details()
        districts = assembly_instance.get_districts_list()

        for district in districts:
            data.append(str(list(district)[0]).rstrip())

    except Exception as e:
        print str(e)
    return render_template("membership/membershipwithnovoteridinternational.html", data=data)


@blueprint_membership.route("/membershipWithNoVoterID", methods=['GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def membershipWithNoVoterID():
    return render_template("membership/membershipwithnovoterid.html")


@blueprint_membership.route("/getManualMembershipCard", methods=['GET'])
def getManualMembershipCard():
    if 'membership_id' not in session.keys():
        abort(401)
    membership_id = session['membership_id']
    if membership_id is None or membership_id == "":
        abort(401)
    membership_instance = Membership_details()
    details = membership_instance.get_membership_details(membership_id, None)
    d = {}
    if details is not None:

        name = details['name']
        phone = details['phone']
        membership_id = details['membership_id']
        status = details['membership_through']
        img_url = details['img_url']

        return render_template("membership/membership_card.html", membership_id=membership_id, name=name, phone=phone,
                               status=status, img_url=img_url)
    else:
        abort(401)

from utilities.general import decode_hashid
@app.route("/card/<hashid>", methods=['GET'])

@crossdomain(origin="*", headers="Content-Type")
def Card(hashid):

    
    if hashid is None or hashid == "":
        abort(401)
    seq_id=decode_hashid(hashid)
    membership_instance = Membership_details()
    details = membership_instance.get_membership_details_seq_id(seq_id)
    d = {}
    if details is not None:
        try:
            name = details['name']
            phone = details['phone']
            membership_id = details['membership_id']
            status = details['membership_through']
            img_url = details['img_url']
            language = details['language']

            if language is None:
                language = 'Telugu'
            if img_url is None or img_url == 'https://janasenaparty.org/static/img/default-avatar.png':
                img_url = '/static/img/default-avatar.png'
            else:
                # once we recover we can delete this check
                img_url_split = str(img_url).split(".")[0]
                if img_url_split.lower() == "https://janasenalive":
                    img_url = '/static/img/default-avatar.png'

            return render_template("membership/membership_card.html", membership_id=membership_id, name=name,
                                   phone=phone, status=status, img_url=img_url, language=language)
        except Exception as e:
            print str(e)
            abort(401)
    else:
        abort(401)
@blueprint_membership.route('/verifyMemberDetailsint', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verifyMemberDetailsint():
    try:
        umobile = request.json['mobile_num']
        voter_id = request.json['voter_id']

        email = request.json['email']
        country_name = request.json['country_name']
        print umobile, voter_id, email
        if umobile is None or umobile == "":
            return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
        if voter_id is None or voter_id == "":
            return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})
        if email is None or email == "":
            return json.dumps({'errors': ['email missing'], 'data': {'status': '0'}})

        try:
            email = str(email).lower()
            voter_id = str(voter_id).lower()
            evoter_id = encrypt_decrypt(voter_id, 'E')
            emobile = encrypt_decrypt(umobile, 'E')
            e_email = encrypt_decrypt(email, 'E')
            membership_instance = Membership_details()
            if membership_instance.check_voterid_existence(evoter_id):

                if membership_instance.check_phone_or_email(emobile, e_email):
                    voterdetails_instance = Voter_details()
                    if voterdetails_instance.check_existence(voter_id):
                        PendingDetails = MembershipPendingDetails()
                        PendingDetails.save_mobile(umobile)

                        if genEmailOtp(umobile, email, "MB"):
                            delete_session()
                            session['umobile'] = umobile
                            session['voter_id'] = str(voter_id).lower()
                            session['evoter_id'] = evoter_id
                            session['emobile'] = emobile
                            session['email'] = email
                            session['country_name'] = country_name
                            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'otp sent  Successfully'}})

                        return json.dumps({'errors': ['something wrong in generating OTP'], 'data': {'status': '0'}})

                    return json.dumps({'errors': ['we could not found the given voter id'], 'data': {'status': '2'}})
                return json.dumps({'errors': ['Phone or Email already Existed'], 'data': {'status': '0'}})
            membership_instance = Membership_details()

            member_details = membership_instance.get_membership_details(None, evoter_id)
            membership_id = member_details['membership_id']
            message = "You are already registered as a member with us.\n Your membership id is " + str(membership_id)
            return json.dumps({'errors': [message], 'data': {'status': '3', "membership_id": membership_id}})

        except psycopg2.Error as e:
            print str(e)
            return json.dumps({'errors': ['something Wrong. please try again'], 'data': {'status': '0'}})
    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Invalid Details. please try again'], 'data': {'status': '0'}})

    ## getting voter details


@blueprint_membership.route("/saveMemberProfileImage", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def saveMemberProfileImage():
    try:

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:

            image_file = request.files['image']

            img_url = upload_image_web(image_file, 'membershipimages')
            # img_url=""
            membership_id = session["membership_id"]

            session['img_url'] = img_url
            cur.execute("update janasena_membership set image_url=%s where membership_id=%s", (img_url, membership_id))
            con.commit()
            con.close()
            return json.dumps({"errors": [], "data": {"status": "1"}})
        except (KeyError, Exception) as e:

            print str(e)

            return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})

    except psycopg2.Exception as e:
        print str(e)

        return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})


@blueprint_membership.route("/saveMemberPhoneNumber", methods=['POST'])
def saveMemberPhoneNumber():
    mobile_num = request.values.get('mobile_num')
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        membership_instance = Membership_details()
        if membership_instance.check_phonenumber_count(mobile_num):
            PendingDetails = MembershipPendingDetails()
            PendingDetails.save_mobile(mobile_num)

            return json.dumps({"errors": [], "data": {"status": '1'}})
        return json.dumps({"errors": ["5 people already registered with this Number "], "data": {"status": '0'}})

    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


@blueprint_membership.route("/saveMemberAssociation", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def saveMemberAssociation():
    try:

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:

            associations = request.values.get('associations')
            member_email = request.values.get('email')
            membership_id = session["membership_id"]
            language = request.values.get('language')
            if language is None:
                language = 'Telugu'
            session['language'] = language
            if member_email is not None and member_email != '':
                status = updatevolunteerstatus(member_email, membership_id)
            cur.execute("update janasena_membership set member_associations=%s,language=%s where membership_id=%s",
                        (associations, language, membership_id))
            con.commit()
            con.close()
            return json.dumps({"errors": [], "data": {"status": "1"}})

        except (KeyError, Exception) as e:

            print str(e)
            con.close()
            return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})


    except psycopg2.Exception as e:
        print str(e)
        con.close()
        return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})


# @blueprint_membership.route("/getMembershipDetails", methods=["GET", "POST"])
# @crossdomain(origin="*", headers="Content-Type")
# def getMembershipDetails():
#     membership_id = request.values.get('membership_id')
#     if membership_id is None:
#         return json.dumps({"errors": ['membership_id is missing'], "data": {"status": '0'}})
#     membership_instance = Membership_details()
#     details = membership_instance.get_membership_details(membership_id, None)
#     d = {}
#     if details is not None:

#         d['name'] = details['name']
#         d['phone'] = details['phone']
#         d['membership_id'] = details['membership_id']
#         return json.dumps({"errors": [], "data": {"status": '1', 'children': d}})
#     else:
#         return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@blueprint_membership.route("/jspMissCallCampaign", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
#@requires_auth
def jspMissCallCampaign():
    phone_number = request.values.get('mobilenumber')
    receivedon = request.values.get('receivedon')
    try:

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == phone_number).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=phone_number,
                                             member_through='MI')

            db.session.add(newRecord)
            db.session.commit()
            return json.dumps({"errors": [], 'data': {'status': '1',"message":"inserted"}})

            # countries_allowed = ["91"]
            # if phone_number.startswith(tuple(countries_allowed)):
            #     member_id = newRecord.jsp_supporter_seq_id

                #membership_id = "JSP" + str(member_id).zfill(8)

                # msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                #     membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here https://goo.gl/vwqBxR"

                #sendSMS.delay(phone_number, msgtoSend)

        return json.dumps({"errors": [], 'data': {'status': '1',"message":"already existed"}})

    except Exception as e:
        print str(e)

        return json.dumps({"errors": ['Exception'], 'data': {'status': '0'}})


## verify voter id already existed or not
@blueprint_membership.route('/verifyMemberDetails', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verifyMemberDetails():
    try:
        umobile = request.json['mobile_num']
        voter_id = request.json['voter_id']
        mtype = request.json['mtype']

        if umobile is None or umobile == "":
            return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
        if voter_id is None or voter_id == "":
            return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})

        if mtype is None:
            mtype = 'p'

        try:
            if mtype == 'm':
                status, mobile = get_phonenumber(umobile)
                if status:
                    umobile = mobile

                else:
                    return json.dumps({'errors': ["No member found with given MembershipID"], 'data': {'status': '4'}})

            membership_instance = Membership_details()
            voter_id = str(voter_id).lower()
            evoter_id = encrypt_decrypt(voter_id, 'E')
            emobile = encrypt_decrypt(umobile, 'E')

            if membership_instance.check_voterid_existence(evoter_id):

                if membership_instance.check_phonenumber_count(emobile):
                    voterdetails_instance = Voter_details()
                    if voterdetails_instance.check_existence(voter_id):
                        pending_details = MembershipPendingDetails()
                        pending_details.save_mobile(umobile)

                        if genOtp(umobile, "MB"):
                            delete_session()
                            session['umobile'] = umobile
                            session['voter_id'] = str(voter_id).lower()
                            session['evoter_id'] = evoter_id
                            session['emobile'] = emobile

                            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'otp sent  Successfully'}})
                        return json.dumps({'errors': ['something wrong in generating OTP'], 'data': {'status': '0'}})
                        

                    return json.dumps({'errors': ['we  could not found the given voter id'], 'data': {'status': '2'}})
                return json.dumps({'errors': ['limit with this Phone Number already reached'], 'data': {'status': '0'}})
            membership_instance = Membership_details()

            member_details = membership_instance.get_membership_details(None, evoter_id)
            membership_id = member_details['membership_id']
            message = "You are already registered as a member with us.\n Your membership id is " + str(membership_id)
            return json.dumps({'errors': [message], 'data': {'status': '3', "membership_id": membership_id}})

        except psycopg2.Error as e:
            print str(e)
            return json.dumps({'errors': ['something Wrong. please try again'], 'data': {'status': '0'}})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors': ['parameters missing. please try again'], 'data': {'status': '0'}})


@blueprint_membership.route("/manualMembershipint", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def manualMembershipint():
    if request.method == 'GET':
        data = []
        try:
            delete_session()
            assembly_instance = Assembly_constituencies_details()
            districts = assembly_instance.get_districts_list()

            for district in districts:
                data.append(str(list(district)[0]).rstrip())

        except Exception as e:
            print str(e)

        return render_template('membership/manualmembershipinternational.html', data=data)
    if request.method == 'POST':
        try:
            mtype = request.values.get("mtype")
            phone = request.values.get("phone")

            if mtype == 'm':
                status, mobile = get_phonenumber(phone)
                if status:
                    phone = mobile
                else:
                    return json.dumps({'errors': [' No Member Found With Given MembershipID'], 'data': {'status': '4'}})

            session['manual_details'] = {}
            d = {}
            d['phone'] = phone
            d['voter_id'] = request.values.get("voter_id")
            d['name'] = request.values.get("name")
            d['email'] = request.values.get("email")
            d['age'] = request.values.get("age")
            d['gender'] = request.values.get("gender")
            d['district'] = request.values.get("district")
            d['constituency'] = request.values.get("constituency")
            d['email'] = str(d['email']).lower()
            d['referral_id'] = ''
            d['country_name'] = request.values.get("country_name")
            d['state_id'] = request.values.get("state_id")
            session['manual_details'] = d

            session['umobile'] = d['phone']

            membership_instance = Membership_details()
            evoter_id = encrypt_decrypt(str(d['voter_id']).lower(), "E")
            emobile = encrypt_decrypt(str(d['phone']), "E")
            e_email = encrypt_decrypt(str(d['email']), "E")
            session['evoter_id'] = evoter_id
            session['emobile'] = emobile
            session['email'] = d['email']
            if d['voter_id'] is not None and d['voter_id'] != "":

                if membership_instance.check_voterid_existence(evoter_id):

                    if membership_instance.check_phone_or_email(emobile, e_email):

                        PendingDetails = MembershipPendingDetails()
                        PendingDetails.save_mobile(d['phone'])
                        if genEmailOtp(d['phone'], d['email'], "MB"):
                            return json.dumps({"errors": [], "data": {"status": '1'}})
                        return json.dumps(
                            {"errors": ['sorry, You already reached OTP Limit. please try after some time.'],
                             "data": {"status": '1'}})

                    return json.dumps({'errors': ['Phone Number or email already Existed'], 'data': {'status': '0'}})

                return json.dumps({'errors': ['This Voter Id already existed'], 'data': {'status': '2'}})
            if membership_instance.check_phone_or_email(emobile, e_email):

                PendingDetails = MembershipPendingDetails()
                PendingDetails.save_mobile(d['phone'])
                if genEmailOtp(d['phone'], d['email'], "MB"):
                    return json.dumps({"errors": [], "data": {"status": '1'}})
                return json.dumps({"errors": ['sorry, You already reached OTP Limit. please try after some time.'],
                                   "data": {"status": '1'}})
            return json.dumps({'errors': ['Phone Number or Email ID already Existed'], 'data': {'status': '0'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something Wrong"], "data": {"status": '0'}})


@blueprint_membership.route("/manualMembershipAPP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def manualMembershipAPP():
    if request.method == 'POST':
        try:

            try:
                d = {}
                d['phone'] = request.json["phone"]
                d['voter_id'] = request.json["voter_id"]
                d['name'] = request.json["name"]
                d['email'] = request.json["email"]
                d['age'] = request.json["age"]
                d['gender'] = request.json["gender"]
                d['district'] = request.json["district"]
                d['constituency'] = request.json["constituency"]
                d['referral_id'] = request.json["referral_id"]
                mtype = request.json["mtype"]
                if mtype is None:
                    mtype = 'p'

                if mtype == 'm':
                    status, mobile = get_phonenumber(d['phone'])
                    if status:
                        d['phone'] = mobile

            except (KeyError, Exception) as e:
                print str(e)
                return json.dumps({'errors': ["Parameter " + str(e) + " Missing"], 'data': {'status': '0'}})

            membership_instance = Membership_details()
            if d['voter_id'] is not None and d['voter_id'] != "":
                voter_id = str(d['voter_id']).lower()
                evoter_id = encrypt_decrypt(voter_id, 'E')

                ephone = encrypt_decrypt(d['phone'], 'E')
                if membership_instance.check_voterid_existence(evoter_id):

                    if membership_instance.check_manual_phonenumber_count(ephone):

                        PendingDetails = MembershipPendingDetails()
                        PendingDetails.save_mobile(d['phone'])

                        try:
                            status = insert_temporary_details(d)
                            if status:
                                if genOtp(d['phone'], "MB"):
                                    return json.dumps({"errors": [], "data": {"status": '1'}})
                                return json.dumps(
                                    {"errors": ['sorry, You already reached OTP Limit. please try after some time.'],
                                     "data": {"status": '1'}})
                            return json.dumps({"errors": ["something Went Wrong please enter correct details"],
                                               "data": {"status": '0'}})
                        except Exception as e:
                            print str(e)
                            return json.dumps({"errors": ["something Wrong"], "data": {"status": '0'}})

                    return json.dumps({'errors': [' Phone Number already Existed'], 'data': {'status': '0'}})

                return json.dumps({'errors': ['This Voter Id already existed'], 'data': {'status': '2'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something Wrong"], "data": {"status": '0'}})


@blueprint_membership.route("/jspChatbotCampaign", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def jspChatbotCampaign():
    phone_number = request.values.get('mobilenumber')
    if phone_number is None or phone_number == '':
        if phone_number is None or phone_number.isspace():
            return makeResponse(json.dumps({"errors": ["phone_number should not be empty'"]}), 400,
                                'application/json')
    try:

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == phone_number).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=phone_number,
                                             member_through='C')
            db.session.add(newRecord)
            db.session.commit()

            countries_allowed = ["91"]
            if phone_number.startswith(tuple(countries_allowed)):

                member_id = newRecord.jsp_supporter_seq_id

                membership_id = "JSP" + str(member_id).zfill(8)

                msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                    membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here https://goo.gl/vwqBxR"

                sendSMS(phone_number, msgtoSend)
                return makeResponse(json.dumps({"messages": [{"text": msgtoSend}]}), 200,
                                    'application/json')
            else:
                member_id = newRecord.jsp_supporter_seq_id

                membership_id = "JSP" + str(member_id).zfill(8)

                msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                    membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here https://goo.gl/NY8bQg"
                return makeResponse(json.dumps({"messages": [{"text": msgtoSend}]}), 200,
                                    'application/json')

        return makeResponse(json.dumps({"messages": [{"text": "phone number already existed"}]}), 200,
                            'application/json')

    except Exception as e:
        print str(e)

        return makeResponse(json.dumps({"errors": ["Something wrong.please try again "]}), 400,
                            'application/json')


@blueprint_membership.route("/validateManualOTP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def validateManualOTP():
    try:

        otp = request.values.get("otp")
        if otp is None or otp == "":
            return json.dumps({"errors": ['invalid otp'], 'data': {'status': '0'}})
        if 'manual_details' not in session.keys():
            return json.dumps({"errors": ['Your Session is Expired. Please try again'], 'data': {'status': '0'}})
        phone = session['manual_details']['phone']
        if 'country_name' in session['manual_details'].keys():
            country_name = session['manual_details']['country_name']
        else:
            country_name = 'india'
        if verifyOtp(phone, otp, "MB"):

            manual_details = session['manual_details']
            voter_id = manual_details['voter_id']
            phone = manual_details['phone']
            emobile = encrypt_decrypt(phone, 'E')
            evoter_id = encrypt_decrypt(str(voter_id).lower(), 'E')
            state_id = manual_details['state_id']
            membership_instance = Membership_details()
            if voter_id is not None and voter_id != '':
                voter_id = str(voter_id).lower()
                if membership_instance.check_voterid_existence(evoter_id):
                    if membership_instance.check_manual_phonenumber_count(emobile):
                        if str(state_id) == '1':
                            state = "AP"
                        elif state_id == '2':
                            state = 'TS'
                        else:
                            state = 'KA'
                        constituency = manual_details["constituency"]

                        status, membership_id = generate_membershipid(manual_details['name'], phone, voter_id, '', '',
                                                                      '', '', manual_details['age'],
                                                                      manual_details['gender'], '0', constituency,
                                                                      state, manual_details['email'], '',
                                                                      manual_details['referral_id'], 'ME',
                                                                      country_name=country_name)

                        if status:
                            ##
                            session['membership_id'] = membership_id
                            session['card_details'] = {}
                            session['card_details']['name'] = manual_details['name']
                            session['card_details']['phone'] = phone
                            session['card_details']['email'] = manual_details['email']
                            session['card_details']['age'] = manual_details['age']
                            session['card_details']['gender'] = manual_details['gender']
                            session['card_details']['volunteer_status'] = 'F'
                            session.pop('manual_details', None)
                            session.pop('umobile', None)

                            return json.dumps({"errors": [], 'data': {"status": '1'}})
                        return json.dumps(
                            {"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})

                    return json.dumps({"errors": ['phone number already  Existed'], 'data': {'status': '0'}})

                return json.dumps({"errors": ['voter_id already  Existed'], 'data': {'status': '0'}})

            else:
                if membership_instance.check_manual_phonenumber_count(emobile):

                    if str(state_id) == '1':
                        state = "AP"
                    elif state_id == '2':
                        state = 'TS'
                    else:
                        state = 'KA'
                    constituency = manual_details["constituency"]

                    status, membership_id = generate_membershipid(manual_details['name'], phone, '', '', '', '', '',
                                                                  manual_details['age'], manual_details['gender'], '0',
                                                                  constituency, state, manual_details['email'], '',
                                                                  str(manual_details['referral_id']).upper(), 'S',
                                                                  country_name=country_name)
                    if status:
                        ##
                        session['membership_id'] = membership_id
                        session['card_details'] = {}
                        session['card_details']['name'] = manual_details['name']
                        session['card_details']['phone'] = phone
                        session['card_details']['email'] = manual_details['email']
                        session['card_details']['age'] = manual_details['age']
                        session['card_details']['gender'] = manual_details['gender']
                        session['card_details']['volunteer_status'] = 'F'
                        session.pop('manual_details', None)
                        session.pop('umobile', None)

                        session.pop('manual_details', None)
                        session.pop('umobile', None)

                        return json.dumps({"errors": [], 'data': {"status": '1'}})

                    return json.dumps(
                        {"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})
                return json.dumps({"errors": ['phone number already existed'], 'data': {'status': '0'}})


        else:
            return json.dumps({"errors": ["Incorrect OTP"], "data": {"status": "0"}})

    except Exception as e:
        print str(e)

        return json.dumps({"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})


# @blueprint_membership.route('/deleteMembership', methods=['DELETE'])
# @crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# def deleteMembership():
#     membership_id = request.values.get('membership_id')
#     print membership_id
#     if membership_id is None or membership_id == '':
#         return json.dumps({'errors': ['parameter membership_id missing'], 'data': {'status': '0'}})
#     try:
#         con = None
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor()
#         cur.execute('delete from janasena_membership where membership_id=%s', (membership_id,))
#         con.commit()
#         con.close()
#         return json.dumps({'errors': [], 'data': {'status': '1'}})
#     except psycopg2.Error as e:
#         print str(e)
#         con.close()
#         return json.dumps({'errors': ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_membership.route("/manualMembership", methods=['POST', 'GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def manualMembership1():
    if request.method == 'GET':
        return render_template('membership/manualmembership.html')
    if request.method == 'POST':
        try:
            mtype = request.values.get("mtype")
            phone = request.values.get("phone")

            if mtype == 'm':
                status, mobile = get_phonenumber(phone)
                if status:
                    phone = mobile
                else:
                    return json.dumps({'errors': [' No Member Found With Given MembershipID'], 'data': {'status': '4'}})

            session['manual_details'] = {}
            d = {}
            d['phone'] = phone
            d['voter_id'] = request.values.get("voter_id")
            d['name'] = request.values.get("name")
            d['email'] = request.values.get("email")
            d['age'] = request.values.get("age")
            d['gender'] = request.values.get("gender")
            d['district'] = request.values.get("district")
            d['constituency'] = request.values.get("constituency")
            d['state_id'] = request.values.get("state_id")
            d['referral_id'] = ''

            session['manual_details'] = d

            session['umobile'] = d['phone']

            membership_instance = Membership_details()
            evoter_id = encrypt_decrypt(str(d['voter_id']).lower(), "E")
            emobile = encrypt_decrypt(str(d['phone']), "E")
            session['evoter_id'] = evoter_id
            session['emobile'] = emobile

            if d['voter_id'] is not None and d['voter_id'] != "":

                if membership_instance.check_voterid_existence(evoter_id):

                    if membership_instance.check_manual_phonenumber_count(emobile):

                        PendingDetails = MembershipPendingDetails()
                        PendingDetails.save_mobile(d['phone'])
                        if genOtp(d['phone'], "MB"):
                            return json.dumps({"errors": [], "data": {"status": '1'}})
                        return json.dumps(
                            {"errors": ['sorry, You already reached OTP Limit. please try after some time.'],
                             "data": {"status": '1'}})

                    return json.dumps({'errors': [' Phone Number already Existed'], 'data': {'status': '0'}})

                return json.dumps({'errors': ['This Voter Id already existed'], 'data': {'status': '2'}})
            if membership_instance.check_manual_phonenumber_count(emobile):

                PendingDetails = MembershipPendingDetails()
                PendingDetails.save_mobile(d['phone'])
                if genOtp(d['phone'], "MB"):
                    return json.dumps({"errors": [], "data": {"status": '1'}})
                return json.dumps({"errors": ['sorry, You already reached OTP Limit. please try after some time.'],
                                   "data": {"status": '1'}})
            return json.dumps({'errors': ['Phone Number already Existed'], 'data': {'status': '0'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": ["something Wrong"], "data": {"status": '0'}})


@blueprint_membership.route("/getQRCodeDetails", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getQRCodeDetails():
    membership_id = request.values.get('id')
    if membership_id is None or membership_id == "":
        return render_template('404.html'), 404
    membership_instance = Membership_details()
    details = membership_instance.get_membership_details(membership_id, None)
    d = {}
    if details is not None:
        try:
            name = details['name']

            membership_id = details['membership_id']

            img_url = details['img_url']

            return render_template("membership/member_details.html", membership_id=membership_id, name=name,
                                   img_url=img_url)
        except Exception as e:
            print str(e)
            abort(401)
    else:
        return render_template('404.html'), 404


@blueprint_membership.route("/voicecallstatus", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def voicecallstatus():
    try:

        response = request.json['smscresponse']
        phone = response['to']
        status = response['callstatus']
        calluid = response['calluid']
        direction = response['direction']
        if phone is None:
            return json.dumps({"errors": ['response missing'], 'data': {'status': '0'}})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("insert into  janasena_voicecalls(phone,status,calluid,direction)values(%s,%s,%s,%s)",
                        (phone, status, calluid, direction))
            con.commit()
            con.close()
        except (psycopg2.Error, Exception) as e:
            con.close()
        return json.dumps({"errors": [], 'data': {'status': '1'}})

    except Exception as e:
        print str(e)

        return json.dumps({"errors": ['Exception'], 'data': {'status': '0'}})


@blueprint_membership.route("/MembershipCard", methods=['GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def MembershipCard():
    try:

        membership_id = session["membership_id"]

        name = session['card_details']["name"]
        phone = session['card_details']["phone"]
        email = session['card_details']['email']
        age = session['card_details']['age']
        gender = str(session['card_details']['gender']).rstrip().lower()
        if 'language' in session.keys():
            language = session['language']
        else:
            language = 'Telugu'

        membership_instance = Membership_details()

        association_status = membership_instance.check_association_status(membership_id)

        if association_status:
            association_status = 'T'
            session['card_details']['association_status'] = "T"
        else:
            association_status = 'F'
            session['card_details']['association_status'] = "F"

        if 'img_url' in session.keys():
            img_url = session['img_url']
        else:
            img_url = './static/img/default-avatar.png'

        return render_template("membership/membership_s.html", membership_id=membership_id, name=name, phone=phone,
                               email=email, age=age, gender=gender, association_status=association_status,
                               img_url=img_url, language=language)
    except (KeyError, Exception) as e:
        print str(e)
        abort(401)

    abort(401)


@blueprint_membership.route("/membershipint", methods=['POST', 'GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def membershipinternational():
    if request.method == 'GET':
        return render_template('membership/membershipinternational.html')


@blueprint_membership.route('/resendOTPint', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def resendOTPint():
    if request.method == 'POST':
        try:
            umobile = session['umobile']
            email = session['email']
        except KeyError as e:
            return json.dumps({"errors": ['Your Session is Expired. Please try again'], 'data': {'status': '0'}})
        if umobile is None and umobile == "":
            return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
        if email is None and email == "":
            return json.dumps({'errors': ['email missing'], 'data': {'status': '0'}})
        if genEmailOtp(umobile, email, "MB"):

            js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'otp sent to your EmailID  Successfully'}})
            return js
        else:
            js = json.dumps({'errors': ['sorry, You already reached OTP limit. please try after some time'],
                             'data': {'status': '0'}})
            return js
    else:
        js = json.dumps({'errors': ['Not Allowed'], 'data': {'status': '0'}})
        return js


@blueprint_membership.route("/validateOTP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def validateOTP():
    try:
        if 'voter_id' in session.keys():
            voter_id = session['voter_id']
        if 'umobile' in session.keys():
            phone = session['umobile']
        otp = request.json['otp']

        if voter_id is None or voter_id.isspace():
            return json.dumps({"errors": ["invalid Voter_id .please try again"], "data": {"status": "0"}})
        if otp is None or otp.isspace():
            return json.dumps({"errors": ["invalid otp given"], "data": {"status": "0"}})
        if phone is None or phone.isspace():
            return json.dumps({"errors": ["invalid phone Number. please try again"], "data": {"status": "0"}})
        if verifyOtp(phone, otp, "MB"):

            children = {}

            voterdetails_instance = Voter_details()
            voter_id = str(voter_id).lower()
            record = voterdetails_instance.get_voter_details(voter_id)

            if record is not None:

                try:
                    children['Name'] = str(record['voter_name']).rstrip()
                except Exception as e:
                    children['Name'] = record['voter_name']
                try:
                    children['relation_name'] = str(record['relation_name']).rstrip()
                except Exception as e:
                    children['relation_name'] = record['relation_name']
                children['relation_type'] = record['relation_type']
                children['Age'] = record['age']
                children['Sex'] = record['sex']
                children['House_No'] = record['house_no']
                children['Booth No'] = record['part_no']
                children['booth_name'] = record['section_name']
                children['Section_id'] = str(record['ac_no']).zfill(3)
                children['state'] = record['state']

                try:
                    con = psycopg2.connect(config.DB_CONNECTION)
                    cur = con.cursor()
                    cur.execute(
                        "select constituency_name,district_name from assembly_constituencies where constituency_id=%s and state_id=%s",
                        (children['Section_id'], children['state']))
                    row = cur.fetchone()
                    con.close()
                    if row is not None:
                        children['constituency_name'] = row[0]
                        children['district_name'] = row[1]
                    else:
                        children['constituency_name'] = ''
                        children['district_name'] = ''

                except Exception as e:
                    print str(e)

                session['member_details'] = children

                return json.dumps({"errors": [], 'data': {'status': '1', 'children': children}})
            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0', 'children': children}})
        else:
            return json.dumps({"errors": ['invalid otp'], 'data': {'status': '0'}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_membership.route("/membership", methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def jspmembership():
    if request.method == 'GET':

        return render_template('membership/membership.html')
    elif request.method == 'POST':
        voter_id = ''
        try:

            if 'voter_id' in session.keys():
                voter_id = session['voter_id']
            if 'umobile' in session.keys():
                phone = session['umobile']
            evoter_id = session['evoter_id']
            emobile = session['emobile']
            if "country_name" in session.keys():
                country_name = session["country_name"]
            else:
                country_name = "india"
            print country_name
            email = request.json['email']

            referral_id = ''


        except (Exception, KeyError) as e:
            print str(e)
            return json.dumps({"errors": ['Your Session is Expired. Please try again'], 'data': {'status': '0'}})

        try:

            voter_id = str(voter_id).lower()

            membership_instance = Membership_details()

            if membership_instance.check_voterid_existence(evoter_id):
                if membership_instance.check_phonenumber_count(emobile):
                    if 'member_details' not in session.keys():
                        return json.dumps(
                            {"errors": ['Your Session is Expired. Please try again'], 'data': {'status': '0'}})
                    children = session['member_details']
                    state = children['state']
                    if str(state) == '1':
                        state = "AP"
                    else:
                        state = 'TS'

                    status, membership_id = generate_membershipid(children['Name'], phone, voter_id,
                                                                  children['relation_name'], children['relation_type'],
                                                                  children['House_No'], '', children['Age'],
                                                                  children['Sex'], children['Booth No'],
                                                                  children['Section_id'], state, email,
                                                                  children['booth_name'], referral_id, 'W',
                                                                  country_name=country_name)
                    if status:
                        session.pop('member_details', None)
                        session['membership_id'] = membership_id

                        session['card_details'] = {}
                        session['card_details']['name'] = children['Name']
                        session['card_details']['phone'] = phone
                        session['card_details']['email'] = email
                        session['card_details']['age'] = children['Age']
                        session['card_details']['gender'] = children['Sex']
                        session['card_details']['volunteer_status'] = 'F'
                        return json.dumps({"errors": [], "data": {"status": "1"}})

                    return json.dumps({"errors": ['Something Wrong . Please Try Again'], "data": {"status": ["0"]}})

                return json.dumps(
                    {"errors": ['limit with this Phone Number already reached'], "data": {"status": ["0"]}})

            return json.dumps({"errors": ['This Voter Id already existed'], "data": {"status": ["0"]}})


        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Something Wrong . Please Try Again'], "data": {"status": ["0"]}})
    else:
        return render_template('membership/membership.html')


@blueprint_membership.route('/resendOTP', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
def resendOTP():
    if request.method == 'POST':
        try:
            umobile = session['umobile']
        except KeyError as e:
            return json.dumps({"errors": ['Your Session is Expired. Please try again'], 'data': {'status': '0'}})
        if umobile is None and umobile == "":
            return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
        if genOtp(umobile, "MB"):

            js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'otp sent  Successfully'}})
            return js
        else:
            js = json.dumps({'errors': ['sorry, You already reached OTP limit. please try after some time'],
                             'data': {'status': '0'}})
            return js
    else:
        js = json.dumps({'errors': ['Not Allowed'], 'data': {'status': '0'}})
        return js

@blueprint_membership.route('/verifyOfflineDetails', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verifyOfflineDetails():
    try:
        umobile = request.values.get('mobile_num')
        voter_id = request.values.get('voter_id')
        
        if umobile is None or umobile == "":
            return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
        if voter_id is None or voter_id == "":
            return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})
       
        try:
           
            membership_instance = Membership_details()
            voter_id = str(voter_id).lower()
            evoter_id = encrypt_decrypt(voter_id, 'E')
            emobile = encrypt_decrypt(umobile, 'E')

            if membership_instance.check_voterid_existence(evoter_id):

                if membership_instance.check_phonenumber_count(emobile):
                    voterdetails_instance = Voter_details()
                    if voterdetails_instance.check_existence(voter_id):
                        record = voterdetails_instance.get_voter_details(voter_id)

                        if record is not None:

                            try:
                                con = psycopg2.connect(config.DB_CONNECTION)
                                cur = con.cursor()
                                cur.execute(
                                    "select district_id from assembly_constituencies where constituency_id=%s and state_id=%s",
                                    (record['ac_no'], record['state']))
                                row = cur.fetchone()
                                con.close()
                                if row is not None:
                                    record['district_id'] = row[0]
                                    
                                else:
                                    record['district_id'] = ''
                            except psycopg2.Error as e:
                                print str(e)


                            return json.dumps({'errors': [], 'data': {'status': '1', 'children': record}})

                        return json.dumps({'errors': ['something wrong in generating OTP'], 'data': {'status': '0'}})

                    return json.dumps({'errors': ['we could not found the given voter id'], 'data': {'status': '2'}})
                return json.dumps({'errors': ['limit with this Phone Number already reached'], 'data': {'status': '0'}})
            membership_instance = Membership_details()

            member_details = membership_instance.get_membership_details(None, evoter_id)
            membership_id = member_details['membership_id']
            message = "some one already registered as a member with this Voter ID"
            return json.dumps({'errors': [message], 'data': {'status': '0'}})

        except psycopg2.Error as e:
            print str(e)
            return json.dumps({'errors': ['something Wrong. please try again'], 'data': {'status': '0'}})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors': ['parameters missing. please try again'], 'data': {'status': '0'}})

@blueprint_membership.route('/getMembershipWithWhatsappBot', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")

def getMembershipWithWhatsappBot():
    mobile = request.values.get('mobilenumber')
    voter_id = request.values.get('voter_id')
    print mobile,voter_id
    print app.config["DB_CONNECTION"]
    if mobile is None or mobile == "":
        return json.dumps({'errors': ['mobile missing'], 'data': {'status': '0'}})
    if voter_id is None or voter_id == "":
        return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})

    try:
        
        if len(str(mobile)) == 10:
            mobile="91"+str(mobile)
            voter_id = urllib.unquote(voter_id).decode('utf8')
            if len(str(voter_id))<10 or len(str(voter_id))>14:
                return json.dumps({'errors': ['You have entered invalid voterid. please check your voter id and  enter again '], 'data': {'status': '0'}})
            membership_instance = Membership_details()

            e_voter_id=encrypt_decrypt(str(voter_id).lower(),'E')
            if membership_instance.check_voterid_existence(e_voter_id):
                emobile=encrypt_decrypt(mobile,'E')
                if membership_instance.check_phonenumber_count(emobile):
                    voterdetails_instance = Voter_details()

                    record = voterdetails_instance.get_voter_details(str(voter_id).lower())

                    if record is not None:

                        name = record['voter_name']

                        phone = mobile
                        if record['state'] == 1:
                            state = 'AP'
                        else:
                            state = 'TS'

                        status, membership_id = generate_membershipid_whatsappbot(name, phone, str(voter_id).lower(), record['relation_name'],
                                                                      record['relation_type'], record['house_no'], '',
                                                                      record['age'], record['sex'], record['part_no'],
                                                                      record['ac_no'], state, '',
                                                                      record['section_name'], '', 'WB')
                        if status:
                            success_message="Congratulations !! You have now joined millions of JanaSainiks contributing to JanaSena Party's vision of building a better Society. Your Membership ID is: "+str(membership_id)
                            return json.dumps({"errors": [],
                                               'data': {'status': '1',"message":success_message}})


                        return json.dumps(
                            {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})


                    return json.dumps({"errors": ['no record found with the given details'],
                                       'data': {'status': '2'}})


                return json.dumps(
                    {'errors': ['limit with this Phone Number already reached'], 'data': {'status': '0'}})

            return json.dumps({'errors': ['someone already Registered with the given VoterID'], 'data': {'status': '0'}})

        else:
            return json.dumps({'errors': ['supporting indian numbers only'], 'data': {'status': '0'}})

    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Exception occured'], 'data': {'status': '0'}})



@blueprint_membership.route('/whatsapp_manual_membership', methods=[ 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def whatsapp_manual_membership():
    try:
        data=request.json
        parameters=["name","voter_id","gender","age","state_id","district","constituency","phone","referral_phone"]
        if set(parameters).issubset(set(data.keys())):
            for parameter in parameters:
                if parameter not in ['voter_id','referral_phone']:
                    if data[parameter] is None or data[parameter]=='':
                        return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
            mobile=data['phone']

            if len(str(mobile)) == 10:
                mobile="91"+str(mobile)
            emobile=encrypt_decrypt(mobile,'E')
            membership_instance = Membership_details()

            
            if membership_instance.check_manual_phonenumber_count(emobile):
                voter_id=data['voter_id']
                voter_status=False
                if voter_id is not None:
                    e_voter_id=encrypt_decrypt(str(voter_id).lower(),'E')

                    if membership_instance.check_voterid_existence(e_voter_id):
                        voter_status=True
                    else:
                        voter_status=False
                else:
                    voter_status=True


                if voter_status:
                    if data['state_id'] == 1:
                        state = 'AP'
                    else:
                        state = 'TS'
                    referral_mobile=data['referral_phone']
                    if referral_mobile is not None:
                        if len(str(referral_mobile))==10:
                            referral_mobile="91"+referral_mobile
                        referral_mobile=encrypt_decrypt(referral_mobile,"E")
                    referral_id=membership_instance.get_membership_id_with_phone_whatsapp(referral_mobile)
                    if not referral_id:
                        referral_id=''

                    status, membership_id = generate_membershipid(data['name'], mobile, str(voter_id).lower(), "",
                                                                      "", "", '',
                                                                      data['age'], data['gender'], 0,
                                                                      data['constituency'], state, '',
                                                                      "", referral_id, 'WB',country_name='india')
                    if status:
                        success_message="Congratulations !! You have now joined millions of JanaSainiks contributing to JanaSena Party's vision of building a better Society. Your Membership ID is: "+str(membership_id)
                        return json.dumps({"errors": [],
                                           'data': {'status': '1',"message":success_message}})


                    return json.dumps(
                        {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})


                else:
                    return json.dumps({'errors': ['someone already Registered with the given VoterID'], 'data': {'status': '0'}})



            return json.dumps(
                    {'errors': [' already registered with this phone'], 'data': {'status': '0'}})

        else:
            return json.dumps({"errors":["Parameters Missing. please check "],"status":"0"})



    except Exception as e:
        print str(e)
        return json.dumps({"errors":["Something Wrong"],"status":"0"})




    

@blueprint_membership.route('/ReferralswithWhatsappBot', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")

def ReferralswithWhatsappBot():
    
    message_body = request.values.get('message')
    
    
    if message_body is None or message_body == "":
        return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})

    try:
        message_body=urllib.unquote(message_body).decode('utf8')
        message_parts=message_body.split("-")
        if len(message_parts)!=3:
            return json.dumps({'errors': ['Invalid Details.ase check and send in correct format'], 'data': {'status': '0'}})
        volunteer_membership_id=str(message_parts[0]).upper()
        referral_mobile=message_parts[1]
        voter_id=message_parts[2]
        print referral_mobile,voter_id
        if len(str(referral_mobile)) == 10 :
            referral_mobile="91"+str(referral_mobile)
            
            if len(str(voter_id))<10 or len(str(voter_id))>14:
                return json.dumps({'errors': ['You have entered invalid voterid. please check your voter id and  enter again '], 'data': {'status': '0'}})
            membership_instance = Membership_details()
                       
            if not membership_instance.check_membership_existence(volunteer_membership_id):
               return json.dumps({'errors': ['we did not found any details with the given referral Membership ID .please check '], 'data': {'status': '0'}}) 
            e_voter_id=encrypt_decrypt(str(voter_id).lower(),'E')
            if membership_instance.check_voterid_existence(e_voter_id):
                emobile=encrypt_decrypt(referral_mobile,'E')
                if membership_instance.check_phonenumber_count(emobile):
                    voterdetails_instance = Voter_details()

                    record = voterdetails_instance.get_voter_details(str(voter_id).lower())

                    if record is not None:
                        name = record['voter_name']

                        phone = referral_mobile
                        if record['state'] == 1:
                            state = 'AP'
                        else:
                            state = 'TS'

                        status, membership_id = generate_membershipid(name, phone, str(voter_id).lower(), record['relation_name'],
                                                                      record['relation_type'], record['house_no'], '',
                                                                      record['age'], record['sex'], record['part_no'],
                                                                      record['ac_no'], state, '',
                                                                      record['section_name'], volunteer_membership_id, 'WB')
                        if status:
                            success_message="Congratulations !! You have now joined millions of JanaSainiks contributing to JanaSena Party's vision of building a better Society. Your Membership ID is: "+str(membership_id)
                            return json.dumps({"errors": [],
                                               'data': {'status': '1',"message":success_message}})


                        return json.dumps(
                            {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})



                        
                    return json.dumps({"errors": ['no record found with the given details'],
                                       'data': {'status': '0'}})


                return json.dumps(
                    {'errors': ['limit with this Phone Number already reached'], 'data': {'status': '0'}})

            return json.dumps({'errors': ['someone already Registered with the given VoterID'], 'data': {'status': '0'}})

        else:
            return json.dumps({'errors': ['supporting indian numbers only'], 'data': {'status': '0'}})

    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Exception occured'], 'data': {'status': '0'}})

@blueprint_membership.route('/verifyRreferralOTP', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")

def verifyRreferralOTP():
    volunteer_mobile = request.values.get('mobilenumber')
    message_body = request.values.get('message')
    otp=request.values.get('otp')
    if volunteer_mobile is None or volunteer_mobile == "":
        return json.dumps({'errors': ['mobile missing'], 'data': {'status': '0'}})
    if message_body is None or message_body == "":
        return json.dumps({'errors': ['voter_id missing'], 'data': {'status': '0'}})

    try:
        message_body=urllib.unquote(message_body).decode('utf8')
        message_parts=message_body.split("-")
        
        referral_mobile=message_parts[0]
        voter_id=message_parts[1]
        
        if len(str(referral_mobile)) == 10 :
                referral_mobile="91"+str(referral_mobile)
                
                if len(str(voter_id))<10 or len(str(voter_id))>14:
                    return json.dumps({'errors': ['You have entered invalid voterid. please check your voter id and  enter again '], 'data': {'status': '0'}})
                
                membership_instance = Membership_details()
                volunteer_mobile="91"+str(volunteer_mobile)
                e_volunteer_mobile=encrypt_decrypt(volunteer_mobile,'E')
                volunteer_membership_id=membership_instance.check_volunteer_membership_id(e_volunteer_mobile)
                if not volunteer_membership_id:
                    return json.dumps({'errors': ['You dont have rights to refer others. Please take your membership  (you are not a member to refer someone)'], 'data': {'status': '0'}}) 

                
                e_voter_id=encrypt_decrypt(str(voter_id).lower(),'E')
                if membership_instance.check_voterid_existence(voter_id):
                    emobile=encrypt_decrypt(referral_mobile,'E')
                    if membership_instance.check_phonenumber_count(emobile):
                        if verifyOtp(referral_mobile, otp, "MB"):
                            voterdetails_instance = Voter_details()

                            record = voterdetails_instance.get_voter_details(str(voter_id).lower())
                            if record is not None:

                                name = record['voter_name']

                                phone = referral_mobile
                                if record['state'] == 1:
                                    state = 'AP'
                                else:
                                    state = 'TS'

                                status, membership_id = generate_membershipid(name, phone, str(voter_id).lower(), record['relation_name'],
                                                                              record['relation_type'], record['house_no'], '',
                                                                              record['age'], record['sex'], record['part_no'],
                                                                              record['ac_no'], state, '',
                                                                              record['section_name'], volunteer_membership_id, 'WB')
                                if status:
                                    success_message="Congratulations !! You have now joined millions of JanaSainiks contributing to JanaSena Party's vision of building a better Society. your Membership ID is: "+str(membership_id)
                                    return json.dumps({"errors": [],
                                                       'data': {'status': '1',"message":success_message}})


                                return json.dumps(
                                    {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})


                            return json.dumps({"errors": ['no record found with the given details'],
                                       'data': {'status': '0'}})


                        return json.dumps(
                        {'errors': ['You have entered invalid OTP. please check and try again '], 'data': {'status': '0'}})


                        

                    return json.dumps(
                        {'errors': ['limit with this Phone Number already reached'], 'data': {'status': '0'}})

                return json.dumps({'errors': ['someone already Registered with the given VoterID'], 'data': {'status': '0'}})

        else:
            return json.dumps({'errors': ['supporting indian numbers only'], 'data': {'status': '0'}})

    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Exception occured'], 'data': {'status': '0'}})


@blueprint_membership.route('/membership_with_callcenter', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def membership_with_callcenter():
    
    try:
        phone = request.values.get('phone')
        voter_id = request.values.get('voter_id')
        constituency_id = request.values.get('constituency_id')
        full_name = request.values.get('full_name')
        district_id = request.values.get('district_id')
        state_id=request.values.get('state_id')
        age=request.values.get('age')
        gender=request.values.get('gender')
        print phone,voter_id,constituency_id,full_name,district_id,state_id,age,gender
        if phone is None or phone=='':
            return json.dumps({'status':0,'errors':['MISSING phone PARAMETER']})
        
        if len(phone) == 10:
            phone = '91'+str(phone)
        encrypted_phone = encrypt_decrypt(phone,'E')
        membership_instance = Membership_details()
        if membership_instance.check_phonenumber_existence(encrypted_phone):
            if voter_id is not None:
                
                encrypted_voter_id = encrypt_decrypt(voter_id,'E')
                if membership_instance.check_voterid_existence(encrypted_voter_id):
                    voterdetails_instance = Voter_details()

                    record = voterdetails_instance.get_voter_details(str(voter_id).lower())

                    if record is not None:

                        name = record['voter_name']

                        phone = phone
                        
                        if record['state'] == 1:
                            state = 'AP'
                        else:
                            state = 'TS'

                        status, membership_id = generate_membershipid_whatsappbot(name, phone, str(voter_id).lower(), record['relation_name'],
                                                                      record['relation_type'], record['house_no'], '',
                                                                      record['age'], record['sex'], record['part_no'],
                                                                      record['ac_no'], state, '',
                                                                      record['section_name'], '', 'CC')
                        if status:
                            
                            success_message="Success !!! Details Updated. Membership ID is: "+str(membership_id)
                            return json.dumps({'status': 1,"errors": [],"message":success_message})

                        else:
                            return json.dumps({'status': 0,"errors": ["something wrong with given details. please try again"]})
                    else:
                        if state_id is None or state_id=='':
                            return json.dumps({'status': 0, 'errors': ['Voter ID not found in database .please send Other PARAMETERs']})
                        if full_name is None or full_name=='':
                                return json.dumps({'status': 0, 'errors': ['MISSING full name PARAMETER']})
                        if age is None or age=='':
                                return json.dumps({'status': 0, 'errors': ['MISSING age PARAMETER']})
                        if gender is None or gender=='':
                                return json.dumps({'status': 0, 'errors': ['MISSING gender PARAMETER']})
                        try:
                            age=int(age)
                
                        except Exception as e:
                            return json.dumps({'status': 0, 'errors': [' age should be number']})
                        if str(state_id)<='2':

                            if constituency_id is None or constituency_id=='':
                                return json.dumps({'status': 0, 'errors': ['MISSING constituency_id PARAMETER']})

                            
                            if district_id is None or district_id=='':
                                return json.dumps({'status': 0, 'errors': ['MISSING district id PARAMETER']})
                        else:
                            district_id=0
                            constituency_id=0
                        status,membership_id=membership_with_novoterid(district_id,constituency_id,full_name,phone,voter_id,state_id,age,gender)
                        if status:
                            success_message="Success !!! Details Updated. Membership ID is: "+str(membership_id)
                            return json.dumps({'status': 1,"errors": [],"message":success_message})
                        else:
                            return json.dumps({'status': 0,"errors": ["something wrong with given details. please try again"]})
            
                else:
                    return json.dumps({'status': 2, 'errors': ['User already registered with this voter ID.']})

            else:
                if state_id is None or state_id=='':
                    return json.dumps({'status': 0, 'errors': ['MISSING state id PARAMETER']})
                if full_name is None or full_name=='':
                        return json.dumps({'status': 0, 'errors': ['MISSING full name PARAMETER']})
                if age is None or age=='':
                    return json.dumps({'status': 0, 'errors': ['MISSING age PARAMETER']})
                if gender is None or gender=='':
                    return json.dumps({'status': 0, 'errors': ['MISSING gender PARAMETER']})
                try:
                    age=int(age)
                
                    
                except Exception as e:
                    return json.dumps({'status': 0, 'errors': [' age should be number']})
                if str(state_id)<='2':

                    if constituency_id is None or constituency_id=='':
                        return json.dumps({'status': 0, 'errors': ['MISSING constituency_id PARAMETER']})

                    
                    if district_id is None or district_id=='':
                        return json.dumps({'status': 0, 'errors': ['MISSING district id PARAMETER']})
                else:
                    district_id=0
                    constituency_id=0    

                status,membership_id=membership_with_novoterid(district_id,constituency_id,full_name,phone,voter_id,state_id,age,gender)        
                
                if status:
                    success_message="Success !!! Details Updated. Membership ID is: "+str(membership_id)
                    return json.dumps({'status': 1,"errors": [],"message":success_message})
                else:
                    return json.dumps({'status': 0,"errors": ["something wrong with given details. please try again"]})
        
        else:
            return json.dumps({'status':2,'errors':['User already registered as a member with this phone number']})
    except Exception as e:
        print str(e)
        return json.dumps({'status':2,'errors':['Something Wrong.please try again']})



def membership_with_novoterid(district_id,constituency_id,full_name,phone,voter_id,state_id,age,gender):
    try:
        
        if str(state_id)=='1':
            state='AP'
        elif str(state_id)=='2':
            state='TS'
        elif str(state_id)=='3':
            state='KA'
        elif str(state_id)=='4':
            state='TN'
        else:
            state='others'

    
        status,membership_id=generate_membershipid(name=full_name,voter_id=voter_id,state=state,
                                  age=age,assembly_id=constituency_id,membership_through='CC',
                                  gender=gender,address='',town='',booth=0,booth_name=None,
                                  email=None,phone=phone,relation_name=None,relation_type=None,referral_id='')
        return status,membership_id
    except Exception as e:
        print str(e)
        return False,''

