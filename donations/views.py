import json
import psycopg2

from flask import Blueprint, request, render_template, flash, url_for
from psycopg2.extras import RealDictCursor
from werkzeug.utils import redirect
from utilities.classes import  encrypt_decrypt
import config
from appholder import csrf,table_service, block_blob_service
from constants import empty_check
from main import generateOrderNumber, create_RPorderId, create_IMorderId,create_NRI_IMorderId
from utilities.decorators import crossdomain, admin_login_required,requires_api_token,required_roles
from utilities.general import get_payment_details,id_generator
from celery_tasks.sms_email import senddonationsemail,senddonationsreceipt,check_all_insta_mojo_transactions,upload_passport_file,upload_visa_file,sendNriDonationsEmail,send_recurring_payment_activation_email,send_recurring_donations_ack,send_subscription_cancel_email
from utilities.others import give_cursor_json

blueprint_donations = Blueprint('donations', __name__)


@blueprint_donations.route('/nri_donations', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
def nri_donations():
    if request.method=='GET':
        return render_template('donations/nri_donations.html')

    payment_option=request.values.get("payment_option")
    
    name = request.values.get('name')
    last_name = request.values.get('last_name')
    #nri_address = request.values.get('nri_address')
    email = request.values.get('email')
    mobile = request.values.get('mobile')
    country = request.values.get('country')
    passport=request.values.get('passport')
    remainder = request.values.get('remainder_status')
    pancard=request.values.get('pancard')
    drno=request.values.get('drno')
    village_street=request.values.get('village_street')
    city=request.values.get('city')
    pincode=request.values.get('pincode')
    state=request.values.get('state')

    errors = []

    if name is None or name == '':
        errors.append("name should not be empty")
    if last_name is None or last_name == '':
        errors.append("last_name should not be empty")
    if nri_address is None or nri_address == '':
        errors.append("address should not be empty")
    if email is None or email == '':
        errors.append("email should not be empty")
    if mobile is None or mobile == '':
        errors.append("mobile number should not be empty")
    if passport is None or passport == '':
        errors.append("passport should not be empty")
    if country is None or country == '':
        errors.append("country should not be empty")
    if pancard is None or pancard == '':
        errors.append("pancard should not be empty")
    if drno is None or drno == '':
        errors.append("Door Number number should not be empty")
    if village_street is None or village_street == '':
        errors.append("village should not be empty")
    if city is None or city == '':
        errors.append("city should not be empty")
    if pincode is None or pincode == '':
        errors.append("pincode should not be empty")
    if state is None or state == '':
        errors.append("state should not be empty")
    if remainder is None or remainder == '':
        errors.append("remainder status should not be empty")
    if len(errors):
        
        return json.dumps({'errors': errors, 'status': 0})
    transaction_id = generateOrderNumber()
    if transaction_id == '':
        
        return json.dumps({'errors': ['something wrong in generating order_id ,please try again'], 'status': 0})
    
    if payment_option=='ind':
        try:

            amount=request.values.get('amount')            
            
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            total_amount = int(amount)
            
            orderid = ''
            payment_url = ''
            pmt_gtw='IM'
            try:
               
                response = create_NRI_IMorderId(amount,mobile,email)
                if response['success']:
                    orderid = response['payment_request']['id']
                    payment_url = response['payment_request']['longurl']
                

            except Exception as e:
                print str(e)
                flash(str(e), 'error')
                return json.dumps({'errors': ['Exception'], 'status': 0})
            if orderid == '' or orderid is None:
                
                return json.dumps({'errors': ['Exception'], 'status': 0})
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            e_mobile=encrypt_decrypt(str(mobile),'E')
            e_passport=encrypt_decrypt(str(passport),'E')
            e_email= encrypt_decrypt(str(email),'E')
            e_last_name=encrypt_decrypt(str(last_name),'E')
            cur.execute(
                    """insert into janasena_nri_donations(donar_name,email,phone,pancard,transaction_id,order_id,
                    initiated_payment,confirmation,payment_status,country,transaction_through,
                    monthly_remainder,last_name,passport,state,city,door_no,village,pincode)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                    (name,  e_email, e_mobile, pancard,  transaction_id,orderid, total_amount, 'Y',
                    'I',country,  pmt_gtw, remainder,e_last_name,e_passport,state,city,drno,village_street,pincode))
            con.commit()
            con.close()
            if config.ENVIRONMENT_VARIABLE!='local':
                
                if request.files['passport_file']:
                    passport_file=request.files['passport_file']
                    
                    upload_passport_file.delay(passport_file.read(), passport_file.filename, passport_file.name, passport_file.content_length, passport_file.content_type, passport_file.headers,transaction_id)
                  
            else:
                
                if request.files['passport_file']:
                    passport_file=request.files['passport_file']
                    
                    upload_passport_file.delay(passport_file.read(), passport_file.filename, passport_file.name, passport_file.content_length, passport_file.content_type, passport_file.headers,transaction_id)
                
            
            return json.dumps({'errors':[],'status':1,'payment_url':payment_url})
            
 
        except (psycopg2.Error, Exception) as e:
            print str(e)
            flash("something wrong in generating order_id ,please try again", 'error')
            return json.dumps({'errors': ['Exception'], 'status': 0})


    else:
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            e_mobile=encrypt_decrypt(str(mobile),'E')
            e_passport=encrypt_decrypt(str(passport),'E')
            e_email= encrypt_decrypt(str(email),'E')
            e_last_name=encrypt_decrypt(str(last_name),'E')
            cur.execute(
                    """insert into janasena_nri_donations(donar_name,address,email,phone,transaction_id,country,
                    transaction_through,last_name,passport,monthly_remainder,pancard,state,city,village,door_no,pincode)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                    (name, nri_address, email, mobile,  transaction_id, country,  "RTGS/NEFT", e_last_name,passport,remainder,pancard,state,city,village_street,drno,pincode))
            con.commit()
            con.close()

            if config.ENVIRONMENT_VARIABLE!='local':
                sendNriDonationsEmail.delay(transaction_id,name,email)
                if request.files['passport_file']:
                    passport_file=request.files['passport_file']
                    
                    upload_passport_file.delay(passport_file.read(), passport_file.filename, passport_file.name, passport_file.content_length, passport_file.content_type, passport_file.headers,transaction_id)
                
            else:
                sendNriDonationsEmail(transaction_id,name,email)
                if request.files['passport_file']:
                    passport_file=request.files['passport_file']
                    
                    upload_passport_file(passport_file.read(), passport_file.filename, passport_file.name, passport_file.content_length, passport_file.content_type, passport_file.headers,transaction_id)
                 
            return json.dumps({'errors':[],'status':1,'transaction_id':transaction_id})
            
 
        except (psycopg2.Error, Exception) as e:
            print str(e)
            flash("something wrong in generating order_id ,please try again", 'error')
            return json.dumps({'errors': ['Exception'], 'status': 0})


@blueprint_donations.route('/donations', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")

def donations():
    if request.method == "GET":

        return render_template('donations/index.html')
    elif request.method == "POST":
        name = request.values.get('name')
        last_name = request.values.get('last_name')
        fathername = request.values.get('fathername')
        email = request.values.get('email')
        mobile = request.values.get('mobile')
        amount = request.values.get('amount')
        address = request.values.get('address')
        pancard = request.values.get('pancard')
        country = request.values.get('country')
        state = request.values.get('state')
      
        remainder = request.form.get('remainder')
        district=request.form.get('district')
        constituency = request.form.get('constituency')
        
        

        errors = []

        if name is None or name == '':
            errors.append("name should not be empty")
        if last_name is None or last_name == '':
            errors.append("last_name should not be empty")
        if fathername is None or fathername == '':
            errors.append("fathername should not be empty")
        if email is None or email == '':
            errors.append("email should not be empty")
        if mobile is None or mobile == '':
            errors.append("mobile number should not be empty")
        if amount is None or amount == '':
            errors.append("amount should not be empty")
        if address is None or address == '':
            errors.append("address should not be empty")

        if len(errors):
            flash(errors, 'error')
            return json.dumps({'errors': ['Exception'], 'status': 0})
        transaction_id = generateOrderNumber()
        if transaction_id == '':
            flash("something wrong in generating order_id ,please try again", 'error')
            return json.dumps({'errors': ['Exception'], 'status': 0})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            total_amount = int(amount)

            orderid = ''
            payment_url = ''
            pmt_gtw='IM'
            try:
               
                response = create_IMorderId(amount, mobile, email)
                if response['success']:
                    orderid = response['payment_request']['id']
                    payment_url = response['payment_request']['longurl']
                

            except Exception as e:
                print str(e)
                flash(str(e), 'error')
                return json.dumps({'errors': ['Exception'], 'status': 0})
            if orderid == '' or orderid is None:
                flash("something wrong in generating order_id ,please try again", 'error')
                return json.dumps({'errors': ['Exception'], 'status': 0})
            cur.execute(
                    "insert into janasena_donations(donar_name,relation_name,email,phone,pancard,address,transaction_id,order_id,initiated_payment,confirmation,payment_status,country,state,district_id,constituency_id,transaction_through,monthly_remainder,last_name)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    (name, fathername, email, mobile, pancard, address, transaction_id, orderid, total_amount, 'Y',
                     'I',
                     country, state,district,constituency, pmt_gtw, remainder,last_name))
            con.commit()
            con.close()
            
                
            return json.dumps({'errors':[],'status':1,'payment_url':payment_url})
            
 
        except (psycopg2.Error, Exception) as e:
            print str(e)
            flash("something wrong in generating order_id ,please try again", 'error')
            return json.dumps({'errors': ['Exception'], 'status': 0})



# nri instamojo response
@blueprint_donations.route('/nri_donation_response', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def nri_donation_response():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        payment_id = request.values.get('payment_id')
        payment_request_id = request.values.get('payment_request_id')
        cur.execute("select seq_id from janasena_nri_donations where payment_id=%s ", (payment_id,))
        row = cur.fetchone()
        if row is None:

            response = get_payment_details(payment_id)

            response = json.loads(response)
            print response
            status = response['payment']['status']

            if status == "Credit":
                payment_method = response['payment']["instrument_type"]
                transaction_payment = response['payment']["amount"]
                cur.execute(
                    "update janasena_nri_donations set payment_id=%s,payment_status=%s,update_dttm=now(),payment_method=%s,transaction_payment=%s where order_id=%s returning seq_id,donar_name,last_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method,country ",
                    (payment_id, 'S', payment_method, transaction_payment, payment_request_id))
                details = cur.fetchone()
                print details
                con.commit()
                con.close()
                payment = {}
                if details is not None:

                    payment = details
                    payment['phone']=encrypt_decrypt(payment['phone'],'D')
                    payment['email']=encrypt_decrypt(payment['email'],'D')
                    last_name=encrypt_decrypt(payment['last_name'],'D')
                    payment['donar_name']=payment['donar_name']+" "+last_name
                    if config.ENVIRONMENT_VARIABLE != 'local':
                        senddonationsemail.delay(payment)
                    else:
                        senddonationsemail(payment)

                    return render_template("donations/payment_success.html", payment=payment)
                else:
                    return render_template("donations/support.html")
            else:

                cur.execute(
                    "update janasena_nri_donations set payment_status=%s,payment_id=%s,update_dttm=now() where order_id=%s returning seq_id ",
                    ('F', payment_id, payment_request_id))
                con.commit()
                con.close()
                return render_template("donations/support.html")
        else:
            return redirect(url_for('.donations'))

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return render_template("donations/support.html")

## instamojo response
@blueprint_donations.route('/donationResponse', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def donationResponse():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        payment_id = request.values.get('payment_id')
        payment_request_id = request.values.get('payment_request_id')
        cur.execute("select seq_id from janasena_donations where payment_id=%s ", (payment_id,))
        row = cur.fetchone()
        if row is None:

            response = get_payment_details(payment_id)

            response = json.loads(response)
            print response
            status = response['payment']['status']

            if status == "Credit":
                payment_method = response['payment']["instrument_type"]
                transaction_payment = response['payment']["amount"]
                cur.execute(
                    "update janasena_donations set payment_id=%s,payment_status=%s,update_dttm=now(),payment_method=%s,transaction_payment=%s where order_id=%s returning seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method ",
                    (payment_id, 'S', payment_method, transaction_payment, payment_request_id))
                details = cur.fetchone()
                print details
                con.commit()
                con.close()
                payment = {}
                if details is not None:
                    payment = details

                    if config.ENVIRONMENT_VARIABLE != 'local':
                        senddonationsemail.delay(payment)
                    else:
                        senddonationsemail(payment)

                    return render_template("donations/payment_success.html", payment=payment)
                else:
                    return render_template("donations/support.html")
            else:

                cur.execute(
                    "update janasena_donations set payment_status=%s,payment_id=%s,update_dttm=now() where order_id=%s returning seq_id ",
                    ('F', payment_id, payment_request_id))
                con.commit()
                con.close()
                return render_template("donations/support.html")
        else:
            return redirect(url_for('.donations'))

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return render_template("donations/support.html")


@blueprint_donations.route('/resenddonationResponse', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def resenddonationResponse():

    status=check_all_insta_mojo_transactions.delay()
    return str(status)


def check_all_insta_mojo_transactions1():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        #'8919983656','9440268572','9666420898','9848030068'
        
        cur.execute("select seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method from janasena_donations where monthly_remainder='MS' ")
        rows = cur.fetchall()
        for row in rows:
            
                
            details = row
            print details
            seq_id=row['seq_id']
            cur.execute("update janasena_donations set monthly_remainder='yes' where seq_id=%s",(seq_id,))
            con.commit()
            
            payment = {}
            if details is not None:
                payment = details

                if config.ENVIRONMENT_VARIABLE != 'local':
                    senddonationsemail.delay(payment)
                else:
                    senddonationsemail(payment)

       
        return 'success'
    except Exception as e:
        print str(e)
        return str(e)

@blueprint_donations.route('/download_donation_receipt', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","AC"])
@csrf.exempt
def download_donation_receipt():
    transaction_id=request.values.get('transaction_id')
    if transaction_id is None:
        return json.dumps({"errors":["transaction_id missing"],'status':"0"})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method  from janasena_donations where transaction_id=%s and payment_status=%s order by create_dttm desc limit 1",(transaction_id,'S'))
        payment=cur.fetchone()
        if payment is not None:
            donation_receipt = get_pdf_receipt(payment)
            if donation_receipt:
                return_file = open(donation_receipt, 'r')
                response = make_response(return_file.read(),200)
   
                response.headers['Content-Description'] = 'File Transfer'
                response.headers['Cache-Control'] = 'no-cache'
                response.headers['Content-Type'] = 'application.pdf'
                response.headers['Content-Disposition'] = 'attachment; filename='+transaction_id+'.pdf'
                return response
        return json.dumps({'errors':["payment details not found"],'status':'0'})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors':["something wrong .please try again"],'status':'0'})



@blueprint_donations.route('/donations_emailreceipt', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","AC"])
@csrf.exempt
def donations_emailreceipt():
    transaction_id=request.values.get('transaction_id')
    if transaction_id is None:
        return json.dumps({"errors":["transaction_id missing"],'status':"0"})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method  from janasena_donations where transaction_id=%s and payment_status=%s order by create_dttm desc limit 1",(transaction_id,'S'))
        payment=cur.fetchone()
        if payment is not None:
            if config.ENVIRONMENT_VARIABLE != 'local':
                senddonationsreceipt.delay(payment)
            else:
                senddonationsreceipt(payment)
        return json.dumps({'errors':[],'status':'1'})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors':["something wrong .please try again"],'status':'0'})


@blueprint_donations.route('/donation_details_by_phone', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def donation_details_by_phone():
    phone=request.values.get('phone')
    if phone is None:
        return json.dumps({"errors":["phone missing"],'status':"0"})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,state,payment_method  from janasena_donations where phone=%s and payment_status=%s order by create_dttm desc",(phone,'S'))
        details=cur.fetchall()
        if len(details) :

            return json.dumps({'errors':[],'status':'1',"data":details})
        else:
            return json.dumps({'errors':[],'status':'1',"data":[]})

    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors':["something wrong .please try again"],'status':'0'})
  
@blueprint_donations.route('/donations_paginate', methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@requires_api_token
def donations_paginate():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
   
        cur.execute("""select cast(date(update_dttm  AT TIME ZONE 'Asia/Calcutta' )as text) as date,cast(sum(cast(transaction_payment as numeric))as text) as amount from janasena_donations where date(update_dttm  AT TIME ZONE 'Asia/Calcutta' ) > current_date - interval '15' day and payment_status='S' group by date order by date desc;""")
        rows=cur.fetchall()
   
        return json.dumps({'errors':[],'status':1,'data':rows})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({'errors':["something wrong .please try again"],'status':0,'data':rows})
def get_payment_details(payment_id):
    try:
        import requests

        headers = {"X-Api-Key": config.insta_mojo_key, "X-Auth-Token": config.insta_mojo_secret}
        response = requests.get(
            "https://www.instamojo.com/api/1.1/payments/" + payment_id + "/",
            headers=headers)

        print response.text
        return response.text
    except Exception as e:
        print str(e)
        return ''


## razor pay
@blueprint_donations.route('/janasenadonations', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def janasenadonations():
    if request.method == "GET":

        return render_template('donations/index.html')
    elif request.method == "POST":
        name = request.form.get('name')
        last_name=request.form.get('last_name')
        fathername = request.form.get('fathername')
        email = request.form.get('email')
        mobile = request.form.get('mobile')
        amount = request.form.get('amount')
        address = request.form.get('address')
        pancard = request.form.get('pancard')
        country = request.form.get('country')
        state = request.form.get('state')
        remainder = request.form.get('remainder')
        print country, state
        errors = []
        if name is None or name == '':
            errors.append("name should not be empty")
        if fathername is None or fathername == '':
            errors.append("fathername should not be empty")
        if email is None or email == '':
            errors.append("email should not be empty")
        if mobile is None or mobile == '':
            errors.append("mobile number should not be empty")
        if amount is None or amount == '':
            errors.append("amount should not be empty")
        if address is None or address == '':
            errors.append("address should not be empty")

        if len(errors):
            flash(errors, 'error')
            return render_template('donations/donate.html')
        transaction_id = generateOrderNumber()
        if transaction_id == '':
            flash("something wrong in generating order_id ,please try again", 'error')
            return render_template('donations/donate.html')
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            total_amount = int(amount)
            print total_amount
            amount_paisa = int(total_amount) * 100
            orderid = ''
            try:
                orderid = create_RPorderId(amount_paisa, transaction_id, 'INR')
            except Exception as e:
                flash(str(e), 'error')
                return render_template('donations/index.html')
            if orderid == '' or orderid is None:
                flash("something wrong in processing  Your Payment ,please try again", 'error')
                return render_template('donations/donate.html')
            print orderid
            cur.execute(
                "insert into janasena_donations(donar_name,relation_name,email,phone,pancard,address,transaction_id,order_id,initiated_payment,confirmation,payment_status,country,state,transaction_through,monthly_remainder)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                (name, fathername, email, mobile, pancard, address, transaction_id, orderid, total_amount, 'Y', 'I',
                 country, state, "RP",remainder))
            con.commit()
            con.close()
            return render_template("donations/razorpay.html", email=email, phone=mobile, name=name, orderid=orderid,
                                   amount=total_amount, key=config.razor_pay_key, transaction_id=transaction_id,
                                   amount_paisa=amount_paisa)

        except (psycopg2.Error, Exception) as e:
            print str(e)
            flash("something wrong in generating order_id ,please try again", 'error')
            return render_template('donations/donate.html')


@blueprint_donations.route('/paytmreceipt', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def paytmreceipt():
    return render_template("donations/paytm_receipt.html")






## razorpay response
@blueprint_donations.route('/donationstatus', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def donationstatus():
    if request.method == 'POST':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            payment_id = request.form.get('razorpay_payment_id')

            if payment_id is None:
                print 'something went wrong'
                return redirect(url_for('.donations'))
            cur.execute("select seq_id from janasena_donations where payment_id=%s ", (payment_id,))
            row = cur.fetchone()
            if row is None:
                client = config.client
                resp = client.payment.fetch(payment_id)
                print resp
                if resp['status'] == 'authorized':
                    order_id = resp['order_id']
                    amount = int(int(resp['amount']) / 100)
                    amount = str(amount)
                    payment_method = resp['method']
                    cur.execute(
                        "update janasena_donations set transaction_payment=%s,payment_id=%s,payment_status=%s,update_dttm=now(),payment_method=%s where order_id=%s returning seq_id,concat(donar_name,' ',last_name) as donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY') as date,state,payment_method",
                        (amount, payment_id, 'S', payment_method, order_id))
                    details = cur.fetchone()
                    con.commit()
                    con.close()
                    payment = {}
                    if details is not None:
                        payment = details
                        if config.ENVIRONMENT_VARIABLE != 'local':
                            senddonationsemail.delay(payment)
                        else:
                            senddonationsemail(payment)
                        
                        try:
                            capture_status=client.payment.capture(payment_id,resp['amount'])
                        except Exception as e:
                            print str(e)
                        return render_template("donations/payment_success.html", payment=payment)
                    else:
                        return render_template("donations/support.html")
                elif resp['status'] == 'failed':
                    order_id = resp['order_id']
                    cur.execute(
                        "update janasena_donations set payment_status=%s,payment_id=%s,update_dttm=now() where order_id=%s returning seq_id ",
                        ('F', payment_id, order_id))
                    con.commit()
                    con.close()
                    return render_template("donations/support.html")
            else:
                return redirect(url_for('.donations'))

        except (psycopg2.Error, Exception) as e:
            print str(e)
            return render_template("donations/support.html")


@blueprint_donations.route('/emailreceipt', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def emailreceipt():
    data = {'donar_name': 'rambabu', 'relation_name': 'chalapati rao', 'email': 'rambabu.v68@gmail.com',
            'phone': '9676881991', 'address': 'guntur', 'transaction_id': '12345678', 'transaction_amount': '2000',
            'date': '01-05-2018', 'state': 'ap', 'payment_method': 'card'}
    return render_template("donations/email_receipt.html", payment=data)

@blueprint_donations.route('/changepaymentgateway', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def changepaymentgateway():
    if request.method == 'POST':
        gateway_id = request.values.get('id')
        if gateway_id is None or gateway_id == '':
            return json.dumps({"errors": ['gateway_id missing'], "data": {"status": "0"}})
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("update gateways set status='D' where status='A'")
            cur.execute("update gateways set status='A' where seq_id=%s", (gateway_id,))
            con.commit()
            con.close()
            return json.dumps({"errors": [], "data": {"status": "1"}})
        except  psycopg2.Error as e:
            print str(e)
            return json.dumps({"errors": ['something wrong'], "data": {"status": "0"}})
    else:
        return json.dumps({"errors": ['method not allowed'], "data": {"status": "0"}})

from main import *
@blueprint_donations.route("/recurring_donation",methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def recurring_donation():
        name = request.values.get('name')
        last_name = request.values.get('last_name')
        fathername = request.values.get('fathername')
        email = request.values.get('email')
        mobile = request.values.get('mobile')
        amount = request.values.get('amount')
        address = request.values.get('address')
        pancard = request.values.get('pancard')
        country = request.values.get('country')
        state = request.values.get('state')
      
        remainder = request.form.get('remainder')
        district=request.form.get('district')
        constituency = request.form.get('constituency')
        
        plan_description="JANASENA PARTY DONATIONS"
        amount=request.values.get("amount")
        currency="INR"
        interval=request.values.get("interval")
        period=request.values.get("period")
        plan_name="JSP_RECURRING_PAYMENTS_"+str(period).upper()+"_"+str(amount)
        total_count=50
        amount=int(amount)
        errors = []

        if name is None or name == '':
            errors.append("name should not be empty")
        if last_name is None or last_name == '':
            errors.append("last_name should not be empty")
        if fathername is None or fathername == '':
            errors.append("fathername should not be empty")
        if email is None or email == '':
            errors.append("email should not be empty")
        if mobile is None or mobile == '':
            errors.append("mobile number should not be empty")
        if amount is None or amount == '':
            errors.append("amount should not be empty")

        if address is None or address == '':
            errors.append("address should not be empty")

        if plan_name is None or plan_name == '':
            errors.append("plan_name should not be empty")
        if amount is None or amount == '':
            errors.append("amount should not be empty")
        if period is None or period == '':
            errors.append("period  should not be empty")
        if total_count is None or total_count == '':
            errors.append("total_count should not be empty")
        if constituency is None or constituency=="":
            constituency=0
        if district is None or district=="":
            district=0
        if len(errors):
            
            return json.dumps({'errors': errors, 'status': 0})
        
        
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            
            cur.execute(
                    """insert into janasena_recurring_payments(
                        donar_name,relation_name,email,phone,pancard,
                address,country,state,district_id,
                constituency_id,
                last_name,plan_name,plan_description,plan_amount,
                subscription_total_count,
                plan_period,plan_quantity)
            values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning seq_id""",
                    (name, fathername, email, mobile, pancard, address,  
                    
                     country, state,district,constituency, last_name,plan_name,
                     plan_description,amount,total_count,period,interval))
            con.commit()
            donor_id=cur.fetchone()['seq_id']
            print donor_id
            plan_response=create_rp_plan(period,interval,plan_details={
                "name": plan_name,
                "description": plan_description,
                "amount": int(amount*100),
                "currency": "INR"
              },notes={})
            rsp_plan=json.loads(plan_response.text)
            print rsp_plan
            try:
                if 'id' in rsp_plan.keys():
                    plan_id= rsp_plan["id"]
                    from datetime import datetime
                    
                    start_time = datetime.now() + timedelta(minutes=15)
                    

                    unixtime = int(time.mktime(start_time.timetuple()))
                    
                    
                    subscription_response=create_rp_subscription(plan_id,total_count,unixtime,notes={})
                    subscription_response=json.loads(subscription_response.text)
                    print subscription_response

                    if 'status' in subscription_response.keys():
                        if subscription_response["status"]=='created':
                         
                            subscription_id=subscription_response["id"]
                            plan_id=subscription_response["plan_id"]
                            subscription_start_date=datetime.utcfromtimestamp(subscription_response["start_at"]).strftime('%Y-%m-%d')
                            subscription_end_date=datetime.utcfromtimestamp(subscription_response["end_at"]).strftime('%Y-%m-%d')
                            subscription_status=subscription_response["status"]
                            subscription_paid_count=subscription_response["paid_count"]
                            
                            subscription_next_due=datetime.utcfromtimestamp(subscription_response["charge_at"]).strftime('%Y-%m-%d')
                            subscription_url=""
                            customer_id=""
                            
                            cur.execute("""update janasena_recurring_payments set 
                        subscription_id=%s,plan_id=%s,
                        subscription_start_date=%s,subscription_end_date=%s,
                        subscription_status=%s,subscription_paid_count=%s,
                        subscription_next_due=%s,subscription_url=%s,
                        customer_id=%s where seq_id=%s""",(subscription_id,plan_id,subscription_start_date,
                            subscription_end_date,subscription_status,subscription_paid_count,subscription_next_due,
                            subscription_url,customer_id,donor_id))
                            con.commit()
                            response_data={}
                            response_data['razor_pay_key']=config.razor_pay_key
                            response_data['subscription_id']=subscription_id
                            response_data['plan_name']=plan_name
                            response_data['plan_description']=plan_description
                            response_data['customer_name']=name
                            response_data['customer_email']=email
                            response_data['customer_phone']=mobile
                            response_data['amount']=amount
                            
                            return json.dumps({'errors': [], 'status': 1,'data':response_data})
                            # return render_template("subscription_heckout.html",
                            #   razor_pay_key=config.razor_pay_key,subscription_id=subscription_id,
                            #   plan_name=plan_name,plan_description=plan_description,
                            #   customer_name=name,customer_email=email,customer_phone=mobile,amount=amount)
                    else:
                        print subscription_response['error']['code']
                        print subscription_response['error']['description']
                        return json.dumps({'errors': ['something went wrong in creating your subscription'], 'status': 0})

                else:
                    print rsp_plan['error']['code']
                    print rsp_plan['error']['description']
                    print rsp_plan['error']['field']
                    return json.dumps({'errors': ['something went wrong in creating your subscription'], 'status': 0})
            except Exception as e:
                print str(e)
                return json.dumps({'errors': ['Exception'], 'status': 0})   
                
            #return json.dumps({'errors':[],'status':1,'payment_url':payment_url})
            
 
        except (psycopg2.Error, Exception) as e:
            print str(e)
            
            return json.dumps({'errors': ['Exception'], 'status': 0})

    
    
@blueprint_donations.route("/activate_recurring_donations",methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def activate_recurring_donations():
    subscription_id=request.values.get('subscription_id')
    payment_id=request.values.get('payment_id')
    signature=request.values.get('signature')
    msg=str(payment_id)+"|"+str(subscription_id)
    import hmac
    import hashlib
    import base64
    
    h = hmac.new( config.razor_pay_secret, msg, hashlib.sha256 )
    
    client_signature =h.hexdigest()
    
    if client_signature==signature:
        response=get_subscription_details(subscription_id)
        response=json.loads(response.text)
        subscription_id=response['id']
        customer_id=response['customer_id']
        subscription_url=response["short_url"]
        status=response['status']
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select subscription_status from janasena_recurring_payments where subscription_id=%s ",(subscription_id,))
            existing_status=cur.fetchone()['subscription_status']
            if existing_status in ['authenticated','active','cancelled','completed']:
                return redirect(url_for('.donations'))
            cur.execute("""update janasena_recurring_payments set subscription_url=%s,
                        customer_id=%s,subscription_status=%s,payment_id=%s where subscription_id=%s 
                        returning plan_name,plan_description,
                        donar_name,phone,email,plan_amount,plan_period,plan_id,subscription_id""",
                        (subscription_url,customer_id,status,payment_id,subscription_id))
            con.commit()
            plan_details=cur.fetchone()
            con.close()

            next_due=datetime.utcfromtimestamp(response["charge_at"]).strftime('%d %b %Y')
            plan_details['next_due']=next_due
            if config.ENVIRONMENT_VARIABLE!='local':
                send_recurring_payment_activation_email.delay(plan_details)
            else:
                send_recurring_payment_activation_email(plan_details)
            return render_template("donations/recurring_payment_success.html",data=plan_details)
        except psycopg2.Error as e:
            print str(e)
            return render_template("donations/support.html")
    else:
        return render_template("donations/support.html")

@blueprint_donations.route("/subscription_status_events",methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def subscription_status_events():
    import hmac
    import hashlib
    import base64
    
    response=request.json
    
    if response["event"]=="subscription.charged" :
        
        try:
            subscription_details=response['payload']['subscription']['entity']
            payent_details=response['payload']['payment']['entity']
            subscription_id=subscription_details['id']
            plan_id=subscription_details['plan_id']
            customer_id=subscription_details['customer_id']
            auth_attempts=subscription_details['auth_attempts']
            payment_id=payent_details['id']
            charged_amount=payent_details['amount']/100
            payment_status=payent_details['status']
            order_id=payent_details['order_id']
            invoice_id=payent_details['invoice_id']
            payment_method=payent_details['method']
            payment_captured=payent_details['captured']
            payment_charged_time=subscription_details['created_at']

            card_details=payent_details['card']
            card_id=card_details['id']
            entity=card_details['entity']
            last4=card_details['last4']
            name=card_details['name']
            network=card_details['network']
            emi=card_details['emi']
            type=card_details['type']
            issuer=card_details['issuer']
            international=card_details['international']

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            print " fetching"
            cur.execute("""select to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY.HH12:MI AM') as date,donar_name,last_name,
                        phone,email,address,state,plan_id,plan_name,plan_period,plan_amount,subscription_id
                        from janasena_recurring_payments where subscription_id=%s""",
                        (subscription_id,))
            details=cur.fetchone()
            print details
            if details:
                cur.execute("select * from recurring_payments_charge_details where payment_id=%s",(payment_id,))
                existing_payment=cur.fetchone()
                if existing_payment is None:
                    print "recurring_donation charging"
                    cur.execute("""insert into recurring_payments_card_details(payment_id,customer_id,
                    subscription_id,order_id,
                    card_id,entity,last4,name,network,emai,type,issuer,international)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(payment_id,customer_id,
                    subscription_id,order_id,
                    card_id,entity,last4,name,network,emi,type,issuer,international))
                    con.commit()

                    cur.execute("""insert into recurring_payments_charge_details
                        (subscription_id,plan_id,customer_id,auth_attempts,payment_id,charged_amount,payment_status,
                        order_id,invoice_id, payment_method,
                        payment_captured,payment_charged_time
                        )values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(subscription_id,plan_id,customer_id,auth_attempts,payment_id,charged_amount,payment_status,order_id,invoice_id,payment_method,payment_captured,payment_charged_time))
                    con.commit()
                    email_data=details
                    email_data['payment_id']=payment_id
                    email_data['date']=str(email_data['date'])
                    print "email template triggered"
                    if config.ENVIRONMENT_VARIABLE!='local':
                        send_recurring_donations_ack.delay(email_data)
                    else:
                        send_recurring_donations_ack(email_data)

                    
            con.close()
            return "success"
        except psycopg2.Error as e:
            print str(e)
            return ''
    
    else:
        pass
        return "success"

    
@blueprint_donations.route("/cancel_subscription",methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def cancel_subscription():
    if request.method=='GET':
        plan_id=request.values.get('pid')
        subscription_id=request.values.get('sid')
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from  janasena_recurring_payments where plan_id=%s and subscription_id=%s",(plan_id,subscription_id))
            details=cur.fetchone()
            con.close()
            if details:
                return render_template("donations/cancel_subscription.html")
            else:
                abort(401)
        except psycopg2.Error as e:
            print str(e)
            abort(401)
    else:
        plan_id=request.values.get('pid')
        subscription_id=request.values.get('sid')
        cancel_state=request.values.get('status')

        if plan_id is None:
            return json.dumps({"errors": ['plan_id missing'], "status": "0"})
        if subscription_id is None:
            return json.dumps({"errors": ['subscription_id missing'],  "status": "0"})
        if cancel_state is None:
            return json.dumps({"errors": ['cancel_state missing'],  "status": "0"})

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from  janasena_recurring_payments where plan_id=%s and subscription_id=%s",(plan_id,subscription_id))
            details=cur.fetchone()
            print details
            if details:
                subscription_status=details['subscription_status']
                print "hello"
                print subscription_status
                if subscription_status not in ['cancelled','completed']:
                    response=''
                    if cancel_state=='end':

                        response=cancel_subscriptions(subscription_id,type='cancel_at_cycle_end')
                    else:
                        response=cancel_subscriptions(subscription_id)
                    response=json.loads(response)
                    print response
                    print response.keys()
                    if 'status' in  response.keys():
                        if response['status']=='cancelled':
                            cur.execute("update janasena_recurring_payments set subscription_status='cancelled' where subscription_id=%s returning plan_period,plan_amount,donar_name,email",(subscription_id,))
                            details=cur.fetchone()
                            con.commit()
                            con.close()
                            if config.ENVIRONMENT_VARIABLE!='local':
                                send_subscription_cancel_email.delay(details)

                            return json.dumps({"errors":[],"status":"1","msg":"subscription cancelled successfully"})
                        else:
                            return json.dumps({"errors":[response['error']['description']],"status":"0"})
                    else:
                        return json.dumps({"errors":["your subscription can not be cancelled"],"status":"0"})
                else:
                    return json.dumps({"errors":["your subscription can not be cancelled"],"status":"0"})
            else:

                return json.dumps({"errors":["No subscription found with given details"],"status":"0"})
    
        except psycopg2.Error as e:
            print str(e)
            return json.dumps({"errors":["something went wrong"],"status":"0"})

    


    