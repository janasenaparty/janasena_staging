import PIL
import io
import urllib
from StringIO import StringIO
from copy import deepcopy
from urllib import urlretrieve

import numpy as np
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.lib import colors

from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph

class CustomPara(Paragraph):
    def wrap(self, availWidth, availHeight):
        # work out widths array for breaking
        self.width = availWidth
        style = self.style
        leftIndent = style.leftIndent
        first_line_width = availWidth - (leftIndent+style.firstLineIndent) - style.rightIndent
        later_widths = availWidth - leftIndent - style.rightIndent
        self._wrapWidths = [first_line_width, later_widths]
        if style.wordWrap == 'CJK':
            #use Asian text wrap algorithm to break characters
            blPara = self.breakLinesCJK(self._wrapWidths)
        else:
            blPara = self.breakLines(self._wrapWidths)
        self.blPara = blPara
        length = 4
        autoLeading = getattr(self,'autoLeading',getattr(style,'autoLeading',''))
        leading = style.leading
        if len(blPara.lines) > 4:
            extra = len(blPara.lines) - 4
            blPara.lines = blPara.lines[0:4]
        if blPara.kind==1:
            if autoLeading not in ('','off'):
                height = 0
                if autoLeading=='max':
                    for l in blPara.lines:
                        height += max(l.ascent-l.descent,leading)
                elif autoLeading=='min':
                    for l in blPara.lines:
                        height += l.ascent - l.descent
                else:
                    raise ValueError('invalid autoLeading value %r' % autoLeading)
            else:
                height = length * leading
        else:
            if autoLeading=='max':
                leading = max(leading,blPara.ascent-blPara.descent)
            elif autoLeading=='min':
                leading = blPara.ascent-blPara.descent
            height = length * leading
        self.height = height
        return self.width, height

def shitty_form(name, age, caste, gender, qualification, mobile_no, email, job, ac, pc, jsp_id, voter_id, exp,

                village,

                mandal, image_url,
                dob, skills=[]
                ):
    if mobile_no:
        packet = io.BytesIO()
        can = Canvas(packet, pagesize=A4)
        can.setFillColor(colors.blue)
        can.setFont('Helvetica-Bold', 10)
        ps = ParagraphStyle('title', fontSize=10, leading=27,
                            firstLineIndent=210,textColor = colors.blue
                            )
        # print(ps.listAttrs())

        basewidth = 300
        tick = u"\u2713"


        can.drawString(109.13258826517648, 690, name.upper())

        can.drawString(87.04978409956811, 663, village.upper())
        can.drawString(296.35636271272534, 663, mandal.upper())

        can.drawString(107.21234442468881, 630, dob.upper())
        can.drawString(238.7490474980949, 630, age.upper())
        can.drawString(347.2428244856489, 630, gender.upper())

        can.drawString(132.17551435102865, 600, caste.upper())
        can.drawString(341.4820929641859, 600, qualification.upper())

        can.drawString(88.97002794005584, 568, job.upper())
        can.drawString(352.0434340868681, 568, mobile_no.upper())

        can.drawString(110.09271018542029, 536, email.upper())

        can.drawString(161.9392938785877, 505, ac.upper())

        can.drawString(180.18161036322067, 474, pc.upper())

        can.drawString(143.69697739395474, 443, jsp_id.upper())
        can.drawString(395.2489204978409, 443, voter_id.upper())

        if ('speech' in skills):
            can.drawString(35.2032004064007, 368, tick.upper())
        if ('problems' in skills):
            can.drawString(335.2413004826009, 368, tick.upper())
        if ('house' in skills):
            can.drawString(35.2032004064007, 338, tick.upper())
        if ('law' in skills):
            can.drawString(335.2413004826009, 338, tick.upper())

        if 'social_media' in skills:
            can.drawString(35.2032004064007, 308, tick.upper())
        if 'banners' in skills:
            can.drawString(335.2413004826009, 308, tick.upper())
        if 'content_writer' in skills:
            can.drawString(35.2032004064007, 274, tick.upper())
        if 'leadership' in skills:
            can.drawString(335.2413004826009, 274, tick.upper())

        if 'political_analysis' in skills:
            can.drawString(35.2032004064007, 226, tick.upper())

        if 'cultural_skills' in skills:
            can.drawString(335.2413004826009, 226, tick.upper())

        experi_p = CustomPara("<b>" + exp + "</b>", ps)
        experi_p.wrapOn(can, 530, 10)
        experi_p.drawOn(can, 53.4056388113, 65)

        if (image_url != "" and image_url != "nan"):
            try:
                urlretrieve(image_url, 'temp.jpg')

                img = Image.open('temp.jpg')
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img = img.crop((0, 0, basewidth, 350))

                im = ImageReader(img)

                can.drawImage(im, 462, 535, 102, preserveAspectRatio=True, anchor='c')
            except Exception as e:
                print(str(e))

        can.showPage()
        can.save()

        # Move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # Read your existing PDF
        existing_pdf = PdfFileReader(open("/static/f3.pdf", "rb"))
        output = PdfFileWriter()
        # Add the "watermark" (which is the new pdf) on the existing page
        page = existing_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)
        # Finally, write "output" to a real file
        outputStream = open(mobile_no + '_' + name.split(' ')[0] + '_' + ac + ".pdf", "wb")
        output.write(outputStream)
        outputStream.close()


def read_excel(excel_file):
    import pandas as pd
    df = pd.read_excel(excel_file, sheet_name=0, dtype=str)
    # sheet_1 = df[0]
    df1 = df.replace('NAN', '', regex=True)
    df1 = df1.replace('NaN', '', regex=True)
    df1 = df1.replace(np.NaN, '', regex=True)
    df1 = df1.replace(np.NAN, '', regex=True)
    for index, row in df1.iterrows():
        shitty_form(name=row['Name'],
                    father_husband=row['Father/Husband'] if row['Father/Husband'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                          np.NAN] else '',
                    age=row['Age'] if row['Age'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    caste=row['Community / Caste'] if row['Community / Caste'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                       np.NAN] else '',
                    gender=row['Gender'] if row['Gender'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    qualification=row['Qualification'] if row['Qualification'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                       np.NAN] else '',
                    mobile_no=row['Mobile'] if row['Mobile'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    email=row['Email'] if row['Email'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    job=row['Occupation'] if row['Occupation'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    ac=row['assembly'] if row['assembly'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    pc=row['Parliament'] if row['Parliament'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    jsp_voter_id=str(
                        row['JSP ID'] if row['JSP ID'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '')
                                 + '/' + str(
                        row['Voter ID'] if row['Voter ID'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else ''),
                    exp=row['Experience'] if row['Experience'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    door_no=row['Door No'] if row['Door No'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    street=row['Street'] if row['Street'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    village=row['Village'] if row['Village'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    pincode=row['Pincode'] if row['Pincode'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    ward_no=row['Ward'] if row['Ward'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    mandal=row['Mandal'] if row['Mandal'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                    image_url=str(row['Passport_photo']))



read_excel('rajahmandri.xlsx')
