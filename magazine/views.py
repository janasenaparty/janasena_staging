import json

from flask import Blueprint, request
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp

from appholder import app, csrf

magazine = Blueprint('magazine', __name__)


@magazine.route('/api/get_profile_data', methods=['GET', 'POST', 'OPTIONS'])
@jwt_required()
def get_profile_data():
    data = {
        "name": 'abhinav',
        "email": 'mac.abhinav@gmail.com',
        "phone": '9849191422',
        "address": {
            "house_no": 'plot 71',
            "street_village": 'Prashasan Nagar',
            "city_town": 'Hyderabad',
            "state": 'Telangana',
            'pincode': '500072',
            "landmark": 'Near Andhrabank'
        }
    }
    return json.dumps({'status': 1, 'data': data})


@magazine.route('/api/get_subscriptions', methods=['GET', 'POST', 'OPTIONS'])
@jwt_required()
def get_subscriptions():
    data = [
        {
            'subscription_plan': 1,
            'trasaction_id': 'JSP202991794919064',
            'subscription_end_date': '2019-08-13',
            'id': 4,
            'subscription_start_date': '2018-08-14',
            'quantity': 1
        },
        {
            'subscription_plan': 1,
            'trasaction_id': 'JSP90321542831520',
            'subscription_end_date': '2019-08-13',
            'id': 5,
            'subscription_start_date': '2018-08-14',
            'quantity': 1
        },
        {
            'subscription_plan': 3,
            'trasaction_id': 'JSP210261425221118',
            'subscription_end_date': '2021-08-13',
            'id': 6,
            'subscription_start_date': '2018-08-14',
            'quantity': 1
        }]
    return json.dumps({'status': 1, 'data': data})
