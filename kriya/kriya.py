# coding= utf-8 
# -*- coding: utf-8 -*-
import flask
from flask import request,abort,render_template
import logging
from appholder import csrf,db
from dbmodels import KriyaVolunteers,KriyaMembers,KriyaVolunteerSession,KriyaMemberNomineeDetails,KriyaMemberpaymentDetails
import config
from functools import update_wrapper,wraps
from utilities.general import  upload_image
import psycopg2
from psycopg2.extras import RealDictCursor
# App Specific Imports
import csv
import time, os,  base64, hmac, hashlib, uuid,urllib
import requests
import random
from utilities.decorators import crossdomain
from utilities.classes import encrypt_decrypt
from celery_tasks.sms_email import sendSMS
logger = logging.getLogger(__name__)

kriya = flask.Blueprint('kriya', __name__)

def SendResponse(code=200, error=None, result=None):
    if error is None:
        status = 'success'
        response = result
    else:
        status = 'error'
        response = {'message':error}
    message = {
        "status": status,
        "response": response,
        "status_code": code
    }
    resp = flask.jsonify(message)
    resp.status_code = code
    return resp
import random
import string

from Crypto import Random

def saveKriyaSession(id,mobile,action):
    try:        
        con=None 
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()                        
        
        sessionId = str(base64.b64encode(Random.get_random_bytes(16)))
        
        
        cur.execute("update kriya_volunteer_session set action='E' where kriya_volunteer_id=%s and action=%s",(id,action))

        cur.execute("INSERT INTO kriya_volunteer_session(kriya_volunteer_id, mobile, session_id,action) VALUES (%s,%s,%s,%s);",(str(id),mobile,sessionId,str(action)))                
        con.commit()   
        con.close()
        
        userAndPass = base64.b64encode(str(id)+":"+str(sessionId)).decode("ascii")
        
        
        
        token =  'Basic %s' %  userAndPass 
                                                                
        return token
    except psycopg2.Error as e:
        print(str(e))
        print("Exception in charging user")
        return "FAILED_TO_SAVE_SESSION"



def requires_kriya_auth_token(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        print(auth)        
        if not auth or auth is None:  # no header set
            
            response = flask.jsonify({
                "error": "Secure Token Validation Failed. Please contact support team.",
                "result": None
                
                    })
            response.status_code = 401
            return response
        id=auth.username
        token=auth.password
        
        action='KVL'
        status=KriyaVolunteerSession.query.filter_by(kriya_volunteer_id=id,session_id=token,action=action).first()
        
        if status is None:            
            response = flask.jsonify({
                "error": "Secure Token Validation Failed. Please contact support team.",
                "result": None
                
                    })
            response.status_code = 401
            return response
        return f(*args, **kwargs)
    return decorated



def verifyKriyaOtp(mobile_no,pin,type=None):  

    try:
        con=None 
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        if type:
            cur.execute("select verification_code,user_verify_seq_id from user_verify where user_mobile=%s and verify_status='N' and otp_type=%s order by update_dttm desc limit 1;",(mobile_no,type))
        else:
            cur.execute("select verification_code,user_verify_seq_id from user_verify where user_mobile=%s and verify_status='N'  order by update_dttm desc limit 1;",(mobile_no,))
        row = cur.fetchone()             
        if row is not None:
            user_pin_db = row[0]
            user_verify_seq_id=row[1]

            if str(pin) == str(user_pin_db):
                cur.execute("update user_verify set verify_status = 'Y' where user_verify_seq_id=%s",(user_verify_seq_id,))
                con.commit()
                
                   
                return True 
                                   
            else:
                return False
        return False
        
    except psycopg2.Error as e:
        print(str(e))
        
        return False
############################# kriya members apis ##################################
def genKriyaOtp(umobile,type=None):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        if type is not None:
           cur.execute("select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and otp_type=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",("N",type,umobile,)) 

        else:
            cur.execute("select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",("N",umobile))
        existing_otp=cur.fetchone()
        
        if existing_otp is None:
            from random import randint
            user_pin = randint(100000, 999999)
            
            cur.execute("update user_verify set verify_status='E' where user_mobile=%s and  verify_status='N' and otp_type=%s;",(umobile,type))
            cur.execute("""INSERT INTO user_verify(user_mobile,verification_code,verify_status,otp_type,otp_counter) VALUES (%s,%s,'N',%s,%s);""",(umobile,user_pin,type,1))
            con.commit()
        else:
            otp_counter=existing_otp[2]
            if otp_counter>=5:
                return False
            cur.execute("update user_verify set otp_counter=otp_counter+1 where user_verify_seq_id=%s",(existing_otp[1],))
            con.commit()

            user_pin=existing_otp[0]

        con.close()
        if type=="KV":
            if len(umobile)==10:
                umobile = "91"+str(umobile)
            numberToSend = str(umobile)
            msgToSend = "Hello , OTP for JanasenaParty Kriya APP Login  is: " + str(user_pin) +" - Team JSP."
        

        sendKriyaOTPSMS(numberToSend,msgToSend)
        
        return user_pin
    except psycopg2.Error as e:
        print(str(e))
        con.close()
        return ''

def sendKriyaOTPSMS(numberToSend,msgToSend):
    try:
        url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx"

        querystring = {"User":config.sms_country_user_otp,"passwd":config.sms_country_password_otp,"mobilenumber":numberToSend,"message":msgToSend,"sid":"JAIJSP","mtype":"N","DR":"Y"}
        
       
        response = requests.request("POST", url, headers={}, params=querystring)
        
        
        
    except Exception as e:
        print(str(e))
        return ''

        

@kriya.route("/api/kriya/loginwithotp", methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
def KriyaLoginWithOTP():
    if request.method == 'GET':
        try:

            mobile = request.values.get('mobile')
            imei   =  request.values.get("imei")

            if mobile is None or mobile.isspace():
                return makeResponse(json.dumps({"errors": ["mobile number should not be empty'"]}), 400,
                                    'application/json')
            
            registered_user = KriyaVolunteers.query.filter(mobile==mobile).filter(imei==imei).first()
            if registered_user is not None:                
                otp = genKriyaOtp(mobile, 'KV')
                return makeResponse(json.dumps({"status": "1", "message": "OTP Sent Successfully"}), 200,
                                    'application/json')
            else:

                return makeResponse(json.dumps({"errors": ["No Details Found With This Mobile  "]}), 400,
                                    'application/json')
        except Exception as e:
            logger.error(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/forgotpin", methods=['GET','POST','OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
def kriyaForgotpin():        
    
        try:
            request_data = request.get_json()
            mobile = request_data.get('mobile',None)
            
            if mobile is None or mobile.isspace():
                return SendResponse(400, "Mobile Number Missing", None)
            
            registered_user = KriyaVolunteers.query.filter(KriyaVolunteers.mobile==mobile).first()
            if registered_user is not None:                
                if registered_user.status == 'Active':
                    otp = genKriyaOtp(mobile, 'KV')
                    return SendResponse(200, None, { "message": "OTP Sent Successfully"})
                else:
                    return SendResponse(200, "volunteer is in "+registered_user.status+" state. please contact jsp support team.", { "message": "OTP Sent Successfully"})
                
            else:
                return SendResponse(200, "No Details Found With This Mobile", None)
                
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
            

@kriya.route("/api/kriya/verifyOTP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="*")
@csrf.exempt
def verifyKriyaVolunteerOTP():    
    try:
        
        request_data = request.get_json()
        if request_data is None:
            return SendResponse(400, "post data missing", None)
        mobile = request_data.get('mobile',None)
        otp = request_data.get('otp',None)        
        if mobile is None or mobile.isspace():
            return SendResponse(400, "Mobile number missing", None)
        if otp is None or otp.isspace():
            return SendResponse(400, "OTP missing", None)
        
        registered_user = KriyaVolunteers.query.filter_by(mobile=mobile).first()
        if registered_user is not None:
            
            if verifyKriyaOtp(mobile, otp, "KV"):
                
                details = registered_user                
                profile = {}
                if details.status == 'Active':
                    profile['name'] = details.name
                    profile['state'] = details.state
                    profile['district_id'] = details.district_id
                    profile['constituency_id'] = details.constituency_id
                    profile['mobile'] = details.mobile
                    profile['status'] = details.status
                    profile['id']     = details.id
                    token = saveKriyaSession(profile['id'], mobile, 'KVL')
                    return SendResponse(code=200,error=None,result = {"authentication": "1", "profile": profile, "token": token})
                else:

                    return SendResponse(code=200,error="your access  is in "+registed_user.status+"state . please contact jsp team",result =None)
            return SendResponse(code=200,error="otp verification failed",result=None)

        return SendResponse(code=200,error=" User not found with the given number",result=None)

    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/resetpin", methods=[ 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def kriyaresetpin():
    
    try:
        
        request_data = request.get_json()
        auth = request.authorization
        id = auth.username
        pin = request_data.get('pin',None)
        if pin is None or pin.isspace():
            return SendResponse(400, "PIN missing", None)  
        
        if len(str(pin)) == 6: 
            registered_user = KriyaVolunteers.query.filter_by(id=id).first()

            if registered_user is not None:
                user_pin = encrypt_decrypt(str(pin), "E")
                #user_pin = pin
                registered_user.pin = user_pin
                db.session.commit()
                return SendResponse(200,None,{"status": "1", "message": "reset pin done "})
                
            return SendResponse(200,"user details not found",None)
        else:
            return SendResponse(200,"user pin should be 6 digits",None)


    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/profile", methods=[ 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def kriyaprofile():
    
    try:        
        
        auth = request.authorization
        id = auth.username
        
        registered_user = KriyaVolunteers.query.filter_by(id=id).first()

        if registered_user is not None:
            details = registered_user                
            profile = {}
            profile['name'] = details.name
            profile['state'] = details.state
            profile['district_id'] = details.district_id
            profile['constituency_id'] = details.constituency_id                
            profile['status'] = details.status
            profile['id']     = details.id 
            profile['mobile']     = details.mobile                    
            
            return SendResponse(code=200,error=None,result = {"authentication": "1", "profile": profile, "token": token})
                      
            
        else:
            return SendResponse(200,"user pin should be 6 digits",None)


    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/logout",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@csrf.exempt
@requires_kriya_auth_token
def kriyaMemberLogout():
    try:
        auth = request.authorization
        id=auth.username
        token=auth.password               
        status='KVL'
        status=KriyaVolunteerSession.query.filter_by(kriya_volunteer_id=id,session_id=token,action=status).first()
        if status is not None:
            status.action='E'
            db.session.commit()            
        return SendResponse(code=200,error = None,result = {"message":"Logged Out Successfully"})    
    except (Exception,KeyError) as e:        
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/login",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriyavolunteerlogin():
    if request.method=='POST':
        try:
            request_data = request.get_json()
            mobile=request_data.get('mobile',None)
            pin=request_data.get('pin',None)
            
            if mobile is None or mobile.isspace():
                return SendResponse(code=400,error="Mobile Number missing",result=None)
                
            if pin is None or pin.isspace() :
                return SendResponse(code=400,error="Pin missing",result=None)
            if len(pin)!=6:
                return SendResponse(code=400,error="pin should  be 6 digits only",result=None)
            epin  = encrypt_decrypt(pin,'E')
            registered_user = KriyaVolunteers.query.filter_by(mobile =mobile,pin=epin).first()
            if registered_user is not None:
                details = registered_user 
                if registered_user.status =='Active':            
                    profile = {}
                    profile['name'] = details.name
                    profile['state'] = details.state
                    profile['district_id'] = details.district_id
                    profile['constituency_id'] = details.constituency_id                
                    profile['status'] = details.status
                    profile['id']     = details.id
                    token = saveKriyaSession(profile['id'], mobile, 'KVL')
                    
                    return SendResponse(code=200,error=None,result = {"authentication": "1", "profile": profile, "token": token})
                else:
                    return SendResponse(code=200,error="your access is in  "+registered_user.status+" state. please contact jsp team",result =None)
                
            else:
                return SendResponse(code=200,error="Login failed. please check again",result =None)
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)





@kriya.route("/api/kriya/get_referrals",methods=['POST'])
@crossdomain(origin="*",headers="Content-Type")
@requires_kriya_auth_token
@csrf.exempt
def get_referrals():
    if request.method=='POST':
        try:
            auth = request.authorization
            id=auth.username       
            
            referrals = KriyaMembers.query.filter(KriyaMembers.added_by ==id).all()
            results = []
            for member in referrals:
                
                d={}
                d['name']=member.name
                d['id'] = member.id
                d['mobile'] = member.mobile
                results.append(d)
                
            return SendResponse(code=200,error=None,result=results)
                
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/get_referrals_count",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@requires_kriya_auth_token
@csrf.exempt
def get_referrals_count():
    if request.method=='POST':
        try:
            auth = request.authorization
            id=auth.username       
            
            count = KriyaMembers.query.filter(KriyaMembers.added_by ==id).count()
            
                
            return SendResponse(code=200,error=None,result={"count":count})
                
            
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

import json
@kriya.route("/api/kriya/form_fields",methods=['GET'])
@crossdomain(origin="*",headers="Content-Type")
#@requires_kriya_auth_token
@csrf.exempt
def get_form_fields():
    
        try:  
            formfields="./kriya/form_fields.json"
            json_data = open(formfields).read()

            data = json.loads(json_data)
                           
               
            return SendResponse(code=200,error=None,result=data)
          
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/check_member_status",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@requires_kriya_auth_token
@csrf.exempt
def check_member_status():
    postdata = request.get_json()
    mobile = postdata.get("mobile",None)
    if mobile is None or mobile.isspace():
        return SendResponse(code = 400,error = "Mobile Number missing",result=None)
    if len(mobile)!=10:
        return SendResponse(code = 400,error = "Invalid Mobile Number",result=None)

    member_existence = KriyaMembers.query.filter_by(mobile=mobile).first()
    if member_existence is None:
        existing = False
    else:
        existing = True

    return SendResponse(code = 200,error = None,result={"existing":existing})


def convert_telugu_english(gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles):
    try:
        gender_telugu = ["పురుషుడు","స్త్రీ"]
        time_telugu = ["పూర్తి సమయం","పాక్షిక సమయం"]
        level_telugu = ["రాష్ట్ర స్థాయి ","జిల్లా స్థాయి", "నియోజకవర్గ స్థాయి"]
        gender_english =["Male","Female"]
        time_english = ["Full Time","Part Time"]
        level_english = ["state level","district level","assembly level"]
        know_principles_telugu = ["అవును","కాదు"]
        know_principles_english = ["Yes","No"]

        if gender in gender_telugu:
            index = gender_telugu.index(gender)
            gender = gender_english[index]
        if how_much_time_can_you_spend_for_party_activities in time_telugu:
            index = time_telugu.index(how_much_time_can_you_spend_for_party_activities)
            how_much_time_can_you_spend_for_party_activities = time_english[index]
        activities =[]
        for activity in participated_jsp_activities:
            if activity in level_telugu:
                index = level_telugu.index(activity)
                activities.append(level_english[index])
            else:
                activities.append(activity)
        if do_you_know_janasena_principles in know_principles_telugu:
            index = know_principles_telugu.index(do_you_know_janasena_principles)
            do_you_know_janasena_principles = know_principles_english[index]
        return gender,how_much_time_can_you_spend_for_party_activities,activities,do_you_know_janasena_principles
    except Exception as e:
        print(str(e))
        return gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles






@kriya.route("/api/kriya/add_member",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
#@requires_kriya_auth_token
@csrf.exempt
def add_member():
    
    try:
        auth = request.authorization
        added_by=1        
        member_details = request.get_json()
        if member_details is None:
            return SendResponse(code=400,error="post data missing",result=None)
        
        mobile  =member_details.get('mobile',None)
        name =member_details.get('fullname',None)
        email =member_details.get('email',None)
        state =member_details.get('state',None)
        district_id =member_details.get('district',None)
        constituency_id =member_details.get('constituency',None)       
        
        address =member_details.get('address',None)
        gender =member_details.get('gender',None)
        #dob =member_details.get('dob',None)
        #education =member_details.get('education',None)
        #occupation =member_details.get('occupation',None)
        aadhar_number =member_details.get('aadhar_number',None)
        working_with_janasena_from_year_and_month =member_details.get('journey_year',None)        
        do_you_know_janasena_principles = member_details.get('do_you_know_janasena_principles',None)
        which_principles_are_you_aware_of = member_details.get('which_principles_are_you_aware_of',None)                
        participated_jsp_activities = member_details.get('participated_jsp_activities',None)
        #nominee_mobile = member_details.get("nominee_mobile",None)
        nominee_name = member_details.get("nominee_name",None)
        aadhar_card = member_details.get("nominee_aadhar_number")       
        
        dob = "2020-01-01"
        education ="NA"
        occupation = "NA"
        how_much_time_can_you_spend_for_party_activities = member_details.get('how_much_time_can_you_spend_for_party_activities',None)
        status,errmsg = KriyaMembers.validateUser(name, mobile, email, state, district_id, constituency_id,dob,
            gender,education,occupation,address,working_with_janasena_from_year_and_month,
            do_you_know_janasena_principles,
            which_principles_are_you_aware_of,how_much_time_can_you_spend_for_party_activities,aadhar_number)
        if status is False:
            return SendResponse(code=400,error=errmsg,result=None)
        
        if nominee_name is None or nominee_name.isspace():
            return SendResponse(code = 400,error = "nominee name missing",result=None)
        if aadhar_card is None or aadhar_card.isspace():
            return SendResponse(code = 400,error = "nominee aadhar number missing",result=None)
        try:
            member = KriyaMembers.query.filter_by(mobile=mobile).first()
            if member is None:
                gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles =convert_telugu_english(gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles)

                new_member = KriyaMembers(name=name,mobile=mobile,email=email,gender=gender,dob=dob,address=address,
                    education=education,occupation=occupation,state=state,district_id=district_id,constituency_id=constituency_id,
                    working_with_janasena_from_year_and_month =working_with_janasena_from_year_and_month,
                    do_you_know_janasena_principles = do_you_know_janasena_principles,
                   which_principles_are_you_aware_of =json.dumps(which_principles_are_you_aware_of),participated_jsp_activities=json.dumps(participated_jsp_activities),
                    how_much_time_can_you_spend_for_party_activities=how_much_time_can_you_spend_for_party_activities,added_by=added_by,aadhar_number=aadhar_number)
                db.session.add(new_member)
                db.session.commit()
                user_id = new_member.id
                nominee_details = KriyaMemberNomineeDetails(kriya_member_id=user_id,nominee_name=nominee_name,
                nominee_mobile=" ",aadhar_id = aadhar_card)
                db.session.add(nominee_details)
                db.session.commit()
                

                return SendResponse(code=200, error = None ,result = {"message":"added successfully","id":user_id})
            return SendResponse(code=200, error = "Memebr already registered with the given mobile number" ,result =None)
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
            
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/upload_kriya_members",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
#@requires_kriya_auth_token
@csrf.exempt
def upload_kriya_members():
    
    try:
        data =[]
        added_by=4324        
        with open('./kriya/razole.csv') as f:
            data = [{k: v for k, v in row.items()} for row in csv.DictReader(f, skipinitialspace=True)]
        count =0
        for member_details in data:
            mobile  =member_details.get('mobile',None)
            name =member_details.get('name',None)
            email =''
            state =member_details.get('state',None)
            district_id =member_details.get('district_id',None)
            constituency_id =member_details.get('constituency_id',None)       
            
            address ="NA"
            gender =member_details.get('gender',None)
            
            aadhar_number =member_details.get('aadhar_number',None)
            working_with_janasena_from_year_and_month ='2015'
            do_you_know_janasena_principles = 'yes'
            which_principles_are_you_aware_of = ["1","2"]
            participated_jsp_activities = ['assembly']
            jsp_id =''      
            nominee_name = member_details.get('nominee_name',None)
            aadhar_card = "NA"
            nominee_aadhar_number=member_details.get('nominee_aadhar_number',None)   
            
            dob = "2020-01-01"
            education ="NA"
            occupation = "NA"
            how_much_time_can_you_spend_for_party_activities = member_details.get('how_much_time_can_you_spend_for_party_activities',None)
            status,errmsg = KriyaMembers.validateUser(name, mobile, email, state, district_id, constituency_id,dob,
                gender,education,occupation,address,working_with_janasena_from_year_and_month,
                do_you_know_janasena_principles,
                which_principles_are_you_aware_of,how_much_time_can_you_spend_for_party_activities,aadhar_number)
            if status is False:
                print(mobile)
            
            
            try:
                member = KriyaMembers.query.filter_by(mobile=mobile).first()
                if member is None:
                    gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles =convert_telugu_english(gender,how_much_time_can_you_spend_for_party_activities,participated_jsp_activities,do_you_know_janasena_principles)

                    new_member = KriyaMembers(name=name,mobile=mobile,email=email,gender=gender,dob=dob,address=address,
                        education=education,occupation=occupation,state=state,district_id=district_id,constituency_id=constituency_id,
                        working_with_janasena_from_year_and_month =working_with_janasena_from_year_and_month,
                        do_you_know_janasena_principles = do_you_know_janasena_principles,
                       which_principles_are_you_aware_of =json.dumps(which_principles_are_you_aware_of),participated_jsp_activities=json.dumps(participated_jsp_activities),
                        how_much_time_can_you_spend_for_party_activities=how_much_time_can_you_spend_for_party_activities,added_by=added_by,aadhar_number=aadhar_number,jsp_id=jsp_id)
                    db.session.add(new_member)
                    db.session.commit()
                    user_id = new_member.id
                    nominee_details = KriyaMemberNomineeDetails(kriya_member_id=user_id,nominee_name=nominee_name,
                    nominee_mobile=" ",aadhar_id = aadhar_card,nominee_aadhar_number=nominee_aadhar_number)
                    db.session.add(nominee_details)
                    db.session.commit()
                    print("inserted - "+str(user_id))
                    

                    count =count+1
                else:
                    print(mobile)
            except Exception as e:
                print(str(e))
        print("inserted "+str(count ))       
        return "success"    
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/add_member2",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@requires_kriya_auth_token
@csrf.exempt
def add_member2():
    
    try:
        auth = request.authorization
        added_by=auth.username        
        member_details = request.get_json()
        if member_details is None:
            return SendResponse(code=400,error="post data missing",result=None)
        
        mobile  =member_details.get('mobile',None)
        name =member_details.get('fullname',None)
        email =member_details.get('email',None)
        state =member_details.get('state',None)
        district_id =member_details.get('district',None)
        constituency_id =member_details.get('constituency',None) 
        mandal_id = member_details.get("mandal_id",None)      
        photo_url =   member_details.get('photo_url') 
        aadhar_number = member_details.get('aadhar_number') 
        address =member_details.get('address',None)
        gender =member_details.get('gender',None)
        dob =member_details.get('dob',None)
        education =member_details.get('education',None)
        occupation =member_details.get('occupation',None)
        working_with_janasena_from_year_and_month =member_details.get('journey_year',None)        
        do_you_know_janasena_principles = member_details.get('do_you_know_janasena_principles',None)
        which_principles_are_you_aware_of = member_details.get('which_principles_are_you_aware_of',None)                
        participated_jsp_activities = member_details.get('participated_jsp_activities',None)
        smart_phone_or_feature_phone = member_details.get('smart_phone_or_feature_phone',None)
        aadhar_card =   member_details.get('aadhar_url') 
        if mandal_id is None:
            return SendResponse(code=400,error="mandal_id is missing",result=None)
        if smart_phone_or_feature_phone is None:
            return SendResponse(code=400,error="smart_phone_or_feature_phone is missing",result=None)
        how_much_time_can_you_spend_for_party_activities = member_details.get('how_much_time_can_you_spend_for_party_activities',None)
        status,errmsg = KriyaMembers.validateUser(name, mobile, email, state, district_id, constituency_id,dob,
            gender,education,occupation,address,working_with_janasena_from_year_and_month,
            do_you_know_janasena_principles,
            which_principles_are_you_aware_of,how_much_time_can_you_spend_for_party_activities,aadhar_number)

        if status is False:
            return SendResponse(code=400,error=errmsg,result=None)
        if photo_url is None or photo_url=="":
            return SendResponse(code=400,error="photo url is missing",result=None)
        if aadhar_card is None or aadhar_card=="":
            return SendResponse(code=400,error="aadhar_card is missing",result=None)
        
        try:
            member = KriyaMembers.query.filter_by(mobile=mobile).first()
            if member is None:
                new_member = KriyaMembers(name=name,mobile=mobile,email=email,gender=gender,dob=dob,address=address,
                    education=education,occupation=occupation,state=state,district_id=district_id,constituency_id=constituency_id,
                    working_with_janasena_from_year_and_month =working_with_janasena_from_year_and_month,
                    do_you_know_janasena_principles = do_you_know_janasena_principles,
                   which_principles_are_you_aware_of =json.dumps(which_principles_are_you_aware_of),participated_jsp_activities=json.dumps(participated_jsp_activities),
                    how_much_time_can_you_spend_for_party_activities=how_much_time_can_you_spend_for_party_activities,added_by=added_by,mandal_id=mandal_id,smart_phone_or_feature_phone=smart_phone_or_feature_phone,
                    photo = photo_url,aadhar_number=aadhar_number,aadhar_card=aadhar_card)
                db.session.add(new_member)
                db.session.commit()
                user_id = new_member.id               
                

                return SendResponse(code=200, error = None ,result = {"message":"added successfully","id":user_id})
            return SendResponse(code=200, error = "Memebr already registered with the given mobile number" ,result =None)
        except Exception as e:
            print(str(e))
            return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
            
        
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/v1/masterdata",methods=['GET'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriyamasterdata_v1():
    try:  
            masterdata="./kriya/master_data.json"
            json_data = open(masterdata).read()

            data = json.loads(json_data)                           
               
            return SendResponse(code=200,error=None,result=data)
          
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)


@kriya.route("/api/kriya/v2/masterdata",methods=['GET'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriyamasterdata_v2():
    try:  
            masterdata="./kriya/master_data_v2.json"
            json_data = open(masterdata).read()

            data = json.loads(json_data)                           
               
            return SendResponse(code=200,error=None,result=data)
          
    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@kriya.route("/api/kriya/masterdata",methods=['GET'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriyamasterdata():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data=[]
        cur.execute("select id,name from state where id=1")
        state_data=cur.fetchall()
        for state in state_data:
            d={}
            d['id']=state['id']
            state_id = state['id']
            d['name']=state['name']
            d['districts']=[]
            cur.execute("select seq_id,district_name from districts where state_id=%s",(state['id'],))
            district_rows=cur.fetchall()
            for district in district_rows:
                dr={}
                dr['id']=district['seq_id']
                dr['name']=str(district['district_name']).rstrip()
                dr['constituencies']=[]
                cur.execute("select constituency_id,constituency_name from assembly_constituencies where district_name=%s",(dr['name'],))
                constituency_rows=cur.fetchall()
                for constituency in constituency_rows:
                    cr={}
                    cr['id']=constituency['constituency_id']
                    cr['name']=str(constituency['constituency_name']).rstrip()
                    cr['mandals'] = []
                    cur.execute("select  id,mandal_name from mandals where state_id=%s and constituency_id=%s order by id",(state_id,cr['id']))
                    mandal_rows = cur.fetchall()
                    for mandal in mandal_rows:
                        mr = {}
                        mr['id'] = mandal['id']
                        mr['name'] = mandal['mandal_name']
                        cr['mandals'].append(mr)
                    dr['constituencies'].append(cr)
                d["districts"].append(dr)
            data.append(d)
        return SendResponse(200,None,result = {"data":data})

    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)




@kriya.route("/api/kriya/update_member_nominee_details",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def update_member_nominee_details():
    try:
        postdata = request.get_json()  
        if postdata is None :
            return SendResponse(code = 400,error = "post data  missing",result=None)
        id =postdata.get("id",None)
        mobile = postdata.get("nominee_mobile",None)
        nominee_name = postdata.get("nominee_name",None)
        aadhar_card = postdata.get("nominee_aadhar_number")
        
        if id is None or str(id).isspace():
            return SendResponse(code = 400,error = "id missing",result=None)
        if mobile is None or mobile.isspace():
            return SendResponse(code = 400,error = "Mobile Number missing",result=None)
        if len(mobile)!=10:
            return SendResponse(code = 400,error = "Invalid Mobile Number",result=None)
        if nominee_name is None or nominee_name.isspace():
            return SendResponse(code = 400,error = "nominee name missing",result=None)
        if aadhar_card is None or aadhar_card.isspace():
            return SendResponse(code = 400,error = "nominee aadhar number missing",result=None)
        
        member_details = KriyaMembers.query.filter_by(id=id).first()
        if member_details is not None:
            nominee_details = KriyaMemberNomineeDetails(kriya_member_id=id,nominee_name=nominee_name,
            nominee_mobile=mobile,aadhar_id = aadhar_card)
            db.session.add(nominee_details)
            db.session.commit()
            return SendResponse(code = 200,error = None,result={"message":"nominee details updated"})
        else:   
            return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/update_member_payment_details",methods=['POST'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def update_member_payment_details():
    try:        
        member_id =request.values.get("id",None)
        payment_mode = request.values.get("payment_mode",None)
        transaction_id = request.values.get("transaction_id",None)        
        
        if member_id is None or str(member_id).isspace():
            return SendResponse(code = 400,error = "id missing",result=None)
        if payment_mode is None or payment_mode.isspace():
            return SendResponse(code = 400,error = "payment_mode missing",result=None)
        if transaction_id is None or transaction_id.isspace():
            return SendResponse(code = 400,error = "transaction_id missing",result=None)
        
        member_details = KriyaMembers.query.filter_by(id=member_id).first()
        if member_details is not None:
            if 'payment_receipt' in request.files:
                receipt = request.files['payment_receipt']
                payment_existence = KriyaMemberpaymentDetails.query.filter_by(payment_id=transaction_id).first()
                if payment_existence is None:
                    payment_receipt_url = upload_image(receipt, 'kriyamembers')
                    payment_details = KriyaMemberpaymentDetails( kriya_member_id=member_id,
                        payment_mode=payment_mode,payment_id=transaction_id,payment_url=payment_receipt_url)
                    
                    db.session.add(payment_details)
                    db.session.commit()
                    member_details.payment_status ='Success'
                    db.session.commit()
                    send_kriya_membership_success_message(member_details)

                    return SendResponse(code = 200,error = None,result={"message":"Thank you for your interest in JanaSena Party. Your Membership registration process is completed successfully . You will be contacted by JSP Team soon"})
                return SendResponse(code = 400,error = "transaction id is already mapped to another member. please check and update",result=None)
            return SendResponse(code = 400,error = "payment receipt is missing",result=None)
        return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

@kriya.route("/api/kriya/update_member_payment_details2",methods=['POST',"OPTIONS"])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def update_member_payment_details2():
    try:
        postdata = request.get_json() 
        member_id =postdata.get("id",None)
        payment_mode = postdata.get("payment_mode",None)
        transaction_id = postdata.get("transaction_id",None)        
        receipt_url =  postdata.get("receipt_url",None)
        if member_id is None or str(member_id).isspace():
            return SendResponse(code = 400,error = "id missing",result=None)
        if payment_mode is None or payment_mode.isspace():
            return SendResponse(code = 400,error = "payment_mode missing",result=None)
        if transaction_id is None or transaction_id.isspace():
            return SendResponse(code = 400,error = "transaction_id missing",result=None)
        if receipt_url is None or receipt_url.isspace():
            return SendResponse(code = 400,error = "receipt_url missing",result=None)
        member_details = KriyaMembers.query.filter_by(id=member_id).first()
        if member_details is not None:
            
                payment_existence = KriyaMemberpaymentDetails.query.filter_by(payment_id=transaction_id).first()
                if payment_existence is None:
                    
                    payment_details = KriyaMemberpaymentDetails( kriya_member_id=member_id,
                        payment_mode=payment_mode,payment_id=transaction_id,payment_url=receipt_url)
                    db.session.add(payment_details)
                    db.session.commit()
                    send_kriya_membership_success_message(member_details)

                    return SendResponse(code = 200,error = None,result={"message":"payment details updated"})
                return SendResponse(code = 400,error = "transaction id is already mapped to another member. please check and update",result=None)
        return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

def send_kriya_membership_success_message(member_details):
    try:
        name = member_details.name
        mobile = member_details.mobile
        numberToSend = "91"+str(mobile)
        messageToSend = """ నమస్తే,
  జనసేన పార్టీలో మీ క్రియాశీలక సభ్యత్వ నమోదు ప్రక్రియ విజయవంతంగా పూర్తయింది. జనసేన నాయకత్వంపై మీరు చూపుతున్న ప్రేమాభిమానాలకు ధన్యవాదాలు. పార్టీ కార్యాలయ ప్రతినిధులు త్వరలో మిమ్మల్ని సంప్రదిస్తారు .
  అభినందనలు."""
        sendSMS(numberToSend,messageToSend)
        return

    except Exception as e:
        print(str(e))

@kriya.route("/api/kriya/update_member_files",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def update_member_files():
    try:        
        id = request.values.get('id')    
        
        if id is None or str(id).isspace():
            return SendResponse(code = 400,error = "id missing",result=None)       
        
        member_details =  KriyaMembers.query.filter_by(id=id).first()
        print(request.files)
        if member_details is not None:
            try:
                if 'user_photo' in request.files and 'aadhar_card' in request.files:
                    photo = request.files['user_photo']
                    aadhar_card = request.files['aadhar_card']

                    photo_url = upload_image(photo, 'kriyamembers')
                    aadhar_card_url = upload_image(aadhar_card, 'kriyamembers') 
                    member_details.photo = photo_url
                    member_details.aadhar_card = aadhar_card_url
                    db.session.commit()

                    return SendResponse(code = 200,error = None,result={"message":"files uploaded successfully"})
                return SendResponse(code = 400,error = "files missing",result=None)    
            except Exception as ex:
                print("exception occurd",str(e))
                return SendResponse(code=500,error="something went wrong. please contact support team",result=None)
        return SendResponse(code = 400,error = "member details not found with given id",result=None)
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)

from werkzeug.utils import secure_filename
@kriya.route("/api/kriya/upload_file",methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def update_files():
    try:        
        
        
        if 'kriya_file' in request.files :
            photo = request.files['kriya_file']   

            try:
                filename = secure_filename(photo.filename)
                print(filename)
                if filename is not None and filename!='':
                    photo_url = upload_image(photo, 'kriyamembers')            

                return SendResponse(code = 200,error = None,result={"message":"files uploaded successfully","path":photo_url})
            except Exception as e:
                print(str(e))
                return SendResponse(code = 400,error = "files missing",result=None)    
        return SendResponse(code = 400,error = "files missing",result=None)    
        
    except (Exception,KeyError) as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@kriya.route("/api/kriya/check_volunteer_pin_status",methods=['POST','OPTIONS'])
@crossdomain(origin="*",headers="*")
@csrf.exempt
def check_volunteer_pin_status():
    postdata = request.get_json()
    mobile = postdata.get("mobile",None)
    if mobile is None or mobile.isspace():
        return SendResponse(code = 400,error = "Mobile Number missing",result=None)
    if len(mobile)!=10:
        return SendResponse(code = 400,error = "Invalid Mobile Number",result=None)

    member_existence = KriyaVolunteers.query.filter_by(mobile=mobile).first()
    print(member_existence)
    if member_existence is None:
        return SendResponse(code = 400,error = "No user found with the given mobile number",result=None)
    else:
        d = {}
        d['name'] =member_existence.name
        if(member_existence.pin is None or member_existence.pin==''):
            d['is_pin_set'] = False
        else:
            d['is_pin_set'] = True      

    return SendResponse(code = 200,error = None,result=d)

@kriya.route("/api/kriya/get_educations",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")

@csrf.exempt
def get_educations():
    d= {"education":["None","10th","Intermediate","Bsc/Bcom/BA","Engineering","Medicine","P.G(MCom/M.Sc Etc","PhD"]   }

    return SendResponse(code = 200,error = None,result=d)

@kriya.route("/api/kriya/get_occupations",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="*")

@csrf.exempt
def get_occupations():
    d= {"occupation":["Farmer","Student","Private Employee","Govt Employee","Teacher","Doctor","Engineer","Fisherman","UnEmployed"]   }

    return SendResponse(code = 200,error = None,result=d)


@kriya.route("/api/kriya/membership_qrcode",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def membership_qrcode():
    d = {"qr_code":"https://janasenabackup.blob.core.windows.net/kriya-payments/membership_kriya_sbi_qr_16112020.jpg"}

    return SendResponse(code = 200,error = None,result=d)


@kriya.route("/api/kriya/video_tutorial",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def video_tutorial():
    d = {"tutorial":"https://tinyurl.com/yygzw6on"}

    return SendResponse(code = 200,error = None,result=d)


@kriya.route("/api/kriya/faq",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriya_faq():
    d = {"tutorial":"https://tinyurl.com/y2t67xxc"}

    return SendResponse(code = 200,error = None,result=d)


@kriya.route("/api/kriya/contact",methods=['GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
@csrf.exempt
def kriya_contact():
    d = [{"name":"Pawan","mobile":"6302753644"},{"name":"Anil","mobile":"8106770099"},{"name":"surendra","mobile":"6304900789"}]

    return SendResponse(code = 200,error = None,result=d)


@kriya.route("/api/kriya/kriya_profile", methods=[ 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def kriya_profile_details():
    
    try:        
        
        auth = request.authorization
        id = auth.username
        
        registered_user = KriyaVolunteers.query.filter_by(id=id).first()

        if registered_user is not None:
            details = registered_user                
            profile = {}
            profile['name'] = details.name                      
            
            profile['id']     = details.id 
            profile['mobile']     = details.mobile
            referrals = KriyaMembers.query.filter(KriyaMembers.added_by==id).count()
            
            profile["added"] = referrals 
            
            return SendResponse(code=200,error=None,result = {"authentication": "1", "profile": profile})
                      
            
        else:
            return SendResponse(200,"volunteer details not found",None)

    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@kriya.route("/api/kriya/v1/kriya_profile", methods=[ 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])
@requires_kriya_auth_token
@csrf.exempt
def kriya_profile_details_v1():
    
    try:        
        
        auth = request.authorization
        id = auth.username
        
        registered_user = KriyaVolunteers.query.filter_by(id=id).first()

        if registered_user is not None:
            details = registered_user                
            profile = {}
            profile['name'] = details.name                      
            
            profile['id']     = details.id 
            profile['mobile']     = details.mobile
            referrals = KriyaMembers.query.filter(KriyaMembers.added_by==id).count()
            success = KriyaMembers.query.filter(KriyaMembers.added_by==id).filter(KriyaMembers.payment_status=='Success').count()
            referrals = int(referrals)-int(success)                      
            profile['completed'] = success
            profile['payment_pending']= referrals
            
            return SendResponse(code=200,error=None,result = {"authentication": "1", "profile": profile})
                      
            
        else:
            return SendResponse(200,"volunteer details not found",None)

    except Exception as e:
        print(str(e))
        return SendResponse(code=500,error="something went wrong. please contact support team",result=None)



@kriya.route("/api/kriya/check_member_register_status",methods=['POST'])
@crossdomain(origin="*",headers="*")
@requires_kriya_auth_token
@csrf.exempt
def check_member_register_status():
    postdata = request.get_json()
    mobile = postdata.get("mobile",None)
    if mobile is None or mobile.isspace():
        return SendResponse(code = 400,error = "Mobile Number missing",result=None)
    if len(mobile)!=10:
        return SendResponse(code = 400,error = "Invalid Mobile Number",result=None)

    member_existence = KriyaMembers.query.filter_by(mobile=mobile).first()
    if member_existence is None:
        return SendResponse(code = 200,error = None,result={"registered":False,"prompt":{"telugu":"ఈ నెంబర్ తో ఏ సభ్యత్వం లేదు . ఇప్పుడు సభ్యత్వం తీసుకోవాలి అనుకుంటున్నారా ?","english":"No member registerd with the given mobile number. do you want to continue to register this?"}})
    else:        
        member_id = member_existence.id
        name = member_existence.name
        photo = member_existence.photo
        files_status = True
        payment_status = True
        aadhar_card = member_existence.aadhar_card
        prompt = {}
        member_payment_details = KriyaMemberpaymentDetails.query.filter_by(kriya_member_id=member_id).first()
        if member_payment_details is None:
            payment_status = False
            prompt["telugu"] = "ఈ సభ్యత్వం లో సభ్యత్వ రుసుము చెల్లించలేదు. ఇప్పుడు చేయటానికి సిద్ధంగా ఉన్నారా?"
            prompt["english"] = "Member payment is pending . do you want to  pay now? "

            
        if aadhar_card is None or len(aadhar_card) < 10:
            files_status = False
            telugu_prompt = "ఈ సభ్యత్వం లో ఫోటో ,ఆధార్ కార్డ్ అప్లోడ్ చేయలేదు మరియు సభ్యత్వ రుసుము కూడా చెల్లించలేదు. ఇప్పుడు చేయటానికి సిద్ధంగా ఉన్నారా?"
            english_prompt = "Member photo , aadhar card files and payment details are pending . do you want to complete those now?"
            prompt["telugu"] = telugu_prompt
            prompt["english"] = english_prompt    
        if payment_status and  files_status:
            prompt["telugu"] = "ఈ నెంబర్ తో సభ్యత్వం పూర్తి అయ్యింది ."
            prompt["english"] = "Membership successfully completed with this mobile number" 


        return SendResponse(code = 200,error = None,result={"registered":True,"name":name,"prompt":prompt,"id":member_id,"files_status":files_status,"payment_status":payment_status})


from utilities.general import decode_hashid
@kriya.route("/kriyamemberstatus/<hashid>", methods=['GET'])

@crossdomain(origin="*", headers="Content-Type")
def kriyamemberstatus(hashid):  
    try: 
        print(hashid)
        if hashid is None or hashid == "":
            return render_template("membership/oops.html")
        print(hashid)
        member_id=decode_hashid(hashid)
        print(member_id)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select km.id,km.name,km.photo,ac.constituency_name,km.jsp_id from kriya_members km join assembly_constituencies ac on km.constituency_id = ac.constituency_id and ac.state_id=1 where km.id=%s",(member_id,))
        details = cur.fetchone()
        print(details)
        if details is not None:
            try:
                name = details['name']
                photo = details["photo"]
                jsp_id = details["jsp_id"]
                constituency_name = details['constituency_name']
                return render_template("membership/kriya_members_qr_code_details.html", jsp_id=jsp_id, name=name,
                                        photo=photo, assembly=constituency_name)
            except Exception as e:
                print str(e)
                return render_template("membership/oops.html")
        else:
           
            return render_template("membership/oops.html")
    except Exception as ex:
        # print(str(e))
        return render_template("membership/oops.html")

