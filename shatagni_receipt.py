from copy import deepcopy
from reportlab.graphics.shapes import Image
from reportlab.lib.colors import red
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph, Table
import tempfile
import os

def subs_receipt(data):
    receipt_no=data['transaction_id']
    receipt_dt=data['date']
    uname=data['name']
    address=data['address']
    phone=data['phone']
    subs_text="1Year or 24 Issues"
    amount=data["transaction_amount"]

    handle, pdffilepath = tempfile.mkstemp(suffix='.pdf')
    
    c = Canvas(pdffilepath)

    default_top_height = A4[1] - 50


    user_image = ImageReader('./static/img/jenasena_logo.png')
    c.drawImage(user_image, 40,default_top_height-150, 100, 140,mask='auto')



    styleSheet = getSampleStyleSheet()
    p_heading_style = deepcopy(styleSheet['BodyText'])
    p_heading_lower_style = deepcopy(styleSheet['BodyText'])
    receipt_name_style = deepcopy(styleSheet['BodyText'])
    receipt_number_style = deepcopy(styleSheet['BodyText'])
    receipt_no_date_style = deepcopy(styleSheet['BodyText'])
    table_pg_heading = deepcopy(styleSheet['BodyText'])
    table_pg_data = deepcopy(styleSheet['BodyText'])

    p_heading_style.alignment = TA_CENTER
    p_heading_style.textColor = red
    p_heading_style.fontSize = 30

    p_heading_lower_style.alignment = TA_CENTER
    p_heading_lower_style.fontSize = 14

    receipt_name_style.fontSize = 25
    receipt_name_style.alignment = TA_CENTER

    receipt_no_date_style.fontSize = 14
    receipt_no_date_style.alignment = TA_LEFT

    table_pg_heading.fontSize = 14
    table_pg_data.fontSize = 14
    table_pg_heading.alignment = TA_LEFT
    table_pg_data.alignment = TA_LEFT




    party_heading = Paragraph('JANASENA PARTY',p_heading_style)
    part_lower = Paragraph('1-90/1, Plot No.20, Kavuri Hills, Madhapur - Hitec City, Hyderabad, 500081',p_heading_lower_style)
    receipt_name = Paragraph('Subscription Receipt',receipt_name_style)

    party_heading.wrapOn(c, A4[0], A4[1])
    party_heading.drawOn(c, 0, default_top_height - 20)

    part_lower.wrapOn(c,  A4[0]-200, A4[1]-150-80)
    part_lower.drawOn(c, 140,default_top_height-70)

    receipt_name.wrapOn(c, A4[0], A4[1])
    receipt_name.drawOn(c, 0, default_top_height - 150)

    receipt_number = Paragraph('<b>Receipt No. :</b> '+str(receipt_no),receipt_no_date_style)
    receipt_number_date = Paragraph('<b>Date :</b> '+str(receipt_dt),receipt_no_date_style)

    receipt_number.wrapOn(c, A4[0]/2, A4[1]/3)
    receipt_number.drawOn(c, 40, default_top_height - 220)

    receipt_number_date.wrapOn(c, A4[0] / 2, A4[1]/3)
    receipt_number_date.drawOn(c, A4[0] -160, default_top_height - 220)

    table_data = [
        [Paragraph('<b>Name</b',table_pg_heading),Paragraph(uname,table_pg_data)],
        [Paragraph('<b>Address</b',table_pg_heading),Paragraph(address,table_pg_data)],
        [Paragraph('<b>Phone</b',table_pg_heading),Paragraph(phone,table_pg_data)],
        [Paragraph('<b>Amount:</b', table_pg_heading), Paragraph('Rs.' + str(amount), table_pg_data)],
        [Paragraph('<b>Purpose:</b', table_pg_heading), Paragraph(subs_text, table_pg_data)],

    ]
    table_styles = [
        ('LEFTPADDING',(0,0),(-1,-1),12),
        ('RIGHTPADDING',(0,0),(-1,-1),12),
        ('TOPPADDING',(0,0),(-1,-1),12),
        ('BOTTOMPADDING',(0,0),(-1,-1),12),
        ('VALIGN',(0,0),(-1,-1),'TOP'),
    ]
    tab = Table(table_data)
    tab.setStyle(table_styles)
    tab.wrapOn(c,A4[0]-80,A4[1])
    tab.drawOn(c,40,default_top_height - 500),
    c.save()

    return pdffilepath

