# coding= utf-8 
# -*- coding: utf-8 -*-
import config

import os
from flask import Flask, request, flash, redirect, url_for, render_template, abort, Response, json,jsonify,make_response,current_app, g,session,send_from_directory


import simplejson as json
import uuid
import time, os,  base64, hmac, hashlib, uuid,urllib

import requests
import psycopg2
from psycopg2.extras import RealDictCursor
from instamojo_wrapper import Instamojo

from Crypto import Random

from datetime import datetime,timedelta
import sys

def remove_trailing_spaces(str):
    try:
        str = re.sub(r'\s+$', '', str)
    except Exception as e:
        print str(e)
    return str
import random
def uniqueid():
    seed = random.getrandbits(32)
    while True:
       yield seed
       seed += 1

unique_sequence_orderId = uniqueid()

def generateOrderNumber():
    try:
          
        randomSeq = str(uuid.uuid4().fields[-1])[:5]
        prefix = "JSP"
        trn_num = str(next(unique_sequence_orderId))
        print trn_num
        orderId_final = prefix + ""+str(randomSeq)+""+str(trn_num)
        
        return orderId_final
        
    except Exception as e:
        print  str(e)
        return ''
def create_RPorderId(amount,transaction_id,currency):
    

    client=config.client
      
    notes={"transaction_id":transaction_id}
    response=client.order.create({'amount':amount, 'currency':currency, 'receipt':transaction_id, 'notes':notes})
    print 'doneee'
    print response
    if response['status']=='created':
        return response['id']
    else:
        return ''

# instamojo order id
def create_IMorderId(amount,phone,email):
    Insta_Mojo_Client=config.IMClient
    response = Insta_Mojo_Client.payment_request_create(
    amount=amount,
    purpose="Janasena Party donations",
    send_email='false',
    send_sms='false',
    email=email,
    buyer_name="Janasena Party",
    phone=phone,
    redirect_url="https://nivedika.janasenaparty.org/donationResponse"
    #redirect_url="https://www.janasenaparty.org/donationResponse"
    )
    print response

    
    
    return response
## creating razorpay recurring payment plans function
def create_rp_plan(period,interval,plan_details={},notes={}):
    
    import requests

    headers = {
    'Content-Type': 'application/json',
    }

    data = {"period": period,  "interval": interval,  "item":plan_details,   "notes": notes}
    
    response = requests.post('https://api.razorpay.com/v1/plans', headers=headers, data=json.dumps(data), auth=(config.razor_pay_key, config.razor_pay_secret))
    
    return response

def create_rp_subscription(plan_id,total_count,start_date,notes={}):
    import requests

    headers = {
        'Content-Type': 'application/json',
    }

    data = {  "plan_id": plan_id,  "total_count": total_count,  "start_at": start_date, "addons": [], "notes":notes, "customer_notify":"1"}    

    response = requests.post('https://api.razorpay.com/v1/subscriptions', headers=headers, data=json.dumps(data), auth=(config.razor_pay_key, config.razor_pay_secret))
    return response
def get_subscription_details(subscription_id):
    import requests
    url="https://api.razorpay.com/v1/subscriptions/"+subscription_id
    response = requests.get(url, auth=(config.razor_pay_key, config.razor_pay_secret))
    return response
def get_card_details(payment_id):
    import requests
    
    url="https://api.razorpay.com/v1/payments/"+payment_id+"/card"
    response = requests.get(url, auth=(config.razor_pay_key, config.razor_pay_secret))
    return response.text
def cancel_subscriptions(subscription_id,type=None):
    import requests
    if type:
        data={"cancel_at_cycle_end": 1}
    else:
        data={}
    headers = {
        'Content-Type': 'application/json',
    }
    url="https://api.razorpay.com/v1/subscriptions/"+subscription_id+"/cancel"
    response = requests.post(url,headers=headers, data=json.dumps(data), auth=(config.razor_pay_key, config.razor_pay_secret))
    print response.text
    return response.text
    
def create_NRI_IMorderId(amount,phone,email):
    Insta_Mojo_Client=config.IMClient
    response = Insta_Mojo_Client.payment_request_create(
    amount=amount,
    purpose="Janasena Party donations",
    send_email='false',
    send_sms='false',
    email=email,
    buyer_name="Janasena Party",
    phone=phone,
    redirect_url="https://nivedika.janasenaparty.org/nri_donation_response"
    #redirect_url="https://www.janasenaparty.org/nri_donation_response"
    )
    print response

    return response

def create_file(path,extension):
    try:
        name=path+"."+extension
        file=open(name,'a')

        file.close()
        return name
    except:
        print("error occured")
        sys.exit(0)
from sys import argv
import httplib2
import simplejson as json
def shurl(longUrl):

    API_KEY = 'AIzaSyAcEBr-GJH6nbur55WiMfNPs25EieRUr8g'
    
    apiUrl = 'https://www.googleapis.com/urlshortener/v1/url?key=%s' % API_KEY
    
    headers = {"Content-type": "application/json"}
    data = {"longUrl": longUrl}
    h = httplib2.Http()
    try:
        headers, response = h.request(apiUrl, "POST", json.dumps(data), headers)
        
        short_url = json.loads(response)['id']
        return short_url
    except Exception as e:
        
        return longUrl
    
def generateLink(membership_id,pagelink,url,querystring):
    try:        
        
                        
        history_uuid = str(uuid.uuid4())
        hasheduuid = hashlib.sha512(membership_id).hexdigest()
        
        linkForReset = str(url)+str(pagelink)+""+str(querystring)+"="+ str(hasheduuid)
        return linkForReset
    except Exception as e:
        print str(e)
        print "Exception in generating link"
        return str(e)
def insert_temporary_details(d):
    try:
        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        cur.execute("delete from temp_membership_details where voter_id=%s  ",(d['voter_id'],))
        cur.execute("insert into temp_membership_details(name,voter_id,phone,age,gender,district,email,referral_id,constituency,verify_status)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning serial_no",(d['name'],d['voter_id'],d['phone'],d['age'],d['gender'],d['district'],d['email'],d['referral_id'],d['constituency'],'N'))
        seq_id=cur.fetchone()
        con.commit()
        con.close()
        return True


    except psycopg2.Error as e:
        print str(e)
        return False

def sendbulkemails(name, to, message, subject, recipient_variables, attachments=None):

        name = "JANASENA PARTY"

        import requests
        try:

            url = 'https://api.mailgun.net/v3/' + config.MAILGUN_DOMAIN_NAME + '/messages'
            auth = ('api', config.MAILGUN_API_KEY)
            data = {
                'from': name + '<janaswaram@janasenaparty.org>',
                'to': to,
                'subject': subject,
                'html': message

            }

            files = None
            if attachments:
                files = {}
                count = 0
                for attachment in attachments:
                    with open(attachment, 'rb') as f:
                        files['attachment[' + str(count) + ']'] = (os.path.basename(attachment), f.read())
                    count = count + 1
            if recipient_variables:
                data['recipient-variables'] = (json.dumps(recipient_variables))

            response = requests.post(url, auth=auth, data=data, files=files)
            response.raise_for_status()
            print response
            return True
        except Exception as e:
            print str(e)
            return True

def find_placeholders(message):

    try:
        import re
        field_names=re.findall(r"{{(\w+)}}", message)
        print field_names
        return field_names

    except Exception as e:
        print str(e)
        return []

def delete_session():
    session.pop('umobile',None)
    session.pop('country_name',None)
    session.pop('voter_id',None)
    session.pop("member_details",None)
    session.pop('membership_id',None)
    session.pop("card_details",None)
    session.pop("img_url",None)
    return 

def verifyOtp(mobile_no,pin,type=None):
   

    try:
        con=None 
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        if type:
            cur.execute("select verification_code,user_verify_seq_id from user_verify where user_mobile=%s and verify_status='N' and otp_type=%s order by update_dttm desc limit 1;",(mobile_no,type))
        else:
            cur.execute("select verification_code,user_verify_seq_id from user_verify where user_mobile=%s and verify_status='N'  order by update_dttm desc limit 1;",(mobile_no,))
        row = cur.fetchone()             
        if row is not None:
            user_pin_db = row[0]
            user_verify_seq_id=row[1]

            if str(pin) == str(user_pin_db) :
                cur.execute("update user_verify set verify_status = 'Y' where user_verify_seq_id=%s",(user_verify_seq_id,))
                con.commit()

                return True 
                                   
            else:
                return False
        return False
        
    except psycopg2.Error as e:
        print str(e)
        
        return False
def get_constituencies(district):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        cur.execute("select constituency_name from assembly_constituencies where lower(district_name)=%s",(district.lower(),))
        rows=cur.fetchall()
        assemblies=[]
        for row in rows:
            assemblies.append(row[0].rstrip())

        return assemblies

    except Exception as e:
        print str(e)
        return "False"

def sendmailmandrill(name,to,message,subject,attachments=None):
        
        name = "JANASENA PARTY"
        
        import requests
        try:
                
            url = 'https://api.mailgun.net/v3/'+config.MAILGUN_DOMAIN_NAME+'/messages'
            auth = ('api', config.MAILGUN_API_KEY)
            data = {
                    'from': name+'<janaswaram@janasenaparty.org>',
                    'to': to,
                    'subject': subject,
                    'html': message
                 
             }
             
            files = None      
            if attachments:
                files = {}
                count=0
                for attachment in attachments:
                    with open(attachment,'rb') as f:
                        files['attachment['+str(count)+']'] = (os.path.basename(attachment), f.read())    
                    count = count+1

            response = requests.post(url, auth=auth, data=data,files=files)
            response.raise_for_status()
            
            return True
        except Exception as e:
            print str(e)
            return True
def genHash(password,unique):
   
    key = hashlib.sha256(password+unique).hexdigest()
    return key

def get_phonenumber(membershipid):
    try:
        seq_id=str(membershipid)[3:11]
        seq_id = seq_id.lstrip("0")
        print seq_id
        con=None 
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone from janasena_missedcalls where jsp_supporter_seq_id=%s",(seq_id,))
        row=cur.fetchone()
        con.close()
        print row
        if row is not None:
            mobile=row['phone']
            #length=len(str(mobile))
            # if length==12:
            #     mobile=mobile[2:12]
            #     print mobile
            # elif length>12 and length<=15:
            #     mobile=mobile
            # elif length==10:
            #     mobile=mobile
            # else:
            #     return (False,'')
            return (True,mobile)
        return (False,'')

    except (psycopg2.Error,Exception) as e:
        print str(e)
        con.close()
        return (False,'')
def saveSession(membership_id,mobile_no,action):
    try:        
        con=None 
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
                         
        
        sessionId = str(base64.b64encode(Random.get_random_bytes(16)))
        
        cur.execute("update member_session set action='E' where membership_id=%s and action=%s",(str(membership_id),action))

        cur.execute("INSERT INTO member_session(membership_id, phone, session_id,action) VALUES (%s,%s,%s,%s);",(str(membership_id),mobile_no,sessionId,str(action)))                
        con.commit()   
        con.close()
        
        userAndPass = base64.b64encode(str(membership_id)+":"+str(sessionId)).decode("ascii")
        token =  'Basic %s' %  userAndPass 
                                                                
        return token
    except psycopg2.Error as e:
        print str(e)
        print "Exception in charging user"
        return "FAILED_TO_SAVE_SESSION"
def send_donation_receipt(payment):

    message=open("./templates/donations/email_txt.html").read()
    temp_text=str(message)

    temp_text=temp_text.replace("{{donar_name}}",str(payment['donar_name']))
    temp_text=temp_text.replace("{{date}}",str(payment['donar_name']))
    temp_text=temp_text.replace("{{address}}",str(payment['address']))
    temp_text=temp_text.replace("{{transaction_payment}}",str(payment['transaction_amount']))
    temp_text=temp_text.replace("{{transaction_id}}",str(payment['transaction_id']))

    
    subject="Thanks! Your Donation Receipt"
    sendmailmandrill("Janasena Party",payment["email"],str(temp_text),subject)

    return ""
import PIL
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

import tempfile

from os import remove


def draw_image(data):
    img_path = "./static/img/donation_certificate.png"
    name = str(data['donar_name']).upper()
    x_position = 800
    if len(name):
        x_position = 1055 - (((len(name) * 35)) / 2)
    print x_position
    img = Image.open(img_path)
    amount = str(data['transaction_amount'])
    amount = unicode(u"\u20B9") + " " + str(amount)
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype("./static/assets/fonts/BebasNeue Bold.otf", size=83)
    draw.text((x_position, 420), name, "#d81615", font=font)
    if len(amount):
        a_x_position = 1055 - (((len(amount) * 35)) / 2)
    draw.text((a_x_position, 630), amount, "#d81615", font=font)
    font1 = ImageFont.truetype("./static/assets/fonts/himalayan.ttf", size=66)
    date = data['date']
    
    if len(date):
        d_x_position = 553 - (((len(date) * 22)) / 2)
    draw.text((d_x_position, 1033), date, "black", font=font1)
    
    if len(data['transaction_id']):
        tx_x_position = 1530 - (((len(data['transaction_id']) * 22)) / 2)
    draw.text((tx_x_position, 1033), data['transaction_id'], "black", font=font1)
    handle, filepath = tempfile.mkstemp(suffix='.png', prefix=data['transaction_id'])
    img.save(filepath)
    img.close()

    return filepath

class Pdf():

    def render_pdf(self, name, html):

        from xhtml2pdf import pisa
        from StringIO import StringIO
        print 'cp3'
        pdf = StringIO()

        pisa.CreatePDF(StringIO(html), pdf,encoding='utf-8')
        print 'cp4'
        return pdf.getvalue()

import tempfile
from flask import make_response    
def get_pdf_receipt(data):
    try:
        html = render_template(
            'donations/email_pdf_receipt.html',payment=data)

        file_class = Pdf()
        pdf = file_class.render_pdf('janasenaparty', html)

        handle, filepath = tempfile.mkstemp(suffix='.pdf',prefix=data['transaction_id'])
        
        with open(filepath, 'wb') as f:
            f.write(pdf)
        return filepath
    except Exception as e:
        print str(e)
        return False
import pdfkit
def get_aspirant_pdf_receipt(data,parliament):
    try:
        options = {
    'page-size': 'Letter',
    'margin-top': '0in',
    'margin-right': '0.5in',
    'margin-bottom': '0in',
    'margin-left': '0.5in',
    'encoding': "UTF-8",
    'dpi':'72',
    'no-outline': None
   
        }
       
        
        html = render_template('admin/aspirant_pdf.html',data=data)
        
        fname=data['name'].split(" ")[0]
        try:
            pdf_file_name="jsp_"+data['mobile']+".pdf"
        except Exception as e:
            pdf_file_name="jsp_"+data['mobile']+".pdf"
        path="C:/Users/Srini1/Desktop/aspirant_pdfs/"+parliament+"/"
        if not os.path.exists(path):
            os.makedirs(path)

        # if data['further_process']=='further process':
        #     path="e:/aspirant_pdfs/fp/"
        # elif data['further_process']=='may be further process':
        #     path="e:/aspirant_pdfs/mbfp/"
        # else:
        #      path="e:/aspirant_pdfs/general/"

        file_path=path+pdf_file_name
        pdfkit.from_string(html, file_path,options=options)
        print file_path +" completed"
        return 'success'
        
        
    except Exception as e:
        print str(e)
        return False
def get_nri_pdf_receipt(data):
    try:
        html = render_template(
            'donations/nri_pdf_receipt.html',payment=data)

        file_class = Pdf()
        pdf = file_class.render_pdf('janasenaparty', html)
        handle, filepath = tempfile.mkstemp(suffix='.pdf',prefix=data['transaction_id'])
        with open(filepath, 'wb') as f:
            f.write(pdf)
        return filepath
    except Exception as e:
        print str(e)
        return False


# mg = Image.open("sample_in.jpg")
# draw = ImageDraw.Draw(img)
# # font = ImageFont.truetype(<font-file>, <font-size>)
# font = ImageFont.truetype("sans-serif.ttf", 16)
# # draw.text((x, y),"Sample Text",(r,g,b))
# draw.text((0, 0),"Sample Text",(255,255,255),font=font)
# img.save('sample-out.jpg')



    #response['Content-Disposition'] = 'attachment; filename=photo.jpg'
    

# def html_pdf(file):
#     options = {
#     'page-size': 'Letter',
#     'margin-top': '0.75in',
#     'margin-right': '0.75in',
#     'margin-bottom': '0.75in',
#     'margin-left': '0.75in',
#     'encoding': "UTF-8",
#     'custom-header' : [
#         ('Accept-Encoding', 'gzip')
#     ]
#     'cookie': [
#         ('cookie-name1', 'cookie-value1'),
#         ('cookie-name2', 'cookie-value2'),
#     ],
#     'no-outline': None
#     }

#     pdfkit.from_file(file, 'out.pdf',options=options)
