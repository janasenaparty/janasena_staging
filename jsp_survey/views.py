import base64
import datetime
import json
from copy import deepcopy
from pprint import pprint
from random import Random

import bson
import pytz
from Crypto.Random import get_random_bytes
from bson import ObjectId
from dateutil import parser
from flask import Blueprint, request, jsonify
from psycopg2.extras import RealDictCursor
from pymongo.errors import BulkWriteError

from appholder import csrf, db
from config import MONGO_URL, MONGO_PORT, VOTERS_TABLE

from constants import empty_check
from dbmodels import Memberships, BisSession, JspsurveySession
from utilities.decorators import crossdomain, requires_authToken, requires_bisToken, requires_jspsurveyToken
from utilities.others import give_cursor_json
from pymongo import MongoClient, InsertOne, UpdateOne, ASCENDING, DESCENDING

jsp_survey = Blueprint('jsp_survey', __name__)


#functions
class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
epoch = datetime.datetime.utcfromtimestamp(0)

def unix_time_millis(dt):
    return int((dt - epoch).total_seconds() * 1000.0)


@jsp_survey.route('/jsp_survey/login', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def login():
    def temp(con, cur):
        if request.method == 'POST':
            try:

                membership_id = request.json['membership_id']
                pin = request.json['pin']
            except (Exception, KeyError) as e:
                print str(e)
                return jsonify({"errors": ['parameter ' + str(e) + " missing"], 'data': {"status": '0'}}), 400

            conn = MongoClient(MONGO_URL, MONGO_PORT)
            mndb = conn.jsp_survey
            mem_assign = mndb['Admins']
            d = {
                'MembershipID':membership_id.upper(),
                'PIN':pin
            }
            member = mem_assign.find_one(d)


            if membership_id is None or membership_id.isspace():
                return jsonify({"errors": [' membership_id should not be empty'], 'data': {"status": '0'}}), 400


            if member is not None:
                sessionId = str(base64.b64encode(get_random_bytes(16)))
                print sessionId
                userAndPass = base64.b64encode(str(membership_id) + ":" + str(sessionId)).decode("ascii")
                token = 'Basic %s' % userAndPass
                a = JspsurveySession()
                a.session_val = sessionId
                a.admin_id = str(membership_id)
                a.status='L'
                db.session.add(a)
                db.session.commit()
                profile = dict()
                profile['state_id'] = member['StateId'][0]['id']
                profile['constituency_name'] = member['ConstituencyId'][0]['ConstituencyName']
                profile['constituency_id'] = member['ConstituencyId'][0]['ConstituencyId']
                profile['district_name'] = member['DistrictId'][0]['DistrictName']
                profile['district_id'] = member['DistrictId'][0]['DistrictId']
                profile['booth_id'] = member['BoothId'][0]['PollingStationNumber']
                profile['booth_name'] = member['BoothId'][0]['PollingStationName']
                profile['age'] = member['Age']
                profile['pin'] = member['PIN']
                profile['name'] = member['Name']

                cur.execute("""
                    select parliament_constituency_name
                    from assembly_constituencies
                    where constituency_id=%s and state_id=%s
                """, (profile['constituency_id'],profile['state_id']))
                parliament_constituency = cur.fetchone()[0]
                profile['parliment_constituency']  = parliament_constituency

                cur.execute("""
                                   select main_town
                                   from polling_station_details
                                   where constituency_id=%s and state_id=%s
                               """, (profile['constituency_id'], profile['state_id']))

                village = cur.fetchone()[0]
                profile['village'] = village

                return json.dumps({"errors": [], "data": {"status": '1', "children": profile, "token": token,
                                                          "is_booth_incharge": 'Y'}})
            else:
                return jsonify({"errors": ['it seems you have not provided details yet. please provide'],
                                'data': {'status': '0'}}), 200

    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/parties', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def parties_table():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('parties_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.parties
            # Get the related collections
            sync_collection = db.bis_sync_time
            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','name','telugu_name'],sort=[('_id',ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['last_updated'],sort=[('last_updated',DESCENDING)])
                # dict_cur.execute("""
                #     select id,name,short_code
                #     from political_parties
                #     order by id
                # """)
                # rows = dict_cur.fetchall()
                # dict_cur.execute("""
                #                     select EXTRACT(EPOCH FROM date_trunc('second', update_dttm)) as update_dttm
                #                     from political_parties
                #                     order by update_dttm desc limit 1
                #                 """)
                #
                # row = dict_cur.fetchone()
                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['last_updated'])
            else:
                d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'last_updated':{"$gt": d}}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['last_updated'], sort=[('last_updated', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['last_updated'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':
            party_name = request.values.get('party_name')
            party_short_code = request.values.get('short_code')
            if party_name in empty_check:
                return json.dumps({'status': 0, 'errors': ['party_name missing']})
            if party_short_code in empty_check:
                return json.dumps({'status': 0, 'errors': ['short_code missing']})
            party_name = party_name.strip()
            party_short_code = party_short_code.strip()
            cur.execute("""
                select * 
                from political_parties
                where name=%s
            """, (party_name,))
            row = cur.fetchone()
            if row:
                return json.dumps({'status': 0, 'errors': ['similar party_name already exists']})
            cur.execute("""
                            select * 
                            from political_parties
                            where short_code=%s
                        """, (party_short_code,))
            row = cur.fetchone()
            if row:
                return json.dumps({'status': 0, 'errors': ['similar short_code already exists']})
            cur.execute("""
                insert into political_parties(name,short_code)
                values(%s,%s)
                returning id
            """, (party_name, party_short_code))
            con.commit()
            id = cur.fetchone()[0]
            return json.dumps({'status': 1, 'data': {'id': id}})

        elif request.method == 'PATCH':
            id = request.values.get('id')
            party_name = request.values.get('party_name')
            party_short_code = request.values.get('short_code')
            if id in empty_check:
                return json.dumps({'status': 0, 'errors': ['id missing']})
            if party_name in empty_check:
                return json.dumps({'status': 0, 'errors': ['party_name missing']})
            if party_short_code in empty_check:
                return json.dumps({'status': 0, 'errors': ['short_code missing']})
            party_name = party_name.strip()
            party_short_code = party_short_code.strip()
            cur.execute("""
                            update political_parties
                            set name=%s,short_code=%s
                            where id=%s
                        """, (party_name, party_short_code, id))
            con.commit()
            return json.dumps({'status': 1})

    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/caste', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def caste():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('community_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.castes
            # Get the related collections
            sync_collection = db.bis_sync_time
            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','name','telugu_name'],sort=[('_id',ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['update_dttm'],sort=[('update_dttm',DESCENDING)])

                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            else:
                d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'update_dttm':{"$gt": int(parties_sync_time)}}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':

            r = request.json
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.castes

            r['update_dttm'] = datetime.datetime.utcnow()
            parties.insert_one(r)
            return json.dumps({'msg':'inserted successfully'})


    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/sympathizer_types', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def sympathizer_types():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('sympathizer_types_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.sympathizer_types
            # Get the related collections
            sync_collection = db.bis_sync_time
            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','name','telugu_name'],sort=[('_id',ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['created'],sort=[('created',DESCENDING)])

                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['created'])
            else:
                d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'created':{"$gt": d}}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['created'], sort=[('created', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['created'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':

            r = request.json
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.sympathizer_types

            r['created'] = datetime.datetime.utcnow()
            parties.insert_one(r)
            return json.dumps({'msg':'inserted successfully'})


    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/religion', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def religion():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('religion_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.religions
            # Get the related collections
            sync_collection = db.bis_sync_time
            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','name','telugu_name'],sort=[('_id',ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['update_dttm'],sort=[('update_dttm',DESCENDING)])

                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            else:
                d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'update_dttm':{"$gt": d}}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])
                # dict_cur.execute("""

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':

            r = request.json
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.religions

            r['update_dttm'] = datetime.datetime.utcnow()
            parties.insert_one(r)
            return json.dumps({'msg':'inserted successfully'})




    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/occupation', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def occupation():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            education_sync_time = request.values.get('occupation_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.occupations
            if education_sync_time in empty_check:

                rows = list(parties.find({}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            else:
                print "i am here"
                d = datetime.datetime.fromtimestamp(int(education_sync_time)/1000)
                rows = list(
                    parties.find({'update_dttm': {"$gt": d}}, ['_id', 'name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows, 'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':
            education = request.values.get('occupation')

            if education in empty_check:
                return json.dumps({'status': 0, 'errors': ['occupation missing']})

                education = education.strip()

            cur.execute("""
                select * 
                from occupation
                where name=%s
            """, (education,))
            row = cur.fetchone()
            if row:
                return json.dumps({'status': 0, 'errors': ['similar occupation already exists']})

            cur.execute("""
                insert into occupation(name)
                values(%s)
                returning id
            """, (education,))
            con.commit()
            id = cur.fetchone()[0]
            return json.dumps({'status': 1, 'data': {'id': id}})

        elif request.method == 'PATCH':
            id = request.values.get('id')
            education = request.values.get('occupation')

            if id in empty_check:
                return json.dumps({'status': 0, 'errors': ['id missing']})
            if education in empty_check:
                return json.dumps({'status': 0, 'errors': ['occupation missing']})

                education = education.strip()

            cur.execute("""
                            update occupation
                            set name=%s
                            where id=%s
                        """, (education, id))
            con.commit()
            return json.dumps({'status': 1})

    return give_cursor_json(temp)


@jsp_survey.route('/jsp_survey/education', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def education():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.educations
            education_sync_time = request.values.get('education_sync_time')
            if education_sync_time in empty_check:
                rows = list(parties.find({}, {'id':'_id','name':'name','telugu_name':'telugu_name'}, sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])

            else:
                d = datetime.datetime.fromtimestamp(int(education_sync_time) / 1000)
                rows = list(
                    parties.find({'update_dttm': {"$gt": d}}, ['_id', 'name' ,'telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:

                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows, 'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':
            education = request.values.get('education')

            if education in empty_check:
                return json.dumps({'status': 0, 'errors': ['education missing']})

                education = education.strip()

            cur.execute("""
                select *
                from education
                where name=%s
            """, (education,))
            row = cur.fetchone()
            if row:
                return json.dumps({'status': 0, 'errors': ['similar education already exists']})

            cur.execute("""
                insert into education(name)
                values(%s)
                returning id
            """, (education,))
            con.commit()
            id = cur.fetchone()[0]
            return json.dumps({'status': 1, 'data': {'id': id}})



    return give_cursor_json(temp)


@jsp_survey.route('/jsp_survey/community', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def community():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.communities
            education_sync_time = request.values.get('education_sync_time')
            if education_sync_time in empty_check:
                rows = list(parties.find({}, {'id': '_id', 'name': 'name','telugu_name':'telugu_name'}, sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])

            else:
                d = datetime.datetime.fromtimestamp(int(education_sync_time) / 1000)
                rows = list(
                    parties.find({'update_dttm': {"$gt": d}}, ['_id', 'name', 'telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('update_dttm', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = unix_time_millis(row['update_dttm'])
            return json.dumps({'status': 1, 'errors': [], 'data': rows, 'last_sync_time':int(last_sync_time)})

        elif request.method == 'POST':
            education = request.values.get('community')

            if education in empty_check:
                return json.dumps({'status': 0, 'errors': ['community missing']})

                education = education.strip()

            cur.execute("""
                select * 
                from community
                where name=%s
            """, (education,))
            row = cur.fetchone()
            if row:
                return json.dumps({'status': 0, 'errors': ['similar community already exists']})

            cur.execute("""
                insert into community(name)
                values(%s)
                returning id
            """, (education,))
            con.commit()
            id = cur.fetchone()[0]
            return json.dumps({'status': 1, 'data': {'id': id}})

        elif request.method == 'PATCH':
            id = request.values.get('id')
            education = request.values.get('community')

            if id in empty_check:
                return json.dumps({'status': 0, 'errors': ['id missing']})
            if education in empty_check:
                return json.dumps({'status': 0, 'errors': ['community missing']})

                education = education.strip()

            cur.execute("""
                            update community
                            set name=%s
                            where id=%s
                        """, (education, id))
            con.commit()
            return json.dumps({'status': 1})

    return give_cursor_json(temp)





def sync(last_updated_time,conn):
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)



        try:

            if last_updated_time in empty_check:
                return {'status': 0, 'errors': ['last_updated_time missing']}
            last_client_time = long((last_updated_time))
            # Connect to mongo db
            # conn = MongoClient(MONGO_URL,MONGO_PORT)
            # print("Connected successfully!!!")
            # # Get the current db
            db = conn.jsp_survey
            # Get the related collections
            sync_collection = db.bis_sync_time
            bis_data = db.bis_data
            mem_assign = db.Admins

            auth = request.authorization
            print auth
            membership_id = auth.username

            # find the related record
            # sync_collection.find({"constituency_id":member.constituency_id,})

            member = Memberships.query.filter_by(membership_id=membership_id.upper()).one_or_none()
            l = list(mem_assign.find({'MembershipID': membership_id.upper()}))
            i = list(l)[0]
            district_id = i['DistrictId'][0]['DistrictId']
            state_id = i['StateId'][0]['id']
            constituency_id = i['ConstituencyId'][0]['ConstituencyId']
            polling_booth_id = i['BoothId'][0]['PollingStationNumber']
            # find the related record
            row = sync_collection.find_one({"constituency_id": constituency_id,
                                        "state_id": state_id,
                                        "polling_booth_id": polling_booth_id,
                                        "member_id": membership_id.upper()})

            # cur.execute("""
            #                 select update_dttm
            #                 from bis_sync_time
            #                 where constituency_id=%s and state_id=%s and polling_booth_id=%s and info_by=%s
            #             """, (member.constituency_id, member.state_id, member.polling_station_id, member.id))
            # row = cur.fetchone()
            if row is None:
                return {'status': 2, 'msg': 'No last timestamp found send all the recorded data'}
            last_server_time = long(row["last_sync_time"])
            if last_server_time == last_client_time:
                return {'status': 3, 'msg': 'Already in sync'}
            elif last_server_time < last_client_time:
                return   {'status': 1, 'msg': 'Need data out of sync',
                     'last_updated_server_time': str(last_server_time)}
            elif last_server_time > last_client_time:
                # last
                data = bis_data.find({
                    "constituency_id": constituency_id,
                    "state_id": state_id,
                    "polling_booth_id": polling_booth_id,
                    "info_by": membership_id.upper(),
                    "update_dttm": {"$gte": last_client_time}

                })
                # dict_cur.execute("""
                #                 select party_sympathizer,mobile_number,voter_id,occupation,education,
                #                community,hobbies,facebook_id,twitter_id
                #                from booth_information
                #                where constituency_id=%s and state_id=%s and polling_booth_id=%s and info_by=%s and update_dttm>%s
                #             """, (last_client_time,))
                # data = dict_cur.fetchall()
                return {'status': 4, 'data': data, 'last_updated_server_time': str(last_server_time)}
            else:
                return {'status': 0}
        except Exception as e:

            print e.message

            print("Could not connect to MongoDB")
            return {'status':0,'msg':'Exception'}

    return give_cursor_json(temp)


@jsp_survey.route('/jsp_survey/update_records', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
@requires_jspsurveyToken
@csrf.exempt
def update_records():
    """
    {
        'voter_id':'abc123456',
        'state_id':1
        'district_id':1,
        'constituency_id':1,
        'polling_station_id':1,

    }

    :return:
    """
    def temp(con, cur):
        print request.json, request.values
        if not request.json:
            return json.dumps({'status': 0, 'error': 'No json sent'})
        if 'saved_records' in request.json:
            tup = request.json['saved_records']
        else:
            return json.dumps({'status': 0, 'error': 'parameter saved_records missing'})
        if 'last_updated_time' in request.json:
            last_updated_time = request.json['last_updated_time']
        else:
            return json.dumps({'status': 0, 'error': 'parameter last_updated_time missing'})

        if last_updated_time in empty_check:
            return json.dumps({'status':0,'error':'parameter last_updated_time missing'})
        # print request.values
        print "Before mongo connection"
        print MONGO_PORT, MONGO_URL
        conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
        print "successfully aquired a mongo connection"
        c = sync(last_updated_time,conn)
        print c,type(c),'This is where i print c'
        # c = json.loads(c)

        auth = request.authorization
        print auth

        membership_id = auth.username

        member = Memberships.query.filter_by(membership_id=membership_id.upper()).one_or_none()
        if member:


            print datetime.datetime.now(),"Init"
            db = conn.jsp_survey
            print datetime.datetime.now(),"bis1 conn"
            bis_data = db.bis_data
            mem_assign = db.Admins
            l = list(mem_assign.find({'MembershipID': membership_id.upper()}))
            i = list(l)[0]
            district_id = i['DistrictId'][0]['DistrictId']
            state_id = i['StateId'][0]['id']
            constituency_id = i['ConstituencyId'][0]['ConstituencyId']
            polling_booth_id = i['BoothId'][0]['PollingStationNumber']
            print datetime.datetime.now(),"bis_Data"
            sync_collection = db.bis_sync_time
            print datetime.datetime.now(),"bis_sync_time"
            max_time = ""
            try:
                if not isinstance(tup,list):

                    tup = json.loads(tup)
            except Exception as e:
                print e
                return json.dumps({'status':0,'errors':['PLEASE STRINGIFY THE ARRAY AND SEND']})
            if c['status'] == 2:
                bulk_write_data = []

                for item in tup:
                    # insert or update the records that are mentioned.
                    if max_time == '':
                        max_time = int(item['update_dttm'])
                    if item['status'] == 'U' or item['status'] == 'I' or item['status'] in ['NU','NI']:
                        data = dict()
                        del item['status']
                        item['sex'] = item['sex'].strip()
                        data['member_id'] = membership_id.upper()
                        data['state_id'] = item['state_id']
                        data['constituency_id'] = item['constituency_id']
                        data['polling_booth_no'] = item['polling_booth_no']
                        data['additional_id'] = item['additional_id']
                        bulk_write_data.append(UpdateOne(data,{'$set':item},upsert=True))
                        lt = int(item['update_dttm'])
                        if max_time < lt:
                            max_time = deepcopy(lt)

                try:
                    print bulk_write_data
                    r = db.bis_data.bulk_write(bulk_write_data)
                    print r
                except BulkWriteError as bwe:

                    pprint(bwe.details)
                sync_collection.update({"constituency_id": constituency_id,
                                        "state_id": state_id,
                                        "polling_booth_no": polling_booth_id,
                                        "member_id": membership_id.upper()}, {'$set': {"last_sync_time": last_updated_time}},upsert=True)
                return json.dumps({'status': 1,"last_sync_time":max_time })
            else:
                return json.dumps({'status': 2, 'errors': ['Already in sync']})
        else:
            return json.dumps({'status': 0, 'errors': ['Sorry you are not a member']})



    return give_cursor_json(temp)


@jsp_survey.route('/jsp_survey/get_master_data', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
@requires_jspsurveyToken
@csrf.exempt
def get_master_data():
    """
    Get master sync time
    if no value is present
    add the current polling booth of the user to the db and add the master sync time, send the polling booth data to user
    else:

    get the polling booth list assigned to the user.
        if the polling booth has a timestamp assigned to it
        compare the timestamp
            if greater then send the new data
            else ignore
        else:
            assign the global timestamp and send that pollingbooth data to the user.



    :return:
    """
    def temp(con, cur):
        if request.method == 'POST':
            print MONGO_PORT,MONGO_URL
            master_data_sync_time = request.json["master_data_sync_time"]
            print request.json
            print "Before mongo connection"
            conn = MongoClient(MONGO_URL, MONGO_PORT)
            print "After mongo connection"
            db = conn.jsp_survey
            mem_assign = db.Admins
            master_time = db.master_time
            last_saved_master_time = master_time.find({})[0]['master_update_dttm']

            if master_data_sync_time in empty_check:
                auth = request.authorization
                membership_id = auth.username
                t =  datetime.datetime.now()
                member = Memberships.query.filter_by(membership_id=membership_id.upper()).one_or_none()
                print datetime.datetime.now() - t, "membership id query"
                if member:
                    # print member.id

                    # print member.id
                    t = datetime.datetime.now()
                    l = list(mem_assign.find({'MembershipID': membership_id}))
                    print datetime.datetime.now() - t, "finding member in memassign query"

                    print datetime.datetime.now() - t, "Update datetime from voters table"

                    all_data = list()
                    if l:
                        i = list(l)[0]
                        dict_cur = con.cursor(cursor_factory=RealDictCursor)

                        district_id = i['DistrictId'][0]['DistrictId']
                        state_id = i['StateId'][0]['id']
                        constituency_id = i['ConstituencyId'][0]['ConstituencyId']
                        polling_booth_id = i['BoothId'][0]['PollingStationNumber']
                        query_string = """
                                          select voter_id,voter_name,age,sex,ac_no as constituency_id,state as state_id,part_no as polling_booth_no,house_no,%s as district_id
                                           from VOTERS_TABLE
                                           where state=%s and ac_no=%s and part_no = %s 
                                           order by house_no asc,age desc
                                           """.replace('VOTERS_TABLE',VOTERS_TABLE)
                        dict_cur.execute(query_string,
                                         (district_id, state_id, constituency_id,
                                               polling_booth_id))
                        t = datetime.datetime.now()
                        rows = dict_cur.fetchall()
                        print datetime.datetime.now() - t, "Voter list  from voters table"
                        all_data.extend(rows)
                        # data = dict()
                        # data['assign_dttm'] = last_saved_master_time
                        #
                        # upsert_check = dict()
                        # upsert_check['_id'] = i['_id']
                        #
                        # mem_assign.update_one(upsert_check, {"$set": data}, upsert=True)



                        return json.dumps({'status': 1, 'data': all_data, 'last_sync_time': last_saved_master_time})
                    else:
                        return json.dumps({'status': 0, 'errors': ['Sorry you are not a member']})
                else:
                    return json.dumps({'status': 0, 'errors': ['Sorry you are not a member']})
            else:
                auth = request.authorization
                membership_id = auth.username
                all_data = list()
                member = Memberships.query.filter_by(membership_id=membership_id.upper()).one_or_none()
                if member:
                    # print member.id

                    # print member.id
                    l = list(mem_assign.find({'MembershipID': membership_id}))


                    if l:
                        i = l[0]
                        dict_cur = con.cursor(cursor_factory=RealDictCursor)


                        if last_saved_master_time > long(master_data_sync_time):

                            district_id = i['DistrictId'][0]['DistrictId']
                            state_id = l[0]['StateId'][0]['id']
                            constituency_id = l[0]['ConstituencyId'][0]['ConstituencyId']
                            polling_booth_id = l[0]['BoothId'][0]['PollingStationNumber']
                            query_string = """
                                              select voter_id,voter_name,age,sex,ac_no as constituency_id,state as state_id,part_no as polling_booth_no,house_no,%s as district_id
                                               from VOTERS_TABLE
                                               where state=%s and ac_no=%s and part_no = %s and EXTRACT(EPOCH FROM date_trunc('second', update_dttm))>%s
                                               order by house_no asc,age desc
                                               """.replace('VOTERS_TABLE',VOTERS_TABLE)
                            dict_cur.execute(query_string,
                                             ( district_id,state_id, constituency_id,
                                               polling_booth_id,master_data_sync_time))
                            rows = dict_cur.fetchall()
                            all_data.extend(rows)


                        return json.dumps({'status': 1, 'data': all_data, 'last_sync_time': last_saved_master_time})


                    else:
                        return json.dumps({'status': 0, 'errors': ['Sorry you are not a member']})




    return give_cursor_json(temp)



@jsp_survey.route('/jsp_survey/bis_assign', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def bis_assign():
    def temp(con, cur):

        if request.method == 'GET':
            conn = MongoClient(MONGO_URL,MONGO_PORT)
            db = conn.jsp_survey
            mem_assign = db.mem_assign
            l = list(mem_assign.find())

            return json.dumps({'status':1,'data':l},cls=JSONEncoder)
            pass
        elif request.method == 'POST':
            member_id = request.values.get('member_id')
            state_id = request.values.get('state_id')
            constituency_id = request.values.get('constituency_id')
            polling_station_id = request.values.get('polling_station_id')
            print request.values
            conn = MongoClient(MONGO_URL,MONGO_PORT)
            db = conn.jsp_survey
            mem_assign = db.mem_assign
            if member_id in empty_check:
                return json.dumps({'status': 0, 'errors': ['member_id missing']})
            if state_id in empty_check:
                return json.dumps({'status': 0, 'errors': ['state_id missing']})
            if constituency_id in empty_check:
                return json.dumps({'status': 0, 'errors': ['constituency_id missing']})
            if polling_station_id in empty_check:
                return json.dumps({'status': 0, 'errors': ['polling_station_id missing']})

            data = dict()
            data['member_id'] = int(member_id)
            data['state_id'] = int(state_id)
            data['constituency_id'] = int(constituency_id)
            data['polling_station_id'] = int(polling_station_id)
            # data['assign_dttm'] = datetime.datetime.now()
            data['status'] = 'A'

            upsert_check = dict()
            upsert_check['status'] = 'A'
            upsert_check['state_id'] = int(state_id)
            upsert_check['constituency_id'] = int(constituency_id)
            upsert_check['polling_station_id'] = int(polling_station_id)
            mem_assign.update_one(upsert_check,{"$set":data},upsert=True)
            return json.dumps({'status':1,'msg':'Inserted successfully'})


    return give_cursor_json(temp)

# @jsp_survey.route('/jsp_survey/api/auth', methods=['GET', 'POST', 'PATCH'])
# @crossdomain(origiauthn="*", headers="Content-Type")
# # @requires_authToken
# @csrf.exempt
# def auth():
#     token = ''
#     return json.dumps({'status': 1, 'msg': 'Logged in successfully','token':token})

@jsp_survey.route('/jsp_survey/problems/add_edit', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def problems_add():
    if request.method == 'POST':
        conn = MongoClient(MONGO_URL, MONGO_PORT)
        db = conn.jsp_survey
        problem_policies = db.problem_policies

        mydict = request.json
        if not mydict:
            return json.dumps({'status':0,'msg':'Please send some data'})
        if 'problem_name' not in mydict:
            return json.dumps({'status':0,'msg':'Please send parameter problem_name'})

        current_time = datetime.datetime.now()
        update_dttm = unix_time_millis(current_time)
        update = False
        insert = False
        mydict['update_dttm'] = update_dttm
        if '_id' in mydict:
            # Update
            search_dict = {'_id':ObjectId(deepcopy(mydict['_id']))}
            del mydict['_id']
            info = problem_policies.find_and_modify(search_dict, {"$set": mydict}, upsert=True, full_response=True)
            to_send = info['lastErrorObject']['upserted']
        else:
            # Insert
            info = problem_policies.insert_one(mydict)

            to_send = info.inserted_id


        
        return json.dumps({'status':1,'msg':'Updated Successfully','_id':str(to_send)})


@jsp_survey.route('/jsp_survey/caste/add_edit', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def caste_add():
    if request.method == 'POST':
        conn = MongoClient(MONGO_URL, MONGO_PORT)
        db = conn.jsp_survey
        castes = db.castes

        mydict = request.json
        if not mydict:
            return json.dumps({'status': 0, 'msg': 'Please send some data'})
        if 'name' not in mydict:
            return json.dumps({'status': 0, 'msg': 'Please send parameter name'})
        if 'telugu_name' not in mydict:
            return json.dumps({'status': 0, 'msg': 'Please send parameter telugu_name'})

        current_time = datetime.datetime.now()
        update_dttm = unix_time_millis(current_time)
        update = False
        insert = False
        mydict['update_dttm'] = update_dttm
        if '_id' in mydict:
            # Update
            search_dict = {'_id': ObjectId(deepcopy(mydict['_id']))}
            del mydict['_id']
            info = castes.find_and_modify(search_dict, {"$set": mydict}, upsert=True, full_response=True)
            to_send = info['lastErrorObject']['upserted']
        else:
            # Insert
            info = castes.insert_one(mydict)

            to_send = info.inserted_id

        return json.dumps({'status': 1, 'msg': 'Updated Successfully', '_id': str(to_send)})


@jsp_survey.route('/jsp_survey/manifesto/add_edit', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def manifesto_add_edit():
    if request.method == 'POST':
        conn = MongoClient(MONGO_URL, MONGO_PORT)
        db = conn.jsp_survey
        problem_policies = db.manifesto

        mydict = request.json
        if not mydict:
            return json.dumps({'status': 0, 'msg': 'Please send some data'})
        if 'manifesto_point' not in mydict:
            return json.dumps({'status': 0, 'msg': 'Please send parameter problem_name'})

        current_time = datetime.datetime.now()
        update_dttm = unix_time_millis(current_time)
        update = False
        insert = False
        mydict['update_dttm'] = update_dttm
        if '_id' in mydict:
            # Update
            search_dict = {'_id': ObjectId(deepcopy(mydict['_id']))}
            del mydict['_id']
            info = problem_policies.find_and_modify(search_dict, {"$set": mydict}, upsert=True, full_response=True)
            to_send = info['lastErrorObject']['upserted']
        else:
            # Insert
            info = problem_policies.insert_one(mydict)

            to_send = info.inserted_id

        return json.dumps({'status': 1, 'msg': 'Updated Successfully', '_id': str(to_send)})



@jsp_survey.route('/jsp_survey/problems', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def problems():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('problems_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.problem_policies
            # Get the related collections
            sync_collection = db.bis_sync_time

            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','problem_name','telugu_name'],sort=[('_id',ASCENDING)]))
                print rows
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['update_dttm'],sort=[('update_dttm',DESCENDING)])

                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            else:
                # d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'update_dttm':{"$gt": parties_sync_time}}, ['_id', 'problem_name','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('last_updated', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})


    return give_cursor_json(temp)

@jsp_survey.route('/jsp_survey/manifesto', methods=['GET', 'POST', 'PATCH'])
@crossdomain(origin="*", headers="Content-Type")
# @requires_authToken
@csrf.exempt
def manifesto():
    def temp(con, cur):
        dict_cur = con.cursor(cursor_factory=RealDictCursor)

        if request.method == 'GET':
            parties_sync_time = request.values.get('manifesto_sync_time')
            conn = MongoClient(host=MONGO_URL, port=MONGO_PORT)
            db = conn.jsp_survey
            parties = db.manifesto
            # Get the related collections
            sync_collection = db.bis_sync_time

            if parties_sync_time in empty_check:

                rows = list(parties.find({},['_id','manifesto_point','telugu_name'],sort=[('_id',ASCENDING)]))
                print rows
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row  = parties.find_one({},['update_dttm'],sort=[('update_dttm',DESCENDING)])

                if row is None:
                    return json.dumps({'status':0,'errors':['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            else:
                # d = datetime.datetime.fromtimestamp(int(parties_sync_time) / 1e3)
                rows = list(parties.find({'update_dttm':{"$gt": parties_sync_time}}, ['_id', 'manifesto_point','telugu_name'], sort=[('_id', ASCENDING)]))
                for r in rows:
                    r['id'] = str(r['_id'])
                    del r['_id']
                row = parties.find_one({}, ['update_dttm'], sort=[('last_updated', DESCENDING)])

                if row is None:
                    return json.dumps({'status': 0, 'errors': ['no data added. Contact Developer']})
                last_sync_time = row['update_dttm']
            return json.dumps({'status': 1, 'errors': [], 'data': rows,'last_sync_time':int(last_sync_time)})


    return give_cursor_json(temp)