import json
import psycopg2

from flask import Blueprint, request, render_template, flash, url_for,session

from psycopg2.extras import RealDictCursor
from werkzeug.utils import redirect
import time, os,  base64, hmac, hashlib, uuid,urllib
import config
from appholder import csrf
from constants import empty_check
from main import generateOrderNumber, create_RPorderId
from utilities.decorators import crossdomain,shatagni_login_required
from utilities.dbrel import object_as_dict
from utilities.general import get_payment_details,checkUserAuth
from utilities.mailers import genOtp
from utilities.classes import ShatagniMagazineDetails
from main import verifyOtp
from dbmodels import ShatagniMagazine
from flask_login import login_user, logout_user, current_user
from celery_tasks.sms_email import send_magazine_subscription_sms
shatagni = Blueprint('shatagni', __name__)

@shatagni.route('/shatagni/register', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def shatagni_register():
	"""
	Registration service for Shatagni Magazine Books
	"""
	if request.method=='GET':
		return render_template("shatagni/register.html")

	try:
		
		phone=request.values.get('phone')

		if phone is None or phone=='':
			return json.dumps({"errors":"phone missing","status":"0"})
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		cur.execute("select phone from shatagni_magazine_team where phone=%s",(phone,))
		existing=cur.fetchone()
		con.close()
		if existing is None:
			
			genOtp(phone,"SM")
			return json.dumps({"errors":[],"status":"1"})
		
		return json.dumps({"errors":["already registered. please login"],"status":"0"})
	except (Exception,psycopg2.Error) as e:
		print str(e)
		return json.dumps({"errors":["something wrong.please try again"],"status":"0"})     

@shatagni.route('/shatagni/resend_otp', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt

def shatagni_resend_otp():
	try:
		phone=request.values.get('phone')
		if phone is None or phone=='':
			return json.dumps({"errors":"phone missing","status":"0"})
				
		genOtp(phone,"SM")
		return json.dumps({"errors":[],"status":"1"})
		
		
	except (Exception,psycopg2.Error) as e:
		print str(e)
		return json.dumps({"errors":["something wrong.please try again"],"status":"0"})     


@shatagni.route('/shatagni/verify_otp', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def shatagni_verify_otp():
	"""
	verify otp service for Shatagni Magazine Books
	"""
	try:
		phone=request.values.get('phone')
		otp=request.values.get('otp')
		password=request.values.get('password')
		
		if phone is None or phone=='':
			return json.dumps({"errors":"phone missing","status":"0"})
		if otp is None or otp=='':
			return json.dumps({"errors":"otp missing","status":"0"})
		if password is None or password=='':
			return json.dumps({"errors":"password missing","status":"0"})
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		if verifyOtp(phone,otp,'SM'):
			
			unique = 'tomatoPotatoadmin'
			key = hashlib.sha256(password + unique).hexdigest()
			cur.execute("select phone from shatagni_magazine_team where phone=%s",(phone,))
			existing=cur.fetchone()
			
			if existing is None:
				cur.execute("insert into shatagni_magazine_team(phone,pin)values(%s,%s)",(phone,key))
				con.commit()
				con.close()
			else:
				
				cur.execute("update shatagni_magazine_team set pin=%s where phone=%s",(key,phone))
				con.commit()
				con.close()
			
			if checkUserAuth(str(phone), password, "SM"):
		
				session['next'] = url_for('shatagni.Dashboard')
				session['current'] = url_for('shatagni.shatagni_register')
				session['logged_in'] = True
				
				return json.dumps({"errors":[],"status":"1","redirect_uri":url_for('shatagni.Dashboard')})

				
			return json.dumps({"errors":["something wrong"],"status":"0"})
			
		return json.dumps({"errors":["Wrong OTP. please check and submit again"],"status":"0"})
	except (Exception,psycopg2.Error) as e:
		print str(e)
		return json.dumps({"errors":["something wrong.please try again"],"status":"0"})     


@shatagni.route('/shatagni_magazine', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def shatagni_magazine():
	return render_template("shatagni/magazine.html")

@shatagni.route('/shatagni/login', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def shatagni_login():
	if request.method=='GET':
		return render_template("shatagni/register.html")
	phone=request.values.get('phone')
	password=request.values.get('password')
	if phone is None or phone=='':
		return json.dumps({"errors":["phone missing"],"status":"0"})
	
	if password is None or password=='':
		return json.dumps({"errors":["password missing"],"status":"0"})
	phone="91"+phone
	if checkUserAuth(str(phone), password, "SM"):
			
		session['next'] = url_for('shatagni.Dashboard')
		session['current'] = url_for('shatagni.shatagni_login')
		session['logged_in'] = True
		return json.dumps({"errors":[],"status":"1","redirect_url":url_for('shatagni.Dashboard')})
	else:
		
		return json.dumps({"errors":["Invalid Details"],"status":"0"})    


	   
@shatagni.route('/shatagni/dashboard', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def Dashboard():
	return render_template("shatagni/dashboard.html")



@shatagni.route('/shatagni/profile', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def shatagni_profile():
	if request.method=='GET':
		return render_template("shatagni/profile.html")
	try:
		member_id=current_user.id
		shatagni_instance=ShatagniMagazineDetails()

		profile = shatagni_instance.get_member_profile(member_id)
		data={}
		if profile:
			name=profile["name"]
			profile_status='T'
			if name is None or name=='':
				profile_status='F'
			data['status']=profile_status
			params=['id','name','phone','email','pincode','state','house_no','landmark','street_village','city_town']
			data['address']=address=str(profile["house_no"])+", "+str(profile["street_village"])+", "+str(profile['city_town'])+", "+str(profile["state"])+", "+str(profile["pincode"])+", landmark :"+str(profile["landmark"])
			
			for param in params:
				data[param]=profile[param]
		
		return json.dumps({"errors":[],"status":"1","profile":data})
	except Exception as e:
		print str(e)
		return json.dumps({"errors":["something wrong"],"status":"0"})


@shatagni.route('/shatagni/update_profile', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def update_shatagni_profile():
	try:
		member_id=current_user.id
		data=request.json
		parameters=["pincode","house_no","name","email","landmark","street_village","city_town","state"]
		
		if set(parameters).issubset(set(data.keys())):
			for parameter in parameters:
				if parameter not in ["email","landmark"]:
					if data[parameter] is None or data[parameter]=='':
						return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		cur.execute("""update shatagni_magazine_team set 
			name=%s,email=%s,house_no=%s,pincode=%s,landmark=%s,street_village=%s,city_town=%s,state=%s where seq_id=%s""",
			(data['name'],data['email'],data['house_no'],data['pincode'],data['landmark'],data['street_village'],data['city_town'],data['state'],member_id))
		con.commit()
		print "done"
		return json.dumps({"errors":[],"status":"1"})

	except (psycopg2.Error,Exception) as e:
		print str(e)
		return json.dumps({"errors":["something wrong"],"status":"0"})

@shatagni.route('/shatagni/change_address', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def shatagni_change_address():
	try:
		member_id=current_user.id
		data=request.json
		parameters=["pincode","house_no","landmark","street_village","city_town","state"]
		
		if set(parameters).issubset(set(data.keys())):
			for parameter in parameters:
				if parameter not in ["landmark"]:
					if data[parameter] is None or data[parameter]=='':
						return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		cur.execute("""update shatagni_magazine_team set 
			house_no=%s,pincode=%s,landmark=%s,street_village=%s,city_town=%s,state=%s where seq_id=%s""",
			(data['house_no'],data['pincode'],data['landmark'],data['street_village'],data['city_town'],data['state'],member_id))
		con.commit()

		return json.dumps({"errors":[],"status":"1"})

	except (psycopg2.Error,Exception) as e:
		print str(e)
		return json.dumps({"errors":["something wrong"],"status":"0"})
		
@shatagni.route('/magazine', methods=['GET','POST'])
@shatagni.route('/subscription', methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
def shatagni_subscription():
	if request.method=='GET':
		return render_template("shatagni/subscription.html")
	try:
		name=request.form.get("name")
		phone=request.form.get("mobile")
		email=request.form.get("email")
		address=request.form.get("address")
		state=request.form.get("state")
		pincode=request.form.get("pincode")
		city_town=request.form.get("city_town")
		payment_mode=request.form.get("payment_mode")
		plan=request.form.get("plan")
		
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		transaction_id = generateOrderNumber()
		
		plan=1
		
		transaction_amount=600
		
		data={}
		quantity=1			
		data['payment_mode']=payment_mode
		data['phone']=phone
		data['email']=email
		data['name']=name
		data['amount']=transaction_amount
		data['plan']=plan
		data['transaction_id']=transaction_id
		data['address']=address
		data['state']=state
		data['pincode']=pincode
		data['city_town']=city_town
		if payment_mode=='online':

			amount_paisa=transaction_amount*100
			orderid = create_RPorderId(100, transaction_id, 'INR')
			
			if orderid:
			
				cur.execute("""insert into shatagni_magazine_subscription_details
					(name,email,phone,address,state,city_town,pincode,payment_mode,
					subscription_plan,transaction_id,order_id,payment_status,transaction_amount,quantity,subscription_start_date,subscription_end_date)
					values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,now(),now()+interval '%s year' - interval '1 day')""",
					(name,email,phone,address,state,city_town,pincode,payment_mode,
						plan,transaction_id,orderid,'I',transaction_amount,quantity,plan))
				con.commit()
				con.close()
				data['razorpay_key']=config.razor_pay_key
				data['order_id']=orderid
				
				data['amount_paisa']=amount_paisa
				return render_template("shatagni/checkout.html",data=data)
			else:
				return render_template("shatagni/subscription.html")
			
		else:
			cur.execute("""insert into shatagni_magazine_subscription_details
					(name,email,phone,address,state,city_town,pincode,payment_mode,
					subscription_plan,transaction_id,payment_status,transaction_amount,quantity)
					values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
					(name,email,phone,address,state,city_town,pincode,payment_mode,
						plan,transaction_id,'I',transaction_amount,quantity))
			con.commit()
			con.close()
			return render_template("shatagni/checkout.html",data=data)	

	except (psycopg2.Error,Exception) as e:
		print str(e)
		return render_template("shatagni/subscription.html")


@shatagni.route('/subscription_status', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")

def magazine_subscription_status():
	try:
		if request.method=='GET':
			return redirect(url_for('shatagni.shatagni_subscription'))
		payment_id = request.form.get('razorpay_payment_id')
		
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)

		cur.execute("select seq_id from shatagni_magazine_payment where payment_id=%s ", (payment_id,))
		row = cur.fetchone()
		if row is None:
			client = config.client
			resp = client.payment.fetch(payment_id)
			
			if resp['status'] == 'authorized':
				order_id = resp['order_id']
				amount = int(int(resp['amount']) / 100)
				amount = str(amount)
				payment_method = resp['method']
				cur.execute(
					"update shatagni_magazine_subscription_details set transaction_amount=%s,payment_id=%s,payment_status=%s,update_dttm=now(),payment_method=%s where order_id=%s returning transaction_id ,transaction_amount ,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY') as date,payment_method,subscription_plan,quantity,name,email,phone,address,state,pincode,city_town",
					(amount, payment_id, 'S', payment_method, order_id))
				details = cur.fetchone()
				details['address']=details['address']+", "+str(profile['city_town'])+", "+str(profile["state"])+", "+str(profile["pincode"])
				con.commit()

				con.close()
				try:
					capture_status=client.payment.capture(payment_id,resp['amount'])
				except Exception as e:
					print str(e)
				payment = {}
				if details is not None:
					payment = details
					
					if config.ENVIRONMENT_VARIABLE != 'local':
						send_magazine_subscription_sms.delay(payment)
					else:
						send_magazine_subscription_sms(payment)
					
					
					return render_template("shatagni/subscription_success.html", payment=payment)
				else:
					return render_template("shatagni/support.html")
			elif resp['status'] == 'failed':
				order_id = resp['order_id']
				cur.execute(
					"update shatagni_magazine_subscription_details set payment_status=%s,payment_id=%s,update_dttm=now() where order_id=%s returning seq_id ",
					('F', payment_id, order_id))
				con.commit()
				con.close()
				return render_template("shatagni/support.html")
		else:
			return redirect(url_for('shatagni.shatagni_subscription'))

	except (psycopg2.Error, Exception) as e:
		print str(e)
		return render_template("shatagni/support.html")


@shatagni.route('/shatagni/subscribe', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def shatagni_subscribe():
	try:
		member_id=current_user.id
		
		data=request.json
		parameters=["plan","quantity"]
		
		if set(parameters).issubset(set(data.keys())):
			for parameter in parameters:
				
				if data[parameter] is None or data[parameter]=='':
					return json.dumps({"errors":[str(parameter)+ " Missing. please check "],"status":"0"})
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		transaction_id = generateOrderNumber()
		if transaction_id == '':
			
			return json.dumps({'errors': ['something wrong in generating transaction id'], 'status': 0})
		plan=int(data['plan'])
		quantity=int(data['quantity'])
		if plan==1:

			transaction_amount=int(500*quantity)
		else:
			transaction_amount=int(1250*quantity)
		try:
					   
		   pmt_gtw='RP' 
		   amount_paisa=transaction_amount*100
		   print amount_paisa
		   orderid = create_RPorderId(100, transaction_id, 'INR')
		   print "helloo"
		except Exception as e:
			print str(e)
			
			return json.dumps({'errors': ['Exception'], 'status': 0})
		if orderid == '' or orderid is None:
			flash("something wrong in generating order_id ,please try again", 'error')
			return json.dumps({'errors': ['Exception'], 'status': 0})
		
		cur.execute("""insert into shatagni_magazine_payment(member_id,payment_status,transaction_id,transaction_payment,order_id,plan,quantity)values(%s,%s,%s,%s,%s,%s,%s)""",
			(member_id,'I',transaction_id,transaction_amount,orderid,data['plan'],data['quantity']))
		con.commit()
		data={}
		shatagni_instance=ShatagniMagazineDetails()
		profile = shatagni_instance.get_member_profile(member_id)
		if profile:
			name=profile['name']
			email=profile['email']
			phone=profile['phone']
		data['key']=config.razor_pay_key
		data['orderid']=orderid
		data['phone']=phone
		data['email']=email
		data['name']=name
		data['amount']=amount_paisa
		print data
		return json.dumps({'errors':[],'status':1,'data':data})
		
	except (psycopg2.Error,Exception) as e:
		print str(e)
		return json.dumps({"errors":["something wrong"],"status":"0"})

@shatagni.route('/shatagni/subscription_status', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def subscription_status():
	try:
		payment_id = request.form.get('razorpay_payment_id')
		
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)

		cur.execute("select seq_id from shatagni_magazine_payment where payment_id=%s ", (payment_id,))
		row = cur.fetchone()
		if row is None:
			client = config.client
			resp = client.payment.fetch(payment_id)
			
			if resp['status'] == 'authorized':
				order_id = resp['order_id']
				amount = int(int(resp['amount']) / 100)
				amount = str(amount)
				payment_method = resp['method']
				cur.execute(
					"update shatagni_magazine_payment set transaction_payment=%s,payment_id=%s,payment_status=%s,update_dttm=now(),payment_method=%s where order_id=%s returning transaction_id,transaction_payment ,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY') as date,payment_method,plan,quantity",
					(amount, payment_id, 'S', payment_method, order_id))
				details = cur.fetchone()
				
				con.commit()

				cur.execute("insert into shatagni_magazine_subscription(trasaction_id,subscription_plan,subscription_start_date,subscription_end_date,quantity,member_id)values(%s,%s,now(),now()+interval '%s year' - interval '1 day',%s,%s)",(details['transaction_id'],details['plan'],details['plan'],details['quantity'],current_user.id))
				con.commit()
				con.close()
				payment = {}
				if details is not None:
					payment = details
					data={}
					shatagni_instance=ShatagniMagazineDetails()
					profile = shatagni_instance.get_member_profile(current_user.id)
					if profile:
						data["name"]=profile['name']
						data['email']=profile['email']
						data['phone']=profile['phone']
						data['transaction_id']=payment['transaction_id']
						data['plan']=payment['plan']
						data['quantity']=payment['quantity']
					if config.ENVIRONMENT_VARIABLE != 'local':
						send_magazine_subscription_sms.delay(data)
					else:
						send_magazine_subscription_sms(data)
					
					try:
						capture_status=client.payment.capture(payment_id,resp['amount'])
					except Exception as e:
						print str(e)
					return render_template("shatagni/subscription_success.html", payment=payment)
				else:
					return render_template("shatagni/support.html")
			elif resp['status'] == 'failed':
				order_id = resp['order_id']
				cur.execute(
					"update shatagni_magazine_payment set payment_status=%s,payment_id=%s,update_dttm=now() where order_id=%s returning seq_id ",
					('F', payment_id, order_id))
				con.commit()
				con.close()
				return render_template("shatagni/support.html")
		else:
			return redirect(url_for('shatagni.Dashboard'))

	except (psycopg2.Error, Exception) as e:
		print str(e)
		return render_template("shatagni/support.html")


@shatagni.route('/shatagni/subscriptions', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def subscriptions():
	try:
		member_id=current_user.id
		con = psycopg2.connect(config.DB_CONNECTION)
		cur = con.cursor(cursor_factory=RealDictCursor)
		cur.execute("select seq_id as id,subscription_plan,quantity,cast(subscription_start_date as text),cast(subscription_end_date as text),trasaction_id from  shatagni_magazine_subscription where member_id=%s",(member_id,))
		rows=cur.fetchall()
		return render_template("shatagni/subscriptions.html",data=rows)

	except (psycopg2.Error, Exception) as e:
		print str(e)


@shatagni.route('/shatagni/logout', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@shatagni_login_required
def shatagni_logout():	
	logout_user()
	session.clear()

	return redirect(url_for('.shatagni_magazine'))


@shatagni.route("/test_receipt",methods=['GET'])

def testReceipt():
	payment = {}
	payment['name']="Rambabu Vucha"
	payment['phone']="9676881991"
	payment["address"]="5-79,vvpalem,cherukupalli,guntur,andhra pradesh,522309"
	payment['transaction_id']="JSP1234567890"
	payment['transaction_amount']="600.00"
	payment['date']='2018-08-29'
	payment['email']='rambabu.v68@gmail.com'
	payment['subscription_plan']=1
	payment['quantity']=1
					
	if config.ENVIRONMENT_VARIABLE != 'local':
		send_magazine_subscription_sms.delay(payment)
	else:
		send_magazine_subscription_sms(payment)
	return 'success'
					