import json
import psycopg2

from flask import Blueprint, request, render_template, session, jsonify
from psycopg2.extras import RealDictCursor
from pymongo import MongoClient
from werkzeug.exceptions import abort

import config
from appholder import csrf
from celery_tasks.sms_email import sendSMS
from dbmodels import Memberships
from main import get_phonenumber, verifyOtp, saveSession
from utilities.classes import Voter_details, Membership_details, Assembly_constituencies_details, encrypt_decrypt
from utilities.decorators import crossdomain, requires_authToken
from utilities.general import upload_image
from celery_tasks.others import delete_blob
from utilities.mailers import sendNotification, genOtp
from utilities.others import generate_membershipid

blueprint_janasainyam = Blueprint('janasainyam', __name__)


@blueprint_janasainyam.route('/v1.4/getPollingBoothVotersList', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def getPollingBoothVotersListv14():
    voter_id = request.values.get("voter_id")
    page_no = request.values.get("page_no")
    if voter_id is None or voter_id == '':
        return json.dumps({"errors": ['Parameter voter_id missing'], 'data': {"status": '0'}})
    if page_no is None:
        page_no = 1
    voters_instance = Voter_details()
    voter_list = voters_instance.get_polling_booth_voters(voter_id, page_no)
    if voter_list is None:
        voter_list = []
    return json.dumps({"errors": [], 'data': {"status": "1", "children": voter_list}})


@blueprint_janasainyam.route("/getAssemblyConstituenciesByParliament", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def getAssemblyConstituenciesByParliament():
    try:
        parliament = request.values.get('parliament')
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select constituency_name from assembly_constituencies where lower(parliament_constituency_name)=%s",
            (str(parliament).lower(),))
        rows = cur.fetchall()
        data = []
        if len(rows):
            for row in rows:
                data.append(str(row['constituency_name']).rstrip())

        return json.dumps({'errors': [], 'data': {'status': '1', 'children': data}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@blueprint_janasainyam.route("/updateVolunteerDeviceDetails", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def updateVolunteerDeviceDetails():
    if request.method == 'POST':
        try:

            device_id = request.json['device_id']
            device_gcm_id = request.json['device_gcm_id']
            membership_id = request.json['membership_id']
        except (KeyError, Exception) as e:
            print str(e)
            return json.dumps({"errors": ['parameter ' + str(e) + " missing"], 'data': {"status": '0'}})
        if membership_id is not None and device_id is not None and device_gcm_id is not None:
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute(
                    "update janasena_membership set user_device_id=%s,user_gcm_id=%s,update_dttm=now() where membership_id=%s",
                    (device_id, device_gcm_id, membership_id))
                con.commit()
                con.close()
                return json.dumps({"errors": [], 'data': {"status": '1'}})
            except psycopg2.Error as e:
                print str(e)
                return json.dumps({"errors": ['something went wrong ,please try again'], 'data': {"status": '0'}})
        return json.dumps({"errors": ['please send correct details'], 'data': {"status": '0'}})


@blueprint_janasainyam.route("/knowAboutPollingStationCount", methods=["GET", "POST"])
@crossdomain(origin="*", headers="Content-Type")
def knowAboutPollingStationCount():
    voter_details_instance = Voter_details()
    info = voter_details_instance.get_polling_station_voter_count()
    return json.dumps({"errors": [], "data": {"status": '1', 'children': info}})


@blueprint_janasainyam.route("/saveMemberProfileImageApp", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
# @requires_authToken
def saveMemberProfileImageApp():
    try:

        membership_id = request.values.get('membership_id')
        if membership_id is None or membership_id == '':
            return json.dumps({"errors": ['membership_id missing'], "data": {"status": "0"}})

        membership_instance = Membership_details()
        if membership_instance.check_membership_existence(membership_id):
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            try:
                profile_image = ''

                if request.files:

                    profile_image = request.files['file']
                    print 'cp2222'
                    img_url = upload_image(profile_image, 'membershipimages')
                    cur.execute("select image_url from janasena_membership where membership_id=%s", (membership_id,))
                    old_image_file = cur.fetchone()

                    cur.execute("update janasena_membership set image_url=%s where membership_id=%s",
                                (img_url, membership_id))
                    con.commit()
                    con.close()
                    if old_image_file is not None and old_image_file != '':

                        if config.ENVIRONMENT_VARIABLE != 'local':
                            old_img_container = str(old_image_file['image_url']).split(".")[0]
                            if old_img_container.lower() != "https://janasenalive":
                                blob_name = str(old_image_file['image_url']).split('/')[-1]
                                print blob_name
                                print "change image in app"
                                delete_blob.delay('membershipimages', blob_name)
                    return json.dumps({"errors": [], "data": {"status": "1", "img_url": img_url}})
                else:
                    return json.dumps({"errors": ['please upload file'], "data": {"status": "0"}})
            except (KeyError, Exception) as e:
                print 'cpi'
                print str(e)
                con.close()
                return json.dumps({"errors": ['something wrong'], "data": {"status": "0"}})
        return json.dumps({"errors": ['Membership_id not found'], "data": {"status": "0"}})

    except psycopg2.Exception as e:
        print str(e)
        con.close()
        return json.dumps({"errors": ['something wrong in data'], "data": {"status": "0"}})


@blueprint_janasainyam.route('/getPollingBoothVotersList', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def getPollingBoothVotersList():
    voter_id = request.values.get("voter_id")
    page_no = request.values.get("page_no")
    if voter_id is None or voter_id == '':
        return json.dumps({"errors": ['Parameter voter_id missing'], 'data': {"status": '0'}})

    if page_no is None:
        page_no = 1
    if str(page_no) == '1':

        voters_instance = Voter_details()
        voter_list = voters_instance.get_polling_booth_voters(voter_id, page_no)
        if voter_list is None:
            voter_list = []
    else:
        voter_list = []

    return json.dumps({"errors": [], 'data': {"status": "1", "children": voter_list}})


@blueprint_janasainyam.route("/getAssemblyConstituencies", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def getAssemblyConstituencies():
    try:
        district = request.values.get('district')
        assembly_instance = Assembly_constituencies_details()
        assemblies = assembly_instance.get_constituencies_list(district)
        data = []
        for assembly in assemblies:
            data.append(str(assembly).rstrip())
        return json.dumps({'errors': [], 'data': {'status': '1', 'children': assemblies}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@blueprint_janasainyam.route("/saveMemberAssociationApp", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def saveMemberAssociationApp():
    try:
        try:
            associations = request.json['associations']

            membership_id = request.json["membership_id"]
        except (Exception, KeyError) as e:
            print (str(e))
            return json.dumps({"errors": ['please check parameters'], "data": {"status": "0"}})
        if associations is None or associations == '':
            return json.dumps({"errors": ['please check associations'], "data": {"status": "0"}})
        if membership_id is None or membership_id == '':
            return json.dumps({"errors": ['please check membership_id'], "data": {"status": "0"}})

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("update janasena_membership set member_associations=%s where membership_id=%s",
                    (associations, membership_id))
        con.commit()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1"}})

    except psycopg2.Exception as e:
        print str(e)
        con.close()
        return json.dumps({"errors": ['something went wrong.please try again'], "data": {"status": "0"}})


@blueprint_janasainyam.route("/getMembershipCardMobile", methods=['GET'])
@blueprint_janasainyam.route("/getMembershipCard", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getMembershipCard():
    membership_id = request.values.get('id')
    if membership_id is None or membership_id == "":
        abort(401)
    membership_instance = Membership_details()
    details = membership_instance.get_membership_details(membership_id, None)
    d = {}
    if details is not None:
        try:
            name = details['name']
            phone = details['phone']
            membership_id = details['membership_id']
            status = details['membership_through']
            img_url = details['img_url']
            language = details['language']

            if language is None:
                language = 'Telugu'
            if img_url is None or img_url == 'https://janasenaparty.org/static/img/default-avatar.png':
                img_url = './static/img/default-avatar.png'
            else:
                # once we recover we can delete this check
                img_url_split = str(img_url).split(".")[0]
                if img_url_split.lower() == "https://janasenalive":
                    img_url = './static/img/default-avatar.png'

            return render_template("membership/membership_card.html", membership_id=membership_id, name=name,
                                   phone=phone, status=status, img_url=img_url, language=language)
        except Exception as e:
            print str(e)
            abort(401)
    else:
        abort(401)


@blueprint_janasainyam.route("/knowAboutPollingStation", methods=["GET", "POST"])
@crossdomain(origin="*", headers="Content-Type")
@requires_authToken
def knowAboutPollingStation():
    voter_id = request.values.get("voter_id")
    if voter_id is None:
        return json.dumps({"errors": ["You have not provided your Voter ID  "], 'data': {'status': '0'}})
    voter_details_instance = Voter_details()
    info = voter_details_instance.get_polling_station_details(voter_id)
    return json.dumps({"errors": [], "data": {"status": '1', 'children': info}})


@blueprint_janasainyam.route("/validateManualOTPApp", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def validateManualOTPApp():
    try:
        try:
            otp = request.json["otp"]
            phone = request.json['mobile']
            mtype = request.json['mtype']
        except (KeyError, Exception) as e:
            print str(e)
            return json.dumps({'errors': ["Parameter " + str(e) + " Missing"], 'data': {'status': '0'}})
        if otp is None or otp == "":
            return json.dumps({"errors": ['invalid otp'], 'data': {'status': '0'}})
        if phone is None or phone == "":
            return json.dumps({"errors": ['invalid mobile number'], 'data': {'status': '0'}})
        if mtype is None:
            mtype = 'p'

        if mtype == 'm':
            status, mobile = get_phonenumber(phone)
            if status:
                phone = mobile

        if verifyOtp(phone, otp):
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                cur.execute(
                    "select * from temp_membership_details where phone=%s and verify_status=%s order by serial_no desc",
                    (phone, 'N'))
                details = cur.fetchone()
                if details is not None:
                    manual_details = details

                    voter_id = manual_details['voter_id']
                    voter_id = str(voter_id).lower()
                    membership_instance = Membership_details()
                    evoter_id = encrypt_decrypt(voter_id, 'E')
                    ephone = encrypt_decrypt(phone, 'E')
                    if membership_instance.check_voterid_existence(evoter_id):
                        if membership_instance.check_manual_phonenumber_count(ephone):

                            assembly_instance = Assembly_constituencies_details()
                            st_details = assembly_instance.get_state_with_constituency(manual_details['constituency'])

                            if st_details:
                                state = st_details.state_id
                                constituency = st_details.constituency_id
                            else:
                                state = '1'
                            if str(state) == '1':
                                state = "AP"
                            else:
                                state = 'TS'
                            constituency = str(constituency).zfill(3)
                            status, membership_id = generate_membershipid(manual_details['name'], phone, voter_id, '',
                                                                          '', '', '', manual_details['age'],
                                                                          manual_details['gender'], '0', constituency,
                                                                          state, manual_details['email'], '',
                                                                          manual_details['referral_id'], 'ME')
                            if status:
                                cur.execute("delete from temp_membership_details where phone=%s", (phone,))
                                con.commit()
                                con.close()

                                return json.dumps(
                                    {"errors": [], 'data': {"status": '1', 'membership_id': membership_id}})
                            return json.dumps(
                                {"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})

                        return json.dumps({"errors": ['phone number already  Existed'], 'data': {'status': '0'}})

                    return json.dumps({"errors": ['voter_id already  Existed'], 'data': {'status': '0'}})
                return json.dumps({"errors": ['No details Found please try again'], 'data': {'status': '0'}})
            except psycopg2.Error as e:
                print str(e)
                con.close()
                return json.dumps({"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})


        else:
            return json.dumps({"errors": ["Incorrect OTP"], "data": {"status": "0"}})

    except Exception as e:
        print str(e)

        return json.dumps({"errors": ["something wrong. please give correct details"], "data": {"status": "0"}})


@crossdomain(origin="*", headers="Content-Type")
@blueprint_janasainyam.route('/testFCMNotification', methods=['GET'])
def testFCMNotification():
    sendNotification("data")
    return "success"


@blueprint_janasainyam.route("/membershipIDstatus", methods=['GET'])
@requires_authToken
def membershipIDstatus():
    try:
        membership_id = request.values.get('id')
        if membership_id is not None and membership_id != '':
            membership_instance = Membership_details()
            if membership_instance.check_membership_existence(membership_id):
                return json.dumps({"errors": [], "data": {"status": '1'}})
        return json.dumps({"errors": ["MembershipID not Found"], "data": {"status": '0'}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


@blueprint_janasainyam.route("/verifyVolunteerOTP", methods=['GET', 'POST'])
@csrf.exempt
def verifyVolunteerOTP():
    try:
        # admin_id = request.authorization.username
        membership_id = request.json['membership_id']
        otp = request.json['otp']

        if membership_id is None or membership_id.isspace():
            return json.dumps({"errors": ["invalid membership_id"], "data": {"status": "0"}})
        if otp is None or otp.isspace():
            return json.dumps({"errors": ["invalid otp given"], "data": {"status": "0"}})
        MembershipID = str(membership_id).upper()
        registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
        if registered_user is not None:
            phone = registered_user.phone
            mobile_no = encrypt_decrypt(phone, 'D')

            if verifyOtp(mobile_no, otp, "ML"):
                token = saveSession(membership_id, mobile_no, 'L')
                membership_instance = Membership_details()
                profile = membership_instance.get_membership_details(membership_id)


                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("""
                    select district_name,constituency_name
                    from assembly_constituencies
                    where state_id=%s and constituency_id=%s
                """,(profile['state_id'],profile['ac_no']))
                row = cur.fetchone()
                profile['constituency_name'] = ""
                profile['district_name'] = ""
                if row is not None:

                    profile['constituency_name'] = row[1]
                    profile['district_name'] = row[0]
                # Adding new flags for determining if the user is a volunteer or not.
                conn = MongoClient(config.MONGO_URL, config.MONGO_PORT)
                db = conn.bis
                mem_assign = db.mem_assign
                count = mem_assign.count({'member_id': str(registered_user.id),'status':'A'})
                if count == 0:
                    is_booth_incharge = 'N'
                else:
                    is_booth_incharge = 'Y'

                cur.execute("""
                    select *
                    from jsp_mandal_admin
                    where jsp_id=%s and status='Y'
                """,(membership_id,))
                c_proxy = cur.fetchone()
                if c_proxy:
                    is_mandal_admin = True
                else:
                    is_mandal_admin = False
                return json.dumps({"errors": [], "data": {"status": '1', "children": profile, "token": token,"is_booth_incharge":is_booth_incharge,"is_mandal_admin":is_mandal_admin}})
            return json.dumps({"errors": ['Wrong  Otp'], 'data': {"status": '0'}})
        return json.dumps({"errors": ['Invalid Details Sent'], 'data': {"status": '0'}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {"status": '0'}})


@blueprint_janasainyam.route('/volunteerResendOTP', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def volunteerResendOTP():
    try:
        if request.method == 'POST':
            umobile = request.json['mobile_number']
            mtype = request.json['mtype']

            if umobile is None and umobile == "":
                return json.dumps({'errors': ['mobile_num missing'], 'data': {'status': '0'}})
            if mtype is None:
                mtype = 'p'

            if mtype == 'm':
                status, mobile = get_phonenumber(umobile)
                if status:
                    umobile = mobile

            otp = genOtp(umobile)

            js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'otp sent  Successfully', 'otp': otp}})
            return js

        else:
            js = json.dumps({'errors': ['Not Allowed'], 'data': {'status': '0'}})
            return js
    except Exception as e:
        print str(e)
        js = json.dumps({'errors': ['Something Went Wrong, Please try again later'], 'data': {'status': '0'}})


@blueprint_janasainyam.route("/forgotmembershipID", methods=["GET", "POST"])
@crossdomain(origin="*", headers="Content-Type")
def forgotmembershipID():
    voter_id = request.values.get('voter_id')

    if voter_id is None:
        return json.dumps({"errors": ['voter_id is missing'], "data": {"status": '0'}})
    membership_instance = Membership_details()
    evoter_id = encrypt_decrypt(str(voter_id).lower(), 'E')
    details = membership_instance.get_membership_details(None, evoter_id)
    d = {}
    if details is not None:

        try:
            phone = encrypt_decrypt(details['phone'])
            numberToSend = str(phone)
            msgToSend = "Hello " + str(details['name']).rstrip() + ", Your Janasena Party membership ID : " + str(
                details['membership_id']) + "  Thanks !"

            sendSMS(numberToSend, msgToSend)

            return json.dumps({"errors": [], "data": {"status": '1'}})
        except (KeyError, Exception) as e:
            print str(e)
            return json.dumps(
                {"errors": ['something went wrong . please try after some time'], "data": {"status": '0'}})
    else:
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@blueprint_janasainyam.route("/getVoterDetails", methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def getVoterDetails():
    try:
        try:
            voter_id = request.json['voter_id']

            phone = request.json['mobile_num']
            otp = request.json['otp']
            mtype = request.json['mtype']
        except (KeyError, Exception) as e:
            return json.dumps({"errors": ["Parameter " + str(e) + " Missing"], 'data': {"status": "0"}})
        if voter_id is None or voter_id.isspace():
            return json.dumps({"errors": ["invalid Voter_id .please try again"], "data": {"status": "0"}})
        if otp is None or otp.isspace():
            return json.dumps({"errors": ["invalid otp given"], "data": {"status": "0"}})
        if phone is None or phone.isspace():
            return json.dumps({"errors": ["invalid phone Number. please try again"], "data": {"status": "0"}})
        if mtype is None:
            mtype = 'p'

        if mtype == 'm':
            status, mobile = get_phonenumber(phone)
            if status:
                phone = mobile

        if verifyOtp(phone, otp, "MB"):

            children = {}

            voterdetails_instance = Voter_details()
            voter_id = str(voter_id).lower()
            record = voterdetails_instance.get_voter_details(voter_id)

            if record is not None:
                try:
                    try:
                        children['Name'] = str(record['voter_name']).rstrip()
                    except Exception as e:
                        children['Name'] = ''
                    try:
                        children['relation_name'] = str(record['relation_name']).rstrip()
                    except Exception as e:
                        children['relation_name'] = ''

                    children['relation_type'] = record['relation_type']
                    children['Age'] = record['age']
                    children['Sex'] = record['sex']
                    children['House_No'] = record['house_no']
                    children['Booth No'] = record['part_no']

                    children['booth_name'] = record['section_name']
                    children['Section_id'] = str(record['ac_no']).zfill(3)
                    children['state'] = record['state']
                    print children['Section_id']
                    try:
                        con = psycopg2.connect(config.DB_CONNECTION)
                        cur = con.cursor()
                        cur.execute(
                            "select constituency_name,district_name from assembly_constituencies where constituency_id=%s and state_id=%s",
                            (children['Section_id'], children['state']))
                        row = cur.fetchone()
                        print row
                        if row is not None:
                            children['constituency_name'] = row[0]
                            children['district_name'] = row[1]
                        else:
                            children['constituency_name'] = ''
                            children['district_name'] = ''

                    except Exception as e:
                        print str(e)
                except Exception as e:
                    print str(e)

                session['member_details'] = children
                print session
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': children}})
            return json.dumps(
                {"errors": ['no record found with the given details'], 'data': {'status': '0', 'children': children}})
        else:
            return json.dumps({"errors": ['invalid otp'], 'data': {'status': '0'}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': '0'}})


@blueprint_janasainyam.route("/getMembershipID", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def getMembershipID():
    if request.method == 'POST':
        try:

            phone = request.json['phone']
            voter_id = request.json['voter_id']
            name = request.json['name'].rstrip()
            email = request.json['email']
            relation_name = request.json['relation']
            relation_type = request.json['relation_type']
            address = str(request.json['address']).rstrip()
            town = request.json['town']
            age = request.json['age']
            gender = request.json['gender']
            booth = request.json['booth']
            assembly_id = request.json['assembly_id']
            booth_name = request.json['booth_name']
            state = request.json['state']
            referral_id = request.json['referral_id']
            mtype = request.json['mtype']
            if state == '1':
                state = "AP"
            else:
                state = 'TS'
        except (Exception, KeyError) as e:
            print str(e)
            return json.dumps({"errors": ['parameter ' + str(e) + " missing"], 'data': {"status": '0'}})

        try:
            if mtype is None:
                mtype = 'p'

            if mtype == 'm':
                status, mobile = get_phonenumber(phone)
                if status:
                    phone = mobile

            voter_id = str(voter_id).lower()
            evoter_id = encrypt_decrypt(voter_id, 'E')
            ephone = encrypt_decrypt(phone, 'E')
            membership_instance = Membership_details()

            if membership_instance.check_voterid_existence(evoter_id):
                if membership_instance.check_phonenumber_count(ephone):

                    status, membership_id = generate_membershipid(name, phone, voter_id, relation_name, relation_type,
                                                                  address, town, age, gender, booth, assembly_id, state,
                                                                  email, booth_name, referral_id, 'M')
                    if status:
                        return json.dumps({"errors": [],
                                           'data': {'status': '1', 'membership_id': membership_id, 'name': name,
                                                    'phone': phone}})

                    return json.dumps({"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})

                return json.dumps({"errors": ['This Voter Id already existed'], 'data': {'status': '0'}})

            return json.dumps({"errors": ['This Voter Id already existed'], 'data': {'status': '0'}})

        except Exception as e:
            print str(e)

            return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})


@blueprint_janasainyam.route("/volunteerLogin", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def volunteerLogin():
    if request.method == 'POST':
        try:

            membership_id = request.json['membership_id']
        except (Exception, KeyError) as e:
            print str(e)
            return jsonify({"errors": ['parameter ' + str(e) + " missing"], 'data': {"status": '0'}}), 400

        if membership_id is None or membership_id.isspace():
            return jsonify({"errors": [' membership_id should not be empty'], 'data': {"status": '0'}}), 400
        membership_instance = Membership_details()

        details = membership_instance.get_membership_details(membership_id)
        if details is not None:
            ephone = details['phone']
            mobile_no = encrypt_decrypt(ephone)
            otp = genOtp(mobile_no, 'ML')

            return jsonify({"errors": [], 'data': {'status': '1'}}), 200
        else:
            return jsonify({"errors": ['it seems you have not provided details yet. please provide'],
                            'data': {'status': '0'}}), 200


@blueprint_janasainyam.route("/getDistricts", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def getDistricts():
    try:
        assembly_instance = Assembly_constituencies_details()
        districts = assembly_instance.get_districts_list()

        data = []
        for district in districts:
            d = {}
            d['name'] = str(list(district)[0]).rstrip()

            data.append(d)
        return json.dumps({'errors': [], 'data': {'status': '1', 'children': data}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})
