# coding= utf-8 
# -*- coding: utf-8 -*-
import operator
import urllib2
from urlparse import urlparse, urlunparse
from azure.storage.table import TablePayloadFormat, TableBatch
from flask_login import login_user, logout_user, current_user
from flask_wtf.csrf import CSRFError
from werkzeug.utils import secure_filename

from appholder import app, login_manager, csrf, table_service, db, block_blob_service
from bis.views import booth_information_system
from booth_mandal_admins.views import booth_mandal_admins
from celery_tasks.others import delete_blob,save_requests
from celery_tasks.sms_email import sendSMS, senddonationsemail, sendVoiceSMS,send_bulk_sms_manual,send_bulk_sms_excel,donations_check_task,jspMissedcallTask2,upload_supporters_data,upload_mrpresident_file,recurring_donations_remainder
from dbmodels import *
from groups.views import group
from janatarangam.views import janatarangam
from jsp_survey.views import jsp_survey

from main import *
from polling_booth_leaders.views import pbl
from surveyforms.views import survey_form
from utilities.classes import Membership_details, MembershipPendingDetails, Voter_details, AnonymousUserMixin, \
    Assembly_constituencies_details, MembershipSMS, encrypt_decrypt
from utilities.dbrel import object_as_dict, strip_non_ascii
from utilities.decorators import crossdomain, admin_login_required, requires_auth, requires_authToken, \
    requires_EventsauthToken, frontdesk_login_required, required_roles, member_login_required,janasainyam_login_required
from utilities.general import checkUserAuth, generate_csrf_token, id_generator, upload_image_web
from utilities.mailers import send_thank_you_mail, genOtp, genEmailOtp, sendNotification
from utilities.sms import sendBULKSMS,sendBULKSMS_samemessage
#  blueprint imports
from events.views import blueprint_event
from utilities.others import makeResponse
from website.views import blueprint_website
from janasainyam_app.views import blueprint_janasainyam
from membership.views import blueprint_membership
from donations.views import blueprint_donations
from admin_dashboard.views import blueprint_admin_dash
from frontdesk.views import blueprint_frontdesk
from janaswaram.views import blueprint_janaswaram
from offline_memberships.views import blueprint_offline_memberships
from membership_reports.views import mem_report
from pysendpulse.pysendpulse import PySendPulse
from offline_books.views import offline_books
from shatagni.views import shatagni
from membership_user_group.views import mug
from kriya.kriya import kriya


#  Register blueprints
app.register_blueprint(blueprint_event)
app.register_blueprint(blueprint_website)
app.register_blueprint(blueprint_janasainyam)
app.register_blueprint(blueprint_membership)
app.register_blueprint(blueprint_donations)
app.register_blueprint(blueprint_admin_dash)
app.register_blueprint(blueprint_frontdesk)
app.register_blueprint(blueprint_janaswaram)
app.register_blueprint(group)
app.register_blueprint(mem_report)
app.register_blueprint(pbl)
app.register_blueprint(blueprint_offline_memberships)
app.register_blueprint(booth_information_system)
app.register_blueprint(offline_books)
app.register_blueprint(shatagni)
app.register_blueprint(jsp_survey)
app.register_blueprint(survey_form)
app.register_blueprint(janatarangam,url_prefix='/janasenatarangam')
app.register_blueprint(booth_mandal_admins,url_prefix='/booth_mandal_admins')
app.register_blueprint(kriya)

@app.before_request
def redirect_www():
    
    """db change for testing purposr """
    try:
        from urlparse import urlparse
        url_obj=urlparse(request.url)
        hostname=str(url_obj.netloc)
        path=str(url_obj.path)
        query=str(url_obj.query)
        requested_url= str(request.url)
        ipaddress=str(request.headers.get("Cf-Connecting-Ip",None))
        if path not in ['/sms_delivery_reports','/jspworker2659.js']:

            if config.ENVIRONMENT_VARIABLE!='local':
                save_requests.delay(ipaddress,requested_url,hostname,path,query)
    except Exception as e:
        print str(e)
    
    # if hostname not in ['127.0.0.1','localhost','192.168.0.23']:
    #     subdomain = urlparse(request.url).hostname.split('.')[0]
    #     if subdomain == 'test':
            
            
    #         DATABASE_LIVE_URL = 'postgresql://postgres:JDatabase)(passwe@10.139.72.246:6432/janasena2'
    #         url = urlparse(DATABASE_LIVE_URL)

    #         APP_NAME = url.path[1:]
    #         DB_USER = url.username
    #         DB_PASSWORD = url.password
    #         DB_HOST = url.hostname
    #         DB_PORT = url.port

    #         app.config.update({"DB_CONNECTION":"host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(DB_HOST, DB_PORT, APP_NAME,DB_USER, DB_PASSWORD)})
    #         app.config.update({"SQLALCHEMY_DATABASE_URI" :'postgresql://' + str(DB_USER) + ':' + str(DB_PASSWORD) + '@' + str(DB_HOST) + '/' + str(APP_NAME)})
            
    #     else:
    #         DATABASE_LIVE_URL = 'postgresql://postgres:JDatabase)(passwe@10.139.72.246:6432/janasena2'
    #         url = urlparse(DATABASE_LIVE_URL)

    #         APP_NAME = url.path[1:]
    #         DB_USER = url.username
    #         DB_PASSWORD = url.password
    #         DB_HOST = url.hostname
    #         DB_PORT = url.port

    #         app.config.update({"DB_CONNECTION":"host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(DB_HOST, DB_PORT, APP_NAME,DB_USER, DB_PASSWORD)})
    #         app.config.update({"SQLALCHEMY_DATABASE_URI" :'postgresql://' + str(DB_USER) + ':' + str(DB_PASSWORD) + '@' + str(DB_HOST) + '/' + str(APP_NAME)})
            
    # else:
    #     DB_HOST = 'localhost'
    #     DB_PORT = '5432'
    #     APP_NAME = 'janasena'
    #     DB_USER = 'postgres'
    #     DB_PASSWORD = "root"
    #     app.config.update({"DB_CONNECTION": "host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(
    #         DB_HOST, DB_PORT, APP_NAME, DB_USER, DB_PASSWORD)})
    #     app.config.update({"SQLALCHEMY_DATABASE_URI": 'postgresql://' + str(DB_USER) + ':' + str(
    #         DB_PASSWORD) + '@' + str(DB_HOST) + '/' + str(APP_NAME)})
        
  

@app.errorhandler(404)
def page_not_found(e):
    urlparts = urlparse(request.url)
    urlparts_list = list(urlparts)

    if urlparts_list[2] == '/nri-connect/':
        urlparts_list[2] = '/nri-connect'
        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/about-janasena-party/':
        urlparts_list[2] = '/about'
        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/janaswaram/':
        urlparts_list[2] = '/janaswaram'

        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/media/':
        urlparts_list[2] = '/media'

        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/andhra-pradesh-special-status-issue/':
        urlparts_list[2] = '/APSpecialStatusIssue'

        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/uddanam-kidney-issue/':
        urlparts_list[2] = '/UddanamKidneyIssue'

        return redirect(urlunparse(urlparts_list), code=301)
    elif urlparts_list[2] == '/bhimavaram-acqua-food-park-issue/':
        urlparts_list[2] = '/BhimavaramAcquaFoodparkIssue'

        return redirect(urlunparse(urlparts_list), code=301)
    elif (any(x.isupper() for x in urlparts_list[2])):
        urlparts_list[2] = urlparts_list[2].lower()

        return redirect(urlunparse(urlparts_list), code=301)

    return render_template('404.html'), 404


login_manager.anonymous_user = AnonymousUserMixin




@login_manager.user_loader
def load_user(id):
    if session['t'] == 'A':
        return Admin.query.get(int(id))
    if session['t'] == 'M':
        return Memberships.query.get(int(id))
    if session['t'] == 'F':
        return Frontdesk.query.get(int(id))
    if session['t'] == 'J':
        return OfflineMemberships.query.get(int(id))
    if session['t'] == 'SM':
        return ShatagniMagazine.query.get(int(id))
    if session['t'] in ['CL','CLA','CLD']:
        return ACHead.query.get(int(id))


###################3





@app.route('/covid19', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeTNConstituencies():
    try:
        assembly_info = "covid19.json"

        json_data = open(assembly_info).read()

        data = json.loads(json_data)
        children = {}
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        keys = ["Email ID","Phone No","Name","District","Constituency","Telugu constituency Name","Village/Ward","Program Name","File Name","Files Count"]
        count =0
        for record in data:
            
            for key in keys:
                if key not in record.keys():
                    record[key]= ""

            cur.execute(
                """insert into covid2(email_id,phone_no,name,district,constituency,telugu_constituency_name,village_ward,program_name,file_name,files_count)
                values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                (record["Email ID"],  record["Phone No"], record["Name"],record["District"],record["Constituency"], record["Telugu constituency Name"],record["Village/Ward"],  record["Program Name"], record["File Name"],record['Files Count']))
            count = count+1
            print(count)
        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'












@app.route('/getNriData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getNriData():
    next_marker = request.values.get('next_marker')
    if next_marker is None:

        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
    all_data = []
    query = "country ne 'in'"
    entities = table_service.query_entities('janasenavolunteer', num_results=1000, filter=query, marker=marker,
                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA, property_resolver=None,
                                            timeout=None)

    entit = list(entities)
    for row in entit:
        row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
    all_data = entit
    x_ms_continuation = {}
    if hasattr(entities, 'next_marker'):
        x_ms_continuation = getattr(entities, 'next_marker')

    # if x_ms_continuation!={}:
    #   marker['nextpartitionkey'] = x_ms_continuation['nextpartitionkey']
    #   marker['nextrowkey'] = x_ms_continuation['nextrowkey']
    # else:
    #   break;

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})


@app.route('/getNriConnect', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getNriConnect():
    return render_template('admin/nri_connect.html')











@app.route('/assignTOReviewCommittee', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
def assignTOReviewCommittee():
    if request.method == 'POST':
        review_person = request.values.get('review_person')
        
        users = request.values.get('users')
        print users
        if review_person is None or review_person == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_review_person_MISSING'], 'data': {'status': '0'}})
        if users is None or users == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_users_MISSING'], 'data': {'status': '0'}})

        else:
            try:
                users = json.loads(users)
                batch = TableBatch()
                for user in users:
                   
                    task = {"assigned_to": review_person, "review_status": "F", "RowKey": str(user),
                            'PartitionKey': 'pk'}
                    if_match = ""
                    batch.merge_entity(task, if_match)
                table_service.commit_batch('janaswaram', batch)
                return json.dumps({'errors': [], 'data': {'status': '1'}})
            except Exception as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})




@app.errorhandler(401)
def un_authorised(error=None):
    errors = []
    errors.append('Unauthorised Access.Please Try Again')
    data = {'status': 401}
    message = {
        'errors': errors,
        'data': data,
    }
    resp = jsonify(message)
    resp.status_code = 401

    return resp


@app.errorhandler(CSRFError)
def errorhandler(reason):
    token = session.pop('_csrf_token', None)

    if not token or token != request.form.get('csrf_token'):
        errors = []
        errors.append('Your CSRF Token Expired.Please Try Again')
        data = {'status': 403}
        message = {
            'errors': errors,
            'data': data,
        }
        resp = jsonify(message)
        resp.status_code = 403

        return resp


app.jinja_env.globals['csrf_token'] = generate_csrf_token


@app.route("/token", methods=['POST', 'GET'])
def token():
    token = generate_csrf_token()
    return json.dumps({"errors": [], "data": {"status": '1', "token": token}})


# def formationday():
#     return redirect("https://formationday.janasenaparty.org")


@app.route("/testvoiceCall", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def testvoiceCall():
    try:
        phone = request.values.get("phone")

        audio_clip = request.values.get("audio_clip")

        sendVoiceSMS(phone, audio_clip)

        return "success"
    except Exception as e:
        return str(e)


@app.route("/checkForDuplicates", methods=['POST', 'GET'])
def checkForDuplicates():
    phone = request.values.get('phone')
    email = request.values.get('email')
    
    tasks = table_service.query_entities('janasenavolunteer',
                                         filter="mobile_num eq '" + str(phone) + "' or email eq '" + str(email) + "'")
    

    for item in tasks:

        return json.dumps({"errors": [], 'data': {'status': '0'}})
    else:

        return json.dumps({"errors": [], 'data': {'status': '1'}})


@app.route("/sendemail", methods=['POST', 'GET'])
def sendemail():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from resourcepersons where attendance_status=%s and district  in %s and email!=%s",
                    ('T', tuple(['Adilabad', 'Mahabubnagar']), ''))

        rows = cur.fetchall()
        con.close()

        message = open("./facilitator_invite.html").read()

        for item in rows:
            try:

                person_Name = item['name']
                if item['email'] != '' and item['email'] is not None:
                    to = item['email']
                    print to
                    # to='managerops@janasenaparty.org'
                    mobile = item['mobile']

                    u_id = str(uuid.uuid4())

                    link = "https://janasenaparty.org/collectresourcepersonsdata?id=" + u_id + "&phone=" + str(mobile)
                    subject = " Opportunity To Be a JanaSena Party Facilitator  /జనసేన పార్టీ సమన్వయకర్తలుగా అవకాశం "
                    name = "JANASENA PARTY"
                    temp_text = str(message)
                    temp_text = temp_text.replace("{{FNAME}}", str(person_Name))
                    temp_text = temp_text.replace("{{link_here}}", str(link))

                    sendmailmandrill(name, to, str(temp_text), subject)
                    numberToSend = str(mobile)
                    msgToSend = "Hi, we have sent an email about an opportunity to be Facilitator with the Janasena Party. Please check your email(include spam folder also) and respond your interest by 12th November 2017 07:00PM"
                    sendSMS(numberToSend, msgToSend)

            except Exception as e:
                print str(e)
                continue
        return " sent successfully"
    except Exception as e:
        print str(e)
        return str(e)

from celery_tasks.sms_email import send_bulk_email_operations
@app.route("/send_covid_email", methods=['POST', 'GET'])
def send_nri_fellowship_email():
    try:

        district_id = request.values.get('id')
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        #cur.execute("select distinct on(email)email,name from nri_volunteers union  select distinct on(email)email,name from nri_data ")
        #rows=cur.fetchall()
        # for row in rows:
        #     row['email']=encrypt_decrypt(row['email'],'D')
        rows=[]
        # subject change chey
        subject="అభినందనలు - మీ పవన్ కళ్యాణ్."
        body=open("covid19.html").read()
        rows=[{"name":"sarath","email":"sarath.pondreti93@gmail.com"}]        
        if config.ENVIRONMENT_VARIABLE!='local':
            send_bulk_email_operations.delay(rows, body, subject, ['name'], [])
        else:
            
            send_bulk_email_operations(rows, body, subject, ['name'], [''])


        return " sent successfully"
    except Exception as e:
        print str(e)
        return str(e)

@app.route('/getresourcepersonsdata', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getresourcepersonsdata():
    next_marker = request.values.get('next_marker')
    district = request.values.get('district')
    if next_marker is None:

        marker = {}
        marker['nextpartitionkey'] = None
        marker['nextrowkey'] = None
    else:
        marker = json.loads(next_marker)
    all_data = []
    if district is not None:
        filter_query = "district eq '" + district + "'"
    else:
        filter_query = None
    entities = table_service.query_entities('uttarandhraresourcepersons', num_results=1000, filter=filter_query,
                                            marker=marker, accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
                                            property_resolver=None, timeout=None)

    entit = list(entities)
    for row in entit:
        row['Timestamp'] = str(row['Timestamp']).split(" ")[0]
    all_data = entit
    x_ms_continuation = {}
    if hasattr(entities, 'next_marker'):
        x_ms_continuation = getattr(entities, 'next_marker')

        # if x_ms_continuation!={}:
        #   marker['nextpartitionkey'] = x_ms_continuation['nextpartitionkey']
        #   marker['nextrowkey'] = x_ms_continuation['nextrowkey']
        # else:
        #   break;

    return json.dumps(
        {"errors": [], 'data': {'status': '1', 'children': {'rows': all_data, "next_marker": x_ms_continuation}}})


@app.route('/memberships', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def memberships():
    return render_template('admin/memberships.html')


@app.route('/resourcepersonCounts', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def resourcepersonCounts():
    try:
        districts = ['Guntur', 'Krishna', 'West Godavari']
        applying_for = ['speaker', 'analyst', 'content writer']

        all_data = {}
        for dist in districts:
            district = {}

            district['total'] = 0
            district['speaker'] = 0
            district['analyst'] = 0
            district['content writer'] = 0
            filter_query = "district eq '" + dist + "'"
            select = 'Name'
            marker = {}
            marker['nextpartitionkey'] = None
            marker['nextrowkey'] = None
            status = True

            for position in applying_for:
                marker = {}
                marker['nextpartitionkey'] = None
                marker['nextrowkey'] = None
                filter_query = "district eq '" + dist + "' and applying_for eq '" + str(position) + "'"
                select = 'Name'
                position_status = True

                while position_status:
                    pos_entities = table_service.query_entities('resourcepersons', num_results=1000,
                                                                filter=filter_query, select=select, marker=marker,
                                                                accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
                                                                property_resolver=None, timeout=None)
                    pos_ent = list(pos_entities)

                    district[position] = district[position] + len(pos_ent)
                    district['total'] = district['total'] + len(pos_ent)
                    x_ms_continuation = {}

                    if hasattr(pos_entities, 'next_marker'):

                        marker = getattr(pos_entities, 'next_marker')
                        if not marker:
                            position_status = False
                        else:
                            position_status = True


                    else:
                        position_status = False
            all_data[dist] = district
        if all_data:
            html_str = """<html><head><style>.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006699; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 15px; font-weight: bold; border-left: 1px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00496B; border-left: 1px solid #E1EEF4;font-size: 12px;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #006699;background: #E1EEF4;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid table tfoot td ul { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid table tfoot  li { display: inline; }.datagrid table tfoot li a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #006699;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; }.datagrid table tfoot ul.active, .datagrid table tfoot ul a:hover { text-decoration: none;border-color: #006699; color: #FFFFFF; background: none; background-color:#00557F;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }</style></head><body><div class="datagrid"><table width='' border='1'><thead><tr><th>District </th><th>Total</th><th>Analyst</th><th>Speaker </th><th>Content Writer </th></tr></thead><tbody>{{rows}}</tbody></table></div></body></html>"""
            rows_str = ""
            for key in all_data.keys():
                print key
                print all_data[key]
                rows_str = rows_str + "<tr><td>" + str(key) + "</td><td>" + str(
                    all_data[key]['total']) + "</td><td>" + str(all_data[key]['analyst']) + "</td><td>" + str(
                    all_data[key]['speaker']) + "</td><td>" + str(all_data[key]['content writer']) + "</td></tr>"
            html_str = html_str.replace("{{rows}}", rows_str)
            sendmailmandrill('JANASENAPARTY', 'rambabu.v68@gmail.com', html_str, 'Resource Persons Counts')
            print "mail sentttttttttttttttttttt "

        return html_str


    except Exception as e:
        return str(e)


@app.route('/getVenueDetails', methods=['GET', 'POST', 'PUT'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def getVenueDetails():
    if request.method == 'GET':
        mobile_number = request.values.get('mobile')
        voter_id = request.values.get('voter_id')
        email = request.values.get('email')
        aadhar_id = request.values.get('aadhar_id')

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            if mobile_number is not None and mobile_number != '':
                cur.execute("select * from facilitators where mobile=%s", (mobile_number,))
                existing_row = cur.fetchone()
                if existing_row is None:
                    cur.execute(
                        "select name,assembly,district,mobile,email,applying_for,age,gender,voter_id,aadhar_id,details_status from resourcepersons where mobile=%s and attendance_status=%s",
                        (mobile_number, 'T'))
                else:
                    return json.dumps(
                        {"errors": ['Details already Existed in Facilitators DB'], 'data': {'status': '0'}})

            elif email is not None and email != '':

                cur.execute(
                    "select name,assembly,district,mobile,email,applying_for,age,gender,voter_id,aadhar_id,details_status from resourcepersons where lower(email)=%s and attendance_status=%s",
                    (str(email).lower(), 'T'))
            elif voter_id is not None and voter_id != '':

                cur.execute(
                    "select name,assembly,district,mobile,email,applying_for,age,gender,voter_id,aadhar_id,details_status from resourcepersons where lower(voter_id)=%s and attendance_status=%s",
                    (str(voter_id).lower(), 'T'))
            elif aadhar_id is not None and aadhar_id != '':

                cur.execute(
                    "select name,assembly,district,mobile,email,applying_for,age,gender,voter_id,aadhar_id,details_status from resourcepersons where aadhar_id=%s and attendance_status=%s",
                    (aadhar_id, 'T'))
            row = cur.fetchone()
            con.close()
            children = row
            if row:
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': children}})

            return json.dumps({"errors": ['Details Not Found'], 'data': {'status': '0'}})
        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Something Wrong . Please Try Again'], 'data': {'status': '0'}})
    if request.method == 'POST':
        mobile = request.values.get('mobile')

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from facilitators where mobile=%s", (mobile,))
            row = cur.fetchone()
            if row:
                return json.dumps({"errors": ["already existed"], 'data': {'status': '0'}})
            name = request.values.get("name")
            email = request.values.get("email")
            age = request.values.get("age")
            gender = request.values.get("gender")
            address = request.values.get("address")
            district = request.values.get("district")
            assembly = request.values.get("assembly")
            profession = request.values.get("profession")
            qualification = request.values.get("qualification")
            voter_id = request.values.get("voter_id")
            aadhar_id = request.values.get("aadhar_id")
            association = request.values.get("association")
            status = request.values.get("status")
            cur.execute(
                "insert into facilitators (name,email,mobile,age,gender,address,district,assembly,qualification,profession,aadhar_id,voter_id,association,facilitator_attendance,status)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning seq_id",
                (name, email, mobile, age, gender, address, district, assembly, qualification, profession, aadhar_id,
                 voter_id, association, 'T', status))
            con.commit()
            row = cur.fetchone()
            con.close()

            return json.dumps({"errors": [], 'data': {'status': '1', 'form_id': row['seq_id']}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})
    if request.method == 'PUT':
        mobile = request.values.get('mobile')
        post = request.values.get('alloted_post')
        form_id = request.values.get('form_id')
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("update facilitators set selected=%s,alloted_post=%s where mobile=%s and form_id=%s",
                        ('T', post, mobile, form_id))
            con.commit()
            return json.dumps({"errors": [], 'data': {'status': '1'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})


@app.route('/getfacilitatorDetails', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def getfacilitatorDetails():
    if request.method == 'GET':
        mobile_number = request.values.get('mobile')
        voter_id = request.values.get('voter_id')
        email = request.values.get('email')
        aadhar_id = request.values.get('aadhar_id')

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            if mobile_number is not None and mobile_number != '':
                cur.execute(
                    "select seq_id,name,assembly,district,mobile,email,age,gender from resourcepersons where mobile=%s",
                    (mobile_number,))


            elif email is not None and email != '':

                cur.execute(
                    "select seq_id,name,assembly,district,mobile,email,age,gender from resourcepersons where lower(email)=%s ",
                    (str(email).lower(),))
            elif voter_id is not None and voter_id != '':

                cur.execute(
                    "select seq_id,name,assembly,district,mobile,email,age,gender from resourcepersons where lower(voter_id)=%s ",
                    (str(voter_id).lower(),))
            elif aadhar_id is not None and aadhar_id != '':

                cur.execute(
                    "select seq_id,name,assembly,district,mobile,email,age,gender from resourcepersons where aadhar_id=%s ",
                    (aadhar_id,))
            row = cur.fetchone()
            con.close()
            children = row
            if row:
                return json.dumps({"errors": [], 'data': {'status': '1', 'children': children}})

            return json.dumps({"errors": ['Details Not Found'], 'data': {'status': '0'}})
        except Exception as e:
            print str(e)
            return json.dumps({"errors": ['Something Wrong . Please Try Again'], 'data': {'status': '0'}})


@app.route('/postallotments', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
def postallotments():
    if request.method == 'GET':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select seq_id,post_name from member_posts where status='A'")
            rows = cur.fetchall()
            data = []
            if len(rows):
                data = rows

        except Exception as e:
            print str(e)
            return str(e)
        return render_template("postallotment.html", data=data)
    if request.method == 'POST':
        mobile = request.values.get('mobile')
        post = request.values.get('alloted_post')
        form_id = request.values.get('form_id')
        print mobile, post, form_id
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("update resourcepersons set post_allotment=%s where mobile=%s and seq_id=%s",
                        ('T', mobile, form_id))
            cur.execute("insert into member_alloted_posts(resource_person_id,post_id)values(%s,%s)", (form_id, post))
            con.commit()
            return json.dumps({"errors": [], 'data': {'status': '1'}})

        except Exception as e:
            print str(e)
            return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})


@app.route('/writeConstituenciescount', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeConstituenciescount():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select state_id,constituency_name,constituency_id from assembly_constituencies where state_id<=2 order by constituency_id ")
        rows =cur.fetchall()
        
        for ac in rows:
            assembly=row[2]
            state_id=row[0]
            
            cur.execute(
                "select state,ac_no,sex,count (*) from voters group by sex,ac_no,state having  ac_no=%s and state=%s",
                (assembly, state_id))
            rows = cur.fetchall()
            total_voters = 0
            male_voters = 0
            female_voters = 0

            for row in rows:
                total_voters = total_voters + row[3]
                
                if str(row[2]).rstrip() == 'Male':
                    male_voters = male_voters + row[3]
                elif str(row[2]).rstrip() == 'Female':
                    female_voters = female_voters + row[3]
            print total_voters, male_voters, female_voters
            cur.execute(
                "update assembly_constituencies set total_votes=%s,male_votes=%s,female_votes=%s where constituency_id=%s and state_id=%s",
                (total_voters, male_voters, female_voters, id, 2))
            con.commit()

        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writeConstituenciesagecount', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeConstituenciesagecount():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute(
            "select constituency_name,constituency_id from assembly_constituencies where state_id=%s order by constituency_id ",
            (2,))
        rows = cur.fetchall()

        for row in rows:
            assembly = row[0]

            ac_id = row[1]
            cur.execute(
                "select state,ac_no,sex,case when age between 0 and 24 then '0-24' when age between 25 and 34 then '25-34' when age between 35 and 44 then '35-44' when age between 45 and 54 then '45-54' when age >=55 then '>=55' END as age_range, count(*) from voters group by ac_no,state,age_range,sex having ac_no=%s and state =%s and sex in ('Male','Female') order by state,ac_no ,age_range;",
                (ac_id, 2))
            assembly_rows = cur.fetchall()

            for row in assembly_rows:
                print row
                if row[4] is None:
                    count = 0
                else:
                    count = row[4]

                cur.execute(
                    "insert into age_range(state_id,type_id,type,category,age_range,count,gender)values(%s,%s,%s,%s,%s,%s,%s)",
                    (row[0], row[1], 'A', 'V', row[3], count, row[2]))
                con.commit()

        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writedistrictcount', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writedistrictcount():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select district_name from districts where state_id<=2 ")
        prows = cur.fetchall()
        for row in prows:
            district_name = str(row[0]).rstrip()

            print district_name
            cur.execute(
                "select total_votes,male_votes,female_votes from assembly_constituencies where district_name=%s",
                (district_name,))
            crows = cur.fetchall()
            print crows
            total_voters = 0
            male_voters = 0
            female_voters = 0

            for item in crows:
                total_voters = total_voters + item[0]

                male_voters = male_voters + item[1]

                female_voters = female_voters + item[2]
            print total_voters, male_voters, female_voters
            cur.execute(
                "update districts set total_votes=%s,male_votes=%s,female_votes=%s where district_name=%s ",
                (total_voters, male_voters, female_voters, district_name))
            con.commit()

        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writeConstituencies', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeConstituencies():
    try:
        assembly_info = "./telangana.json"

        json_data = open(assembly_info).read()

        data = json.loads(json_data)
        children = {}
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        for record in data:
            cur.execute("select constituency_name,constituency_id from assembly_constituencies where state_id=%s ",
                        (2,))
            rows = cur.fetchall()
            for row in rows:
                assembly = row[0]
                id = row[1]
                for item in data:
                    if str(id) == str(item['AC No']):
                        cur.execute(
                            "update assembly_constituencies set parliament_constituency_name=%s where constituency_id=%s and state_id=%s",
                            (item['PC Name'], id, 2))
                        con.commit()
                        break

        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writePConstituencies', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writePConstituencies():
    try:

        children = {}
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select parliament_constituency_name from parliament_constituency ")
        rows = cur.fetchall()
        print rows
        for row in rows:
            pc = str(row[0]).rstrip()
            cur.execute("select district_name from assembly_constituencies where parliament_constituency_name=%s",
                        (pc,))
            dist = cur.fetchone()
            print dist
            cur.execute("update parliament_constituency set district_name=%s where parliament_constituency_name=%s ",
                        (dist[0], pc))
            con.commit()

        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


# @app.route('/resourcepersonsDashboard',methods=['GET'])
# @crossdomain(origin="*",headers="Content-Type")

# def resourcepersonsDashboard():

@app.route('/janasenaDashboard', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def janasenaDashboard():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select country_name,count from  nriconnect_counts where country<>'in' order by count desc limit 10;")
        nri_connect_data = cur.fetchall()
        cur.execute("select policy,count from  janaswaram_counts  order by count desc limit 10;")
        janaswaram_data = cur.fetchall()
        cur.execute(
            "select district,total,speaker,analyst,content_writer from resourcepersons_counts where district not in %s order by total desc;",
            (tuple(['Vishakhapatnam', 'visakhapatnam']),))
        resourcepersons_data = cur.fetchall()
        data = {}
        data['nri_connect_data'] = nri_connect_data
        data['janaswaram_data'] = janaswaram_data
        data['resourcepersons_data'] = resourcepersons_data
        return json.dumps({"errors": [], 'data': {'status': '1', 'children': data}})


    except Exception as err:
        print str(err)
        return json.dumps({"errors": [str(err)], 'data': {'status': '0'}})


@app.route('/janasenaAnalytics', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def janasenaAnalytics():
    return render_template("admin/janasenadashboard.html")


@app.route('/slide', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def slide():
    return render_template("admin/slide.html")


# @app.route('/writeresourcepersons', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# def writeresourcepersons():
#     try:
#         id_no = request.values.get('id')
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor()
#         offset = 5 * int(id_no)
#         cur.execute(
#             "select distinct(district_name) from assembly_constituencies order by district_name limit %s offset %s  ",
#             (5, offset))
#         rows = cur.fetchall()
#         districts = []
#         for row in rows:
#             if row[0].rstrip() == "Vishakhapatnam":
#                 districts.append('Visakhapatnam')
#             elif row[0].rstrip() == "Mahabubnagar":
#                 districts.append('Mahaboobnagar')
#             else:
#                 districts.append(row[0].rstrip())
#         print districts
#         applying_for = ['speaker', 'analyst', 'content writer']
#
#         for dist in districts:
#             if dist == 'Hyderabad':
#                 table = 'ghmcresourcepersons'
#
#             elif dist in ['Vizianagaram', 'Srikakulam', 'Visakhapatnam']:
#                 table = 'uttarandhraresourcepersons'
#             else:
#                 table = 'resourcepersons'
#
#             district = {}
#
#             district['total'] = 0
#             district['Male'] = 0
#             district['Female'] = 0
#
#             filter_query = "district eq '" + dist + "'"
#             select = 'Name'
#             marker = {}
#             marker['nextpartitionkey'] = None
#             marker['nextrowkey'] = None
#
#             for g in gender:
#
#                 marker = {}
#                 marker['nextpartitionkey'] = None
#                 marker['nextrowkey'] = None
#                 if dist == 'Hyderabad':
#                     filter_query = "attendance_status eq 'T' "
#                 else:
#                     filter_query = "district eq '" + dist + "' and attendance_status eq 'T'"
#                 select = 'Name'
#                 position_status = True
#
#                 while position_status:
#                     pos_entities = table_service.query_entities(table, num_results=1000, filter=filter_query,
#                                                                 select=select, marker=marker,
#                                                                 accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
#                                                                 property_resolver=None, timeout=None)
#                     pos_ent = list(pos_entities)
#                     district['total'] = district['total'] + len(pos_ent)
#                     # district[position]=district[position]+len(pos_ent)
#                     x_ms_continuation = {}
#
#                     if hasattr(pos_entities, 'next_marker'):
#
#                         marker = getattr(pos_entities, 'next_marker')
#                         if not marker:
#                             position_status = False
#                         else:
#                             position_status = True
#
#
#                     else:
#                         position_status = False
#             try:
#
#                 con = psycopg2.connect(config.DB_CONNECTION)
#                 cur = con.cursor()
#                 cur.execute(
#                     "select district,total,speaker,analyst,content_writer from resourcepersons_counts where district=%s ",
#                     (dist,))
#                 row = cur.fetchone()
#                 if row is not None:
#                     # speakers_attended=%s,analysts_attended=%s,content_writer_attended=%s
#                     # district['speaker'],district['analyst'],district['content writer'],
#                     cur.execute("update resourcepersons_counts set total_attended=%s where district=%s",
#                                 (district['total'], dist))
#                     con.commit()
#                 # else:
#                 #     cur.execute("insert into  resourcepersons_counts(district,total_attended,speaker,analyst,content_writer)values(%s,%s,%s,%s,%s)  ",(dist,district['total'],district['speaker'],district['analyst'],district['content writer']))
#                 #     con.commit()
#             except Exception as e:
#                 print str(e)
#                 return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})
#
#         return json.dumps({"errors": [], 'data': {'status': '1'}})
#
#
#     except Exception as e:
#         return str(e)
#         return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/writeTsConstituencies', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeTsConstituencies():
    try:
        assembly_info = "./telangana.json"

        json_data = open(assembly_info).read()

        data = json.loads(json_data)
        children = {}
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        for record in data:
            cur.execute(
                "insert into assembly_constituencies(constituency_name,state_id,constituency_id,district_name)values(%s,%s,%s,%s)",
                (record["AC Name"], 2, record["AC No"], record["Old District"]))
        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writeApConstituencies', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeApConstituencies():
    try:
        assembly_info = "./pcnames.json"

        json_data = open(assembly_info).read()

        data = json.loads(json_data)
        children = {}
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        for record in data:
            cur.execute(
                "insert into assembly_constituencies(constituency_name,state_id,constituency_id,district_name)values(%s,%s,%s,%s)",
                (record["AC Name"], 1, record["AC. No"], record["District"]))
        con.commit()
        con.close()


    except Exception as e:
        print str(e)
    return 'True'


@app.route('/writenriconnect', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writenriconnect():
    try:
        codes_file = "static/nri_codes.json"
        json_data = open(codes_file).read()
        data = json.loads(json_data)

        for item in data:

            country = item['Code'].lower()
            country_name = item['Name']
            print country_name
            marker = {}
            marker['nextpartitionkey'] = None
            marker['nextrowkey'] = None

            filter_query = "country eq '" + country + "'"
            select = 'name'
            position_status = True
            count = 0
            while position_status:
                pos_entities = table_service.query_entities('janasenavolunteer', num_results=1000, filter=filter_query,
                                                            select=select, marker=marker,
                                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
                                                            property_resolver=None, timeout=None)
                pos_ent = list(pos_entities)
                count = count + len(pos_ent)

                x_ms_continuation = {}

                if hasattr(pos_entities, 'next_marker'):

                    marker = getattr(pos_entities, 'next_marker')
                    if not marker:
                        position_status = False
                    else:
                        position_status = True


                else:
                    position_status = False
            try:

                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("select country,count from nriconnect_counts where country=%s ", (country,))
                row = cur.fetchone()

                if row is not None:
                    cur.execute("update nriconnect_counts set count=%s where country=%s", (count, country))
                    con.commit()
                else:
                    cur.execute("insert into  nriconnect_counts(country,count,country_name)values(%s,%s,%s)  ",
                                (country, count, country_name))
                    con.commit()

            except Exception as e:

                print str(e)
                return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})
                con.close()
        con.close()
        return json.dumps({"errors": [], 'data': {'status': '1'}})




    except Exception as e:
        return str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/writejanaswaram', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writejanaswaram():
    try:
        codes_file = "static/policies.json"
        json_data = open(codes_file).read()
        data = json.loads(json_data)
        for item in data:

            policy = item['policy'].rstrip()
            print policy
            marker = {}
            marker['nextpartitionkey'] = None
            marker['nextrowkey'] = None

            filter_query = "Policy eq '" + policy + "'"
            select = 'Name'
            position_status = True
            count = 0
            while position_status:
                pos_entities = table_service.query_entities('janaswaram', num_results=1000, filter=filter_query,
                                                            select=select, marker=marker,
                                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
                                                            property_resolver=None, timeout=None)
                pos_ent = list(pos_entities)
                count = count + len(pos_ent)

                x_ms_continuation = {}

                if hasattr(pos_entities, 'next_marker'):

                    marker = getattr(pos_entities, 'next_marker')
                    if not marker:
                        position_status = False
                    else:
                        position_status = True


                else:
                    position_status = False
            try:

                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("select policy,count from janaswaram_counts where policy=%s ", (policy,))
                row = cur.fetchone()

                if row is not None:
                    cur.execute("update janaswaram_counts set count=%s where policy=%s", (count, policy))
                    con.commit()
                else:
                    cur.execute("insert into  janaswaram_counts(policy,count)values(%s,%s)  ", (policy, count))
                    con.commit()

            except Exception as e:

                print str(e)
                return json.dumps({"errors": [str(e)], 'data': {'status': '0'}})
                con.close()
        con.close()
        return json.dumps({"errors": [], 'data': {'status': '1'}})




    except Exception as e:
        return str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/janasenaSocialMediaDashboard', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def janasenaSocialMediaDashboard():
    return render_template("admin/socialmedia.html")


@app.route('/socialmediaDashboard', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def socialmediaDashboard():
    try:
        fb_data = json.loads(fb.get_page_fan_count())

        twitter_data = fb.api.me().followers_count
        youtube_data = json.loads(fb.get_youtube_data())

        insta_followers = 0
        try:
            insta_data = json.loads(fb.get_instagram_followers())
            insta_followers = insta_data['data']['counts']['followed_by']
            print insta_followers

        except Exception as e:
            print str(e)

        fb_insights = json.loads(fb.get_page_insights())
        data = {}
        data['instagram'] = insta_followers
        country_wise_story_tellers = []
        if 'data' in fb_insights.keys():
            for item in fb_insights['data']:
                if item['name'] == 'page_fans_country':
                    try:
                        country_wise_fans = item['values'][0]['value']

                        data['country_wise_fans'] = country_wise_fans
                    except Exception as e:
                        data['country_wise_fans'] = []

                elif item['name'] == 'page_storytellers_by_country' and item['period'] == "days_28":
                    try:
                        values = item['values'][0]['value']
                        top_five_countries_story_tellars = (
                            sorted(values.iteritems(), key=operator.itemgetter(1), reverse=True)[:5])

                        for item in top_five_countries_story_tellars:
                            d = {}

                            d['country'] = item[0]
                            d['story_tellers'] = item[1]
                            country_wise_story_tellers.append(d)
                    except Exception as e:
                        country_wise_story_tellers = []
                    data['country_wise_story_tellers'] = country_wise_story_tellers
                elif item['name'] == 'page_fans_gender_age':
                    try:
                        values = item['values'][0]['value']

                        page_fans_gender_age = {}
                        for key in values.keys():

                            split_data = key.split(".")
                            if split_data[1] in page_fans_gender_age.keys():
                                if split_data[0] == "M":
                                    page_fans_gender_age[split_data[1]]['Male'] = values[key]
                                elif split_data[0] == "F":

                                    page_fans_gender_age[split_data[1]]['Female'] = values[key]
                                else:
                                    page_fans_gender_age[split_data[1]]['Transgender'] = values[key]
                            else:
                                page_fans_gender_age[split_data[1]] = {"Male": 0, "Female": 0, "Transgender": 0}
                                if split_data[0] == "M":
                                    page_fans_gender_age[split_data[1]]['Male'] = values[key]
                                elif split_data[0] == "F":

                                    page_fans_gender_age[split_data[1]]['Female'] = values[key]
                                else:
                                    page_fans_gender_age[split_data[1]]['Transgender'] = values[key]
                    except Exception as e:
                        page_fans_gender_age = {}

                    fans_gender_age = (sorted(page_fans_gender_age.iteritems(), key=operator.itemgetter(0)))

                    page_fans_gender_age_sorted = []
                    for item in fans_gender_age:
                        d = {}
                        d['age_range'] = item[0]
                        d['Male'] = item[1]['Male']
                        d['Female'] = item[1]['Female']
                        d['Transgender'] = item[1]['Transgender']
                        page_fans_gender_age_sorted.append(d)
                    data['page_fans_gender_age'] = page_fans_gender_age_sorted

        if 'fan_count' in fb_data.keys():
            data['fan_count'] = fb_data['fan_count']

        else:
            data['fan_count'] = "NA"
        if twitter_data is not None:
            data['twitter_count'] = twitter_data
        else:
            data['twitter_count'] = "NA"
        if 'items' in youtube_data.keys():

            if 'statistics' in youtube_data['items'][0].keys():

                data['youtube_data'] = youtube_data['items'][0]['statistics']
            else:

                data['youtube_data'] = "NA"
        else:
            data['youtube_data'] = "NA"
        return json.dumps({'errors': [], "data": {'children': data, 'status': '1'}})
    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['some thing went wrong'], "data": {'status': '0'}})


# resource persons


@app.route('/get_constituencieslist', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def get_constituencieslist():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        cur.execute("select district_name,constituency_name from assembly_constituencies order by district_name ")
        rows = cur.fetchall()
        assemblies = []
        for row in rows:
            d = {}
            d['district'] = row[0].rstrip()
            d['assembly'] = row[1].rstrip()
            assemblies.append(d)

        return json.dumps({'data': assemblies})
    except Exception as e:
        print str(e)
        return "False"


# @app.route("/writevoters",methods=['GET','POST'])
# @crossdomain(origin="*",headers="Content-Type")
# def writevoters():
#     try:

#         import mssql

#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor()

#         for i in range(111,120):
#             start_time=datetime.now()
#             print start_time
#             database_name="S29_AC"+str(i).zfill(3)
#             table_name="AC_"+str(i).zfill(3)
#             print database_name
#             print table_name
#             rows=mssql.getting_voters(database_name,table_name)
#             fetching_time=datetime.now()
#             ascii_voters=[]
#             for row in rows:
#                 try:
#                     voter_id=str(row[1]).strip()

#                     name=strip_non_ascii(row[2])

#                     name_str=str(name).strip()

#                     relation_name=row[3]

#                     try:
#                         cur.execute("insert into voters(serial_no,voter_id,voter_name,relation_name,relation_type,house_no,age,sex,section_no,section_name,part_no,page_no,ac_no,state)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",(row[0],voter_id.lower(),name,relation_name,row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],'2'))
#                         con.commit()
#                     except psycopg2.Error as e:
#                         print str(e)
#                         con.rollback()
#                 except Exception as e:
#                     print str(e)
#                     con.rollback()
#                     print row[1]
#                     ascii_voters.append(row[1])


#             end_time=datetime.now()
#             fetch_time=fetching_time-start_time
#             completed_time=end_time-start_time
#             write_time=end_time-fetching_time
#             print "fetch_time is "+ str(fetch_time.seconds)
#             print "write_time is "+ str(write_time.seconds)
#             print "Total_time is "+ str(completed_time.seconds)
#             print len(ascii_voters)
#         return json.dumps({'missed_voters':ascii_voters})

#     except Exception as e:
#         print str(e)
#         return "exception occured"


#################### membership services ###################################


# @app.route("/membership",methods=['POST','GET','OPTIONS'])
# @crossdomain(origin="*",headers="Content-Type")

# def membership():
#     return render_template("membership/membership_launching.html")


## service to verify otp and give voter details


# @app.route("/saveMemberProfileImageApp",methods=['POST','OPTIONS'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# #@requires_authToken
# def saveMemberProfileImageApp():
#     try:

#         membership_id=request.values.get('membership_id')
#         if membership_id is None or membership_id =='':
#            return json.dumps({"errors":['membership_id missing'],"data":{"status":"0"}})
#         membership_instance=Membership_details()
#         if membership_instance.check_membership_existence(membership_id):
#             con=None
#             con = psycopg2.connect(config.DB_CONNECTION)
#             cur = con.cursor(cursor_factory=RealDictCursor)
#             try:
#                 profile_image=''

#                 if request.files:
#                     profile_image=request.files['file']
#                     img_url=upload_image(profile_image,'membershipimages')
#                     cur.execute("update janasena_membership set image_url=%s where membership_id=%s",(img_url,membership_id))
#                     con.commit()
#                     con.close()
#                     return json.dumps({"errors":[],"data":{"status":"1"}})
#                 else:
#                     return json.dumps({"errors":['please upload file'],"data":{"status":"0"}})
#             except (KeyError,Exception) as e:
#                 print 'cpi'
#                 print str(e)
#                 con.close()
#                 return json.dumps({"errors":['something wrong'],"data":{"status":"0"}})
#         return json.dumps({"errors":['Membership_id not found'],"data":{"status":"0"}})

#     except psycopg2.Exception as e:
#         print str(e)
#         con.close()
#         return json.dumps({"errors":['something wrong in data'],"data":{"status":"0"}})

## mobile app service
## commented before live
# @app.route("/getVoterDetails",methods=['GET','POST','OPTIONS'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# def getVoterDetails():
#     try:
#         try:
#             voter_id=request.json['voter_id']

#             phone=request.json['mobile_num']
#             otp=request.json['otp']
#         except (KeyError,Exception) as e:
#             return json.dumps({"errors":["Parameter "+str(e)+" Missing"],'data':{"status":"0"}})
#         if voter_id is None or voter_id.isspace():
#             return json.dumps({"errors":["invalid Voter_id .please try again"],"data":{"status":"0"}})
#         if otp is None or otp.isspace():
#             return json.dumps({"errors":["invalid otp given"],"data":{"status":"0"}})
#         if phone is None or phone.isspace():
#             return json.dumps({"errors":["invalid phone Number. please try again"],"data":{"status":"0"}})
#         if verifyOtp(phone,otp,"MB"):

#             children={}

#             voterdetails_instance=Voter_details()
#             voter_id=str(voter_id).lower()
#             record=voterdetails_instance.get_voter_details(voter_id)

#             if record is not None:
#                 try:
#                     try:
#                         children['Name']=str(record['voter_name']).rstrip()
#                     except Exception as e:
#                         children['Name']=''
#                     try:
#                         children['relation_name']=str(record['relation_name']).rstrip()
#                     except Exception as e:
#                         children['relation_name']=''

#                     children['relation_type']=record['relation_type']
#                     children['Age']=record['age']
#                     children['Sex']=record['sex']
#                     children['House_No']=record['house_no']
#                     children['Booth No']=record['part_no']

#                     children['booth_name']=record['section_name']
#                     children['Section_id']=str(record['ac_no']).zfill(3)
#                     children['state']=record['state']

#                     try:
#                         con = psycopg2.connect(config.DB_CONNECTION)
#                         cur = con.cursor()
#                         cur.execute("select constituency_name,district_name from assembly_constituencies where constituency_id=%s and state_id=%s",(children['Section_id'],children['state']))
#                         row=cur.fetchone()
#                         con.close()
#                         if row is not None:
#                             children['constituency_name']=row[0]
#                             children['district_name']=row[1]
#                         else:
#                             children['constituency_name']=''
#                             children['district_name']=''

#                     except Exception as e:
#                         print str(e)
#                 except Exception as e:
#                     print str(e)

#                 session['member_details']=children

#                 return json.dumps({"errors":[],'data':{'status':'1','children':children}})
#             return json.dumps({"errors":['no record found with the given details'],'data':{'status':'0','children':children}})
#         else:
#             return json.dumps({"errors":['invalid otp'],'data':{'status':'0'}})
#     except Exception as e:
#         print str(e)
#         return json.dumps({"errors":['Exception Occured'],'data':{'status':'0'}})





## comented before live app service
# @app.route("/getMembershipID",methods=['POST'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# @requires_authToken
# def getMembershipID():

#     if request.method=='POST':
#         try:

#             phone=request.json['phone']
#             voter_id=request.json['voter_id']
#             name=request.json['name'].rstrip()
#             email=request.json['email']
#             relation_name=request.json['relation']
#             relation_type=request.json['relation_type']
#             address=str(request.json['address']).rstrip()
#             town=request.json['town']
#             age=request.json['age']
#             gender=request.json['gender']
#             booth=request.json['booth']
#             assembly_id=request.json['assembly_id']
#             booth_name=request.json['booth_name']
#             state=request.json['state']
#             referral_id=request.json['referral_id']
#             if state=='1':
#                 state="AP"
#             else:
#                 state='TS'
#         except (Exception,KeyError) as e:
#             print str(e)
#             return json.dumps({"errors":['parameter '+ str(e)+" missing"],'data':{"status":'0'}})

#         try:

#             voter_id=str(voter_id).lower()
#             evoter_id=encrypt_decrypt(voter_id,'E')
#             ephone=encrypt_decrypt(phone,'E')
#             membership_instance=Membership_details()

#             if membership_instance.check_voterid_existence(evoter_id):
#                 if membership_instance.check_phonenumber_count(ephone):

#                     status,membership_id=generate_membershipid(name,phone,voter_id,relation_name,relation_type,address,town,age,gender,booth,assembly_id,state,email,booth_name,referral_id,'M')
#                     if status:


#                         return json.dumps({"errors": [],'data':{'status':'1','membership_id':membership_id,'name':name,'phone':phone}})

#                     return json.dumps({"errors": ['something went wrong,please try again'],'data':{'status':'0'}})

#                 return json.dumps({"errors": ['This Voter Id already existed'],'data':{'status':'0'}})


#             return json.dumps({"errors": ['This Voter Id already existed'],'data':{'status':'0'}})

#         except Exception as e:
#             print str(e)

#             return json.dumps({"errors": [str(e)],'data':{'status':'0'}})


# def generate_membershipid(name,phone,voter_id,relation_name,relation_type,address,town,age,gender,booth,assembly_id,state,email,booth_name,referral_id,membership_through):
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor()
#         try:
#             n_voter_id=voter_id
#             n_phone=phone
#             phone=encrypt_decrypt(phone,'E')
#             if voter_id!='':
#                 voter_id=encrypt_decrypt(voter_id,'E')
#             n_email=''
#             if email is not None and email!='':

#                 n_email=encrypt_decrypt(email,'E')
#         except Exception as e:
#             print str(e)


#         user_status=''
#         existing=db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(Janasena_Missedcalls.phone==n_phone).first()

#         if existing is None:
#             newRecord = Janasena_Missedcalls(phone =n_phone,member_through = 'W')
#             db.session.add(newRecord)
#             db.session.commit()

#             member_id=newRecord.jsp_supporter_seq_id
#             user_status='F'

#         else:

#             membership_instance=Membership_details()
#             if membership_instance.check_phonenumber_existence(phone):

#                 user_status='E'
#                 member_id=existing[0]

#             else:
#                 newRecord = Janasena_Missedcalls(phone=n_phone,member_through = 'W')
#                 db.session.add(newRecord)
#                 db.session.commit()
#                 member_id=newRecord.jsp_supporter_seq_id
#                 user_status='F'
#         jsp_membership_id="JSP"+str(member_id).zfill(8)

#         cur.execute("insert into janasena_membership (name,phone,voter_id,relationship_name,relation_type,address,village_town,age,gender,polling_station_id,constituency_id,state,email,polling_station_name,referral_id,membership_through,membership_id)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ",(name,phone,voter_id,relation_name,relation_type,address,town,age,gender,booth,assembly_id,state,n_email,booth_name,str(referral_id).upper(),membership_through,jsp_membership_id))

#         con.commit()
#         con.close()
#         if config.ENVIRONMENT_VARIABLE == 'local':
#             clear_and_update_voter(n_voter_id,n_phone,jsp_membership_id)
#         else:


#             clear_and_update_voter.delay(n_voter_id,n_phone,jsp_membership_id)
#         if user_status=='F':
#             try:
#                 numberToSend=str(n_phone)

#                 if config.ENVIRONMENT_VARIABLE == 'local':
#                     send_membership_sms({"phone_number":numberToSend,"membership_id":jsp_membership_id,'email':email})

#                 else:
#                     send_membership_sms.delay({"phone_number":numberToSend,"membership_id":jsp_membership_id,'email':email})

#             except Exception as e:
#                 print str(e)

#         return (True,jsp_membership_id)
#     except Exception as e:
#         print str(e)
#         return (False,'Exception')


# @app.route("/getMembershipCardMobile",methods=['GET'])

# def getMembershipCardMobile():
#     membership_id=request.values.get('id')
#     if membership_id is None or membership_id=="":
#         abort(400)
#     membership_instance=Membership_details()
#     details=membership_instance.get_membership_details(membership_id,None)
#     d={}
#     if details is not None:

#         name=details['name']
#         phone=details['phone']
#         membership_id=details['membership_id']
#         status=details['membership_through']

#         return render_template("membership/membership_cardmobile.html",membership_id=membership_id,name=name,phone=phone,status=status)
#     else:
#         return """<h2><div align='center' style="margin-top:100px;" > Not a valid membership id. please try again.</div></h2>"""


@app.route("/sendPollingBoothDetails", methods=['POST'])
def sendPollingBoothDetails():
    email = request.values.get("email")
    try:
        membership_id = session["membership_id"]
        session['card_details']['volunteer_status'] = 'T'

    except (KeyError, Exception) as e:
        print str(e)
        return json.dumps({"errors": ['Your Session is  Expired. Please try again'], 'data': {'status': '0'}})

    errors = []

    if membership_id is None or membership_id == "":
        errors.append("invalid membership_id")
    if len(errors):
        return json.dumps({"errors": errors, "data": {"status": '0'}})
    try:

        membership_instance = Membership_details()

        membership_instance.check_and_update_volunteer_status(membership_id, email)
        if config.ENVIRONMENT_VARIABLE != "local":
            data = json.dumps({"membership_id": membership_id, "email": email})
            

        return json.dumps({"errors": [], "data": {"status": '1'}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


# commented before Live
# @app.route('/volunteerResendOTP', methods=['GET','POST','OPTIONS'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# def volunteerResendOTP():
#     try:
#         if request.method=='POST':
#             umobile=request.json['mobile_number']
#             if umobile is None and umobile=="":
#                 return json.dumps({'errors': ['mobile_num missing'],'data' : {'status':'0'}})
#             otp=genOtp(umobile)


#             js= json.dumps({'errors': [],'data' : {'status':'1','msg':'otp sent  Successfully','otp':otp}})
#             return js

#         else:
#             js= json.dumps({'errors': ['Not Allowed'],'data' : {'status':'0'}})
#             return js
#     except Exception as e:
#         print str(e)
#         js= json.dumps({'errors': ['Something Went Wrong, Please try again later'],'data' : {'status':'0'}})


# @app.route('/getPollingBoothVotersList', methods=['GET'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# @requires_authToken
# def getPollingBoothVotersList():
#     voter_id=request.values.get("voter_id")
#     page_no=request.values.get("page_no")
#     if voter_id is None or voter_id=='':
#         return json.dumps({"errors":['You have not provided your Voter ID '],'data':{"status":'0'}})
#     if page_no is None:
#         page_no=1
#     voters_instance=Voter_details()
#     voter_list=voters_instance.get_polling_booth_voters(voter_id,page_no)
#     if voter_list is None:
#         voter_list=[]
#     return json.dumps({"errors":[],'data':{"status":"1","children":voter_list}})


@app.route('/getVolunteerProfile', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
#@requires_authToken
def getVolunteerProfile():
    try:
        membership_id = request.values.get('membership_id')
        if membership_id is None:
            return json.dumps({"errors": ['Parameter membership_id missing'], 'data': {"status": '0'}})
        membership_instance = Membership_details()
        profile = membership_instance.get_membership_details(membership_id)

        return json.dumps({"errors": [], "data": {"status": '1', "children": profile}})
    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['something went wrong'], "data": {"status": '0'}})


# @app.route('/writevoters1', methods=['GET'])
# @crossdomain(origin="*",headers="Content-Type")
# @csrf.exempt
# def writevoters1():
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)


#         emails_file="./voters.json"
#         json_data=open(emails_file).read()
#         data=json.loads(json_data)

#         for row in data:


#             cur.execute( "insert into voters(serial_no,voter_id,voter_name,relation_name,relation_type,house_no,age,sex,section_no,section_name,part_no,page_no,ac_no,state)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(row['serial_no'],row['voter_id'],row['voter_name'],row['relation_name'],row['relation_type'],row['house_no'],row['age'],row['sex'],row['section_no'],row['section_name'],row['part_no'],row['page_no'],row['ac_no'],row['state']))
#             con.commit()

#     except Exception as e:
#         print str(e)
#     return ""

################### end of memership services ############################


################################ membership with SMS ##################


# commented before live app
# @app.route("/forgotmembershipID",methods=["GET","POST"])
# @crossdomain(origin="*",headers="Content-Type")
# def forgotmembershipID():
#     voter_id=request.values.get('voter_id')

#     if voter_id is None:
#         return json.dumps({"errors":['voter_id is missing'],"data":{"status":'0'}})
#     membership_instance=Membership_details()
#     details=membership_instance.get_membership_details(None,str(voter_id).lower())
#     d={}
#     if details is not None:

#         try:
#             phone=encrypt_decrypt(details['phone'])
#             numberToSend=str(phone)
#             msgToSend="Hello "+str(details['name']).rstrip()+", Your Janasena Party membership ID : "+str(details['membership_id'])+"  Thanks !"

#             sendSMS(numberToSend,msgToSend)

#             return json.dumps({"errors":[],"data":{"status":'1'}})
#         except (KeyError,Exception) as e:
#             print str(e)
#             return json.dumps({"errors":['something went wrong . please try after some time'],"data":{"status":'0'}})
#     else:
#         return json.dumps({"errors":['details not Found'],"data":{"status":'0'}})


####################### end of membership with SMS SErvices ###########
@app.route("/snapshot", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def snapshot():
    return render_template("mp_constituency.html")


@app.route("/mp_data", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def mp_data():
    try:
        age_file = "./agerange.json"
        issues_file = "./machilipatnam.json"
        age_data = open(age_file).read()
        issues_data = open(issues_file).read()

        age_data = json.loads(age_data)
        issues_data = json.loads(issues_data)
        return json.dumps({"errors": [], "data": {"status": '1', "age_data": age_data, "issues_data": issues_data}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@app.route("/writevoters", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def writevoters():
    try:

        import mssql

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        for i in [102]:
            start_time = datetime.now()
            print start_time
            database_name = "S29_AC" + str(i).zfill(3)
            table_name = "AC_" + str(i).zfill(3)
            print database_name
            print table_name
            rows = mssql.getting_voters(database_name, table_name)
            fetching_time = datetime.now()
            ascii_voters = []
            for row in rows:
                try:
                    voter_id = str(row[1]).strip()

                    name = strip_non_ascii(row[2])

                    name_str = str(name).strip()

                    relation_name = row[3]
                    ac_no = str(row[12]).rstrip()
                    part_no = str(row[10]).rstrip()
                    try:
                        cur.execute(
                            "insert into voters(serial_no,voter_id,voter_name,relation_name,relation_type,house_no,age,sex,section_no,section_name,part_no,page_no,ac_no,state)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",
                            (row[0], voter_id.lower(), name, relation_name, row[4], row[5], row[6], row[7], row[8],
                             row[9], part_no, row[11], ac_no, '2'))
                        con.commit()
                    except psycopg2.Error as e:
                        print str(e)
                        con.rollback()
                except Exception as e:
                    print str(e)
                    con.rollback()
                    print row[1]
                    ascii_voters.append(row[1])

            end_time = datetime.now()
            fetch_time = fetching_time - start_time
            completed_time = end_time - start_time
            write_time = end_time - fetching_time
            print "fetch_time is " + str(fetch_time.seconds)
            print "write_time is " + str(write_time.seconds)
            print "Total_time is " + str(completed_time.seconds)
            print len(ascii_voters)
        return json.dumps({'missed_voters': ascii_voters})

    except Exception as e:
        print str(e)
        return "exception occured"


###########################################leader dashboard services ###############


@app.route("/getMPConstituencies/<district>", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def getMPConstituencies(district):
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute(
            """select  ac.constituency_name,ac.parliament_constituency_name,ar.age_range,ar.gender,ar.count  from assembly_constituencies ac join age_range ar on (ar.type_id=ac.constituency_id and ar.state_id=ac.state_id) where lower(ac.district_name)=%s and category='V' and type='A'""",
            (str(district).lower(),))
        ac_rows = cur.fetchall()
        ac_rows = format_rows(ac_rows)
        d = {'ac_data': ac_rows}
        return json.dumps({"errors": [], "data": {"status": '1', "children": d}})

    except (psycopg2.Error, Exception)as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


@app.route("/getConstituenciesByDistrict/<district>", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def getConstituenciesByDistrict(district):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from assembly_constituencies where lower(district_name)=%s", (str(district).lower(),))
        rows = cur.fetchall()
        rows = format_rows(rows)
        return json.dumps({"errors": [], "data": {"status": '1', "data": rows}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


@app.route("/getConstituenciesByParliament/<pc>", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def getConstituenciesByParliament(pc):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from assembly_constituencies where lower(parliament_constituency_name)=%s",
                    (str(pc).lower(),))
        rows = cur.fetchall()
        rows = format_rows(rows)
        return json.dumps({"errors": [], "data": {"status": '1', "data": rows}})

    except (psycopg2.Error, Exception)as e:
        print str(e)
        return json.dumps({"errors": ["Exception occured"], "data": {"status": '0'}})


def format_rows(rows):
    try:
        for row in rows:
            for item in row:

                import datetime
                if isinstance(row[item], datetime.date) or isinstance(row[item], datetime.datetime):
                    row[item] = str(row[item].strftime("%d-%b-%Y"))
                else:
                    row[item] = str(row[item]).rstrip()
        return rows
    except Exception as e:
        return False


@app.route("/writecastedetails", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def writecastedetails():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        caste_file = "./machilipatnam.json"
        caste_data = open(caste_file).read()

        caste_data = json.loads(caste_data)

        all_data = {}

        for key in caste_data.keys():

            if key != 'Caste':

                all_data[key] = []
                for index in range(len(caste_data[key])):
                    c = {}
                    c['caste'] = caste_data['Caste'][index]
                    c['count'] = caste_data[key][index]
                    all_data[key].append(c)
                cur.execute(
                    "select ac.constituency_id,ac.state_id,pc.seq_id from assembly_constituencies ac join parliament_constituencies pc on pc.parliament_constituency_name=ac.parliament_constituency_name where lower(ac.constituency_name)=%s",
                    (key.lower(),))
                row = cur.fetchone()
                if row is not None:
                    cur.execute(
                        "insert into constituency_analysis(state_id,ac_no,votes_details,pc_no)values(%s,%s,%s,%s) ",
                        (row[1], row[0], json.dumps(all_data[key]), row[2]))
                    con.commit()
        con.close()
        return json.dumps({"data": all_data})



    except Exception as e:
        print str(e)
        return 'False'


@app.route("/writeanalysisdetails", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def writeanalysisdetails():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        caste_file = "./machilipatnam_suggestions.json"
        analysed_data = open(caste_file).read()

        analysed_data = json.loads(analysed_data)

        for item in analysed_data:
            print item
            assembly = item['Assembly']
            data = {}
            for key in item.keys():

                if key != 'Assembly':
                    data[key] = item[key]

            cur.execute(
                "select ac.constituency_id,ac.state_id,pc.seq_id from assembly_constituencies ac join parliament_constituencies pc on pc.parliament_constituency_name=ac.parliament_constituency_name where lower(ac.constituency_name)=%s",
                (assembly.lower(),))
            row = cur.fetchone()
            if row is not None:
                cur.execute(
                    "update  constituency_analysis set constituency_analysis=%s where state_id=%s and ac_no=%s ",
                    (json.dumps(data), row[1], row[0]))
                con.commit()
        con.close()
        return json.dumps({"data": data})



    except Exception as e:
        print str(e)
        return 'False'


@app.route("/getAnalysisDetails/<district>", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def getAnalysisDetails(district):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        if district is not None and district != "":
            district = 'krishna'
            cur.execute(
                "select ac.constituency_name,ac.district_name,ac.parliament_constituency_name,ac.state_id,vc.votes_details,vc.constituency_analysis from assembly_constituencies ac join  constituency_analysis vc on (vc.state_id=ac.state_id and vc.ac_no=ac.constituency_id) where lower(ac.district_name)=%s ",
                (district,))
            rows = cur.fetchall()

            return json.dumps({"errors": [], "data": {"status": '1', "children": rows}})
        return json.dumps({"errors": ['district name is missing'], "data": {"status": '0'}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['Exception occured'], "data": {"status": '0'}})

    ##################### member login services #################


##################### member login services #################


@app.route('/memberlogin', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def memberlogin():
    if request.method == 'POST':
        password = request.form.get('member_pin')
        uname = request.form.get('membership_id')
        utype = 'M'
        print password, uname
        if uname is None or password is None:
            flash('Invalid Details', 'error')
            return redirect(url_for('.memberlogin'))
        password = encrypt_decrypt(password, 'E')

        if checkUserAuth(uname, password, utype):
            session['next'] = url_for('memberDashboard')
            session['current'] = url_for('memberlogin')
            session['member_logged_in'] = True

            return redirect(url_for('.memberDashboard'))
        else:
            flash('Incorrect Details Given. Please check', 'error')
            return redirect(url_for('.memberlogin'))
    else:

        if session.get('member_logged_in') is not None:
            if session.get('member_logged_in'):
                return redirect(url_for('.memberDashboard'))

        return render_template("memberdashboard/memberlogin.html")


@app.route("/memberLoginOTP", methods=['GET', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def memberLoginOTP():
    if request.method == 'GET':
        try:

            MembershipID = request.values.get('MembershipID')

            if MembershipID is None or MembershipID.isspace():
                return makeResponse("MembershipID should not be empty'", 400, 'application/json')
            MembershipID = str(MembershipID).upper()
            registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
            if registered_user is not None:
                phone = registered_user.phone
                mobile_no = encrypt_decrypt(phone, 'D')

                otp_message = "OTP Sent Successfully"
                countries_allowed = ["91"]
                if mobile_no.startswith(tuple(countries_allowed)):

                    otp = genOtp(mobile_no, 'ML')
                    otp_message = "OTP has been sent to Registered Mobile Successfully"
                else:
                    e_email = registered_user.email

                    if e_email is not None or e_email != '':
                        email = encrypt_decrypt(e_email, 'D')

                        otp = genEmailOtp(mobile_no, email, 'ML')
                        otp_message = "OTP has been sent to Registered Email Successfully"

                    else:
                        otp = genOtp(mobile_no, 'ML')
                        otp_message = "OTP has been sent to Registered Mobile Successfully"

                return makeResponse(json.dumps({"status": "1", "message": otp_message}), 200,
                                    'application/json')
            else:

                return makeResponse("It seems You have not Provided Details. please provide and Login", 400,
                                    'application/json')
        except Exception as e:
            print str(e)
            return makeResponse("something wrong With This MembershipId  ", 400, 'application/json')


@app.route('/memberLogin_withOTP', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
def memberLogin_withOTP():
    try:

        otp = request.values.get("otp")
        membership_id = request.values.get("membership_id")

        if membership_id is None or membership_id.isspace():
            return json.dumps({"errors": ["invalid membership_id"], "data": {"status": "0"}})
        if otp is None or otp.isspace():
            return json.dumps({"errors": ["invalid otp"], "data": {"status": "0"}})

        MembershipID = str(membership_id).upper()
        registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
        if registered_user is not None:
            phone = registered_user.phone
            mobile_no = encrypt_decrypt(phone, 'D')
            if verifyOtp(mobile_no, otp, "ML"):
                login_user(registered_user)

                session['t'] = 'M'
                session['next'] = url_for('memberDashboard')
                session['current'] = url_for('memberlogin')
                if registered_user.member_pin is None or registered_user.member_pin == '':
                    session['pin'] = 'N'
                else:
                    session['pin'] = 'Y'
                session['member_logged_in'] = True
            return json.dumps({"errors": [], "data": {"status": "1"}})

        return json.dumps(
            {"errors": ["It seems You have not Provided Details. please provide and Login"], "data": {"status": "0"}})


    except Exception as e:
        print str(e)

        return json.dumps({"errors": ["Invalid Details given"], "data": {"status": "0"}})


@app.route("/changeMemberProfileImage", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@member_login_required
def changeMemberProfileImage():
    try:
        membership_id = request.values.get('membership_id')
        if membership_id is None or membership_id == '':
            return json.dumps({"errors": ['membership_id missing'], "data": {"status": "0"}})
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:
            old_image_file = current_user.image_url

            image_file = request.files['image']

            img_url = upload_image_web(image_file, 'membershipimages')
            # img_url=""
            cur.execute("update janasena_membership set image_url=%s where membership_id=%s", (img_url, membership_id))
            con.commit()
            con.close()
            if old_image_file is not None and old_image_file != '':

                if config.ENVIRONMENT_VARIABLE != 'local':
                    old_img_container = str(old_image_file).split(".")[0]
                    if old_img_container.lower() != "https://janasenalive":
                        blob_name = str(old_image_file).split('/')[-1]
                        print blob_name
                        print "change image in portal"
                        delete_blob.delay('membershipimages', blob_name)
            return json.dumps({"errors": [], "data": {"status": "1", "image_url": img_url}})

        except (KeyError, Exception) as e:

            print str(e)
            con.close()
            return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})
    except psycopg2.Exception as e:
        print str(e)
        con.close()
        return json.dumps({"errors": ['Your Session Expired'], "data": {"status": "0"}})


@app.route('/memberDashboard', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@member_login_required
def memberDashboard():
    
    return render_template("memberdashboard/memberDashboard.html")


@app.route('/creatememberpin', methods=['POST', 'OPTIONS', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@member_login_required
@csrf.exempt
def creatememberpin():
    try:

        pin = request.values.get('pin')

        if pin is None or pin == '' or len(str(pin)) != 6:
            return json.dumps({"errors": ["Invalid Details given"], "data": {"status": "0"}})
        MembershipID = current_user.membership_id

        registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()

        if registered_user is not None:
            user_pin = encrypt_decrypt(str(pin), "E")

            registered_user.member_pin = user_pin
            db.session.commit()

            return json.dumps({"errors": [], "data": {"status": "1"}})
        return json.dumps({"errors": ["Invalid Details given"], "data": {"status": "0"}})



    except Exception as e:
        print str(e)
        return json.dumps({"errors": ["Invalid Details given"], "data": {"status": "0"}})


@app.route('/memberlogout')
@member_login_required
def memberlogout():
    logout_user()
    session.clear()

    return redirect(url_for('memberlogin'))


@app.route('/memberProfile', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@member_login_required
def memberProfile():
    profile = object_as_dict(current_user)

    if profile['email'] is not None and profile['email'] != '':
        profile['email'] = encrypt_decrypt(profile['email'], 'D')
    if profile['phone'] is not None and profile['phone'] != '':
        profile['phone'] = encrypt_decrypt(profile['phone'], 'D')
    if profile['voter_id'] is not None and profile['voter_id'] != '':
        profile['voter_id'] = encrypt_decrypt(profile['voter_id'], 'D')
    if profile['image_url'] is None or profile['image_url'] == '':
        profile['image_url'] = './static/img/default-avatar.png'
    else:
        # once we recover janasenalive we can delete this check
        img_url = str(profile['image_url']).split(".")[0]
        if img_url.lower() == "https://janasenalive":
            profile['image_url'] = './static/img/default-avatar.png'
    profile_keys = ['name', 'relation_type','email', 'phone', 'voter_id', 'image_url', 'sex', 'membership_id', 'age', 'is_volunteer',
                    'polling_station_name']

    data = {}

    for key in profile_keys:
        data[key] = profile[key]
    if profile['member_pin'] is None or profile['member_pin'] == '':
        data['pinstatus'] = 'N'
    else:
        data['pinstatus'] = 'Y'

    return json.dumps({"errors": [], "data": {"status": "1", "children": data}})


@app.route('/memberReferrals', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@member_login_required
def memberReferrals():
    membership_instance = Membership_details()
    data = membership_instance.get_memberReferrals()

    return json.dumps({"errors": [], "data": {"status": "1", "children": data}})




########################################################
###############################################
@app.route('/write_ec_data', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def write_ec_data():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        caste_file = "./2004.json"
        ecrdata = open(caste_file).read()

        ecrdata = json.loads(ecrdata)
        for item in ecrdata:
            print item
            try:
                cur.execute(
                    "select constituency_id,constituency_name from assembly_constituencies where state_id=%s and lower(constituency_name)=%s",
                    (1, str(item['Assembly Constituency Name']).lower()))
                row = cur.fetchone()
                print row
                if row is not None:
                    ac_no = row[0]
                    cur.execute(
                        "insert into election_trends(state_id,ac_no,party_name,candidate_name,community,votes,year)values(%s,%s,%s,%s,%s,%s,%s)",
                        (1, ac_no, item['Winning Party'], item['Winner Candidates Name'], '', item['winner Votes'],
                         2004))
                    con.commit()
                    cur.execute(
                        "insert into election_trends(state_id,ac_no,party_name,candidate_name,community,votes,year)values(%s,%s,%s,%s,%s,%s,%s)",
                        (1, ac_no, item['Runner Party'], item['Runner UP'], '', item['runner Votes'], 2004))
                    con.commit()
            except Exception as e:
                print str(e)
                print item

        return 'true'
    except Exception as e:
        print str(e)
        return 'false'





@app.route('/getGeocodes', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getGeocodes():
    con = psycopg2.connect(config.DB_CONNECTION)
    cur = con.cursor(cursor_factory=RealDictCursor)
    import os
    import re
    path = 'polling stations/Visakhapatnam'
    missed = []
    for filename in os.listdir(path):
        print filename
        if filename[0] != '.':
            constituency_name = filename.split('.')[0]
            print constituency_name
            cur.execute(
                "select constituency_id from assembly_constituencies where lower(constituency_name)=%s and state_id=%s",
                (constituency_name.lower(), 1))
            constituency_details = cur.fetchone()
            print constituency_details
            if constituency_details is not None:
                ac_id = constituency_details['constituency_id']
                geo_file = path + "/" + filename

                with open(geo_file, 'rb') as f:
                    contents = f.read()

                geo_data = json.loads(contents)
                print type(geo_data)

                data = []
                errors = 0
                success = 0
                if 'd' in geo_data.keys():
                    for station in geo_data['d']['Points']:

                        try:
                            d = {}
                            d['latituede'] = station['Latitude']
                            d['Longitude'] = station['Longitude']

                            station_str = station['InfoHTML']
                            station_str1 = station_str.split("Polling Station No and Name :")[1]

                            sub_str = station_str1.split("<br/>")[0]

                            station_id = re.findall(r'\d+', sub_str)

                            station_name = ''.join([i for i in sub_str if not i.isdigit()])
                            d['polling_station_id'] = int(station_id[0])
                            d['polling_station_name'] = station_name.strip()
                            cur.execute(
                                "insert into geocoordinates_ps(state_id,constituency_id,lat,longi,ps_id,ps_name)values(%s,%s,%s,%s,%s,%s)",
                                (1, ac_id, d['latituede'], d['Longitude'], d['polling_station_id'],
                                 d['polling_station_name']))
                            con.commit()
                            data.append(d)
                            success += 1
                        except (KeyError, Exception) as e:
                            print str(e)
                            errors += 1
            missed.append(filename)
    con.close()
    print errors, success
    print filename
    return json.dumps({"errors": [], "data": {"status": '1', 'children': data}})


from geopy.distance import vincenty

#
@app.route('/mapmandals', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def mapmandals():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        path = 'jsonfiles/mandals'
        missed = []
        for filename in os.listdir(path):
            # geo_file="./krishna.json"
            geo_file = path + "/" + filename
            geo_data = open(geo_file).read()
            geo_data = json.loads(geo_data)
            geo_data = geo_data['data']
            for item in geo_data:
                print item

                mandal_name = item['Mandals']
                co_ordinates = " ".join(item['Values'].split())
                co_ordinates1 = co_ordinates.split(',')
                print co_ordinates1
                lat = ''
                longi = ''
                if len(co_ordinates1) >= 2:
                    lat = str(co_ordinates1[0])
                    longi = str(co_ordinates1[1])
                    print lat, longi

                state_id = 1
                assembly_name = item['Assembly Constituency']
                cur.execute(
                    "select constituency_id from assembly_constituencies where lower(constituency_name)=%s and state_id=%s",
                    (assembly_name.lower(), 1))
                acrow = cur.fetchone()
                if acrow is not None:
                    ac_id = acrow[0]
                    cur.execute(
                        "select * from geocoordinates_mdl where lower(mdl_name)=%s and state_id=%s and constituency_id=%s",
                        (mandal_name.lower(), 1, ac_id))
                    mdrow = cur.fetchone()
                    if mdrow is None:
                        cur.execute(
                            "insert into geocoordinates_mdl(state_id,constituency_id,lat,longi,mdl_name)values(%s,%s,%s,%s,%s)",
                            (state_id, ac_id, lat, longi, mandal_name))
                        con.commit()

                        print mandal_name
                    else:
                        print '/////// existing'
                        print mandal_name
                else:
                    d = {}
                    d['file'] = filename
                    d['assembly_name'] = assembly_name
                    missed.append(d)

                    print 'assembly name'
                    print assembly_name.lower()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1", "missed": missed}})
    except psycopg2.Error as e:
        print str(e)
        con.close()
        return 'False'


@app.route('/getDistance', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getDistance():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select constituency_id from assembly_constituencies where district_name='Guntur'")
        assemblies = cur.fetchall()
        for row in assemblies:
            constituency_id = row['constituency_id']
            cur.execute("select * from geocoordinates_mdl where state_id=1 and constituency_id=%s ", (constituency_id,))
            mdl_rows = cur.fetchall()
            cur.execute("select * from geocoordinates_ps where state_id=1 and constituency_id=%s ", (constituency_id,))
            ps_rows = cur.fetchall()
            data = []
            for item in ps_rows:
                d = {}
                lat1 = item['lat']
                lon1 = item['longi']
                d['ps_id'] = item['ps_id']
                d['ps_name'] = item['ps_name']
                source = tuple([lon1, lat1])
                dist = []
                mdls = []
                x = []
                for row in mdl_rows:
                    m = {}
                    lat2 = row['lat']
                    lon2 = row['longi']
                    destination = tuple([lon2, lat2])
                    distance = ((vincenty(source, destination)).miles)
                    dist.append(distance)
                    m['mdl_id'] = row['seq_id']
                    m['mdl'] = row['mdl_name']
                    m['distance'] = distance
                    x.append(m)
                minimum = min(dist)
                for obj in x:
                    if obj['distance'] == minimum:
                        d['mdl_name'] = obj['mdl']
                        d['mdl_id'] = obj['mdl_id']
                        d['distance'] = obj['distance']
                        break;
                data.append(d)
            for item in data:
                cur.execute(
                    "update  geocoordinates_ps set mandal_id=%s,distance=%s where state_id=%s and constituency_id=%s and ps_id=%s",
                    (item['mdl_id'], item['distance'], 1, constituency_id, item['ps_id']))
                con.commit()
        con.close()

        return json.dumps({"errors": [], "data": {"status": "1", "children": data}})
    except Exception as e:
        print str(e)
        return 'false'


# @app.route('/getCounts', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# def getCounts():
#     try:
#         import collections
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select * from geocoordinates_ps ")
#         ps_rows = cur.fetchall()
#         cur.execute("select seq_id,mdl_name from geocoordinates_mdl ")
#         mdl_rows = cur.fetchall()
#         geo_file = "./avinigadda.json"
#         geo_data = open(geo_file).read()
#         geo_data = json.loads(geo_data)
#         ps_ids = {str(key) for key in geo_data.keys()}
#         for item in ps_rows:
#             if str(item['ps_id']) in ps_ids:
#                 geo_data[str(item['ps_id'])]['mandal_id'] = item['mandal_id']
#
#         print 'cp1'
#         d = {}
#         for key in geo_data.keys():
#
#             try:
#                 mandal_id = geo_data[key]['mandal_id']
#             except KeyError as e:
#                 print geo_data[key]
#                 continue
#                 print str(e)
#
#             if mandal_id in d.keys():
#                 d[mandal_id]['TDP'] += geo_data[key]['TDP']
#                 d[mandal_id]['INC'] += geo_data[key]['INC']
#                 d[mandal_id]['PRAP'] += geo_data[key]['PRAP']
#             else:
#                 row = {}
#                 row['TDP'] = geo_data[key]['TDP']
#                 row['INC'] = geo_data[key]['INC']
#                 row['PRAP'] = geo_data[key]['PRAP']
#                 d[mandal_id] = row
#
#         return json.dumps({'errors': [], "data": {"status": '1', "children": d}})
#
#     except Exception as e:
#         print str(e)
#         return 'false'





@app.route('/getGeocodesMandals', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getGeocodesMandals():
    geo_file = "./mandals.json"
    geo_data = open(geo_file).read()
    geo_data = json.loads(geo_data)
    data = []
    import requests
    for item in geo_data:
        url = "https://maps.googleapis.com/maps/api/geocode/json?&address=" + str(item['Mandal Name'])

        response = requests.request("GET", url, params='')
        d = {}
        d[item['Mandal Name']] = response.text
        data.append(d)
        print  (response.text)
    return json.dumps({"errors": [], "data": {"status": '1', 'children': data}})


from excel_to_json import *


@app.route('/writeexceldata', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeexceldata():
    try:
        excel_path = 'excel_files/facilitator/araku'
        # json_path='excel_files/verify/json'
        extension = 'json'
        for filename in os.listdir(excel_path):
            input_file = excel_path + "/" + filename
            file_name = filename.split('.')[0]
            json_file = excel_path + '/' + file_name
            output = create_file(json_file, extension)
            print json_file
            convert_excel_to_json(input_file, output)

        return 'true'
    except Exception as e:
        print str(e)


################################donations#####################################


## instamojo response


## razorpay response


@app.route('/updatelateral_resourcepersons', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def updatelateral_resourcepersons():
    try:
        tables = ['resourcepersons', 'ghmcresourcepersons', 'uttarandhraresourcepersons']
        for table in tables:
            tasks = table_service.query_entities(table, filter="lateral eq 'T'")
            print len(list(tasks))
            for task_item in tasks:
                batch = TableBatch()
                mobile = task_item['mobile_num']
                position = str(task_item['applying_for']).lower()
                print position
                cposition = ''
                if str(position[0]).lower() == 's':
                    cposition = 'speaker'
                elif str(position[0]).lower() == 'a':
                    cposition = 'analyst'
                else:
                    cposition = 'content writer'
                print cposition
                task = {'applying_for': cposition, 'mobile_num': mobile, 'PartitionKey': 'pk', 'attendance_status': 'T',
                        'RowKey': task_item['RowKey']}
                if_match = ""
                batch.merge_entity(task, if_match)
                table_service.commit_batch(table, batch)

        return 'true'

    except Exception as e:
        print str(e)
        return ''


@app.route('/writelateral_resourcepersons', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def writelateral_resourcepersons():
    try:
        rp_file = "./excel_files/verify/excel/hyd.json"
        json_data = open(rp_file).read()
        data = json.loads(json_data)
        district = 'Hyderabad'
        update_count = 0
        for item in data['data']:

            try:
                phoneno = str(item['Mobile']).split(".")[0]
            except Exception as e:
                continue
            assembly = item['Assembly']
            district = item['District']
            print phoneno
            # tasks = table_service.query_entities('ghmcresourcepersons', filter="mobile_num eq '")
            tasks = table_service.query_entities('ghmcresourcepersons', filter="mobile_num eq '" + str(phoneno) + "'")

            for task_item in tasks:
                update_count += 1
                batch = TableBatch()
                task = {"mobile_num": str(phoneno), 'district': str(district), 'PartitionKey': 'pk',
                        'attendance_status': 'T', 'RowKey': task_item['RowKey'], 'assembly_constituency': assembly,
                        'district': district}
                if_match = ""
                batch.merge_entity(task, if_match)
                table_service.commit_batch('ghmcresourcepersons', batch)
                break
            else:
                print 'else clause'
                data = {}
                try:
                    data['Name'] = str(item['Name']) + " " + str(item['Surname'])
                    # +" "+str(item['Surname'])
                    data['applying_for'] = item['applied_for']

                    if 'Email' in item.keys():
                        data['email'] = str(item['Email']).lower()
                    else:
                        data['email'] = ''

                    data['mobile_num'] = phoneno
                    # data['district']=str(district)
                    data['assembly_constituency'] = assembly
                    data['PartitionKey'] = 'pk'
                    data['RowKey'] = str(uuid.uuid4())
                    data['lateral'] = 'T'
                    data['attendance_status'] = 'T'
                    data['age'] = data['Age']
                    data['district'] = district
                    task = table_service.insert_entity('ghmcresourcepersons', data)
                except (KeyError, Exception) as e:
                    print str(e)

        print update_count
        return 'True'


    except Exception as e:
        print str(e)
        return 'False'


# @app.route('/writelateral_resourcepersons',methods=['GET','POST'])
# @crossdomain(origin="*",headers="Content-Type")

# def writelateral_resourcepersons():
#     try:
#         rp_file="./jsonfiles/resourcepersons/Visakhapatnam.json"
#         json_data=open(rp_file).read()
#         data=json.loads(json_data)
#         district='Visakhapatnam'
#         for item in data['data']:

#             batch = TableBatch()
#             try:
#                 phoneno=str(item['MOBILE']).split(".")[0]
#             except Exception as e:
#                 continue

#             print phoneno
#             tasks = table_service.query_entities('uttarandhraresourcepersons', filter="mobile_num eq '"+str(phoneno)+"' and district eq '"+str(district)+"'")

#             print list(tasks)
#             for task_item in tasks:
#                 task={"district":str(district),"mobile_num":str(phoneno),'PartitionKey':'pk','attendance_status':'T','RowKey':task_item['RowKey']}
#                 if_match=""
#                 batch.merge_entity(task,if_match)
#                 table_service.commit_batch('uttarandhraresourcepersons', batch)
#                 break
#             else:
#                 print 'else clause'
#                 data={}
#                 try:
#                     data['Name']=str(item['NAME'])+" "+str(item['SURNAME'])
#                     #
#                     data['applying_for']=item['applied_for']
#                     data['email']=str(item['E-MAIL']).lower()

#                     data['mobile_num']=phoneno

#                     data['district']=district
#                     data['assembly_constituency']=item["ASSEMBLY CONSTITUENCY"]
#                     #CONSTITUENCY


#                     data['PartitionKey']='pk'
#                     data['RowKey']=str(uuid.uuid4())
#                     data['lateral']='T'
#                     data['attendance_status']='T'

#                     task=table_service.insert_entity('uttarandhraresourcepersons',data)
#                 except (KeyError,Exception) as e:
#                     print str(e)


#         return 'True'


#     except Exception as e:
#         print str(e)
#         return 'False'
@app.route('/getResourcepersonsCOunts', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getResourcepersonsCOunts():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select district_name from districts")
        districts = cur.fetchall()
        data = []
        for district in districts:
            # district_data={}
            district = (str(district[0]).lower()).rstrip()
            # print district
            # district_data={"district":district,"total_applications":0,"total_attended":0,"assemblies":[]}

            # cur.execute("select count(*) from resourcepersons where lower(district)=%s",(district,))
            # district_applications_count=cur.fetchone()
            # print district_applications_count
            # district_data['total_applications']=district_applications_count[0]
            # cur.execute("select count(*) from resourcepersons where lower(district)=%s and attendance_status=%s",(district,'T'))
            # district_attended_count=cur.fetchone()
            # district_data['total_attended']=district_attended_count[0]
            cur.execute("select constituency_name from assembly_constituencies where lower(district_name)=%s",
                        (district,))
            assemblies = cur.fetchall()
            total_assemblies_count = 0
            for assembly in assemblies:
                assembly = (str(assembly[0]).lower()).rstrip()
                cur.execute("select count(*) from resourcepersons where lower(district)=%s and lower(assembly)=%s",
                            (district, assembly))
                assembly_applications_count = cur.fetchone()
                cur.execute(
                    "select count(*) from resourcepersons where lower(district)=%s and lower(assembly)=%s and attendance_status=%s",
                    (district, assembly, 'T'))
                assembly_attended_count = cur.fetchone()
                # total_assemblies_count=total_assemblies_count+assembly_attended_count[0]
                d = {}
                d['district'] = district
                d['assembly'] = assembly
                d['total_application'] = assembly_applications_count[0]
                d['attended'] = assembly_attended_count[0]
                # district_data['assemblies'].append(d)
                # unknown=district_data['total_attended']-total_assemblies_count
                # district_data['assembly_unknown']=unknown
                data.append(d)
        return json.dumps(data)

    except Exception as e:
        return str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/writeresourcepersons2', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeresourcepersons2():
    try:

        start = request.values.get('start')
        end = request.values.get('end')
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        print start, end
        json_path = './excel_files/one/resourcepersonsfinal.json'

        json_data = open(json_path).read()
        print type(json_data)
        print 'cp1'
        # print json_data
        data = json.loads(json_data)
        print 'cp2'
        data = data['data']

        for entry in data[int(start):int(end)]:
            # [int(start):int(end)]

            try:
                district = entry['district']
                assembly = entry['assembly']
                mobile = str(entry['mobile']).split(".")[0]
                try:
                    if len(entry['name']) > 100:
                        entry['name'] = 'NA'
                except Exception as e:
                    entry['name'] = 'NA'
                state = str(entry['state']).split(".")[0]
                if len(entry['age']) > 10:
                    age = ''
                else:
                    age = entry['age']
                print entry['seq_id']
                cur.execute(
                    "insert into  resourcepersons(district,assembly,name,email,applying_for,gender,age,attendance_status,lateral_app,mobile,state)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)  ",
                    (
                        district, assembly, entry['name'], entry['email'], entry['applying_for'], entry['gender'], age,
                        'T',
                        'T', mobile, state))


            except (psycopg2.Error, Exception) as e:
                print str(e)
                # continue
                return json.dumps(entry)
        con.commit()
        con.close()

        return json.dumps({"errors": [], 'data': {'status': '1'}})


    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/writeresourcepersons1', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writeresourcepersons1():
    try:

        id_no = request.values.get('id')
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        offset = 5 * int(id_no)
        cur.execute(
            "select distinct(district_name) from assembly_constituencies order by district_name limit %s offset %s  ",
            (5, offset))
        rows = cur.fetchall()
        districts = []
        for row in rows:
            if row[0].rstrip() == "Vishakhapatnam":
                districts.append('Visakhapatnam')
            else:
                districts.append(row[0].rstrip())
        print districts

        for dist in districts:
            if dist == 'Hyderabad':
                table = 'ghmcresourcepersons'

            elif dist in ['Vizianagaram', 'Srikakulam', 'Visakhapatnam']:
                table = 'uttarandhraresourcepersons'
            else:
                table = 'resourcepersons'

            marker = {}
            marker['nextpartitionkey'] = None
            marker['nextrowkey'] = None
            if dist != 'Hyderabad':
                select = 'Name,email,mobile_num,assembly_constituency,attendance_status,lateral,age,gender,applying_for'
                filter_query = "district eq '" + dist + "'"
            elif dist in ['Vizianagaram', 'Srikakulam', 'Visakhapatnam']:
                select = 'Name,email,mobile_num,assembly,attendance_status,lateral,age,gender,applying_for'
                filter_query = "district eq '" + dist + "'"
            else:
                select = 'Name,email,mobile_num,division,attendance_status,lateral,age,gender,applying_for'
                filter_query = ""
            position_status = True
            marker = {}
            marker['nextpartitionkey'] = None
            marker['nextrowkey'] = None
            print  dist
            while position_status:
                pos_entities = table_service.query_entities(table, num_results=1000, filter=filter_query, select=select,
                                                            marker=marker,
                                                            accept=TablePayloadFormat.JSON_MINIMAL_METADATA,
                                                            property_resolver=None, timeout=None)

                for entry in pos_entities:
                    print entry

                    if dist == "Hyderabad":
                        assembly = str(entry['division'])
                    elif dist in ['Vizianagaram', 'Srikakulam', 'Visakhapatnam']:
                        assembly = str(entry['assembly'])
                    else:
                        assembly = str(entry["assembly_constituency"])
                    try:
                        cur.execute(
                            "insert into  resourcepersons(district,assembly,name,email,applying_for,gender,age,attendance_status,lateral_app,mobile)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)  ",
                            (dist, assembly, str(entry['Name']), str(entry['email']), str(entry['applying_for']),
                             str(entry['gender']), str(entry['age']), str(entry['attendance_status']),
                             str(entry['lateral']), str(entry['mobile_num'])))
                        con.commit()
                    except (psycopg2.Error, Exception) as e:
                        print str(e)
                        break

                if hasattr(pos_entities, 'next_marker'):

                    marker = getattr(pos_entities, 'next_marker')
                    if not marker:
                        position_status = False
                    else:
                        position_status = True


                else:
                    position_status = False

        return json.dumps({"errors": [], 'data': {'status': '1'}})


    except Exception as e:
        return str(e)
        return json.dumps({"errors": ['exception occured'], 'data': {'status': '0'}})


@app.route('/getresourcepersons1', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def getresourcepersons1():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select district,total,speaker,analyst,content_writer,total_attended from resourcepersons_counts")
        rows = cur.fetchall()
        return json.dumps({"rows": rows})
    except Exception as e:
        print str(e)
        return "false"


@app.route('/collectresourcepersonsdata', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def collectresourcepersonsdata():
    if request.method == 'GET':
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)

            phone = request.values.get('phone')

            if phone is None:
                return render_template("thanks_message.html", message=" Your details are not available with us. ")

            cur.execute("select * from resourcepersons where mobile=%s and attendance_status=%s ", (phone, 'T'))
            rows = cur.fetchone()

            con.close()
            if rows:
                print rows
                if rows['details_status'] is None:
                    return render_template("collectresourcepersonsdata.html")
                else:
                    return render_template("thanks_message.html", message="Your details are already submitted.")
            else:
                return render_template("thanks_message.html", message="Your details are not available with us.")
        except (psycopg2.Error, Exception) as e:
            print str(e)
            abort(401)
    else:

        phone = request.values.get('phone')

        if phone is None or phone == '':
            return render_template("thanks_message.html", message="Your details are not available with us.")

        voter_id = request.form.get('voter_id')
        aadhar = request.form.get('aadhar_id')
        confirmation = request.form.get('confirmation')
        
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("select * from resourcepersons where mobile=%s and attendance_status=%s", (phone, 'T'))
            rows = cur.fetchone()
            if len(rows):
                cur.execute(
                    "update resourcepersons set voter_id=%s,aadhar_id=%s,details_status=%s,confirmation=%s where mobile=%s",
                    (voter_id, aadhar, 'T', confirmation, phone))
                con.commit()
                con.close()
                return render_template("thanks_message.html", message="Thanks For Submitting Your Details")

            con.close()
            return render_template("thanks_message.html", message="Your details are not available with us.")

        except (psycopg2.Error, Exception) as e:
            print str(e)
            abort(401)

@app.route('/get_volunteers_mandal', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def get_volunteers_mandal():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select district_id,constituency_id,mandal_id,mandal_name from mandals where state_id='1' order by district_id,constituency_id ")
        rows=cur.fetchall()
        for row in rows:
            mandal_id=row['mandal_id']
            #cur.execute("select m.name,m.membershipid,")

    except Exception as e:
        print str(e)


@app.route('/getfacilitators', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getfacilitators():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select r.district,r.assembly,r.name,r.mobile,r.email,a.parliament_constituency_name,r.gender,r.voter_id,r.aadhar_id,r.confirmation,r.miss_call_status,r.state from resourcepersons r left join assembly_constituencies a on r.assembly=lower(a.constituency_name) where attendance_status=%s and (details_status=%s or miss_call_status=%s) ",
            ('T', 'T', 'T'))
        rows = cur.fetchall()
        if len(rows):
            filepath, file_size = create_facilitatorcsv(rows)

            # return_file = open(server_path+file_basename, 'r')
            return_file = open(filepath, 'r')
            file_basename = 'facilitator.csv'

            response = make_response(return_file.read(), 200)

            response.headers['Content-Description'] = 'File Transfer'
            response.headers['Cache-Control'] = 'no-cache'
            response.headers['Content-Type'] = 'text/csv'
            response.headers['Content-Disposition'] = 'attachment; filename=%s' % file_basename
            response.headers['Content-Length'] = file_size
            return response

        return 'no data found'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


# @app.route('/getMembershipFiles', methods=['GET', 'POST'])
# @crossdomain(origin="*", headers="Content-Type")
# #@admin_login_required
# def getMembershipFiles():
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select distinct on (district_id)district_name,district_id from assembly_constituencies where state_id<=2 and district_id>=10 order by district_id")
#         rows=cur.fetchall()
#         print rows
#         for row in rows:
#             district=row['district_name']
#             dist_id=row['district_id']
#             d_type='district'
#             value=district
#             print district
#             membership_instance = Membership_details()

#             data = membership_instance.get_members_basic_details(data_type=d_type, value=value)

#             try:
#                 status=write_csv_to_file(data,district)
#                 print status
#             except Exception as e:
#                 print str(e)
#                 continue
        
#         return "success"


#     except Exception as e:
#         print str(e)
#         return str(e)



@app.route('/writecsvfiles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
def writecsvfiles(): 
    try:
        district_name='West Godavari'
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select constituency_id,constituency_name from assembly_constituencies where district_name=%s ",(district_name,))
        rows=cur.fetchall()
        for row in rows:
            constituency_name=row['constituency_name']
            constituency_id=row['constituency_id']
            print constituency_id
            
            booth_ranges=['1-51','51-101','101-151','151-350']
            for booths in booth_ranges:
                start=int(booths.split('-')[0])
                end=int(booths.split('-')[1])
                if start==1:
                    folder_number='1'
                elif start==51:
                    folder_number='2'
                elif start==101:
                    folder_number='3' 
                else:
                    folder_number='4'   

                data=[]
                for booth in range(start,end):
                    count=1
                    booth_id=booth
                    
                    cur.execute("select distinct on (phone) phone,name from janasena_membership where state_id=1 and constituency_id=%s and polling_station_id=%s ",(constituency_id,booth_id))
                    brows=cur.fetchall()
                   
                    for brow in brows:

                        d={}
                        d['First Name']=str(constituency_name[0:2])+str(booth_id).zfill(3)+"_"+str(count).zfill(3)
                        d['Mobile Phone']="+"+encrypt_decrypt(str(brow['phone']),'D')
                        data.append(d)
                        count=count+1
                if len(data):
                    write_csv_to_file(data,constituency_name,district_name,folder_number)
            print  str(constituency_name)+" completed"
        return 'success'

    except Exception as e:
        print str(e)
        return 'False'



@app.route('/write_booth_csvfiles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
def write_booth_csvfiles(): 
    try:
        district_name='Visakhapatnam'
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select constituency_id,constituency_name from assembly_constituencies where district_name=%s ",(district_name,))
        rows=cur.fetchall()
        for row in rows:
            constituency_name=row['constituency_name']
            constituency_id=row['constituency_id']
            # geo 
            cur.execute("select state_id,constituency_id,ps_id,ps_name,concat(lat,':',longi) from geocoordinates_ps where state_id=1 and constituency_id=%s order by ps_id",(constituency_id,))
            geo_data=cur.fetchall()
            if len(geo_data):
                write_csv_to_file(geo_data,constituency_name,district_name,'geo')
            
            # 2009
            cur.execute("select state_id,constituency_id,polling_station_id,polling_station_name,polling_station_address,'' as latlong from polling_booths_2009_latest where state_id=1 and constituency_id=%s order by polling_station_id",(constituency_id,))
            p2009_data=cur.fetchall()
            if len(p2009_data):
                write_csv_to_file(p2009_data,constituency_name,district_name,'2009')
            
           
            # 2018
            cur.execute("select state_id,constituency_id,part_no,polling_station_name,polling_station_address,'' as latlong from polling_booths_2018_jan where state_id=1 and constituency_id=%s order by part_no",(constituency_id,))
            p2018_data=cur.fetchall()
            if len(p2018_data):
                write_csv_to_file(p2018_data,constituency_name,district_name,'2018')
            
           
            
            print  str(constituency_name)+" completed"
        return 'success'

    except Exception as e:
        print str(e)
        return 'False'

@app.route('/write_campaign_data_csvfiles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
def write_campaign_data_csvfiles(): 
    try:
        #ids=[12,45,46,29,2,5,168,38,20,89,135,41,150,22,102,23,25,59,32,30,35,145,27,166,171,39,81,139,52,47,26,31,43,153,33,99,74,49,51,159,61,57,65,91,93,94,95,92,68,75,112,167,76,90,140,128,79,97,98,100,104,58,37,62,165]
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        
        cur.execute("select distinct(district_name) from assembly_constituencies where state_id=1 ")
        districts=cur.fetchall()
        for district in districts:
            district_name=district['district_name']
            

            cur.execute("select distinct(phone) from campaign_numbers where ac_no in (select constituency_id from assembly_constituencies where district_name=%s) ",(district_name,))
            rows=cur.fetchall()
            print district_name,len(rows)
            file_name=district_name+"_numbers.csv"
            write_csv_to_file(rows,"",district,file_name)
        return 'success'
    except Exception as e:
        print str(e)


def write_csv_to_file(data,constituency,district,file_name):
    try:
        path="C:/Users/Srini1/Desktop/top65/"
        
        # if not os.path.exists(path):
        #   os.makedirs(path)

        csv_file=path+"/"+str(file_name)
        
        w_file = open(csv_file, 'w')

        headings = data[0].keys()

        header_string = ','.join(map(str, headings))
        header_string = header_string + '\n'

        header = (header_string)

        w_file.write((header))

        for row in data:
            new_row = []

            for item in headings:
                
                

                new_row.append(str(row[item]).replace(',','!'))

            row_as_string = ",".join(new_row)
            w_file.write(row_as_string + '\n')

        w_file.close()
        return "done"
    except Exception as e:
        print str(e)






@app.route('/sendsmstounknownemails', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sendsmstounknownemails():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        # cur.execute("select name,mobile from resourcepersons where attendance_status=%s and email=%s and district in %s",('T','',tuple(['Mahabubnagar','Karimnagar','Adilabad'])))
        cur.execute("select phone from janasena_missedcalls where supporter_id in (select membership_id from membership_targets where achievement>=30)")
        contacts = cur.fetchall()
       
        msgToSend = "Hi, Janasena Party is conducting training classes for its members. If you are interested to participate in training sessions, please SMS TRAIN to 8096268268"
        send_bulk_sms_manual(contacts, msgToSend)

        return "sent successfully"

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


# import unicodedata
import tempfile
import os


def create_facilitatorcsv(data):
    """ returns (file_basename, server_path, file_size) """
    handle, filepath = tempfile.mkstemp(suffix='.csv')

    w_file = open(filepath, 'w')

    header = (
        'district,assembly,name,mobile,email,parliament_constituency_name,gender,voter_id,aadhar_id,confirmation,miss_call_status,state\n')
    w_file.write((header))

    print len(data)
    for row in data:
        new_row = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

        for item in row.keys():
            if item == 'district':
                new_row[0] = row[item]


            elif item == 'assembly':
                new_row[1] = row[item]
            elif item == 'name':
                new_row[2] = row[item]
            elif item == 'mobile':
                new_row[3] = row[item]
            elif item == 'email':
                new_row[4] = row[item]
            elif item == 'parliament_constituency_name':
                new_row[5] = row[item]
            elif item == 'gender':
                new_row[6] = row[item]
            elif item == 'voter_id':
                new_row[7] = row[item]
            elif item == 'aadhar_id':
                new_row[8] = row[item]
            elif item == 'confirmation':
                new_row[9] = row[item]
            elif item == 'miss_call_status':
                new_row[10] = row[item]
            elif item == 'state':
                new_row[11] = row[item]

        row_as_string = str(new_row)
        w_file.write(row_as_string[1:-1] + '\n')

    w_file.close()
    w_file = open(filepath, 'r')
    file_size = len(w_file.read())

    return filepath, file_size


@app.route('/facilitatorsDetails', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def facilitatorsDetails():
    return render_template('admin/facilitators.html')


@app.route('/getFacilitatorData', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def getFacilitatorData():
    page = request.values.get("page")
    if page is None:
        page = 0
    offset = 1000 * int(page)
    print offset
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select r.district,r.assembly,r.name,r.mobile,r.email,a.parliament_constituency_name,r.gender,r.age,r.voter_id,r.aadhar_id,r.confirmation,r.miss_call_status from resourcepersons r left join assembly_constituencies a on r.assembly=lower(a.constituency_name) where attendance_status=%s and (details_status=%s or miss_call_status=%s)  order by r.seq_id limit %s offset %s ",
            ('T', 'T', 'T', 1000, offset))

        rows = cur.fetchall()
        cur.execute(
            "select count(*) from resourcepersons where attendance_status=%s and (details_status=%s or miss_call_status=%s)",
            ('T', 'T', 'T'))
        count = cur.fetchone()
        print count
        if len(rows):
            return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "count": count['count']}})

        return json.dumps({"errors": ['no data found'], "data": {"status": "0"}})

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": [str(e)], "data": {"status": "0"}})

    #########################################################################################



@app.route('/getfacilitatorsFinalTeam', methods=['GET'])
@admin_login_required
def getfacilitatorsFinalTeam():
    data = []
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select parliament_constituency_name from parliament_constituencies order by seq_id")
        rows = cur.fetchall()
        for row in rows:
            data.append(str(row['parliament_constituency_name']).rstrip())

    except psycopg2.Error as e:
        print str(e)

    return render_template("admin/facilitators_final.html", data=data)


@app.route('/getfacilitatorsTeam', methods=['GET'])
@admin_login_required
def getfacilitatorsTeam():
    try:
        page = request.values.get("page")
        if page is None:
            page = 0
        offset = 1000 * int(page)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select f.name,f.email,f.mobile,f.assembly,f.district,f.age,f.gender,f.address,f.qualification,f.profession,f.association,f.voter_id,f.aadhar_id,a.parliament_constituency_name,f.status from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly) and lower(a.district_name)=lower(f.district) order by f.seq_id limit %s offset %s",
            (1000, offset))
        rows = cur.fetchall()
        cur.execute("select count(*) as count from facilitators ")
        count = cur.fetchone()
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "count": count['count']}})
    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": [], "data": {"status": "0"}})


@app.route('/downloadfacilitatorsFinalTeam', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def downloadfacilitatorsFinalTeam():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select f.name,f.email,f.mobile,f.assembly,f.district,f.age,f.gender,f.qualification,f.profession,f.association,f.voter_id,f.aadhar_id,a.parliament_constituency_name,f.status from facilitators f join assembly_constituencies a  on lower(a.constituency_name)=lower(f.assembly) and lower(a.district_name)=lower(f.district) order by f.seq_id ")
        rows = cur.fetchall()
        if len(rows):
            filepath, file_size = writeDictToCSVFIle(rows)

            return_file = open(filepath, 'r')

            datetime_string = time.strftime("%Y%m%d%H%M%S")
            file_basename = 'facilitator' + str(datetime_string) + ".csv"

            response = make_response(return_file.read(), 200)

            response.headers['Content-Description'] = 'File Transfer'
            response.headers['Cache-Control'] = 'no-cache'
            response.headers['Content-Type'] = 'text/csv'
            response.headers['Content-Disposition'] = 'attachment; filename=%s' % file_basename
            response.headers['Content-Length'] = file_size
            return response

        return 'no data found'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


def writeDictToCSVFIle(data):
    """ returns (file_basename, server_path, file_size) """
    handle, filepath = tempfile.mkstemp(suffix='.csv')

    w_file = open(filepath, 'w')

    headings = data[0].keys()

    header_string = ','.join(map(str, headings))
    header_string = header_string + '\n'

    header = (header_string)

    w_file.write((header))

    for row in data:
        new_row = []

        for item in headings:
            if row[item] is None or row[item] == 'None':
                row[item] = ''
            new_row.append(str(row[item]).replace(",","!"))

        row_as_string = ",".join(new_row)
        w_file.write(row_as_string + '\n')

    w_file.close()
    w_file = open(filepath, 'r')
    file_size = len(w_file.read())

    return filepath, file_size


@app.route('/generateMpConstituencyReport', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def generateMpConstituencyReport():
    parliament_constituency = request.values.get("mp_constituency")
    if parliament_constituency is None or parliament_constituency == '':
        return json.dumps({"errors": ["mp_constituency missing"], "data": {"status": "0"}})
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data = {"status_wise": {}, "gender_wise": {}, "profession_wise": {}, "qualification_wise": {},
                "assembly_wise": {}, "age_wise": {}}
        cur.execute(
            "select f.status,count(*) as old_new,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.status,a.parliament_constituency_name having a.parliament_constituency_name=%s;",
            (parliament_constituency,))
        status_stats = cur.fetchall()
        data['status_wise'] = status_stats
        cur.execute(
            "select f.gender,count(*) as gender_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.gender,a.parliament_constituency_name having a.parliament_constituency_name=%s ;",
            (parliament_constituency,))
        gender_stats = cur.fetchall()
        data['gender_wise'] = gender_stats
        cur.execute(
            "select f.profession,count(*) as profession_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.profession,a.parliament_constituency_name having a.parliament_constituency_name=%s ;",
            (parliament_constituency,))
        profession_stats = cur.fetchall()
        data['profession_wise'] = profession_stats
        cur.execute(
            "select f.qualification,count(*) as qualification_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.qualification,a.parliament_constituency_name having a.parliament_constituency_name=%s ;",
            (parliament_constituency,))
        qualification_wise = cur.fetchall()
        data['qualification_wise'] = qualification_wise
        cur.execute(
            "select f.assembly,count(*) as assembly_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by f.assembly,a.parliament_constituency_name having a.parliament_constituency_name=%s ;",
            (parliament_constituency,))
        assembly_wise = cur.fetchall()
        data['assembly_wise'] = assembly_wise
        cur.execute(
            "select case when f.age  between '0' and '25' then '0-25' when f.age between '26' and '35' then '26-35' when f.age between '36' and '45' then '36-45' when age between '46' and '55' then '46-55' when age between '56' and '65' then '56-65' when age >'65' then '65+' else 'not mentioned' END as age_range,count(f.age) as age_stats,cast(round((count(*) *100 / SUM(count(*)) OVER ()),2) as text) AS  percentage from facilitators f join assembly_constituencies a on lower(a.constituency_name)=lower(f.assembly)  group by age,a.parliament_constituency_name,age_range having a.parliament_constituency_name=%s order by age_range ;",
            (parliament_constituency,))
        age_wise = cur.fetchall()

        data['age_wise'] = age_wise
        con.commit()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1", "children": data}})


    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@app.route('/socialmediaApplicants', methods=['GET'])
@admin_login_required
def socialmediaApplicants():
    data = []
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select parliament_constituency_name from parliament_constituencies order by seq_id")
        rows = cur.fetchall()
        for row in rows:
            data.append(str(row['parliament_constituency_name']).rstrip())

    except psycopg2.Error as e:
        print str(e)

    return render_template("admin/socialmediaapplicants.html", data=data)


@app.route('/GetsocialmediavolunteersData', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def GetsocialmediavolunteersData():
    try:
        page = request.values.get("page")
        if page is None:
            page = 0
        offset = 1000 * int(page)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select name,fathername,mothername,mobile,email,voter_id,aadhar_id,pc_name,assembly,replace(cast(skills as text),',',' and ') as skills,city_town,pincode,image_url from socialmediavolunteers order by seq_id limit %s offset %s",
            (1000, offset))

        rows = cur.fetchall()
        cur.execute("select count(*) as count from socialmediavolunteers")
        count = cur.fetchone()
        return json.dumps({"errors": [], "data": {"status": "1", "children": rows, "count": count['count']}})


    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ["something wrong"], "data": {"status": "0"}})


@app.route('/DownloadsocialmediavolunteersData', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def DownloadsocialmediavolunteersData():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select name,fathername,mothername,mobile,email,voter_id,aadhar_id,pc_name,assembly,replace(cast(skills as text),',',' and ') as skills,city_town,pincode,image_url from socialmediavolunteers")
        rows = cur.fetchall()
        if len(rows):
            filepath, file_size = writeDictToCSVFIle(rows)

            return_file = open(filepath, 'r')

            datetime_string = time.strftime("%Y%m%d%H%M%S")
            file_basename = 'socialmediateam' + str(datetime_string) + ".csv"

            response = make_response(return_file.read(), 200)

            response.headers['Content-Description'] = 'File Transfer'
            response.headers['Cache-Control'] = 'no-cache'
            response.headers['Content-Type'] = 'text/csv'
            response.headers['Content-Disposition'] = 'attachment; filename=%s' % file_basename
            response.headers['Content-Length'] = file_size
            return response

        return 'no data found'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return json.dumps({"errors": ["something wrong"], "data": {"status": "0"}})


# @app.route('/socialmediavolunteer',methods=['GET','POST'])
# @crossdomain(origin="*",headers="Content-Type")

# def socialmediavolunteer():
#     if request.method=='GET':
#         data=[]
#         try:
#             con = psycopg2.connect(config.DB_CONNECTION)
#             cur = con.cursor(cursor_factory=RealDictCursor)
#             cur.execute("select parliament_constituency_name from parliament_constituencies order by parliament_constituency_name")
#             rows=cur.fetchall()
#             if len(rows):
#                 for row in rows:
#                     data.append(str(row['parliament_constituency_name']).rstrip())

#         except (psycopg2.Error,Exception) as e:
#             print str(e)
#             return str(e)
#         return render_template("website/socialmediavolunteer.html",data=data)
#     else:

#         name=request.form.get("name")
#         fathername=request.form.get("fathername")
#         mothername=request.form.get("mothername")
#         mobile=request.form.get("mobile")
#         email=request.form.get("email")
#         voter_id=request.form.get("voter_id")
#         aadhar_id=request.form.get("aadhar_id")
#         parliament_constituency=request.form.get("parliament_constituency")
#         assembly_constituency=request.form.get("assembly_constituency")
#         skills=request.form.get("skills[]")
#         city_town=request.form.get("city_town")
#         pincode=request.form.get("pincode")

#         address=request.form.get("address")
#         profile_pic=request.files['profile_pic']
#         errors=[]
#         required_fields=['name','fathername','mothername','mobile','email','aadhar_id','parliament_constituency','assembly_constituency','skills[]','address']

#         for field in required_fields:

#             if request.form.get(field) is None or request.form.get(field)=='':
#                 errors.append(field+" Field Missing")
#         if profile_pic is None:
#             errors.append("Your Photo Missing")
#         if len(errors):

#             return json.dumps({"errors":errors,"data":{"status":"0"}})

#         image_url=upload_image_web(profile_pic,'socialmediaimages')
#         print image_url
#         try:
#             con = psycopg2.connect(config.DB_CONNECTION)
#             cur = con.cursor(cursor_factory=RealDictCursor)
#             cur.execute("select * from socialmediavolunteers where mobile=%s or lower(email)=%s",(mobile,email))
#             existing=cur.fetchone()
#             if existing is None:
#                 cur.execute("insert into socialmediavolunteers(name,fathername,mothername,mobile,email,voter_id,aadhar_id,pc_name,assembly,skills,address,city_town,pincode,image_url)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ",(name,fathername,mothername,mobile,email.lower(),voter_id,aadhar_id,parliament_constituency,assembly_constituency,json.dumps(skills),address,city_town,pincode,image_url))
#                 con.commit()
#                 con.close()


#                 return json.dumps({"errors":[],"data":{"status":"1"}})


#             return json.dumps({"errors":["Mobile or Email already Existed. please register with different details"],"data":{"status":"0"}})

#         except (psycopg2.Error,Exception) as e:
#             print str(e)
#             con.close()
#             errors.append(str(e))

#             return json.dumps({"errors":[errors],"data":{"status":"0"}})


################## media dashboard services ##########
@app.route('/writemediacontacts', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def writemediacontacts():
    json_path = './excel_files/facilitator/araku/mediacontacts.json'
    json_data = open(json_path).read()
    data = json.loads(json_data)
    data = data['data']
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        for row in data:
            mobile = str(row['Phone_Number']).split(".")[0]
            cur.execute("insert into media_contacts(mobile)values(%s)", (mobile,))
        con.commit()
        con.close()
        return 'success'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


@app.route('/sendMediaMessages', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def sendMediaMessages():
    message = request.values.get("message")
    message_type = request.values.get("type")
    if message is None or message == '':
        return json.dumps({"errors": ["message should not be empty"], "data": {"status": "0"}})
    if message_type is None or message_type == '':
        message_type = 'T'
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        if message_type == 'T':
            contacts = [{"mobile": '9676881991', "name": "Rambabu"}, {'mobile': '9949912346', 'name': "Venu Gopal"}]
        else:
            cur.execute("select name,mobile from media_contacts ")
            # where status='A'
            contacts = cur.fetchall()
        if len(contacts):
            for contact in contacts:
                name = contact['name']
                mobile = contact['mobile']
                print message
                print mobile
                mobile = "91" + str(mobile)
                sendSMS(mobile, message)
        return 'success'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)


##################################################
@crossdomain(origin="*", headers="Content-Type")
@app.route('/testFCM', methods=['POST'])
def testFCM():
    payload = {
        "to": "chhZMj2jNWw:APA91bES9QqCyjb4YExMhBnkPK4D6dkdU7dBp4fpJcn1VNZ4HeGRA_y3OeCynq4Lf__188yNkYZ9eLjcnQNZQ0_-10dmXxqS4rq7uGw_23GjvzYITa8daDXNuJwYZlacF6pASErO6gJM",
        "data": {
            "someData": "This is some data",
            "someData2": "etc"
        }
    }

    url = 'https://fcm.googleapis.com/fcm/send'
    myKey = "AIzaSyBdS7w-Cd32ZdqOczt_L-pvCXJVuPSUDUo"
    payload = json.dumps(payload)

    headers = {'Content-Type': 'application/json', 'Authorization': 'key=%s' % myKey}
    req = urllib2.Request(url, payload, headers)
    f = urllib2.urlopen(req)
    response = json.loads(f.read())
    print "------------------"
    print response
    print "-------android push responseee class--------------"
    return str(response)



################## accessmanagement services ##########


def getPermissiondata(membershipId):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select membership_id as MembershipId,region_id as  RegionID,permission_level_id as PermissionLevel,issuperadmin as IsAdmin from user_permission where membership_id=%s",
            (str(membershipId).upper(),))
        details = cur.fetchone()
        if details:
            return details
        else:
            d = {}
            d['MembershipId'] = membershipId
            d['RegionID'] = ''
            d['PermissionLevel'] = ''
            d['IsAdmin'] = ''
            return d

    except psycopg2.Error  as e:
        print str(e)
        return {}


@app.route("/write_txt_to_json", methods=['GET'])
@csrf.exempt
def write_txt_to_json():
    files = ['Kurnool', 'nellore', 'Prakasam', 'Srikakulam', 'Tirupati', 'Visakhapatnam', 'Vizianagaram Dist',
             'West Godavari', 'East godavari', 'Prakasam']
    missed = []
    for file in files:
        path = 'polling stations/' + file
        json_path = 'polling stations/Json/' + file
        # try:
        #     os.stat(json_path)
        # except Exception as e:
        #     os.mkdir(json_path)
        for filename in os.listdir(path):
            if filename[0] != '.':
                geo_file = path + "/" + filename
                file_name_with = filename.split(".")[1]
                if file_name_with != 'json':
                    print geo_file
                    os.remove(geo_file)

                # json_file=json_path+"/"+ file_name_with+".json"
                # print json_file
                # contents=''
                # with open(geo_file, 'rb') as f:
                #     contents = f.read()
                # print type(contents)
                # with open(json_file, "wb") as fout:
                #     json.dump(contents, fout, indent=1, encoding="utf-8", ensure_ascii=True)

    return 'success'


@app.route('/AMS/Permissions', methods=['POST', 'GET', 'PUT', 'DELETE'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def amsPermission():
    if request.method == 'POST':
        name = request.values.get('name')
        description = request.values.get('description')

        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})

        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            admin_id = current_user.id
            cur.execute("select * from permission_level where lower(name)=%s", (name.lower(),))
            existing = cur.fetchone()
            if existing is None:
                cur.execute('insert into permission_level(name,description,created_by,status) values(%s,%s,%s,%s)',
                            (name, description, admin_id, 'A'))
                con.commit()

                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
            return json.dumps({'errors': ["Permission name already Existed"], 'data': {'status': '0'}})
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    elif request.method == 'GET':
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select p.id,p.name,p.description,p.status,concat_ws(' ',a.first_name::text,a.last_name) as lastChangedBy from permission_level p join admin_team a on a.admin_id = p.created_by  order by id")
            rows = cur.fetchall()

            con.close()
            return render_template("Accessmanagement/Permissions.html", data=rows)
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return str(e)
    elif request.method == "PUT":
        name = request.values.get('name')
        description = request.values.get('description')
        permission_id = request.values.get('id')
        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if permission_id is None or permission_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("select * from permission_level where lower(name)=%s and id!=%s", (name.lower(), permission_id))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    "update permission_level set name=%s,description=%s,created_by=%s,update_dttm=now() where id=%s",
                    (name, description, '1', permission_id))
                con.commit()
                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Permission updated"}})

            return json.dumps({'errors': ["Permission name already Existed"],
                               'data': {'status': '0', 'msg': "Permission name already Existed"}})


        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})
    elif request.method == "DELETE":

        permission_id = request.values.get('id')

        if permission_id is None or permission_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("update permission_level set status=%s,created_by=%s,update_dttm=now() where id=%s",
                        ('D', admin_id, permission_id))
            con.commit()
            con.close()
            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Permission Disabled Successfully"}})

        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})


@app.route('/AMS/RegionLevels', methods=['POST', 'GET', 'PUT', 'DELETE'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def amsRegionLevels():
    if request.method == 'POST':
        name = request.values.get('name')
        description = request.values.get('description')

        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})

        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            admin_id = current_user.id
            cur.execute("select * from region_level where lower(name)=%s", (name.lower(),))
            existing = cur.fetchone()
            if existing is None:
                cur.execute('insert into region_level(name,description,created_by,status) values(%s,%s,%s,%s)',
                            (name, description, admin_id, 'A'))
                con.commit()

                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
            return json.dumps({'errors': ["region level already Existed"], 'data': {'status': '0'}})
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    elif request.method == 'GET':
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select rl.id,rl.name,rl.description,rl.status,concat_ws(' ',a.first_name::text,a.last_name) as lastChangedBy from region_level rl join admin_team a on a.admin_id = rl.created_by  order by id")
            rows = cur.fetchall()

            con.close()
            return render_template("Accessmanagement/RegionLevels.html", data=rows)
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return str(e)
    elif request.method == "PUT":
        name = request.values.get('name')
        description = request.values.get('description')
        region_level_id = request.values.get('id')
        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if region_level_id is None or region_level_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("select * from region_level where lower(name)=%s and id!=%s", (name.lower(), region_level_id))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    "update permission_level set name=%s,description=%s,created_by=%s,update_dttm=now() where id=%s",
                    (name, description, '1', region_level_id))
                con.commit()
                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Permission updated"}})

            return json.dumps({'errors': ["Permission name already Existed"],
                               'data': {'status': '0', 'msg': "Permission name already Existed"}})


        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})
    elif request.method == "DELETE":

        region_level_id = request.values.get('id')

        if region_level_id is None or region_level_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("update region_level set status=%s,created_by=%s,update_dttm=now() where id=%s",
                        ('D', admin_id, region_level_id))
            con.commit()
            con.close()
            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Region Level Disabled Successfully"}})

        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})


@app.route('/AMS/RegionTypes', methods=['POST', 'GET', 'PUT', 'DELETE'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def amsRegionTypes():
    if request.method == 'POST':
        name = request.values.get('name')
        description = request.values.get('description')
        level_id = request.values.get('level_id')
        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if level_id is None or level_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_level_id_MISSING'], 'data': {'status': '0'}})
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            admin_id = current_user.id
            cur.execute("select * from region_type where lower(name)=%s", (name.lower(),))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    'insert into region_type(name,description,created_by,status,regionlevelid) values(%s,%s,%s,%s,%s)',
                    (name, description, admin_id, 'A', level_id))
                con.commit()

                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
            return json.dumps({'errors': ["RegionType name already Existed"], 'data': {'status': '0'}})
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    elif request.method == 'GET':
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select r.id,r.name,r.description,r.status,concat_ws(' ',a.first_name::text,a.last_name) as lastChangedBy,rl.name as regionlevel from region_type r join admin_team a on a.admin_id = r.created_by join region_level rl on rl.id=r.regionlevelid  order by id")
            rows = cur.fetchall()

            con.close()
            return render_template("Accessmanagement/Regiontypes.html", data=rows)
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return str(e)
    elif request.method == "PUT":
        name = request.values.get('name')
        description = request.values.get('description')
        region_type_id = request.values.get('id')
        region_level_id = request.values.get('level_id')
        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if region_type_id is None or region_type_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_region_type_id_MISSING'], 'data': {'status': '0'}})
        if region_level_id is None or region_level_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_region_level_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("select * from region_type where lower(name)=%s and id!=%s", (name.lower(), region_type_id))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    "update region_type set name=%s,description=%s,regionlevelid=%s,created_by=%s,update_dttm=now() where id=%s",
                    (name, description, region_level_id, admin_id, region_type_id))
                con.commit()
                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Permission updated"}})

            return json.dumps({'errors': ["Permission name already Existed"],
                               'data': {'status': '0', 'msg': "Permission name already Existed"}})


        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})
    elif request.method == "DELETE":

        region_type_id = request.values.get('id')

        if region_type_id is None or region_type_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("update region_type set status=%s,created_by=%s,update_dttm=now() where id=%s",
                        ('D', admin_id, region_type_id))
            con.commit()
            con.close()
            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Region Type Disabled Successfully"}})

        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})


@app.route('/AMS/Regions', methods=['POST', 'GET', 'PUT', 'DELETE'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def amsRegions():
    if request.method == 'POST':
        name = request.values.get('name')
        description = request.values.get('description')
        region_type = request.values.get('region_type')
        region_parent = request.values.get('region_parent')

        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if region_type is None or region_type == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_region_type_MISSING'], 'data': {'status': '0'}})

        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            admin_id = current_user.id
            cur.execute("select * from region where lower(name)=%s", (name.lower(),))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    'insert into region(name,description,created_by,status,regiontypeiid,parentregionid) values(%s,%s,%s,%s,%s,%s)',
                    (name, description, admin_id, 'A', region_type, region_parent))
                con.commit()

                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
            return json.dumps({'errors': ["RegionType name already Existed"], 'data': {'status': '0'}})
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    elif request.method == 'GET':
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                "select r.id,r.name,r.description,concat_ws(' ',a.first_name::text,a.last_name) as lastChangedBy,rt.name as regionType,rp.name as regionParent,rpt.name as parentregiontype ,r.status from region r  join region_type rt on rt.id=r.regiontypeiid join admin_team a on a.admin_id = r.created_by left join region rp on rp.id=r.parentregionid left join region_type rpt on rpt.id=rp.regiontypeiid order by r.id")
            rows = cur.fetchall()
            for row in rows:
                print row['id']
                print str(row['name'])

            con.close()
            return render_template("Accessmanagement/Regions.html", data=rows)
        except psycopg2.Error as e:
            print str(e)
            con.close()
            return str(e)
    elif request.method == "PUT":
        name = request.values.get('name')
        description = request.values.get('description')
        region_type_id = request.values.get('id')
        region_id = request.values.get('region_id')
        region_parent_id = request.values.get('region_parent_id')
        if name is None or name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_name_MISSING'], 'data': {'status': '0'}})
        if region_type_id is None or region_type_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_region_type_id_MISSING'], 'data': {'status': '0'}})
        if region_id is None or region_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_region_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("select * from region where lower(name)=%s and id!=%s", (name.lower(), region_type_id))
            existing = cur.fetchone()
            if existing is None:
                cur.execute(
                    "update region set name=%s,description=%s,created_by=%s,update_dttm= now(),regiontypeiid=%s,parentregionid=%s where id=%s",
                    (name, description, admin_id, region_type_id, region_parent_id, region_id))
                con.commit()
                con.close()
                return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Permission updated"}})

            return json.dumps({'errors': ["Permission name already Existed"],
                               'data': {'status': '0', 'msg': "Permission name already Existed"}})


        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})
    elif request.method == "DELETE":

        region_id = request.values.get('id')

        if region_id is None or region_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_id_MISSING'], 'data': {'status': '0'}})
        try:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            admin_id = current_user.id
            cur.execute("update region set status=%s,created_by=%s,update_dttm=now() where id=%s",
                        ('D', admin_id, region_id))
            con.commit()
            con.close()
            return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "Region Type Disabled Successfully"}})

        except psycopg2.Error as e:
            print str(e)
            con.close()
            return json.dumps({'errors': ["something went Wrong"], 'data': {'status': '0'}})


@app.route('/AMS/CadreManagement', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def amsCadreManagement():
    if request.method == 'GET':
        try:
            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute(
                """select m.membership_id,m.name,m.phone,m.email,m.polling_station_name,a.constituency_name,m.state,a.district_name,a.parliament_constituency_name,p.name as permission_level,r.name as region_name,rt.name as region_type from janasena_membership m left join assembly_constituencies a on (a.state_id=case when m.state='AP' then 1 else 2 end and m.constituency_id=a.constituency_id )  left join user_permission u on u.membership_id=m.membership_id left join region r on u.region_id=r.id left join region_type rt on rt.id=r.regiontypeiid left join permission_level p on u.permission_level_id=p.id order by m.create_dttm desc limit 100""")
            rows = cur.fetchall()
            return render_template("Accessmanagement/cadre.html", data=rows)
        except psycopg2.Error as e:
            print str(e)
            return render_template("Accessmanagement/cadre.html", data=[])


@app.route('/CadreAccessManagement', methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def CadreAccessManagement():
    try:
        membershipId = request.values.get('membershipId')
        region_id = request.values.get('region')
        permission_id = request.values.get('permission_id')
        if membershipId is None or membershipId == '':
            return json.dumps({"errors": ["parameter membership id Missing"], "data": {"status": "0"}})
        if region_id is None or region_id == '':
            return json.dumps({"errors": ["parameter region_id Missing"], "data": {"status": "0"}})
        if permission_id is None or permission_id == '':
            return json.dumps({"errors": ["parameter permission_id Missing"], "data": {"status": "0"}})
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select * from user_permission where membership_id=%s", (membershipId,))
        existing = cur.fetchone()
        if existing is None:
            cur.execute("insert into user_permission(membership_id,region_id,permission_level_id)values(%s,%s,%s)",
                        (membershipId, region_id, permission_id))
        else:
            cur.execute("update user_permission set region_id=%s,permission_level_id=%s where membership_id=%s ",
                        (region_id, permission_id, membershipId))
        con.commit()
        con.close()
        return json.dumps({"errors": [], "data": {"status": "1"}})
    except psycopg2.Error as e:
        print str(e)
        json.dumps({"errors": ["Something Wrong"], "data": {"status": "0"}})


@app.route('/CadreManagement', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def CadreManagement():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            """select m.membership_id,m.name,m.phone,m.email,m.polling_station_name,a.constituency_name,m.state,a.district_name,a.parliament_constituency_name,p.name as permission_level,r.name as region_name,rt.name as region_type from janasena_membership m left join assembly_constituencies a on (a.state_id=case when m.state='AP' then 1 else 2 end and m.constituency_id=a.constituency_id )  left join user_permission u on u.membership_id=m.membership_id left join region r on u.region_id=r.id left join region_type rt on rt.id=r.regiontypeiid left join permission_level p on u.permission_level_id=p.id order by m.create_dttm desc limit 100""")
        rows = cur.fetchall()
        return render_template("admin/cadre.html", data=rows)

    except psycopg2.Error as e:
        print str(e)
        return render_template("admin/cadre.html", data=[])


@app.route('/CadredetailsById', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def CadredetailsById():
    try:
        MembershipId = request.values.get('MembershipId')
        if MembershipId is None or MembershipId == '':
            return json.dumps({"errors": ["parameter membershipid is missiong"], "data": {"status": "0"}})

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            """select m.membership_id,m.name,m.phone,m.email,m.polling_station_name,a.constituency_name,m.state,a.district_name,a.parliament_constituency_name,p.name as permission_level,r.name as region_name,rt.name as region_type from janasena_membership m left join assembly_constituencies a on (a.state_id=case when m.state='AP' then 1 else 2 end and m.constituency_id=a.constituency_id )  left join user_permission u on u.membership_id=m.membership_id left join region r on u.region_id=r.id left join region_type rt on rt.id=r.regiontypeiid left join permission_level p on u.permission_level_id=p.id where m.membership_id=%s order by m.create_dttm limit 100""",
            (MembershipId,))
        row = cur.fetchone()
        if row is not None:

            row['phone'] = encrypt_decrypt(str(row['phone']), 'D')
            if row['email'] is not None and row['email'] != '':
                row['email'] = encrypt_decrypt(str(row['email']), 'D')
            return json.dumps({"errors": [], "data": {"status": "1", "children": row}})
        return json.dumps({"errors": ['No data found'], "data": {"status": "0"}})

    except psycopg2.Error as e:
        print str(e)
        return json.dumps({"errors": ['something wrong '], "data": {"status": "0"}})


@app.route('/updateresourcepersonsage', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def updateresourcepersonsage():
    start = request.values.get('start')
    end = request.values.get('end')
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        json_path = './excel_files/facilitator/araku/resourcepersons.json'
        json_data = open(json_path).read()
        data = json.loads(json_data)
        data = data['data']
        for row in data:
            try:
                age = row['age']
                mobile = row['mobile']
                mobile = str(mobile).split(".")[0]
                print age, mobile
                age = str(age).split(".")[0]
                if len(str(age)) <= 3:
                    cur.execute("update resourcepersons set age=%s where mobile=%s", (age, mobile))

            except psycopg2.Error as e:

                print str(e)
                continue
        con.commit()
        con.close()
        return 'success'
    except psycopg2.Error as e:
        print str(e)
        return 'failed'


@app.route('/prepareRegionFiles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def prepareRegionFiles():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select id,name,status from region_level")
        regionlevels = cur.fetchall()

        with open("./static/json/regions/regionlevel.json", "wb") as jsfile:
            jsfile.truncate()
            jsfile.write(json.dumps(regionlevels, indent=2))
        cur.execute("select id,name,status,regionlevelid from region_type")
        regiontypes = cur.fetchall()
        with open("./static/json/regions/regiontype.json", "wb") as jsfile:
            jsfile.truncate()
            jsfile.write(json.dumps(regiontypes, indent=2))
        cur.execute("select id,name,status,regiontypeiid,parentregionid from region")
        regions = cur.fetchall()
        with open("./static/json/regions/region.json", "wb") as jsfile:
            jsfile.truncate()
            jsfile.write(json.dumps(regions, indent=2))
        return 'success'

    except psycopg2.Error as e:
        print str(e)
        return 'failed'


@app.route('/addmandalstoregions', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def addmandalstoregions():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        state_id = 1
        parentregionid = 2
        mandals = []
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select id,name from region where regiontypeiid=3  and parentregionid=%s", (parentregionid,))

        district_rows = cur.fetchall()

        for drow in district_rows:
            parent_district_id = drow['id']
            parent_district_name = drow['name']
            cur.execute("select id,name from region where regiontypeiid=4 and parentregionid=%s", (parent_district_id,))
            assembly_region_rows = cur.fetchall()
            for arow in assembly_region_rows:
                parent_assembly_id = arow['id']
                parent_assembly_name = arow['name']
                cur.execute(
                    "select seq_id,mdl_name from geocoordinates_mdl where state_id=%s and constituency_id= (select constituency_id from assembly_constituencies where constituency_name=%s and district_name=%s)",
                    (state_id, parent_assembly_name, parent_district_name))
                mandal_rows = cur.fetchall()
                for mandal in mandal_rows:
                    cur.execute(
                        "insert into region(name,description,regiontypeiid,parentregionid,status,created_by,sourceid)values(%s,%s,%s,%s,%s,%s,%s)",
                        (mandal['mdl_name'], mandal['mdl_name'], 9, parent_assembly_id, 'A', 1, mandal['seq_id']))
                    con.commit()

        return 'success'

    except psycopg2.Error as e:
        print str(e)
        return 'failed'


########################## NRI Services ###########
@app.route("/api/NRIProfile", methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def NRIProfile():
    if request.method=='GET':
        auth = request.authorization
        nri_id = auth.username
        
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select email,phone,profession,twitter,facebook,linked_in,door_no,street,city,state,country from nri_members where membership_id=%s",(nri_id,))
        row=cur.fetchone()
        con.close()
        if row is not None:
            return makeResponse(json.dumps({"status": "1", "data":row }), 200,
                                'application/json')

        else:
            return makeResponse(json.dumps({"status": "1", "data":{} }), 200,
                                'application/json')


    else:

        try:
            auth = request.authorization
            nri_id = auth.username
            
            
            try:
                twitter=request.json['twitter_handle']
                facebook=request.json['facebook_id']
                linked_in=request.json['linked_in']
                door_no=request.json['door_no']
                street=request.json['street']
                state = request.json['state']
                email=request.json['email']
                phone=request.json['phone']
                profession = request.json['profession']
                city = request.json['city']
                country = request.json['country']
            
            except (KeyError, Exception) as e:
                print str(e)
                return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select 1 from nri_members where membership_id=%s",(nri_id,))
            existing=cur.fetchone()
            if existing is not None:
                cur.execute(
                    """update nri_members set email=%s,phone=%s,profession=%s,
                    twitter=%s,facebook=%s,linked_in=%s,door_no=%s,street=%s,
                    city=%s,state=%s,country=%s,update_dttm=now()
                     where membership_id=%s""",
                    (email,phone,profession,twitter,facebook,linked_in,door_no,
                        street,city,state,country,nri_id
                         ))
                con.commit()
            else:
                cur.execute("""insert into nri_members
                    (email,phone,profession,twitter,facebook,
                        linked_in,door_no,street,city,state,country,membership_id)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                (email,phone,profession,twitter,facebook,linked_in,door_no,
                        street,city,state,country,nri_id))
                con.commit()
            con.close()
            return makeResponse(json.dumps({"status": "1", "message": "Profile Updated Successfully"}), 200,
                                'application/json')

        except psycopg2.Error as e:
            print str(e)
            con.close()
            return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')


@app.route("/api/getNRIReferrals", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def getNRIReferrals():
    try:
        auth = request.authorization
        nri_id = auth.username

        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select id,phone,first_name,last_name,age,gender,email,constituency_id,state,district,profession,zipcode,voter_id,aadhar_id,country,jsp_supporter,calls_count from nri_referrals  where nriid=%s order by create_dttm desc limit 100",
            (nri_id,))
        rows = cur.fetchall()
        con.close()
        data = []
        if len(rows):
            data = rows
        return makeResponse(json.dumps({"status": "1", "children": data}), 200, 'application/json')
    except psycopg2.Error as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')

@app.route("/api/NRIReferralProgram", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def NRIReferralProgram():
    try:
        auth = request.authorization
        nri_id = auth.username
        
        try:

            firstName = request.json['firstName']
           
            lastName = request.json['lastName']
            
            state = request.json['state']
            district = request.json['district']
            constituency = request.json['constituency']
            jsp_supporter=request.json['jsp_supporter']

            age = request.json['age']
            profession = request.json['profession']
            gender = request.json['gender']
            
            email = request.json['email']
            
            country = request.json['countryOfResidence']
            zipCode = request.json['zipCode']
            mobileNumber = request.json['mobileNumber']
            voterId = request.json['voterId']
            aadharNumber = request.json['aadharNumber']
            
            
        except (KeyError, Exception) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')
        

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        referral_id=''
        cur.execute(
            "insert into  nri_referrals(nriid,phone,first_name,last_name,age,gender,email,constituency_id,state,district,profession,zipcode,voter_id,aadhar_id,country,jsp_supporter) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)returning id",
            (nri_id, mobileNumber, firstName, lastName, age, gender, email, constituency, state, district,
             profession, zipCode, voterId, aadharNumber, country,jsp_supporter))
        con.commit()
        referral_id=cur.fetchone()['id']
        con.close()
        return makeResponse(json.dumps({"status": "1", "message": "Added Successfully","id":referral_id}), 200,
                            'application/json')
       
    except psycopg2.Error as e:
        print str(e)

        return makeResponse(json.dumps({"errors": ["Something Wrong "]}), 500, 'application/json')


@app.route("/api/update_supporter_status", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def updateSupporterStatus():
    try:
        try:
            auth = request.authorization
            nri_id = auth.username
            
            jsp_supporter = request.json['jsp_supporter']
            id=request.json['id']

        except (KeyError, Exception) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "update nri_referrals set jsp_supporter=%s where id=%s and nriid=%s",
            (jsp_supporter,id,nri_id))
        con.commit()
        con.close()
        return makeResponse(json.dumps({"status": "1", "message": " Updated Successfully"}), 200,
                                'application/json')
    except psycopg2.Error as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Something Wrong "]}), 500, 'application/json')

    
@app.route("/api/update_calls_count", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def updateCallsCount():
    try:
        try:
            auth = request.authorization
            nri_id = auth.username
            
            id=request.json['id']

        except (KeyError, Exception) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "update nri_referrals set calls_count=calls_count+1 where id=%s and nriid=%s",
            (id,nri_id))
        con.commit()
        con.close()
        return makeResponse(json.dumps({"status": "1", "message": " Updated Successfully"}), 200,
                                'application/json')
    except psycopg2.Error as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Something Wrong "]}), 500, 'application/json')
    




@app.route("/api/updateNRIDeviceDetails", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def updateNRIDeviceDetails():
    if request.method == 'POST':
        try:
            auth = request.authorization
            nriid = auth.username
            device_id = request.json['deviceId']
            device_gcm_id = request.json['fcmId']
            deviceType = request.json['deviceType']

        except (KeyError, Exception) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')
        if device_id is not None and device_gcm_id is not None and deviceType is not None:
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("update nri_members set user_device_id=%s,user_gcm_id=%s,device_type=%s where membership_id=%s",
                            (device_id, device_gcm_id, deviceType, nriid))
                con.commit()
                con.close()
                return makeResponse(json.dumps({"status": "1", "message": "Device Details Updated Successfully"}), 200,
                                    'application/json')
            except psycopg2.Error as e:
                print str(e)
                return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')

        return makeResponse(json.dumps({"errors": ['please send correct details']}), 400, 'application/json')


############################# end of Nri services #############

@app.route("/api/getActivityDetails", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def getActivityDetails():
    try:
        state = request.values.get('state')
        constituency = request.values.get('constituency_id')
        if state is None or constituency is None:
            return makeResponse(json.dumps({"errors": ["Parameters missing "]}), 400, 'application/json')
        # con = psycopg2.connect(config.DB_CONNECTION)
        # cur = con.cursor(cursor_factory=RealDictCursor)
        # cur.execute("select phone,first_name,last_name,age,gender,email,constituency_id,state,district,profession,zipcode,voter_id,aadhar_id from nri_referrals where nriid=%s",(nriid,))
        # rows=cur.fetchall()
        # con.close()
        data = []
        event = {"policy": "Agriculture", "name": "JanaSena Rythu Sadassu", "location": "Vijayawada Benz Circle",
                 "time": "12-01-2018 11:00 AM", "agenda": "Meeting with Krishna District Formers"}
        data.append(event)

        return makeResponse(json.dumps({"status": "1", "children": data}), 200, 'application/json')
    except psycopg2.Error as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')


@app.route("/api/getPartyActivityInformation", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_authToken
def getPartyActivityInformation():
    try:

        data = [{"type": "1",
                 "details": "JanaSena Party Chief Pawan Kalyan Meeting With Undavalli Arun Kumar Press Meet 11/2/18"},
                {"type": "2", "details": "https://www.youtube.com/watch?v=3chZ5iTu6TQ"}]

        return makeResponse(json.dumps({"status": "1", "children": data}), 200, 'application/json')
    except psycopg2.Error as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')

@app.route("/MissedcallSMS", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt

def MissedcallSMS():
    try:
        from celery_tasks.sms_email import send_missedcall_sms
        send_missedcall_sms.delay()
        return 'success'

    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')

@app.route("/updateManualVotersId", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def updateManualVotersId():
    try:
        from celery_tasks.sms_email import update_manual_voter_count
        update_manual_voter_count.delay()
        return 'success'

    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')


# @crossdomain(origin="*",headers="Content-Type")

# def socialmediavolunteer():
#     if request.method=='GET':
#         data=[]
#         try:
#             assembly_instance=Assembly_constituencies_details()
#             districts=assembly_instance.get_districts_list()
#             data=[]
#             for district in districts:

#                 data.append(str(list(district)[0]).rstrip())
#         except (psycopg2.Error,Exception) as e:
#             print str(e)
#             return str(e)
#         return render_template("website/socialmediavolunteer.html",data=data)
#     else:

#         name=request.values.get("name")
#         dob=request.values.get("dob")
#         experience=request.values.get("experience")
#         mobile=request.values.get("mobile")
#         email=request.values.get("email")
#         voter_id=request.values.get("voter_id")
#         aadhar_id=request.values.get("aadhar_id")
#         district=request.values.get("district")
#         assembly_constituency=request.values.get("assembly_constituency")
#         skills=request.values.get("skills")
#         socialmedia=request.values.get("socialmedia")
#         city_town=request.values.get("city_town")
#         other_info=request.values.get("other_info")


#         errors=[]
#         required_fields=['name','dob','mobile','email','aadhar_id','district','assembly_constituency','skills','socialmedia']

#         for field in required_fields:

#             if request.values.get(field) is None or request.values.get(field)=='':
#                 errors.append(field+" Field Missing")

#         if len(errors):

#             return json.dumps({"errors":errors,"data":{"status":"0"}})


#         try:
#             con = psycopg2.connect(config.DB_CONNECTION)
#             cur = con.cursor(cursor_factory=RealDictCursor)
#             cur.execute("select name from sathagni_volunteers where mobile=%s or lower(email)=%s",(mobile,email))
#             existing=cur.fetchone()
#             if existing is None:


#                 cur.execute("insert into sathagni_volunteers(name,mobile,email,voter_id,aadhar_id,district,assembly,skills,socialmedia,city_town,other_info,experience,dob)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ",(name,mobile,str(email).lower(),voter_id,aadhar_id,district,assembly_constituency,skills,socialmedia,city_town,other_info,experience,dob))
#                 con.commit()
#                 con.close()

#                 return json.dumps({"errors":[],"data":{"status":"1"}})


#             return json.dumps({"errors":["Mobile or Email already Existed. please register with different details"],"data":{"status":"0"}})

#         except (psycopg2.Error,Exception) as e:
#             print str(e)
#             con.close()
#             errors.append(str(e))

#             return json.dumps({"errors":[errors],"data":{"status":"0"}})


## end of accessmanagement ######################
import urllib
import os
@app.route("/getpdffiles", methods=["GET"])
def fun():
    try:

        main_url="http://ceotelangana.nic.in/GE_2009/Form-20/"
        destination="e:/2009_files/"
        if not os.path.exists(constituency_folder):
                os.makedirs(constituency_folder)
        for row in range(120,295):
            file_name="AC_"+str(row)+".pdf"
            
                
            linkToFile = str(base_url)+file_name
            
            localDestination = destination + file_name
            resultFilePath, responseHeaders = urllib.urlretrieve(linkToFile, localDestination)
            print "done"
        return "true"
    except Exception as e:
        print str(e)
        return str(e)


@app.route("/districts", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def districts():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select seq_id as district_id,district_name from districts where state_id in (1,2) order by seq_id")
        rows = cur.fetchall()
        con.close()
        return json.dumps({'errors': [], 'data': {'status': '1', 'children': rows}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


@app.route("/constituencies", methods=["GET"])
@crossdomain(origin="*", headers="Content-Type")
def constituencies():
    try:
        district = request.values.get('district_id')
        if district is None or district == '':
            return json.dumps({"errors": ['param district_id missing'], "data": {"status": '0'}})
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select district_id,constituency_id,constituency_name from assembly_constituencies where district_id=%s order by constituency_id",
            (district,))
        rows = cur.fetchall()
        con.close()

        return json.dumps({'errors': [], 'data': {'status': '1', 'children': rows}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


# @app.route("/create_whatsappgroups",methods=["GET"])
# @crossdomain(origin="*", headers="Content-Type")
# def createWhatsappGroups():

#     district_id = request.values.get('district_id')
#     constituency_id = request.values.get('constituency_id')
#     if district_id is None or district_id == '':
#         return json.dumps({"errors": ['param district_id missing'], "data": {"status": '0'}})
#     if constituency_id is None or constituency_id == '':
#         return json.dumps({"errors": ['param constituency_id missing'], "data": {"status": '0'}})
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select district_name,constituency_name from assembly_constituencies where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#         row=cur.fetchone()


        
#         if row is not None:
#             district_name=row['district_name']
#             constituency_name=row['constituency_name']
#             dist_sub=district_name[0:4]
#             consti_sub=constituency_name[0:4]
#             consti_whatsapp_group_name=consti_sub+"_"+dist_sub+"_"+str(constituency_id)

#             cur.execute("select count(distinct(phone))as count from janasena_membership where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#             count_row=cur.fetchone()
#             total_members=count_row['count']
#             import math
#             no_of_groups=int(math.ceil(total_members/256))
#             print no_of_groups
#             cur.execute("select max(group_no) from whatsapp_groups where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#             max_row=cur.fetchone()
            
#             if max_row['max'] is not None:
#                 max_group=max_row['max']
#                 if max_group<no_of_groups:
#                     for i in range(max_group+1,no_of_groups+1):
#                         w_group=str(i)+"_"+consti_whatsapp_group_name
#                         cur.execute("insert into whatsapp_groups(district_id,constituency_id,group_no,group_name,status)values(%s,%s,%s,%s,%s)",(district_id,constituency_id,i,w_group,'F'))
#                         con.commit()

#             else:
#                 for i in range(1,no_of_groups+1):
#                     w_group=str(i)+"_"+consti_whatsapp_group_name
#                     cur.execute("insert into whatsapp_groups(district_id,constituency_id,group_no,group_name,status)values(%s,%s,%s,%s,%s)",(district_id,constituency_id,i,w_group,'F'))
#                     con.commit()

#             return json.dumps({'errors': [], 'data': {'status': '1'}})
#         return json.dumps({'errors': ['no details found'], 'data': {'status': '0'}})

#     except Exception as e:
#         print str(e)
#         return json.dumps({"errors": ['something wrong'], "data": {"status": '0'}})
        
# def whatsappgroups(district_id,constituency_id):
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select district_name,constituency_name from assembly_constituencies where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#         row=cur.fetchone()
        

        
#         if row is not None:
#             district_name=row['district_name']
#             constituency_name=row['constituency_name']
#             dist_sub=district_name[0:4]
#             consti_sub=constituency_name[0:4]
#             consti_whatsapp_group_name=consti_sub+"_"+dist_sub+"_"+str(constituency_id)

#             cur.execute("select count(distinct(phone))as count from janasena_membership where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#             count_row=cur.fetchone()
#             total_members=count_row['count']
#             import math
#             no_of_groups=((total_members+256-1)/256)
#             print no_of_groups
#             cur.execute("select max(group_no) from whatsapp_groups where district_id=%s and constituency_id=%s",(district_id,constituency_id))
#             max_row=cur.fetchone()
            
#             if max_row['max'] is not None:
#                 max_group=max_row['max']
#                 if max_group<no_of_groups:
#                     for i in range(max_group+1,no_of_groups+1):
#                         w_group=str(i)+"_"+consti_whatsapp_group_name
#                         cur.execute("insert into whatsapp_groups(district_id,constituency_id,group_no,group_name,status)values(%s,%s,%s,%s,%s)",(district_id,constituency_id,i,w_group,'F'))
#                         con.commit()

#             else:
#                 for i in range(1,no_of_groups+1):
#                     w_group=str(i)+"_"+consti_whatsapp_group_name
#                     cur.execute("insert into whatsapp_groups(district_id,constituency_id,group_no,group_name,status)values(%s,%s,%s,%s,%s)",(district_id,constituency_id,i,w_group,'F'))
#                     con.commit()
#             return True
#         return False
            
#     except Exception as e:
#         print str(e)
#         return False

# @app.route("/whatsapp_groups_status", methods=["POST"])
# @crossdomain(origin="*", headers="Content-Type")
# @csrf.exempt
# def whatsapp_groups_status():
#     ip_address=request.remote_addr
#     if ip_address!='183.82.126.214':
#         return json.dumps({"errors": ['You dont have access'], "data": {"status": '0'}})
#     district_id = request.values.get('district_id')
#     constituency_id = request.values.get('constituency_id')
#     group_no = request.values.get('group_no')
#     if district_id is None or district_id == '':
#         return json.dumps({"errors": ['param district_id missing'], "data": {"status": '0'}})
#     if constituency_id is None or constituency_id == '':
#         return json.dumps({"errors": ['param constituency_id missing'], "data": {"status": '0'}})
#     if group_no is None or group_no == '':
#         return json.dumps({"errors": ['param group_no missing'], "data": {"status": '0'}})

#     con = psycopg2.connect(config.DB_CONNECTION)
#     cur = con.cursor(cursor_factory=RealDictCursor)
#     try:
#         cur.execute("update whatsapp_groups set status='T' where district_id=%s and constituency_id=%s and group_no=%s",(district_id,constituency_id,group_no))
#         con.commit()
#         con.close()
#         return json.dumps({'errors': [], 'data': {'status': '1'}})

#     except Exception as e:
#         print str(e)
        
#         return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})


# @app.route("/whatsapp_groups_members", methods=["GET"])
# @crossdomain(origin="*", headers="Content-Type")
# def whatsappgroupmembers():
#     try:
        
       
#         district_id = request.values.get('district_id')
#         constituency_id = request.values.get('constituency_id')
#         if district_id is None or district_id == '':
#             return json.dumps({"errors": ['param district_id missing'], "data": {"status": '0'}})
#         if constituency_id is None or constituency_id == '':
#             return json.dumps({"errors": ['param constituency_id missing'], "data": {"status": '0'}})
#         group_no = request.values.get('group_no')
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         if group_no is None:
#             whatsappgroups(district_id,constituency_id)
#             cur.execute(
#                 "select group_name,group_no,status from whatsapp_groups where status='F' and district_id=%s and constituency_id=%s   ",
#                 (district_id, constituency_id))
#             rows = cur.fetchall()
#             return json.dumps({'errors': [], 'data': {'status': '1', 'groups': rows}})


#         else:
#             limit = 256

#             offset = (int(group_no) - 1) * limit

#             cur.execute(
#                 """select * from (select distinct on (m.phone)m.membership_id,m.phone,ac.constituency_id,m.polling_station_id,ac.district_id,ac.district_name,ac.constituency_name,m.create_dttm from janasena_membership
#                 m join assembly_constituencies ac on m.state_id=ac.state_id and m.constituency_id=ac.constituency_id where ac.district_id=%s and ac.constituency_id=%s order by m.phone) mb order by create_dttm limit %s offset %s """,
#                 (district_id, constituency_id, limit, offset))
#             rows = cur.fetchall()

#             con.close()

#             # for row in rows:
#             #     row['phone'] = "+"+encrypt_decrypt(row['phone'], 'D')
#             #     del row['create_dttm']
#             rows=[{
#                 "polling_station_id": 190,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP47636551",
#                 "phone": "+919440430422",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP49960602",
#                 "phone": "+919502082823",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP36770890",
#                 "phone": "+919866007878",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP44749064",
#                 "phone": "+919640120557",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP15591671",
#                 "phone": "+918096358523",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP37738057",
#                 "phone": "+919704018105",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP15985242",
#                 "phone": "+917285944045",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP21587109",
#                 "phone": "+919791644916",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP24271245",
#                 "phone": "+917799004459",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 72,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP15418623",
#                 "phone": "+919533342089",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             },
#             {
#                 "polling_station_id": 0,
#                 "constituency_id": 90,
#                 "district_id": 5,
#                 "membership_id": "JSP02112706",
#                 "phone": "+917673962109",
#                 "constituency_name": "Repalle",
#                 "district_name": "Guntur"
#             }]

#             return json.dumps({'errors': [], 'data': {'status': '1', 'children': rows}})

#     except Exception as e:
#         print str(e)
#         return json.dumps({"errors": ['details not Found'], "data": {"status": '0'}})

@app.route('/sendsmstovolunteers', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sendsmstovolunteers():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        
        cur.execute(
            "select id,phone from active_volunteers where status is null  ")
        rows = cur.fetchall()

        for row in rows:
            try:
                # name=str(row['name'])
                mobile = row['phone']
                

                msgToSend = "Thanks for your interest to be a volunteer. You are invited to come to JSP office Hyderabad on Saturday 09/06/18 between 10AM and 04PM for a Introductory session"

                print msgToSend

                sendSMS(mobile, msgToSend)
            except Exception as e:
                continue

        return "sent successfully"

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)

@app.route('/sendpulseapi', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sendpulseapi():
    SPApiProxy = PySendPulse(config.REST_API_ID, config.REST_API_SECRET, config.TOKEN_STORAGE)
    
    print "Get amount of websites"
    print SPApiProxy.push_count_websites()

    print "Get list of variables for website"
    print SPApiProxy.push_get_variables(32530)

    print "Get list of subscriptions for website"
    print SPApiProxy.push_get_subscriptions(32530)
    print "Get amount of subscriptions for website"
    print SPApiProxy.push_count_subscriptions(32530)
    return "true"

#     # Activate/Deactivate subscriber, state=1 - activate, state=2 - deactivate
#     SPApiProxy.push_set_subscription_state(SUBSCRIBER_ID, STATE)

#     # Create new push task
#     SPApiProxy.push_create('Hello!', WEBSITE_ID, 'This is my first push message', '10', {'filter_lang':'en', 'filter': '{"variable_name":"some","operator":"or","conditions":[{"condition":"likewith","value":"a"},{"condition":"notequal","value":"b"}]}'})



@app.route('/missedcalls_checking', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def missedcalls_checking():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        for i in range(1,400000,50000):

            cur.execute("select mi.phone,mi.supporter_id from janasena_missedcalls mi left join janasena_membership m on m.membership_id=mi.supporter_id where m.membership_id is null order by mi.create_dttm limit 50000 offset 1 ")
            rows=cur.fetchall()
            print len(rows)
            count=0
            for row in rows:
                
                phone=row['phone']
                supporter_id=row['supporter_id']
                if len(str(phone))==12:
                    countries_allowed = ["91"]
                    if phone.startswith(tuple(countries_allowed)): 
                        mobile=phone[2:12]
                        
                        if check_existing(mobile):
                            count=count+1
                            print count
                            cur.execute("update janasena_missedcalls set search_status='s' where supporter_id=%s",(supporter_id,))
                            con.commit()
        con.close()
        return 'success'
    except (psycopg2.Error,Exception) as e:
        print str(e)
        return str(e)

# import pymysql as MySQLdb
# def check_existing(phone):
#     try:
        
#         db = MySQLdb.connect("localhost","root","root","zdata18" )
        
#         cursor = db.cursor()
        
#         cursor.execute("select pincode from zdata where mobile=%s",(phone,))
#         result = cursor.fetchone()
        
#         if result:
#             return True
#         else:
#             return False

#     except Exception as e:
#         print str(e)
#         return False
from utilities.sms import send_bulk_sms_same_message
#import pymysql as MySQLdb
@app.route('/testsmstakeapi', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def testsmstakeapi():
    message="Greetings from Janasena! For Society that protects customs and culture. Pls give a missed call on 9394022222 and become pride a member of JSP"
    db = MySQLdb.connect("localhost","root","root","zdata18" )
        
    cursor = db.cursor()
    cursor.execute("select mobile from zdata  where pincode='522309' limit 100")
    rows=cursor.fetchall() 
    contacts=[]
    for row in rows:
        contacts.append(str(row[0]))

   
    print contacts 
    send_bulk_sms_same_message(message,contacts)
    return "success"

@app.route('/missedcalls_mapping', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def missedcalls_mapping():
    from utilities.others import missedcalls_mapping_task
    if config.ENVIRONMENT_VARIABLE!='local':
        #donations_check_task.delay()

        missedcalls_mapping_task.delay()
    else:
        missedcalls_mapping_task()


    return "statrted"


@app.route('/getMembershipWithVolunteerSMS', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@requires_auth
def getMembershipWithVolunteerSMS():
    mobile = request.values.get('mobilenumber')
    message_body = request.values.get('message')
    print mobile,message_body
    if mobile is None or mobile == "":
        return json.dumps({'errors': ['mobile missing'], 'data': {'status': '0'}})
    if message_body is None or message_body == "":
        return json.dumps({'errors': ['message_body missing'], 'data': {'status': '0'}})

    try:
        countries_allowed = ["91"]
        if len(str(mobile)) == 12 and mobile.startswith(tuple(countries_allowed)):

                voter_id = urllib.unquote(message_body).decode('utf8')
                if len(str(voter_id))<10 or len(str(voter_id))>14:
                    return json.dumps({'errors': ['you have entered invalid voterid.please check your voterid and enter again '], 'data': {'status': '0'}})
                membership_instance = Membership_details()

                e_voter_id=encrypt_decrypt(str(voter_id).lower(),'D')
                if membership_instance.check_voterid_existence(voter_id):
                    emobile=encrypt_decrypt(mobile,'E')
                    if membership_instance.check_phonenumber_count(emobile):
                        voterdetails_instance = Voter_details()

                        record = voterdetails_instance.get_voter_details(e_voter_id)

                        if record is not None:

                            name = record['voter_name']

                            phone = mobile
                            if record['state'] == 1:
                                state = 'AP'
                            else:
                                state = 'TS'

                            status, membership_id = generate_membershipid(name, phone, str(voter_id).lower(), record['relation_name'],
                                                                          record['relation_type'], record['house_no'], '',
                                                                          record['age'], record['sex'], record['part_no'],
                                                                          record['ac_no'], state, '',
                                                                          record['section_name'], '', 'WB')
                            if status:
                                return json.dumps({"errors": [],
                                                   'data': {'status': '1'}})

                            return json.dumps(
                                {"errors": ['something went wrong,please try again'], 'data': {'status': '0'}})


                        return json.dumps({"errors": ['no record found with the given details'],
                                           'data': {'status': '0'}})


                    return json.dumps(
                        {'errors': ['limit with this Phone Number already reached'], 'data': {'status': '3'}})

                return json.dumps({'errors': ['someone already Registered with the given VoterID'], 'data': {'status': '2'}})

        else:
            return json.dumps({'errors': ['supporting indian numbers only'], 'data': {'status': '2'}})

    except Exception as e:
        print str(e)
        return json.dumps({'errors': ['Exception occured'], 'data': {'status': '0'}})


import pymysql as MySQLdb
import pymysql.cursors
from celery_tasks.sms_email import send_bulk_sms_operations
@app.route('/sendSMStogeneralPublic', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def sendSMStogeneralPublic():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone as mobile from missed")
        rows=cur.fetchall()
        
                
        message = "Greetings from Janasena! To get your membership card fill the details in the below link  https://janasenaparty.org/membership"
                
        send_bulk_sms_operations(rows, message, [], [])
            
        return "success"
        

    except Exception as e:
        print str(e)
        return False

import os
import csv
import codecs
@app.route('/writemysqltopostgres', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def writemysqltopostgres():
    try:
        path="E:/voters excel files/DATA/andh_ac004.csv"
        # db = MySQLdb.connect(host="localhost",user="root",password="root",db="bdata" )
            
        # cursor = db.cursor()
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        
        copy_sql = """
           COPY voters_v3 FROM stdin WITH CSV HEADER
           DELIMITER as ','
           """
        # for fil in os.listdir(path):
        #   e_file=path+"/"+fil
        with open(path, 'r') as f:
            #fx = codecs.EncodedFile(f,"U", "UTF8")
            f = open(path, 'r')
            #cur.copy_expert(sql=copy_sql, file=f)
            cur.copy_from(f, 'voters_v3', sep=',')
            con.commit()
            f.close()
            
        print path +" completed"
        return "success"
        

    except Exception as e:
        print str(e)
        return False
          
@app.route('/convertmysqltopostgres', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def convertmysqltopostgres():
    try:
        path="e:/general_public/"
        db = MySQLdb.connect(host="localhost",user="root",password="root",db="zdata18" )
            
        cursor = db.cursor()
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select district_name from districts where state_id=2 order by seq_id")
        rows=cur.fetchall()
        count=0
        for row in rows:
            district=row['district_name']
            dist_path=path+district
            if not os.path.exists(dist_path):
                try:
                    os.makedirs(dist_path)
                except OSError as exc: # Guard against race condition
                        print str(exc)
                        raise
            cur.execute("select constituency_id from assembly_constituencies where district_name=%s order by constituency_id",(district,))
            crows=cur.fetchall()
            for crow in crows:
                constituency_id=crow['constituency_id']
                print constituency_id
                constituency_file=dist_path+"/"+str(constituency_id)+".csv"
                if not os.path.exists(os.path.dirname(dist_path)):
                    try:
                        os.makedirs(os.path.dirname(constituency_file))
                    except OSError as exc: # Guard against race condition
                            print str(exc)
                            raise

                cursor.execute("select z.mobile,z.cname as name,z.address,z.pincode,p.state,p.constituency_id from zdata z left join pincodes p on p.pincode=z.pincode where p.state=%s and p.constituency_id=%s ",(1,constituency_id))
                
                mysql_rows = cursor.fetchall()
                
                column_names = [i[0] for i in cursor.description]
                fp = open(constituency_file ,'wb')
                myFile = csv.writer(fp, lineterminator = '\r\n',delimiter = ',',dialect='excel') 
                myFile.writerow(column_names)
                myFile.writerows(list(mysql_rows))
                fp.close()
                count=count+1
                print str(count)+" completed"

        return "success"
        

    except Exception as e:
        print str(e)
        return False

@app.route("/decrypt",methods=["GET"])
def decrypt():

    message=request.values.get('message')
    converted=encrypt_decrypt(message,'E')
    return converted
@app.route("/nriconnectdata",methods=["GET"])
def nriconnectdata():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select name,mobile,email,city,country,profession from nriconnect where country!='India'")
        rows=cur.fetchall()
        
        filepath, file_size = writeDictToCSVFIle(rows)

        return_file = open(filepath, 'r')

        datetime_string = time.strftime("%Y%m%d%H%M%S")
        file_basename = 'NRI_memberships' + str(datetime_string) + ".csv"

        response = make_response(return_file.read(), 200)

        response.headers['Content-Description'] = 'File Transfer'
        response.headers['Cache-Control'] = 'no-cache'
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=%s' % file_basename
        response.headers['Content-Length'] = file_size
        return response

    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})
@app.route("/nrimemberships",methods=["GET"])
def nrimemberships():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select m.name,m.phone,m.email,m.age,m.gender,m.country,ac.district_name,ac.constituency_name from janasena_membership m join assembly_constituencies ac on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id where country!='india'")
        rows=cur.fetchall()
        for row in rows:
            row['phone']=encrypt_decrypt(str(row['phone']),'D')
            if row['email'] is not None and row['email']!='':
                row['email']=encrypt_decrypt(str(row['email']),'D')
        print rows
        filepath, file_size = writeDictToCSVFIle(rows)

        return_file = open(filepath, 'r')

        datetime_string = time.strftime("%Y%m%d%H%M%S")
        file_basename = 'NRI_memberships' + str(datetime_string) + ".csv"

        response = make_response(return_file.read(), 200)

        response.headers['Content-Description'] = 'File Transfer'
        response.headers['Cache-Control'] = 'no-cache'
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=%s' % file_basename
        response.headers['Content-Length'] = file_size
        return response

    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})

@app.route("/api/membersdata", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def members_data():
    try:
        data_type = request.values.get('type')
        value = request.values.get('value')
        state = None
        page_no=request.values.get('page_no')
        if data_type == 'assembly':
            state = request.values.get('state')
        if data_type is None:
            return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
        if value is None:
            return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
        #membership_instance = Membership_details()
        #data=membership_instance.get_members_details(data_type=data_type,value=value,state=state,page_no=page_no)
        #saverequestdata(data_type,value,state,page_no)
        data=[{"phone":"919652262151","booth":"0"},{"phone":"918790079997","booth":"0"},{"phone":"919676881991","booth":"0"},{"phone":"918328468195","booth":"0"},{"phone":"917680076268","booth":"0"},{"phone":"919959951123","booth":"0"},{"phone":"919849191422","booth":"0"},{"phone":"919160311671","booth":"0"}]
        value=json.loads(value)
        for member in data:
            member['constituency_id']=value[0]
           # member['phone'] = encrypt_decrypt(member['phone'],'D')

        return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

    except (Exception, KeyError) as e:

        return json.dumps({"errors": [str(e)], "data": {"status": 0}})



def saverequestdata(data_type,value,state,page_no):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("insert into request_data(name,data_type,value,state,page_no) values(%s,%s,%s,%s,%s)",("membersdata",data_type,value,state,page_no))
        con.commit()
        con.close()

    except Exception as e:
        print str(e)




@app.route('/get_voters_count', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def get_voters_count():
                
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        request_type=request.values.get("type")
        if request_type is None or request_type=='':
            return json.dumps({"errors":["parameter type missing"],"status":"0"})
        if request_type=='state':
            state_id=request.values.get("state_id")
            if state_id is None or state_id=='':
                return json.dumps({"errors":["parameter state_id missing"],"status":"0"})

            cur.execute("select total_votes from state where id=%s",(state_id,))
            row=cur.fetchone()
            if row:
                total_votes=row['total_votes']
            else:
                total_votes='NA'

            
            con.close()
            return json.dumps({"errors":[],"status":"1","total_votes":total_votes})
        if request_type=='district':
            district_id=request.values.get("district_id")
            if district_id is None or district_id=='':
                return json.dumps({"errors":["parameter district_id missing"],"status":"0"})

            cur.execute("select total_votes from districts where seq_id=%s",(district_id,))
            row=cur.fetchone()
            if row:
                total_votes=row['total_votes']
            else:
                total_votes='NA'
            
            con.close()
            return json.dumps({"errors":[],"status":"1","total_votes":total_votes})
        if request_type=='constituency':
            state_id=request.values.get("state_id")
            constituency_id=request.values.get("constituency_id")
            if state_id is None or state_id=='':
                return json.dumps({"errors":["parameter state_id missing"],"status":"0"})


                
            if constituency_id is None or constituency_id=='':
                return json.dumps({"errors":["parameter constituency_id missing"],"status":"0"})

            cur.execute("select total_votes from assembly_constituencies where state_id=%s and constituency_id=%s",(state_id,constituency_id,))
            row=cur.fetchone()
            if row:
                total_votes=row['total_votes']
            else:
                total_votes='NA'
            
            con.close()
            return json.dumps({"errors":[],"status":"1","total_votes":total_votes})
        if request_type=='booth':
            state_id=request.values.get("state_id")
            constituency_id=request.values.get("constituency_id")
            booth_id=request.values.get("booth_id")
            if state_id is None or state_id=='':
                return json.dumps({"errors":["parameter state_id missing"],"status":"0"})
    
            if constituency_id is None or constituency_id=='':
                return json.dumps({"errors":["parameter constituency_id missing"],"status":"0"})
            if booth_id is None or booth_id=='':
                return json.dumps({"errors":["parameter booth_id missing"],"status":"0"})

            cur.execute("select total as total_votes from polling_station_details where state_id=%s and constituency_id=%s and polling_station_no=%s",(state_id,constituency_id,booth_id))
            row=cur.fetchone()
            if row:
                total_votes=row['total_votes']
            else:
                total_votes='NA'
            
            con.close()
            return json.dumps({"errors":[],"status":"1","total_votes":total_votes})
    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})


@app.route('/find_constituency', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def find_constituency():
                
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select district_name from districts where state_id<3")
        districts=cur.fetchall()
        dist=[]
        assembly=[]
        mdls=[]
        for district in districts:
            dist.append(district['district_name'])
        print dist
        cur.execute("select constituency_name from assembly_constituencies where state_id<3")
        constituencies=cur.fetchall()
        for constituency in constituencies:
            assembly.append(constituency['constituency_name'])
        print assembly
        cur.execute("select mandal_name from mandals where state_id<3")
        mandals=cur.fetchall()
        for mandal in mandals:
            mdls.append(mandal['mandal_name'])    
        print mdls

        cur.execute("select phone,address,pincode from mapped_members")
        rows=cur.fetchall()
        count=0
        for row in rows:
            for district in dist:
                print district
                if str(district).lower() in row['address'].lower():

                    cur.execute("update mapped_members set district=%s where phone=%s",(district,row['phone']))
                    con.commit()
                    break
            for constituency in assembly:
                if str(constituency).lower() in row['address'].lower():

                    cur.execute("update mapped_members set constituency=%s where phone=%s",(constituency,row['phone']))
                    con.commit()
                    break
            for mandal in mdls:
                if str(mandal).lower() in row['address'].lower():

                    cur.execute("update mapped_members set mandal=%s where phone=%s",(mandal,row['phone']))
                    con.commit()
                    break
            count=count+1
            print count
        return 'success'
    
    
    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})



# @app.route('/whatsapp_groups/<constituency>', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# def whatsapp_groups(constituency):
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         print constituency
#         if constituency is not None:
#             cur.execute("select * from whatsapp_groups_number where lower(constituency)=%s",(constituency,))
#             row=cur.fetchone()
#             con.close()
#             if row:
#                 phone=row['phone']

#                 return render_template("whatsapp_groups.html",constituency=constituency,phone=phone)
        
#         return render_template("invalid_details.html")

#     except Exception as e:
#         print str(e)
#         return render_template("invalid_details.html")

@app.route('/panchayats_data', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def panchayats_data():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        
       
        data={}
        cur.execute("select distinct(district) from grampanchayats")
        rows=cur.fetchall()
        for row in rows:
            district=row['district']
            data[district]=[]
            cur.execute("select distinct on (mandal)district,mandal from grampanchayats where district=%s",(row['district'],))
            mandal_rows=cur.fetchall()

            for m in mandal_rows:
                mand={}
                mandal=m['mandal']
                mand[mandal]=[]
                cur.execute("select * from  grampanchayats where district=%s and mandal=%s",(district,mandal))
                panchayats=cur.fetchall()
                mand[mandal].append(panchayats)
                data[district].append(mand)
        with open('panchayats.json', 'w') as outfile:
            json.dump(data, outfile)
     
        
        return "success"

    except Exception as e:
        print str(e)
        return "failed"

@app.route('/nri_data', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
def nri_data():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        
        data={}
        cur.execute("select * from nri_memberships")
        rows=cur.fetchall()
        count=0
        for row in rows:
            email=encrypt_decrypt(row['email'],'D')
            cur.execute("update nri_memberships set email=%s where email=%s",(email,row['email']))
            con.commit()
            count=count+1
            print count
        return "success"

    except Exception as e:
        print str(e)
        return "failed"
# import pandas as pd
# @app.route('/booths_copy', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# def booths_copy():
#     try:
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
        
#         import os
#         path="C:/Users/Srini1/Desktop/2014_booths/"
#         arr = os.listdir(path)
        
#         for filename in arr:
                        
#             constituency=filename.split('.')[0]
            

#             df = pd.read_csv(path+filename, usecols=['area','Polling_Station_No','Polling_Station'])
#             result = df.to_dict(orient='records')
            
#             for row in result:
#                 cur.execute("insert into polling_booths_2014(constituency_name,polling_station_id,polling_station_name,polling_station_address)values(%s,%s,%s,%s)",(constituency,int(row['Polling_Station_No']),row['Polling_Station'],row['area']))
#                 con.commit()
#             print constituency+"-" + str(len(result))
#         return 'success'
#     except Exception as e:
#         print str(e)
#         return 'failed'
from dbmodels import JanasenaMissedcalls2
@app.route("/jspMissCallCampaign2", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
#@requires_auth
def jspMissCallCampaign2():
    phone_number = request.values.get('mobilenumber')
    receivedon = request.values.get('receivedon')
    try:

        existing = db.session.query(JanasenaMissedcalls2.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == phone_number).first()

        if existing is None:
            newRecord = JanasenaMissedcalls2(phone=phone_number,
                                             member_through='M')

            db.session.add(newRecord)
            db.session.commit()
            
            jspMissedcallTask2(phone_number)


            return json.dumps({"errors": [], 'data': {'status': '1',"message":"inserted"}})


        return json.dumps({"errors": [], 'data': {'status': '1',"message":"already existed"}})

    except Exception as e:
        print str(e)

        return json.dumps({"errors": ['Exception'], 'data': {'status': '0'}})



from utilities.general import decode_hashid,create_hashid
@app.route('/get_hash_id', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_hash_id():
    seq_no=request.values.get('seq_no')
    return create_hashid(int(seq_no))


@app.route('/sendsmsvenue', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sendsmsvenue():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select constituency_id,contact_person,contact_phone from poc_calenders where constituency_id =77")
        rows=cur.fetchall()
        print len(rows)
        template="""మీకు జనసేన పార్టీ కార్యాలయం నుండి 25 క్యాలెండర్ లు వచ్చాయి.Mallikarjun (8712342363) గారి దగ్గర నుండి వీటిని తీసుకొని వివిధ షాపులలో అందరికి కనిపించే విధంగా ఏర్పాటు చెయ్యండి """
        count=0
        for row in rows:
            constituency_id=row['constituency_id']
            contact_name=row['contact_person']
            contact_phone=row['contact_phone']
           
            cur.execute("""select distinct on(phone)member,phone from cadre_Called_final where constituency_id=%s""",(constituency_id,))
            
            mandal_rows=cur.fetchall()
            msgToSend=template
            msgToSend=msgToSend.replace("{{contact_person}}",str(contact_name))
            msgToSend=msgToSend.replace("{{contact_phone}}",str(contact_phone))
            
            for mrow in mandal_rows:
                try:
                    name = str(mrow['member'])
                    mobile = mrow['phone']
                   
                    if len(mobile)==10:
                       
                        numberToSend = "91" + str(mobile)
                    else:
                        numberToSend=mobile
                    
                        
                   
                    print name, numberToSend
                    # print msgToSend
                        
                    sendSMS(numberToSend, msgToSend)
                    count=count+1
                    print count
                except Exception as e:
                    continue
        sendSMS("919676881991", msgToSend)
        print len(rows)
        return "sent successfully"

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)

from utilities.others import send_memebrship_update_msg
@app.route('/send_membership_updating_msg', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def send_membership_updating_msg():
    pc_id=request.values.get('pc_id')
    if config.ENVIRONMENT_VARIABLE=='local':
        send_memebrship_update_msg(pc_id)
    else:
        send_memebrship_update_msg.delay(pc_id)
    return "success"

@app.route('/sendsobserversmsg', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sendsobserversmsg():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("""select m.jsp_membership_seq_id,m.phone from janasena_membership m join assembly_constituencies ac
 on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id 
 where ac.state_id=1 and m.country='india' and m.polling_station_id=0 and ac.parliament_constituency_id=7 limit 10 """)
        rows=cur.fetchall()
       
        template="""మీ మండలంలో జనసేన కార్యక్రమాలలో యాక్టీవ్ గా పార్టిసిపేట్ చేయదలుచుకుంటే, ఈ లింక్ {{link}} ని క్లిక్ చేసి వివరాలు తెలియచేయండి"""
        count=0
        data=[]
        for row in rows:
            try:
                d={}
               
                mobile = encrypt_decrypt(row['phone'],'D')

                seq_id=create_hashid(row['jsp_membership_seq_id'])
                if len(mobile)==10:
                   
                    numberToSend = "91" + str(mobile)
                else:
                    numberToSend=mobile
                long_url="janasenaparty.org/jsp_membership_update?hash="+str(seq_id)
                url=shurl(long_url)
                url=url.replace("https://",'')
                msgToSend=template
              
                msgToSend=msgToSend.replace("{{link}}",str(url))
                
                count=count+1
                print count
                
                
                #sendSMS(numberToSend, msgToSend)
            except Exception as e:
                continue
        
        
        sendSMS("919676881991", msgToSend)
        return "sent successfully"

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return str(e)

@app.route("/m_active", methods=['POST', 'GET'])
@csrf.exempt
def m_active():
    return render_template("website/jsp_mandal_team-n.html")
@app.route("/jsp_mandal_team", methods=['POST', 'GET'])
@csrf.exempt
def jsp_mandal_team():
    if request.method == 'GET':
        try:
            hash = request.values.get('hash')
            if hash is None:
                return redirect(url_for('.m_active'))
            decoded = decode_hashid(hash)
            con = psycopg2.connect(config.DB_CONNECTION)
            with con.cursor(cursor_factory=RealDictCursor) as cur:
                cur.execute("""
                select * from
                mandal_wise_cadre
                where seq_id=%s
                """,(decoded))
                amap = cur.fetchone()
                print amap

                if amap is None:
                    return render_template('website/jsp_mandal_team.html')

                data = dict()
                data['pc_id'] = amap['parliament_constituency_id']
                data['constituency_id'] = amap['constituency_id']
                data['mandal_id'] = amap['mandal_id']
                data['name'] = amap['member']
                voter_id=amap['voter_id']
                if voter_id!='':

                    data['voter_id']=encrypt_decrypt(str(voter_id),'D')
                else:
                    data['voter_id']=''
                try:
                    data['age'] = int(amap['age'])
                except Exception as e:
                    data['age'] = amap['age']

                data['phone'] = amap['phone'][2:]
                data['hash'] = hash
                
                return render_template('website/jsp_mandal_team.html',data=data)
        except (psycopg2.Error,Exception) as e:
            print(e )
            return redirect(url_for('.m_active'))


    elif request.method == 'POST': 
        try:
            data = request.json
            print(data)
            if 'hash' not in data:
                return json.dumps({'status':0,"msg":"Please send data"})
            unhash = decode_hashid(data['hash'])
            if data is None:
                return json.dumps({'status':0,"msg":"Please send data"})
            con = psycopg2.connect(config.DB_CONNECTION)
            with con.cursor() as cur:

                print(unhash,"unhash",data['phone'])
                cur.execute("""
                    select * 
                    from mandal_wise_cadre
                    where phone=%s and seq_id=%s
                """,("91"+data['phone'],unhash[0]))
                not_user = cur.fetchone()
                print("notuser",not_user)
                if not_user is None:
                    return json.dumps({'status':2,"msg":"You are not authorized. Redirecting"}) #redirect(url_for('.m_active'))
                cur.execute("""
                    select * 
                    from jsp_mandal_cadre
                    where phone=%s
                """,(data['phone'],))
                phone_proxy = cur.fetchone()
                print(phone_proxy)
                if phone_proxy:
                    return json.dumps({'status':0,'msg':'User with the phone number already exists.'})
                cur.execute("""
                    insert into jsp_mandal_cadre(name,age,gender,voter_id,constituency_id,mandal_id,location,skills,parliament_constituency_id,phone,whats_app,work_location,prev_exp)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                """,(data['name'],data['age'],data['gender'],data['voter_id'],data['assembly'],data['mandal'],data['location'],
                     data['skills'],data['parliament'],data['phone'],data['whats_app'],data['work_location'],data['prev_exp']))
                con.commit()

                return json.dumps({'status':1,"msg":"Inserted Successfully"})
        except (psycopg2.Error,Exception) as e:
            print(e)
            return json.dumps({'status':0,'msg':'Something Wrong'})


@app.route('/volunteer', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def volunteer():
    if request.method=='GET':

        return render_template("website/volunteer.html")

    else:
        data=request.json
        for key in data.keys():
            if data[key] is None:
                error_str="parameter"+data[key]+"  missing"
                return json.dumps({"errors":[error_str],"status":"0"})
        try:
            name=data["name"]
            mobile=data["mobile"]
            email=data["email"]
            membership_id=data["membership_id"]
            district=data["district"]
            constituency=data["constituency"]
           
            volunteer_type=data["volunteer_type"]
            location=data["location"]
        except Exception as e:
            return json.dumps({"errors":["parameters missing"],"status":"0"})

        
            
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("select * from janasena_volunteers where mobile=%s",(mobile,))
            existing=cur.fetchone()
            if existing is None:

                cur.execute("insert into janasena_volunteers(name,mobile,email,membership_id,district,constituency_id,volunteer_type,location)values(%s,%s,%s,%s,%s,%s,%s,%s)",(name,mobile,email,membership_id,district,constituency,volunteer_type,location))
                con.commit()
                con.close()
                return json.dumps({"errors": [], 'status': '1',"message":"saved successfully"})
            return json.dumps({"errors": [], 'status': '2',"message":"someone already registered with this mobile number "})
        except (psycopg2.Error, Exception) as e:
            print str(e)
            return json.dumps({"errors": ['something wrong'], 'status': '0'})

@app.route('/jspworker2659.js', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sw():
    print(request.path[1:])
    return send_from_directory(app.static_folder, "jspwebsitemain/jspworker2659.js")




@app.route('/update_mandal_team', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def update_mandal_team():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone,voter_id from mandal_wise_cadre where polling_station_id!=0 and assembly='Amalapuram (SC)'")
        rows=cur.fetchall()
        for row in rows:
            voter_id=row['voter_id']
            polling_station_id=get_booth_id(voter_id)
            cur.execute("update mandal_wise_cadre set polling_station_id=%s where voter_id=%s",(polling_station_id,voter_id))
            con.commit()

        return 'done'

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return 'failed'
  

@app.route("/jsp_active_team", methods=['POST', 'GET'])

def jsp_active_team():
    if request.method == 'GET':
       
            
        return render_template('website/jsp_assembly_team.html')
    
    elif request.method == 'POST': 
        try:
            data = {}
            data['name']=request.values.get('name')
            data['village']=request.values.get('village')
            data['mandal']=request.values.get('mandal')
            data['age']=request.values.get('age')
            data['occupation']=request.values.get('occupation')
            data['phone']=request.values.get('phone')
            data['email']=request.values.get('email')
            data['assembly']=request.values.get('assembly')
            data['gender']=request.values.get('gender')
            data['caste']=request.values.get('caste')
            data['education']=request.values.get('education')
            data['parliament']=request.values.get('parliament')
            data['membership_id']=request.values.get('membership_id')
            data['voter_id']=request.values.get('voter_id')
            data['experience']=request.values.get('experience')
            data['skills']=json.dumps(request.values.get('skills'))
            
            con = psycopg2.connect(config.DB_CONNECTION)
            with con.cursor() as cur:

               
                cur.execute("""
                    select * 
                    from assembly_comittee
                    where phone=%s
                """,(data['phone'],))
                phone_proxy = cur.fetchone()
                print(phone_proxy)
                if phone_proxy:
                    return json.dumps({'status':0,'msg':'User with the phone number already exists.'})
                photo_url=''
                
                image_file = request.files['image']

                photo_url = upload_assembly_members_img(image_file, 'membershipimages')

                cur.execute("""
                    insert into assembly_comittee(name,village,mandal,age,gender,caste,education,occupation,
                        phone,email,assembly,parliament,membership_id,voter_id,experience,skills,photo_url)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                """,(data['name'],data['village'],data['mandal'],data['age'],data['gender'],data['caste'],data['education'],
                     data['occupation'],data['phone'],data['email'],data['assembly'],data['parliament'],data['membership_id'],data['voter_id'],data['experience'],data['skills'],photo_url))
                con.commit()


                return json.dumps({'status':1,"msg":"Inserted Successfully"})
        except (psycopg2.Error,Exception) as e:
            print(e)
            return json.dumps({'status':0,'msg':'Something Wrong'})





def upload_assembly_members_img(file, container):
    try:

        timestr = time.strftime("%Y%m%d-%H%M%S")
        filename = timestr
        fileextension = 'png'
        Randomfilename = id_generator()

        filename = str(Randomfilename) + str(filename) + '.' + fileextension

        attachment = ''
        try:
            block_blob_service.create_blob_from_stream(container, filename, file)
            attachment = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
        except Exception as e:
            print 'Exception=' + str(e)

        return attachment
    except Exception as e:
        print str(e)
        return "False"





def get_booth_id(voter_id):
    try:
        con = psycopg2.connect(config.DB_CONNECTION_test)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select part_no from voters where voter_id=%s",(voter_id,))
        v_row=cur.fetchone()
        if v_row is not None:
            return v_row['part_no']
        else:
            return 0


    except Exception as e:
        print str(e)

from pdf_gen import shitty_form
@app.route("/generate_memebr_pdf", methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD","OP"])
@csrf.exempt
def generate_memebr_pdf():

    if request.method=='GET':
        try:
            seq_id=request.values.get('seq_id')
            membership_id=request.values.get('membership_id')
            
            print seq_id,membership_id
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            
            cur.execute("""select cm.seq_id,cm.name,cm.phone,cm.age,cm.gender,
                ac.constituency_name as assembly,ac.parliament_constituency_name as parliament,mdl.mandal_name as mandal,
                cm.photo_url,cm.village,cm.skills,cm.education,cm.occupation,cm.membership_id,
                cm.voter_id,cm.village,cm.email,cm.caste,cm.experience from assembly_comittee cm 
                join assembly_constituencies ac on ac.constituency_id=cm.assembly
                join mandals mdl on mdl.id=cm.mandal
                 where ac.state_id=1 
                  and cm.membership_id in %s """,(tuple(['JSP13396447','JSP07447864','JSP04438724','JSP43736478',
 'JSP28311035','JSP29686096','JSP40807631','JSP3235677',
 'JSP21472242','JSP48228811','JSP25911224','JSP56474894',
 'JSP37463189','JSP36534127','JSP65877733','JSP10126813',
 'JSP58105190','JSP35531147','JSP61111873','JSP16606034',
 'JSP24831322','JSP00308095','JSP27041369','JSP36128682',
 'JSP57915007','JSP04367478','JSP23283363','JSP23375841',
 'JSP39682283','JSP50634670','JSP12045706','JSP19058663',
 'JSP08742090','JSP54293800','JSP59375392','JSP45240635',
 'JSP43569558','JSP09538585','JSP53256668','JSP58988390',
 'JSP47489013','JSP27694212','JSP23648930','JSP12079786',
 'JSP62690885','JSP66057058','JSP56666371','JSP32516309',
 'JSP57642090','JSP39126866','JSP65770859','JSP14598042',
 'JSP47507072','JSP31092810','JSP56555455','JSP59685748',
 'JSP45858420','JSP41964455','JSP54439495','JSP61165484',
 'JSP18145022','JSP40067706','JSP04792868','JSP14333572',
 'JSP16811029','JSP49826589','JSP28955077','JSP39543656',
 'JSP56826519','JSP19582526','JSP56331260']),))
            rows =cur.fetchall()
            for row in rows:
                skills=row['skills'].split(',')
                for key in row.keys():
                    if row[key] is None or row[key]=='':
                        row[key]=' '
                #pdf_file_name = row['name'].split(' ')[0] + '_' + str(row['phone']) + '_' + str(row['assembly'])+ ".pdf"
                pdf_co=shitty_form(name=row['name'],age=str(row['age']),caste=row['caste'] ,gender=row['gender'] ,qualification=row['education'] ,mobile_no=row['phone'] ,email=row['email'] ,job=row['occupation'] ,ac=row['assembly'] ,pc=row['parliament'] ,jsp_id=str(row['membership_id'] ),voter_id =str(row['voter_id'] ),exp=row['experience'],village=row['village'],mandal=row['mandal'],image_url=str(row['photo_url']),skills=skills)
                
            return json.dumps({"errors":[],"status":"1"})
            
            # else:
            #     return json.dumps({"errors":["details not found"],"status":"0"})
            

        except Exception as e:
            print str(e)
            return json.dumps({"errors":["something wrong"],"status":"0"})


@app.route("/jsp_aspirants", methods=['POST', 'GET'])
@csrf.exempt
def jsp_aspirants():
    if request.method == 'GET':
       
            
        return render_template('website/jsp_aspirants.html')
    
    elif request.method == 'POST': 
        try:
            data = {}
            data['aspirant_type']=request.values.get('aspirant_type')
            data['location']=request.values.get('aspirant_location')
            data['name']=request.values.get('name')
            data['relation_name']=request.values.get('relation_name')
            data['dob']=request.values.get('dob')
            data['birth_place']=request.values.get('birth_place')
            data['education']=request.values.get('education')
            data['school_and_place']=request.values.get('school_and_place')
            data['higher_education']=request.values.get('higher_education')
            data['phone']=request.values.get('phone')
            data['email']=request.values.get('email')
            data['membership_id']=request.values.get('membership_id')
            data['occupation']=request.values.get('occupation')
            data['present_address']=request.values.get('present_address')
            data['permenant_address']=request.values.get('permenant_address')
            data['gender']=request.values.get('gender')
            data['relation_with_jsp']=request.values.get('relation_with_jsp')
            data['issues_in_assembly']=request.values.get('issues_in_assembly')
           
            data['prev_party']=request.values.get('prev_party')
            data['role']=request.values.get('role')
            data['duration']=request.values.get('duration')
            data['relation_with_other_leaders']=request.values.get('relation_with_other_leaders')
            data['achievements']=request.values.get('achievements')
            data['criminal_cases']=request.values.get('criminal_cases')
            data['remarks']=request.values.get('remarks')
            data['further_process']=request.values.get('further_process')
            aspirant_locations=request.values.get('aspirant_locations')
            locations={}
            locations['locations']=str(aspirant_locations).split(",")
            data['aspirant_locations']=json.dumps(locations)
            
            con = psycopg2.connect(config.DB_CONNECTION)
            with con.cursor() as cur:

               
                cur.execute("""
                    select * 
                    from aspirants
                    where lower(membership_id)=%s
                """,(data['membership_id'].lower(),))
                phone_proxy = cur.fetchone()
                
                if phone_proxy:
                    if request.files:
                        image_file = request.files['image']

                        data['photo_url'] = upload_assembly_members_img(image_file, 'membershipimages')
                    else:
                        data['photo_url']=request.values.get('photo_url')
                        if data['photo_url']=='https://janasenaparty.org/static/img/default-avatar.png':
                            
                            data['photo_url']=''
                    
                    cur.execute("""update aspirants set aspirant_type=%s,aspirant_locations=%s,
                            name=%s,relation_name=%s,dob=%s,birth_place=%s,education=%s,
                            school_and_place=%s,higher_education=%s,occupation=%s,current_address=%s,
                            permenant_address=%s,mobile=%s,email=%s,photo_url=%s,
                            relationship_with_janasena=%s,
                            issues_in_assembly=%s,prev_exp_party=%s,role=%s,duration=%s,
                            relationship_with_other_party_leaders=%s,achievements=%s,
                            criminal_cases=%s,remarks=%s,further_process=%s,gender=%s where membership_id=%s """,(data['aspirant_type'],data['aspirant_locations'],
                        data['name'],data['relation_name'],data['dob'],
                        data['birth_place'],data['education'],
                         data['school_and_place'],data['higher_education'],
                         data['occupation'],
                         data['present_address'],data['permenant_address'],
                         data['phone'],data['email'],data['photo_url'],
                         data['relation_with_jsp'],
                         data['issues_in_assembly'],data['prev_party'],
                          data['role'],data['duration'],data['relation_with_other_leaders'],
                         data['achievements'],data['criminal_cases'],
                          data['remarks'],data['further_process'],data['gender'],data['membership_id'],))
                    con.commit()

                else:

                    if request.files:
                        image_file = request.files['image']

                        data['photo_url'] = upload_assembly_members_img(image_file, 'membershipimages')
                    else:
                        data['photo_url']=request.values.get('photo_url')
                        if data['photo_url']=='https://janasenaparty.org/static/img/default-avatar.png':
                            
                            data['photo_url']=''
                    cur.execute("""
                        insert into aspirants(aspirant_locations,
                            name,relation_name,dob,birth_place,education,
                            school_and_place,higher_education,occupation,current_address,
                            permenant_address,mobile,email,photo_url,
                            membership_id,relationship_with_janasena,
                            issues_in_assembly,prev_exp_party,role,duration,
                            relationship_with_other_party_leaders,achievements,
                            criminal_cases,remarks,further_process,gender)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                            %s,%s,%s,%s,%s,%s)
                    """,(data['aspirant_locations'],
                        data['name'],data['relation_name'],data['dob'],
                        data['birth_place'],data['education'],
                         data['school_and_place'],data['higher_education'],
                         data['occupation'],
                         data['present_address'],data['permenant_address'],
                         data['phone'],data['email'],data['photo_url'],
                         data['membership_id'],data['relation_with_jsp'],
                         data['issues_in_assembly'],data['prev_party'],
                          data['role'],data['duration'],data['relation_with_other_leaders'],
                         data['achievements'],data['criminal_cases'],
                          data['remarks'],data['further_process'],data['gender']
                         ))
                    con.commit()


                return json.dumps({'status':1,"msg":"Inserted Successfully"})
        except (psycopg2.Error,Exception) as e:
            print(str(e))
            return json.dumps({'status':0,'errors':['Something Wrong']})

@app.route("/delete_candidate", methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def delete_candidate():
    name=request.values.get('name')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute('delete from jsp_candidates where name=%s',(name,))
        con.commit()
    return 'success'

@app.route("/jsp_aspirants_pdfs", methods=['POST', 'GET'])

def jsp_aspirants_pdf_by_id():
    if request.method == 'GET':
        parliament='candidates'
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            # cur.execute(
            #       "select constituency_name from assembly_constituencies where parliament_constituency_name =%s",
            #       (parliament,))
            # assemblies = cur.fetchall()
            # asm=[]
            # for item in assemblies:
            #   asm.append(item['constituency_name'] + ' assembly')
            # asm.append(parliament + ' parliament')
            # asms = ', '.join("'{0}'".format(w) for w in asm)
               
            #cur.execute("select * from aspirants where  (aspirant_locations -> 'locations')::jsonb ?| array[" + asms + "] order by seq_id ")
            cur.execute("select * from aspirants where mobile in (select phone from jsp_candidates)")
            rows = cur.fetchall()
            for row in rows:
                row['create_date']=str(row['create_date']).split(" ")[0]
                row['locations']=','.join(row['aspirant_locations']['locations'])
                
                get_aspirant_pdf_receipt(row,parliament)
                
                
            return 'response' 
                
           


@app.route('/fetch_aspirant_membership_details', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
def fetch_aspirant_membership_details():
    try:
        membership_id=request.values.get("membership_id")
        if membership_id is None:
            return json.dumps({'status':0,'errors':['membership id missing']})
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("select  * from aspirants where lower(membership_id)=%s",(str(membership_id).lower(),))
            row=cur.fetchone()
            
            if row is not None:
                details=row
                
                
                details['create_date']=str(details['create_date']).split(" ")[0]
                if details['photo_url']=='' or details['photo_url'] is None:
                    details['img_url']='https://janasenaparty.org/static/img/default-avatar.png'
                else:
                    details['img_url']=details['photo_url']
                return json.dumps({'status':1,'data':details})

            else:


                membership_instance = Membership_details()
                membership_id=str(membership_id).upper()
                details = membership_instance.get_membership_details(membership_id, None)
                d = {}
                if details is not None:
                    
                    details['relation_name']=details['relationship_name']
                    
                    if len(details['phone'])==12:
                        details['mobile']=details['phone'][2:12]
                    if details['email'] is not None and details['email']!='':
                        details['email']=encrypt_decrypt(details['email'],'D')
                    
                        
                    return json.dumps({'status':1,'data':details})
                return json.dumps({'status':0,'errors':['details not found']})       
        

    except Exception as e:
        print str(e)
        return json.dumps({'status':0,'msg':'Something Wrong'})
        
            
@app.route("/jsp_candidates", methods=[ 'GET'])

def jsp_candidates():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)              
        cur = con.cursor(cursor_factory=RealDictCursor)
    
        cur.execute("select   from jsp_candidates order by seq_id ")
        rows=cur.fetchall()
        con.close()

        return render_template("website/jsp_candidates.html",candidates=rows)
    except (psycopg2.Error,Exception) as e:
        print str(e)
        return render_template("website/jsp_candidates.html",candidates=[])
                    
@app.route("/add_jsp_candidates", methods=[ 'GET','POST'])
@csrf.exempt
def add_jsp_candidates():
    if request.method=='GET':
        return render_template("website/jsp_candidates_add.html")


    try:
        name=request.values.get('name')
        location_type=request.values.get('location_type')
        location=request.values.get('location')
        phone=request.values.get('phone')
        district=request.values.get('district')
        print name,location_type,location,phone,district
        img_url=''
        if request.files:
            image_file = request.files['image']
            img_url=upload_assembly_members_img(image_file, 'membershipimages')

            
        con = psycopg2.connect(config.DB_CONNECTION)              
        cur = con.cursor(cursor_factory=RealDictCursor)
    
        cur.execute("insert into jsp_candidates(name,location,location_type,district,phone,img_url)values(%s,%s,%s,%s,%s,%s)",(name,location,location_type,district,phone,img_url))
        con.commit()
        
        con.close()

        return json.dumps({"status":"1","msg":"Added Successfully"})
    except (psycopg2.Error,Exception) as e:
        print str(e)
        return json.dumps({"status":"0","msg":"something wrong"})
        



@app.route("/get_local_bodies_info",methods=['GET','POST'])
@csrf.exempt
def get_local_bodies_info():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)              
        cur = con.cursor(cursor_factory=RealDictCursor)
        mode=request.values.get("mode")
        if mode=='districts':
            cur.execute("select id,district_name from ts_new_districts order by id")
            rows=cur.fetchall()
            con.close()
            return json.dumps({"errors":[],"status":"1","data":rows})
        elif mode=='mandals':
            district_id=request.values.get('district_id')
            if district_id is None:
                return json.dumps({"errors":["district_id missing"],"status":"0"})
            cur.execute("select district_id,id as mandal_id,mandal_name from ts_new_mandals where district_id=%s order by district_id,id;",(district_id,))
            rows=cur.fetchall()
            con.close()
            return json.dumps({"errors":[],"status":"1","data":rows})
        elif mode=='mptc':
            district_id=request.values.get('district_id')
            if district_id is None:
                return json.dumps({"errors":["district_id missing"],"status":"0"})
            mandal_id=request.values.get('mandal_id')
            if mandal_id is None:
                return json.dumps({"errors":["mandal_id missing"],"status":"0"})
            cur.execute("select district_id,mandal_id,id as mptc_id,mptc_name from ts_new_mptc where district_id=%s and mandal_id=%s  order by district_id,mandal_id,id;",(district_id,mandal_id))
            rows=cur.fetchall()
            con.close()
            return json.dumps({"errors":[],"status":"1","data":rows})
        elif mode=='grampanchayats':
            district_id=request.values.get('district_id')
            if district_id is None:
                return json.dumps({"errors":["district_id missing"],"status":"0"})
            mandal_id=request.values.get('mandal_id')
            if mandal_id is None:
                return json.dumps({"errors":["mandal_id missing"],"status":"0"})
            cur.execute("select district_id,mandal_id,id as panchayat_id,panchayat_name from ts_new_grampanchayats where district_id=%s and mandal_id=%s order by district_id,mandal_id,id;",(district_id,mandal_id))
            rows=cur.fetchall()
            con.close()
            return json.dumps({"errors":[],"status":"1","data":rows})
        elif mode=='booths':
            district_id=request.values.get('district_id')
            if district_id is None:
                return json.dumps({"errors":["district_id missing"],"status":"0"})
            mandal_id=request.values.get('mandal_id')
            if mandal_id is None:
                return json.dumps({"errors":["mandal_id missing"],"status":"0"})
            panchayat_id=request.values.get('panchayat_id')
            if panchayat_id is None:
                return json.dumps({"errors":["panchayat_id missing"],"status":"0"})
            cur.execute("select district_id,mandal_id,panchayat_id,booth_id,booth_name from ts_new_polling_booths where district_id=%s and mandal_id=%s and panchayat_id=%s order by district_id,mandal_id,panchayat_id,booth_id;",(district_id,mandal_id,panchayat_id))
            rows=cur.fetchall()
            con.close()
            return json.dumps({"errors":[],"status":"1","data":rows})
        else:
            con.close()
            return json.dumps({"errors":[],"status":"1","data":[]})


    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})
#### visitors Data ################################
@app.route("/meet_your_president",methods=['GET','POST'])
@app.route("/meetyourpresident",methods=['GET','POST'])
@app.route("/meetourpresident",methods=['GET','POST'])
@csrf.exempt
def meetyourpresident():
    if request.method=='GET':
        return render_template("website/meetourpresident.html")

    try:
        con = psycopg2.connect(config.DB_CONNECTION)              
        cur = con.cursor(cursor_factory=RealDictCursor)
        visitor_location=request.values.get('visitor_location')
        whom_to_meet=request.values.get('whom_to_meet')
        #visiting_date=request.values.get('visiting_date')
        #visiting_time=request.values.get('visiting_time')
        state=request.values.get('state')
        district=request.values.get('district')
        constituency=request.values.get('constituency')
        mandal=request.values.get('mandal')
        visitor_name=request.values.get('visitor_name')
        visitor_age=request.values.get('visitor_age')
        visitor_gender=request.values.get('visitor_gender')
        visitor_phone=request.values.get('visitor_phone')
        visitor_whatsapp=request.values.get('visitor_whatsapp')
        visitor_address=request.values.get('visitor_address')
        
        purpose_of_meet=request.values.get('purpose_of_meet')
        notes_or_issue=request.values.get('notes_or_issue')
        type_of_visit=request.values.get('type_of_visit')
        refer_status=request.values.get('refer_status')

        if visitor_location is None or visitor_location=='':
            return json.dumps({"errors":[" parameter visiting location should not be empty"],"status":"0"})
        if whom_to_meet is None or whom_to_meet=='':
            return json.dumps({"errors":[" parameter whom_to_meet should not be empty"],"status":"0"})
        if state is None or state=='':
            return json.dumps({"errors":[" parameter state should not be empty"],"status":"0"})
        if district is None or district=='':
            return json.dumps({"errors":[" parameter district should not be empty"],"status":"0"})
        if constituency is None or constituency=='':
            return json.dumps({"errors":[" parameter constituency should not be empty"],"status":"0"})
        if mandal is None or mandal=='':
            return json.dumps({"errors":[" parameter mandal should not be empty"],"status":"0"})
        if visitor_name is None or visitor_name=='':
            return json.dumps({"errors":[" parameter visitor_name should not be empty"],"status":"0"})
        if visitor_age is None or visitor_age=='':
            return json.dumps({"errors":[" parameter visitor_age should not be empty"],"status":"0"})
        if visitor_gender is None or visitor_gender=='':
            return json.dumps({"errors":[" parameter visitor_gender should not be empty"],"status":"0"})
        if visitor_phone is None or visitor_phone=='':
            return json.dumps({"errors":[" parameter visitor_phone should not be empty"],"status":"0"})
        if visitor_whatsapp is None or visitor_whatsapp=='':
            return json.dumps({"errors":[" parameter visitor_whatsapp should not be empty"],"status":"0"})
        if visitor_address is None or visitor_address=='':
            return json.dumps({"errors":[" parameter visitor_address should not be empty"],"status":"0"})
        if purpose_of_meet is None or purpose_of_meet=='':
            return json.dumps({"errors":[" parameter purpose_of_meet should not be empty"],"status":"0"})
        if notes_or_issue is None or notes_or_issue=='':
            return json.dumps({"errors":[" parameter notes_or_issue should not be empty"],"status":"0"})
        if type_of_visit is None or type_of_visit=='':
            return json.dumps({"errors":[" parameter type_of_visit should not be empty"],"status":"0"})
        visitor_membership_status=request.values.get('membership_status')
        if visitor_membership_status is None or visitor_membership_status=='':
            return json.dumps({"errors":[" parameter membership_status should not be empty"],"status":"0"})
        if visitor_membership_status=='yes':
            visitor_membership_id=request.values.get('membership_id')
            
            if visitor_membership_id is None or visitor_membership_id=='':
                return json.dumps({"errors":[" parameter membership_id should not be empty"],"status":"0"})
            
        else:
            visitor_membership_id=''
        print 'reference details'    
        if refer_status=='yes':
            referency_by_name=request.values.get('referency_by_name')
            reference_phone=request.values.get('reference_phone')
            if referency_by_name is None or referency_by_name=='':
                return json.dumps({"errors":[" parameter referency_by_name should not be empty"],"status":"0"})
            if reference_phone is None or reference_phone=='':
                return json.dumps({"errors":[" parameter reference_phone should not be empty"],"status":"0"})
        else:
            referency_by_name=''
            reference_phone=''

        if request.files:
            visitor_photo = request.files['visitor_image']
            photo_url=upload_image_web(visitor_photo,'meetyourpresident')
            print photo_url
        else:
            return json.dumps({"errors":[" parameter visitor_photo missing"],"status":"0"})
        print 'data inserting'
        cur.execute("""insert into visitors_data(place_of_visit,
          whom_to_meet,
          state,district,
          constituency,
          mandal,
          visitor_name,
        visitor_phone,
        visitor_photo,
        visitor_gender,
        visitor_age,
        visitor_whatsapp,
        visitor_address,
        visitor_membership_id,
        visitor_membership_status,
        purpose_of_meeting,
        notes_or_issue_des,
        type_of_visiting ,
         reference_status,
        reference_name,
        reference_phone)values(%s,%s,%s,%s,%s,
        %s,%s,%s,%s,%s,
        %s,%s,%s,%s,%s,
        %s,%s,%s,%s,%s,%s) returning visitor_id """,
         (visitor_location,whom_to_meet,state,district,
         constituency,mandal,
         visitor_name ,visitor_phone,      
                photo_url,visitor_gender,visitor_age,
                visitor_whatsapp,visitor_address,
                visitor_membership_id,visitor_membership_status,
                purpose_of_meet,notes_or_issue,
        type_of_visit,refer_status,referency_by_name,reference_phone))
        visitor_id=cur.fetchone()['visitor_id']
        con.commit()
        support_documents_count=request.values.get('support_documents_count')
        
        if support_documents_count>=1:
            for i in range(0,int(support_documents_count)):
                file_name="support_document_"+str(i)
                try:
                
                    support_file=request.files[file_name]
                    if config.ENVIRONMENT_VARIABLE!='local':
                        upload_mrpresident_file.delay(support_file.read(), support_file.filename, support_file.name, support_file.content_length, support_file.content_type, support_file.headers,visitor_id)
                    else:
                        upload_mrpresident_file.delay(support_file.read(), support_file.filename, support_file.name, support_file.content_length, support_file.content_type, support_file.headers,visitor_id)
                except Exception as e:
                    print str(e)
                    print "No support documents "
            print 'done'

        if type_of_visit!='single':
            supporters_data=json.loads(request.values.get('supporters_data'))
            for supporter in supporters_data:
                if request.files[supporter['photo_name']]:
                    supporter_photo=request.files[supporter['photo_name']]
                    if config.ENVIRONMENT_VARIABLE!='local':
                        upload_supporters_data.delay(supporter_photo.read(), supporter_photo.filename, supporter_photo.name, supporter_photo.content_length, supporter_photo.content_type, supporter_photo.headers,visitor_id,supporter)
                    else:
                        upload_supporters_data(supporter_photo.read(), supporter_photo.filename, supporter_photo.name, supporter_photo.content_length, supporter_photo.content_type, supporter_photo.headers,visitor_id,supporter)

        today=datetime.now().strftime("%Y/%m/%d")
        visitor_format_number="JSP/V/"+str(today)+"-"+str(visitor_id)+"/R0"
        print visitor_format_number
        return json.dumps({"errors":[],"status":"1","data":{"visitor_id":visitor_format_number}})    
    except (psycopg2.Error,Exception) as e:
        print str(e)
        return json.dumps({"errors":["something wrong"],"status":"0"})

# @app.route("/test_remainder",methods=["GET"])
# def test_remainder():
#     recurring_donations_remainder()
#     return "success"

    
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 8000))
    app.run(host='0.0.0.0', port=port, debug=True)

