from sqlalchemy import create_engine, MetaData, Table

# MySQL database connection
source_engine = create_engine('mysql+pymysql://root:root@localhost:3306/zdata18')
source_connection = source_engine.connect()

# Read the entire data
def read_data():
    ''' reads all the data and returns it row by row to save memory'''
    data = source_connection.execute('SELECT * FROM voters')
    batch_counter = 0
    batch_of_rows = []
    for row in data:
        batch_of_rows.append(row)
        batch_counter = batch_counter + 1
        # set this to be the batch size that optimizes your code for memory and time of execution.
        if batch_counter == 5000: 
            batch_counter = 0
            yield batch_of_rows

# close the MySQL connection
source_connection.close()

# function to transform data
def transform(data):

    def process_row(row):
    	row['area']=row['area'].replace(",","!")
    
	return row

    # process and return the incoming dataset as a list of dicts
    processed_data = [dict(zip(data.keys(), process_row(d))) for d in data]
    return processed_data


# Postgres database connection
dest_connection = create_engine('postgresql://postgres:root@localhost:5432/janasena_test')
dest_meta = MetaData(bind=dest_connection, reflect=True, schema='janasena_test')

table = Table('voters', dest_meta, autoload=True)
for data_row in read_data():
    transformed_data = transform(data)
    dest_connection.execute(table.insert().values(transformed_data))

dest_connection.close()