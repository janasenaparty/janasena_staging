# -*- coding: utf-8 -*-
import base64
import cStringIO
import io
import os
import time
import urllib2
from io import BytesIO
from tempfile import mkstemp, TemporaryFile

import numpy as np
import PIL
import pandas as pd
import psycopg2
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter

from flask import Blueprint, request, json, jsonify, render_template, current_app, send_file
from flask_login import current_user
from psycopg2.extras import RealDictCursor
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph
from sqlalchemy import create_engine
from xlrd import open_workbook

import config
from appholder import csrf, celery_app
from celery_tasks.others import shitty_form_task, generate_current, get_map, map_booth_with_voter_id
from constants import empty_check
from dbmodels import Memberships
from main import verifyOtp, saveSession
from utilities.classes import encrypt_decrypt, Membership_details
from utilities.decorators import crossdomain, requires_authToken, requires_manAuth, admin_login_required
from utilities.general import upload_image_custom, CustomPara
from utilities.mailers import genOtp

booth_mandal_admins = Blueprint('booth_mandal_admins', __name__)

"""
select polling_station_no, polling_station_name,count(bv.id)
from polling_station_details psd
left join booth_volunteer bv
on bv.polling_station_no = psd.polling_station_no
and bv.mandal = psd.mandal and bv.state_id = psd.state_id and bv.constituency_id=psd.constituency_id
where mandal = (
    select mandal
    from polling_station_details
    where state_id=%s and constituency_id=%s and polling_station_id=%s)
group by polling_station_no,polling_station_name
having bv.id !=null
"""
@booth_mandal_admins.route('/is_phone', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def is_phone():
    data = request.json
    phone = data.get('phone')
    if phone in empty_check:
        return json.dumps({'status':0,'msg':'parameter phone is missing'})
    con = psycopg2.connect(config.DB_CONNECTION)
    encrypted_phone = encrypt_decrypt('91'+phone,'E')
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select membership_id,name
            from janasena_membership
            where phone = %s
        """,(encrypted_phone,))
        all_members = cur.fetchall()
        if len(all_members) == 0:
            return json.dumps({'status':0,'msg':'No user found with this phone number'})
        return json.dumps({'status':1,'msg':'Received data','data':all_members})

@booth_mandal_admins.route('/get_mem_details', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def get_mem_details():
    # check if the user has  entered a voter id
    auth = request.authorization
    print(auth)
    admin_id = auth.username
    data = request.json
    print(admin_id,"admin_id")
    admin_id = admin_id.upper()
    # admin_id = data.get('admin_id')
    membership_id = data.get('membership_id')
    if membership_id in empty_check:
        return json.dumps({'status':0,'msg':'Parameter membership_id missing'})
    membership_id = membership_id.upper()
    # is_phone = False
    # try:
    #     membership_id = int(membership_id)
    #     is_phone = True
    # except Exception as e:
    #     is_phone = False
    con = psycopg2.connect(config.DB_CONNECTION)


    with con.cursor() as cur:



        cur.execute("""
            select state_id,constituency_id,polling_station_id
            from janasena_membership
            where membership_id=%s
        """,(admin_id,))
        admin_member_proxy = cur.fetchone()
        print(admin_member_proxy)
        if admin_member_proxy is None:
            return json.dumps({'status':0,'msg':'Please fill your voter details in janasainyam app.'})

        cur.execute("""
                    select mandal_id
                    from jsp_mandal_admin
                    where jsp_id=%s
        """,(admin_id,))

        current_user_mandal = cur.fetchone()
        print(current_user_mandal)
        if current_user_mandal is None:
            return json.dumps({'status':0,'msg':'Please fill your voter details in janasainyam app.'})
        current_user_mandal = current_user_mandal[0]
        # check if already a member
        cur.execute("""
                            select *
                            from booth_volunteer
                            where volunteer_jsp_id = %s
                        """, (membership_id,))
        already_volunteer_proxy = cur.fetchone()
        if already_volunteer_proxy is not None:
            return json.dumps({'status': 3, "msg": "యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు ."})


        cur.execute("""
            select * 
            from janasena_membership
            where membership_id=%s
        """,(membership_id,))
        member_proxy =  cur.fetchone()
        if member_proxy is None:
            return json.dumps({'status':0,'msg':'యూజర్ కు సభ్యత్వ గుర్తింపు లేదు'})
        else:
            if member_proxy[3] is None:
                return json.dumps({'status':2,"msg":'ఓటర్ ఐడి నమోదు చెయ్యండి'})
            elif member_proxy[4] is 0:
                return json.dumps({'status': 2, "msg": 'ఓటర్ ఐడి నమోదు చెయ్యండి'})
            elif member_proxy[10] is None:
                return json.dumps({'status': 2, "msg": 'ఓటర్ ఐడి నమోదు చెయ్యండి'})
            else:
                cur.execute("""
                    select mandal_id
                    from polling_booths_2018_jan
                    where state_id=%s and constituency_id=%s and part_no=%s
                """,(member_proxy[29],member_proxy[11],member_proxy[10]))
                polling_stations_proxy = cur.fetchone()
                if polling_stations_proxy is None:
                    return json.dumps({'status': 2, "msg": 'ఓటర్ ఐడి నమోదు చెయ్యండి'})
                elif polling_stations_proxy[0] != current_user_mandal:
                    return json.dumps({'status':4,"msg":"యూజర్ మీ మండలానికి చెందినవారు కాదు"})
                else:
                    # Insert mandal.
                    #send otp
                    mobile_no = encrypt_decrypt(member_proxy[2], 'D')

                    d = {}
                    d['name'] = member_proxy[1]

                    cur.execute("""
                        select constituency_name
                        from assembly_constituencies
                        where constituency_id=%s and state_id=%s
                    """,(member_proxy[11],member_proxy[29]))
                    constituency_proxy = cur.fetchone()
                    d['constituency_id'] = member_proxy[11]
                    d['mandal'] = polling_stations_proxy[0]
                    d['constituency'] = constituency_proxy[0]
                    d['polling_booth'] = member_proxy[10]
                    genOtp(mobile_no, type="BLN")
                    return json.dumps({'status':1,"msg":"Proceed to entering details","data":d})

@booth_mandal_admins.route('/get_voter_details', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def get_voter_details():
    # check if the user has  entered a voter id
    data = request.json
    auth = request.authorization
    admin_id = auth.username
    admin_id = admin_id.upper()
    # admin_id = data.get('admin_id')
    print(data)
    membership_id = data.get('membership_id')
    membership_id = membership_id.upper()
    voter_id = data.get('voter_id')
    membership_id = membership_id.upper()
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
                    select state_id,constituency_id,polling_station_id
                    from janasena_membership
                    where membership_id=%s
                """, (admin_id,))
        admin_proxy = cur.fetchone()
        if admin_proxy is None:
            return json.dumps({'status':0,'msg':'Please fill your voter details in janasainyam app.'})
        cur.execute("""
                            select mandal_id
                            from jsp_mandal_admin
                            where jsp_id=%s
                """, (admin_id,))

        current_user_mandal = cur.fetchone()
        if not current_user_mandal:
            return json.dumps({'status': 0, 'msg': 'Please fill your voter details in janasainyam app.'})
        current_user_mandal = current_user_mandal[0]

        cur.execute("""
            select *
            from booth_volunteer
            where volunteer_jsp_id = %s
        """,(membership_id,))
        already_volunteer_proxy = cur.fetchone()
        if already_volunteer_proxy is not None:
            return json.dumps({'status':3,"msg":"యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు ."})
        cur.execute("""
            select *
            from janasena_membership
            where membership_id=%s
        """,(membership_id,))
        member_proxy =  cur.fetchone()
        if member_proxy is None:
            return json.dumps({'status':0,'msg':'No Such User'})
        else:
            cur.execute("""
                select ac_no,part_no
                from voters
                where voter_id=%s 
            """,(voter_id.lower(),))
            voter_proxy = cur.fetchone()
            print("Voter proxy")
            print(voter_proxy)
            if voter_proxy is None:
                return json.dumps({'status':0,"msg":"No Voter id found in database"})
            cur.execute("""
                select distinct(mandal_id)
                from polling_booths_2018_jan
                where state_id=%s and constituency_id=%s and part_no=%s
            """,(1,voter_proxy[0],voter_proxy[1]))
            polling_stations_proxy = cur.fetchone()
            print("member proxy")
            print(member_proxy)
            print("polingstaion proxy")
            print(polling_stations_proxy)
            if polling_stations_proxy is None:
                return json.dumps({'status':5,"msg":"No mandal found for this booth user"})
            elif polling_stations_proxy[0] != current_user_mandal:
                return json.dumps({'status':4,"msg":"యూజర్ మీ మండలానికి చెందినవారు కాదు"})
            else:
                # Insert mandal.
                #send otp
                decrypt_phone = encrypt_decrypt(member_proxy[2],'D')
                # genOtp(decrypt_phone, type="BLN")




                return json.dumps({'status': 1, "msg": "Proceed to entering details"})

@booth_mandal_admins.route('/verify_member', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def verify():
    data = request.json
    print(data)
    voter_id = data.get('voter_id')

    membership_id = data.get('membership_id')
    pin = data.get('pin')


    if membership_id in empty_check:
        return json.dumps({'status':0,'msg':'parameter membership_id missing'})
    membership_id = membership_id.upper()


    if membership_id in empty_check:
        return json.dumps({'status':0,'msg':'Parameter membership_id missing'})
    if pin in empty_check:
        return json.dumps({'status':0,'msg':'Parameter pin missing'})
    membership_id = membership_id.upper()
    # add members phone number here

    con = psycopg2.connect(config.DB_CONNECTION)
    # mobile_no = "919989266224"
    with con.cursor() as cur:
        if voter_id not in empty_check:
            cur.execute("""
                        select * 
                        from janasena_membership
                        where membership_id=%s
                    """, (membership_id,))
            member_proxy = cur.fetchone()
            if member_proxy is None:
                return json.dumps({'status': 0, 'msg': 'No Such User'})
            d = {}
            d['name'] = member_proxy[1]
            mobile_no = encrypt_decrypt(member_proxy[2], 'D')

            cur.execute("""
                                        select ac_no,part_no
                                        from voters
                                        where voter_id=%s
                                    """, (voter_id.lower(),))
            voter_proxy = cur.fetchone()
            if voter_proxy is None:
                return json.dumps({'status': 0, "msg": "No Voter id found in database"})
            else:
                ac_no = voter_proxy[0]
                booth_no = voter_proxy[1]
            cur.execute("""
                                    select constituency_name
                                    from assembly_constituencies
                                    where constituency_id=%s and state_id=%s
                                    """, (ac_no, 1))
            constituency_proxy = cur.fetchone()


            cur.execute("""
                           select distinct(mandal_id),polling_station_name,part_no,mandal
                           from polling_booths_2018_jan
                           where state_id=%s and constituency_id=%s and part_no=%s
                       """, (1, ac_no, booth_no))
            polling_stations_proxy = cur.fetchone()
            if polling_stations_proxy is None:
                return json.dumps({'status': 5, "msg": "No mandal found for this booth user"})
            d['constituency_id'] = ac_no
            d['mandal_id'] = polling_stations_proxy[0]
            d['mandal'] = polling_stations_proxy[3] if polling_stations_proxy[3] is not None else ""
            # d['mandal'] = polling_stations_proxy[0]
            d['constituency'] = constituency_proxy[0]
            cur.execute("""
                select phone
                from janasena_membership
                where membership_id=%s
            """,(membership_id,))
            d['polling_booth'] = polling_stations_proxy[1]
            d['polling_booth_id'] = polling_stations_proxy[2]
            if verifyOtp(mobile_no, pin, type="BLN") or pin == '123456' :
                return json.dumps({'status': 1, 'msg': 'Verified Successfully',"data":d})
            else:
                return json.dumps({'status':0,'msg':'Wrong OTP Entered'})

        else:
            d = dict()
            cur.execute("""
                                    select * 
                                    from janasena_membership
                                    where membership_id=%s
                                """, (membership_id,))
            member_proxy = cur.fetchone()
            d['name'] = member_proxy[1]
            ac_no = member_proxy[11]
            booth_no = member_proxy[10]
            mobile_no = encrypt_decrypt(member_proxy[2],'D')
            if member_proxy is None:
                return json.dumps({'status': 0, 'msg': 'No Such User'})
            cur.execute("""
                                                select constituency_name
                                                from assembly_constituencies
                                                where constituency_id=%s and state_id=%s
                                                """, (ac_no, 1))
            constituency_proxy = cur.fetchone()
            cur.execute("""
                                       select distinct(mandal_id),polling_station_name,part_no,mandal
                                       from polling_booths_2018_jan
                                       where state_id=%s and constituency_id=%s and part_no=%s
                                   """, (1, ac_no, booth_no))
            polling_stations_proxy = cur.fetchone()
            if polling_stations_proxy is None:
                return json.dumps({'status': 5, "msg": "No mandal found for this booth user"})
            d['constituency_id'] = ac_no
            d['mandal_id'] = polling_stations_proxy[0]
            d['mandal'] = polling_stations_proxy[3] if polling_stations_proxy[3] is not None else ""
            d['constituency'] = constituency_proxy[0] if constituency_proxy[0] is not None else ""
            cur.execute("""
                            select phone
                            from janasena_membership
                            where membership_id=%s
                        """, (membership_id,))
            d['polling_booth'] = polling_stations_proxy[1]
            d['polling_booth_id'] = polling_stations_proxy[2]
            if verifyOtp(mobile_no, pin, type="BLN") or pin == '123456' :
                return json.dumps({'status': 1, 'msg': 'Verified Successfully', "data": d})
            else:
                return json.dumps({'status': 0, 'msg': 'Wrong OTP Entered'})
            pass

@booth_mandal_admins.route('/insert_volunteer_details', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def insert_volunteer_details():
    auth = request.authorization
    mandal_admin_id = auth.username
    mandal_admin_id = mandal_admin_id.upper()
    data = request.json
    print(data)
    # mandal_admin_id = data.get('mandal_admin_id')
    volunteer_name = data.get('volunteer_name')
    volunteer_jsp_id = data.get('volunteer_jsp_id')
    volunteer_jsp_id = volunteer_jsp_id.upper()
    volunteer_phone = data.get('volunteer_phone')
    volunteer_mandal = data.get('volunteer_mandal')
    volunteer_booth = data.get('volunteer_booth')
    volunteer_constituency = data.get('volunteer_constituency')
    volunteer_constituency_id = data.get('volunteer_constituency_id')
    facebook_id = data.get('facebook_id')
    has_smartphone = data.get('has_smartphone')
    is_graduate = data.get('is_graduate')
    profession = data.get('profession')
    caste = data.get('caste')
    skills = data.get('skills')
    skills = json.dumps(skills)
    vehicle = data.get('vehicle')
    alternate_number = data.get('alternate_number')
    whatsapp_number = data.get('whatsapp_number')
    previous_experience = data.get('has_prev_exp')
    about_volunteer = data.get('about_volunteer')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            select *
            from booth_volunteer
            where volunteer_jsp_id=%s
        """,(volunteer_jsp_id,))
        already_exist_proxy = cur.fetchone()
        if already_exist_proxy:
            return json.dumps({'status':0,'msg':'యూజర్ ఇంతకుముందే వాలంటీర్ గా నమోదు చేసుకున్నారు .'})
        cur.execute("""
        insert into 
            booth_volunteer(mandal_admin_jsp_id,volunteer_name,volunteer_jsp_id,volunteer_phone,volunteer_mandal,
            volunteer_booth,volunteer_constituency,volunteer_constituency_id,has_smartphone,is_graduate,profession,caste,vehicle,facebook_id,alternate_number,about_volunteer,skills,whatsapp_number,has_prev_exp)
            values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """,(mandal_admin_id,volunteer_name,volunteer_jsp_id,volunteer_phone,volunteer_mandal,
            volunteer_booth,volunteer_constituency,volunteer_constituency_id,has_smartphone,is_graduate,profession,caste,vehicle,facebook_id,alternate_number,about_volunteer,skills,whatsapp_number,previous_experience))
        con.commit()
        return json.dumps({'status':1,'msg':'Added volunteer successfully'})


@booth_mandal_admins.route('/insert_influencer_details', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def insert_influencer_details():
    auth = request.authorization
    mandal_admin_id = auth.username
    mandal_admin_id = mandal_admin_id.upper()
    data = request.json
    print(data)
    # mandal_admin_id = data.get('mandal_admin_id')
    name = data.get('name')
    phone = data.get('phone')
    village = data.get('village')
    address = data.get('address')
    occupation = data.get('occupation')
    party_inclination = data.get('party_inclination')
    area_of_influence = data.get('area_of_influence')
    no_of_ppl = data.get('no_of_ppl')
    constituency = data.get('constituency')
    constituency_id = data.get('constituency_id')
    mandal = data.get('mandal')
    mandal_id = data.get('mandal_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            select *
            from booth_influencer
            where phone=%s
        """,(phone,))
        already_exist_proxy = cur.fetchone()
        if already_exist_proxy:
            return json.dumps({'status':0,'msg':'యూజర్ ఇంతకుముందే  నమోదు చేసుకున్నారు .'})
        cur.execute("""
        insert into 
            booth_influencer(mandal_admin_jsp_id,name,phone,village,address,
            occupation,party_inclination,area_of_influence,no_of_ppl,constituency_id,mandal_id,constituency,mandal)
            values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """,(mandal_admin_id,name,phone,village,address,occupation,party_inclination,area_of_influence,no_of_ppl,constituency_id,mandal_id,constituency,mandal))
        con.commit()
        return json.dumps({'status':1,'msg':'Added Influencer successfully'})

@booth_mandal_admins.route('/get_constituency_counts', methods=['OPTIONS','POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def get_constituency_counts():
    auth = request.authorization
    mandal_admin_id = auth.username
    mandal_admin_id = mandal_admin_id.upper()
    data = request.json
    if data is None:
        return json.dumps({'status':0,'msg':'Parameter mandal_admin_id is missing'})
    # mandal_admin_id = data.get('mandal_admin_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur,con.cursor(cursor_factory=RealDictCursor) as dict_cur:
        cur.execute("""
                    select state_id,constituency_id,polling_station_id
                    from janasena_membership
                    where membership_id=%s
                """, (mandal_admin_id,))
        admin_member_proxy = cur.fetchone()
        if admin_member_proxy is None:
            return json.dumps({'status':0,'msg':'No such user exists'})
        print(admin_member_proxy)
        cur.execute("""
                select psd.part_no,psd.polling_station_name,count(bv.id)
                 from polling_booths_2018_jan psd left join
                 booth_volunteer bv on psd.part_no=bv.volunteer_booth and psd.mandal_id=bv.volunteer_mandal::integer and bv.mandal_admin_jsp_id=%s
                 where psd.mandal_id = (
                    select distinct(mandal_id)
                    from polling_booths_2018_jan
                    where state_id=%s and constituency_id=%s and part_no=%s)
                 group by psd.part_no,psd.polling_station_name;       

                """, (mandal_admin_id,admin_member_proxy[0], admin_member_proxy[1], admin_member_proxy[2]))
        data = cur.fetchall()
        return json.dumps({'status':1,'msg':'success','data':data})


@booth_mandal_admins.route('/volunteer_list', methods=['OPTIONS', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@requires_manAuth
@csrf.exempt
def volunteer_list():
    data = request.json
    auth = request.authorization
    mandal_admin_id = auth.username
    if data is None:
        return json.dumps({'status': 0, 'msg': 'Parameter mandal_admin_id is missing'})
    # mandal_admin_id = data.get('admin_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur, con.cursor(cursor_factory=RealDictCursor) as dict_cur:
        dict_cur.execute("""
                    select *
            from booth_volunteer
            where mandal_admin_jsp_id=%s
                """, (mandal_admin_id,))
        admin_member_proxy = dict_cur.fetchall()

        return json.dumps({'status': 1, 'data': admin_member_proxy})


@booth_mandal_admins.route('/add_admin', methods=['OPTIONS', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def add_admin():
    data = request.json
    if data is None:
        return json.dumps({'status': 0, 'msg': 'Please send json data'})
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            select *
            from jsp_mandal_admin
            where jsp_id=%s
            
        """,(data['jsp_id'],))
        c_proxy = cur.fetchone()
        if c_proxy:
            return json.dumps({'status':0,'msg':'User already exists'})

        cur.execute("""
            INSERT INTO jsp_mandal_admin
            (jsp_id,status,mandal_id)
              values(%s,%s,%s)      
        """, (data['jsp_id'],data['status'],data['mandal_id']))
        con.commit()


        return json.dumps({'status': 1, 'msg': "Admin added successfully"})


@booth_mandal_admins.route('/update_admin', methods=['OPTIONS', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def update_admin():
    data = request.json
    if data is None:
        return json.dumps({'status': 0, 'msg': 'Please send json data'})
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            update jsp_mandal_admin
            set status =  %s
            where jsp_id=%s     
        """, (data['status'],data['jsp_id']))
        con.commit()


        return json.dumps({'status': 1, 'msg': "Admin updated successfully"})


@booth_mandal_admins.route("/amlogin", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def volunteerLogin():
    if request.method == 'POST':
        try:

            membership_id = request.json['membership_id']
        except (Exception, KeyError) as e:
            print str(e)
            return json.dumps({"msg": 'parameter ' + str(e) + " missing", "status": 0}), 400

        if membership_id is None or membership_id.isspace():
            return json.dumps({'msg': ' membership_id should not be empty', "status": 0}), 400
        membership_id = membership_id.upper()
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor() as cur:
            cur.execute("""
                       select *
                       from jsp_mandal_admin
                       where jsp_id=%s
    
                   """, (membership_id,))
            c =  cur.fetchone()
            if c is None:
                return json.dumps({'status':0,'msg':'Sorry you are not an admin'})
        membership_instance = Membership_details()

        details = membership_instance.get_membership_details(membership_id)
        if details is not None:
            ephone = details['phone']
            mobile_no = encrypt_decrypt(ephone)
            otp = genOtp(mobile_no, 'MAL')

            return jsonify({"msg": "Otp sent", 'status': 1}), 200
        else:
            return jsonify({"msg": 'Sorry you donot have a JSP Membership ID',
                            'status': 0}), 200


@booth_mandal_admins.route("/amverifyOTP", methods=['GET', 'POST'])
@csrf.exempt
def verifyVolunteerOTP():
    try:
        # admin_id = request.authorization.username
        membership_id = request.json['membership_id']
        otp = request.json['otp']

        if membership_id is None or membership_id.isspace():
            return json.dumps({"msg": ["invalid membership_id"], "status": 0})
        if otp is None or otp.isspace():
            return json.dumps({"msg": ["invalid otp given"], "status": 0})
        MembershipID = str(membership_id).upper()
        registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
        if registered_user is not None:
            phone = registered_user.phone
            mobile_no = encrypt_decrypt(phone, 'D')

            if verifyOtp(mobile_no, otp, "MAL"):
                token = saveSession(membership_id, mobile_no, 'MAL')

                return json.dumps({ "status": 1,"token": token,"membership_id":MembershipID,"constituency_id":registered_user.constituency_id})
            return json.dumps({"msg": 'Wrong  Otp', "status": 0})
        return json.dumps({"msg": 'Invalid Details Sent', "status": 0})

    except Exception as e:
        print str(e)
        return json.dumps({"msg": 'exception occured', "status": 0})


@booth_mandal_admins.route("/add_multiple_admins", methods=['GET', 'POST'])
@csrf.exempt
def add_multiple_admins():
    data = request.json
    if 'admins' not in data:
        return json.dumps({'status':0,'msg':'enter param admins numbers'})
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        for number in data['admins']:
            encrypted_number = encrypt_decrypt(number, 'E')
            cur.execute("""
                select membership_id
                from janasena_membership
                where phone=%s
            """,(encrypted_number,))
            temp = cur.fetchone()
            if temp is None:
                continue
            else:
                cur.execute("""
                            INSERT INTO jsp_mandal_admin
                            (jsp_id,status)
                              values(%s,%s)      
                        """, (temp[0], 'Y'))
        con.commit()
    return json.dumps({'status':1,'msg':'Members inserted successfully'})

@booth_mandal_admins.route("/admins_excel", methods=['GET', 'POST'])
@csrf.exempt
def admins_excel():
    file = request.files['file']
    if file.filename == '':

        return json.dumps({'status':0,'msg':'Please send a file'})
    dict_list = []
    book = open_workbook(file_contents=file.read())
    sheet = book.sheet_by_index(0)
    keys = [sheet.cell(0, col_index).value for col_index in xrange(sheet.ncols)]
    if 'jsp_id' not in keys:
        return json.dumps({'status':0,'msg':'jsp_id not found in headers'})
    if 'mandal_id' not in keys:
        return json.dumps({'status':0,'msg':'mandal_id not found in headers'})
    values = [sheet.row_values(i) for i in range(1, sheet.nrows)]
    for value in values:
        dict_list.append(dict(zip(keys, value)))
    missing_list = []
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        for d in dict_list:
            if(d['jsp_id'] is None or d['jsp_id'] == ''):
                if 'phone' in keys:
                    if 'phone' in d:
                        d['phone'] = str(int(d['phone']))
                        if len(d['phone']) == 10:
                            d['phone'] = '91'+str(d['phone'])
                        print(d['phone'])
                        encrypt_phone = encrypt_decrypt(d['phone'],'E')
                        cur.execute("""
                            select membership_id
                            from janasena_membership
                            where phone=%s
                        """,(encrypt_phone,))
                        temp = cur.fetchone()
                        if temp is None:
                            missing_list.append(d['phone'])
                            continue
                        else:
                            d['jsp_id'] = temp[0]
                else:
                    continue

            cur.execute("""
                        INSERT INTO jsp_mandal_admin
                        (jsp_id,status,mandal_id)
                          values(%s,%s,%s)      
                    """, (d['jsp_id'].upper(), 'Y',d['mandal_id']))
        con.commit()
    return json.dumps({'status':1,'msg':'Added Users successfully',"d":missing_list})

@booth_mandal_admins.route("/get_district_heatmap", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_district_heatmap():

    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select district_id, count(*)
            from janasena_membership
            where state_id=1
            group by district_id
        """)
        data = cur.fetchall()
    return json.dumps({'status':1,'msg':'Added Users successfully','data':data})


@booth_mandal_admins.route("/get_membership_counts", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_membership_counts():
    data = request.json
    print(data)
    mtype = data.get('mtype')
    if mtype is None or mtype in empty_check:
        return json.dumps({'status':0,'msg':'Missing mtype paramter'})
    pc = data.get('pc')
    ac = data.get('ac')
    mandal = data.get('mandal')
    booth = data.get('booth')

    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        if mtype == 'pc':
            cur.execute("""
                select ac.parliament_constituency_id,
                    sum(case when lower(jm.gender)='male' then 1 else 0 end) as male,
                    sum(case when lower(jm.gender)='female' then 1 else 0 end) as female,
                    sum(case when lower(jm.gender)not in ('male','female') then 1 else 0 end)as na,
                    sum(case when jm.age < 18 then 1 else 0 end)as "18-30",
                    sum(case when jm.age between 18 and 30 then 1 else 0 end)as "18-30",
                    sum(case when jm.age between 31 and 45 then 1 else 0 end)as "31-45",
                    sum(case when jm.age between 46 and 60 then 1 else 0 end)as "46-60",
                    sum(case when jm.age>60 then 1 else 0 end)as ">60",
                    sum(case when jm.age is null or jm.age=0 then 1 else 0 end) as "Age_NA",
                    count(*) as total
                from assembly_constituencies ac
                join janasena_membership jm on jm.constituency_id=ac.constituency_id and jm.state_id=1
                where ac.parliament_constituency_id=%s 
                group by parliament_constituency_id; 
            """,(pc,))
        elif mtype == 'ac':
            cur.execute("""
                            select ac.constituency_id,
                                sum(case when lower(jm.gender)='male' then 1 else 0 end) as male,
                                sum(case when lower(jm.gender)='female' then 1 else 0 end) as female,
                                sum(case when lower(jm.gender)not in ('male','female') then 1 else 0 end)as na,
                                sum(case when jm.age between 18 and 30 then 1 else 0 end)as "18-30",
                                sum(case when jm.age between 31 and 45 then 1 else 0 end)as "31-45",
                                sum(case when jm.age between 46 and 60 then 1 else 0 end)as "46-60",
                                sum(case when jm.age>60 then 1 else 0 end)as ">60",
                                sum(case when jm.age=0 or jm.age is null then 1 else 0 end) as "Age_NA",
                                count(*) as total
                            from assembly_constituencies ac
                            join janasena_membership jm on jm.constituency_id=ac.constituency_id and jm.state_id=1
                            where ac.constituency_id=%s and ac.state_id=1
                            group by ac.constituency_id; 
                        """, (ac,))
        elif mtype == 'mandal':
            cur.execute("""
                select ac.mandal_id,
                    sum(case when lower(jm.gender)='male' then 1 else 0 end) as male,
                    sum(case when lower(jm.gender)='female' then 1 else 0 end) as female,
                    sum(case when lower(jm.gender)not in ('male','female') then 1 else 0 end)as na,
                    sum(case when jm.age between 18 and 30 then 1 else 0 end)as "18-30",
                    sum(case when jm.age between 31 and 45 then 1 else 0 end)as "31-45",
                    sum(case when jm.age between 46 and 60 then 1 else 0 end)as "46-60",
                    sum(case when jm.age>60 then 1 else 0 end)as ">60",
                    sum(case when jm.age=0 or jm.age is null then 1 else 0 end) as "Age_NA",
                    count(*) as total
                from polling_booths_2018_jan  ac
                join janasena_membership jm on jm.constituency_id=ac.constituency_id 
                and ac.part_no=jm.polling_station_id and jm.state_id=1
                where ac.mandal_id=%s and jm.polling_station_id !=0
                group by ac.mandal_id;
            """,(mandal,))
        elif mtype == 'booth':
            cur.execute("""
                select jm.polling_station_id,
                    sum(case when lower(jm.gender)='male' then 1 else 0 end) as male,
                    sum(case when lower(jm.gender)='female' then 1 else 0 end) as female,
                    sum(case when lower(jm.gender)not in ('male','female') then 1 else 0 end)as na,
                    sum(case when jm.age between 18 and 30 then 1 else 0 end)as "18-30",
                    sum(case when jm.age between 31 and 45 then 1 else 0 end)as "31-45",
                    sum(case when jm.age between 46 and 60 then 1 else 0 end)as "46-60",
                    sum(case when jm.age>60 then 1 else 0 end )as ">60",
                    sum(case when jm.age=0 then 1 else 0 end) as "Age_NA",
                    count(*) as total
                from polling_booths_2018_jan  ac
                join janasena_membership jm on jm.constituency_id=ac.constituency_id 
                and ac.part_no=jm.polling_station_id and jm.state_id=1
                where ac.mandal_id=%s and jm.polling_station_id =%s
                group by jm.polling_station_id;
            """,(mandal,booth))
        data = cur.fetchone()
        return json.dumps({'status':1,'msg':'Success','data':data})


@booth_mandal_admins.route("/get_booths", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def query_execute():
    print(request.values)
    mandal_id  = request.values.get('mandal_id')
    if mandal_id is None:
        return json.dumps({'data':[]})
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        # cur.execute("""
        #     select parliament_constituency_id as pcid,constituency_id as id,constituency_name as text
        #     from assembly_constituencies
        #     where state_id=1
        #     order by parliament_constituency_id
        # """)
        cur.execute("""
            select part_no as id,concat(part_no::text,'  ',polling_station_name) as text,mandal_id
            from polling_booths_2018_jan
            where state_id=1 and mandal_id=%s
        """,(mandal_id,))
        data =cur.fetchall()
        return json.dumps({'data':data})

@booth_mandal_admins.route("/pg_render", methods=['GET', 'POST'])
@booth_mandal_admins.route("/pg_render/", methods=['GET', 'POST'])
@admin_login_required
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def pg_render():
    return render_template("m2/index.html")

# @booth_mandal_admins.route("/smth", methods=['GET', 'POST'])
# @csrf.exempt
# def smth():
#     con = psycopg2.connect(config.DB_CONNECTION)
#     with con.cursor() as cur:
#         cur.execute("""
#             select acm.seq_id,acm.name,acm.village,m.mandal_name as mandal,acm.age,acm.gender,acm.caste,acm.education,acm.occupation,acm.phone,acm.email,
#                 acs.constituency_name as assembly,acs.parliament_constituency_name as parliament,acm.membership_id,acm.voter_id,acm.experience,acm.skills,acm.photo_url
#              from assembly_comittee acm
#              join assembly_constituencies acs on acm.assembly=acs.constituency_id and state_id=1
#              join mandals m on m.id=acm.mandal and m.state_id=1 and m.constituency_id=acm.assembly
#              where acm.photo_url is not null and acm.photo_url != '' and completed_prints != 'YES'
#              order by seq_id,acm.assembly
#              limit 50
#         """)
#         cur.fetchall()
#     return json.dumps({"something":[('jsp123456','something_url'),('jsp123455','something_url'),('jsp123457','something_url'),('jsp123459','something_url')]})
#
# @booth_mandal_admins.route("/say_yes", methods=['GET', 'POST'])
# @csrf.exempt
# def say_yes():
#     j = request.json
#     jsp_id = j['jsp_id']
#     con = psycopg2.connect(config.DB_CONNECTION)
#     with con.cursor() as cur:
#         cur.execute("""
#             update assembly_comittee
#              set completed_prints='YES'
#              where jsp_id=%s
#         """,(jsp_id,))
#         con.commit()
#         return json.dumps({'status':1,'msg':'Updated successfully'})





def shitty_form(name, age, caste, gender, qualification, mobile_no, email, job, ac, pc, jsp_id, voter_id, exp,

                village,

                mandal, image_url,
                skills=[]
                ):
    if image_url == "" or image_url[:4] == 'data':
        print " no image didn't create pdf"
        return
    if mobile_no:
        # folder = r"C:/Users/janasena/PycharmProjects/do-func/formbuilding/new_pdfs/" + pc + '/' + ac + '/'



        packet = io.BytesIO()
        can = Canvas(packet, pagesize=A4)
        can.setFillColor(colors.blue)
        can.setFont('Helvetica-Bold', 10)
        ps = ParagraphStyle('title', fontSize=10, leading=27,
                            firstLineIndent=190,textColor = colors.blue
                            )
        # print(ps.listAttrs())

        basewidth = 300
        tick = u"\u2713"


        can.drawString(89.13258826517648, 690, name.upper())

        can.drawString(77.04978409956811, 663, village.upper())
        can.drawString(276.35636271272534, 663, mandal.upper())

        # can.drawString(107.21234442468881, 630, dob.upper())
        can.drawString(108.7490474980949, 630, age.upper())
        can.drawString(227.2428244856489, 630, gender.upper())

        can.drawString(112.17551435102865, 600, caste.upper())
        can.drawString(321.4820929641859, 600, qualification.upper())

        can.drawString(68.97002794005584, 568, job.upper())
        can.drawString(332.0434340868681, 568, mobile_no.upper())

        can.drawString(90.09271018542029, 536, email.upper())

        can.drawString(141.9392938785877, 505, ac.upper())

        can.drawString(160.18161036322067, 474, pc.upper())

        can.drawString(123.69697739395474, 443, jsp_id.upper())
        can.drawString(375.2489204978409, 443, voter_id.upper())
        if ('motivational speakers' in skills):
            can.drawString(25.2032004064007, 368, tick.upper())
        if ('local issues' in skills):
            can.drawString(305.2413004826009, 368, tick.upper())
        if ('Door to door election campaign' in skills):
            can.drawString(25.2032004064007, 338, tick.upper())
        if ('law and order' in skills):
            can.drawString(305.2413004826009, 338, tick.upper())

        if 'social media publicity' in skills:
            can.drawString(25.2032004064007, 308, tick.upper())
        if 'banners publicity' in skills:
            can.drawString(305.2413004826009, 308, tick.upper())
        if 'content writing' in skills:
            can.drawString(25.2032004064007, 274, tick.upper())
        if 'leadership' in skills:
            can.drawString(305.2413004826009, 274, tick.upper())

        if 'political analysis ' in skills:
            can.drawString(25.2032004064007, 226, tick.upper())

        if 'cultural activities' in skills:
            can.drawString(305.2413004826009, 226, tick.upper())

        experi_p = CustomPara("<b>" + exp + "</b>", ps)
        experi_p.wrapOn(can, 500, 10)
        experi_p.drawOn(can, 42.4056388113, 65)

        if (image_url != "" and image_url != "nan"):
            try:
                print(image_url)
                # urlretrieve(image_url, 'temp.jpg')
                if (image_url[:4] == 'data'):
                    image_url = image_url.split(',')[1]
                    filedata = cStringIO.StringIO(base64.decodestring(image_url))
                else:
                    filedata = urllib2.urlopen(image_url)
                # datatowrite = filedata.read()
                # with open('temp.jpg', 'wb') as f:
                #     f.write(datatowrite)
                # time.sleep(2)
                img = Image.open(filedata)
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img = img.crop((0, 0, basewidth, 350))

                im = ImageReader(img)

                can.drawImage(im, 422, 535, 102, preserveAspectRatio=True, anchor='c')
            except Exception as e:

                print(str(e))
                print()
                return

        can.showPage()
        can.save()

        # Move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # Read your existing PDF
        print(os.getcwd())
        with current_app.open_resource('static/pdf_downloader/f3.pdf','rb') as f:
            # print(f.read())
            existing_pdf = PdfFileReader(f)
            output = PdfFileWriter()
            # Add the "watermark" (which is the new pdf) on the existing page
            page = existing_pdf.getPage(0)
            page.mergePage(new_pdf.getPage(0))
            output.addPage(page)
            # Finally, write "output" to a real file
            name = name.strip()
            file_name = str(jsp_id).lower() + ".pdf"

            try:
                # folder = r"C:/Users/janasena/PycharmProjects/do-func/formbuilding/new_pdfs/"+pc+'/'+ac+'/'


                # print(temp)

                f = io.BytesIO()
                output.write(f)

                # # temp.seek(0)
                # f.seek(0)
                # print(f)
                # outputStream = open(temp, "rb")

                f.seek(0)

                file_url = upload_image_custom(f, "assembly-pdfs-test",file_name)
                print(file_url)
                con = psycopg2.connect(config.DB_CONNECTION)
                with con.cursor() as cur:
                    cur.execute("""
                        update assembly_comittee
                         set completed_prints='YES',pdf_url=%s
                         where membership_id=%s
                    """,(file_url,jsp_id,))
                    con.commit()
                    return file_url

            except Exception as e:
                print("here ")
                print str(e)





@booth_mandal_admins.route("/something", methods=['GET', 'POST'])

# @admin_login_required
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def pdf_downloader():
    membership_list = request.json.get('membership_list',None)
    recreate = request.json.get('recreate',None)
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI, echo=False, convert_unicode=True)
    if membership_list:
        placeholders = ','.join('%s' for _ in range(len(membership_list)))
        query = """
            select acm.seq_id,acm.name,acm.village,m.mandal_name as mandal,acm.age,acm.gender,acm.caste,acm.education,acm.occupation,acm.phone,acm.email,
                acs.constituency_name as assembly,acs.parliament_constituency_name as parliament,acm.membership_id,acm.voter_id,acm.experience,acm.skills,acm.photo_url,acm.pdf_url from assembly_comittee acm
             join assembly_constituencies acs on acs.state_id=acm.state_id and acm.assembly=acs.constituency_id 
             left join mandals m on m.state_id=acs.state_id and m.id=acm.mandal  and m.constituency_id=acm.assembly
             where acm.membership_id  in (%s)
            """%(placeholders)
        print(query)

        # print(str(tuple(membership_list)))
        df = pd.read_sql_query(query, con=engine, params=membership_list)
        engine.dispose()
        df['age'] = df.age.astype(str)


        # sheet_1 = df[0]
        df1 = df.replace('NAN', '', regex=True)
        df1 = df1.replace('NaN', '', regex=True)
        df1 = df1.replace(np.NaN, '', regex=True)
        df1 = df1.replace(np.NAN, '', regex=True)
        pdf_urls = []
        for index, row in df1.iterrows():
            print("I am inside")
            skills = row['skills'].replace('"', '').split(',')
            pdf_url = row['pdf_url']
            if pdf_url in [np.NAN,np.NaN,'',None] :

                smth = shitty_form(name=row['name'],

                            age=row['age'] if row['age'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            caste=row['caste'] if row['caste'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                       np.NAN] else '',
                            gender=row['gender'] if row['gender'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            qualification=row['education'] if row['education'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                       np.NAN] else '',
                            mobile_no=row['phone'] if row['phone'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            email=row['email'] if row['email'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            job=row['occupation'] if row['occupation'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            ac=row['assembly'] if row['assembly'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            pc=row['parliament'] if row['parliament'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            jsp_id=str(
                                row['membership_id'] if row['membership_id'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                     np.NAN] else ''),
                            voter_id=str(
                                row['voter_id'] if row['voter_id'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else ''),
                            exp=row['experience'] if row['experience'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',

                            village=row['village'] if row['village'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',

                            mandal=row['mandal'] if row['mandal'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                            image_url=str(row['photo_url']),
                            skills=skills)
            else:
                smth = pdf_url
            pdf_urls.append(smth)
        return json.dumps({'urls':pdf_urls,'status':1})
    return json.dumps({'msg':'enter atleast one','status':0})





@booth_mandal_admins.route("/multiple_pdf_downloads", methods=['GET', 'POST'])

# @admin_login_required
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def multiple_downloader():
    print(request.json)
    query_extra = ''
    if request.json:
        parliament_id = request.json.get('parliament_id',None)
        consituency_id = request.json.get('constituency_id',None)


        if parliament_id not in [None,'']:
            query_extra = query_extra + ' and '
            query_extra += 'acm.parliament = %s'%(str(parliament_id))
        if consituency_id not in [None,'']:
            query_extra =  query_extra + ' and '
            query_extra += ' acm.assembly = %s'%(str(consituency_id))


    engine = create_engine(config.SQLALCHEMY_DATABASE_URI, echo=False, convert_unicode=True)


    query = """
    select acm.seq_id,acm.name,acm.village,m.mandal_name as mandal,acm.age,acm.gender,acm.caste,acm.education,acm.occupation,acm.phone,acm.email,
    acs.constituency_name as assembly,acs.parliament_constituency_name as parliament,acm.membership_id,acm.voter_id,acm.experience,acm.skills,acm.photo_url,acm.pdf_url
    from assembly_comittee acm
    join assembly_constituencies acs on acm.assembly=acs.constituency_id and acs.state_id=1
    join mandals m on m.id=acm.mandal and m.state_id=1 and m.constituency_id=acm.assembly
    where acm.photo_url is not null and acm.photo_url != ''  and (source is null) and pdf_url is not null
    and membership_id in ('JSP51597035','JSP64735557','JSP52158118','JSP08993278','JSP23974609','JSP39068434','JSP65106151','JSP47416209','JSP52684940','JSP06803554','JSP43515571')
    order by seq_id,acm.assembly
        """%(query_extra)
    print(query)


    # print(str(tuple(membership_list)))
    df = pd.read_sql_query(query, con=engine)
    engine.dispose()
    df['age'] = df.age.astype(str)


    # sheet_1 = df[0]
    df1 = df.replace('NAN', '', regex=True)
    df1 = df1.replace('NaN', '', regex=True)
    df1 = df1.replace(np.NaN, '', regex=True)
    df1 = df1.replace(np.NAN, '', regex=True)
    pdf_urls = []
    ids = []
    for index, row in df1.iterrows():

        skills = row['skills'].replace('"', '').split(',')
        pdf_url = row['pdf_url']
        print(pdf_url)

        if pdf_url in [np.NAN,np.NaN,'',None]:

            shitty_form_task.delay(name=row['name'],

                        age=row['age'] if row['age'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        caste=row['caste'] if row['caste'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                   np.NAN] else '',
                        gender=row['gender'] if row['gender'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        qualification=row['education'] if row['education'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                   np.NAN] else '',
                        mobile_no=row['phone'] if row['phone'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        email=row['email'] if row['email'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        job=row['occupation'] if row['occupation'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        ac=row['assembly'] if row['assembly'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        pc=row['parliament'] if row['parliament'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        jsp_id=str(
                            row['membership_id'] if row['membership_id'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                 np.NAN] else ''),
                        voter_id=str(
                            row['voter_id'] if row['voter_id'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else ''),
                        exp=row['experience'] if row['experience'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',

                        village=row['village'] if row['village'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',

                        mandal=row['mandal'] if row['mandal'] not in ['NAN', 'NaN', 'nan', np.NaN, np.NAN] else '',
                        image_url=str(row['photo_url']),
                        skills=skills)
        else:
            smth = pdf_url
            ids.append(row['seq_id'])
            pdf_urls.append(smth)
    if ids:
        con = psycopg2.connect(config.DB_CONNECTION)
        with con.cursor() as cur:
            cur.execute("""
                update assembly_comittee
                set completed_prints='YES'
                where seq_id in %s
            """,(tuple(ids),))
            con.commit()
    return json.dumps({'urls':pdf_urls,'status':1})
    return json.dumps({'msg':'enter atleast one','status':0})


@booth_mandal_admins.route("/downloader", methods=['GET', 'POST'])

# @admin_login_required
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def downloader():
    return render_template('admin/multi_pdf_downloader.html')

@booth_mandal_admins.route("/generate_current", methods=['GET', 'POST'])

# @admin_login_required
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def something_else():

    generate_current.delay()
    return json.dumps({'msg': 'generation started', 'status': 1})




@booth_mandal_admins.route("/get_map", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def smt():
    get_map.delay()
    return "Working on it please wait"


# def get_map():
#     print("got hit")
#     bio = BytesIO()
#     con = psycopg2.connect(config.DB_CONNECTION)
#     with con.cursor() as cur:
#         df1 = pd.read_excel('static/assets/Guntur Final List.xlsx')
#         df1['Party Memebership NO.'] = ''
#         for row in df1.iterrows():
#             print(row[0])
#             item = row[1]['Contact Number']
#             items = str(item).strip().split('/')
#             for item in items:
#                 if len(item) == 12:
#                     search = item
#                 else:
#                     search = '91' + item
#
#                 crypted = encrypt_decrypt(str(search),'E')
#                 cur.execute("""
#                     select membership_id,polling_station_id,voter_id,constituency_id,state_id
#                     from janasena_membership
#                     where phone=%s
#                 """,(crypted,))
#                 r  = cur.fetchone()
#                 if r is not None:
#                     df1.loc[row[0],'Party Memebership NO.'] = r[0]
#                     polling_station_id = r[1]
#                     df1.loc[row[0],'Voter ID NO.'] = encrypt_decrypt(r[2])
#                     constituency_id = r[3]
#                     state_id = r[4]
#                     cur.execute("""
#                         select polling_station_name
#                         from polling_booths_2018_jan
#                         where constituency_id=%s and state_id=%s and part_no=%s
#                     """,(constituency_id,state_id,polling_station_id))
#                     rm = cur.fetchone()
#                     if rm is None:
#                         continue
#                     else:
#                         df1.loc[row[0], 'Booth Name & Number'] = str(polling_station_id)+"-"+str(rm[0])
#         writer = pd.ExcelWriter(bio, engine='xlsxwriter')
#         df1.to_excel(writer, sheet_name='Sheet1')
#         writer.save()
#         bio.seek(0)
#         return send_file(bio,as_attachment=True,attachment_filename='test.xlsx',mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

@booth_mandal_admins.route("/v_form", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def v_form():
    return render_template('village_mapper/i2.html')

@booth_mandal_admins.route("/get_d", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def d():
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select district_id,district_name
            from dist
        """)
        rows = cur.fetchall()
        return json.dumps({'status':1,'data':rows})


@booth_mandal_admins.route("/get_m", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def m():
    district_id = request.json.get('district_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select mandal_id,mandal_name
            from mand
            where  district_id=%s
        """,(district_id,))
        rows = cur.fetchall()
        return json.dumps({'status':1,'data':rows})

@booth_mandal_admins.route("/get_pc", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_pc():
    district_id = request.json.get('district_id')
    mandal_id = request.json.get('mandal_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select panchayat_id,panchayat_name
            from punch
            where  district_id=%s and mandal_id=%s
        """,(district_id,mandal_id))
        rows = cur.fetchall()
        return json.dumps({'status':1,'data':rows})


@booth_mandal_admins.route("/get_vil", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_vil():
    district_id = request.json.get('district_id')
    mandal_id = request.json.get('mandal_id')
    panchayat_id = request.json.get('panchayat_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select village_id,village_name
            from vil
            where  district_id=%s and mandal_id=%s and panchayat_id=%s
        """,(district_id,mandal_id,panchayat_id))
        rows = cur.fetchall()
        return json.dumps({'status':1,'data':rows})


@booth_mandal_admins.route("/get_hb", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def get_hb():
    district_id = request.json.get('district_id')
    mandal_id = request.json.get('mandal_id')
    panchayat_id = request.json.get('panchayat_id')
    village_id = request.json.get('village_id')
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select habitation_id,habitation_name
            from hb
            where  district_id=%s and mandal_id=%s and panchayat_id=%s and village_id=%s
        """,(district_id,mandal_id,panchayat_id,village_id))
        rows = cur.fetchall()
        return json.dumps({'status':1,'data':rows})


@booth_mandal_admins.route("/chk_voter", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def chk_voter():
    voter_id = request.json.get('voter_id')
    print(voter_id)
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select state,ac_no,part_no
            from voters
            where voter_id=%s
        
        """,(voter_id,))
        row = cur.fetchone()
        if row is None:
            return json.dumps({'status':0,'msg':'Sorry we couldn\'t find any one with the voter id'})
        else:
            print(row)
            state_id = row['state']
            constituency_id = row['ac_no']
            polling_station_id = row['part_no']
            cur.execute("""
                select id as village_id,village_name,v.my_district_id,
                v.my_mandal_id,pb.state_id,pb.constituency_id,pb.part_no
                from polling_booths_2018_jan pb
                join vil v on v.my_district_id= pb.district_id and v.my_mandal_id = pb.mandal_id
                where pb.state_id=%s and pb.constituency_id=%s and pb.part_no=%s
            """,(state_id,constituency_id,polling_station_id))
            rows = cur.fetchall()

            return json.dumps({'status':1,'data':rows})


@booth_mandal_admins.route("/save_village_map", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def save_village_map():
    village_id = request.json.get('village_id')
    voter_id = request.json.get('voter_id')
    print(request.json)
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select * from village_pb_map
            where voter_id=%s
        """,(voter_id,))
        row = cur.fetchone()
        if row is not None:
            return json.dumps({'status':0,'msg':'Voter id has already been mapped to a village'})
    if not village_id:
        village_name = request.json.get('village_name')

        with con.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute("""
                    select state,ac_no,part_no
                    from voters
                    where voter_id=%s

                """, (voter_id,))
            row = cur.fetchone()
            if row is None:
                return json.dumps({'status': 0, 'msg': 'Sorry we couldn\'t find any one with the voter id'})
            else:
                print(row)
                state_id = row['state']
                constituency_id = row['ac_no']
                polling_station_id = row['part_no']
                #get district id and mandal id to save.
                cur.execute("""
                    select district_id,mandal_id
                    from polling_booths_2018_jan
                    where state_id=%s and constituency_id=%s and part_no=%s
                """,(state_id,constituency_id,polling_station_id))
                row = cur.fetchone()
                district_id = row['district_id']
                mandal_id = row['mandal_id']
                cur.execute("""
                    insert into vil(my_district_id,my_mandal_id,
                    village_name)
                    values(%s,%s,%s)
                    returning id
                """,(district_id,mandal_id,village_name))
                row  = cur.fetchone()
                cur.execute("""
                    INSERT INTO village_pb_map(state_id,district_id,constituency_id,
                    mandal_id,village_id,polling_booth_id,voter_id)
                    values(%s,%s,%s,%s,%s,%s,%s)
                """,(state_id,district_id,constituency_id,mandal_id,row['id'],polling_station_id,voter_id))
                con.commit()
                return json.dumps({'status':1,'msg':'Thanks for entering the data'})
    else:
        data = request.json

        with con.cursor(cursor_factory=RealDictCursor) as cur:

            cur.execute("""
                                INSERT INTO village_pb_map(state_id,district_id,constituency_id,
                                mandal_id,village_id,polling_booth_id,voter_id)
                                values(%s,%s,%s,%s,%s,%s,%s)
                            """, (data['state_id'], data['my_district_id'],
                                  data['constituency_id'], data['my_mandal_id'], data['village_id'],
                                  data['part_no'],data['voter_id']))
            con.commit()

            return json.dumps({'status': 1, 'msg': 'Thanks for entering the data'})


@booth_mandal_admins.route("/save_voter_mapping", methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def save_voter_mapping():
    data = request.json
    district_id = data.get('district_id')
    mandal_id = data.get('mandal_id')
    panchayat_id = data.get('panchayat_id')
    village_id = data.get('village_id')
    habitat_id = data.get('habitat_id')
    voter_id = data.get('voter_id')

    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select *
            from voter_mapping
            where voter_id = %s
        
        """,(voter_id,))
        row = cur.fetchone()
        if row:
            return json.dumps({'status':0,'msg':'User has already been mapped'})
        else:
            cur.execute("""
                insert into voter_mapping(district_id,mandal_id,panchayat_id,village_id,habitat_id,voter_id)
                values(%s,%s,%s,%s,%s,%s)
            """,(district_id,mandal_id,panchayat_id,village_id,habitat_id,voter_id))
            con.commit()
            map_booth_with_voter_id.delay(voter_id)
            return json.dumps({'status':1,'msg':'Data saved successfully'})
