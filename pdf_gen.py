import os

import PIL
import io
import urllib
from StringIO import StringIO
from copy import deepcopy
from urllib import urlretrieve

import numpy as np
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.lib import colors

from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph
import tempfile
import os

class CustomPara(Paragraph):
    def wrap(self, availWidth, availHeight):
        # work out widths array for breaking
        self.width = availWidth
        style = self.style
        leftIndent = style.leftIndent
        first_line_width = availWidth - (leftIndent+style.firstLineIndent) - style.rightIndent
        later_widths = availWidth - leftIndent - style.rightIndent
        self._wrapWidths = [first_line_width, later_widths]
        if style.wordWrap == 'CJK':
            #use Asian text wrap algorithm to break characters
            blPara = self.breakLinesCJK(self._wrapWidths)
        else:
            blPara = self.breakLines(self._wrapWidths)
        self.blPara = blPara
        length = 4
        autoLeading = getattr(self,'autoLeading',getattr(style,'autoLeading',''))
        leading = style.leading
        if len(blPara.lines) > 4:
            extra = len(blPara.lines) - 4
            blPara.lines = blPara.lines[0:4]
        if blPara.kind==1:
            if autoLeading not in ('','off'):
                height = 0
                if autoLeading=='max':
                    for l in blPara.lines:
                        height += max(l.ascent-l.descent,leading)
                elif autoLeading=='min':
                    for l in blPara.lines:
                        height += l.ascent - l.descent
                else:
                    raise ValueError('invalid autoLeading value %r' % autoLeading)
            else:
                height = length * leading
        else:
            if autoLeading=='max':
                leading = max(leading,blPara.ascent-blPara.descent)
            elif autoLeading=='min':
                leading = blPara.ascent-blPara.descent
            height = length * leading
        self.height = height
        return self.width, height
errors = []
def shitty_form(name, age, caste, gender, qualification, mobile_no, email, job, ac, pc, jsp_id, voter_id, exp,

                village,

                mandal, image_url,
                skills=[]
                ):
    if mobile_no:
        folder = r"e:/new_pdfs/" + pc + '/' + ac + '/'
        f_name=(name.split(' ')[0]).strip()
        file_name = f_name + '_' + mobile_no + '_' + '_' + ac + ".pdf"
        destination = folder + file_name
        if os.path.exists(destination):
            print("Already created")
            return
        ###########
        from reportlab.pdfbase import pdfmetrics
        from reportlab.pdfbase.ttfonts import TTFont
        from reportlab.pdfgen.canvas import Canvas

        pdfmetrics.registerFont(
            TTFont("rambhadra", "/static/fonts/Rambhadra.ttf"))
        ########
        packet = io.BytesIO()
        can = Canvas(packet, pagesize=A4)
        can.setFillColor(colors.blue)
        can.setFont('Helvetica-Bold', 10)
        ps = ParagraphStyle('title', fontSize=10, leading=27,
                            firstLineIndent=190,textColor = colors.blue
                            )
        # print(ps.listAttrs())

        basewidth = 300
        tick = u"\u2713"


        can.drawString(89.13258826517648, 690, name.upper())

        can.drawString(77.04978409956811, 663, village.upper())
        can.drawString(276.35636271272534, 663, mandal.upper())

        # can.drawString(107.21234442468881, 630, dob.upper())
        can.drawString(108.7490474980949, 630, age.upper())
        can.drawString(227.2428244856489, 630, gender.upper())

        can.drawString(112.17551435102865, 600, caste.upper())
        can.drawString(321.4820929641859, 600, qualification.upper())

        can.drawString(68.97002794005584, 568, job.upper())
        can.drawString(332.0434340868681, 568, mobile_no.upper())

        can.drawString(90.09271018542029, 536, email.upper())

        can.drawString(141.9392938785877, 505, ac.upper())

        can.drawString(160.18161036322067, 474, pc.upper())

        can.drawString(123.69697739395474, 443, jsp_id.upper())
        can.drawString(375.2489204978409, 443, voter_id.upper())
        if ('motivational speakers' in skills):
            can.drawString(25.2032004064007, 368, tick.upper())
        if ('local issues' in skills):
            can.drawString(305.2413004826009, 368, tick.upper())
        if ('Door to door election campaign' in skills):
            can.drawString(25.2032004064007, 338, tick.upper())
        if ('law and order' in skills):
            can.drawString(305.2413004826009, 338, tick.upper())

        if 'social media publicity' in skills:
            can.drawString(25.2032004064007, 308, tick.upper())
        if 'banners publicity' in skills:
            can.drawString(305.2413004826009, 308, tick.upper())
        if 'content writing' in skills:
            can.drawString(25.2032004064007, 274, tick.upper())
        if 'leadership' in skills:
            can.drawString(305.2413004826009, 274, tick.upper())

        if 'political analysis ' in skills:
            can.drawString(25.2032004064007, 226, tick.upper())

        if 'cultural activities' in skills:
            can.drawString(305.2413004826009, 226, tick.upper())

        experi_p = CustomPara("<b>" + exp + "</b>", ps)
        experi_p.wrapOn(can, 500, 10)
        experi_p.drawOn(can, 42.4056388113, 65)

        if (image_url != "" and image_url != "nan"):
            try:
                urlretrieve(image_url, './static/temp.jpg')

                img = Image.open('./static/temp.jpg')
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img = img.crop((0, 0, basewidth, 350))

                im = ImageReader(img)

                can.drawImage(im, 422, 535, 102, preserveAspectRatio=True, anchor='c')
            except Exception as e:

                print(str(e))
                return

        can.showPage()
        can.save()

        # Move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # Read your existing PDF
        existing_pdf = PdfFileReader(open("./static/f3.pdf", "rb"))
        output = PdfFileWriter()
        # Add the "watermark" (which is the new pdf) on the existing page
        page = existing_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)
        # Finally, write "output" to a real file
        name=(name.split(' ')[0]).strip()
        file_name = name + '_' + mobile_no + '_' + '_' + ac + ".pdf"
        try:
            folder = r"e:/new_pdfs/"+pc+'/'+ac+'/'

            outputStream = open(file_name, "wb")
            output.write(outputStream)

            outputStream.close()

            if not os.path.exists(folder):
                os.makedirs(folder)
            destination = folder + file_name
            os.rename(file_name, destination)
        except Exception as e:
            print str(e)
            os.remove(file_name)

            errors.append({pc: file_name})
        if os.path.exists('./static/temp.jpg'):

            os.remove('./static/temp.jpg')  

    return 'success'
############



