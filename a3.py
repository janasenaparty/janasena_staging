# -*- coding: utf8 -*-
import PIL
import base64
import cStringIO
import io
import urllib2

from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.utils import ImageReader
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph


class CustomPara(Paragraph):
    def wrap(self, availWidth, availHeight):
        # work out widths array for breaking
        self.width = availWidth
        style = self.style
        leftIndent = style.leftIndent
        first_line_width = availWidth - (leftIndent+style.firstLineIndent) - style.rightIndent
        later_widths = availWidth - leftIndent - style.rightIndent
        self._wrapWidths = [first_line_width, later_widths]
        if style.wordWrap == 'CJK':
            #use Asian text wrap algorithm to break characters
            blPara = self.breakLinesCJK(self._wrapWidths)
        else:
            blPara = self.breakLines(self._wrapWidths)
        self.blPara = blPara
        length = 2
        autoLeading = getattr(self,'autoLeading',getattr(style,'autoLeading',''))
        leading = style.leading
        if len(blPara.lines) > 2:
            extra = len(blPara.lines) - 2
            blPara.lines = blPara.lines[0:2]
        if blPara.kind==1:
            if autoLeading not in ('','off'):
                height = 0
                if autoLeading=='max':
                    for l in blPara.lines:
                        height += max(l.ascent-l.descent,leading)
                elif autoLeading=='min':
                    for l in blPara.lines:
                        height += l.ascent - l.descent
                else:
                    raise ValueError('invalid autoLeading value %r' % autoLeading)
            else:
                height = length * leading
        else:
            if autoLeading=='max':
                leading = max(leading,blPara.ascent-blPara.descent)
            elif autoLeading=='min':
                leading = blPara.ascent-blPara.descent
            height = length * leading
        self.height = height
        return self.width, height
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas

encodingName='WinAnsiEncoding',        
pdfmetrics.registerFont(TTFont('Ramabadra', 'Ramabhadra.ttf'))



def aspirant_pdf(data):
    assembly_parliament=data['locations']
    name=data['name'].decode('utf-8')
    father_name=data['relation_name']
    dob=data['dob']
    birth_place=data['birth_place']
    school=data['school_and_place']
    qualification=data['higher_education']
    occupation=data['occupation']
    permanant_addr=data['permenant_address']
    cur_addr=data['current_address']
    image_url=data['photo_url']
    mobile=data['mobile']
    email=data['email']
    further_status=data['further_process']


    basewidth = 300
    packet = io.BytesIO()
    can = Canvas(packet, pagesize=A4)
    can.setFillColor(colors.blue)
    can.setFont('Ramabadra', 12)

    ps = ParagraphStyle('title', fontSize=10, leading=20,
                        firstLineIndent=105, textColor=colors.blue,fontName='Ramabadra',encoding="utf-8")
    # assembly parliament
    can.drawString(295, 620, assembly_parliament)

    #name
    can.drawString(110, 550, name)

    # father name
    can.drawString(132, 520, father_name)

    # dob
    can.drawString(122, 490, dob)

    # birthplace
    can.drawString(125, 460, birth_place)

    # 10th school
    can.drawString(278, 382, school)
    # highest qualification
    can.drawString(155, 360, qualification)
    #occupation
    can.drawString(100, 340, occupation)

    #address
    current_address = CustomPara("<b>" + cur_addr + "</b>", ps)
    current_address.wrapOn(can, 400, 10)
    current_address.drawOn(can, 72, 200)

    permanant_address = CustomPara("<b>" + permanant_addr + "</b>", ps)
    permanant_address.wrapOn(can, 400, 10)
    permanant_address.drawOn(can, 72, 150)



    #mobile
    can.drawString(150, 125, mobile)
    #email
    can.drawString(370, 125, email)

    if (image_url != "" and image_url != "nan"):
        try:
            # print(image_url)
            # urlretrieve(image_url, 'temp.jpg')
            if (image_url[:4] == 'data'):
                image_url = image_url[23:]
                filedata = cStringIO.StringIO(base64.decodestring(image_url))
            else:
                filedata = urllib2.urlopen(image_url)
            img = Image.open(filedata)
            wpercent = (basewidth / float(img.size[0]))
            hsize = int((float(img.size[1]) * float(wpercent)))
            img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
            img = img.crop((0, 0, basewidth, 350))

            im = ImageReader(img)

            can.drawImage(im, 400, 350, 102, preserveAspectRatio=True, anchor='c')

        except Exception as e:

            print(str(e))
            print()
            return





    can.showPage()
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # Read your existing PDF
    existing_pdf = PdfFileReader(open("1.pdf", "rb"))
    output = PdfFileWriter()
    # Add the "watermark" (which is the new pdf) on the existing page
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    if further_status=='further process':
        path="C:/Users/Srini1/Desktop/aspirant_pdfs/fs/"
    else:

        path="C:/Users/Srini1/Desktop/aspirant_pdfs/mbfs/"
    file_name = path+str(data['membership_id'])+".pdf"
    
    outputStream = open(file_name, "wb")
    output.write(outputStream)

    outputStream.close()
    print 'completed'




