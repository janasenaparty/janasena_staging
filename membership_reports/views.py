import requests
from flask import request, json, render_template
from psycopg2.extras import RealDictCursor

from appholder import csrf
from constants import empty_check

from utilities.decorators import crossdomain, admin_login_required, required_roles,ip_validation_required
from utilities.others import give_cursor_json

from flask import Blueprint

mem_report = Blueprint('mem_report', __name__,static_url_path='/static/test')
# top n assemby volunteer target assembly

@mem_report.route('/report/group_n_days', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def group_n_days():

    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            group_type = request.values.get('group_type')
            number_of_items = request.values.get('no_items', 2)
            number_of_days = request.values.get('no_days',2)
            default_id = request.values.get('default_id')
            if group_type in empty_check:
                return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})
            if number_of_items in empty_check:
                return json.dumps({'errors': ['PARAMETER no_items EMPTY OR MISSING'], 'data': {'status': 0}})
            if number_of_days in empty_check:
                return json.dumps({'errors': ['PARAMETER no_days EMPTY OR MISSING'], 'data': {'status': 0}})
            if default_id in empty_check:
                return json.dumps({'errors': ['PARAMETER default_id EMPTY OR MISSING'], 'data': {'status': 0}})

            group_map = {
                'state':'state_id',
                'district':'district_id',
                'assembly':'constituency_id'

            }
            cur.execute("""
                                select constituency_id,constituency_name
                                from assembly_constituencies
                            """)
            assemblies = cur.fetchall()
            am = {str(a[0]): str(a[1]) for a in assemblies}
            cur.execute("""
                                               select seq_id,district_name
                                               from districts
                                           """)
            assemblies = cur.fetchall()
            ds = {str(a[0]): str(a[1]) for a in assemblies}
            cur.execute("""
                                                               select id,name
                                                               from state
                                                           """)
            assemblies = cur.fetchall()
            st = {str(a[0]): str(a[1]) for a in assemblies}
            if group_type == 'A':
                state_id = request.values.get('state_id')
                if state_id in empty_check:
                    return json.dumps({'errors': ['PARAMETER state_id EMPTY OR MISSING'], 'data': {'status': 0}})
                query = " date(create_dttm) > current_date - interval %s day and state_id=(select id from state where state=%s) "

                data_tuple = (number_of_days,state_id)


            else:
                query = " date(create_dttm) > current_date - interval %s day "
                data_tuple = (number_of_days, )

            main_query = """
                select {{mapp}},count(*)
                from janasena_membership
                where {{query}}
                group by {{mapp}}
                order by count(*) desc
                limit {{limit_val}};
            """

            main_query = main_query.replace('{{query}}',query).replace('{{limit_val}}',number_of_items)\
                .replace('{{mapp}}',group_map[group_type])
            print main_query

            cur.execute(main_query, data_tuple)
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'status': 1,'data':rows,'st':st,'ds':ds,'am':am})

    return give_cursor_json(temp)

@mem_report.route('/report/trend_graph', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def trend_graph():

    def temp(con, cur):
        if request.method == 'POST':
            group_type = request.values.get('group_type')

            number_of_days = request.values.get('no_days')
            default_id = request.values.get('default_id')
            print request.values
            if group_type in empty_check:
                return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})

            if number_of_days in empty_check:
                return json.dumps({'errors': ['PARAMETER no_days EMPTY OR MISSING'], 'data': {'status': 0}})
            if default_id in empty_check:
                return json.dumps({'errors': ['PARAMETER default_id EMPTY OR MISSING'], 'data': {'status': 0}})

            group_map = {
                'state': 'state_id',
                'district': 'district_id',
                'assembly': 'constituency_id'

            }
            if group_type == 'assembly':
                state_id = request.values.get('state_id')
                if state_id in empty_check:
                    return json.dumps({'errors': ['PARAMETER state_id EMPTY OR MISSING'], 'data': {'status': 0}})
                query = " date(create_dttm) > current_date -  %s and district_id=(select seq_id from districts where district_name=%s) and {{mapp}} =%s"

                data_tuple = (int(number_of_days),state_id,default_id,int(number_of_days))
            elif group_type == 'state':
                query = " date(create_dttm) > current_date - %s   and {{mapp}} = (select id from state where name=%s)"

                data_tuple = (int(number_of_days), default_id,int(number_of_days))
                pass
            else:
                query = " date(create_dttm) > current_date - %s and {{mapp}} =(select seq_id from districts where district_name=%s)"
                data_tuple = (int(number_of_days),default_id,int(number_of_days) )

            main_query = """
            select to_char(dates.d,'DD-MM-YYYY'),coalesce(sum(c),0)
            from
                (select date(jm.create_dttm) as d,count(*) as c
                from 
                janasena_membership jm
                where {{query}}
                group by date(jm.create_dttm)
                order by date(jm.create_dttm) desc) ct 
            right join (
            select d::date from generate_series(current_date - %s, 
            current_date, '1 day'::interval) d
            )dates on dates.d=ct.d
            group by dates.d
            order by dates.d;              
            """
            main_query = main_query.replace('{{query}}',query).replace('{{mapp}}',group_map[group_type])
            print main_query,data_tuple

            cur.execute(main_query, data_tuple)
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'status': 1,'data':rows})

    return give_cursor_json(temp)


# Number of members in polling booth ranges 0 1-5 5-10 gt 10
@mem_report.route('/report/polling_booth_counts', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def polling_booth_counts():
    def temp(con, cur):
        if request.method == 'POST':
            group_type = request.values.get('group_type')
            number_of_items = request.values.get('no_items', 2)
            number_of_days = request.values.get('no_days',2)
            default_id = request.values.get('default_id')
            if group_type in empty_check:
                return json.dumps({'errors': ['PARAMETER group_type EMPTY OR MISSING'], 'data': {'status': 0}})
            if number_of_items in empty_check:
                return json.dumps({'errors': ['PARAMETER no_items EMPTY OR MISSING'], 'data': {'status': 0}})
            if number_of_days in empty_check:
                return json.dumps({'errors': ['PARAMETER no_days EMPTY OR MISSING'], 'data': {'status': 0}})
            if default_id in empty_check:
                return json.dumps({'errors': ['PARAMETER default_id EMPTY OR MISSING'], 'data': {'status': 0}})

            group_map = {
                'S':'state_id',
                'D':'district_id',
                'A':'constituency_id'

            }
            if group_map == 'A':
                cur.execute("""
                
                """)
                pass

    return give_cursor_json(temp)


@mem_report.route('/report/ui', methods=['GET', 'POST','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def report_ui():
    return render_template('membership_reports/mem_rep.html')


@mem_report.route('/report/test', methods=['GET', 'POST'])
@mem_report.route('/report/test/', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def mtest():
    return render_template('test/index.html')

@mem_report.route('/report/polling_booth_0', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def polling_booth_0():

    def temp(con, cur):
        if request.method == 'POST':

            district_id = request.values.get('district_id')
            constituency_id = request.values.get('constituency_id')
            cur.execute("""
                        select distinct on( ac.state_id,ac.district_name,ac.constituency_id,psd.part_no ) 
                        ac.district_name,ac.parliament_constituency_name,ac.constituency_name,psd.mandal,psd.part_no as Polling_booth_no,psd.polling_station_name,psd.polling_station_address as Polling_Booth_Address,
                        case when jm.count is null then 0 else jm.count
                        end as member_count,
                         case when jm.count is null then 'To be identified' else ''
                        end as identification
                        from assembly_constituencies ac
                        join polling_station_details psd on psd.state_id=ac.state_id and psd.constituency_id=ac.constituency_id
                        left join (select state_id,district_id,constituency_id,polling_station_id,count(*)
                        from janasena_membership
                        where district_id=%s and constituency_id=%s
                        group by state_id,district_id,constituency_id,polling_station_id) jm on jm.state_id=psd.state_id and jm.constituency_id=psd.constituency_id and jm.polling_Station_id=psd.part_no
                        where ac.district_id=%s and ac.constituency_id=%s
                        order by ac.state_id,ac.district_name,ac.constituency_id,psd.part_no
    
                    """, (district_id, constituency_id, district_id, constituency_id))
            rows = cur.fetchall()
            return json.dumps({'status':1,'data':rows})
        elif request.method == 'GET':
            return render_template('')
    pass


@mem_report.route('/report/sql_downloader', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def sql_downloader():
    def temp(con, cur):
        if request.method == 'GET':
            pass
        elif request.method == 'POST':
            pass
        pass

    return give_cursor_json(temp)

# Kuldeep requirements
@mem_report.route('/mem_report/total_member_count', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def mem_count():
    def temp(con, cur):
        cur.execute("""
            select count(membership_id)
            from janasena_membership
        """)
        count = cur.fetchone()[0]
        return json.dumps({'status':1,'data':{'total_members':count}})

    return give_cursor_json(temp)


@mem_report.route('/mem_report/total_member_count_month', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def total_member_count_month():
    """
    {'status':1,'data':{'total_members':{'JAN':150,'FEB':200}}
    :return:
    """
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        dictcur.execute("""
            select to_char(create_dttm,'fmMonth') as month_name, extract(month from create_dttm) as month,
            extract(year from create_dttm) as year,
            count(membership_id)
            from janasena_membership
            group by 3,2,1
            order by 3,2,1
        """)
        counts = dictcur.fetchall()
        return json.dumps({'status':1,'data':{'total_members':counts}})

    return give_cursor_json(temp)


@mem_report.route('/mem_report/total_member_count_region', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def total_member_count_region():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        if request.method == 'POST':
            region_level = request.values.get('region_level')
            region_id = request.values.get('region_id')

            if region_id in empty_check:
                return json.dumps({'status':0,'error':'parameter region_id missing'})
            if region_level in empty_check:
                return json.dumps({'status':0,'error':'parameter region_level missing'})
            if region_level =='state':
                dictcur.execute("""
                               select count(membership_id)
                               from janasena_membership
                               where state_id=%s
                           """,(region_id,))
            elif region_level =='district':
                dictcur.execute("""
                                               select count(membership_id)
                                               from janasena_membership
                                               where district_id=%s
                                           """, (region_id,))
            elif region_level == 'constituency':
                state_id = request.values.get('state_id')
                if state_id in empty_check:
                    return json.dumps({'status': 0, 'error': 'parameter state_id missing'})
                dictcur.execute("""
                                       select count(membership_id)
                                       from janasena_membership
                                       where state_id=%s and constituency_id=%s
                                   """, (state_id,region_id,))



            counts = dictcur.fetchone()['count']
            return json.dumps({'status':1,'data':{'total_members':counts}})

    return give_cursor_json(temp)\

@mem_report.route('/mem_report/top_constituency_counts', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def top_constituency_counts():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        if request.method == 'POST':
            order_type = request.values.get('order_type','1')

            od = {
                '-1':'asc',
                '1':'desc'
            }
            if order_type in empty_check:
                return json.dumps({'status':0,'error':'parameter order_type missing'})
            query = """
            select s.name,ac.constituency_name,count
            from (
                select state_id,constituency_id,count(*) as count
                from janasena_membership
                group by state_id,constituency_id
                order by count {{order_type}}
                limit 10)
                t join state s on s.id = t.state_id
            join assembly_constituencies ac on ac.constituency_id = t.constituency_id and ac.state_id=t.state_id
            """
            query = query.replace('{{order_type}}',od[order_type])
            dictcur.execute(query)



            counts = dictcur.fetchall()
            return json.dumps({'status':1,'data':counts})

    return give_cursor_json(temp)

@mem_report.route('/mem_report/top_district_counts', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def top_district_counts():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        if request.method == 'POST':
            order_type = request.values.get('order_type','1')

            od = {
                '-1':'asc',
                '1':'desc'
            }
            if order_type in empty_check:
                return json.dumps({'status':0,'error':'parameter order_type missing'})
            query = """
                select district_name,count
                from districts d
                join(
                select district_id,count(*) as count
                from janasena_membership
                group by district_id
                order by count {{order_type}}
                limit 10) jm on jm.district_id = d.seq_id
            """
            query = query.replace('{{order_type}}',od[order_type])
            dictcur.execute(query)

            """select membership_through,count(membership_id) from janasena_membership group by membership_through;"""

            counts = dictcur.fetchall()
            return json.dumps({'status':1,'data':counts})

    return give_cursor_json(temp)



@mem_report.route('/mem_report/channel_member_count', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def channel_member_count():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        query ="""
            select mt,sum(count)
            from(
            select case when regexp_replace(membership_through, '\s+$', '') in ('W','S','ME','U') then 'WEBSITE'
                        WHEN regexp_replace(membership_through, '\s+$', '')= 'M' then 'APP'
                        WHEN regexp_replace(membership_through, '\s+$', '') ='CC' then 'CALL CENTER'
                        WHEN regexp_replace(membership_through, '\s+$', '') in ('OFNF','OFV') THEN 'OFFLINE'
                        when regexp_replace(membership_through, '\s+$', '') in ('MC','MU','SM') then 'CONTACTS'
                        when regexp_replace(membership_through, '\s+$', '') ='WB' then 'WHATSAPPBOT'                        
                        else 'UNKNOWN'
                        end as mt,count(*) 
            from janasena_membership 
            group by membership_through
            order by count desc) t
            group by mt
            """


        dictcur.execute(query)
        counts = dictcur.fetchall()
        return json.dumps({'status': 1, 'data': counts})
    return give_cursor_json(temp)


@mem_report.route('/mem_report/strong_referrers', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def strong_referrers():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        query ="""
                select jm.name,jt.count,ac.district_name,ac.constituency_name
                from janasena_membership jm
                join
                (select referral_id,count(*)
                from janasena_membership
                where referral_id is not null and referral_id !='' 
                group by referral_id
                
                order by count desc
                limit 10) jt on jt.referral_id = jm.membership_id
                join assembly_constituencies ac on ac.district_id=jm.district_id and ac.constituency_id=jm.constituency_id 
                order by count desc
            """


        dictcur.execute(query)
        counts = dictcur.fetchall()
        return json.dumps({'status': 1, 'data': counts})
    return give_cursor_json(temp)


@mem_report.route('/mem_report/monthly_growth', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def monthly_growth():
    def temp(con, cur):
        dictcur = con.cursor(cursor_factory=RealDictCursor)
        query = """
            select state_id,constituency_id,lm.count-l2m.count as growth
        
            (select count(membership_id),state_id,constituency_id
            from janasena_membership
            where create_dttm >= (current_date - interval '1' month)
            group by state_id,constituency_id) lm
            join
            (select count(membership_id),state_id,constituency_id
            from janasena_membership
            where create_dttm >= (current_date - interval '1' month)
            group by state_id,constituency_id) l2m
            on lm.state_id = l2m.state_id and lm.constituency_id=l2m.constituency_id
            order by growth desc
            
           
            """

        dictcur.execute(query)
        counts = dictcur.fetchall()
        return json.dumps({'status': 1, 'data': counts})

    return give_cursor_json(temp)


@mem_report.route('/mem_report/constituency_pollbooth_member_count', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@ip_validation_required
@required_roles(["AD","OP","MB","MR"])
def constituency_pollbooth_member_count():
    def temp(con, cur):
        if request.method == 'GET':
            return render_template("membership_reports/constituency_poll_booth_count.html")
        elif request.method == 'POST':
            print request.values
            # state_id = request.values.get('state_id')
            district_name = request.values.get('district_name')
            constituency_id = request.values.get('constituency_id')

            cur.execute("""
                select seq_id,district_name,state_id
                from districts
                where district_name=%s
            """,(district_name,))
            d_row=cur.fetchone()
            district_id = d_row[0]
            state_id=d_row[2]
            cur.execute("select count(*) as count from janasena_membership where district_id=%s and constituency_id=%s",(district_id,constituency_id))
            memberships_row=cur.fetchone()
            print memberships_row
            if memberships_row is not None:
                memberships=memberships_row[0]
            else:
                memberships=0
            cur.execute("select count(distinct(psd.part_no)) from polling_station_details psd left join janasena_membership jm on jm.state_id=psd.state_id and jm.constituency_id=psd.constituency_id and jm.polling_station_id=psd.part_no where jm.polling_station_id is null and psd.state_id=%s and psd.constituency_id=%s",(state_id,constituency_id))
            tbi_row=cur.fetchone()
            print tbi_row
            if tbi_row is not None:
                to_be_identified=tbi_row[0]
            else:
                to_be_identified=0
            cur.execute("""
                        select distinct on( ac.state_id,ac.district_name,ac.constituency_id,psd.part_no ) 
                        ac.district_name,ac.parliament_constituency_name,ac.constituency_name,psd.mandal,psd.part_no as Polling_booth_no,psd.polling_station_name,psd.polling_station_address as Polling_Booth_Address,
                        case when jm.count is null then 0 else jm.count
                        end as member_count,
                         case when jm.count is null then 'To be identified' else ''
                        end as identification
                        from assembly_constituencies ac
                        join polling_station_details psd on psd.state_id=ac.state_id and psd.constituency_id=ac.constituency_id
                        left join (select state_id,district_id,constituency_id,polling_station_id,count(*)
                        from janasena_membership
                        where district_id=%s and constituency_id=%s
                        group by state_id,district_id,constituency_id,polling_station_id) jm on jm.state_id=psd.state_id and jm.constituency_id=psd.constituency_id and jm.polling_Station_id=psd.part_no
                        where ac.district_id=%s and ac.constituency_id=%s   
                        order by ac.state_id,ac.district_name,ac.constituency_id,psd.part_no

                    """, (district_id, constituency_id, district_id, constituency_id))
            dts = cur.fetchall()
            
            return json.dumps({'data':dts,'status':1,'memberships':memberships,"to_be_identified":to_be_identified})

    return give_cursor_json(temp)

@mem_report.route('/mem_report/constituency_pollbooth_member_count_consolidated', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@admin_login_required
@ip_validation_required
@required_roles(["AD", "OP", "MB","MR"])
def constituency_pollbooth_member_count_consolidated():
    def temp(con, cur):
        if request.method == 'GET':
            return render_template("membership_reports/constituency_consolidated.html")
        elif request.method == 'POST':
            
            dictcur = con.cursor(cursor_factory=RealDictCursor)
            dictcur.execute("""
                        select ac.constituency_id,ac.district_name,ac.constituency_name,consolidated.filled,consolidated.unfilled,consolidated.total from (
select state_id,district_id,constituency_id,sum(filled) as filled,sum(unfilled) as unfilled,count(*) as total
from
(select psd.state_id,psd.district_id,psd.constituency_id,psd.polling_station_id,
sum(case when md.memc is not null then 1
else 0 end) as  filled,
sum(case when md.memc is null then 1
else 0 end) as  unfilled
from
(select distinct on (state_id,district_id,constituency_id,part_no)state_id,district_id,constituency_id,part_no as polling_station_id
from polling_station_details)psd

left join 

(select state_id,district_id,constituency_id,polling_station_id,count(*) as memc
from janasena_membership
where polling_station_id != 0
group by state_id,district_id,constituency_id,polling_station_id
order by state_id,district_id,constituency_id,polling_station_id)md
on md.state_id = psd.state_id and md.district_id= psd.district_id and md.constituency_id=psd.constituency_id
and md.polling_station_id=psd.polling_station_id
group by psd.state_id,psd.district_id,psd.constituency_id,psd.polling_station_id
order by psd.state_id,psd.district_id,psd.constituency_id,psd.polling_station_id)lm
group by state_id,district_id,constituency_id)consolidated join assembly_constituencies ac on ac.state_id=consolidated.state_id and ac.constituency_id=consolidated.constituency_id order by ac.constituency_id

                    """, )
            dts = dictcur.fetchall()
            return json.dumps(
                {'data': dts, 'status': 1})

    return give_cursor_json(temp)




@mem_report.route('/tag_search',methods=['GET','POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def tag_search():
    tag = request.values.get('tag')
    payload = {
        'tag':tag
    }
    news = requests.post("http://httpbin.org/post", data=payload)
    events = requests.post("http://httpbin.org/post", data=payload)
    somethingelse = requests.post("http://httpbin.org/post", data=payload)

    try:
        news_data = json.loads(news.text)
        events_data = json.loads(events.text)
        somethingelse_data = json.loads(somethingelse.text)
        return json.dumps({'status':1,'data':[].extend(news_data).extend(events_data).extend(somethingelse_data)})
    except Exception as e:
        print str(e)
        return json.dumps({'status':0,'errors':['Could not get the data from one of the source']})

        pass

