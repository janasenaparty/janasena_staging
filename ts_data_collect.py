import requests
import psycopg2
from psycopg2.extras import RealDictCursor
import config
from bs4 import BeautifulSoup
def collect():
	try:

		
		con = psycopg2.connect(config.DB_CONNECTION)              
		cur = con.cursor(cursor_factory=RealDictCursor)

		url = "https://tsec.gov.in/nominationFormsRural.do"
		cur.execute("select id,district_name from ts_new_districts where id>8")
		districts=cur.fetchall()
		for district in districts:
			print district['district_name'] +" started"
			district_id=str(district['id']).zfill(2)
			cur.execute("select id,mandal_name from ts_new_mandals where district_id=%s",(district['id'],))
			mandals=cur.fetchall()
			for mandal in mandals:
				mandal_id=str(mandal['id']).zfill(2)

				cur.execute("select id,panchayat_name from ts_new_grampanchayats where district_id=%s and mandal_id=%s",(district['id'],mandal['id']))
				panchayats=cur.fetchall()
				for panchayat in panchayats:

					panchayat_id=str(panchayat['id']).zfill(2)
					try:
						querystring = {"mode":"getPSList","district_id":str(district_id),"mandal_id":str(mandal_id),"gpcode":str(panchayat_id),"mptc":0,"nominationFor":"M","gpward": 0}

						payload = ""
						headers = {
							'JSESSIONID': "F84913369FE92E06E08687EC8706363E"
							
							}

					
						response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

						data = response.text
						soup = BeautifulSoup(data)
						for option in soup.find_all('option'):
							
							booth_id=option['value']
							name=option.text
							if str(booth_id)!='0':
								try:
									cur.execute("insert into ts_new_polling_booths(district_id,mandal_id,panchayat_id,booth_id,booth_name)values(%s,%s,%s,%s,%s)",(district_id,mandal_id,panchayat_id,booth_id,name,))
									con.commit()
								except Exception as e:
									print str(e)
									continue
					except Exception as e:
								print str(e)
								continue



	except Exception as e:
		print str(e)

collect()
