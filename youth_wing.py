import os

import psycopg2
from flask import url_for
from xlrd import open_workbook

import config
from appholder import celery_app,db
from utilities.classes import encrypt_decrypt
from utilities.others import generate_membershipid

from psycopg2.extras import RealDictCursor
@celery_app.task
def random_membership():
    con = psycopg2.connect(config.DB_CONNECTION)
    cur = con.cursor(cursor_factory=RealDictCursor)
    path = os.getcwd()
    
    book = open_workbook('Srikakulam Youth wing Members.xlsx')
    sheet = book.sheet_by_index(0)
    
    district_ids = {}
    count=0
    print sheet.nrows
    for row_index in xrange(1, sheet.nrows):

        try:
            mobile = sheet.cell(row_index, 3).value
            
            assembly_name = sheet.cell(row_index, 1).value
            name = str(sheet.cell(row_index, 2).value)

            tn = int(mobile)
        except Exception as e:
            # print e
            tn = 0
            continue
        tn = str(tn)
       
        if tn != '0':
            print tn
            encrypted_mobile = encrypt_decrypt(str(tn), 'E')
            cur.execute('select phone,name from janasena_membership where phone=%s', (encrypted_mobile,))
            mobile_row = cur.fetchone()
            print mobile_row
            
            if  mobile_row is None:
                count=count+1
                print count

                assembly_name=assembly_name.lower()
                cur.execute(
                    'select constituency_id,district_id,state_id from assembly_constituencies where lower(constituency_name)=%s',
                    (assembly_name,))
                row = cur.fetchone()
                
                if row:
                   
                    constituency_id = row['constituency_id']
                else:
                   constituency_id=1 
                
                state = 'AP'
                
                voter_id=''
                relation_name=''
                relation_type=''
                address=''
                town=''
                age=00
                gender=''
                booth=0
                assembly_id=constituency_id
                state='AP'
                email=''
                booth_name=''
                referral_id=''
                membership_through='S'

                print str(name)
                status,membership_id=generate_membershipid(name, tn, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                          assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india')
                cur.execute("update janasena_membership set member_associations=%s,language=%s where membership_id=%s",
                        ('Youth Wing', 'Telugu', membership_id))
                con.commit()
                print membership_id

    con.close()