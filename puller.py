import getopt
import sys
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.dialects.mysql.base import TINYINT
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import ProgrammingError
import pymysql as MySQLdb
import pymysql.cursors
def get_table_list_from_db(metadata):
    """
    return a list of table names from the current
    databases public schema
    """
    sql="select table_name from information_schema.tables where table_schema='public' order by table_name desc"
    return ['hyd_aircel_prepaid_view']

def make_session(connection_string):
    
    engine = create_engine(connection_string, echo=False, convert_unicode=True)
    Session = sessionmaker(bind=engine)
    return Session(), engine

def pull_data(from_db, to_db):
    print locals()
    source, sengine = make_session(from_db)
    smeta = MetaData(bind=sengine)
    destination, dengine = make_session(to_db)
    dmeta = MetaData(bind=dengine)
    
    dest_tables = get_table_list_from_db(dengine)
    print dest_tables
    for table_name in dest_tables:
        print table_name
        print 'Processing', table_name
        print 'Pulling schema from source server'
        table = Table(table_name, smeta, autoload=True)
        print 'Creating table on destination server'
        table.metadata.create_all(dengine)
        
        WINDOW_SIZE = 100000
        
        start = 0
        while True:
            stop = start + WINDOW_SIZE
            records = source.query(table).slice(start, stop).all()
            if records is None:
                break
            try:
                dengine.execute(table.insert().values(records))
            except Exception as e:
                
                print "something wrong"
                #print str(e)
            print 'Committing changes'
            destination.commit()
            if len(records) < WINDOW_SIZE:
                break
            start += WINDOW_SIZE
            print start
            

    print 'Fixing sequences'
    return 'success'



if __name__ == '__main__':
    from_db='mysql://root:root@localhost:3306/contacts_latest?charset=utf8'
    to_db='postgresql://postgres:root@localhost:5432/contacts_db'
    
    pull_data(from_db,to_db)