from sqlalchemy import *

from appholder import db
import datetime

class JanasenaMissedcalls2(db.Model):
    __tablename__ = 'janasena_missedcalls2'

    jsp_supporter_seq_id = Column(Integer, primary_key=True, server_default=text("nextval('janasena_missedcalls2_jsp_supporter_seq_id_seq'::regclass)"))
    phone = Column(String(15))
    supporter_id = Column(String(15))
    create_dttm = Column(DateTime(True), server_default=text("now()"))
    update_dttm = Column(DateTime(True), server_default=text("now()"))
    member_through = Column(String(50))
    email = Column(String)

    def __init__(self, phone, member_through):
        self.phone = phone

        self.member_through = member_through

    def __repr__(self):
        return '<Missedcalls2 %r>' % (self.jsp_supporter_seq_id)




class ShatagniMagazine(db.Model):
    __tablename__ = "shatagni_magazine_team"
    id = db.Column('seq_id', Integer, primary_key=True)
    phone=db.Column('phone',db.String(30),unique=True)
    pin=db.Column('pin',db.String(30),unique=True)
    name=db.Column('name',db.String(200))
    email=db.Column('email',db.String(100))
    pincode=db.Column('pincode',db.String(10))
    landmark=db.Column('landmark',db.String(200))
    house_no=db.Column('house_no',db.String(100))
    street_village=db.Column('street_village',db.String(200))
    city_town=db.Column('city_town',db.String(200))
    state=db.Column('state',db.String(100))
   
    def __init__(self, id, phone,name,email,pincode,landmark,house_no,street_village,city_town,state):
        self.id = id
        self.phone = phone
        self.name = name
        self.pincode = pincode
        self.landmark=landmark
        self.house_no=house_no
        self.street_village=street_village
        self.city_town=city_town
        self.state=state
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)
    def __repr__(self):
        return '<ShatagniMagazine %r>' % (self.id)


class ACHead(db.Model):
    __tablename__ = "ac_head"
    id = db.Column('id', Integer, primary_key=True)
    phone = db.Column('phone', db.String(30), unique=True)
    name = db.Column('name', db.String(200))
    state_id = db.Column('state_id',db.Integer)
    constituency_id = db.Column('constituency_id',db.Integer)
    district_id = db.Column('district_id',db.Integer)
    utype = db.Column('utype',db.String(10))

    def __init__(self, id, phone, name, utype, state_id,district_id,constituency_id):
        self.id = id
        self.phone = phone
        self.name = name
        self.utype = utype
        self.state_id = state_id
        self.district_id = district_id
        self.constituency_id = constituency_id

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<ACHead %r>' % (self.id)

class MembershipTargets(db.Model):
    __tablename__ = "membership_targets"
    id = db.Column('seq_id', Integer, primary_key=True)
    membership_id = db.Column('membership_id', db.String(15), unique=True)
    target = db.Column('target', Integer)
    achievement = db.Column('achievement', Integer)
    def __init__(self, id, membership_id,target,achievement):
        self.id = id
        self.membership_id = membership_id
        self.target = target
        self.achievement = achievement
    def __repr__(self):
        return '<MembershipTargets %r>' % (self.id)
class MembershipFields(db.Model):
    __tablename__ = "member_fields"
    
    id = db.Column('seq_id', Integer, primary_key=True)
    membership_id = db.Column('membership_id', db.String(15), unique=True)
    submitted_by = db.Column('submitted_by', Integer)
    village = db.Column('village', db.String(150))
    mandal  = db.Column('mandal', db.String(150))
    phone_number_status  = db.Column('phone_number_status', db.String(150))
    def __init__(self, id, membership_id,submitted_by,village,mandal,phone_number_status):
        self.id = id
        self.membership_id = membership_id
        self.submitted_by = submitted_by
        self.village = village
        self.mandal = mandal
        self.phone_number_status = phone_number_status
    def __repr__(self):
        return '<MembershipFields %r>' % (self.id)
class SMSDeliveryCodes(db.Model):
    __tablename__ = "sms_delivery_codes"
    
    code = db.Column('code', db.String(150), primary_key=True)
    status = db.Column('status', db.String(150), unique=True)
    
    def __init__(self, code, status):
        self.code = code
        self.status = status
        
    def __repr__(self):
        return '<SMSDeliveryCodes %r>' % (self.code)

class AdminIPAddress(db.Model):
    __tablename__ = "admin_ip_address"
    
    id = db.Column('seq_id', Integer, primary_key=True)
    ip_address = db.Column('ip_address', db.String(150))
    location = db.Column('location', db.String(150))
    
    def __init__(self, id,ip_address, location):
        self.id=id
        self.ip_address = ip_address
        self.location = location
        
    def __repr__(self):
        return '<adminIPAddress %r>' % (self.id)

class PollingStations(db.Model):
    __tablename__ = "polling_booths_2018_jan"
    id = db.Column('seq_id', Integer, primary_key=True)
    state_id = db.Column('state_id', Integer)
    constituency_id = db.Column('constituency_id', Integer)
    ps_id = db.Column('part_no', Integer)
    mandal = db.Column('mandal', Integer)
    mandal_id = db.Column('mandal_id', Integer)
    ps_name = db.Column('polling_station_name', db.String(1000))

    def __init__(self, id, state_id, constituency_id, ps_id, mandal, ps_name,mandal_id):
        self.id = id
        self.ps_name = ps_name
        self.state_id = state_id
        self.constituency_id = constituency_id
        self.ps_id = ps_id
        self.mandal = mandal
        self.mandal_id = mandal_id
    def __repr__(self):
        return '<PollingStations %r>' % (self.id)


class Mandals(db.Model):
    __tablename__ = "mandals"
    id = db.Column('id', Integer, primary_key=True)
    state_id = db.Column('state_id', Integer)
    constituency_id = db.Column('constituency_id', Integer)
    district_id = db.Column('district_id', Integer)
    mandal_name = db.Column('mandal_name', db.String(50))
    

    def __init__(self, seq_id, state_id, constituency_id, district_id, mandal_name):
        self.seq_id = seq_id
       
        self.state_id = state_id
        self.constituency_id = constituency_id
        self.district_id = district_id
        self.mandal_name = mandal_name

    def __repr__(self):
        return '<Mandals %r>' % (self.id)

class member_session(db.Model):
    __tablename__ = "member_session"
    seq_id = db.Column('seq_id', Integer, primary_key=True)
    action = db.Column('action', db.String(5))
    session_id = db.Column('session_id', db.String(100))
    phone = db.Column('phone', db.String(15))
    membership_id = db.Column('membership_id', db.String(15))

    def __init__(self, seq_id, action, session_id, phone, membership_id):
        self.seq_id = seq_id
        self.action = action
        self.session_id = session_id
        self.phone = phone
        self.membership_id = membership_id

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.seq_id)

    def __repr__(self):
        return '<member_session %r>' % (self.membership_id)


class NRIMembers(db.Model):
    __tablename__ = "nri_members"
    id = db.Column('nriid', Integer, primary_key=True)
    first_name = db.Column('first_name', db.String(100))
    last_name = db.Column('last_name', db.String(100))
    phone = db.Column('phone', db.String(15))
    email = db.Column('email', db.String(256))
    age = db.Column('age', Integer)
    gender = db.Column('gender', db.String(80))
    constituency_id = db.Column('constituency_id', Integer)
    city = db.Column('city', db.String(100))
    country = db.Column('country', db.String(100))
    zipcode = db.Column('zipcode', db.String(80))
    education = db.Column('education', db.String(200))
    profession = db.Column('profession', db.String(200))
    state = db.Column('state', Integer)
    profile_status = db.Column('profile_status', db.String(80))
    voterId = db.Column('voterid', db.String(15))
    isACitizenOfIndia = db.Column('isacitizenofindia', db.String(20))

    def __init__(self, id, constituency_id, first_name, last_name,
                 phone, email, age, voterId, isACitizenOfIndia, gender, city, country, zipcode, profile_status, state,
                 education, profession):
        self.id = id
        self.constituency_id = constituency_id
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.email = email
        self.age = age
        self.gender = gender
        self.city = city
        self.country = country
        self.zipcode = zipcode
        self.profile_status = profile_status
        self.education = education
        self.state = state
        self.profession = profession
        self.isACitizenOfIndia = isACitizenOfIndia
        self.voterId = voterId

    def __repr__(self):
        return '<NRIMembers %r>' % (self.id)


class Memberships(db.Model):
    __tablename__ = "janasena_membership"
    id = db.Column('jsp_membership_seq_id', Integer, primary_key=True)
    voter_id = db.Column('voter_id', db.String(256), unique=True)
    name = db.Column('name', db.String(100))
    phone = db.Column('phone', db.String(256))
    membership_id = db.Column('membership_id', db.String(15), unique=True)
    relationship_name = db.Column('relationship_name', db.String(15))
    relation_type = db.Column('relation_type', db.String)
    address = db.Column('address', db.String(50))
    age = db.Column('age', db.String(50))
    sex = db.Column('gender', db.String(80))
    polling_station_id = db.Column('polling_station_id', db.String(8))
    polling_station_name = db.Column('polling_station_name', db.String(8))
    village_town = db.Column('village_town', db.String(500))
    email = db.Column('email', db.String(256))
    state = db.Column('state', db.String(80))
    constituency_id = db.Column('constituency_id', db.String(80))
    referral_id = db.Column('referral_id', db.String(15))
    is_volunteer = db.Column('is_volunteer', db.String(2))
    membership_through = db.Column('membership_through', db.String(5))
    member_associations = db.Column('member_associations', db.String(500))
    image_url = db.Column('image_url', db.String(500))
    member_pin = db.Column('member_pin', db.String(256))
    language = db.Column('language', db.String(100))
    district_id = db.Column('district_id', Integer)
    state_id = db.Column('state_id', Integer)
    user_gcm_id = db.Column('user_gcm_id', db.String(5000))
    create_dttm=db.Column('create_dttm', DateTime(timezone=True), default=datetime.datetime.utcnow)
    def __init__(self, id, constituency_id, voter_id, name, relationship_name, relation_type, address, age, sex,
                 polling_station_id, village_town, email, state, phone, membership_id, referral_id, is_volunteer,
                 polling_station_name, membership_through, member_associations, image_url, member_pin, language,
                 state_id, district_id,user_gcm_id,create_dttm):
        self.id = id
        self.voter_id = voter_id
        self.name = name
        self.relationship_name = relationship_name
        self.relation_type = relation_type
        self.address = address
        self.age = age
        self.sex = sex
        self.polling_station_id = polling_station_id
        self.village_town = village_town
        self.email = email
        self.state = state
        self.constituency_id = constituency_id
        self.phone = phone
        self.membership_id = membership_id
        self.referral_id = referral_id
        self.is_volunteer = is_volunteer
        self.polling_station_name = polling_station_name
        self.membership_through = membership_through
        self.member_associations = member_associations
        self.image_url = image_url
        self.member_pin = member_pin
        self.language = language
        self.district_id = district_id
        self.state_id = state_id
        self.user_gcm_id=user_gcm_id
        self.create_dttm=create_dttm
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        if self.is_volunteer == 'T':
            return 'V'
        else:

            return 'M'

    def __repr__(self):
        return '<Memberships %r>' % (self.membership_id)


class MembersThroughSMS(db.Model):
    __tablename__ = "member_with_sms"
    id = db.Column('seq_id', Integer, primary_key=True)
    voter_id = db.Column('voter_id', db.String(20))
    volunteer_mobile = db.Column('volunteer_mobile', db.String(15))
    new_member_mobile = db.Column('new_member_mobile', db.String(15))
    status = db.Column('status', db.String(5))

    def __init__(self, voter_id, volunteer_mobile, new_member_mobile, status):
        self.voter_id = voter_id
        self.volunteer_mobile = volunteer_mobile
        self.new_member_mobile = new_member_mobile
        self.status = status

    def __repr__(self):
        return '<member_with_sms %r>' % (self.voter_id)


class AssemblyConstituencies(db.Model):
    __tablename__ = "assembly_constituencies"
    seq_id = db.Column('seq_id', Integer, primary_key=True)
    constituency_name = db.Column('constituency_name', db.String(50))
    state_id = db.Column('state_id', Integer)
    constituency_id = db.Column('constituency_id', Integer)
    district_id = db.Column('district_id', Integer)
    parliament_constituency_id = db.Column('parliament_constituency_id', Integer)
    parliament_constituency_name = db.Column('parliament_constituency_name', db.String(150))
    district_name = db.Column('district_name', db.String(50))

    def __init__(self, seq_id, constituency_name, state_id, constituency_id, parliament_constituency_id, district_name,parliament_constituency_name):
        self.seq_id = seq_id
        self.constituency_name = constituency_name
        self.state_id = state_id
        self.constituency_id = constituency_id
        self.parliament_constituency_id = parliament_constituency_id
        self.district_name = district_name
        self.parliament_constituency_name=parliament_constituency_name
        
    def __repr__(self):
        return '<AssemblyConstituencies %r>' % (self.seq_id)


class VoterStatus(db.Model):
    __tablename__ = "voter_registration_status"
    seq_id = db.Column('seq_id', Integer, primary_key=True)
    voter_id = db.Column('voter_id', db.String(20), unique=True)
    status = db.Column('status', db.String(20))

    def __init__(self, seq_id, voter_id, state_id, status):
        self.seq_id = seq_id
        self.voter_id = voter_id
        self.status = status

    def __repr__(self):
        return '<VoterStatus %r>' % (self.seq_id)


class Voters(db.Model):
    __tablename__ = "voters"

    voter_id = db.Column('voter_id', db.String(20), primary_key=True)
    voter_name = db.Column('voter_name', db.String(100))
    relation_name = db.Column('relation_name', db.String(15))
    relation_type = db.Column('relation_type', db.String)
    house_no = db.Column('house_no', db.String(50))
    age = db.Column('age', db.String(50))
    sex = db.Column('sex', db.String(80))
    section_no = db.Column('section_no', db.String(80))
    section_name = db.Column('section_name', db.String(80))
    part_no = db.Column('part_no', Integer)
    page_no = db.Column('page_no', db.String(80))
    ac_no = db.Column('ac_no', Integer)
    state = db.Column('state', Integer)
    status = db.Column('status', db.String(10))

    def __init__(self, ac_no, voter_id, voter_name, relation_name, relation_type, house_no, age, sex, section_no,
                 section_name, part_no, page_no, state, status):
        self.voter_id = voter_id
        self.voter_name = voter_name
        self.relation_name = relation_name
        self.relation_type = relation_type
        self.house_no = house_no
        self.age = age
        self.sex = sex
        self.section_no = section_no
        self.section_name = section_name
        self.part_no = part_no
        self.page_no = page_no
        self.ac_no = ac_no
        self.state = state
        self.status = status

    def __repr__(self):
        return '<Voters %r>' % (self.voter_id)


class MembershipPending(db.Model):
    __tablename__ = "membership_pending"
    id = db.Column('membership_pending_seq_id', db.Integer, primary_key=True)
    user_mobile = db.Column('user_mobile', db.String(15))
    status = db.Column('status', db.String(15))

    def __init__(self, user_mobile, status):
        self.user_mobile = user_mobile
        self.status = status

    def __repr__(self):
        return '<MembershipPending %r>' % (self.user_mobile)


class Admin(db.Model):
    __tablename__ = "admin_team"
    id = db.Column('admin_id', db.Integer, primary_key=True)
    email = db.Column('user_name', db.String(100))
    phone = db.Column('admin_mobile', db.String(15))
    hashed_password = db.Column('user_mod_hash', db.String)
    fname = db.Column('first_name', db.String(50))
    lname = db.Column('last_name', db.String(50))
    urole = db.Column('role', db.String(80))

    def __init__(self,id, email, phone, hashed_password, fname, lname):
        self.email = email
        self.phone = phone
        self.hashed_password = hashed_password
        self.fname = fname
        self.lname = lname
        self.urole = 'AD'
        self.id = id

    def is_authenticated(self):
        return True

    def is_active(self, session_id):
        active_session = Admin.query.join(AdminSession, Admin.id == AdminSession.admin_id).filter(
            AdminSession.session_id == session_id, AdminSession.status == 'A').first()

        if active_session:
            return True
        return False

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<Admin %r>' % (self.email)


class AdminSession(db.Model):
    __tablename__ = "session_admin"

    session_id = db.Column('session_id', db.Integer, primary_key=True)
    admin_id = db.Column('admin_id', db.Integer)
    status = db.Column('status', db.String(80))
    session_val = db.Column('session_val', db.String(1000))

    def __init__(self, admin_id, session_id, status, session_val):
        self.admin_id = admin_id
        self.session_id = session_id
        self.status = status
        self.session_val = session_val

    def __repr__(self):
        return '{}-{}-{}-{}'.format(self.admin_id, self.status, self.session_id, self.session_val)


class Frontdesk(db.Model):
    __tablename__ = "front_desk_team"
    id = db.Column('id', db.Integer, primary_key=True)
    email = db.Column('user_email', db.String(100))
    phone = db.Column('mobile', db.String(15))
    hashed_password = db.Column('user_mod_hash', db.String)
    fname = db.Column('first_name', db.String(50))
    lname = db.Column('last_name', db.String(50))
    urole = db.Column('role', db.String(80))
    status = db.Column('status', db.String(10))
    location_id = db.Column('location_id', db.Integer)

    def __init__(self,id, email, phone, hashed_password, fname, lname, status, location_id, urole):
        self.id = id
        self.email = email
        self.phone = phone
        self.hashed_password = hashed_password
        self.fname = fname
        self.lname = lname
        self.urole = urole
        self.status = status
        self.location_id = location_id

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<Frontdesk %r>' % (self.email)

class OfflineMemberships(db.Model):
    __tablename__ = "janasainyam_team"
    id = db.Column('id', db.Integer, primary_key=True)
    email = db.Column('user_email', db.String(100))
    phone = db.Column('mobile', db.String(15))
    hashed_password = db.Column('user_mod_hash', db.String)
    fname = db.Column('first_name', db.String(50))
    lname = db.Column('last_name', db.String(50))
    urole = db.Column('role', db.String(80))
    status = db.Column('status', db.String(10))
    

    def __init__(self,id, email, phone, hashed_password, fname, lname, status, urole):
        self.id = id
        self.email = email
        self.phone = phone
        self.hashed_password = hashed_password
        self.fname = fname
        self.lname = lname
        self.urole = urole
        self.status = status
        

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<OfflineMemberships %r>' % (self.email)

class Janasena_Missedcalls(db.Model):
    __tablename__ = "janasena_missedcalls"
    jsp_supporter_seq_id = db.Column('jsp_supporter_seq_id', db.Integer, primary_key=True)
    phone = db.Column('phone', db.String(15))
    supporter_id = db.Column('supporter_id', db.String(15))
    member_through = db.Column('member_through', db.String(15))

    def __init__(self, phone, member_through):
        self.phone = phone

        self.member_through = member_through

    def __repr__(self):
        return '<Missedcalls %r>' % (self.jsp_supporter_seq_id)


class BisSession(db.Model):
    __tablename__ = 'bis_session'

    id = Column(Integer, primary_key=True, server_default=text("nextval('bis_session_id_seq'::regclass)"))
    session_val = Column(String(256))
    create_dttm = Column(DateTime(True), server_default=text("now()"))
    update_dttm = Column(DateTime(True), server_default=text("now()"))
    admin_id = Column(Integer)
    status = Column(String(10))

    def __init__(self,id=None, session_val=None, create_dttm=None, update_dttm=None, admin_id=None, status=None):
        self.session_val = session_val
        self.create_dttm = create_dttm
        self.update_dttm = update_dttm
        self.admin_id = admin_id
        self.status = status
        self.urole = 'BI'
        self.id = id

    def is_authenticated(self):
        return True


    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<BIS %r>' % (self.admin_id)

class JspsurveySession(db.Model):
    __tablename__ = 'jspsurvey_session'

    id = Column(Integer, primary_key=True, server_default=text("nextval('jspsurvey_session_id_seq'::regclass)"))
    session_val = Column(String(256))
    create_dttm = Column(DateTime(True), server_default=text("now()"))
    update_dttm = Column(DateTime(True), server_default=text("now()"))
    admin_id = Column(String(24))
    status = Column(String(10))

    def __init__(self,id=None, session_val=None, create_dttm=None, update_dttm=None, admin_id=None, status=None):
        self.session_val = session_val
        self.create_dttm = create_dttm
        self.update_dttm = update_dttm
        self.admin_id = admin_id
        self.status = status
        self.urole = 'JS'
        self.id = id

    def is_authenticated(self):
        return True


    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<BIS %r>' % (self.admin_id)



################################ kriya models ##################

def check_valid_date(inputDate):
        import datetime
        year,month,day = inputDate.split('-')
        isValidDate = True
        try :
            datetime.datetime(int(year),int(month),int(day))
        except ValueError :
            isValidDate = False
        return isValidDate

##################### KRIYA Modals ############## ##########################################
class KriyaVolunteers(db.Model):
    __tablename__ = 'kriya_volunteers'

    id = Column(Integer, primary_key=True)
    mobile = Column(String(15))
    name = Column(String(100))
    created_on = Column(DateTime(True), server_default=text("now()"))
    updated_on = Column(DateTime(True), server_default=text("now()"))
    approved_by = Column(String(50))
    email = Column(String)
    pin = Column(String)
    status = Column(String)
    state = Column(Integer)
    district_id = Column(Integer)
    constituency_id = Column(Integer)
    mandal = Column(Integer)
    imei = Column(String)

    def __init__(self, mobile, name,state,district_id,constituency_id,status='Pending'):
        self.mobile = mobile

        self.name = name 
        self.status = status 
        
        self.state = state
        self.district_id = district_id
        self.constituency_id = constituency_id
        
    Active    = "Active"
    Suspended = "Suspended"
    Pending   = "Pending"
    
    

    def __repr__(self):
        return '<KriyaVolunteer %r>' % self.id
    
    def isUserActive(self):
        return self.status == User.Active
    def verifyPin(self,pin):
        return self.pin == pin  


    @staticmethod
    def validateUser( name, mobile,state,district_id,constituency_id):
        
        if  name is None :
            logger.error(" Name should not be empty")
            return False, "Name should not be empty"

        if  len(name) > 100 :
            logger.error(" Name cannot be bigger than 100 characters")
            return False, "Name is too big."      
        if  mobile is None :
            logger.error(" mobile should not be empty")
            return False, "mobile should not be empty"
        
        if len(mobile) > 10:
            logger.error("Mobile Number cannot be bigger than 10 digits")
            return False, "Mobile Number is too big."
        if state is None:
            return False, "state should not be empty: {}".format(state)
        if district_id is None:
            return False, "district_id should not be empty: {}".format(district_id)
        if constituency_id is None:
            return False, "constituency_id should not be empty: {}".format(constituency_id)
        return True, None

class KriyaMembers(db.Model):
    __tablename__ = 'kriya_members'

    id = Column(Integer, primary_key=True)
    mobile = Column(String(15))
    name = Column(String(100))
    created_on = Column(DateTime(True), server_default=text("now()"))
    updated_on = Column(DateTime(True), server_default=text("now()"))
    
    email = Column(String)
    
    status = Column(String)
    state = Column(Integer)
    district_id = Column(Integer)
    constituency_id = Column(Integer)
    mandal_id = Column(Integer)
    
    jsp_id  =Column(String)
    address =Column(String)
    remarks =Column(String)
    gender =Column(String)
    dob =Column(Date)
    age =Column(String)
    education =Column(String)
    occupation =Column(String)
    working_with_janasena_from_year_and_month =Column(String)
    jsp_id =Column(String)
    do_you_know_janasena_principles =Column(String)
    which_principles_are_you_aware_of =Column(String)
    smart_phone_or_feature_phone =Column(String)
    payment_mode  =Column(String)
    have_you_ever_participate_in_jsp_activitis =Column(String)
    participated_jsp_activities =Column(String)
    how_much_time_can_you_spend_for_party_activities =Column(String)
    aadhar_number = Column(String)
    photo =Column(String)
    aadhar_card =Column(String)
    payment_id =Column(String)
    payment_status =Column(String)
    added_by =Column(String)
    def __init__(self, mobile,address,state,district_id,constituency_id, name,email,gender,dob,education,occupation,
        working_with_janasena_from_year_and_month,do_you_know_janasena_principles,
        which_principles_are_you_aware_of,
        how_much_time_can_you_spend_for_party_activities,added_by,participated_jsp_activities=''
        ,smart_phone_or_feature_phone='',payment_mode='',have_you_ever_participate_in_jsp_activitis='',payment_id='',payment_status='',photo='',aadhar_card='',mandal_id=0,aadhar_number='',jsp_id=None,remarks=None):
        self.mobile = mobile
        self.name = name
        self.email= email
        self.gender = gender
        self.address = address
        self.added_by = added_by
        self.remarks=remarks
        self.state = state
        self.district_id = district_id
        self.constituency_id =  constituency_id
        self.mandal_id = mandal_id
        self.have_you_ever_participate_in_jsp_activitis =have_you_ever_participate_in_jsp_activitis
        self.participated_jsp_activities =participated_jsp_activities
        self.how_much_time_can_you_spend_for_party_activities =how_much_time_can_you_spend_for_party_activities
        self.dob = dob
        self.education = education
        self.occupation = occupation
        self.photo = photo
        self.aadhar_card = aadhar_card
        self.working_with_janasena_from_year_and_month =working_with_janasena_from_year_and_month
        self.jsp_id=jsp_id
        self.do_you_know_janasena_principles = do_you_know_janasena_principles
        self.which_principles_are_you_aware_of =which_principles_are_you_aware_of

        self.smart_phone_or_feature_phone = smart_phone_or_feature_phone
        self.payment_mode = payment_mode


        self.aadhar_number = aadhar_number 
    def __repr__(self):
        return '<KriyaMember %r>' % self.id
    
    
   
    @staticmethod
    def validateUser(name, mobile, email, state, district_id, constituency_id,dob,
        gender,education,occupation,address,working_with_janasena_from_year_and_month,
        do_you_know_janasena_principles,
        which_principles_are_you_aware_of,how_much_time_can_you_spend_for_party_activities,aadhar_number):   


        
        if len(name) > 100:
            logger.error(" Name cannot be bigger than 100 characters")
            return False, "Name is too big."      
        if name is None or name.isspace():
            return False, "Full name is missing."   
        if mobile is None or mobile.isspace():
            return False, "Mobile number is missing."
        if len(mobile) != 10:            
            return False, "Invalid mobile number."

        # Validate Email:
        if email is None or email.isspace():            
            return False, "email id missing: "
        if state is None or state.isspace():
            return False, "state should not be empty: "
        if district_id is None or district_id.isspace():
            return False, "district_id should not be empty"
        if constituency_id is None or constituency_id.isspace():
            return False, "constituency_id should not be empty"
        if dob is None or dob.isspace():
            return False, "dob field should not be empty"
        if not check_valid_date(dob):
            return False, "Invalid date of birth. please check agaiin"
        if education is None or education.isspace():
            return False, "education field should not be empty"    
        if occupation is None or occupation.isspace():
            return False, "occupation field should not be empty"  
        if address is None or address.isspace():
            return False, "address field should not be empty" 
        if working_with_janasena_from_year_and_month is None or working_with_janasena_from_year_and_month.isspace():
            return False," journey_year missing"
        if working_with_janasena_from_year_and_month is None or working_with_janasena_from_year_and_month.isspace():
            return False," journey_year missing"
        if do_you_know_janasena_principles is None or do_you_know_janasena_principles.isspace():
            return False," do_you_know_janasena_principles missing"
        if which_principles_are_you_aware_of is None:
            return False," which_principles_are_you_aware_of missing"
        if how_much_time_can_you_spend_for_party_activities is None or how_much_time_can_you_spend_for_party_activities.isspace():
            return False," how_much_time_can_you_spend_for_party_activities field missing"
        if aadhar_number is None or aadhar_number.isspace():
            return False, "Aadhar number is missing."
        if len(aadhar_number) != 12:            
            return False, "your aadhar number is invalid"

        
        return True, None
    
class KriyaMemberNomineeDetails(db.Model):
    __tablename__ = 'kriya_member_nominee_details'
    id = Column(Integer, primary_key=True)
    kriya_member_id = Column(Integer)
    nominee_mobile = Column(String(15))
    nominee_name = Column(String(150))

    aadhar_id = Column(String(150))
    aadhar_card = Column(String(1500))
    created_on = Column(DateTime(True), server_default=text("now()"))
    updated_on = Column(DateTime(True), server_default=text("now()"))
    
    

    def __init__(self, kriya_member_id,nominee_mobile,nominee_name,aadhar_id):
        

        self.kriya_member_id = kriya_member_id   
        self.nominee_mobile = nominee_mobile
        self.nominee_name = nominee_name
        self.aadhar_id = aadhar_id  
    

    def __repr__(self):
        return '<KriyamemberNominee %r>' % self.id
    

class KriyaVolunteerSession(db.Model):
    __tablename__ = 'kriya_volunteer_session'

    id = Column(Integer, primary_key=True)
    kriya_volunteer_id = Column(Integer)
    mobile = Column(String(15))
    action = Column(String(100))
    created_on = Column(DateTime(True), server_default=text("now()"))
    updated_on = Column(DateTime(True), server_default=text("now()"))
    session_id = Column(String(50))
    

    def __init__(self, mobile, session_id):
        self.mobile = mobile

        self.session_id = session_id     
    

    def __repr__(self):
        return '<KriyaVolunteerSession %r>' % self.id
    
class KriyaMemberpaymentDetails(db.Model):
    __tablename__ = 'kriya_member_payment_details'

    id = Column(Integer, primary_key=True)
    kriya_member_id = Column(Integer)
    amount  = Column(String)
    payment_id = Column(String)
    customer_id = Column(String)
    payment_mode = Column(String)
    payment_status = Column(String)
    payment_url = Column(String)
    created_on = Column(DateTime(True), server_default=text("now()"))
    updated_on = Column(DateTime(True), server_default=text("now()"))
    
    

    def __init__(self, kriya_member_id,payment_mode,payment_id,payment_url,customer_id='',amount=500,payment_status='Success'):
        
        self.kriya_member_id = kriya_member_id 
        self.payment_mode=payment_mode
        self.payment_id = payment_id
        self.payment_url = payment_url
        self.customer_id = customer_id
        self.amount = amount
        self.payment_status = payment_status    
    

    def __repr__(self):
        return '<KriyaMemberpaymentDetails %r>' % self.id
from sqlalchemy.ext.declarative import declarative_base, DeferredReflection

Base = declarative_base(cls=DeferredReflection)

class Donations(db.Model):
    __tablename__ = 'donations_view'  
    
    seq_id = db.Column('seq_id',Integer, primary_key=True)
    name = db.Column('name',db.String(200))  
    relation_name =   db.Column('relation_name',db.String(200))
    phone  = db.Column('phone',db.String(20))
    email = db.Column('email',db.String(20))
    pancard = db.Column('pancard',db.String(20))
    transaction_id = db.Column('transaction_id',db.String(100))
    payment_status = db.Column('payment_status',db.String(20))
    state = db.Column('state',db.String(20)) 
    constituency_name =   db.Column('constituency_name',db.String(50))
    district_name =   db.Column('district_name',db.String(50))
    transaction_amount = db.Column('transaction_payment',db.String(100))
    address = db.Column('address',db.String(1000))
    create_dttm=db.Column('create_dttm', DateTime(timezone=True), default=datetime.datetime.utcnow)
    update_dttm=db.Column('update_dttm', DateTime(timezone=True), default=datetime.datetime.utcnow)
    UI_Columns = ["constituency_name","district_name","update_dttm","seq_id","address","transaction_amount","name","relation_name","phone","email","pancard","transaction_id","payment_status","state"]
    
    

    def __repr__(self):
        return '<janasena_donations %r>' % self.seq_id

   

class KriyaMembersDashboard(db.Model):
    __tablename__ = 'kriya_members_dashboard_view'

    added_by =  db.Column('added_by',db.String(200))  
    dob = db.Column('dob', DateTime(timezone=True))
    jsp_id = db.Column('jsp_id',db.String(200))  
    id = db.Column('id',Integer, primary_key=True)
    payment_verified = db.Column('payment_verified',db.String(200))  
    name = db.Column('name',db.String(200))  
    mobile = db.Column('mobile',db.String(200))  
    remarks = db.Column('remarks',db.String(200))  
    gender = db.Column('gender',db.String(200))  
    email = db.Column('email',db.String(200))  
    state = db.Column('state',Integer)
    district_id = db.Column('district_id',Integer)
    constituency_id = db.Column('constituency_id',Integer)
    address = db.Column('address',db.String(200))  
    district_name = db.Column('district_name',db.String(200))  
    constituency_name = db.Column('constituency_name',db.String(200))  
    aadhar_number = db.Column('aadhar_number',db.String(200))  
    nominee_name = db.Column('nominee_name',db.String(200))  
    nominee_aadhar_number = db.Column('nominee_aadhar_number',db.String(200))  
    working_with_janasena_from_year_and_month = db.Column('working_with_janasena_from_year_and_month',db.String(200))  
    do_you_know_janasena_principles = db.Column('do_you_know_janasena_principles',db.String(200))  
    which_principles_are_you_aware_of = db.Column('which_principles_are_you_aware_of',db.String(200))  
    participated_jsp_activities = db.Column('participated_jsp_activities',db.String(200))  
    how_much_time_can_you_spend_for_party_activities = db.Column('how_much_time_can_you_spend_for_party_activities',db.String(200))  
    payment_mode = db.Column('payment_mode',db.String(200))  
    payment_id = db.Column('payment_id',db.String(200))  
    payment_date  = db.Column('created_on', DateTime(timezone=True), default=datetime.datetime.utcnow)
    payment_url = db.Column('payment_url',db.String(200))  
    payment_status = db.Column('payment_status',db.String(200))  
    aadhar_card = db.Column('aadhar_card',db.String(200))  
    photo = db.Column('photo',db.String(200)) 
    age = db.Column('age',Integer) 
    table_metadata = [
    {"data":"id","title":"id"},
   { "data": "name" ,"title": "name"},
   { "data": "mobile","title" :"mobile"},
   { "data": "gender","title":"gender" },
   {"data":"state","title":"state"},       
   { "data": "district_name" ,"title":"District"},
   { "data": "constituency_name" ,"title":"Assembly"},
   { "data": "aadhar_number","title":"Aadhar" },
   { "data": "nominee_name","title":"Nominee name" },
   { "data": "nominee_aadhar_number","title":"nominee_aadhar_number" },
   { "data": "dob","title":"dob" },
   { "data": "age" ,"title":"age"},
   {"data":"jsp_id","title":"jsp_id"},
   { "data": "payment_date","title":"Payment Date" },
    {"data":"payment_status","title":"Payment Status"},
    {"data":"payment_id","title":"Payment ID"},
    {"data":"how_much_time_can_you_spend_for_party_activities","title":"how_much_time_can_you_spend_for_party_activities"},
    {"data":"payment_verified","title":"Payment Verified"},
    {"data":"added_by","title":"Added By"},
    {"data":"remarks","title":"Remarks"}
   
   ]
    UI_Columns = ['state','district_id','age',
'constituency_id',
'address',
'district_name',
'constituency_name',
'aadhar_number',
'nominee_name',
'nominee_aadhar_number',
'working_with_janasena_from_year_and_month',
'do_you_know_janasena_principles',
'which_principles_are_you_aware_of',
'participated_jsp_activities',
'how_much_time_can_you_spend_for_party_activities',
'payment_mode',
'payment_id',
'payment_date',
'payment_url',
'payment_status',
'aadhar_card',
'photo',
'added_by',
'dob',
'jsp_id',
'id',
'payment_verified',
'name',
'mobile',
'remarks',
'gender',
'email'
]
    def __repr__(self):
        return '<janasena_kriya_members_dashboard %r>' % self.id