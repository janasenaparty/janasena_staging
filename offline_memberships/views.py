import hashlib
import json
import operator
import uuid

import psycopg2
from flask import Blueprint, request, session, render_template, flash, url_for
from flask_login import current_user, logout_user
from psycopg2.extras import RealDictCursor
from werkzeug.exceptions import abort
from werkzeug.utils import redirect
import config
from appholder import csrf
from dbmodels import OfflineMemberships
from main import sendmailmandrill,sendbulkemails,verifyOtp,find_placeholders
from utilities.classes import Assembly_constituencies_details, encrypt_decrypt,Membership_details,Voter_details
from utilities.decorators import crossdomain,admin_login_required, janasainyam_login_required, required_roles
from utilities.general import checkUserAuth
from utilities.sms import sendBULKSMS,sendBULKSMS_samemessage
from celery_tasks.sms_email import send_bulk_email_operations,send_bulk_sms_excel,send_bulk_sms_operations,send_bulk_sms_manual
from utilities.mailers import genOtp
blueprint_offline_memberships = Blueprint('offline_memberships', __name__)
from utilities.others import generate_membershipid, makeResponse
from flasgger import swag_from

@blueprint_offline_memberships.route("/",methods=["GET","POST"])
@crossdomain(origin="*", headers="Content-Type")
def index():
    # from urlparse import urlparse
    # hostname=urlparse(request.url).hostname
    # districts={}
   
    # if hostname not in ['127.0.0.1','localhost']:
    #     subdomain = urlparse(request.url).hostname.split('.')[0]

    #     if subdomain is not None:
    #         districts=['srikakulam','viziangaram','visakhapatnam','eastgodavari','westgodavari','krishna','guntur','prakasam','nellore','anatapur','kadapa','kurnool','chittoor']
    #         if lower(subdomain) in districts:
    #             try:
    #                 districts['guntur']={'district_name':"Guntur","small_images":
    #                 ["/static/districts/hero-image-01-xs.jpg","/static/districts/hero-image-06-xs.jpg","/static/districts/hero-image-11-xs.jpg"],"medium_images":["/static/districts/hero-image-01.jpg","/static/districts/hero-image-06.jpg","/static/districts/hero-image-11_tel.jpg"]}
    #                 data=districts[lower(subdomain)]
                    
    #                 return render_template('districts/index.html',data=districts['guntur'])
    #             except Exception as e:
    #                 print str(e)
   
    return render_template('JSWebiste/index.html')


@blueprint_offline_memberships.route("/janasainyamLogin",methods=["GET","POST"])
@swag_from("swaggers/janasainyam_login_get.yml",methods=['GET'])
@swag_from("swaggers/janasainyam_login_post.yml",methods=['POST'])
def janasainyamLogin():
    
    if request.method == "POST":
        uname = request.form.get('username')
        password = request.form.get('password')
        utype = 'J'
        
        if checkUserAuth(uname, password, utype):
            
            session['next'] = url_for('offline_memberships.offlineMemberships')
            session['current'] = url_for('offline_memberships.janasainyamLogin')
            session['logged_in'] = True
            
            
            return redirect(url_for('.offlineMemberships'))
        else:
            
            flash('Invalid Details', 'error')
            return redirect(url_for('.janasainyamLogin'))
    else:
        if session.get('logged_in') is not None:
            if session.get('logged_in') == True and session.get('t')=='J':
                
                return redirect(url_for('.offlineMemberships'))
               
        
        return render_template("offline_memberships/login.html")


@blueprint_offline_memberships.route('/forgotJanasainyampass', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@swag_from("swaggers/janasainyam_forgot_password_get.yml",methods=['GET'])
@swag_from("swaggers/janasainyam_forgot_password_post.yml",methods=['POST'])
def forgotJanasainyampass():
    if request.method == "POST":

        email = request.form.get("email")
        
        try:

            registered_user = OfflineMemberships.query.filter_by(email=email.lower()).first()
            
            if registered_user is not None:

                

                uuid_str = str(uuid.uuid4())
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                cur.execute("update janasainyam_team set uuid=%s where lower(user_email)=%s",(uuid_str,str(email).lower()))
                con.commit()
                con.close()

                if (config.ENVIRONMENT_VARIABLE != 'local'):
                    resetlink = "<html>Hi, <br/><br/> Please click on the link below to reset your password:<a href=\'https://janasainyam.janasenaparty.org/resetJanasainyampassword?check=" + uuid_str + "&check=" + email + "\'>Click here to Reset Password</a></html>"
                else:
                    resetlink = "<html>Hi, <br/><br/> Please click on the link below to reset your password:<a href=\'http://127.0.0.1:5000/resetJanasainyampassword?check=" + uuid_str + "&check=" + email + "\'>Click here to Reset Password</a></html>"

                subject = "JanaSena Party Password Reset Link"
                msgToSendEmail = resetlink
                name = "JanaSena Party"
                sendmailmandrill(name, email, resetlink, subject)

                flash("Password Reset Link was sent successfully", 'success')
                return render_template("offline_memberships/forgotpassword.html")
            else:
                flash("Email Not Found", 'error')
                return render_template("offline_memberships/forgotpassword.html")

        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)
            return json.dumps({'status': 'exception occured'})
    else:
        return render_template("offline_memberships/forgotpassword.html")

@blueprint_offline_memberships.route('/resetJanasainyampassword', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")

def resetJanasainyampassword():
    if request.method == 'GET':
        urldata = request.values.getlist('check')

        janasainyam_uuid = urldata[0]
        janasainyam_email = str(urldata[1])
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("select user_email from janasainyam_team where uuid=%s",(janasainyam_uuid,))
            existing=cur.fetchone()
            if existing is not None:
                return render_template("offline_memberships/resetpassword.html")
            else:
                return "It Seems Given Link Expired.please request New one"

        except (psycopg2.Warning, psycopg2.Error) as e:
            return json.dumps({'error': 'exception occured'})

    elif request.method == 'POST':

        urldata = request.values.getlist('check')

        admin_uuid = urldata[0]
        admin_email = str(urldata[1])
        newpassword = request.form.get("newpass")
        print admin_uuid, admin_email, newpassword

        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute("select user_email from janasainyam_team where uuid=%s",(admin_uuid,))
            existing=cur.fetchone()
            if existing is not None:
                unique = 'tomatoPotatoadmin'
                hashed_password = hashlib.sha256(newpassword + unique).hexdigest()
                cur.execute("update janasainyam_team set user_mod_hash=%s,update_dttm=now() where uuid=%s",(hashed_password,admin_uuid))
                con.commit()
                flash("Changed password Successfully", "success")
                return redirect(url_for("offline_memberships.janasainyamLogin"))
            else:
                flash("incorrect Details provided", "error")
                return redirect(url_for("offline_memberships.resetJanasainyampassword"))
            
            
        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)
            return json.dumps({'status': 'exception occured'})

@blueprint_offline_memberships.route('/changePasswordjanasainyam', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required

def changePasswordjanasainyam():
    if request.method == 'POST':

        oldpass = request.values.get('oldpass')
        newpass = request.values.get('newpass')

        if oldpass is None or oldpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_oldpass_MISSING'], 'data': {'status': '0'}})
        if newpass is None or newpass == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_newpass_MISSING'], 'data': {'status': '0'}})

        try:
            print current_user
            admin_id = current_user.id
            print admin_id
            

            unique = 'tomatoPotatoadmin'

            old_hashed_password = hashlib.sha256(oldpass + unique).hexdigest()

            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor()
            cur.execute('select user_mod_hash from janasainyam_team where id=%s', (admin_id,))
            password = cur.fetchone()[0]

            if str(password) == str(unicode(old_hashed_password, errors='ignore')):

                new_HashedPassword = hashlib.sha256(newpass + unique).hexdigest()
                cur.execute('update janasainyam_team  set user_mod_hash=%s,update_dttm=now() where id=%s',
                            (new_HashedPassword, admin_id,))

                con.commit()

                js = json.dumps({'errors': [], 'data': {'status': '1', 'msg': 'Password Changed successfully'}})

                return js

            else:
                js = json.dumps({'errors': ['old password does not match'], 'data': {'status': '0'}})

                return js



        except (psycopg2.Warning, psycopg2.Error) as e:
            print str(e)

            js = json.dumps({'errors': ['Exception Occured'], 'data': {'status': '0'}})
            return js
    else:
        return render_template("offline_memberships/changepassword.html")    

@blueprint_offline_memberships.route('/call_center_form', methods=['POST','GET','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
@csrf.exempt
def call_center_form():
    if request.method=='GET':
        return render_template("offline_memberships/callcenter.html")

@blueprint_offline_memberships.route('/offlineMembershipsissues', methods=['POST','GET','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
@csrf.exempt
def offlineMembershipsissues():
    if request.method=='GET':
        return render_template("offline_memberships/issues.html")
    else:
        try:
            submitted_by=current_user.id
            data= request.json
            name = data["name"]
            issue=data['issue']
            district = data["district"]
            constituency = data["constituency"]
            referral_id = data["referral_id"]
            receipt_no=data['receipt_no']
            place=data['place']
            print data
            if name is None or name=='':
                return json.dumps({"errors":["phone missing"],"data":{"status":"0"}})
            if receipt_no is None or receipt_no=='':
                return json.dumps({"errors":["receipt number missing"],"data":{"status":"0"}})
            if district is None or district=='':
                return json.dumps({"errors":["district missing"],"data":{"status":"0"}})
            if constituency is None or constituency=='':
                return json.dumps({"errors":["constituency missing"],"data":{"status":"0"}})
            if referral_id is None or referral_id=='':
                return json.dumps({"errors":["referral_id missing"],"data":{"status":"0"}})
            if issue is None or issue=='':
                return json.dumps({"errors":["issue missing"],"data":{"status":"0"}})
            referral_id=referral_id.upper()
            membership_instance = Membership_details()
            if not membership_instance.check_membership_existence(referral_id):
                return  json.dumps({"errors":["given incorrect Refeeral ID. please check"],"data":{"status":"0"}})
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("insert into offline_books_issues(referral_id,form_no,name,issue,district_id,constituency_id,place)values(%s,%s,%s,%s,%s,%s,%s)",(referral_id,receipt_no,name,issue,district,constituency,place))
            con.commit()
            con.close()
            return json.dumps({"errors":[],"data":{"status":"1"}})
        except Exception as e:
            print str(e)
            return json.dumps({"errors":["some fields missing"],"data":{"status":"0"}})

@blueprint_offline_memberships.route('/offlineMemberships', methods=['POST','GET','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
@csrf.exempt
def offlineMemberships():
    if request.method=='GET':
        return render_template("offline_memberships/index.html")
    else:
        try:
            submitted_by=current_user.id
            data= request.json
            phone = data['phone']
            voter_id = data["voter_id"]
            email = data["email"]
            
            name = data["name"]
            relation_name=data['relation_name']
            
            age = data["age"]
            gender = data["gender"]

            district = data["district"]
            constituency = data["constituency"]
            state_id = data["state_id"]
            country_name = 'india'
            mandal=data['mandal']
            booth_id=data['booth_id']

            referral_id = data["referral_id"]
            
            fb_id=data['fb_id']
            
            receipt_no=data['receipt_no']
            education=data['education']
            occupation=data['occupation']
            house_no=data['house_no']
            address=data['address']
            pincode=data['pincode']
            village=data['village']
        except Exception as e:
            print str(e)
            return json.dumps({"errors":["some fields missing"],"data":{"status":"0"}})
        if phone is None or phone=='':
            return json.dumps({"errors":["phone number missing"],"data":{"status":"0"}})
        if name is None or phone=='':
            return json.dumps({"errors":["phone missing"],"data":{"status":"0"}})
        if receipt_no is None or receipt_no=='':
            return json.dumps({"errors":["receipt number missing"],"data":{"status":"0"}})
        if district is None or district=='':
            return json.dumps({"errors":["district missing"],"data":{"status":"0"}})
        if constituency is None or constituency=='':
            return json.dumps({"errors":["constituency missing"],"data":{"status":"0"}})
        if age is None or age=='':
            return json.dumps({"errors":["age missing"],"data":{"status":"0"}})
        if gender is None or gender=='':
            return json.dumps({"errors":["gender missing"],"data":{"status":"0"}})
        membership_instance = Membership_details()
        emobile=encrypt_decrypt(phone,'E')
        print emobile
        if membership_instance.check_phonenumber_count(emobile):
            if voter_id is not None:
                evoter_id=encrypt_decrypt(voter_id,'E')
                if membership_instance.check_voterid_existence(evoter_id):

                    voterdetails_instance = Voter_details()
                    if voterdetails_instance.check_existence(str(voter_id).lower()):
                        membership_through='OFV'

                    else:
                       membership_through='OFNF' 

                else:
                    

                    return json.dumps({"errors":["already registered with this voterID"],"data":{"status":"2"}})

                        
            else:
                membership_through='OFW'

            if referral_id is not None and referral_id!='':
                referral_id=referral_id.upper()
                if not membership_instance.check_membership_existence(referral_id):
                    return  json.dumps({"errors":["given incorrect Refeeral ID. please check"],"data":{"status":"0"}})

            if str(state_id) == '1':
                state = "AP"
            elif str(state_id) == '2':
                state = "TS"
            else:
                state = "KA"
            status, membership_id = generate_membershipid(name, phone, voter_id, relation_name, '', house_no, address, age, gender, booth_id,constituency, state, email, '', referral_id, membership_through, country_name='india')
            if status:

                update_member_other_details(membership_id,fb_id,village,mandal,education,occupation,pincode,receipt_no,submitted_by)
                return json.dumps({"errors":[],"data":{"status":"1","membership_id":membership_id}})
                
            else:
                return json.dumps({'errors': ['something wrong.please try again later'], 'data': {'status': '0'}})
        return json.dumps({'errors': ['limit with this Phone Number already reached'], 'data': {'status': '3'}})


@blueprint_offline_memberships.route('/sms_delivery_reports', methods=['GET','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")

@csrf.exempt
def sms_delivery_reports():

    jobno=request.values.get('jobno')
    print request.args
    if jobno is None or jobno=='':
        return json.dumps({"errors":["jobno missing"],"status":"0"})
    status=request.values.get('status')
    if status is None or status=='':
        return json.dumps({"errors":["status missing"],"status":"0"})
    doneTime =request.values.get('DoneTime')
    if doneTime is None or doneTime=='':
        return json.dumps({"errors":["doneTime missing"],"status":"0"})
    print jobno,status,doneTime
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("update member_fields set phone_number_status=%s,doneTime=%s where job_id=%s",(status,str(doneTime),jobno))
        con.commit()
        con.close()
        return json.dumps({"errors":[],"data":{"status":"1"}})

    except Exception as e:
        print str(e)
        return json.dumps({"errors":["something wroong . please try again"],"data":{"status":"0"}})


@blueprint_offline_memberships.route('/edit_offline', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
@csrf.exempt
def edit_offline():
    return render_template("offline_memberships/offline_edit.html")

# @blueprint_offline_memberships.route('/offline_card', methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")

# @csrf.exempt
# def offline_card():
#     from utilities.others import send_membership_card_email
#     send_membership_card_email('rambabu.v68@gmail.com', "JSP39340612","Siva Shankara Vara prasad Mogalipuvvu")
#     return 'success'

@blueprint_offline_memberships.route('/edit_offline_Membership', methods=['POST','GET','OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
@csrf.exempt
def edit_offline_Membership():
    if request.method=='GET':
        membership_id=request.values.get("membership_id")
        if membership_id is None or membership_id=='':
            return json.dumps({'errors': ['Membership ID Missing'], 'data': {'status': '0'}})
        try:
            submitted_by=current_user.id
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            membership_id=str(membership_id).upper()
            cur.execute("""select m.name,m.phone,m.membership_id,m.name,m.relationship_name,
                m.age,m.gender,m.email,m.voter_id,m.state_id,m.district_id,
                m.constituency_id,m.address,m.polling_station_id,
                od.fb_id,od.education,od.occupation,od.village,od.mandal,od.receipt_no,od.submitted_by
                 from janasena_membership m left join member_fields od on od.membership_id=m.membership_id where m.membership_id=%s """,(membership_id,))
            row=cur.fetchone()
            if row is not None:
                #if submitted_by==row['submitted_by']:

                    row['phone']=encrypt_decrypt(row['phone'],'D')
                    if row['email'] is not None:
                        row['email']=encrypt_decrypt(row['email'],'D')
                    if row['voter_id'] is not None:
                        row['voter_id']=encrypt_decrypt(row['voter_id'],'D')
                    return json.dumps({"errors":[],"data":{"status":"1","children":row}})
                # else:
                #     return json.dumps({"errors":["you are not authorized to edit this form"],"data":{"status":"0"}})

            return json.dumps({"errors":["no data found"],"data":{"status":"0"}})
        except psycopg2.Error as e:
            print str(e)
            return json.dumps({"errors":["something wroong . please try again"],"data":{"status":"0"}})
   

    else:

        try:
            submitted_by=current_user.id

            data= request.json
            print data
            membership_id=data['membership_id']
            phone = data['phone']
            voter_id = data["voter_id"]
            email = data["email"]

            name = data["name"]
            relation_name=data['relation_name']
            
            age = data["age"]
            gender = data["gender"]

            district = data["district"]
            constituency = data["constituency"]
            state_id = data["state_id"]
            country_name = 'india'
            mandal=data['mandal']
            

            
            
            fb_id=data['fb_id']
            
            receipt_no=data['receipt_no']
            education=data['education']
            occupation=data['occupation']
            house_no=data['house_no']
            
            pincode=data['pincode']
            village=data['village']
        except Exception as e:
            print str(e)
            return json.dumps({"errors":["some fields missing"],"data":{"status":"0"}})
        if membership_id is None or membership_id=='':
            return json.dumps({"errors":["membership_id missing"],"data":{"status":"0"}})
        if phone is None or phone=='':
            return json.dumps({"errors":["phone number missing"],"data":{"status":"0"}})
        if name is None or phone=='':
            return json.dumps({"errors":["phone missing"],"data":{"status":"0"}})
        if receipt_no is None or receipt_no=='':
            return json.dumps({"errors":["receipt number missing"],"data":{"status":"0"}})
        if district is None or district=='':
            return json.dumps({"errors":["district missing"],"data":{"status":"0"}})
        if constituency is None or constituency=='':
            return json.dumps({"errors":["constituency missing"],"data":{"status":"0"}})
        if age is None or age=='':
            return json.dumps({"errors":["age missing"],"data":{"status":"0"}})
        if gender is None or gender=='':
            return json.dumps({"errors":["gender missing"],"data":{"status":"0"}})
        
        try:
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            ephone=encrypt_decrypt(str(phone),'E')
            membership_instance = Membership_details()
            evoter_id=''
            if membership_instance.check_edit_phonenumber_count(ephone,membership_id):
                if voter_id is not None and voter_id!='':
                    evoter_id=encrypt_decrypt(str(voter_id).lower(),'E')
                    if not membership_instance.check_edit_voterid_existence(evoter_id,membership_id):
                        return json.dumps({"errors":["already someother registered with the given voter id"],"data":{"status":"0"}}) 
                if email is not None and email!='':
                    email=encrypt_decrypt(email,'E')
                print 'updating'
                cur.execute("""update janasena_membership set name=%s,phone=%s,email=%s,voter_id=%s,
                    relationship_name=%s,age=%s,gender=%s,state_id=%s,district_id=%s,
                    constituency_id=%s where membership_id=%s
                    """,(name,ephone,email,evoter_id,relation_name,age,gender,state_id,district,constituency,membership_id))
                con.commit()
                cur.execute("update member_fields set fb_id=%s,education=%s,occupation=%s,village=%s,mandal=%s,pin_code=%s,receipt_no=%s where membership_id=%s",(fb_id,education,occupation,village,mandal,pincode,receipt_no,membership_id))
                con.commit()
                cur.execute("update janasena_missedcalls set phone=%s where  'JSP'||lpad(cast(jsp_supporter_seq_id as text),8,'0')=%s",(phone,membership_id))
                con.commit()
                if voter_id is not None:
                    try:
                        cur.execute("insert into voter_registration_status(voter_id,status)values(%s,%s)", (voter_id, 'T'))
                        con.commit()
                    except psycopg2.Error as e:
                        print str(e)
                con.close()
                
                return json.dumps({"errors":[],"data":{"status":"1"}}) 
            return json.dumps({"errors":["Limit with this phone number already reached"],"data":{"status":"0"}}) 
        except psycopg2.Error as e:
            print str(e)
            return json.dumps({"errors":["something wrong"],"data":{"status":"0"}}) 
        

        
def update_member_other_details(membership_id,fb_id,village,mandal,education,occupation,pincode,receipt_no,submitted_by):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("insert into member_fields(membership_id,fb_id,education,occupation,village,mandal,pin_code,receipt_no,submitted_by)values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(membership_id,fb_id,education,occupation,village,mandal,pincode,receipt_no,submitted_by))
        con.commit()
        con.close()

    except psycopg2.Error as e:
        print str(e)
        return 'false'



@blueprint_offline_memberships.route('/janasainyamlogout', methods=[ 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@janasainyam_login_required
def janasainyamlogout():
    logout_user()
    session.clear()
    return redirect(url_for('offline_memberships.janasainyamLogin'))


@blueprint_offline_memberships.route('/getJanasainyamTeam', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
@required_roles(["AD","MB","OP"])
def getJanasainyamTeam():
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(
            "select j.id,j.user_email,j.first_name,j.last_name,j.mobile,j.status,ac.district_name,ac.constituency_name from janasainyam_team j join Assembly_constituencies  ac on ac.district_id=j.district_id and ac.constituency_id=j.constituency_id order by j.id ")
        team = cur.fetchall()
        return render_template("offline_memberships/janasainyam_team.html", team=team)
    except psycopg2.Error as e:
        print str(e)
        return str(e)


@blueprint_offline_memberships.route('/createJanasainyamUser', methods=['POST', 'GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def createJanasainyamUser():
    if request.method == 'POST':

        uname = request.values.get('uname')
        
        first_name = request.values.get('first_name')
        last_name = request.values.get('last_name')

        district_id = request.values.get('district_id')
        constituency_id=request.values.get('constituency_id')
        mobile = request.values.get('mobile')

        if uname is None or uname == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_uname_MISSING'], 'data': {'status': '0'}})
        if district_id is None or district_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_district_id_MISSING'], 'data': {'status': '0'}})
        if first_name is None or first_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_first_name_MISSING'], 'data': {'status': '0'}})
        if last_name is None or last_name == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_last_name_MISSING'], 'data': {'status': '0'}})
        if mobile is None or mobile == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_mobile_MISSING'], 'data': {'status': '0'}})
        if constituency_id is None or constituency_id == "":
            return json.dumps({'errors': ['BAD_REQUEST', 'PARAMETER_constituency_id_MISSING'], 'data': {'status': '0'}})
        
        else:
            try:
                con = None
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor()
                admin_id = current_user.id
                

                email = str(uname).lower()

                
                cur.execute("select * from janasainyam_team where mobile=%s or lower(user_email)=%s", (mobile, email))
                row = cur.fetchone()
                if row is None:
                    admin_id = current_user.id
                    uuid_str = str(uuid.uuid4())
                    cur.execute(
                        'insert into janasainyam_team(user_email,first_name,last_name,mobile,status,district_id,constituency_id,created_by,uuid) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        (str(email),str(first_name),str(last_name),mobile,'A', district_id,constituency_id,admin_id,uuid_str))
                    con.commit()

                    
                
                    con.close()

                    if (config.ENVIRONMENT_VARIABLE != 'local'):
                        resetlink = "<html>Hi "+str(first_name)+" , <br/><br/>JanaSena Party has given you access. Please click on the link below to create password:<a href=\'https://janasainyam.janasenaparty.org/resetJanasainyampassword?check=" + uuid_str + "&check=" + email + "\'>Click here to Create Password</a></html>"
                    else:
                        resetlink = "<html>Hi "+str(first_name)+" , <br/><br/>JanaSena Party has given you access. Please click on the link below to create  password:<a href=\'http://127.0.0.1:5000/resetJanasainyampassword?check=" + uuid_str + "&check=" + email + "\'>Click here to Create Password</a></html>"

                    subject = "JanaSena Party Password Creation Link"
                    msgToSendEmail = resetlink
                    name = "JanaSena Party"
                    sendmailmandrill(name, email, resetlink, subject)



                    return json.dumps({'errors': [], 'data': {'status': '1', 'msg': "inserted successfully"}})
                else:
                    return json.dumps({'errors': ['Email or Mobile already Existed'], 'data': {'status': '0'}})


            except psycopg2.Error as e:
                print str(e)
                return json.dumps({'errors': ['BAD_REQUEST', 'EXCEPTION OCCURRED'], 'data': {'status': '0'}})

    else:
        try:
            
            return render_template("offline_memberships/create_janasainyam.html")
        except psycopg2.Error as e:
            print str(e)
            return str(e)
#