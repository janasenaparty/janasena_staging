import requests
import psycopg2
from psycopg2.extras import RealDictCursor
import config
from bs4 import BeautifulSoup
import requests
import os
import pdfkit
path="e:/local_bodies_voters_files/"

def download_voters():
	con = psycopg2.connect(config.DB_CONNECTION)              
	cur = con.cursor(cursor_factory=RealDictCursor)
	cur.execute("select id,district_name from ts_new_districts ")
	districts=cur.fetchall()
	for district in districts:
		print district['district_name'] +" started"
		district_id=str(district['id']).zfill(2)
		district_name=district['district_name']
		district_path=path+district_name+"/"
		if not os.path.exists(district_path):
			os.makedirs(district_path)
		cur.execute("select id,mandal_name from ts_new_mandals where district_id=%s",(district['id'],))
		mandals=cur.fetchall()
		for mandal in mandals:
			mandal_id=str(mandal['id']).zfill(2)
			mandal_name=mandal['mandal_name']
			mandal_path=district_path+mandal_name+"/"
			if not os.path.exists(mandal_path):
				os.makedirs(mandal_path)
			mptcs=get_mptc_ids(district_id,mandal_id)
			for mptc in mptcs:
				mptc_name=mptc['mptc_name']
				mptc_id=str(mptc['mptc_id']).zfill(2)
				mptc_path=mandal_path+mptc_name+"/"
				if not os.path.exists(mptc_path):
					os.makedirs(mptc_path)
				gpids=get_gp_ids(district_id,mandal_id,mptc_id)
				
				for gp in gpids:
					gp_id=str(gp['gp_id']).zfill(2)
					gp_name=gp['gp_name']
					gp_path=mptc_path+gp_name+"/"
					if not os.path.exists(gp_path):
						os.makedirs(gp_path)
					url = "https://tsec.gov.in/pswisereportmptc.se"

					querystring = {"mode":"getPSWiseDataForCandiadatePortal","captchanew":"9%204%20%204%208","property(mptc_gp_ward)":"MGP","property(district_id)":str(district_id),"property(mandal_id)":str(mandal_id),"property(mptc_id)":str(mptc_id),"property(gpcode)":str(gp_id),"inputcaptcha":"9449","property%28gpcode%29":str(gp_id),"property%28mptc_id%29":str(mptc_id),"property%28mandal_id%29":str(mandal_id),"property%28district_id%29":str(district_id),"property%28mptc_gp_ward%29":"MGP"}
					
					payload = ""
					headers = {
						'JSESSIONID': "D27E27703D65B1F5666D117BD2BAA770",
						'cache-control': "no-cache",
						'Postman-Token': "4ff60eb8-2a26-4b92-b0e7-db7885bd160f"
						}

					response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
					pdf_files_paths=get_pdf_files_path(response.text)
					
					download_files(pdf_files_paths,gp_path)

def download_files(pdf_files_paths,file_path):
	baseurl='https://tsec.gov.in/elecrollmptc/'
	options = {
    'page-size': 'Letter',
    'margin-top': '0in',
    'margin-right': '0.5in',
    'margin-bottom': '0in',
    'margin-left': '0.5in',
    'encoding': "UTF-8",
    'dpi':'72',
    'no-outline': None
   
        }
	
	for pdf_path in pdf_files_paths:
		file_name=pdf_path['ps_name']+"_"+str(pdf_path['ps_no'])
		
		reg_url=baseurl+pdf_path['reg']
		supl_url=baseurl+pdf_path['supl']
		r = requests.get(reg_url, stream = True)
		reg_file_path=file_name+"_regular.pdf"
		reg_file_path=file_path+reg_file_path
		
		pdfkit.from_string(r.text, reg_file_path,options=options)
		 

		print reg_file_path +' completed'
		r = requests.get(reg_url, stream = True)
		supl_file_path=file_name+"_supl.pdf"
		supl_file_path=file_path+supl_file_path
		pdfkit.from_string(r.text, supl_file_path,options=options)

		

		print supl_file_path +' completed'


def get_pdf_files_path(data):
	soup = BeautifulSoup(data)
	table = soup.find("table", {"id": "GridView1"})
	pdf_paths=[]
	
	rows = table.find('tbody').findAll('tr')

	for row in rows:
		d={}
		cols=row.find_all('td')
		
		d['ps_no']=cols[1].text
		d['ps_name']=cols[2].text
		d['ps_address']=cols[3].text
		
		
		
		d['reg']=(str(cols[4].find('input')['onclick'])[11:])[:-2]
		d['supl']=(str(cols[5].find('input')['onclick'])[11:])[:-2]
		
		pdf_paths.append(d)
	return pdf_paths

def get_mptc_ids(district_id,mandal_id):



	url = "https://tsec.gov.in/pswisereportmptc.do"

	querystring = {"mode":"getMPTC","district_id":str(district_id),"mandal_id":str(mandal_id)}

	payload = ""
	headers = {
		'JSESSIONID': "D27E27703D65B1F5666D117BD2BAA770",
		'cache-control': "no-cache",
		'Postman-Token': "d62ff41d-3cdc-4203-8184-eb27e594949d"
		}

	response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

	data = response.text
	mptc_ids=[]
	soup = BeautifulSoup(data)
	for option in soup.find_all('option'):
		
		mptc_id=option['value']
		mptc_name=option.text
		if str(mptc_id)!='0':
			d={}
			d['mptc_id']=mptc_id
			d['mptc_name']=mptc_name
			mptc_ids.append(d)

	return mptc_ids

def get_gp_ids(district_id,mandal_id,mptc_id):


	url = "https://tsec.gov.in/pswisereportmptc.do"

	querystring = {"mode":"getGramPanchayat","district_id":str(district_id),"mandal_id":str(mandal_id),"mptc_id":str(mptc_id)}

	payload = ""
	headers = {
		'JSESSIONID': "D27E27703D65B1F5666D117BD2BAA770",
		'cache-control': "no-cache",
		'Postman-Token': "da8c8a5d-5ed3-4d3e-9118-d38247bea6cb"
		}

	response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

	gp_ids=[]
	soup = BeautifulSoup(response.text)
	for option in soup.find_all('option'):
		
		gp_id=option['value']
		gp_name=option.text
		if str(gp_name)!='---select---':
			d={}
			d['gp_id']=gp_id
			d['gp_name']=gp_name
			gp_ids.append(d)

	return gp_ids

download_voters()