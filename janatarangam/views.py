from flask import Blueprint, json, request, render_template, send_file
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate,Table as Tbl
from sqlalchemy import create_engine, Table, select, cast, Date, func, case, MetaData, String, desc, and_, or_, Column, \
    Integer, insert
from sqlalchemy.exc import CompileError
from sqlalchemy.sql.functions import count

import config
from appholder import csrf
from utilities.decorators import crossdomain, admin_login_required

janatarangam = Blueprint('janatarangam', __name__)

@janatarangam.route('/get_jt_details', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def get_jt_details():
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData()

    missed_calls2 = Table('janasena_missedcalls2', metadata, autoload=True, autoload_with=engine)
    missed_calls = Table('janasena_missedcalls', metadata, autoload=True, autoload_with=engine)
    membership = Table('janasena_membership', metadata, autoload=True, autoload_with=engine)


    """
    update janasena_missedcalls set member_through='JT' WHERE phone in (select distinct(jm2.phone) from janasen
    a_missedcalls2 jm2 
    join janasena_missedcalls jm on jm.phone=jm2.phone where jm2.create_dttm<jm.create_dttm and jm.membe
    r_through='M');
    """





    sub = select([missed_calls.c.phone.distinct(),cast(cast(missed_calls2.c.update_dttm,Date),String).label('date')
                   ,missed_calls.c.supporter_id.label('supporter_id'),missed_calls.c.phone.label('p1'),
                  missed_calls.c.member_through.label('member_through')])
    sub = sub.select_from(
        missed_calls2.join(missed_calls, missed_calls.c.phone == missed_calls2.c.phone)

    )

    sub = sub.alias('subquery')
    main_stmt = select([sub.c.date.label('date'),
                        func.sum(
                            case(
                                [
                                    (and_(membership.columns.membership_id == None,
                                          sub.c.member_through =='JT'), 1),

                                ],
                                else_=0
                            )
                        ).label('jt_missedcalls'),
                        func.sum(
                            case(
                                [
                                    (and_(membership.columns.membership_id != None,
                                          sub.c.member_through == 'JT'), 1),

                                ],
                                else_=0
                            )
                        ).label('jt_members'),
                        func.sum(
                            case(
                                [
                                    (and_(membership.columns.membership_id == None,
                                          sub.c.member_through != 'JT',
                                          sub.c.member_through == 'M'), 1),

                                ],
                                else_=0
                            )
                        ).label('prev_missedcalls'),

                        ])
    main_stmt = main_stmt.select_from(
        sub.join(membership, membership.c.membership_id == sub.c.supporter_id,isouter=True)
    )

    main_stmt = main_stmt.group_by(sub.c.date)
    main_stmt = main_stmt.order_by(desc(sub.c.date))

    total = select([cast(cast(missed_calls2.c.update_dttm,Date),String).label('date'),
                    count(missed_calls2.c.jsp_supporter_seq_id).label('total')])
    total = total.select_from(
        missed_calls2
    )
    total = total.group_by(cast(cast(missed_calls2.c.update_dttm,Date),String).label('date'))
    total = total.order_by(desc(cast(cast(missed_calls2.c.update_dttm,Date),String).label('date')))
    print(str(main_stmt))
    with engine.connect() as connection:
        result_proxy = connection.execute(main_stmt).fetchall()
        list_of_dicts = [{key: value for (key, value) in row.items()} for row in result_proxy]
        # print(list_of_dicts)
        result_proxy = connection.execute(total).fetchall()
        tt = [{key: value for (key, value) in row.items()} for row in result_proxy]
        return json.dumps({'errors':[],'data':list_of_dicts,'total':tt})

@janatarangam.route('/missed_call_member', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def missed_call_member():
    return render_template('janatarangam/missed_call_member.html')

@janatarangam.route('/observers', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@admin_login_required
def observers():
    return render_template('janatarangam/observers.html')

@janatarangam.route('/total_received_nt_counts', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def total_received_nt_counts():
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData()
    observers = Table('party_observers', metadata, autoload=True, autoload_with=engine)
    stmt = select([cast(observers.columns.observer_type,String).label('region_type'),
                   func.sum(
                       case(
                           [
                               (func.upper(observers.columns.material_status) != None, 1),

                           ],
                           else_=0
                       )).label('total'),
                   func.sum(
                       case(
                           [
                               (func.upper(observers.columns.material_status) == 'NO', 1),

                           ],
                           else_=0
                       )
                   ).label('no'),
                   func.sum(
                       case(
                           [
                               (func.upper(observers.columns.material_status) == 'YES', 1),

                           ],
                           else_=0
                       )
                   ).label('yes')
                   ])
    stmt = stmt.select_from(observers)
    stmt = stmt.where(observers.c.state_id == 1)
    stmt = stmt.group_by(observers.columns.observer_type)
    with engine.connect() as connection:
        result_proxy = connection.execute(stmt).fetchall()
        list_of_dicts = [{key: value for (key, value) in row.items()} for row in result_proxy]
        # print(list_of_dicts)
        return json.dumps({'errors':[],'data':list_of_dicts})


@janatarangam.route('/received_nt_counts',methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
def received_nt_counts():
    area_type = request.values.get('area_type')
    status_type = request.values.get('status_type')

    at_map = {
        "ac":'CONSTITUENCY_OBS',
        'mandal':'MANDAL_OBS'
    }

    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData()
    observers = Table('party_observers', metadata, autoload=True, autoload_with=engine)
    mandals = Table('mandals', metadata, autoload=True, autoload_with=engine)
    assembly = Table('assembly_constituencies', metadata, autoload=True, autoload_with=engine)

    stmt = select([observers.c.id.distinct(),assembly.c.constituency_name,
                   mandals.c.mandal_name,
                   observers.c.obs_name,observers.c.phone,
                   observers.c.material_status,
                   observers.c.remarks
                   ])
    stmt = stmt.select_from(
        observers.join(assembly,and_(assembly.c.constituency_id == observers.c.constituency_id,assembly.c.state_id==1),isouter=True)
        .join(mandals, mandals.c.id == observers.c.mandal_id,isouter=True)

    )

    stmt = stmt.where(and_(observers.columns.observer_type == at_map[area_type],
                           observers.c.material_status == status_type))

    stmt.order_by(observers.columns.id)
    # print(stmt)
    with engine.connect() as connection:
        result_proxy = connection.execute(stmt).fetchall()
        list_of_dicts = [{key: value for (key, value) in row.items()} for row in result_proxy]
        # print(list_of_dicts)
        return json.dumps({'errors':[],'data':list_of_dicts})


@janatarangam.route('/jt_sv_dt',methods=['POST','OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Access-Control-Allow-Origin"])
@csrf.exempt
def jt_sv_dt():
   
    data = request.json
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData()


    jt_data = Table('jt_data', metadata, autoload=True, autoload_with=engine)

    try:
        with engine.connect() as connection:
            connection.execute(jt_data.insert().values(data))
            return json.dumps({'status':1})
    except (CompileError,Exception) as e:
        print(e)
        return json.dumps({'status':0,'error':"Send correct parameters"})

@janatarangam.route('/', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt

def jt_sv_dt_tmp():
    return render_template('janatarangam/active_members.html')


@janatarangam.route('/observer_count_pdf', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def observer_count_pdf():
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData()
    pc = Table('parliament_constituencies', metadata, autoload=True, autoload_with=engine)
    ppmw = Table('pickup_points_mandal_wise', metadata, autoload=True, autoload_with=engine)
    ppcw = Table('pickup_points_constituency_wise', metadata, autoload=True, autoload_with=engine)
    observers = Table('party_observers', metadata, autoload=True, autoload_with=engine)
    mandals = Table('mandals', metadata, autoload=True, autoload_with=engine)
    assembly = Table('assembly_constituencies', metadata, autoload=True, autoload_with=engine)




    ynsm = select([ppmw.c.parliament_constituency_name.label('pc_name'),
                   ppmw.c.pickup_point_name.label('pickup_point_name'),
                   func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'YES', 1),

                       ],
                       else_=0
                   )).label('yes'),
                   func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'NO', 1),

                       ],
                       else_=0
                   )).label('no'),
                   func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'NULL', 1),

                       ],
                       else_=0
                   )).label('unanswered')
                   ])

    ynsm = ynsm.select_from(
        ppmw.join(observers,ppmw.c.mandal_id == observers.c.mandal_id)
        .join(pc, pc.c.parliament_constituency_name == ppmw.c.parliament_constituency_name)
    )
    ynsm = ynsm.where(observers.c.observer_type == "MANDAL_OBS")
    ynsm = ynsm.group_by(ppmw.c.parliament_constituency_name,ppmw.c.pickup_point_name,observers.columns.material_status)
    ynsm = ynsm.alias('m')

    ynsac = select([ppcw.c.parliament_constituency_name.label('pc_name'),
                    ppcw.c.pickup_point_name.label('pickup_point_name'),
                   func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'YES', 1),

                       ],
                       else_=0
                   )).label('yes'),
                    func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'NO', 1),

                       ],
                       else_=0
                   )).label('no'),
                    func.sum(case(
                       [
                           (func.upper(observers.columns.material_status) == 'NULL', 1),

                       ],
                       else_=0
                   )).label('unanswered')
                   ])

    ynsac = ynsac.select_from(
        ppcw.join(observers, ppcw.c.constituency_id == observers.c.constituency_id)
            .join(pc, pc.c.parliament_constituency_name == ppcw.c.parliament_constituency_name)
    )
    ynsac = ynsac.where(observers.c.observer_type == "CONSTITUENCY_OBS")
    ynsac = ynsac.group_by(ppcw.c.parliament_constituency_name,ppcw.c.pickup_point_name,observers.columns.material_status,)
    ynsac = ynsac.alias('ac')

    stmt = select([ynsm.c.pc_name.label('pc_name'),ynsm.c.pickup_point_name.label('pickup_point_name'),
                  ynsm.c.yes.label('mandal_yes'),
                   ynsm.c.no.label('mandal_no'),
                   ynsm.c.unanswered.label('mandal_unanswered'),
                   ynsac.c.yes.label('ac_yes'),
                   ynsac.c.no.label('ac_no'),
                   ynsac.c.unanswered.label('ac_unanswered')
                   ])
    stmt = stmt.select_from(
        ynsm.join(ynsac,and_(func.lower(ynsm.c.pc_name) == func.lower(ynsac.c.pc_name),
                       func.lower(ynsm.c.pickup_point_name) == func.lower(ynsac.c.pickup_point_name)),isouter=True)
    )
    stmt = stmt.order_by(ynsm.c.pc_name,ynsm.c.pickup_point_name)
    a = (A4[1],A4[0])
    doc = SimpleDocTemplate("simple_table_grid.pdf", pagesize=a)
    m = []
    row_count = 0
    with engine.connect() as connection:
        pc_proxy = connection.execute(stmt)
        all_pcs = pc_proxy.fetchall()

        all_pcs = [[str(x).strip() for x in a] for a in all_pcs]
        all_pcs.insert(0,["Parliament Constituency","Pickup Point","Mandal Yes","Mandal No","Mandal Unanswered",
                          "Assembly Yes","Assembly No","Assembly Unanswered"])
        print(all_pcs)
        t = Tbl(all_pcs,repeatCols=0,colWidths=[None,None,None,None,None,None,None,None])
        m.append(t)

        #get individual assemblies with count
        # get individual assembly details
    doc.build(m)


    return json.dumps({"status":0})


@janatarangam.route('/jsp_mandal_team', methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def new_data_form():
    return render_template()