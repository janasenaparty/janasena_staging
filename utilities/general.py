import random
import string
import time
import uuid

import psycopg2
from azure.storage.blob import PublicAccess
from flask import session
from flask_login import login_user, current_user
from psycopg2.extras import RealDictCursor
from reportlab.platypus import Paragraph
from werkzeug.utils import secure_filename

import config
from appholder import block_blob_service,app
from dbmodels import Admin, Memberships, Frontdesk, OfflineMemberships, ShatagniMagazine, ACHead
from main import genHash
from utilities.classes import Membership_details

from hashids import Hashids
 
def create_hashid(id):
    hashids = Hashids(min_length=5, salt=app.secret_key,alphabet='abcdefghijklmnopqrstuvwxyz')
    hashid = hashids.encode(id)
    return hashid
 
def decode_hashid(hashid):
    hashids = Hashids(min_length=5, salt=app.secret_key,alphabet='abcdefghijklmnopqrstuvwxyz')
    id = hashids.decode(hashid)
    return id

def checkUserAuth(uname, password, utype):
    if utype == 'A':
        unique = "tomatoPotatoadmin"

        registered_user = Admin.query.filter_by(email=uname, hashed_password=genHash(password, unique)).first()

        if registered_user is None:
            return False
        session.clear()
        login_user(registered_user)

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        mhash = uuid.uuid4().hex
        cur.execute("update session_admin set status=%s where admin_id=%s and status=%s",
                    ('E', registered_user.id, 'A'))
        con.commit()

        cur.execute(
            'insert into session_admin(session_val,create_dttm,update_dttm,admin_id,status) values(%s,now(),now(),%s,%s) returning session_id',
            (mhash, registered_user.id, 'A'))
        cursession = cur.fetchone()

        session['session'] = cursession[0]
        con.commit()
        print current_user.is_authenticated()

        session['t'] = 'A'

        return True
    elif utype == 'M':
        registered_user = Memberships.query.filter_by(membership_id=str(uname).upper(), member_pin=password).first()
        session.clear()
        if registered_user is None:
            return False

        login_user(registered_user)

        print current_user.is_authenticated()

        session['t'] = 'M'

        return True
    elif utype == 'F':
        unique = "tomatoPotatoadmin"

        registered_user = Frontdesk.query.filter_by(email=uname, hashed_password=genHash(password, unique)).first()
        print registered_user
        if registered_user is None:
            return False
        session.clear()
        login_user(registered_user)

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        mhash = uuid.uuid4().hex

        cur.execute(
            'insert into session_frontdesk_team(session_val,create_dttm,update_dttm,id) values(%s,now(),now(),%s) returning session_id',
            (mhash, registered_user.id))
        cursession = cur.fetchone()

        session['session'] = cursession[0]
        con.commit()
        print current_user.is_authenticated()
        print "current user statusss"
        session['t'] = 'F'

        return True
    elif utype == 'J':
        unique = "tomatoPotatoadmin"

        registered_user = OfflineMemberships.query.filter_by(email=uname, hashed_password=genHash(password, unique)).first()
        print registered_user
        if registered_user is None:
            return False
        session.clear()
        login_user(registered_user)

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        mhash = uuid.uuid4().hex

        # cur.execute(
        #     'insert into session_frontdesk_team(session_val,create_dttm,update_dttm,id) values(%s,now(),now(),%s) returning session_id',
        #     (mhash, registered_user.id))
        # cursession = cur.fetchone()

        # session['session'] = cursession[0]
        con.commit()
        con.close()
        print current_user.is_authenticated()
        print "current user statuss"
        session['t'] = 'J'

        return True
    elif utype == 'SM':
        unique = "tomatoPotatoadmin"

        registered_user = ShatagniMagazine.query.filter_by(phone=uname, pin=genHash(password, unique)).first()
        print registered_user
        if registered_user is None:
            return False
        session.clear()
        login_user(registered_user)

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        mhash = uuid.uuid4().hex
        cur.execute("update shatagni_magazine_session set action=%s where id=%s and action=%s",
                    ('E', registered_user.id, 'L'))
        con.commit()
        cur.execute(
            'insert into shatagni_magazine_session(id,phone,session_id,action) values(%s,%s,%s,%s) returning session_id',
            (registered_user.id,uname,mhash,'L'))
        cursession = cur.fetchone()

        session['session'] = cursession[0]
        con.commit()
        con.close()
        print current_user.is_authenticated()
        print "current user statuss"
        session['t'] = 'SM'

        return True
    elif utype in ['CL','CLA','CLD']:
        registered_user = ACHead.query.filter_by(phone=uname).first()
        print(registered_user)
        if registered_user is None:
            return False

        session.clear()

        login_user(registered_user)

        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        mhash = uuid.uuid4().hex

        cur.execute(
            'insert into session_admin(session_val,create_dttm,update_dttm,admin_id,status) values(%s,now(),now(),%s,%s) returning session_id',
            (mhash, registered_user.id, registered_user.utype))
        cursession = cur.fetchone()

        session['session'] = cursession[0]
        con.commit()
        print("IN user auth")
        print current_user.is_authenticated()
        print(registered_user.district_id,registered_user.constituency_id)
        session['t'] = registered_user.utype
        if registered_user.constituency_id:
            session['constituency_id'] = registered_user.constituency_id
        session['state_id'] = registered_user.state_id
        if registered_user.district_id is not None:
            session['district_id'] = registered_user.district_id

        return True

    else:
        return False


def some_random_string():
	import os, random, string

	length = 13
	chars = string.ascii_letters + string.digits + '!@#$%^&*()'
	random.seed = (os.urandom(1024))
	rand_string=''.join(random.choice(chars) for i in range(length))
	print rand_string
	return rand_string
def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = some_random_string()
    return session['_csrf_token']

def id_generator(size=32, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def update_membership_targets(membership_id, referral_id):
    try:
        con = psycopg2.connect(app.config["DB_CONNECTION"])
        cur = con.cursor()
        cur.execute("insert into membership_targets(membership_id)values(%s)", (membership_id,))
        if referral_id is not None and referral_id != '':
            cur.execute(
                "update membership_targets set achievement=achievement+1,update_dttm=now() where membership_id=%s ",
                (referral_id,))
        con.commit()
        con.close()

    except (psycopg2.Error, Exception) as e:
        print str(e)

def upload_image_custom(file, container,filename):
    try:
        filename = secure_filename(filename)
        Randomfilename = id_generator()
        filename = Randomfilename +'_'+filename


        try:
            container_name = container
            container = block_blob_service.create_container(container_name)
            if container:
                print 'Container %s created' % (container_name)
                block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)
            else:
                print 'Container %s exists' % (container_name)
                block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)
            print("inside upload image")
            filename = secure_filename(filename)
            block_blob_service.create_blob_from_stream(container_name=container_name, blob_name=filename, stream=file)

            attachment = 'https://janasenabackup.blob.core.windows.net/' + container_name + '/' + filename
            print(attachment)
            return attachment
        except Exception as e:
            print("Heer")
            print 'Exception=' + str(e)
            return ''


    except Exception as e:
        print str(e)
        return ""



def upload_image(file, container):
    try:
        filename = secure_filename(file.filename)
        timestr = time.strftime("%Y%m%d-%H%M%S")
        fileextension = filename.rsplit('.', 1)[1]
        Randomfilename = id_generator()
        
        filename = Randomfilename + timestr+'.' + fileextension

        try:
            block_blob_service.create_blob_from_stream(container, filename, file)
            attachment = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            return attachment
        except Exception as e:
            print 'Exception=' + str(e)
            return ''


    except Exception as e:
        print str(e)
        return ""

def upload_image_web(file, container):
    try:

        timestr = time.strftime("%Y%m%d-%H%M%S")
        filename = timestr
        fileextension = 'png'
        Randomfilename = id_generator()

        filename = str(Randomfilename) + str(filename) + '.' + fileextension

        attachment = ''
        try:
            block_blob_service.create_blob_from_stream(container, filename, file)
            attachment = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
        except Exception as e:
            print 'Exception=' + str(e)

        return attachment
    except Exception as e:
        print str(e)
        return "False"

def updatevolunteerstatus(email, membership_id):
    try:

        session['card_details']['volunteer_status'] = 'T'

    except (KeyError, Exception) as e:
        print str(e)
        return False

    errors = []

    try:

        membership_instance = Membership_details()

        membership_instance.check_and_update_volunteer_status(membership_id, email)
        # if config.ENVIRONMENT_VARIABLE!="local":
        #     data=json.dumps({"membership_id":membership_id,"email":email})
        #     addmailsToRedis(data)

        return True

    except Exception as e:
        print str(e)
        return False

def get_payment_request_details(order_id):
    try:
        import requests


        headers = { "X-Api-Key": config.insta_mojo_key, "X-Auth-Token": config.insta_mojo_secret}
        response = requests.get(
        "https://www.instamojo.com/api/1.1/payment-requests/"+order_id+"/",
            headers=headers)
        return response.text

    except Exception as e:
        print str(e)
def get_payment_details(payment_id):
    try:
        import requests

        headers = {"X-Api-Key": config.insta_mojo_key, "X-Auth-Token": config.insta_mojo_secret}
        response = requests.get(
            "https://www.instamojo.com/api/1.1/payments/" + payment_id + "/",
            headers=headers)

        print response.text
        return response.text
    except Exception as e:
        print str(e)
        return ''

def getAllOfficeLocations():
    rows = []
    try:
        con = None
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select seq_id,office_name from jsp_office_locations where status=%s", ("A",))
        rows = cur.fetchall()
        con.close()
        return rows

    except psycopg2.Error as e:
        print str(e)
        return []


class CustomPara(Paragraph):
    def wrap(self, availWidth, availHeight):
        # work out widths array for breaking
        self.width = availWidth
        style = self.style
        leftIndent = style.leftIndent
        first_line_width = availWidth - (leftIndent+style.firstLineIndent) - style.rightIndent
        later_widths = availWidth - leftIndent - style.rightIndent
        self._wrapWidths = [first_line_width, later_widths]
        if style.wordWrap == 'CJK':
            #use Asian text wrap algorithm to break characters
            blPara = self.breakLinesCJK(self._wrapWidths)
        else:
            blPara = self.breakLines(self._wrapWidths)
        self.blPara = blPara
        length = 4
        autoLeading = getattr(self,'autoLeading',getattr(style,'autoLeading',''))
        leading = style.leading
        if len(blPara.lines) > 4:
            extra = len(blPara.lines) - 4
            blPara.lines = blPara.lines[0:4]
        if blPara.kind==1:
            if autoLeading not in ('','off'):
                height = 0
                if autoLeading=='max':
                    for l in blPara.lines:
                        height += max(l.ascent-l.descent,leading)
                elif autoLeading=='min':
                    for l in blPara.lines:
                        height += l.ascent - l.descent
                else:
                    raise ValueError('invalid autoLeading value %r' % autoLeading)
            else:
                height = length * leading
        else:
            if autoLeading=='max':
                leading = max(leading,blPara.ascent-blPara.descent)
            elif autoLeading=='min':
                leading = blPara.ascent-blPara.descent
            height = length * leading
        self.height = height
        return self.width, height