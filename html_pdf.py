from flask import Flask, render_template
app = Flask(__name__)


class Pdf():

    def render_pdf(self, name, html):

        from xhtml2pdf import pisa
        from StringIO import StringIO

        pdf = StringIO()

        pisa.CreatePDF(StringIO(html), pdf)

        return pdf.getvalue()

import tempfile
from flask import make_response
@app.route('/invoice',  methods=['GET'])
def view_invoice():

    
    html = render_template(
        'pdf.html')
    file_class = Pdf()
    pdf = file_class.render_pdf('janasenaparty', html)
    handle, filepath = tempfile.mkstemp(suffix='.pdf')
    with open(filepath, 'wb') as f:
        f.write(pdf)
    return_file = open(filepath, 'r')
    response = make_response(return_file.read(),200)
   
    response.headers['Content-Description'] = 'File Transfer'
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Content-Type'] = 'application.pdf'
    response.headers['Content-Disposition'] = 'attachment; filename=certificate.pdf' 
    
    return response 
@app.route('/html_pdf',  methods=['GET'])
def html_pdf():

    import pdfkit
    handle, filepath = tempfile.mkstemp(suffix='.pdf')
    with open('./templates/pdf.html') as f:
        pdfkit.from_file(f, filepath)
    return_file = open(filepath, 'r')
    response = make_response(return_file.read(),200)
   
    response.headers['Content-Description'] = 'File Transfer'
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Content-Type'] = 'application.pdf'
    response.headers['Content-Disposition'] = 'attachment; filename=certificate.pdf' 
    
    return response    

if __name__ == '__main__':
    app.run(debug=True)