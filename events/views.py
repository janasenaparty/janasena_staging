import json
import psycopg2

from flask import Blueprint, request
from psycopg2.extras import RealDictCursor

import config

from appholder import csrf, db
from dbmodels import Memberships, member_session
from main import verifyOtp, saveSession
from utilities.classes import encrypt_decrypt, Membership_details
from utilities.decorators import crossdomain, requires_EventsauthToken,requires_api_token
from utilities.mailers import genOtp
from utilities.others import makeResponse

blueprint_event = Blueprint('event', __name__)


@blueprint_event.route("/api/GuestLogin/verifyOTP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verifyGuestLoginOTP():
    try:
        try:

            mobile = request.json['Mobile']
            otp = request.json['Otp']

        except (Exception, KeyError) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

        if 10 <= len(str(mobile)) <= 15:

            if otp is None or len(str(otp)) == 6:

                if verifyOtp(mobile, otp, "GL"):
                    return makeResponse(json.dumps({"authentication": "1", "message": "Verified Successfully"}), 200,
                                        'application/json')
                return makeResponse(json.dumps({"authentication": "0", "message": "Login Failed"}), 200,
                                    'application/json')
            return makeResponse(json.dumps({"errors": ["invalid otp"]}), 400, 'application/json')
        return makeResponse(json.dumps({"errors": ["invalid Mobile"]}), 400, 'application/json')

    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')


@blueprint_event.route("/api/Login/forgotpassword", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def memberForgotpin():
    if request.method == 'GET':
        try:

            MembershipID = request.values.get('MembershipID')

            if MembershipID is None or MembershipID.isspace():
                return makeResponse(json.dumps({"errors": ["MembershipID should not be empty'"]}), 400,
                                    'application/json')
            MembershipID = str(MembershipID).upper()
            registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
            if registered_user is not None:
                phone = registered_user.phone
                mobile_no = encrypt_decrypt(phone, 'D')

                otp = genOtp(mobile_no, 'ML')
                return makeResponse(json.dumps({"status": "1", "message": "OTP Sent Successfully"}), 200,
                                    'application/json')
            else:

                return makeResponse(json.dumps({"errors": ["No Details Found With This MembershipId  "]}), 400,
                                    'application/json')
        except Exception as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ["Internal Server Error"]}), 500, 'application/json')


@blueprint_event.route("/api/Login/verifyOTP", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verifyJspMemberOTP():
    try:
        try:

            membership_id = request.json['MembershipID']
            otp = request.json['Otp']
        except (Exception, KeyError) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')
        if membership_id is None or membership_id.isspace():
            return makeResponse(json.dumps({"errors": ["invalid MembershipID"]}), 400, 'application/json')
        if otp is None or otp.isspace():
            return makeResponse(json.dumps({"errors": ["invalid otp"]}), 400, 'application/json')

        MembershipID = str(membership_id).upper()
        registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
        if registered_user is not None:
            phone = registered_user.phone
            mobile_no = encrypt_decrypt(phone, 'D')
            if verifyOtp(mobile_no, otp, "ML"):
                token = saveSession(membership_id, mobile_no, 'EL')
                membership_instance = Membership_details()
                details = membership_instance.get_membership_details(membership_id)
                profile = {}
                profile['membership_id'] = details['membership_id']
                profile['state_id'] = details['state_id']
                profile['district_id'] = details['district_id']
                profile['assembly_constituency_id'] = details['ac_no']
                profile['name'] = details['name']
                profile['image_url'] = details['img_url']

                return makeResponse(json.dumps({"authentication": "1", "profile": profile, "token": token}), 200,
                                    'application/json')
            return makeResponse(json.dumps({"authentication": "0", "message": "Login Failed"}), 200, 'application/json')

        return makeResponse(json.dumps({"errors": ["No Details Found With Given MembershipId "]}), 400,
                            'application/json')

    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error "]}), 500, 'application/json')


@blueprint_event.route("/api/ValidateToken", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def jspMemberTokenValidation():
    try:
        auth = request.authorization
        if auth:
            membership_id = auth.username

            token = auth.password
            status = 'L'
            status = member_session.query.filter_by(membership_id=membership_id, session_id=token,
                                                    action=status).first()
            if status is not None:
                return makeResponse(json.dumps({"status": "1", "message": "Valid Token"}), 200, 'application/json')
            else:
                return makeResponse(json.dumps({"status": "0", "message": "Not Valid Token"}), 200, 'application/json')
        return makeResponse(json.dumps({"errors": ["No Token Passed in header"]}), 400, 'application/json')



    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error"]}), 500, 'application/json')


@blueprint_event.route("/api/LoginWithOTP", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def LoginWithOTP():
    if request.method == 'GET':
        try:

            MembershipID = request.values.get('MembershipID')

            if MembershipID is None or MembershipID.isspace():
                return makeResponse(json.dumps({"errors": ["MembershipID should not be empty'"]}), 400,
                                    'application/json')
            MembershipID = str(MembershipID).upper()
            registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()
            if registered_user is not None:
                phone = registered_user.phone
                mobile_no = encrypt_decrypt(phone, 'D')

                otp = genOtp(mobile_no, 'ML')
                return makeResponse(json.dumps({"status": "1", "message": "OTP Sent Successfully"}), 200,
                                    'application/json')
            else:

                return makeResponse(json.dumps({"errors": ["No Details Found With This MembershipId  "]}), 400,
                                    'application/json')
        except Exception as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ["Internal Server Error"]}), 500, 'application/json')



# @blueprint_event.route("/api/membersdata", methods=['GET'])
# @crossdomain(origin="*", headers="Content-Type")
# @requires_api_token
# def jspmembersdata():
#     try:
#         data_type = request.values.get('type')
#         value = request.values.get('value')
#         state = None
#         page_no=request.values.get('page_no')
#         if page_no is None:
#             page_no=1
#         if data_type == 'assembly':
#             state = request.values.get('state')
#         if data_type is None:
#             return json.dumps({"errors": ['parameter type missing'], "data": {"status": 0}})
#         if value is None:
#             return json.dumps({"errors": ['parameter value missing'], "data": {"status": 0}})
#         membership_instance = Membership_details()
#         saverequestdata(data_type,value,state,page_no)
#         data = membership_instance.get_members_details(data_type=data_type, value=value, state=state,page_no=page_no)

#         for member in data:
#             member['phone'] = encrypt_decrypt(member['phone'], 'D')
#         # data=[{"phone":"919652262151","membership_id":""},{"phone":"918790079997","membership_id":""},{"phone":"919676881991","membership_id":""},{"phone":"918328468195","membership_id":""},{"phone":"917680076268","membership_id":""},{"phone":"919959951123","membership_id":""}]
#         return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

#     except (Exception, KeyError) as e:

#         return json.dumps({"errors": [str(e)], "data": {"status": 0}})

def saverequestdata(data_type,value,state,page_no):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("insert into request_data(name,data_type,value,state,page_no) values(%s,%s,%s,%s,%s)",("membersdata",data_type,value,state,page_no))
        con.commit()
        con.close()

    except Exception as e:
        print str(e)
@blueprint_event.route("/api/GuestLogin", methods=['POST', 'OPTIONS'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def GuestLogin():
    try:
        try:
            mobile = request.json["Mobile"]
        except (KeyError, Exception) as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

        if 10 <= len(str(mobile)) <= 15:

            con = None
            con = psycopg2.connect(config.DB_CONNECTION)
            cur = con.cursor(cursor_factory=RealDictCursor)
            cur.execute("insert into guests(mobile)values(%s)", (mobile,))
            con.commit()
            con.close()
            genOtp(mobile, 'GL')
            return makeResponse(json.dumps({"status": "1", "message": "OTP Sent Successfully"}), 200,
                                'application/json')
        else:

            return makeResponse(json.dumps({"errors": ["Invalid Mobile Number "]}), 400, 'application/json')

    except (psycopg2.Error, Exception) as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Something Went Wrong"]}), 500, 'application/json')


@blueprint_event.route("/api/masterdata", methods=['GET'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def Jspmasterdata():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        data = []
        cur.execute("select id,Name from state")
        state_data = cur.fetchall()
        for state in state_data:
            d = {}
            d['id'] = state['id']
            d['name'] = state['name']
            d['districts'] = []
            cur.execute("select seq_id,district_name from districts where state_id=%s", (state['id'],))
            district_rows = cur.fetchall()
            for district in district_rows:
                dr = {}
                dr['id'] = district['seq_id']
                dr['name'] = str(district['district_name']).rstrip()
                dr['constituencies'] = []
                cur.execute(
                    "select constituency_id,constituency_name from assembly_constituencies where district_name=%s",
                    (dr['name'],))
                constituency_rows = cur.fetchall()
                for constituency in constituency_rows:
                    cr = {}
                    cr['id'] = constituency['constituency_id']
                    cr['name'] = str(constituency['constituency_name']).rstrip()
                    cr['booths'] = []
                    dr['constituencies'].append(cr)
                d["districts"].append(dr)
            data.append(d)
        return json.dumps({"errors": [], "data": {"status": 1, "children": data}})

    except (Exception, KeyError) as e:
        return str(e)


@blueprint_event.route("/api/SetPin", methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin="*", headers=["Content-Type","Authorization"])

@requires_EventsauthToken
@csrf.exempt
def memberSetPin():
    try:
        try:
            auth = request.authorization
            membership_id = auth.username

            pin = request.json['Pin']


        except (Exception, KeyError) as e:

            return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')

        if len(str(pin)) == 6:
            # try:
            #     pin =int(pin)
            # except Exception as e:
            #     return makeResponse(json.dumps({"errors":["Pin number Should be Numeric "]}),400,'application/json')
            MembershipID = str(membership_id).upper()

            registered_user = Memberships.query.filter_by(membership_id=MembershipID).first()

            if registered_user is not None:
                user_pin = encrypt_decrypt(str(pin), "E")

                registered_user.member_pin = user_pin
                db.session.commit()

                return makeResponse(json.dumps({"status": "1", "message": "Saved PIn Successfully"}), 200,
                                    'application/json')
            return makeResponse(json.dumps({"errors": ["No Details Found . Please try again"]}), 400,
                                'application/json')
        else:
            return makeResponse(json.dumps({"errors": ["user pin should be 6 digits"]}), 400, 'application/json')

    except Exception as e:
        print str(e)
        return makeResponse(json.dumps({"errors": ["Internal Server Error"]}), 500, 'application/json')


@blueprint_event.route("/api/Logout", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
@requires_EventsauthToken
def JspMemberLogout():
    try:
        auth = request.authorization
        membership_id = auth.username
        token = auth.password

        status = 'L'
        status = member_session.query.filter_by(membership_id=membership_id, session_id=token, action=status).first()
        if status is not None:
            status.action = 'E'
            db.session.commit()
            print status
        return makeResponse(json.dumps({"status": "1", "message": "Logged Out Successfully"}), 200, 'application/json')

    except (Exception, KeyError) as e:

        return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400, 'application/json')


@blueprint_event.route("/api/Login", methods=['POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def jspMemberLogin():
    if request.method == 'POST':
        try:
            try:
                MembershipID = request.json['MembershipID']
                password = request.json['Password']
            except (Exception, KeyError) as e:
                print str(e)

                return makeResponse(json.dumps({"errors": ['parameter ' + str(e) + " missing"]}), 400,
                                    'application/json')

            if MembershipID is None or MembershipID.isspace():
                return makeResponse(json.dumps({"errors": ["MembershipID should not be empty'"]}), 400,
                                    'application/json')

            if password is None or password.isspace():
                return makeResponse(json.dumps({"errors": ["Password should not be empty'"]}), 400, 'application/json')
            if len(password) != 6:
                return makeResponse(json.dumps({"errors": ["Password should  be 6 digits only'"]}), 400,
                                    'application/json')

            password = encrypt_decrypt(password, 'E')

            MembershipID = str(MembershipID).upper()
            registered_user = Memberships.query.filter_by(membership_id=MembershipID, member_pin=password).first()
            if registered_user is not None:
                phone = registered_user.phone
                mobile_no = encrypt_decrypt(phone, 'D')
                token = saveSession(MembershipID, mobile_no, 'EL')
                membership_instance = Membership_details()
                details = membership_instance.get_membership_details(MembershipID)
                profile = {}
                profile['membership_id'] = details['membership_id']
                profile['state_id'] = details['state_id']
                profile['district_id'] = details['district_id']
                profile['assembly_constituency_id'] = details['ac_no']
                profile['name'] = details['name']
                profile['image_url'] = details['img_url']
                # if details['img_url'] is not None and details['img_url']!='':
                #     sp_img_url=str(details['img_url']).split(".")[0]
                #     if sp_img_url.lower()=="https://janasenalive":
                #         profile['image_url']='https://janasenaparty.org/static/img/default-avatar.png'
                #     else:
                #         profile['image_url']=details['img_url']
                # else:
                #     profile['image_url']='https://janasenaparty.org/static/img/default-avatar.png'

                return makeResponse(json.dumps({"authentication": '1', "profile": profile, "token": token}), 200,
                                    'application/json')

            else:
                return makeResponse(json.dumps({"authentication": "0", "message": "Login Failed"}), 200,
                                    'application/json')
        except Exception as e:
            print str(e)
            return makeResponse(json.dumps({"errors": ["Internal Server Error"]}), 500, 'application/json')
