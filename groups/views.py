import os
import tempfile
from subprocess import Popen, PIPE
import psycopg2
from flask import Blueprint, request, render_template, flash, session, url_for
from flask_login import current_user, logout_user
from werkzeug.utils import redirect

import config
from appholder import db, csrf
from constants import empty_check
from dbmodels import Memberships
from groups.models import GroupMemberRole, LocalGroupAdmin, GroupMemberLink
from groups.utils import tree_changer, remove_admin
from main import genHash
from utilities.classes import encrypt_decrypt
from utilities.decorators import crossdomain, admin_login_required, local_admin_login_required
import simplejson as json

from utilities.general import checkUserAuth
from utilities.mailers import genOtp
from utilities.others import give_cursor_json

group = Blueprint('group', __name__,
                  template_folder='templates/groups')


@group.route('/group/create', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def group_create():
    """
    truncate table team_hierarchy;
    alter sequence team_hierarchy_id_seq restart 1;

    select id, name_path, nlevel(name_path) from team_hierarchy
    where nlevel(name_path)=1;

    (select name_path from team_hierarchy where nlevel(name_path)=1);
    select users from team_hierarchy where name_path in
    (select subpath(name_path,0,-1) from team_hierarchy where nlevel(name_path)=1);

    select * from team_hierarchy;
    select users from team_hierarchy where name_path <@
    (select name_path from team_hierarchy where nlevel(name_path)=1);
    :return:
    """
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
    except psycopg2.OperationalError as e:
        print e.pgerror
        return render_template('500.html')
    try:
        cur = con.cursor()
        if request.method == 'POST':
            parent_path = request.values.get('parent_path', '')
            group_name = request.values.get('group_name', None)
            group_type_id = request.values.get('group_type_id', None)
            if group_name in empty_check:
                return json.dumps({'errors': ['PARAMETER group_name EMPTY OR MISSING'], 'data': {'status': 0}})
            if parent_path != "":
                current_name_path = parent_path + '.' + group_name.lower().replace(" ", "_")
            else:
                current_name_path = group_name.lower().replace(" ", "_")
            cur.execute("""
                insert into team_hierarchy(team_name, name_path,group_type_id)
                values(%s, %s, %s)
                returning id
            """, (group_name, current_name_path, group_type_id))
            row = cur.fetchone()
            curr_id = row[0]
            con.commit()
            return json.dumps({'errors': [], 'data': {'status': 1, 'curr_id': curr_id}})

        elif request.method == 'GET':

            return render_template('groups/g1.html')

    except psycopg2.Error as e:
        print e.message
        return render_template('500.html')
    finally:
        con.close()
    return "Hello"


@group.route('/group/edit', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def group_edit():
    def temp(con, cur):
        if request.method == 'POST':
            current_path = request.values.get('current_path', None)
            group_name = request.values.get('group_name', None)
            group_type_id = request.values.get('group_type_id', None)
            if group_name in empty_check:
                return json.dumps({'errors': ['PARAMETER group_name EMPTY OR MISSING'], 'data': {'status': 0}})
            if current_path in empty_check:
                return json.dumps({'errors': ['PARAMETER group_name EMPTY OR MISSING'], 'data': {'status': 0}})
            cur.execute("""
                select * 
                from team_hierarchy
                where name_path=%s
            """, (current_path,))
            row = cur.fetchone()
            old_gname = row[3]
            if old_gname != group_name:
                # update the lower tier tree.
                # select 'A.B.G' || path as concatenated from tree where path <@ 'A.C'
                sm = current_path.split('.')
                print sm
                sm.pop()
                print sm
                sm.append(tree_changer(group_name))
                splited = '.'.join(sm)
                cur.execute("""
                    update team_hierarchy
                    set name_path = %s, team_name=%s
                    where id = %s
                """, (splited, group_name, row[0]))
                cur.execute("""
                    update team_hierarchy 
                    set name_path = %s || subpath(subpath(name_path, nlevel(%s)-1),1)
                    where name_path <@ %s;
                """, (splited, current_path, current_path))
                con.commit()
                pass
            return json.dumps({'errors': [], 'status': 0})
        pass

    return give_cursor_json(temp)


@group.route('/group_type/create', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def group_type_create():
    def temp(con, cur):
        if request.method == 'POST':
            parent_path = request.values.get('parent_path', '')
            group_name = request.values.get('group_name', None)
            if group_name in empty_check:
                return json.dumps({'errors': ['PARAMETER group_name EMPTY OR MISSING'], 'data': {'status': 0}})
            if parent_path != "":
                current_name_path = parent_path + '.' + group_name.lower().replace(" ", "_")
            else:
                current_name_path = group_name.lower().replace(" ", "_")
            cur.execute("""
                insert into group_types(group_name, group_path)
                values(%s, %s)
                returning id
            """, (group_name, current_name_path))
            row = cur.fetchone()
            curr_id = row[0]
            con.commit()
            return json.dumps({'errors': [], 'data': {'status': 1, 'curr_id': curr_id}})
        elif request.method == 'GET':

            return render_template('groups/g2.html')
        return "Not yet created"

    return give_cursor_json(temp)


@group.route('/group_type_list', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def group_type_list():
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            group_type_id = request.values.get('group_type_id')
            if group_type_id not in empty_check:
                cur.execute("""
                                select id,group_type_name
                                from group_types
                                where group_type_path <@ (select group_type_path from group_types where id=%s) and nlevel(group_type_path)=(select nlevel(group_type_path)+1 from group_types where id=%s)
                                
                            """, (group_type_id, group_type_id))
            else:
                cur.execute("""
                                                select id,group_type_name
                                                from group_types
                                                where nlevel(group_type_path)=1

                                            """)

            rows = cur.fetchall()

            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})

    return give_cursor_json(temp)


@group.route('/group_type_tree', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def group_type_tree():
    def temp(con, cur):
        if request.method == 'POST':
            cur.execute("""
                            select group_type_path, group_type_name  , nlevel(group_type_path),id
                            from group_types
                            order by nlevel(group_type_path),id
                        """)
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})
        pass

    return give_cursor_json(temp)


@group.route('/get_tree', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_tree():
    def temp(con, cur):
        if request.method == 'POST':
            cur.execute("""
                            select name_path, team_name, nlevel(name_path),group_type_id
                            from team_hierarchy
                            
                            order by nlevel(name_path),id
                        """)
            rows = cur.fetchall()

            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})
        pass

    return give_cursor_json(temp)


@group.route('/get_group_members', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_group_members():
    def temp(con, cur):
        if request.method == 'POST':
            current_path = request.values.get('current_path', None)
            count = request.values.get('count', None)
            print current_path, count
            if current_path in empty_check:
                return json.dumps({'errors': ['PARAMETER current_path NOT PROVIDED'], 'data': {'status': 0}})
            if count is None:
                cur.execute("""
                    select jm.name,gml.user_role,gml.id,lga.id as is_admin
                    from janasena_membership jm
                    left join group_member_link gml on gml.member_id = jm.jsp_membership_seq_id
                    left join local_group_admin lga on gml.id = lga.member_id
                    where gml.path_id = (select id from team_hierarchy where name_path=%s )
                    order by gml.id
                """, (current_path,))
            else:
                cur.execute("""
                                   select jm.name,gml.user_role,gml.id,lga.id as is_admin
                                   from janasena_membership jm
                                   left join group_member_link gml on gml.member_id = jm.jsp_membership_seq_id
                                   left join local_group_admin lga on gml.id = lga.member_id
                                   where gml.path_id in (select id from team_hierarchy where name_path <@ %s )
                                   order by gml.id
                               """, (current_path,))
            rows = cur.fetchall()
            print rows
        return json.dumps({"errors": [], 'data': {'status': 1, 'children': rows}})

    return give_cursor_json(temp)


@group.route('/add_members', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def add_members():
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            member_list = request.values.get('member_ids', '[]')
            member_list = json.loads(member_list)
            path = request.values.get('current_path', None)
            role = request.values.get('role', None)
            print path, member_list
            if path in empty_check:
                return json.dumps({'errors': ['PARAMETER path MISSING'], 'data': {'status': 0}})
            elif not member_list:
                return json.dumps({'errors': ['NO MEMBER LIST PROVIDED'], 'data': {'status': 0}})
            elif role in empty_check:
                return json.dumps({'errors': ['NO role PROVIDED'], 'data': {'status': 0}})
            # check if user already has some role assigned
            cur.execute("""
                            select count(gml.id)
                            from group_member_link gml
                            where member_id in %s
                            """, (tuple(member_list),))
            already_assigned = cur.fetchone()[0]
            if already_assigned != 0:
                return json.dumps({'errors': ['A ROLE HAS ALREADY BEEN ASSIGNED TO HIM'], 'status': 0, 'data': {}})
            # Get the count of the members added
            cur.execute("""
                        select count(gml.id)
                        from group_member_link gml
                        join team_hierarchy th on th.id = gml.path_id
                        where th.name_path=%s and gml.user_role=%s
                        """, (path, role))
            current_count = cur.fetchone()[0]
            # Get the max count for that role

            cur.execute("""
                        select max_count
                        from group_member_roles 
                        where id=%s
                        """, (role,))
            max_count = cur.fetchone()[0]
            print current_count, max_count
            # if the count of the members added greater than equal to the members added throw error else add the user
            if current_count >= max_count:
                return json.dumps({'errors': ['MAX NUMBER OF PEOPLE FOR THAT ROLE EXCEEDED'], 'status': 0, 'data': {}})
            else:
                cur.execute("""
                select id
                from team_hierarchy
                where name_path=%s 
                """, (path,))
                path_id = cur.fetchone()
                if not path_id:
                    return json.dumps({'errors': ['INVALID PATH PROVIDED'], 'status': 0})
                path_id = path_id[0]
                tup = []

                for member in member_list:
                    te = cur.mogrify("(%s,%s,%s)", (path_id, int(member), int(role)))
                    tup.append(te)
                args_str = ','.join(tup)

                cur.execute("""
                insert into group_member_link(path_id,member_id,user_role)
                values
                """ + args_str)
                con.commit()

                return json.dumps({'errors': [], 'data': {'status': 1, 'msg': 'Added members successfully'}})

    return give_cursor_json(temp)
    pass


@group.route('/group/member_edit', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def edit_members():
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            current_path = request.values.get('current_path');
            role_id = request.values.get('role_id');
            gml_id = request.values.get('id')
            # Get the count of the members added
            cur.execute("""
                                    select count(gml.id)
                                    from group_member_link gml
                                    join team_hierarchy th on th.id = gml.path_id
                                    where th.name_path=%s and gml.user_role=%s
                                    """, (current_path, role_id))
            current_count = cur.fetchone()[0]
            # Get the max count for that role

            cur.execute("""
                                   select max_count
                                   from group_member_roles 
                                   where id=%s
                                   """, (role_id,))
            max_count = cur.fetchone()[0]
            if current_count >= max_count:
                return json.dumps({'errors': ['MAX NUMBER OF PEOPLE FOR THAT ROLE EXCEEDED'], 'status': 0, 'data': {}})
            else:
                print "b4 exe"
                cur.execute("""
                            update group_member_link
                            set user_role = %s
                            where id=%s
                            
                            
                            """, (role_id, gml_id))

                print "after exe"
                con.commit()
                print "after commit"
            return json.dumps({'errors': [], 'data': {'status': 1}})

    return give_cursor_json(temp)


@group.route('/delete_member', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def delete_member():
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            member_list = request.values.get('delete_id', '[]')
            member_list = json.loads(member_list)

            if not member_list:
                return json.dumps({'errors': ['NO MEMBER LIST PROVIDED'], 'data': {'status': 0}})

            for i in member_list:
                print i
                cur.execute("""
                    delete from group_member_link
                    where id = %s
                """, (i['id'],))
                cur.execute("""
                     update local_group_admin
                    set status='D'
                    where member_id = %s
                                """, (i['id'],))
            con.commit()
            return json.dumps({'errors': [], 'data': {'status': 1}})

    return give_cursor_json(temp)


@group.route('/search_unassigned_members', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def search_unassigned_members():
    def temp(con, cur):
        if request.method == 'POST':
            mobile_no = request.values.get('mobile_no')
            print request.values

            mobile_query = ''
            params = []
            # params.append(state_id)
            if mobile_no in empty_check:
                return json.dumps({'errors': ['PLEASE ENTER A PHONE NUMBER'], 'status': 0})
            else:
                if mobile_no[0].isalpha():

                    mobile_query = 'membership_id=%s'
                    params.append(mobile_no)
                else:
                    print 'i m here'
                    enc_mobile = encrypt_decrypt(mobile_no, 'E')
                    mobile_query = 'phone=%s'
                    params.append(enc_mobile)

            base_query = """
            select jsp_membership_seq_id,name
            from janasena_membership jm
            left join group_member_link gml on gml.member_id = jm.jsp_membership_seq_id
            """
            where = ' where gml.id is null'

            query = base_query
            query = query + where + (mobile_query if not mobile_query else ' and ' + mobile_query)
            print query, params
            cur.execute(query, params)

            rows = cur.fetchall()
            print rows
            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}}, default=str)

        pass

    return give_cursor_json(temp)


@group.route('/default_members', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def default_members():
    def temp(con, cur):
        if request.method == 'POST':
            cur.execute("""
            select jm.name,jm.gender,jm.age,jsp_membership_seq_id, gml.id
            from janasena_membership jm
            left join group_member_link gml on gml.member_id = jm.jsp_membership_seq_id
            where gml.id is null
            """)
            rows = cur.fetchall()
            return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}}, default=str)

    return give_cursor_json(temp)


@group.route('/groups/add_role', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def add_role():
    def temp(con, cur):
        if request.method == 'POST':
            role_name = request.values.get('role_name', None)
            group_type_id = request.values.get('group_type_id', None)
            max_count = request.values.get('max_count', 1)

            group_member_role = GroupMemberRole(role_name=role_name, group_type_id=group_type_id, max_count=max_count)
            db.session.add(group_member_role)
            db.session.flush()
            db.session.commit()
            return json.dumps({'errors': [], 'data': {'status': 1}}, default=str)

    return give_cursor_json(temp)


@group.route('/group/get_roles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_roles():
    def temp(con, cur):
        if request.method == 'POST':

            group_type_id = request.values.get('group_type_id', None)

            print group_type_id
            roles = GroupMemberRole.query.filter_by(group_type_id=group_type_id).all()
            dat = []
            for role in roles:
                # print role
                t = {}
                t['id'] = role.id
                t['name'] = role.role_name
                t['max_count'] = role.max_count
                dat.append(t)
            db.session.commit()
            return json.dumps({'errors': [], 'data': {'status': 1, 'children': dat}}, default=str)

    return give_cursor_json(temp)


@group.route('/group/get_all_roles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_all_roles():
    def temp(con, cur):
        if request.method == 'POST':

            roles = GroupMemberRole.query.all()
            dat = {}
            for role in roles:
                # print role
                dat[str(role.id)] = [role.role_name, role.max_count]

            db.session.commit()
            return json.dumps({'errors': [], 'data': {'status': 1, 'children': dat}}, default=str)

    return give_cursor_json(temp)


@group.route('/group/get_avalilable_roles', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_avalilable_roles():
    def temp(con, cur):
        if request.method == 'POST':

            group_type_id = request.values.get('group_type_id', None)

            print group_type_id
            roles = GroupMemberRole.query.filter_by(group_type_id=group_type_id)
            dat = []
            for role in roles:
                # print role
                t = {}
                t['id'] = role.id
                t['name'] = role.role_name
                t['max_count'] = role.max_count
                dat.append(t)
            db.session.commit()
            return json.dumps({'errors': [], 'data': {'status': 1, 'children': dat}}, default=str)

    return give_cursor_json(temp)


@group.route('/group/delete', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def delete_group():
    def temp(con, cur):
        current_path = request.values.get('current_path')
        print request.values
        if current_path in empty_check:
            return json.dumps({'errors': ['PATH NOT PROVIDED'], 'status': 0})

        # Get the count of the members added
        cur.execute("""
                            select count(gml.id)
                            from group_member_link gml
                            join team_hierarchy th on th.id = gml.path_id
                            where th.name_path @> %s
                            """, (current_path,))
        current_count = cur.fetchone()[0]
        print current_count
        if current_count > 0:
            return json.dumps({'errors': ['ONE OF THE GROUPS HAS MEMBERS ASSIGNED. CANNOT DELETE'], 'status': 0})
        # Get the count of the members added
        cur.execute("""
                                select count(id)
                                from team_hierarchy th
                                where th.name_path <@ %s 
                                """, (current_path,))
        current_count = cur.fetchone()[0]
        print current_count
        if current_count > 1:
            return json.dumps({'errors': ['ONE OF THE GROUPS HAS SUBGROUPS ASSIGNED. CANNOT DELETE'], 'status': 0})
        else:
            # Update status to deactivated
            # delete path
            cur.execute("""
                delete from team_hierarchy
                where name_path = %s
                                            """, (current_path,))

            # deactivate all admin roles for this path
            remove_admin(current_path,cur)
            con.commit()
        return json.dumps({'errors': [], 'data': {'status': 1}, 'status': 1})

    return give_cursor_json(temp)


@group.route('/group/assign_admin', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def assign_admin():
    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            member_id = request.values.get('member_id')
            path_id = request.values.get('current_path')

            if not member_id:
                return json.dumps({'errors': ['NO MEMBER LIST PROVIDED'], 'data': {'status': 0}})
            cur.execute("""
            select id from team_hierarchy where name_path=%s""", (path_id,))
            row = cur.fetchone()
            if row is None:
                return json.dumps({'errors': ['INVALID PATH PROVIDED'], 'status': 0})
            path_id = row[0]
            cur.execute(
                """
                select id 
                from local_group_admin
                where path_id=%s
                """, (path_id,)
            )
            row = cur.fetchone()
            if row is None:
                # Insert as there is no entry
                cur.execute("""
                    insert into local_group_admin(path_id,member_id)
                    values(%s,%s)
                """, (path_id, member_id))
                pass
            else:
                # Update it as there is a entry
                cur.execute("""
                                    update local_group_admin
                                    set member_id=%s
                                    where id=%s
                                """, (member_id, row[0]))
            con.commit()
            return json.dumps({'errors': [], 'data': {'status': 1}})

    return give_cursor_json(temp)


@group.route('/group/lalogin', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
@csrf.exempt
def lalogin():
    if request.method == 'GET':
        return render_template('groups/login.html')
    elif request.method == 'POST':
        phone = request.values.get('username')
        password = request.values.get('password')
        utype = 'LA'
        if checkUserAuth(phone, password, utype):
            session['next'] = url_for('group.local_admin_dashboard')
            session['current'] = url_for('group.lalogin')
            session['member_logged_in'] = True
            return redirect('/group/local_admin_dashboard')

        else:
            flash('Invalid Details', 'error')
            return render_template('groups/login.html',)

@group.route('/group/group_lalogout', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
def group_lalogout():
    logout_user()

    session.clear()

    return redirect(url_for('group.lalogin'))


@group.route('/group/forgotlaPassword', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def forgotlapass():
    if request.method == 'GET':
        return render_template('/groups/la_forgot_pass.html')
    elif request.method == 'POST':
        phone = request.values.get('phone')
        print request.values
        u = Memberships.query.filter(Memberships.phone == encrypt_decrypt(phone, 'E')).first()
        if not u:
            return json.dumps({'errors': ['NOT A REGISTERED MEMBER'], 'status': 0})

        gml = GroupMemberLink.query.filter(GroupMemberLink.member_id == u.id).first()
        if not gml:
            return json.dumps({'errors': ['NOT ASSIGNED ANY ROLE'], 'status': 0})
        s = LocalGroupAdmin.query.filter(LocalGroupAdmin.member_id == gml.id).first()
        if not s:
            return json.dumps({'errors': ['YOU ARE NOT AN LOCAL ADMIN'], 'status': 0})
        else:
            genOtp('91' + phone, type='LA')
        return json.dumps({'errors': [], 'status': 1})


@group.route('/group/verify_otp', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def verify_otp():
    def temp(con, cur):
        if request.method == 'POST':
            phone = request.values.get('phone')
            otp = request.values.get('otp')
            if phone in empty_check:
                return json.dumps({'errors': ['PARAMETER PHONE NOT FOUND'], 'status': 0})
            if otp in empty_check:
                return json.dumps({'errors': ['PARAMETER otp NOT FOUND'], 'status': 0})
            u_type = 'LA'
            return json.dumps({'errors': [], 'status': 1})
            if verifyOtp(phone, otp, u_type):
                return json.dumps({'errors':[],'status':1})
            else:
                return json.dumps({'errors': [], 'status': 0})
    return give_cursor_json(temp)

@group.route('/group/set_password', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def set_password():
    def temp(con, cur):
        unique = "tomatoPotatoadmin"
        if request.method == 'POST':
            p1 = request.values.get('p1')
            p2 = request.values.get('p2')
            phone = request.values.get('phone')
            if phone in empty_check:
                return json.dumps({'errors': ['PARAMETER PHONE NOT FOUND'], 'status': 0})
            if p1 in empty_check:
                return json.dumps({'errors': ['PARAMETER p1 NOT FOUND'], 'status': 0})
            if p2 in empty_check:
                return json.dumps({'errors': ['PARAMETER p2 NOT FOUND'], 'status': 0})
            if p1 != p2:
                return json.dumps({'errors':['PASSWORDS DONT MATCH'],'status':0})
            else:
                u = Memberships.query.filter(Memberships.phone == encrypt_decrypt(phone, 'E')).first()
                if not u:
                    return json.dumps({'errors': ['NOT A REGISTERED MEMBER'], 'status': 0})

                gml = GroupMemberLink.query.filter(GroupMemberLink.member_id == u.id).first()
                if not gml:
                    return json.dumps({'errors': ['NOT ASSIGNED ANY ROLE'], 'status': 0})
                s = LocalGroupAdmin.query.filter(LocalGroupAdmin.member_id == gml.id).first()
                if not s:
                    return json.dumps({'errors': ['NOT A LOCAL ADMIN'], 'status': 0})
                else:
                    s.password_hash = genHash(p1, unique)
                    db.session.commit()
                    return json.dumps({'errrors':[],'msg':'password updated successfully','status':1})
    return give_cursor_json(temp)


@group.route('/group/local_admin_dashboard', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def local_admin_dashboard():
    def temp(con, cur):



        return render_template('groups/local_group.html',)
    return give_cursor_json(temp)


@group.route('/group/check_local_admin', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@csrf.exempt
def check_local_admin():
    print "I am here"

    def temp(con, cur):
        if request.method == 'POST':
            print request.values
            phone = request.values.get('phone')

            u = Memberships.query.filter(Memberships.phone == encrypt_decrypt(phone, 'E')).first()
            if not u:
                return json.dumps({'errors': ['NOT A REGISTERED MEMBER'], 'status': 0})

            gml = GroupMemberLink.query.filter(GroupMemberLink.member_id == u.id).first()
            if not gml:
                return json.dumps({'errors': ['NOT ASSIGNED ANY ROLE'], 'status': 0})
            s = LocalGroupAdmin.query.filter(LocalGroupAdmin.member_id == gml.id).first()
            if not s:
                return json.dumps({'errors': ['NOT AN LOCAL ADMIN'], 'status': 0})
            if not s.password_hash:
                return json.dumps({'errors': ['SET A PASSWORD BY CLICKING FORGOT PASSWORD'], 'status': 2})
            else:
                return json.dumps({'errors': [], 'status': 1})

    return give_cursor_json(temp)


@group.route('/group/get_la_tree', methods=['GET', 'POST'])
@crossdomain(origin="*", headers="Content-Type")
@local_admin_login_required
def get_la_tree():
    def temp(con, cur):
        if request.method == 'POST':
            print "I am here"
            if session['t'] == 'LA':
                lga = LocalGroupAdmin.query.filter_by(id=current_user.id).first()
                path_id = lga.path_id
                cur.execute("""
                                select name_path, team_name, nlevel(name_path),group_type_id
                                from team_hierarchy
                                where name_path <@ (select name_path from team_hierarchy where id=%s)
                                order by nlevel(name_path),id
                            """, (path_id,))
                rows = cur.fetchall()

                return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})
            elif session['t'] == 'A':
                cur.execute("""
                                            select name_path, team_name, nlevel(name_path),group_type_id
                                            from team_hierarchy

                                            order by nlevel(name_path),id
                                        """)
                rows = cur.fetchall()

                return json.dumps({'errors': [], 'data': {'status': 1, 'children': rows}})
            else:
                return json.dumps({'errors': [], 'data': {'status': 0}})
        pass

    return give_cursor_json(temp)