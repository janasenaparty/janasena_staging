from utilities.others import give_cursor_json

def tree_changer(path_string):
    return path_string.lower().replace(' ', '_').replace('(','_').replace(')','_').replace('-','_').replace('.','_').replace(',','')

def add_mandals():
    def temp(con, cur):
        cur.execute("""
            select lower(zm.district_name),zm.zptc_name,th.name_path
            from zptc_members zm
            join districts d on lower(d.district_name) = lower(zm.district_name)
            join team_hierarchy th on lower(d.district_name) = lower(team_name)
        """)
        rows = cur.fetchall()
        query = 'insert into team_hierarchy(name_path,team_name,group_type_id) values '
        params = []
        string = ''
        group_type_id = 9
        length = len(rows)
        for i, row in enumerate(rows):
            district_name, zptc_name, parent_path = row
            path = parent_path + '.' + zptc_name.lower().replace(' ', '_').replace('(','_').replace(')','_').replace('-','_').replace('.','_').replace(',','')
            string += "('%s','%s',%s)"
            params.extend((path, zptc_name, group_type_id))
            if i != (length - 1):
                string += ', '
            pass
        query = query + string
        # print params
        # print query.format(tuple(params))
        print query%tuple(params)
        cur.execute(query%tuple(params))
        con.commit()
        pass

    give_cursor_json(temp)

def remove_admin(current_path, cur):
    cur.execute("""
                    update local_group_admin
                    set status='D'
                    where path_id =
                    (select id 
                    from group_member_link 
                    where path_id=
                    (select id 
                    from team_hierarchy 
                    where name_path=%s))
                                                            """, (current_path,))

