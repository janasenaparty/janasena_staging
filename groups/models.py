# sqlacodegen --outfile models.py postgresql://postgres:root@localhost:5432/janasena --tables group_member_roles
from sqlalchemy import Column, Integer, String, text, ARRAY
from sqlalchemy.sql.sqltypes import NullType, INTEGER

from appholder import db
from flask_login import UserMixin

class GroupMemberRole(db.Model):
    __tablename__ = 'group_member_roles'

    id = Column(Integer, primary_key=True, server_default=text("nextval('group_types_seq'::regclass)"))
    role_name = Column(String(100))
    group_type_id = Column(Integer)
    max_count = Column(Integer, server_default=text("1"))


class LocalGroupAdmin(db.Model,UserMixin):
    __tablename__ = 'local_group_admin'

    id = Column(Integer, primary_key=True, server_default=text("nextval('local_group_admin_id_seq'::regclass)"))
    member_id = Column(Integer)
    path_id = Column(Integer)
    status = Column(String(3), server_default=text("'A'::character varying"))
    password_hash = Column(String(512))




class GroupMemberLink(db.Model):
    __tablename__ = 'group_member_link'

    id = Column(Integer, primary_key=True, server_default=text("nextval('group_member_link_seq'::regclass)"))
    path_id = Column(Integer)
    member_id = Column(Integer)
    status = Column(String(3), server_default=text("'A'::character varying"))
    user_role = Column(Integer)

class TeamHierarchy(db.Model):
    __tablename__ = 'team_hierarchy'

    id = Column(Integer, primary_key=True, server_default=text("nextval('team_hierarchy_id_seq'::regclass)"))
    users = Column(ARRAY(INTEGER()), server_default=text("ARRAY[]::integer[]"))
    name_path = Column(NullType, index=True)
    team_name = Column(String(100))
    group_type_id = Column(Integer)
    status = Column(String(3), server_default=text("'A'::character varying"))
