
create sequence shatagni_magazine_team_seq;
create table shatagni_magazine_team(seq_id integer NOT NULL DEFAULT nextval('shatagni_magazine_team_seq'::regclass) primary key,
                 phone varchar unique,
                 name varchar,
                 email varchar,
                pincode varchar,
                  landmark text,
                  house_no varchar,
                  street_village text,
                  city_town text,
                  state varchar,
                create_dttm timestamp with time zone default now(),
                 update_dttm timestamp with time zone default now()
               );
alter table shatagni_magazine_team add column pin varchar;
create sequence shatagni_magazine_session_seq;
            CREATE TABLE public.shatagni_magazine_session
(
    seq_id integer NOT NULL DEFAULT nextval('shatagni_magazine_session_seq'::regclass),
    id int ,
    phone character varying(15) ,
    session_id character varying(100) ,
    action character varying(5)  ,
    create_dttm timestamp with time zone DEFAULT now(),
    update_dttm timestamp with time zone DEFAULT now(),
    CONSTRAINT magazine_session_pkey PRIMARY KEY (seq_id)
);


create sequence shatagni_magazine_subscription_seq;
CREATE TABLE public.shatagni_magazine_subscription
(
    seq_id integer NOT NULL DEFAULT nextval('shatagni_magazine_subscription_seq'::regclass),
    member_id int references shatagni_magazine_team(seq_id) ,
    subscription_plan int,
    subscription_start_date date,
    subscription_end_date  date ,
    CONSTRAINT subscription_pkey PRIMARY KEY (seq_id)
);

   

create sequence shatagni_magazine_payment_seq;
CREATE TABLE public.shatagni_magazine_payment
(
    seq_id integer NOT NULL DEFAULT nextval('shatagni_magazine_payment_seq'::regclass),
    member_id int  ,
    payment_status varchar ,
  transaction_id varchar,
  transaction_payment character varying, 
  order_id varchar,
  payment_id varchar,
  payment_method varchar,
  create_dttm timestamp with time zone DEFAULT now(),
    update_dttm timestamp with time zone DEFAULT now(),
    CONSTRAINT shatagni_magazine_payment_pkey PRIMARY KEY (seq_id)
);